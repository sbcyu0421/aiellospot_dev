// ISoundAIService.aidl
package ai.aiello.soundaiservice;

// Declare any non-default types here with import statements
import ai.aiello.soundaiservice.ISAIClientCallback;

interface ISoundAIService {

                        int getPid();

                       void initSaiClient(boolean imgChannelAligned, String mac, String wakeupword);

                       void startBeam(float angle);

                       void stopBeam();

                       void startVoip();

                       void stopVoip();

                       void release();

                       void registerCallback(ISAIClientCallback cb);

                       void unregisterCallback(ISAIClientCallback cb);

                       void spotOnASR(boolean onASR);

                        void changeWakeupWord(String wakeupword);

}
