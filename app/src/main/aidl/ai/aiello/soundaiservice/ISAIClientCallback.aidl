// ISAIClientCallback.aidl
package ai.aiello.soundaiservice;

// Declare any non-default types here with import statements

interface ISAIClientCallback {

    oneway void onAsrDataCallback(in byte[] buffer, int size);

    oneway void onWakeupCallback(float wakeup_angle, String wakeup_word, float score) ;

    oneway void onVoipDataCallback(in byte[] buffer, int size);

    oneway void onVadCallback(int type);

}
