package ai.aiello.aiellospot.utlis;

import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class Downloader {

    private static Downloader instance;
    private static String TAG = Downloader.class.getSimpleName();

    public static Downloader getInstance() {
        if (instance == null) {
            instance = new Downloader();
        }
        return instance;
    }

    public interface OnDownLoadListener {
        void onProgress(int progress);

        void onSuccess(File file);

        void onFail(String error);
    }

    public void download(String urlStr, OnDownLoadListener listener) {
        Log.d(TAG, "urlStr:" + urlStr);
        InputStream in = null;
        FileOutputStream out = null;
        try {
            URL url = new URL(urlStr);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);
            urlConnection.setConnectTimeout(3 * 1000);
            urlConnection.setReadTimeout(3 * 1000);
            urlConnection.setRequestProperty("Connection", "Keep-Alive");
            urlConnection.setRequestProperty("Charset", "UTF-8");
            urlConnection.setRequestProperty("Accept-Encoding", "gzip, deflate");

            urlConnection.connect();
            long bytetotal = urlConnection.getContentLength();
            long bytesum = 0;
            int byteread = 0;
            in = urlConnection.getInputStream();

            String fileName = urlStr.substring(urlStr.lastIndexOf("/") + 1);
            File file = new File("/sdcard/ads/", fileName);
            if (!file.exists()){
                file.deleteOnExit();
            }
            boolean c = file.createNewFile();
            out = new FileOutputStream(file);
            Log.d(TAG, "download" + urlStr + "start");
            byte[] buffer = new byte[10 * 1024];// 8k ~ 32K
            int progress = 0;
            while ((byteread = in.read(buffer)) != -1) {
                bytesum += byteread;
                out.write(buffer, 0, byteread);
                progress = (int) (bytesum * 100L / bytetotal);
                listener.onProgress(progress);
            }
            if (bytesum == bytetotal) {
                listener.onSuccess(file);
            } else {
                listener.onFail("download not completed");
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            listener.onFail(e.getMessage());
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ignored) {

                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ignored) {

                }
            }
        }
    }


}
