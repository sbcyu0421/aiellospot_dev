package ai.aiello.aiellospot.utlis;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class HomeUIRenderer {

    static private final String TAG = HomeUIRenderer.class.getSimpleName();

    static public HashMap<String, JSONObject> getModulePositionSet() throws JSONException {
        HashMap<String, JSONObject> positionMap = new HashMap<String, JSONObject>();
//        if (!AdsManager.getInstance().getLayout().toString().equals("")) {
//            positionMap.put("AdsModule", AdsManager.getInstance().getLayout());
//        }
//        if (!TaxiManager.getInstance().getLayout().toString().equals("")) {
//            positionMap.put("TaxiModule", TaxiManager.getInstance().getLayout());
//        }
//        if (!CheckInOutManager.getInstance().getLayout().toString().equals("{}")) {
//            positionMap.put("CheckInOutModule", CheckInOutManager.getInstance().getLayout());
//        }
//
//        if (!UserAlarmManager.getInstance().getLayout().toString().equals("{}")) {
//            positionMap.put("UserAlarmModule", UserAlarmManager.getInstance().getLayout());
//        }
//        if (!IoTManager.getInstance().getLayout().toString().equals("{}")) {
//            positionMap.put("IoTModule", IoTManager.getInstance().getLayout());
//        }
//        if (!MusicServiceManager.getInstance().getLayout().toString().equals("{}")) {
//            positionMap.put("MusicServiceModule", MusicServiceManager.getInstance().getLayout());
//        }
//        if (!VoipManager.getInstance().getLayout().toString().equals("{}")) {
//            positionMap.put("VoipModule", VoipManager.getInstance().getLayout());
//        }
////        positionMap.put("NotificationModule", NotificationManager.getLayout()); // not singleton
//
//        if (!RoomStatusManager.getInstance().getLayout().toString().equals("{}")) {
//            if (!RoomStatusManager.getInstance().getLayout().isNull("mur")) {
//                positionMap.put("RoomStatusModule_mur", new JSONObject(RoomStatusManager.getInstance().getLayout().getString("mur")));
//            }
//            if (!RoomStatusManager.getInstance().getLayout().isNull("dnd")) {
//                positionMap.put("RoomStatusModule_dnd", new JSONObject(RoomStatusManager.getInstance().getLayout().getString("dnd")));
//            }
//        }
//        if (!SurroundingManager.getInstance().getLayout().toString().equals("{}")) {
//            if (!SurroundingManager.getInstance().getLayout().isNull("sights")) {
//                positionMap.put("SurroundingModule_sights", new JSONObject(SurroundingManager.getInstance().getLayout().getString("sights")));
//            }
//            if (!SurroundingManager.getInstance().getLayout().isNull("transport")) {
//                positionMap.put("SurroundingModule_transport", new JSONObject(SurroundingManager.getInstance().getLayout().getString("transport")));
//            }
//            if (!SurroundingManager.getInstance().getLayout().isNull("cuisine")) {
//                positionMap.put("SurroundingModule_cuisine", new JSONObject(SurroundingManager.getInstance().getLayout().getString("cuisine")));
//            }
//            if (!SurroundingManager.getInstance().getLayout().isNull("living")) {
//                positionMap.put("SurroundingModule_living", new JSONObject(SurroundingManager.getInstance().getLayout().getString("living")));
//            }
//        }

//        positionMap.put("AdsModule", new JSONObject(""));
//        positionMap.put("UserAlarmModule", new JSONObject(""));
//        positionMap.put("CheckInOutModule", new JSONObject("{\"skin\": \"Classic\", \"page\": 1.0, \"position\": 6.0}"));
        positionMap.put("IoTModule", new JSONObject("{\"skin\": \"Classic\", \"page\": 1.0, \"position\": 1.0}"));
        positionMap.put("MusicServiceModule", new JSONObject("{\"skin\": \"Classic\", \"page\": 1.0, \"position\": 2.0}"));
//        positionMap.put("NotificationModule", new JSONObject(""));
        positionMap.put("RoomStatusModule_mur", new JSONObject("{\"skin\": \"Classic\", \"page\": 1.0, \"position\": 3.0}"));
//        positionMap.put("RoomStatusModule_dnd", new JSONObject("{\"skin\": \"Classic\", \"page\": 1.0, \"position\": 5.0}"));
        positionMap.put("SurroundingModule_sights", new JSONObject("{\"skin\": \"Classic\", \"page\": 2.0, \"position\": 1.0}"));
        positionMap.put("SurroundingModule_transport", new JSONObject("{\"skin\": \"Classic\", \"page\": 2.0, \"position\": 2.0}"));
        positionMap.put("SurroundingModule_cuisine", new JSONObject("{\"skin\": \"Classic\", \"page\": 2.0, \"position\": 3.0}"));
        positionMap.put("SurroundingModule_living", new JSONObject("{\"skin\": \"Classic\", \"page\": 2.0, \"position\": 4.0}"));
//        positionMap.put("TaxiModule", new JSONObject("{\"skin\": \"Classic\", \"page\": 2.0, \"position\": 5.0}"));
//        positionMap.put("CheckInOutModule", new JSONObject("{\"skin\": \"Classic\", \"page\": 2.0, \"position\": 6.0}"));
//        positionMap.put("VoipModule", new JSONObject("{\"skin\": \"Classic\", \"page\": 1.0, \"position\": 4.0}"));

        Log.d(TAG, "homeModuleSet: " + positionMap.toString());
        return positionMap;
    }

    static public int extraForegroundHomeModuleCount(HashMap<String, JSONObject> modulePositionSet) {
        int count = 0;
        for (Map.Entry<String, JSONObject> modulePosition: modulePositionSet.entrySet()) {
            try {
                if ((float) modulePosition.getValue().getInt("page") == 1.0f) {
                    count++;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Log.d(TAG, "home 1 module count: " + count);
        return count;
    }

    static public int extraBackgroundHomeModuleCount(HashMap<String, JSONObject> modulePositionSet) {
        int count = 0;
        for (Map.Entry<String, JSONObject> modulePosition: modulePositionSet.entrySet()) {
            try {
                if ((float) modulePosition.getValue().getInt("page") == 2.0f) {
                    count++;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Log.d(TAG, "home 2 module count: " + count);
        return count;
    }

    static public HashMap<String, JSONObject> getModulePositionInfo() throws JSONException {
        HashMap<String, JSONObject> positionMap = new HashMap<String, JSONObject>();
//        //        positionMap.put("AdsModule", new JSONObject(""));
////        positionMap.put("UserAlarmModule", new JSONObject(""));
////        positionMap.put("CheckInOutModule", new JSONObject("{\"skin\": \"Classic\", \"page\": 1.0, \"position\": 6.0}"));
//        positionMap.put("IoTModule", new JSONObject("{\"position\" : [0, 0, 4, 2]}"));
//        positionMap.put("MusicServiceModule", new JSONObject("{\"position\" : [0, 2, 1, 2]}"));
////        positionMap.put("NotificationModule", new JSONObject(""));
//        positionMap.put("RoomStatusModule_mur", new JSONObject("{\"position\" : [1, 2, 1, 2]}"));
//        positionMap.put("RoomStatusModule_dnd", new JSONObject("{\"position\" : [2, 2, 1, 2]}"));
//        positionMap.put("SurroundingModule_sights", new JSONObject("{\"position\" : [3, 2, 1, 2]}"));
//        positionMap.put("SurroundingModule_transport", new JSONObject("{\"position\" : [4, 0, 2, 2]}"));
//        positionMap.put("SurroundingModule_cuisine", new JSONObject("{\"position\" : [6, 0, 2, 2] }"));
//        positionMap.put("SurroundingModule_living", new JSONObject("{\"position\" : [4, 2, 4, 2]}"));
////        positionMap.put("TaxiModule", new JSONObject("{\"skin\": \"Classic\", \"page\": 2.0, \"position\": 5.0}"));
////        positionMap.put("CheckInOutModule", new JSONObject("{\"skin\": \"Classic\", \"page\": 2.0, \"position\": 6.0}"));
////        positionMap.put("VoipModule", new JSONObject("{\"skin\": \"Classic\", \"page\": 1.0, \"position\": 4.0}"));


        positionMap.put("IoTModule", new JSONObject("{\"position\" : [0, 0, 6, 2]}"));
        positionMap.put("MusicServiceModule", new JSONObject("{\"position\" : [0, 2, 2, 2]}"));
        positionMap.put("RoomStatusModule_mur", new JSONObject("{\"position\" : [2, 2, 3, 1]}"));
        positionMap.put("RoomStatusModule_dnd", new JSONObject("{\"position\" : [2, 3, 2, 1]}"));
        positionMap.put("SurroundingModule_sights", new JSONObject("{\"position\" : [4, 3, 1, 1]}"));
        positionMap.put("SurroundingModule_transport", new JSONObject("{\"position\" : [6, 0, 2, 3]}"));
        positionMap.put("SurroundingModule_cuisine", new JSONObject("{\"position\" : [5, 2, 1, 2] }"));
        positionMap.put("SurroundingModule_living", new JSONObject("{\"position\" : [6, 3, 2, 1]}"));

        Log.d(TAG, "homeModuleSet: " + positionMap.toString());
        return positionMap;
    }
}
