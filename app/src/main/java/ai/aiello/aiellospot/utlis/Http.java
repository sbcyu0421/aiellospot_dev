package ai.aiello.aiellospot.utlis;

import androidx.annotation.Nullable;

import com.litesuits.android.log.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


import ai.aiello.aiellospot.core.log.SpotDeviceLog;

public class Http {


    private static final String TAG = Http.class.getSimpleName();
    private static final String SUB_KEY = "Ocp-Apim-Subscription-Key";
    public static final String PUB1 = "c342b7abc99f46ae91206ff99345966a";

    public static HttpRes get(String apiUrl, int timeout, String subKey) {
        URL url = null;
        String response = "";
        int responseCode = 0;
        Exception exception = null;
        HttpURLConnection connection = null;
        Log.d(TAG, String.format("url = %s, method = %s ", apiUrl, "GET"));
        try {
            url = new URL(apiUrl);
            connection = (HttpURLConnection) url.openConnection();

            //header
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            connection.setRequestProperty(SUB_KEY, subKey);

            //timeout
            connection.setConnectTimeout(timeout);
            connection.setReadTimeout(timeout);

            //connect
            connection.connect();

            //response
            responseCode = connection.getResponseCode();
            Log.d(TAG, responseCode);
            if (responseCode / 100 == 2) {

                InputStream inputStream = connection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
                response = result.toString();
                Log.d(TAG, response);
                reader.close();
                inputStream.close();
            } else {
                throw new Exception(responseCode + "");
            }

        } catch (Exception e) {
            response = "";
            exception = e;
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e(TAG, "request error: " + e.getMessage());
        } finally {
            if (connection != null) {
                try {
                    connection.getInputStream().close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                connection.disconnect();
            }

        }
        return makeException(responseCode, response, exception);
    }

    private static HttpRes makeException(int responseCode, String response, Exception exception) {
        return new HttpRes(responseCode, response, exception);
    }

    public static class HttpRes {
        public int responseCode;
        public String response;
        public Exception exception;

        public HttpRes(int responseCode, String response, Exception exception) {
            this.responseCode = responseCode;
            this.response = response;
            this.exception = exception;
        }
    }

    public static HttpRes post(String apiUrl, @Nullable JSONObject jsonBody, int timeout, String subKey) {
        URL url = null;
        String response = "";
        int responseCode = 0;
        Exception exception = null;
        HttpURLConnection connection = null;
        Log.d(TAG, String.format("url = %s, body = %s, method = %s ", apiUrl, jsonBody, "POST"));
        try {
            url = new URL(apiUrl);
            connection = (HttpURLConnection) url.openConnection();

            //header
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            connection.setRequestProperty(SUB_KEY, subKey);
            connection.setDoOutput(true);
            connection.setDoInput(true);

            //timeout
            connection.setConnectTimeout(timeout);
            connection.setReadTimeout(timeout);

            //body
            if (jsonBody != null) {
                DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());
                byte[] t = jsonBody.toString().getBytes("utf-8");
                dataOutputStream.write(t);
                dataOutputStream.flush();
                dataOutputStream.close();
            }

            //connect
            connection.connect();

            //response
            responseCode = connection.getResponseCode();
            Log.d(TAG, responseCode);
            if (responseCode / 100 == 2) {

                InputStream inputStream = connection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
                response = result.toString();
                Log.d(TAG, response);
                reader.close();
                inputStream.close();
            } else {
                throw new Exception(responseCode + "");
            }

        } catch (Exception e) {
            response = "";
            exception = e;
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e(TAG, "request error: " + e.getMessage());
        } finally {
            if (connection != null) {
                try {
                    connection.getInputStream().close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                connection.disconnect();
            }

        }
        return makeException(responseCode, response, exception);
    }

    public static HttpRes put(String apiUrl, @Nullable JSONObject jsonBody, int timeout, String subKey) {
        URL url = null;
        String response = "";
        int responseCode = 0;
        Exception exception = null;
        HttpURLConnection connection = null;
        Log.d(TAG, String.format("url = %s, body = %s, method = %s ", apiUrl, jsonBody, "PUT"));
        try {
            url = new URL(apiUrl);
            connection = (HttpURLConnection) url.openConnection();

            //header
            connection.setRequestMethod("PUT");
            connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            connection.setRequestProperty(SUB_KEY, subKey);
            connection.setDoOutput(true);
            connection.setDoInput(true);

            //timeout
            connection.setConnectTimeout(timeout);
            connection.setReadTimeout(timeout);

            //body
            if (jsonBody != null) {
                DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());
                byte[] t = jsonBody.toString().getBytes("utf-8");
                dataOutputStream.write(t);
                dataOutputStream.flush();
                dataOutputStream.close();
            }

            //connect
            connection.connect();

            //response
            responseCode = connection.getResponseCode();
            Log.d(TAG, responseCode);
            if (responseCode / 100 == 2) {

                InputStream inputStream = connection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
                response = result.toString();
                Log.d(TAG, response);
                reader.close();
                inputStream.close();
            } else {
                throw new Exception(responseCode + "");
            }

        } catch (Exception e) {
            response = "";
            exception = e;
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e(TAG, "request error: " + e.getMessage());
        } finally {
            if (connection != null) {
                try {
                    connection.getInputStream().close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                connection.disconnect();
            }

        }
        return makeException(responseCode, response, exception);
    }

    public static HttpRes getByQuery(String apiUrl, int timeout, String subKey) {
        URL url = null;
        String response = "";
        int responseCode = 0;
        Exception exception = null;
        HttpURLConnection connection = null;
        Log.d(TAG, String.format("url = %s, method = %s ", apiUrl, "GET"));
        try {
            url = new URL(apiUrl);
            connection = (HttpURLConnection) url.openConnection();

            //header
            connection.setRequestMethod("GET");
//            connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            connection.setRequestProperty(SUB_KEY, subKey);

            //timeout
            connection.setConnectTimeout(timeout);
            connection.setReadTimeout(timeout);

            //connect
            connection.connect();

            //response
            responseCode = connection.getResponseCode();
            Log.d(TAG, responseCode);
            if (responseCode / 100 == 2) {

                InputStream inputStream = connection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
                response = result.toString();
                Log.d(TAG, response);
                reader.close();
                inputStream.close();
            } else {
                throw new Exception(responseCode + "");
            }

        } catch (Exception e) {
            response = "";
            exception = e;
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e(TAG, "request error: " + e.getMessage());
        } finally {
            if (connection != null) {
                try {
                    connection.getInputStream().close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                connection.disconnect();
            }

        }
        return makeException(responseCode, response, exception);
    }

}
