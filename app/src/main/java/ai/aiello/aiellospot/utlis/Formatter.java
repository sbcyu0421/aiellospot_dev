package ai.aiello.aiellospot.utlis;

import android.text.format.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Formatter {

    public static String binaryFormat(int i) {
        String t = "";
        if (i < 10) {
            t = "0" + i;
        } else {
            t = String.valueOf(i);
        }
        return t;
    }

    public static String formatToISO8601(Date date){
        return ISO8601DATEFORMAT.format(date);
    }

    public static String formatElsDate(Date date){
        return DateFormatForEls.format(date);
    }

    public static SimpleDateFormat ISO8601DATEFORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    public static SimpleDateFormat DateFormatForEls = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

    public static String getSpeechRelativeDay(Date date) {
        Date today = new Date();
        if (date.getMonth() == today.getMonth() + 1 || date.getDate() >= today.getDate() + 8) {
            SimpleDateFormat speechDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            return speechDateFormat.format(date) + " ";
        } else {
            return DateUtils.getRelativeTimeSpanString(date.getTime(), System.currentTimeMillis(), DateUtils.DAY_IN_MILLIS, DateUtils.FORMAT_SHOW_TIME).toString();
        }
    }

    public static String getSpeechTimeDay(Date date) {
        SimpleDateFormat speechDateFormat = new SimpleDateFormat("hh:mmaa", Locale.ENGLISH);
        return speechDateFormat.format(date);
    }

    public static String getSpeechDayAndTimeFormat(Date date) {
        SimpleDateFormat speechDateFormat = new SimpleDateFormat("hh:mmaa", Locale.ENGLISH);
        String res = "";
        Date today = new Date();
        if (date.getMonth() == today.getMonth() + 1 || date.getDate() >= today.getDate() + 8) {
            SimpleDateFormat speechDateFormatDay = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            res += speechDateFormatDay.format(date)+ " ";
        } else {
            res += DateUtils.getRelativeTimeSpanString(date.getTime(), System.currentTimeMillis(), DateUtils.DAY_IN_MILLIS, DateUtils.FORMAT_SHOW_TIME).toString();
        }
        res += " " + speechDateFormat.format(date);
        return res;
    }

}
