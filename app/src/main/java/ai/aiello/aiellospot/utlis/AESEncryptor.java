package ai.aiello.aiellospot.utlis;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

public class AESEncryptor {
    private static final String ALGORITHM = "AES/CBC/PKCS5Padding";
    private static final String PK = "Aiello";
    private static final String IV = "ai";

    public static String encrypt(String value) throws BadPaddingException, IllegalBlockSizeException,
            InvalidAlgorithmParameterException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException {
        return encrypt(value, PK, IV);
    }

    public static String encrypt(String value, String privateKey) throws BadPaddingException,
            IllegalBlockSizeException, InvalidAlgorithmParameterException, InvalidKeyException,
            NoSuchAlgorithmException, NoSuchPaddingException {
        return encrypt(value, privateKey, null);
    }

    public static String encrypt(String value, String privateKey, String iv) throws BadPaddingException,
            IllegalBlockSizeException, InvalidAlgorithmParameterException, InvalidKeyException,
            NoSuchAlgorithmException, NoSuchPaddingException {
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        md5.update(privateKey.getBytes(StandardCharsets.UTF_8));
        SecretKeySpec secretKey = new SecretKeySpec(md5.digest(), "AES");
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        if (Objects.isNull(iv) || iv.isEmpty()) {
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        } else {
            md5.update(iv.getBytes(StandardCharsets.UTF_8));
            IvParameterSpec ivParameterSpec = new IvParameterSpec(md5.digest());
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivParameterSpec);
        }
        byte[] bytes_cookie = cipher.doFinal(value.getBytes(StandardCharsets.UTF_8));
        char[] hexArray = "0123456789ABCDEF".toCharArray();
        char[] hex_cookie = new char[bytes_cookie.length * 2];
        for (int i = 0; i < bytes_cookie.length; i++) {
            int _value = bytes_cookie[i] & 0xFF;
            hex_cookie[i*2] = hexArray[_value >>> 4];
            hex_cookie[i*2+1] = hexArray[_value & 0x0F];
        }
        return new String(hex_cookie);
    }

    public static String decrypt(String value) throws BadPaddingException, IllegalBlockSizeException,
            InvalidAlgorithmParameterException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
            NumberFormatException {
        return decrypt(value, PK, IV);
    }

    public static String decrypt(String value, String privateKey) throws BadPaddingException,
            IllegalBlockSizeException, InvalidAlgorithmParameterException, InvalidKeyException,
            NoSuchAlgorithmException, NoSuchPaddingException, NumberFormatException {
        return decrypt(value, PK, null);
    }

    public static String decrypt(String value, String privateKey, String iv) throws BadPaddingException,
            IllegalBlockSizeException, InvalidAlgorithmParameterException, InvalidKeyException,
            NoSuchAlgorithmException, NoSuchPaddingException, NumberFormatException {
        if ((value.length() % 2) != 0)
            throw new NumberFormatException("It is not a valid value. [ " + value + " ]");
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        md5.update(privateKey.getBytes(StandardCharsets.UTF_8));
        SecretKeySpec secretKey = new SecretKeySpec(md5.digest(), "AES");
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        if (Objects.isNull(iv) || iv.isEmpty()) {
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
        } else {
            md5.update(iv.getBytes(StandardCharsets.UTF_8));
            IvParameterSpec ivParameterSpec = new IvParameterSpec(md5.digest());
            cipher.init(Cipher.DECRYPT_MODE, secretKey, ivParameterSpec);
        }
        byte[] hex_value = new byte[value.length() / 2];
        for (int i = 0; i < value.length(); i+=2) {
            hex_value[(int)(i/2)] = (byte)((Character.digit(value.charAt(i), 16) << 4) + Character.digit(value.charAt(i+1), 16));
        }
        return new String(cipher.doFinal(hex_value), StandardCharsets.UTF_8);
    }
}
