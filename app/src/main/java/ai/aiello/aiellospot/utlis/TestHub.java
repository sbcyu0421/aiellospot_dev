package ai.aiello.aiellospot.utlis;

import com.litesuits.android.log.Log;
import com.microsoft.signalr.HubConnection;
import com.microsoft.signalr.HubConnectionBuilder;
import com.microsoft.signalr.HubConnectionState;
import com.microsoft.signalr.OnClosedCallback;


import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import io.reactivex.Completable;
import org.json.JSONObject;

import static ai.aiello.aiellospot.modules.iot.IoTConn.TAG;

public class TestHub {
    private static HubConnection connection;
    public static boolean debug_mode=false;
    private static Completable c_hub;
    public static void Connect() {
      try {
        connection = HubConnectionBuilder.create("http://65.52.165.205:3326/TestHub").build();
        connection.setServerTimeout(30000);
        connection.setKeepAliveInterval(5000);
        connection.onClosed(new OnClosedCallback() {
          @Override
          public void invoke(Exception exception) {
            try {
              Log.e(TAG, "@@@TestHub onClosed, " + exception.toString());
              SpotDeviceLog.exception(TAG,"",exception.getMessage());
              reStartTestHub(connection, "http://65.52.165.205:3326/TestHub");
            } catch (Exception e) {
              Log.e(TAG, "@@@TestHub onClosed invoke fail = " + e.toString());
              SpotDeviceLog.exception(TAG,"",e.getMessage());
              Log.e(TAG, "force restart from hell, avoid crash popup");
              System.exit(0);
            }
          }
        });
        c_hub = connection.start().onErrorComplete(throwable -> {
          throw new Exception("cancelOutstandingInvocations");
        });
        c_hub.blockingAwait();
      } catch (Exception ex) {
        Log.e(TAG, "@@@TestHub reconnect exception = " + ex.toString());
        SpotDeviceLog.exception(TAG,"",ex.getMessage());

      }
    }
      public static void reStartTestHub(HubConnection hubConnection, String iot_hub_surl) {
        Log.d(TAG, "reStartAielloHub");
        try {
          Thread.sleep(5000);
        } catch (InterruptedException e) {
          SpotDeviceLog.exception(TAG,"",e.getMessage());
          e.printStackTrace();
        }
        Connect();
      }
      public static boolean ConnectionState(){
          if(connection == null||connection.getConnectionState().equals(HubConnectionState.DISCONNECTED))
            return false;
          else if(connection.getConnectionState().equals(HubConnectionState.CONNECTED))
            return true;
          else
            return false;
      }
      public static void Disable(){
         try {
              connection.stop();
              connection = null;
            }catch (Exception ex){}

          }
      public static void SendWakeUpEvent(){
        if(connection != null&&connection.getConnectionState().equals(HubConnectionState.CONNECTED))
          connection.send("SendWakeup");
      }
      public static void SendChatbotEvent(JSONObject obj){
    if(connection != null&&connection.getConnectionState().equals(HubConnectionState.CONNECTED))
      connection.send("SendResult",obj);
    }
}
