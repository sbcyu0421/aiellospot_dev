package ai.aiello.aiellospot.utlis;

import android.content.Context;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.core.config.SystemConfig;
import ai.aiello.aiellospot.core.info.DeviceInfo;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;

import com.litesuits.android.log.Log;
import com.litesuits.common.io.FileUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class AielloFileUtils {

    private static final String TAG = AielloFileUtils.class.getSimpleName();
    private static final String filePath = "/data/logs/";
    private static UploadFinishListener uploadFinishListener = null;

    public interface UploadFinishListener {
        void onSuccess();

        void onFail();
    }

    public static String readFileFromAssets(String inFile) {
        String tContents = "";
        try {
            InputStream stream = ChatApplication.context.getAssets().open(inFile);
            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();
            tContents = new String(buffer);
        } catch (IOException e) {
            // Handle exceptions here
        }
        return tContents;
    }

    public static void startUploadFile(String mac, UploadFinishListener uploadFinishListener) {

        AielloFileUtils.uploadFinishListener = uploadFinishListener;

        String mac2 = mac.replace(":", "");

        String[] fileName = new String[]{"android", "kernel", "process" };

        File myDirectory = new File(filePath);
        File[] directories = myDirectory.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isDirectory();
            }
        });


        for (File directory : directories) {
            Log.d(TAG, " directory = " + directory.getName() + " , filePath = " + directory.getAbsolutePath()); // '/data/logs/19-08-27-10-05-07'

            String abPath = directory.getAbsolutePath() + "/";

            for (String s1 : fileName) {
                File file2 = FileUtils.getFile(abPath + s1);
                Log.d(TAG, "restore = clientLog");
                boolean upload_status = uploadFile(file2, directory.getName() + "_" + s1);

                if (!upload_status) {
                    Log.e(TAG, "uploadFile fail");
                    if (uploadFinishListener != null) {
                        uploadFinishListener.onFail();
                    }
                } else {
                    Log.e(TAG, "uploadFile success");
                    if (uploadFinishListener != null) {
                        uploadFinishListener.onSuccess();
                    }
                }
                uploadFinishListener = null;

            }

        }


    }


    public static boolean uploadFile(File file, String fileName) {
        FTPClient con = null;
        boolean result = false;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm");
        boolean isDeviceCreated;
        try {
            con = new FTPClient();
            String[] urlt = SystemConfig.LogFtpBean.url.split(":");
            con.connect(urlt[0], Integer.valueOf(urlt[1]));

            if (con.login(SystemConfig.LogFtpBean.username, SystemConfig.LogFtpBean.password)) {
                con.enterLocalPassiveMode();
                con.setFileType(FTP.BINARY_FILE_TYPE);
                FileInputStream in = new FileInputStream(file);
                Date today = Calendar.getInstance().getTime();
                String folder = "log_" + DeviceInfo.MAC.replace(":", "");
                boolean isDeviceExisted = con.changeWorkingDirectory(folder);

                if (!isDeviceExisted) {
                    isDeviceCreated = con.makeDirectory(folder);
                    con.changeWorkingDirectory(folder);
                }
                boolean isDateExisted = con.changeWorkingDirectory(formatter.format(today));
                if (!isDateExisted) {
                    con.makeDirectory(formatter.format(today));
                    con.changeWorkingDirectory(formatter.format(today));
                }
                result = con.storeFile(fileName, in);
                in.close();
            }
            con.logout();
            con.disconnect();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
          SpotDeviceLog.exception(TAG,"",e.getMessage());
        }
        return result;
    }



    public static File getCacheDirectory(Context context) {
        File appCacheDir = context.getCacheDir();
        if (appCacheDir == null) {
            Log.w("StorageUtils", "Can't define system cache directory! The app should be re-installed.");
        }
        return appCacheDir;
    }

    public static boolean deleteDirContent(File dir) {
        try {
            android.util.Log.d(TAG, "deleteDirContent: file name = " + dir.getName() + " , is Directory = " + dir.isDirectory());
            if (dir != null && dir.isDirectory()) {
                String[] children = dir.list();
                for (int i = 0; i < children.length; i++) {
                    Log.d(TAG, children[i]);
                    try {
                        deleteDirContent(new File(dir, children[i]));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            boolean success = dir.isDirectory() ? true : dir.delete();
            android.util.Log.d(TAG, "deleteDirContent: success = " + success);
            return success;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
