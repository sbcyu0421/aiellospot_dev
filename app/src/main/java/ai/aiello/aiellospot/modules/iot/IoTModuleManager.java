package ai.aiello.aiellospot.modules.iot;

import com.litesuits.android.log.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.core.config.APIConfig;
import ai.aiello.aiellospot.core.config.Configer;
import ai.aiello.aiellospot.core.device.DeviceControl;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.events.server.ServerEvent;
import ai.aiello.aiellospot.events.system.SystemEvent;
import ai.aiello.aiellospot.events.module.IoTStateChange;
import ai.aiello.aiellospot.modules.AbstractModuleManager;
import io.reactivex.annotations.Nullable;

public class IoTModuleManager extends AbstractModuleManager implements IoTConnCallback{
    public static IoTConn _srv;
    public static String TAG = IoTModuleManager.class.getSimpleName();

    private static IoTModuleManager instance = null;
    public static int devicesAmount;
    public static JSONArray devices_array;
    public static APIConfig.WEBAPI configFileApi;
    public static HashMap<String, IotDeviceInfo> iotDeviceInfo = new HashMap<>();

    public enum LayoutName { // matches layout name from config data
        IoT,
    }

    public static IoTModuleManager getInstance() {
        if (instance == null) {
            instance = new IoTModuleManager();
        }
        return instance;
    }

    public IoTModuleManager() {
        android.util.Log.d(TAG, "initialized");
    }

    @Override
    public void create() {
        // setup data
        try {
            setupIoTDeviceInUI();
            configFileApi = APIConfig.WEBAPI.build(
                    "iot_config",
                    Configer.moduleConfigData.getJSONObject("iot").getJSONObject("service_data").getJSONObject("config_file"),
                    Configer.useProductAPI);
            _srv = new IoTConn(ChatApplication.context, this);
            _srv.bindService();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupIoTDeviceInUI() { 
        try {
            JSONObject jsonObject = Configer.moduleConfigData;
            devices_array = jsonObject.getJSONObject("iot").getJSONObject("service_data").getJSONObject("devices").getJSONArray(DeviceInfo.roomType);
            devicesAmount = devices_array.length();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onHotReload() {
        // setup data
        try {
            setupIoTDeviceInUI();
            configFileApi = APIConfig.WEBAPI.build(
                    "iot_config",
                    Configer.moduleConfigData.getJSONObject("iot").getJSONObject("service_data").getJSONObject("config_file"),
                    Configer.useProductAPI);
            // rebind while onUnbind
            _srv.shutdownService();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface IotDoneListener {
        void onIOTDone(int msg);
    }

    public void push_iot_intent_to_remoteService(String json, String intentName, @Nullable IotDoneListener listener) {
        if (_srv.getConnectionState()) {
            _srv.deliveryIntent_IOT(json, intentName, listener);
        } else {
            Log.e(TAG, "_srv is not bound");
            SpotDeviceLog.error(TAG, "", "_srv is not bound");
        }

        try {
            JSONObject t = new JSONObject(json);
            if (t.getString("type").contains("good_night_mode")) {
                DeviceControl.backLightOff();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void deliveryRCUAction(String actions) {
        if (_srv.getConnectionState()) {
            _srv.deliveryRCUAction(actions);
        } else {
            Log.e(TAG, "_srv is not bound");
            SpotDeviceLog.error(TAG, "", "_srv is not bound");
        }
    }


    public void push_iot_intent_to_remoteService(JSONArray loop_code_array, String intentName) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("intent", intentName);
            jsonObject.put("loop_codes", loop_code_array);
            push_iot_intent_to_remoteService(jsonObject.toString(), intentName, null);
        } catch (Exception e) {
            e.getStackTrace();
            SpotDeviceLog.error(TAG, "", "push_iot_loop_codes_error=" + loop_code_array);
        }
    }

    public void deliver_UI_IOT(String iot_ui_control) {
        try {
            if (_srv.getConnectionState()) {
                _srv.delivery_IOTUIControl(iot_ui_control);
                android.util.Log.d(TAG, "deliver_UI_IOT: " + iot_ui_control);
            }
        } catch (Exception e) {
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e(TAG, "iot_ui_control error = " + e.toString());
        }
    }


    public void shutdownService() {
        _srv.shutdownService();
    }

    public void checkin() {
        _srv.checkin();
    }

    public void checkout() {
        _srv.checkout();
    }

    public static void testToT(boolean enable) {
        if (enable) {
            _srv.startIoTTest();
        } else {
            _srv.stopIoTTest();
        }
    }

    public static void updateIotDeviceList(JSONObject jsonDeviceInfo) throws JSONException {
        String name = jsonDeviceInfo.getString("name");;
        Boolean online = jsonDeviceInfo.getBoolean("online");
        String uid = jsonDeviceInfo.getJSONObject("other_info").getJSONObject("profile").getString("uid");
        iotDeviceInfo.put(uid, new IotDeviceInfo(uid, name, online));
    }

    public static class IotDeviceInfo {

        String uid;
        String name;
        Boolean online;

        public IotDeviceInfo(String uid, String name, Boolean online) {
            this.uid = uid;
            this.name = name;
            this.online = online;
        }

        public String getUid() {
            return uid;
        }

        public String getName() {
            return name;
        }

        public Boolean getOnline() {
            return online;
        }
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onMessageEvent(ServerEvent event) {
        try {
            switch (event.getType()) {
                case IOTCONFIG_RELOAD:
                    shutdownService();
                    break;
                case IOT_EVENT:
                    push_iot_intent_to_remoteService(new JSONArray(event.getData()), "Dialogflow.IoT.Custom");
                    break;
                case CHECK_IN:
                    checkin();
                    break;
                case CHECK_OUT:
                case APPREBOOT:
                case DEVICEREBOOT:
                    checkout();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onMessageEvent(SystemEvent event) {
        super.onMessageEvent(event);
        try {
            switch (event.getType()) {
                case ALL_CHANNEL_STOP:
                    deliver_UI_IOT("stop");
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAcDataUpdate(String data) {
        EventBus.getDefault().post(new IoTStateChange(IoTStateChange.Target.ON_AC_PANEL_UPDATE, data));
    }

}



