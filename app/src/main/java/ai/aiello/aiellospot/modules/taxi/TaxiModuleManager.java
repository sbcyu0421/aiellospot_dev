package ai.aiello.aiellospot.modules.taxi;

import android.util.Log;

import ai.aiello.aiellospot.modules.AbstractModuleManager;

public class TaxiModuleManager extends AbstractModuleManager {

    private static String TAG = TaxiModuleManager.class.getSimpleName();
    private static TaxiModuleManager instance;
    public static TaxiModuleManager getInstance() {
        if (instance == null) {
            instance = new TaxiModuleManager();
        }
        return instance;
    }

    private TaxiModuleManager() {
        Log.d(TAG, "initialized");
    }

    @Override
    public void create() {
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onHotReload() {

    }


}
