package ai.aiello.aiellospot.modules.voip;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.AssetFileDescriptor;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Message;
import android.os.Process;

import com.litesuits.android.log.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.core.config.Configer;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.soundai.SoundAIManager;
import ai.aiello.aiellospot.events.system.SystemEvent;
import ai.aiello.aiellospot.events.module.VoIPStateChange;
import ai.aiello.aiellospot.modules.AbstractModuleManager;

public class VoipModuleManager extends AbstractModuleManager {

    public final static String TAG = VoipModuleManager.class.getSimpleName();
    private static VoipService mVoipService = null;
    private static AudioTrack mAudioTrack = null;
    private static MediaPlayer mMediaPlayer = null;
    private static Thread audioTrackThread = null;
    private static byte[] CallbackBuf;
    private static byte[] SendBuf;
    private static String roomName;

    public static final String MAKE_CALL_RESPONSE = "MAKE_CALL_RESPONSE";
    public static final String CANCEL_MAKE_CALL = "CANCEL_MAKE_CALL";
    public static final String TX_INCOMING_CALL = "TX_INCOMING_CALL";
    public static final String TX_INCOMING_CALL_CANCEL = "TX_INCOMING_CALL_CANCEL";
    public static final String RX_ACCEPT_CALL = "RX_ACCEPT_CALL";
    public static final String RX_DECLINE_CALL = "RX_DECLINE_CALL";
    public static final String TX_DISABLE_CALL = "TX_DISABLE_CALL";
    public static final String CALL_END = "CALL_END";

    public static String targetNum = "";

    private static PipedInputStream pipedInputStream;
    private static PipedOutputStream pipedOutputStream;

    private static boolean callMuted = false;
    private static boolean muteTracker = false;

    private static Handler workderHandler;
    private static boolean use_ping_pong;
    private static boolean pong = false;
    private static int pingId;

    private static Timer onCallTimer;
    private static int onCallCount;

    public IVoipState currentState = new IdleState();
    public ConnStatus connStatus = ConnStatus.init;

    private static VoipModuleManager instance = null;

    public static String voipUrl;

    public static VoipModuleManager getInstance() {
        if (instance == null) {
            instance = new VoipModuleManager();
        }
        return instance;
    }

    private VoipModuleManager() {
        android.util.Log.d(TAG, "initialized");
    }

    @Override
    public void create() {

    }


    private void startVoipService(){
        // setup data
        try {
            voipUrl = Configer.moduleConfigData.getJSONObject("voip").getJSONObject("service_data").getString("url");
            Phonebook.FRONT_DESK_NUMBER = Configer.moduleConfigData.getJSONObject("voip").getJSONObject("service_data").getString("front_desk");
            Phonebook.RESTAURANT_NUMBER = Configer.moduleConfigData.getJSONObject("voip").getJSONObject("service_data").getString("restaurant");
            Phonebook.SOS_NUMBER = Configer.moduleConfigData.getJSONObject("voip").getJSONObject("service_data").getString("sos");
        } catch (Exception e) {
            e.printStackTrace();
        }
        android.util.Log.d(TAG, String.format("voipUrl=%s, front_desk=%s, restaurant=%s, sos=%s", voipUrl, Phonebook.FRONT_DESK_NUMBER, Phonebook.RESTAURANT_NUMBER, Phonebook.SOS_NUMBER));
        roomName = DeviceInfo.roomName;
        if (workderHandler != null) {
            workderHandler.sendEmptyMessage(1);
            return;
        }
        Intent serviceIntent = new Intent(ChatApplication.context, VoipService.class);
        ChatApplication.context.bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE);
        HandlerThread handlerThread = new HandlerThread("voip handler thread");
        handlerThread.start();
        workderHandler = new Handler(handlerThread.getLooper(), new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                try {
                    mVoipService.disConnect();
                    connStatus = ConnStatus.offline;
                    EventBus.getDefault().post(new VoIPStateChange(VoIPStateChange.VOIP_STATUS, ""));
                    Log.d(TAG, "reConnectServer, id = " + Thread.currentThread().getId());
                    connectServer(2);  //msg.what=1;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }
        });
    }

    @Override
    public void onStart() {
        startVoipService();
    }

    @Override
    public void onHotReload() {
        startVoipService();
    }

    public enum ConnStatus {
        init, online, offline
    }

    public enum Event {
        offline, makeCallRes, accept, decline, inComing, endCall, disable
    }

    public enum StateName {
        OnCall, OnInComingCallWaiting, OnMakeCallWaiting, Idle, Disable
    }

    public enum ResponseStatus {
        SUCCESS,
        FAIL,
        SELF,
        BUSY,
        OFFLINE
    }

    public interface IVoipState {
        StateName getName();

        void handleEvent(Event event, String info);
    }


    class DisableState implements IVoipState {

        DisableState() {
            Log.e(TAG, "change state to : " + getName());
        }

        @Override
        public StateName getName() {
            return StateName.Disable;
        }

        @Override
        public void handleEvent(Event event, String info) {
            Log.d(TAG, getName() + ", handleEvent = " + event);
            switch (event) {
                case endCall:
                case offline:
                case decline:
                    currentState = new IdleState(false);
                    EventBus.getDefault().post(new VoIPStateChange(VoIPStateChange.CALL_END, info));
                    break;
            }
        }
    }

    class OnCallState implements IVoipState {

        OnCallState() {
            EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.TEMP_MEDIA_OCCUPY));
            Log.e(TAG, "change state to : " + getName());
        }

        @Override
        public StateName getName() {
            return StateName.OnCall;
        }

        @Override
        public void handleEvent(Event event, String info) {
            Log.d(TAG, getName() + ", handleEvent = " + event);
            switch (event) {
                case endCall:
                case offline:
                    if (info.equals(TX)) {
                        mVoipService.Tx_Call_End(roomName);
                    }
                    stopVoip();
                    currentState = new IdleState();
                    EventBus.getDefault().post(new VoIPStateChange(VoIPStateChange.CALL_END, info));
                    break;
            }
        }
    }

    public static String TX = "TX";

    class OnInComingCallWaitingState implements IVoipState {

        OnInComingCallWaitingState() {
            EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.TEMP_MEDIA_OCCUPY));
            Log.e(TAG, "change state to : " + getName());
        }

        @Override
        public StateName getName() {
            return StateName.OnInComingCallWaiting;
        }

        @Override
        public void handleEvent(Event event, String info) {
            Log.e(TAG, getName() + ", handleEvent = " + event);
            switch (event) {
                case accept:
                    //call separately (for multi-device in the same room)
                    try {
                        char c1 = DeviceInfo.MAC.charAt(12);
                        char c2 = DeviceInfo.MAC.charAt(13);
                        char c3 = DeviceInfo.MAC.charAt(15);
                        char c4 = DeviceInfo.MAC.charAt(16);
                        int sleep = ((c1 + c2) * (c3 + c4)) % 1000;
                        Log.d(TAG, "mac = " + DeviceInfo.MAC + ", sleep = " + sleep);
                        Thread.sleep(sleep);
                    } catch (Exception e) {
                        Log.e(TAG, e.toString());
                    }
                    mVoipService.Rx_Accept_Call(roomName);
                    startVoip();
                    stopRingTone();
                    currentState = new OnCallState();
                    EventBus.getDefault().post(new VoIPStateChange(VoIPStateChange.ACCEPT_CALL, ""));
                    break;
                case decline:
                    stopRingTone();
                    if (info.equals(TX)) {
                        mVoipService.Rx_Decline_Call(roomName);
                        EventBus.getDefault().post(new VoIPStateChange(VoIPStateChange.DECLINE_CALL, roomName));
                    } else {
                        EventBus.getDefault().post(new VoIPStateChange(VoIPStateChange.INCOMING_CALL_CANCELED, info));
                    }
                    currentState = new IdleState();
                    break;
                case disable:
                    stopRingTone();
                    currentState = new DisableState();
                    EventBus.getDefault().post(new VoIPStateChange(VoIPStateChange.DISABLE, info));
                    break;
                case offline:
                    stopRingTone();
                    currentState = new IdleState();
                    break;
            }
        }
    }

    class OnMakeCallWaitingState implements IVoipState {

        OnMakeCallWaitingState() {
            EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.TEMP_MEDIA_OCCUPY));
            Log.e(TAG, "change state to : " + getName());
        }

        @Override
        public StateName getName() {
            return StateName.OnMakeCallWaiting;
        }

        @Override
        public void handleEvent(Event event, String info) {
            Log.e(TAG, getName() + ", handleEvent = " + event);
            switch (event) {
                case accept:
                    startVoip();
                    stopRingTone();
                    currentState = new OnCallState();
                    EventBus.getDefault().post(new VoIPStateChange(VoIPStateChange.ACCEPT_CALL, info));
                    break;
                case decline:
                    stopRingTone();
                    currentState = new IdleState();
                    if (info.equals(TX)) {
                        EventBus.getDefault().post(new VoIPStateChange(VoIPStateChange.CANCEL_MAKE_CALL, info));
                        mVoipService.Tx_Call_Make_Cancel(roomName);
                    } else {
                        EventBus.getDefault().post(new VoIPStateChange(VoIPStateChange.DECLINE_CALL, info));
                    }
                    break;
                case offline:
                    stopRingTone();
                    currentState = new IdleState();
                    break;
            }
        }
    }

    class IdleState implements IVoipState {

        IdleState() {
            EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.TEMP_MEDIA_RELEASE));
            Log.e(TAG, "change state to : " + getName());
        }

        IdleState(boolean resumeMusic) {
            Log.e(TAG, "change state to : " + getName() + ", dose not resume music");
        }

        @Override
        public StateName getName() {
            return StateName.Idle;
        }

        @Override
        public void handleEvent(Event event, String info) {
            Log.e(TAG, getName() + ", handleEvent = " + event + ", info = " + info);
            switch (event) {
                case inComing:
                    StartRingTone();
                    currentState = new OnInComingCallWaitingState();
                    VoipModuleManager.targetNum = info;
                    EventBus.getDefault().post(new VoIPStateChange(VoIPStateChange.INCOMING_CALL, ""));
                    break;
                case makeCallRes:
                    makeCallRes = info;
                    if (info.equals(ResponseStatus.SUCCESS.name())) {
                        StartWaitingRingTone();
                        startMakeCallTimeout(30);
                        currentState = new OnMakeCallWaitingState();
                        EventBus.getDefault().post(new VoIPStateChange(VoIPStateChange.ON_MAKE_CALL_WAITING, ""));
                    } else {
                        targetNum = "";
                    }
                    EventBus.getDefault().post(new VoIPStateChange(VoIPStateChange.MAKE_CALL_RESPONSE, info));
                    break;
                case endCall:
                    EventBus.getDefault().post(new VoIPStateChange(VoIPStateChange.CALL_END, info));
                    break;
                case disable:
                    currentState = new DisableState();
                    EventBus.getDefault().post(new VoIPStateChange(VoIPStateChange.DISABLE, info));
                    break;
            }
        }
    }

    public String makeCallRes;

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            Log.e(TAG, "onVoip ServiceConnected");
            VoipService.LocalBinder binder = (VoipService.LocalBinder) service;
            mVoipService = binder.getService();
            mVoipService.setCallback(voipServiceCallback);
            connectServer(1);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            Log.e(TAG, "onVoip ServiceDisconnected");
            Intent serviceIntent = new Intent(ChatApplication.context, VoipService.class);
            ChatApplication.context.bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE);
        }
    };


    private VoipService.VoipCallback voipServiceCallback = new VoipService.VoipCallback() {

        @Override
        public void onCallUpdate(String event, String info) {
            switch (event) {
                case VoipModuleManager.MAKE_CALL_RESPONSE:
                    currentState.handleEvent(Event.makeCallRes, info);
                    break;

                case VoipModuleManager.TX_INCOMING_CALL:
                    currentState.handleEvent(Event.inComing, info);
                    break;

                case VoipModuleManager.TX_INCOMING_CALL_CANCEL:
                case VoipModuleManager.RX_DECLINE_CALL:
                    currentState.handleEvent(Event.decline, info);
                    break;

                case VoipModuleManager.TX_DISABLE_CALL:
                    currentState.handleEvent(Event.disable, info);
                    break;

                case VoipModuleManager.RX_ACCEPT_CALL:
                    currentState.handleEvent(Event.accept, info);
                    break;

                case VoipModuleManager.CALL_END:
                    currentState.handleEvent(Event.endCall, info);
                    break;
                default:
                    break;
            }

        }

        @Override
        public void onSessionUpdate(VoipService.SessionStatus status) {
            switch (status) {
                case SESSION_INIT:
                    connStatus = ConnStatus.init;
                    Log.d(TAG, "SESSION_Init, id = " + Thread.currentThread().getId());
                    startPing();
                    break;
                case SESSION_MY_PONG:
                    setPong(true);
                    if (connStatus != ConnStatus.online) {
                        EventBus.getDefault().post(new VoIPStateChange(VoIPStateChange.VOIP_STATUS, ""));
                    }
                    connStatus = ConnStatus.online;
                    break;
                case SESSION_DISCONNECT:
                    Log.d(TAG, "SESSION_DISCONNECT, id = " + Thread.currentThread().getId());
                    break;
                case SESSION_CONNECT:
                    Log.d(TAG, "SESSION_CONNECT, id = " + Thread.currentThread().getId());
                    break;
            }
        }

        @Override
        public void onVoiceDataRecv(byte[] data) {
            trackQueue.offer(data);
        }

        @Override
        public void onMute(boolean setTrackerMute) {
            muteTracker = setTrackerMute;
        }


    };

    private static PingThread pingThread;

    class PingThread extends Thread {
        @Override
        public void run() {
            try {
                while (use_ping_pong) {
                    Thread.sleep(4000);
                    mVoipService.myPing(roomName);
                    setPong(false);
                    Thread.sleep(2000);
                    if (!pong) {
                        stopPing();
                    }
                }
                workderHandler.sendEmptyMessage(1);
                currentState.handleEvent(Event.offline, "voip timeout");
                Log.e(TAG, "ping_pong_timeout");
            } catch (Exception e) {
                Log.e(TAG, e.toString());
                SpotDeviceLog.exception(TAG, "", e.getMessage());
            }
        }
    }

    private void startPing() {
        pingId = 0;
        use_ping_pong = true;
        pingThread = new PingThread();
        pingThread.start();
    }


    private void setPong(boolean getPong) {
        pong = getPong;
        if (getPong) {
            if (pingId % 100 == 0) {
                Log.d(TAG, "voip_pong (" + pingId + "), thread Id = " + Thread.currentThread().getId());
            }
        } else {
            pingId++;
            if (pingId % 100 == 0) {
                Log.d(TAG, "voip_ping (" + pingId + "), thread Id = " + Thread.currentThread().getId());
            }
        }
    }

    private static void connectServer(int i) {
        Log.d(TAG, "connectServer = " + i + ", url = " + voipUrl);
        mVoipService.init(false, voipUrl);
        mVoipService.setRoomID(roomName);
    }


    private static ArrayBlockingQueue<byte[]> trackQueue;

    private static ExecutorService emitService;

    private static final Object mutex = new Object();

    public void startVoip() {
        callMuted = false;
        muteTracker = false;
        onCallCount = 1;  // init 0
        emitService = Executors.newFixedThreadPool(1);
        stopMakeCallTimeout();
        Log.e(TAG, "startVoip");
        trackQueue = new ArrayBlockingQueue<>(500, true);
        audioTrackThread = new Thread(new Runnable() {
            @Override
            public void run() {
                byte[] buffer;
                while (SoundAIManager.onVoip) {
                    try {
                        buffer = trackQueue.take();
                        if (!muteTracker) {
                            getAudioTrack().write(buffer, 0, buffer.length);
                        } else {
                            Log.d(TAG, "OtherSide Mute");
                        }
                    } catch (Exception e) {
                        Log.e(TAG, e.toString());
                        SpotDeviceLog.exception(TAG, "", e.getMessage());
                    }

                }
                Log.d(TAG, "close and release mAudioTrack");
                getAudioTrack().stop();
                getAudioTrack().release();
                mAudioTrack = null;

            }
        });

        try {
            pipedOutputStream = new PipedOutputStream();
            pipedInputStream = new PipedInputStream(pipedOutputStream, 102400);

             /*
			AMR codec spec:
				input -> PCM mono, 16bit, 8k, 20ms frame(320 bytes)
				output -> 32 bytes amr format frame (10% compress)
		*/

            int CALL_BACK_FRAME = 640;  //1 channel 16bit,16k,16ms
            int SEND_FRAME = 320; // PCM mono, 16bit, 8k, 16ms
            CallbackBuf = new byte[CALL_BACK_FRAME];
            SendBuf = new byte[SEND_FRAME];
            final int[] allSize = new int[1];

            SoundAIManager.setOnSAIVoipListener(new SoundAIManager.OnVoipListener() {
                @Override
                public void onVoip(byte[] buffer, int size) {
                    try {
                        pipedOutputStream.write(buffer);
                        synchronized (mutex) {
                            allSize[0] = allSize[0] + size;
                        }

                        if (allSize[0] >= 640) {
                            emitService.submit(new Runnable() {
                                @Override
                                public void run() {
                                    Process.setThreadPriority(Process.THREAD_PRIORITY_URGENT_AUDIO);
                                    synchronized (mutex) {
                                        allSize[0] = allSize[0] - 640;
                                    }
                                    try {
                                        pipedInputStream.read(CallbackBuf, 0, CALL_BACK_FRAME);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    int a = 0;
                                    for (int r = 0; r < CALL_BACK_FRAME; r = r + 4) {
                                        SendBuf[a] = CallbackBuf[r];
                                        SendBuf[a + 1] = CallbackBuf[r + 1];
                                        a = a + 2;
                                    }
                                    byte[] temp = null;

                                    if (!isCallMuted()) {
                                        temp = SendBuf.clone();
                                    } else {
                                        temp = new byte[SEND_FRAME];
                                    }
                                    mVoipService.sendVoiceData(temp);
                                }
                            });
                        }

                    } catch (IOException e) {
                        Log.e(TAG, "pipedOutputStream error = " + e.toString());
                        SpotDeviceLog.exception(TAG, "", e.getMessage());
                    }
                }
            });


            //resume function
            SoundAIManager.startVoip();
            audioTrackThread.start();

            //timer
            onCallTimer = new Timer();
            onCallTimer.scheduleAtFixedRate(new OnCallTask(), 1000, 1000);

        } catch (IOException e) {
            Log.e(TAG, "startVoip error = " + e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }

    }

    private AudioTrack getAudioTrack() {
        if (mAudioTrack == null) {
            mAudioTrack = new AudioTrack(
                    AudioManager.STREAM_MUSIC,
                    8000,
                    AudioFormat.CHANNEL_OUT_MONO,
                    AudioFormat.ENCODING_PCM_16BIT,
                    12 * 320,
                    AudioTrack.MODE_STREAM);

            mAudioTrack.setVolume(2.0f);
            mAudioTrack.play();
        }
        return mAudioTrack;
    }


    class OnCallTask extends TimerTask {
        public void run() {
            EventBus.getDefault().post(new VoIPStateChange(VoIPStateChange.UPDATE_ON_CALL_TIME, getTime()));
        }
    }


    public void stopVoip() {
        Log.e(TAG, "stop voip");

        try {
            SoundAIManager.stopVoip();
            SoundAIManager.setOnSAIVoipListener(null);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }

        try {
            emitService.shutdown();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }

        try {
            pipedOutputStream.close();
            pipedInputStream.close();
            pipedOutputStream = null;
            pipedInputStream = null;
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }

        try {
            audioTrackThread.interrupt();
            audioTrackThread = null;
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }

        try {
            onCallTimer.cancel();
            onCallTimer = null;
            Log.d(TAG, " onCallTimer.cancel()");
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }

    }


    private void StartRingTone() {
        try {
            Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setDataSource(ChatApplication.context, alert);
            final AudioManager audioManager = (AudioManager) ChatApplication.context.getSystemService(Context.AUDIO_SERVICE);
            if (audioManager.getStreamVolume(AudioManager.STREAM_RING) != 0) {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_RING);
                mMediaPlayer.setLooping(true);
                mMediaPlayer.prepare();
                mMediaPlayer.start();
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }
    }

    private void stopRingTone() {
        try {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        } catch (Exception e) {

        }
    }

    private void StartWaitingRingTone() {
        try {
            mMediaPlayer = new MediaPlayer();
            AssetFileDescriptor file = ChatApplication.context.getResources().openRawResourceFd(R.raw.ringtone1);
            mMediaPlayer.setDataSource(file.getFileDescriptor(), file.getStartOffset(),
                    file.getLength());
            mMediaPlayer.setVolume(0.5f, 0.5f);
            mMediaPlayer.setLooping(true);
            mMediaPlayer.prepare();
            mMediaPlayer.start();
            file.close();

        } catch (Exception e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }
    }


    public void makeCall(String roomNumber) {
        Log.d(TAG, "makeCall to Room: " + roomNumber);
        targetNum = roomNumber;
        makeCallRes = "";
        if (connStatus != ConnStatus.online) {
            currentState.handleEvent(Event.makeCallRes, ResponseStatus.OFFLINE.name());
            return;
        }
        if (targetNum.equals(DeviceInfo.roomName)) {
            currentState.handleEvent(Event.makeCallRes, ResponseStatus.SELF.name());
            return;
        }
        mVoipService.makeCall(targetNum);
    }


    public boolean isCallMuted() {
        return callMuted;
    }

    private static Thread makeCallTimeout = null;

    private void startMakeCallTimeout(int second) {

        makeCallTimeout = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(second * 1000);
                    if (!SoundAIManager.onVoip) {
                        currentState.handleEvent(Event.decline, TX);
                    }
                } catch (InterruptedException ignore) {

                }
            }
        });
        makeCallTimeout.start();
    }

    public void stopMakeCallTimeout() {
        Log.d(TAG, "stopMakeCallTimeout");
        if (makeCallTimeout != null) {
            makeCallTimeout.interrupt();
            makeCallTimeout = null;
        }
    }


    private String getTime() {
        int inSec = VoipModuleManager.onCallCount++;
        int day, hour, minute, second;
        day = (inSec / 60 / 60 / 24);
        hour = (inSec / 60 / 60) % 24;
        minute = (inSec / 60) % 60;
        second = inSec % 60;

        String showTime = "";
        if (day != 0) {
            showTime = day + " : " + binaryFormat(hour) + " : " + binaryFormat(minute) + " : " + binaryFormat(second);
        } else if (hour != 0) {
            showTime = binaryFormat(hour) + " : " + binaryFormat(minute) + " : " + binaryFormat(second);
        } else if (minute != 0) {
            showTime = binaryFormat(minute) + " : " + binaryFormat(second);
        } else if (second != 0) {
            showTime = " 00 : " + binaryFormat(second);
        }
        return showTime;
    }

    private String binaryFormat(int i) {
        String t = "";
        if (i < 10) {
            t = "0" + i;
        } else {
            t = String.valueOf(i);
        }
        return t;
    }

    public void stopPing() {
        use_ping_pong = false;
        pingThread.interrupt();
        pingThread = null;
    }


    public void setCallMute() {
        callMuted = !callMuted;
    }


    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onMessageEvent(SystemEvent event) {
        super.onMessageEvent(event);
        switch (event.getType()) {
            case WIFI_DISCONNECTED:
                //close voip process
                stopVoip();
                currentState.handleEvent(Event.offline, "");
                break;
        }
    }

}
