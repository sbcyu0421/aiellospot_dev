package ai.aiello.aiellospot.modules.voip;

import ai.aiello.aiellospot.core.info.DeviceInfo;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.litesuits.android.log.Log;

import io.kvh.media.amr.AmrDecoder;
import io.kvh.media.amr.AmrEncoder;
import io.socket.client.IO;
import io.socket.client.Manager;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.transports.PollingXHR;
import io.socket.engineio.client.transports.WebSocket;
import io.socket.engineio.parser.Parser;

import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.logging.*;

public class VoipService extends Service {

    //VoipService
    public final static String TAG = VoipService.class.getSimpleName();
    private final IBinder mBinder = new LocalBinder();
    private static VoipCallback mCallback;

    //Socket-io
    public static final String EVENT_CALL_MAKE = "TX_CALL_MAKE";
    public static final String EVENT_TX_CALL_MAKE_CANCEL = "TX_CALL_MAKE_CANCEL";
    public static final String EVENT_INCOMING_CALL = "RX_INCOMING_CALL";
    public static final String EVENT_INCOMING_CALL_CANCEL = "RX_INCOMING_CALL_CANCEL";
    public static final String EVENT_ACCEPT_CALL = "RX_ACCEPT_CALL";
    public static final String EVENT_DECLINE_CALL = "RX_DECLINE_CALL";
    public static final String EVENT_CALL_END = "TX_CALL_END";

    public static final String EVENT_TX_DISABLE_CALL = "TX_DISABLE_CALL";

    public static final String EVENT_TX_MUTE_CALL = "TX_MUTE_CALL";

    public static final String EVENT_MY_PING = "myping";
    public static final String EVENT_MY_PONG = "mypong";
    public static final String EVENT_VOICE_CHUNK = "recognize";
    private static Socket mSocket;

    //OpenAMR
    public static long state;
    public static byte[] RecvBuf;

    public enum SessionStatus {
        SESSION_INIT,
        SESSION_CONNECT,
        SESSION_DISCONNECT,
        SESSION_RECONNECT,
        SESSION_MY_PING,
        SESSION_MY_PONG,
        SESSION_MUTE,
    }

    public interface VoipCallback {
        void onCallUpdate(String event, String info);

        void onSessionUpdate(SessionStatus status);

        void onVoiceDataRecv(byte[] data);

        void onMute(boolean setTrackerMute);
    }

    public class LocalBinder extends Binder {
        VoipService getService() {
            return VoipService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.e(TAG, "onBind");
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    public void init(boolean debug, String ServerAddress) {
        try {
            IO.Options opt = new IO.Options();
            opt.forceNew = false;
            opt.reconnection = true;
            opt.timeout = 1500;
            //opt.transports = new String[]{"websocket", "polling"};
            opt.transports = new String[]{"websocket"};
            //opt.reconnectionDelay = 100;//reconnection delay time after timeout
            //opt.reconnectionDelayMax = 500;
            mSocket = IO.socket(ServerAddress, opt);
            //mSocket = IO.socket(ServerAddress);

            //Basic Event
            mSocket.on(Socket.EVENT_CONNECT, onConnect);
            mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
            mSocket.on(Socket.EVENT_RECONNECT, onReconnect);
            //mSocket.on(Socket.EVENT_PING, onPing);
            //mSocket.on(Socket.EVENT_PONG, onPong);
            mSocket.on(EVENT_MY_PONG, onMyPong);


            //Voip Event
            mSocket.on(EVENT_CALL_MAKE, onCallMake);
            mSocket.on(EVENT_INCOMING_CALL, onIncomingCall);
            mSocket.on(EVENT_INCOMING_CALL_CANCEL, onIncomingCallCancel);
            mSocket.on(EVENT_ACCEPT_CALL, onAcceptCall);
            mSocket.on(EVENT_DECLINE_CALL, onDeclineCall);
            mSocket.on(EVENT_CALL_END, onCallEnd);
            mSocket.on(EVENT_TX_MUTE_CALL, onVoipMute);
            mSocket.on(EVENT_TX_DISABLE_CALL, onDisableCall);

            mSocket.on(EVENT_VOICE_CHUNK, onRecognize);

            mSocket.connect();

            mCallback.onSessionUpdate(SessionStatus.SESSION_INIT);

            AmrEncoder.init(0);
            state = AmrDecoder.init();

            if (debug) {
                // init a new logger with level FINE
                Logger logger = Logger.getLogger(Socket.class.getName());
                logger.setLevel(Level.FINE);
                Handler consoleHandler = new ConsoleHandler();
                consoleHandler.setLevel(Level.ALL);
                logger.addHandler(consoleHandler);

                ArrayList<String> loggerName = new ArrayList<String>();
                loggerName.add(IO.class.getName());
                loggerName.add(Socket.class.getName());
                loggerName.add(Manager.class.getName());
                loggerName.add(PollingXHR.class.getName());
                loggerName.add(WebSocket.class.getName());
                loggerName.add(io.socket.engineio.client.Socket.class.getName());
                loggerName.add(Parser.class.getName());

                for (String tag : loggerName) {
                    Logger temp = LogManager.getLogManager().getLogger(tag);
                    if (temp != null) {
                        temp.setLevel(Level.ALL);
                        temp.addHandler(consoleHandler);
                    } else {
                        Log.e(TAG, "add log=" + tag);
                    }
                }
            }
        } catch (URISyntaxException e) {
            Log.d(TAG, e.toString());
        }


    }

    private String enCodeRoomName(String room) {
        return room + "_" + DeviceInfo.MAC.replace(":", "").toUpperCase();
    }


    public void setRoomID(String room) {
        Log.e(TAG, "setRoomID=" + enCodeRoomName(room));
        mSocket.emit("voice connect", enCodeRoomName(room));
    }

    public void setCallback(VoipCallback cb) {
        mCallback = cb;
    }

    public void makeCall(String room) {
        Log.e(TAG, EVENT_CALL_MAKE + " = " + room);
        mSocket.emit(EVENT_CALL_MAKE, room);
    }

    public void Tx_Call_Make_Cancel(String room) {
        Log.e(TAG, EVENT_TX_CALL_MAKE_CANCEL + " = " + room);
        mSocket.emit(EVENT_TX_CALL_MAKE_CANCEL, room);
    }

    public void Rx_Accept_Call(String room) {
        Log.e(TAG, EVENT_ACCEPT_CALL + " = " + room);
        mSocket.emit(EVENT_ACCEPT_CALL, room);
    }

    public void Rx_Decline_Call(String room) {
        Log.e(TAG, EVENT_DECLINE_CALL + " = " + room);
        mSocket.emit(EVENT_DECLINE_CALL, room);
    }

    public void Tx_Call_End(String room) {
        Log.e(TAG, EVENT_CALL_END + " = " + room);
        mSocket.emit(EVENT_CALL_END, room);
    }


    public void myPing(String room) {
        mSocket.emit(EVENT_MY_PING, enCodeRoomName(room));
//        Log.i(TAG, "myPing = " + enCodeRoomName(room));
        mCallback.onSessionUpdate(SessionStatus.SESSION_MY_PING);
    }

    public void TX_Mute_Call(boolean setMute) {
        Log.e(TAG, EVENT_TX_MUTE_CALL + " = " + setMute);
        mSocket.emit(EVENT_TX_MUTE_CALL, setMute);
    }


    //short array read from AudioRecorder, length 160
    //Log.d(TAG, "data.length = " + data.length);
    short[] in = new short[160];
    //output amr frame, length 32
    byte[] out = new byte[32];

    public void sendVoiceData(byte[] data) {
        ByteBuffer.wrap(data).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(in);
        long t1 = System.currentTimeMillis();
        AmrEncoder.encode(AmrEncoder.Mode.MR122.ordinal(), in, out);
        long t2 = System.currentTimeMillis();
        mSocket.emit("send voice", out);
        long t3 = System.currentTimeMillis();
//        Log.d(TAG, "encodeTime = " + (t2 - t1) + ", emitTime = " + (t3 - t2));
    }


    public void disConnect() {

        //Basic Event
        mSocket.off(Socket.EVENT_CONNECT, onConnect);
        mSocket.off(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.off(Socket.EVENT_RECONNECT, onReconnect);
        mSocket.off(EVENT_MY_PONG, onMyPong);

        //Voip Event
        mSocket.off(EVENT_CALL_MAKE, onCallMake);
        mSocket.off(EVENT_INCOMING_CALL, onIncomingCall);
        mSocket.off(EVENT_INCOMING_CALL_CANCEL, onIncomingCallCancel);
        mSocket.off(EVENT_ACCEPT_CALL, onAcceptCall);
        mSocket.off(EVENT_DECLINE_CALL, onDeclineCall);
        mSocket.off(EVENT_CALL_END, onCallEnd);
        mSocket.off(EVENT_TX_MUTE_CALL, onVoipMute);
        mSocket.off(EVENT_TX_DISABLE_CALL, onDisableCall);

        mSocket.off(EVENT_VOICE_CHUNK, onRecognize);
        mSocket.disconnect();
        mSocket.close();
    }

    /*
        Server Side Message
     */
    //Session message
    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            mCallback.onSessionUpdate(SessionStatus.SESSION_CONNECT);
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            mCallback.onSessionUpdate(SessionStatus.SESSION_DISCONNECT);
        }
    };

    private Emitter.Listener onReconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            mCallback.onSessionUpdate(SessionStatus.SESSION_RECONNECT);
        }
    };

    private Emitter.Listener onVoipMute = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
//            boolean muteTracker = (boolean) args[0];
//            mCallback.onMute(muteTracker);
            android.util.Log.d(TAG, "onVoipMute, do not handle");
        }
    };

    private Emitter.Listener onMyPong = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            mCallback.onSessionUpdate(SessionStatus.SESSION_MY_PONG);
        }
    };

    //Call message
    private Emitter.Listener onCallMake = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            int res = (int) args[0];
            if (res == 0) {
                mCallback.onCallUpdate(VoipModuleManager.MAKE_CALL_RESPONSE, "SUCCESS");
            } else if (res == 5) {
                mCallback.onCallUpdate(VoipModuleManager.MAKE_CALL_RESPONSE, "BUSY");
            } else {
                mCallback.onCallUpdate(VoipModuleManager.MAKE_CALL_RESPONSE, "FAIL");
            }
        }
    };

    private Emitter.Listener onIncomingCall = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e(TAG, "onIncomingCall=" + (String) args[0]);
            mCallback.onCallUpdate(VoipModuleManager.TX_INCOMING_CALL, (String) args[0]);
        }
    };

    private Emitter.Listener onDisableCall = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d(TAG, VoipModuleManager.TX_DISABLE_CALL);
            mCallback.onCallUpdate(VoipModuleManager.TX_DISABLE_CALL, "");
        }
    };


    private Emitter.Listener onIncomingCallCancel = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            mCallback.onCallUpdate(VoipModuleManager.TX_INCOMING_CALL_CANCEL, (String) args[0]);
        }
    };

    private Emitter.Listener onAcceptCall = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            mCallback.onCallUpdate(VoipModuleManager.RX_ACCEPT_CALL, (String) args[0]);
        }
    };

    private Emitter.Listener onDeclineCall = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            mCallback.onCallUpdate(VoipModuleManager.RX_DECLINE_CALL, (String) args[0]);
        }
    };

    private Emitter.Listener onCallEnd = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            mCallback.onCallUpdate(VoipModuleManager.CALL_END, "");
        }
    };

    short[] pcmframs = new short[160];
    ByteBuffer buffer;
    byte[] bytes;

    private Emitter.Listener onRecognize = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            RecvBuf = (byte[]) args[0];
            AmrDecoder.decode(state, RecvBuf, pcmframs);

            buffer = ByteBuffer.allocate(pcmframs.length * 2);
            buffer.order(ByteOrder.LITTLE_ENDIAN);
            buffer.asShortBuffer().put(pcmframs);
            bytes = buffer.array();

            mCallback.onVoiceDataRecv(bytes);
        }
    };

    private Emitter.Listener onDataDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("Marco", "diconnected");
        }
    };


}
