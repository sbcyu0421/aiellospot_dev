package ai.aiello.aiellospot.modules.ads;

import androidx.annotation.Nullable;

import java.io.Serializable;

public class AdsObject implements Serializable {

    private String title;
    private String description;
    private String hint;
    private AdsMediaType type;
    private int period;
    private String url;
    private AdsGroup adsGroup;
    private String qrcode;
    private String lang;
    private boolean showDetail;
    private String localUrl;

    AdsObject(String title, String description, String hint, String type, String period, String imgUrl, String adsGroup, String qrcode, @Nullable String lang, boolean showDetail) {
        this.title = title;
        this.hint = hint;
        this.description = description;
        this.type = AdsMediaType.valueOf(type);
        this.period = Integer.valueOf(period);
        this.url = imgUrl;
        this.adsGroup = AdsGroup.valueOf(adsGroup);
        this.qrcode = qrcode;
        this.lang = lang;
        this.showDetail = showDetail;
    }

    public String getTitle() {
        return title;
    }

    ;

    public String getDescription() {
        return description;
    }

    ;

    public AdsMediaType getType() {
        return type;
    }

    ;

    public String getHint() {
        return hint;
    }

    ;

    public int getPeriod() {
        return period;
    }

    public String getUrl() {
        return url;
    }

    public AdsGroup getAdsGroup() {
        return adsGroup;
    }

    public String getQrcode() {
        return qrcode;
    }

    public String getLang() {
        return lang;
    }

    public boolean isShowDetail() {
        return showDetail;
    }

    public String getLocalUrl() {
        return localUrl;
    }

    public void setLocalUrl(String localUrl) {
        this.localUrl = localUrl;
    }
}
