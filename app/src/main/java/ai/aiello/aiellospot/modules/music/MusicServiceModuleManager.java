package ai.aiello.aiellospot.modules.music;

import android.content.Context;
import android.media.AudioManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.core.config.APIConfig;
import ai.aiello.aiellospot.core.config.Configer;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.media.AielloSoundPool;
import ai.aiello.aiellospot.events.module.MusicStateChange;
import ai.aiello.aiellospot.events.server.ServerEvent;
import ai.aiello.aiellospot.events.system.SystemEvent;
import ai.aiello.aiellospot.modules.AbstractModuleManager;
import ai.aiello.aiellospot.utlis.Http;

public class MusicServiceModuleManager extends AbstractModuleManager implements MusicConnCallback {

    private static final String TAG = MusicServiceModuleManager.class.getSimpleName();
    public static MusicServiceConn musicServiceConn;
    public static ArrayList<AlbumObject> albumList = null;
    public static TrackObject trackObject = new TrackObject();

    public static int position;
    public static int duration;
    public static int progress;

    public static Vendor vendor = Vendor.None;

    public static PlayerStatus playerStatus = PlayerStatus.Offline;

    private static CustomPayLoad customPayLoad;

    private APIConfig.WEBAPI kkboxQrcodeApi;
    private APIConfig.WEBAPI music_take_token;
    private APIConfig.WEBAPI music_return_token;
    private APIConfig.WEBAPI music_public_broadcast_qrcode;


    static class CustomPayLoad {
        int type;
        String message;

        public static final int album = 1;
        public static final int queryText = 2;


        public CustomPayLoad(int type, String message) {
            this.type = type;
            this.message = message;
        }

        public int getType() {
            return type;
        }

        public String getMessage() {
            return message;
        }

    }

    public enum MusicState {PLAY, STOP, HOLD}
    public static MusicState playStatus = MusicState.STOP;

    synchronized static public void setPlayerStatus(PlayerStatus newPlayerStatus) {
        playerStatus = newPlayerStatus;
    }

    public void pbrDone() {
        if (isPbrAuthorized) return;
        isPbrAuthorized = true;
        AielloSoundPool.playDefaultVolSound(AielloSoundPool.pbr_success);
        if (!UIRegistered) { // if user stay at the view, then play it.
            Log.d(TAG, "pbrDone: UI is not registered");
            return;
        }
        if (playerStatus == PlayerStatus.onPlay) { // if is playing ( in the multi device in same room condition)
            Log.d(TAG, "pbrDone: is playing now");
            return;
        }
        if (customPayLoad == null) {
            Log.d(TAG, "pbrDone: no custom play load");
            return;
        }
        EventBus.getDefault().post(new MusicStateChange(MusicStateChange.Target.ON_PBR_SUCCESS));
        switch (customPayLoad.getType()) {
            case CustomPayLoad.album:
                Log.d(TAG, "pbrDone: resume playtrack");
                musicServiceConn.request_playAlbum(customPayLoad.getMessage());
                playStatus = MusicState.PLAY;
                EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.MEDIA_OCCUPY,null, TAG));
                break;
            case CustomPayLoad.queryText:
                Log.d(TAG, "pbrDone: resume playtrack");
                musicServiceConn.requestAssistant(customPayLoad.getMessage());
                playStatus = MusicState.PLAY;
                EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.MEDIA_OCCUPY,null, TAG));
                break;
        }
        customPayLoad = null;
    }

    public enum PlayerStatus {
        Online, Query, onPause, onPlay, Offline
    }

    public enum Vendor {
        None, KKBOX, QQ
    }

    public enum LayoutName { // matches layout name from config data
        Music,
    }

    public static final String Type_NicheAudio = "NicheAudio";
    public static final String Type_News = "News";

    private static AudioManager audioManager;

    private static Handler handler;
    private static boolean syncRemoteService = false;
    private static boolean UIRegistered = false;

    public static boolean isPbrAuthorized = true;

    private static MusicServiceModuleManager instance = null;

    public static MusicServiceModuleManager getInstance() {
        if (instance == null) {
            instance = new MusicServiceModuleManager();
        }
        return instance;
    }

    private MusicServiceModuleManager() {
        android.util.Log.d(TAG, "initialized");
    }

    @Override
    public void create() {
        //setup data
        try {
            kkboxQrcodeApi = APIConfig.WEBAPI.build(Configer.moduleConfigData.getJSONObject("music")
                    .getJSONObject("service_data").getJSONObject("kkbox-qrcode"), Configer.useProductAPI);
            music_take_token = APIConfig.WEBAPI.build("music_take_token",Configer.moduleConfigData.getJSONObject("music")
                    .getJSONObject("service_data").getJSONObject("music_take_token"), Configer.useProductAPI);
            music_return_token = APIConfig.WEBAPI.build("music_return_token", Configer.moduleConfigData.getJSONObject("music")
                    .getJSONObject("service_data").getJSONObject("music_return_token"), Configer.useProductAPI);
            music_public_broadcast_qrcode = APIConfig.WEBAPI.build(Configer.moduleConfigData.getJSONObject("music")
                    .getJSONObject("service_data").getJSONObject("music_public_broadcast_qrcode"), Configer.useProductAPI);
            isPbrAuthorized = DeviceInfo.accuRedirectUrl.isEmpty();
            Log.d(TAG, "enable qrcode auth with accu qrcode = " + !isPbrAuthorized);
            vendor = Vendor.valueOf(Configer.moduleConfigData.getJSONObject("music")
                    .getJSONObject("service_data").getString("sponsor"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        if (MusicServiceModuleManager.musicServiceConn != null) return;
        musicServiceConn = new MusicServiceConn(ChatApplication.context, this);
        musicServiceConn.bindService();
        audioManager = (AudioManager) ChatApplication.context.getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        //for UI sync
        HandlerThread handlerThread = new HandlerThread("Media Sync worker");
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());
    }

    @Override
    public void onHotReload() {
        try {
            kkboxQrcodeApi = APIConfig.WEBAPI.build(Configer.moduleConfigData.getJSONObject("music")
                    .getJSONObject("service_data").getJSONObject("kkbox-qrcode"), Configer.useProductAPI);
            music_take_token = APIConfig.WEBAPI.build("music_take_token",Configer.moduleConfigData.getJSONObject("music")
                    .getJSONObject("service_data").getJSONObject("music_take_token"), Configer.useProductAPI);
            music_return_token = APIConfig.WEBAPI.build("music_return_token", Configer.moduleConfigData.getJSONObject("music")
                    .getJSONObject("service_data").getJSONObject("music_return_token"), Configer.useProductAPI);
            music_public_broadcast_qrcode = APIConfig.WEBAPI.build(Configer.moduleConfigData.getJSONObject("music")
                    .getJSONObject("service_data").getJSONObject("music_public_broadcast_qrcode"), Configer.useProductAPI);
            vendor = Vendor.valueOf(Configer.moduleConfigData.getJSONObject("music")
                    .getJSONObject("service_data").getString("sponsor"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        // rebind while onUnbind
        musicServiceConn.shutdownService();
    }

    private void startSyncRemoteService() {
        if (syncRemoteService) {
            return;
        }
        syncRemoteService = true;
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (UIRegistered) {
                    EventBus.getDefault().post(new MusicStateChange(MusicStateChange.Target.ON_PROGRESS_BAR_UPDATE));
                }
                if (syncRemoteService) {
                    progress = position * 100 / duration;
                    position = position + 1000;
                    handler.postDelayed(this, 1000);
                }
            }
        });
    }


    private void stopSyncRemoteService() {
        syncRemoteService = false;
        handler.removeCallbacks(null);
    }


    public void registerAudioSync() {
        UIRegistered = true;
    }

    public void unRegisterAudioSync() {
        UIRegistered = false;
    }


    void updateTrackInfo(int m_position, int m_duration) {
        duration = m_duration;
        position = m_position;
        startSyncRemoteService();
    }

    private void queryServiceAndStopTimer() {
        EventBus.getDefault().post(new MusicStateChange(MusicStateChange.Target.LOADING));
        stopSyncRemoteService();
    }


    public void getAlbumList() {
        musicServiceConn.request_getAlbumList();
    }

    public void playAlbum(String id) {
        customPayLoad = new CustomPayLoad(CustomPayLoad.album, id);
        // go to the player, player page would handle it, this obj is a dummy
        MusicServiceModuleManager.trackObject = new TrackObject();
        if (isPbrAuthorized) {
            musicServiceConn.request_playAlbum(id);
            playStatus = MusicState.PLAY;
            EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.MEDIA_OCCUPY, null, TAG));
        }
        else EventBus.getDefault().post(new MusicStateChange(MusicStateChange.Target.ON_PLAY));
    }


    public TrackObject getCurrentTrackInfo() {
        return trackObject;
    }

    public void playNext() {
        musicServiceConn.requestNext();
        playStatus = MusicState.PLAY;
        EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.MEDIA_OCCUPY,null, TAG));

    }

    public void playPrev() {
        musicServiceConn.requestPrev();
        playStatus = MusicState.PLAY;
        EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.MEDIA_OCCUPY,null, TAG));

    }

    public void play() {
        // change play state
        if (playStatus == MusicState.PLAY) return;
        playStatus = MusicState.PLAY;
        // send notification with eventbus
        EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.MEDIA_OCCUPY, null, TAG));
        // execute action
        musicServiceConn.requestResume();
    }

    public void pause() {
        if (playStatus == MusicState.PLAY)musicServiceConn.requestPause(true);
        playStatus = MusicState.HOLD;
    }

    public void stop() {
        if (playStatus == MusicState.PLAY)musicServiceConn.requestPause(true);
        playStatus = MusicState.STOP;
    }

    public void seekTo(int milliSec) {
        musicServiceConn.requestSeekTo(milliSec);
        playStatus = MusicState.PLAY;
        EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.MEDIA_OCCUPY,null, TAG));
    }

    public void askKKAssistant(String querytext) {
        customPayLoad = new CustomPayLoad(CustomPayLoad.queryText, querytext);
        if (isPbrAuthorized) {
            musicServiceConn.requestAssistant(querytext);
            playStatus = MusicState.PLAY;
            EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.MEDIA_OCCUPY,null, TAG));
        }
//        else EventBus.getDefault().post(new UI_MusicEvent(UI_MusicEvent.Target.SHOW_PBR_DIALOG));
    }


    public void requestCustomAudio(String album, String artist, String audio, String image, String name, String length, String subject) {
        musicServiceConn.requestCustomAudio(album, artist, audio, image, name, length, subject);
    }




    public static void updateLicenceState() {
        String appState = ChatApplication.currentAppState.name();
        musicServiceConn.updateMusicLicenceState(appState);
        Log.d(TAG, "updateIdleState: IdleState = " + appState);
    }

    public void shutdownService() {
        musicServiceConn.shutdownService();
    }

    public void setTerritory() {
        musicServiceConn.setTerritory();
        albumList = null;
    }

    private String kkboxQRcode = "";

    public String getKkboxQRcode() {
        return kkboxQRcode;
    }

    // deprecated
    private void setKkboxQRCode() {
        final int tryMax = 5;
        if (vendor == Vendor.KKBOX) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    int tryCount = 0;
                    while (kkboxQRcode.isEmpty() && tryCount < tryMax) {
                        try {
                            Log.i(TAG, "setKkboxQRCode");
                            String url = String.format(kkboxQrcodeApi.getUrl(), DeviceInfo.uuid);
                            String s = Http.get(url, 5000, kkboxQrcodeApi.getKey()).response;
                            String kkboxImageUrl = "https://qrcode.kkbox.com.tw/generator?content=%s&image_size=%s&logo_size=%s&response_type=%s";
                            String content = URLEncoder.encode(new JSONObject(s).getString("data"));
                            if (content.isEmpty()) {
                                throw new Exception("content is empty");
                            }
                            //params
                            int image_size = 150;
                            int logo_size = 50;
                            String response_type = "image";
                            kkboxQRcode = String.format(kkboxImageUrl, content, image_size, logo_size, response_type);
                        } catch (Exception e) {
                            Log.e(TAG, "setKkboxQRCode error = " + e.toString());
                            SpotDeviceLog.exception(TAG, "", "setKkboxQRCode error = " + e.getMessage());
                        }
                        tryCount++;
                    }
                }
            });
        }
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onMessageEvent(ServerEvent event) {
        switch (event.getType()) {
            case PBR_AUTH:
                pbrDone();
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onMessageEvent(SystemEvent event) {
        super.onMessageEvent(event);
        switch (event.getType()) {
            case ALL_CHANNEL_STOP:
            case TEMP_MEDIA_OCCUPY:
                switch (playStatus) {
                    case PLAY:
                        pause();
                        break;
                    case HOLD:
                        stop();
                        break;
                    case STOP:
                        break;
                }
                break;
            case APP_STATE_CHANGED:
                updateLicenceState();
                break;
            case TEMP_MEDIA_RELEASE:
                if (playStatus == MusicState.HOLD) play();
                break;
            case MEDIA_OCCUPY:
                if (event.getMusicSource().equals(TAG)) return;
                switch (playStatus) {
                    case PLAY:
                    case HOLD:
                        stop();
                        break;
                }
                break;
        }
    }

    @Override
    public void onGetAlbumsCompleted() {
        Log.d(TAG, "onGetAlbumsCompleted");
        EventBus.getDefault().post(new MusicStateChange(MusicStateChange.Target.ON_FETCH_MENU_COMPLETED));
    }

    @Override
    public void onLoading() {
        Log.d(TAG, "onLoading");
        handler.post(new Runnable() {
            @Override
            public void run() {
                queryServiceAndStopTimer();
                musicServiceConn.setTimeout(10 * 1000);
            }
        });
    }

    @Override
    public void onPlay() {
        Log.d(TAG, "onPlay");
        EventBus.getDefault().post(new MusicStateChange(MusicStateChange.Target.ON_PLAY));
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause");
        EventBus.getDefault().post(new MusicStateChange(MusicStateChange.Target.ON_PAUSE));
    }

    @Override
    public void onError() {
        Log.d(TAG, "onError");
        EventBus.getDefault().post(new MusicStateChange(MusicStateChange.Target.ERROR));
    }

    @Override
    public void onTrackInfoLoaded() {
        Log.d(TAG, "onTrackInfoLoaded");
        EventBus.getDefault().post(new MusicStateChange(MusicStateChange.Target.ON_TRACK_INFO_LOADED));
    }


}
