package ai.aiello.aiellospot.modules.music;

public class Constants {

    //receive
    public static final int request_initService = 9901;
    public static final int request_getAlbumList = 9902;

    public static final int request_playTrack = 9100;
    public static final int request_next = 9101;
    public static final int request_prev = 9102;
    public static final int request_resume = 9103;
    public static final int request_pause = 9104;
    public static final int request_seekTo = 9105;

    public static final int request_volDown = 9106;
    public static final int request_volResume = 9107;

    public static final int request_shutdown = 9150;
    public static final int update_app_state = 9153;

    public static final int request_assistant = 9200;
    public static final int request_set_territory = 9300;

    public static final int request_custom_audio = 9400;

    public static final String KEY_ALBUMID = "KEY_ALBUMID";
    public static final String KEY_SEEKTO = "KEY_SEEKTO";
    public static final String KEY_CONTENT = "KEY_CONTENT";
    public static final String KEY_ASSISTANT = "KEY_ASSISTANT";
    public static final String KEY_VOL = "KEY_VOL";
    public static final String KEY_UUID = "KEY_UUID";
    public static final String KEY_MUSIC = "KEY_MUSIC";
    public static final String KEY_HOTEL = "KEY_HOTEL";
    public static final String KEY_TERRITORY = "KEY_TERRITORY";
    public static final String KKBOX_USERNAME = "KKBOX_USERNAME";

    public static final String KEY_CUSTOM_ALBUM = "KEY_CUSTOM_ALBUM";
    public static final String KEY_CUSTOM_ARTIST = "KEY_CUSTOM_ARTIST";
    public static final String KEY_CUSTOM_AUDIO = "KEY_CUSTOM_AUDIO";
    public static final String KEY_CUSTOM_IMAGE = "KEY_CUSTOM_IMAGE";
    public static final String KEY_CUSTOM_NAME = "KEY_CUSTOM_NAME";
    public static final String KEY_CUSTOM_LENGTH = "KEY_CUSTOM_LENGTH";
    public static final String KEY_CUSTOM_SUBJECT = "KEY_CUSTOM_SUBJECT";

    public static final String KEY_APP_STATE = "KEY_APP_STATE";
    public static final String KEY_MAC = "KEY_MAC";
    public static final String KEY_ROOM = "KEY_ROOM";

    public static final int response_getAlbumList = 8801;
    public static final int response_trackInfo = 8802;
    public static final int response_assistResponse = 8803;
    public static final int response_network_error = 8804;

    public static final int status_onPlay = 8901;
    public static final int status_onPause = 8902;
    public static final int status_onResume = 8903;

    public static final int status_onLine = 8701;
    public static final int status_offLine = 8702;
    public static final int status_loadError = 8703;

    public static int kkbox_subscribe_success = 8601;
}
