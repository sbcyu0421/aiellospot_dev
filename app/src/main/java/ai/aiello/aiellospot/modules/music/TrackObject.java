package ai.aiello.aiellospot.modules.music;

public class TrackObject {


    private String id;
    private String trackName;
    private int trackDuration;
    private String trackImages;
    private String artistName;
    private String albumName;

    public TrackObject(String id, String trackName, int trackDuration, String trackImages, String artistName, String albumName) {
        this.id = id;
        this.trackName = trackName;
        this.trackDuration = trackDuration;
        this.trackImages = trackImages;
        this.artistName = artistName;
        this.albumName = albumName;
    }

    public TrackObject() {}

    public String getId() {
        return id;
    }

    public String getTrackName() {
        return trackName;
    }

    public int getDuration() {
        return trackDuration;
    }

    public String getTrackImages() {
        return trackImages;
    }

    public String getArtistName() {
        return artistName;
    }

    public String getAlbumName() {
        return albumName;
    }

    @Override
    public String toString() {
        return "trackName='" + trackName + '\'' +", albumName='" + albumName + '\'';
    }
}
