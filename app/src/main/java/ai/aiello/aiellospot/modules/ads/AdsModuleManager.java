package ai.aiello.aiellospot.modules.ads;

import android.util.Log;

import androidx.annotation.Nullable;

import com.litesuits.common.io.FileUtils;

import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.core.config.APIConfig;
import ai.aiello.aiellospot.core.config.Configer;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.modules.AbstractModuleManager;
import ai.aiello.aiellospot.utlis.Http;
import ai.aiello.chinese.ChineseUtils;

public class AdsModuleManager extends AbstractModuleManager {

    private static String TAG = AdsModuleManager.class.getSimpleName();
    private static AdsModuleManager instance;
    private ArrayList<AdsObject> playList;

    private APIConfig.WEBAPI adsApi;

    public static AdsModuleManager getInstance() {
        if (instance == null) {
            instance = new AdsModuleManager();
        }
        return instance;
    }

    public enum LayoutName {
        Ads,
    }

    private AdsModuleManager() {
        android.util.Log.d(TAG, "initialized");
    }

    @Override
    public void create() {
        // setup data
        try {
            adsApi = APIConfig.WEBAPI.build(Configer.moduleConfigData.getJSONObject("ads").getJSONObject("service_data"), Configer.useProductAPI);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //set module idle state from hot reload timing
        isIdleModule = true;
    }

    @Override
    public void onStart() {
        fetchAds(5000, 3000, null);
    }

    @Override
    public void onHotReload() {
        try {
            adsApi = APIConfig.WEBAPI.build(Configer.moduleConfigData.getJSONObject("ads").getJSONObject("service_data"), Configer.useProductAPI);
        } catch (Exception e) {
            e.printStackTrace();
        }
        fetchAds(5000, 3000, null);
    }

    private int cursor;

    public void setCursor(int index) {
        cursor = index;
        Log.d(TAG, "set cursor = " + cursor);
    }

    public int getCursor() {
        Log.d(TAG, "load cursor = " + cursor);
        return cursor;
    }

    public ArrayList<AdsObject> getPlayList() {
        return playList;
    }

    private String adsDirectory = "sdcard/ads";

    private Thread downloadThread;

    public boolean isReady() {
        return getPlayList().size() > 0;
    }

    public interface OnCompleteListener {
        void onComplete();
    }


    public synchronized void fetchAds(int initDelayMs, int freqDelayMs, @Nullable OnCompleteListener onCompleteListener) {
        if (downloadThread != null) {
            Log.e(TAG, "restart fetch ads");
            downloadThread.interrupt();
        }
        downloadThread = new Thread(new Runnable() {
            @Override
            public void run() {
                cursor = 0;
                playList = new ArrayList<>();
                try {
                    try {
                        FileUtils.deleteDirectory(new File(adsDirectory));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Thread.sleep(initDelayMs);
                    String adsUrl_formate = String.format(adsApi.getUrl(), DeviceInfo.hotelID, ChatApplication.locale.toString());
                    String res = Http.get(adsUrl_formate, 5000, adsApi.getKey()).response;
                    if (res.isEmpty()) {
                        return;
                    }
                    res = StringEscapeUtils.unescapeJava(res);
                    Log.d(TAG, "fetch ads online = " + res);
                    JSONArray jsonArray = new JSONArray(res);
                    for (int t = 0; t < jsonArray.length(); t++) {
                        try {
                            JSONObject ado = jsonArray.getJSONObject(t);
                            String title = ado.getString("title");
                            String description = ado.getString("description");
                            String url = ado.getString("url");
                            String qrcode = ado.getString("qrcode");
                            String period = ado.getString("period");
                            String type = ado.getString("type");
                            String adsGroup = ado.getString("adsGroup");
                            String hint = ado.getString("hint");
                            String lang = ado.getString("lang");
                            Boolean showDetail = false;
                            try {
                                showDetail = ado.getBoolean("showDetail");
                            } catch (Exception e) {
                                showDetail = false;
                            }

                            if (!title.isEmpty() && !description.isEmpty() && !type.isEmpty() && !type.equals("Video") &&
                                    !period.isEmpty() && !hint.isEmpty() && !url.isEmpty() && !adsGroup.isEmpty() && !lang.isEmpty()) {
                                switch (ChatApplication.system_lang) {
                                    case zh_TW:
                                        if (lang.equals("cn")) {
                                            AdsObject io = new AdsObject(ChineseUtils.toTraditional(title, false),
                                                    ChineseUtils.toTraditional(description, false),
                                                    ChineseUtils.toTraditional(hint, false),
                                                    ChineseUtils.toTraditional(type, false),
                                                    ChineseUtils.toTraditional(period, false),
                                                    ChineseUtils.toTraditional(url, false),
                                                    ChineseUtils.toTraditional(adsGroup, false),
                                                    ChineseUtils.toTraditional(qrcode, false),
                                                    ChineseUtils.toTraditional(lang, false),
                                                    showDetail);
                                            downloadImage(io);
                                        } else {
                                            AdsObject io = new AdsObject(title, description, hint, type, period, url, adsGroup, qrcode, lang, showDetail);
                                            downloadImage(io);
                                        }
                                        break;
                                    case zh_CN:
                                        if (lang.equals("tw")) {
                                            AdsObject io = new AdsObject(ChineseUtils.toSimplified(title, false),
                                                    ChineseUtils.toSimplified(description, false),
                                                    ChineseUtils.toSimplified(hint, false),
                                                    ChineseUtils.toSimplified(type, false),
                                                    ChineseUtils.toSimplified(period, false),
                                                    ChineseUtils.toSimplified(url, false),
                                                    ChineseUtils.toSimplified(adsGroup, false),
                                                    ChineseUtils.toSimplified(qrcode, false),
                                                    ChineseUtils.toSimplified(lang, false),
                                                    showDetail);
                                            downloadImage(io);
                                        } else {
                                            AdsObject io = new AdsObject(title, description, hint, type, period, url, adsGroup, qrcode, lang, showDetail);
                                            downloadImage(io);
                                        }
                                        break;
                                    default:
                                        Log.e(TAG, "default ad = " + title);
                                        AdsObject io = new AdsObject(title, description, hint, type, period, url, adsGroup, qrcode, lang, showDetail);
                                        downloadImage(io);
                                        break;
                                }

                            } else {
                                Log.e(TAG, "sth missing");
                            }
                        } catch (Exception e) {
                            SpotDeviceLog.exception(TAG, "", e.getMessage());
                            Log.e(TAG, e.toString());
                        }
                        //download delay per ads
                        Thread.sleep(freqDelayMs);
                    }
                    if (onCompleteListener != null) {
                        onCompleteListener.onComplete();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        downloadThread.start();

    }


    /*
    download image file to sdcard on initial and certain fetching time,
    this is the strategy for showing ads image without network
     */
    private synchronized void downloadImage(AdsObject io) {
        try {
            AdsObject tmp = download(io);
            while (tmp == null) {
                //retry download image
                Thread.sleep(10 * 1000);
                tmp = download(io);
            }
            playList.add(io);
        } catch (Exception e) {
            Log.e(TAG, "drop the fail img");
        }
    }

    private AdsObject download(AdsObject io) {
        try {
            Log.d(TAG, "start fetch ads image = " + io.getUrl());
            String urlStr = io.getUrl();
            String fileName = urlStr.substring(urlStr.lastIndexOf("/") + 1, urlStr.length());
            FileUtils.copyURLToFile(new URL(io.getUrl()), new File(adsDirectory + "/" + fileName), 30 * 1000, 60 * 1000);
            io.setLocalUrl(adsDirectory + "/" + fileName);
            return io;
        } catch (Exception e) {
            Log.e(TAG, "fail to download img file");
            return null;
        }
    }

}
