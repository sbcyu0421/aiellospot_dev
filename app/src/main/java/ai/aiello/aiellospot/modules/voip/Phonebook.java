package ai.aiello.aiellospot.modules.voip;

public class Phonebook {

    public static final String MAKE_CALL_MSG = "MAKE_CALL_MSG";
    public static String FRONT_DESK_NUMBER = "";
    public static String RESTAURANT_NUMBER = "";
    public static String SOS_NUMBER = "";

    //for hotel
    public final static String MN_APK_REBOOT = "##886886##";

    //for aiello only
    public final static String MN_SETTING_WIFI = "***1000***";
    public final static String MN_SETTING_BT ="***0100***";
    public final static String MN_SETTING ="***0010***";

    public final static String MN_APK_ASR = "***4801***";
    public final static String MN_APK_OTA_PRODUCTION ="***4802***";
    public final static String MN_APK_OTA_VIP ="***4803***";
    public final static String MN_APK_ADS ="***4804***";

    public final static String MN_IMAGE_OTA = "***5801***";

    public final static String MN_DEVICE_REBOOT ="***6801***";

    public final static String MM_DEBUG_AUTO = "***4805***";
    public final static String MN_WEBVIEW ="***1212***";

    public final static String MN_UPLOAD ="***8080***";

    public final static String MN_CLIENT_INFO ="***8081***";

    public final static String MN_SPOT_CONFIG ="***8082***";

    public final static String MN_CHATBOT_AUTOTEST ="***8200***";

    public final static String MN_IOT_TEST_START ="***8501***";
    public final static String MN_IOT_TEST_STOP ="***8502***";
    public final static String MN_IOT_INTEGRATION_MODE ="***8510***";

    public final static String MN_API_DEV_ENV ="***8610***";

    public final static String MN_WIFI_TEST_START ="***8505***";
    public final static String MN_WIFI_TEST_STOP ="***8506***";

    public final static String MN_SYSDIAG_TEST ="***8901***";

}
