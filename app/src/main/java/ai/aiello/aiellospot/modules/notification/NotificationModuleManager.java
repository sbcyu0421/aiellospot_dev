package ai.aiello.aiellospot.modules.notification;

import android.content.Context;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.core.device.DeviceControl;
import ai.aiello.aiellospot.core.media.AielloSoundPool;
import ai.aiello.aiellospot.events.module.NotificationStateChange;
import ai.aiello.aiellospot.events.server.ServerEvent;
import ai.aiello.aiellospot.modules.AbstractModuleManager;

/**
 * Created by a1990 on 2019/5/15.
 */

public class NotificationModuleManager extends AbstractModuleManager {

    private static ArrayList<NotifyObject> notifyList = new ArrayList<>();
    private String TAG = NotificationModuleManager.class.getSimpleName();

    public static final String TYPE_BROADCAST_MSG = "Broadcast_Msg";
    public static final String TYPE_BROADCAST_RATING = "Broadcast_Rating";
    private static NotificationModuleManager instance;

    public enum LayoutName { // matches layout name from config data
        Notification,
    }

    private NotificationModuleManager() {
        Log.d(TAG, "initialized");
    }

    public static NotificationModuleManager getInstance(){
        if (instance == null) {
            instance = new NotificationModuleManager();
        }
        return instance;
    }

    public static void executeShowMsg(String msgFromServer, Context context) {
        AielloSoundPool.play(AielloSoundPool.broadcast_ring);
        String intentName = TYPE_BROADCAST_MSG;
        String nt_msg = msgFromServer;
        Date date = new Date();

        NotifyObject ntf = new NotifyObject(
                intentName,
                context.getString(R.string.notify_title),
                nt_msg,
                date);

        notifyList.add(ntf);
        notifyUpdateView();
    }


    public static void executeShowRating(String taskId, Context context) {
        String intentName = TYPE_BROADCAST_RATING;
        Date date = new Date();

        NotifyObject ntf = new NotifyObject(
                intentName,
                context.getString(R.string.notify_roomservice),
                context.getString(R.string.notify_giverating),
                date,
                taskId);

        notifyList.add(ntf);
        notifyUpdateView();
    }


    public static void notifyUpdateView() {
        EventBus.getDefault().post(new NotificationStateChange());
        if (getUnReadCount() > 0) DeviceControl.backLightOn();
    }


    public static ArrayList<NotifyObject> getNotificationList() {
        return notifyList;
    }

    public static int getUnReadCount() {
        int t = 0;
        for (NotifyObject r : notifyList) {
            if (!r.isRead())
                t++;
        }
        return t;
    }


    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onMessageEvent(ServerEvent event) {
        try {
            switch (event.getType()){
                case SATISFACTION:
                    JSONObject msg_task = new JSONObject(event.getData());
                    String taskId = msg_task.getJSONObject("DATA").getString("TASKID");
                    executeShowRating(taskId, ChatApplication.context);
                    break;

                case NOTIFICATION:
                    JSONObject msg_notify = new JSONObject(event.getData());
                    String msgFromServer = msg_notify.getJSONObject("DATA").getString("MESSAGE");
                    executeShowMsg(msgFromServer, ChatApplication.context);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void create() {
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onHotReload() {

    }
}
