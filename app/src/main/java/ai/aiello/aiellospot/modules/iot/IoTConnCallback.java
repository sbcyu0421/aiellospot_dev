package ai.aiello.aiellospot.modules.iot;

public interface IoTConnCallback {
    void onAcDataUpdate(String data);
}
