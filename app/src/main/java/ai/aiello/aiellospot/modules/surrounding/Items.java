package ai.aiello.aiellospot.modules.surrounding;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import ai.aiello.aiellospot.core.info.DeviceInfo;

public class Items implements Serializable {

    /**
     * type : Cuisine
     * language : zh_TW
     * items : [{"category":["asian","chinese","asian","restaurant","chinese"],"detail":{"geo":"25.04801,121.56638","images":["https://cloud.aiello.ai/place/10d3kijy.pep.jpg"],"address":"台灣台北市新仁里","name":"家樂福（東興店）","rating":0,"placeId":"158wsqqq-76d9f8bec040411d856ecfa9e548ca7f","openingHours":[{"start":"T090000","duration":"PT14H00M","recurrence":"FREQ:DAILY;BYDAY:MO,TU,WE,TH,SU"},{"start":"T090000","duration":"PT15H00M","recurrence":"FREQ:DAILY;BYDAY:FR,SA"}],"location":"https://share.here.com/p/s-Yz1yZXN0YXVyYW50O2lkPTE1OHdzcXFxLTc2ZDlmOGJlYzA0MDQxMWQ4NTZlY2ZhOWU1NDhjYTdmO2xhdD0yNS4wNDgwMTtsb249MTIxLjU2NjM4O249JUU1JUFFJUI2JUU2JUE4JTgyJUU3JUE2JThGJUVGJUJDJTg4JUU2JTlEJUIxJUU4JTg4JTg4JUU1JUJBJTk3JUVGJUJDJTg5O25sYXQ9MjUuMDQ3OTk7bmxvbj0xMjEuNTY2Mzk7cGg9JTJCODg2Mjg3NjgxMTExO2g9M2UwYjI5","href":"https://places.ls.hereapi.com/places/v1/places/158wsqqq-76d9f8bec040411d856ecfa9e548ca7f;context=Zmxvdy1pZD0yMGU0ODE3Ny1iOWM5LTVhNTktODQyOC1jMzNkODY0ZTBkZTBfMTU4OTUxMTM3NzU1OF8wXzMwMTkmcmFuaz05?app_id=LAhJK8ZiyNzTuwsOC9of&app_code=5vdlK1yPwa0kLEU62Mbl_Q","name_en":"Carrefour (Dongxing Store)"}},{"category":["chinese-taiwanese","asian","chinese","asian","restaurant","chinese","chinese-taiwanese"],"detail":{"geo":"25.05184,121.55281","images":["https://cloud.aiello.ai/place/88233bb8-ffb9-58fb-a229-11f175dcc13f.jpg","https://cloud.aiello.ai/place/97ff101f-d9ff-57da-b9fb-3c5bece95069.jpg","https://cloud.aiello.ai/place/b8cba35d-4a09-5bac-b7d6-255334ff1b34.jpg","https://cloud.aiello.ai/place/a2e930da-88a7-501d-a212-f886f7276e9d.jpg","https://cloud.aiello.ai/place/d806fd90-9996-5fd1-b541-6b2e84a1c8f1.jpg"],"address":"台灣台北市松山區南京東路四段61號","rating":0,"placeId":"158wsqqt-b6dc18ff652c43e88133dc99bbdd210b","name":"點水樓","location":"https://share.here.com/p/s-Yz1yZXN0YXVyYW50O2lkPTE1OHdzcXF0LWI2ZGMxOGZmNjUyYzQzZTg4MTMzZGM5OWJiZGQyMTBiO2xhdD0yNS4wNTE4NDtsb249MTIxLjU1MjgxO249JUU5JUJCJTlFJUU2JUIwJUI0JUU2JUE4JTkzO25sYXQ9MjUuMDUxNjk7bmxvbj0xMjEuNTUyODtwaD0lMkI4ODYyODcxMjY2ODk7aD03NDMxM2I","openingHours":[{"start":"T173000","duration":"PT04H30M","recurrence":"FREQ:DAILY;BYDAY:MO,TU,WE,TH,FR,SA,SU"}],"href":"https://places.ls.hereapi.com/places/v1/places/158wsqqt-b6dc18ff652c43e88133dc99bbdd210b;context=Zmxvdy1pZD0yMGU0ODE3Ny1iOWM5LTVhNTktODQyOC1jMzNkODY0ZTBkZTBfMTU4OTUxMTM3NzU1OF8wXzMwMTkmcmFuaz0xNg?app_id=LAhJK8ZiyNzTuwsOC9of&app_code=5vdlK1yPwa0kLEU62Mbl_Q","name_en":"Dian Shui Lou"}}]
     */

    private String type;
    private String language;
    private List<ItemsBean> items;
    private Dummies dummies;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public Dummies getDummies() {
        return dummies;
    }

    public static class ItemsBean implements Serializable{
        /**
         * category : ["asian","chinese","asian","restaurant","chinese"]
         * detail : {"geo":"25.04801,121.56638","images":["https://cloud.aiello.ai/place/10d3kijy.pep.jpg"],"address":"台灣台北市新仁里","name":"家樂福（東興店）","rating":0,"placeId":"158wsqqq-76d9f8bec040411d856ecfa9e548ca7f","openingHours":[{"start":"T090000","duration":"PT14H00M","recurrence":"FREQ:DAILY;BYDAY:MO,TU,WE,TH,SU"},{"start":"T090000","duration":"PT15H00M","recurrence":"FREQ:DAILY;BYDAY:FR,SA"}],"location":"https://share.here.com/p/s-Yz1yZXN0YXVyYW50O2lkPTE1OHdzcXFxLTc2ZDlmOGJlYzA0MDQxMWQ4NTZlY2ZhOWU1NDhjYTdmO2xhdD0yNS4wNDgwMTtsb249MTIxLjU2NjM4O249JUU1JUFFJUI2JUU2JUE4JTgyJUU3JUE2JThGJUVGJUJDJTg4JUU2JTlEJUIxJUU4JTg4JTg4JUU1JUJBJTk3JUVGJUJDJTg5O25sYXQ9MjUuMDQ3OTk7bmxvbj0xMjEuNTY2Mzk7cGg9JTJCODg2Mjg3NjgxMTExO2g9M2UwYjI5","href":"https://places.ls.hereapi.com/places/v1/places/158wsqqq-76d9f8bec040411d856ecfa9e548ca7f;context=Zmxvdy1pZD0yMGU0ODE3Ny1iOWM5LTVhNTktODQyOC1jMzNkODY0ZTBkZTBfMTU4OTUxMTM3NzU1OF8wXzMwMTkmcmFuaz05?app_id=LAhJK8ZiyNzTuwsOC9of&app_code=5vdlK1yPwa0kLEU62Mbl_Q","name_en":"Carrefour (Dongxing Store)"}
         */

        private DetailBean detail;
        private List<String> category;

        public DetailBean getDetail() {
            return detail;
        }

        public void setDetail(DetailBean detail) {
            this.detail = detail;
        }

        public List<String> getCategory() {
            return category;
        }

        public void setCategory(List<String> category) {
            this.category = category;
        }

        public static class DetailBean implements Serializable {
            /**
             * geo : 25.04801,121.56638
             * images : ["https://cloud.aiello.ai/place/10d3kijy.pep.jpg"]
             * address : 台灣台北市新仁里
             * name : 家樂福（東興店）
             * rating : 0
             * placeId : 158wsqqq-76d9f8bec040411d856ecfa9e548ca7f
             * openingHours : [{"start":"T090000","duration":"PT14H00M","recurrence":"FREQ:DAILY;BYDAY:MO,TU,WE,TH,SU"},{"start":"T090000","duration":"PT15H00M","recurrence":"FREQ:DAILY;BYDAY:FR,SA"}]
             * location : https://share.here.com/p/s-Yz1yZXN0YXVyYW50O2lkPTE1OHdzcXFxLTc2ZDlmOGJlYzA0MDQxMWQ4NTZlY2ZhOWU1NDhjYTdmO2xhdD0yNS4wNDgwMTtsb249MTIxLjU2NjM4O249JUU1JUFFJUI2JUU2JUE4JTgyJUU3JUE2JThGJUVGJUJDJTg4JUU2JTlEJUIxJUU4JTg4JTg4JUU1JUJBJTk3JUVGJUJDJTg5O25sYXQ9MjUuMDQ3OTk7bmxvbj0xMjEuNTY2Mzk7cGg9JTJCODg2Mjg3NjgxMTExO2g9M2UwYjI5
             * href : https://places.ls.hereapi.com/places/v1/places/158wsqqq-76d9f8bec040411d856ecfa9e548ca7f;context=Zmxvdy1pZD0yMGU0ODE3Ny1iOWM5LTVhNTktODQyOC1jMzNkODY0ZTBkZTBfMTU4OTUxMTM3NzU1OF8wXzMwMTkmcmFuaz05?app_id=LAhJK8ZiyNzTuwsOC9of&app_code=5vdlK1yPwa0kLEU62Mbl_Q
             * name_en : Carrefour (Dongxing Store)
             * recommand : 1~10
             * googleOpeningHours : {"Mon": ['0700-2100', '0800-2200'], "Tue":['0700-2100', '0800-2200'], "Sat":['0800-2400']}
             */

            private Geo geo;
            private String address;
            private String name;
            private float rating;
            private String placeId;
            private String qrcode;
            private String name_en;

            @SerializedName("recommend")
            private int recommand;
            private List<String> images;
            private float distance;
            private Object openingHours;
            private String openingHoursType;

            public Geo getGeo() {
                return geo;
            }

            public void setGeo(Geo geo) {
                this.geo = geo;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public float getRating() {
                return rating;
            }

            public void setRating(int rating) {
                this.rating = rating;
            }

            public String getPlaceId() {
                return placeId;
            }

            public void setPlaceId(String placeId) {
                this.placeId = placeId;
            }

            public String getQrcode() {
                return qrcode;
            }

            public void setQrcode(String qrcode) {
                this.qrcode = qrcode;
            }

            public String getName_en() {
                return name_en;
            }

            public void setName_en(String name_en) {
                this.name_en = name_en;
            }

            public List<String> getImages() {
                return images;
            }

            public void setImages(List<String> images) {
                this.images = images;
            }

            public int getRecommand() {
                return recommand;
            }

            public void setRecommand(int recommand) {
                this.recommand = recommand;
            }

            public float getDistance() {
                return distance;
            }

            public void setDistance(float distance) {
                this.distance = distance;
            }

            public Object getOpeningHours() {
                return openingHours;
            }

            public void setOpeningHours(Object openingHoursText) {
                this.openingHours = openingHoursText;
            }

            public String getOpeningHoursType() {
                return openingHoursType;
            }

            public void setOpeningHoursType(String openingHoursType) {
                this.openingHoursType = openingHoursType;
            }

            public static class Geo {
                float lat;
                float lng;

                Geo(float lat, float lng) {
                    this.lat = lat;
                    this.lng = lng;
                }

                public float getLat() {
                    return lat;
                }
                public float getLng() {
                    return lng;
                }
            }
        }
    }



    public static class OpeningDay {

        ArrayList<Period> dayPeriods = new ArrayList<>();

        public ArrayList<Period> getDayPeriods() {
            return dayPeriods;
        }

        public void setTime(List<String> dataList) {
            try {
                for (String data : dataList) {
                    try {
                        String start = data.split("-")[0];
                        String end = data.split("-")[1];
                        //start
                        Date startDate = new Date();
                        startDate.setHours(Integer.parseInt(start.substring(0, 2)));
                        startDate.setMinutes(Integer.parseInt(start.substring(2, 4)));
                        //end
                        Date endDate = new Date();
                        endDate.setHours(Integer.parseInt(end.substring(0, 2)));
                        endDate.setMinutes(Integer.parseInt(end.substring(2, 4)));
                        Period period = new Period(startDate, endDate);
                        dayPeriods.add(period);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public static class Period {
        Date start;
        Date end;

        Period(Date start, Date end) {
            this.start = start;
            this.end = end;
        }

        public Date getStart() {
            return start;
        }

        public Date getEnd() {
            return end;
        }
    }

    public static class Dummies {
        int index;
        String name;
        String qrcode;
        List<String> images;

        public int getIndex() {
            return index;
        }

        public String getName() {
            return name;
        }

        public String getQrcode() throws JSONException, UnsupportedEncodingException {
            JSONObject queryParamsJobj = new JSONObject();
            queryParamsJobj.put("hotel", DeviceInfo.hotelName);
            queryParamsJobj.put("device", DeviceInfo.uuid);
            queryParamsJobj.put("TS", Instant.now().getEpochSecond());
            String queryText = queryParamsJobj.toString();
            Base64.Encoder encoder = Base64.getEncoder();
            byte[] textByte = queryText.getBytes("UTF-8");
            String encodedDauth = encoder.encodeToString(textByte);
            Log.d("buildAccuQrcodeForm", "buildAccuQrcodeForm: " + qrcode + "&dauth=" + encodedDauth);
            return qrcode + "&dauth=" + encodedDauth;
        }

        public List<String> getImages() {
            return images;
        }
    }
}