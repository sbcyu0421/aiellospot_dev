package ai.aiello.aiellospot.modules.music;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import com.litesuits.android.log.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.core.config.Configer;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.core.log.MusicLogItem;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.core.media.MsttsManager;

public class MusicServiceConn implements ServiceConnection {

    public static String TAG = MusicServiceConn.class.getSimpleName();

    private Messenger requestMessager;
    private Messenger responseMessenger;
    private Context context;
    private HandlerThread handlerThread;

    private static Thread timeoutThread;

    private static boolean workdone;

    private MusicConnCallback callback;

    private synchronized void setWorkState(boolean done, boolean log) {
        workdone = done;
        if (log) android.util.Log.d(TAG, Thread.currentThread() + " setWorkState: " + done);
        if (done) stopTimeout("get the correct response");
    }

    public void setTimeout(int millisecond) {
        setWorkState(false, true);
        stopTimeout("release old timeout");
        android.util.Log.d(TAG, "create timeout, await = " + millisecond + "ms");
        timeoutThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(millisecond);
//                    Thread.sleep(1500);
                    if (!workdone) {
                        shutdownService();
                        String i = context.getString(R.string.music_resource_error);
                        if (!MsttsManager.getInstance().isPlaying()) {
                            MsttsManager.getInstance().speakWord(i, false, context);
                        }
                        MusicServiceModuleManager.playerStatus = MusicServiceModuleManager.PlayerStatus.Offline;
                        callback.onError();
                    }
                } catch (InterruptedException ignore) {

                } finally {
                    timeoutThread = null;
                }
            }
        });
        timeoutThread.start();
    }

    private void stopTimeout(String from) {
        try {
            if (timeoutThread != null) {
                android.util.Log.d(TAG, "stopTimeout, cause = " + from);
                timeoutThread.interrupt();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            timeoutThread = null;
        }
    }

    MusicServiceConn(Context ctx, MusicConnCallback callback) {
        this.context = ctx;
        this.callback = callback;
        if (handlerThread == null) {
            handlerThread = new HandlerThread("MusicServiceConn Worker");
            handlerThread.start();
        }
        responseMessenger = new Messenger(new Handler(handlerThread.getLooper()) {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                android.util.Log.d(TAG, "handleMessageinit: " + msg.what);
                switch (msg.what) {
                    case Constants.response_getAlbumList:
                        Log.e(TAG, "response_getAlbumList");
                        setWorkState(true, true);
                        MusicServiceModuleManager.albumList = new ArrayList<>();
                        String response = msg.getData().getString(Constants.KEY_CONTENT);

                        try {
                            JSONObject jObj = new JSONObject(response);
                            JSONArray data_array = jObj.getJSONArray("data");
                            for (int i = 0; i < data_array.length(); i++) {
                                JSONObject jObjData = data_array.getJSONObject(i);
                                String id = jObjData.getString("id");
                                String title = jObjData.getString("title");
                                String description = jObjData.getString("description");
                                String oburl = jObjData.getString("url");
                                String imgurl = jObjData.getJSONArray("images").getJSONObject(0).getString("url");
                                AlbumObject ao = new AlbumObject(id, title, description, imgurl, oburl);
                                MusicServiceModuleManager.albumList.add(ao);
                            }
                        } catch (Exception e) {
                            MusicServiceModuleManager.albumList = null;
                            SpotDeviceLog.exception(TAG, "", e.getMessage());
                            Log.e(TAG, e.toString());
                        } finally {
                            callback.onGetAlbumsCompleted();
                        }
                        break;

                    case Constants.response_trackInfo:
                        Log.e(TAG, "response_trackInfo");
                        String trackInfo = msg.getData().getString(Constants.KEY_CONTENT);
                        try {
                            JSONObject jObj = new JSONObject(trackInfo);
                            String id = jObj.getString("id");
                            int position = jObj.getInt("position");
                            String trackName = jObj.getString("name");
                            int trackDuration = jObj.getInt("duration");
                            JSONObject jObj_album = jObj.getJSONObject("album");
                            String albumName = jObj_album.getString("name");
                            String trackImages = ((JSONObject) jObj_album.getJSONArray("images").get(0)).getString("url");
                            String artistName = jObj_album.getJSONObject("artist").getString("name");
                            MusicServiceModuleManager.trackObject = new TrackObject(id, trackName, trackDuration, trackImages, artistName, albumName);
                            //get the info
                            MusicServiceModuleManager.getInstance().updateTrackInfo(position, trackDuration);
                            callback.onTrackInfoLoaded();
                        } catch (Exception e) {
                            MusicServiceModuleManager.trackObject = null;
                            SpotDeviceLog.exception(TAG, "", e.getMessage());
                            Log.e(TAG, e.toString());
                            callback.onError();
                        }
                        break;

                    case Constants.status_onLine:
                        Log.e(TAG, "status_onLine");
                        MusicServiceModuleManager.setPlayerStatus(MusicServiceModuleManager.PlayerStatus.Online);
                        break;

                    case Constants.status_onPlay:
                        Log.e(TAG, "status_onPlay");
                        setWorkState(true, true);
                        MusicServiceModuleManager.setPlayerStatus(MusicServiceModuleManager.PlayerStatus.onPlay);
                        callback.onPlay();
                        MusicLogItem.buildMusicItem(null, SpotUserTraceLog3.EventAction.PLAY, "", "", MusicServiceModuleManager.trackObject.getTrackName(), null);
                        break;

                    case Constants.status_onPause:
                        Log.e(TAG, "status_onPause");
                        setWorkState(true, true);
                        MusicServiceModuleManager.setPlayerStatus(MusicServiceModuleManager.PlayerStatus.onPause);
                        callback.onPause();
                        MusicLogItem.buildMusicItem(null, SpotUserTraceLog3.EventAction.PAUSE, "", "", MusicServiceModuleManager.trackObject.getTrackName(), null);
                        break;

                    case Constants.status_offLine:
                        Log.e(TAG, "status_offLine");
                        MusicServiceModuleManager.setPlayerStatus(MusicServiceModuleManager.PlayerStatus.Offline);
                        requestPause(false);
                        callback.onError();
                        break;

                    case Constants.request_resume:

                        break;

                    case Constants.status_loadError:
                        //EventBus.getDefault().post(new UpdateUIEvent(UpdateUIEvent.TARGET_MUSIC_ERROR));
                        MusicLogItem.buildMusicItem(null, SpotUserTraceLog3.EventAction.ERROR, "", "", MusicServiceModuleManager.trackObject.getTrackName(), null);
                        break;

                    case Constants.response_assistResponse:
                        String request_assistant = msg.getData().getString(Constants.KEY_CONTENT);
                        Log.e(TAG, "request_assistant, response  = " + request_assistant);
                        try {

                            if (request_assistant.contains("KKBOX 有些問題，請再嘗試一次")) {
                                requestPause(false);
                                String i = context.getString(R.string.music_resource_error);
                                if (!MsttsManager.getInstance().isPlaying()) {
                                    MsttsManager.getInstance().speakWord(i, false, context);
                                }
                                MusicServiceModuleManager.setPlayerStatus(MusicServiceModuleManager.PlayerStatus.Offline);
                                callback.onError();
                                return;
                            }

                            JSONObject jObj = new JSONObject(request_assistant);
                            JSONObject kkbox_response = jObj.getJSONObject("response");
                            JSONArray kkbox_response_directives = kkbox_response.getJSONArray("directives");

                            if (kkbox_response_directives.length() == 0) {
                                requestPause(false);
                                MusicServiceModuleManager.getInstance().askKKAssistant("幫我播一首歌");
                                Log.d(TAG, "new query");
                            }

                        } catch (Exception e) {
                            setWorkState(true, true);
                            Log.e(TAG, "music_assist_error , cause " + e.toString());
                            requestPause(false);
                            String i = context.getString(R.string.music_assist_error);
                            if (!MsttsManager.getInstance().isPlaying()) {
                                MsttsManager.getInstance().speakWord(i, false, context);
                            }
                            callback.onError();
                            MusicLogItem.buildMusicItem(null, SpotUserTraceLog3.EventAction.ERROR, "", "", MusicServiceModuleManager.trackObject.getTrackName(), null);
                        }

                        break;

                    case Constants.response_network_error:
                        setWorkState(true, true);
                        callback.onError();
                        MusicLogItem.buildMusicItem(null, SpotUserTraceLog3.EventAction.ERROR, "", "", MusicServiceModuleManager.trackObject.getTrackName(), null);
                        break;

                }
            }
        });
    }


    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        Log.d(TAG, "onServiceConnected");
        requestMessager = new Messenger(service);
        request_initService(DeviceInfo.uuid, MusicServiceModuleManager.vendor.name(),
                DeviceInfo.hotelName, ChatApplication.currentAppState.name(),
                DeviceInfo.MAC, DeviceInfo.roomName
        );
        android.util.Log.d(TAG, "onServiceConnected: requestMessager renew success");
    }


    @Override
    public void onServiceDisconnected(ComponentName name) {
        Log.e(TAG, "onServiceDisconnected");
        SpotDeviceLog.exception(TAG, "", "onMusicServiceDisconnected");
        MusicServiceModuleManager.setPlayerStatus(MusicServiceModuleManager.PlayerStatus.Offline);
        callback.onError();
        bindService();
    }

    public void bindService() {
        String territory = "TW";
        if (ChatApplication.locale.equals(Locale.ENGLISH)) {
            territory = "SG";
        }

        Intent intent = new Intent("ai.aiello.musicservice.IMusicService");
        intent.putExtra("territory", territory);
        intent.putExtra(Configer.KEY_CONFIG, Configer.getClientServiceConfig());
        intent.setPackage("ai.aiello.musicservice");
        context.bindService(intent, this, Context.BIND_AUTO_CREATE);
    }


    public void request_initService(String uuid, String musicType, String hotelName,
                                    String appState, String mac,
                                    String room ) {
        Message message = Message.obtain();
        message.what = Constants.request_initService;
        Bundle bundle = new Bundle();
        bundle.putString(Constants.KEY_UUID, uuid);
        bundle.putString(Constants.KEY_MUSIC, musicType);
        bundle.putString(Constants.KEY_HOTEL, hotelName);
        bundle.putString(Constants.KEY_APP_STATE, appState);
        bundle.putString(Constants.KEY_MAC, mac);
        bundle.putString(Constants.KEY_ROOM, room);
        message.setData(bundle);
        message.replyTo = responseMessenger;
        try {
            requestMessager.send(message);
        } catch (RemoteException e) {
            bundle.clear();
            bundle = null;
            message.recycle();
            message = null;
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }
    }


    public void request_getAlbumList() {
        callback.onLoading();
        Message message = Message.obtain();
        message.what = Constants.request_getAlbumList;
        message.replyTo = responseMessenger;
        try {
            requestMessager.send(message);
        } catch (RemoteException e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }

    }

    public void request_playAlbum(String id) {
        callback.onLoading();
        Message message = Message.obtain();
        message.what = Constants.request_playTrack;
        Bundle bundle = new Bundle();
        bundle.putString(Constants.KEY_ALBUMID, id);
        message.setData(bundle);
        message.replyTo = responseMessenger;
        try {
            requestMessager.send(message);
        } catch (RemoteException e) {
            bundle.clear();
            bundle = null;
            message.recycle();
            message = null;
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }
    }


    public void requestSeekTo(int milliSec) {
        callback.onLoading();
        Message message = Message.obtain();
        message.what = Constants.request_seekTo;
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.KEY_SEEKTO, milliSec);
        message.setData(bundle);
        message.replyTo = responseMessenger;
        try {
            requestMessager.send(message);
        } catch (RemoteException e) {
            bundle.clear();
            bundle = null;
            message.recycle();
            message = null;
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }
    }


    public void requestNext() {
        callback.onLoading();
        Message message = Message.obtain();
        message.what = Constants.request_next;
        message.replyTo = responseMessenger;
        try {
            requestMessager.send(message);
        } catch (RemoteException e) {
            message.recycle();
            message = null;
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }

    }

    public void requestPrev() {
        callback.onLoading();
        Message message = Message.obtain();
        message.what = Constants.request_prev;
        message.replyTo = responseMessenger;
        try {
            requestMessager.send(message);
        } catch (RemoteException e) {
            message.recycle();
            message = null;
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }
    }

    public void requestResume() {
        callback.onLoading();
        Message message = Message.obtain();
        message.what = Constants.request_resume;
        message.replyTo = responseMessenger;
        try {
            requestMessager.send(message);
        } catch (RemoteException e) {
            message.recycle();
            message = null;
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }

    }

    public void requestPause(boolean showLoading) {
        if (showLoading) callback.onLoading();
        Message message = Message.obtain();
        message.what = Constants.request_pause;
        message.replyTo = responseMessenger;
        try {
            requestMessager.send(message);
        } catch (RemoteException e) {
            message.recycle();
            message = null;
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }

    }

    public void requestAssistant(String querytext) {
        callback.onLoading();
        Message message = Message.obtain();
        message.what = Constants.request_assistant;
        Bundle bundle = new Bundle();
        bundle.putString(Constants.KEY_ASSISTANT, querytext);
        message.setData(bundle);
        message.replyTo = responseMessenger;
        try {
            requestMessager.send(message);
        } catch (RemoteException e) {
            bundle.clear();
            bundle = null;
            message.recycle();
            message = null;
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }

    }

    public void requestCustomAudio(String album, String artist, String audio, String image, String name, String length, String subject) {
        callback.onLoading();
        Message message = Message.obtain();
        message.what = Constants.request_custom_audio;
        Bundle bundle = new Bundle();
        bundle.putString(Constants.KEY_CUSTOM_ALBUM, album);
        bundle.putString(Constants.KEY_CUSTOM_ARTIST, artist);
        bundle.putString(Constants.KEY_CUSTOM_AUDIO, audio);
        bundle.putString(Constants.KEY_CUSTOM_IMAGE, image);
        bundle.putString(Constants.KEY_CUSTOM_NAME, name);
        bundle.putString(Constants.KEY_CUSTOM_LENGTH, length);
        bundle.putString(Constants.KEY_CUSTOM_SUBJECT, subject);

        message.setData(bundle);
        message.replyTo = responseMessenger;
        try {
            requestMessager.send(message);
        } catch (RemoteException e) {
            bundle.clear();
            bundle = null;
            message.recycle();
            message = null;
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }

    }

//    public void requestVolDown(int curVol) {
//
//        Message message = Message.obtain();
//        message.what = Constants.request_volDown;
//        Bundle bundle = new Bundle();
//        bundle.putInt(Constants.KEY_VOL, curVol);
//        message.setData(bundle);
//        message.replyTo = responseMessenger;
//        try {
//            requestMessager.send(message);
//        } catch (RemoteException e) {
//            bundle.clear();
//            bundle = null;
//            message.recycle();
//            message = null;
//            Log.e(TAG, e.toString());
//            KafkaManager.getInstance().enqueueLog(ElasticDeviceLog.createExceptionlog(TAG, "", e.getMessage()));
//        }
//
//    }

//    public void requestVolResume() {
//
//        Message message = Message.obtain();
//        message.what = Constants.request_volResume;
//        message.replyTo = responseMessenger;
//        try {
//            requestMessager.send(message);
//        } catch (RemoteException e) {
//            message.recycle();
//            message = null;
//            Log.e(TAG, e.toString());
//            KafkaManager.getInstance().enqueueLog(ElasticDeviceLog.createExceptionlog(TAG, "", e.getMessage()));
//        }
//
//    }

    public boolean shutdownService() {
        Message msg = Message.obtain();
        msg.what = Constants.request_shutdown;
        try {
            requestMessager.send(msg);
        } catch (RemoteException e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }
        return true;
    }


    public void updateMusicLicenceState(String stateName) { // app state: 1. "Busy", 2. "Idle"
        Message msg = Message.obtain();
        msg.what = Constants.update_app_state;
        Bundle bundle = new Bundle();
        bundle.putString(Constants.KEY_APP_STATE, stateName);
        msg.setData(bundle);
        try {
            requestMessager.send(msg);
        } catch (RemoteException e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }
    }

    public void setTerritory() {

        String territory = "TW";
        if (ChatApplication.locale.equals(Locale.ENGLISH)) {
            territory = "SG";
        }

        Message message = Message.obtain();
        message.what = Constants.request_set_territory;
        Bundle bundle = new Bundle();
        bundle.putString(Constants.KEY_TERRITORY, territory);
        message.setData(bundle);
        message.replyTo = responseMessenger;
        try {
            requestMessager.send(message);
        } catch (RemoteException e) {
            bundle.clear();
            bundle = null;
            message.recycle();
            message = null;
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }

    }

}
