package ai.aiello.aiellospot.modules.roomstatus;

import com.litesuits.android.log.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.core.config.APIConfig;
import ai.aiello.aiellospot.core.config.Configer;
import ai.aiello.aiellospot.core.config.SystemConfig;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.events.module.RoomStatusStateChange;
import ai.aiello.aiellospot.events.server.ServerEvent;
import ai.aiello.aiellospot.events.system.SystemEvent;
import ai.aiello.aiellospot.intents.sdui.ActionManager;
import ai.aiello.aiellospot.intents.sdui.ServerDrivenHelper;
import ai.aiello.aiellospot.modules.AbstractModuleManager;
import ai.aiello.aiellospot.modules.iot.IoTModuleManager;
import ai.aiello.aiellospot.utlis.Http;
import ai.aiello.aiellospot.utlis.JsonUtils;

public class RoomStatusModuleManager extends AbstractModuleManager {


    private String TAG = RoomStatusModuleManager.class.getSimpleName();
    public RoomStatus roomStatus = RoomStatus.None;
    public CardStatus cardStatus = CardStatus.None;
    private static final Object mutex = new Object();
    private StateRepository stateRepository;

    private static RoomStatusModuleManager instance = null;

    private APIConfig.WEBAPI murApi;
    private APIConfig.WEBAPI dndApi;

    public static RoomStatusModuleManager getInstance() {
        if (instance == null) {
            instance = new RoomStatusModuleManager();
        }
        return instance;
    }

    private RoomStatusModuleManager() {
        android.util.Log.d(TAG, "initialized");
    }

    @Override
    public void create() {
        applyFetchData();
        // push once at first
        pushCurrentRsToDB();
    }

    private void applyFetchData() {
        stateRepository = new StateRepository(ChatApplication.context);
        roomStatus = RoomStatus.valueOf(
                stateRepository.get(StateRepository.KEY_ROOM_STATUS, "None"));

        // setup data
        JSONObject jsonObject = Configer.moduleConfigData;
        try {
            murApi = APIConfig.WEBAPI.build(jsonObject.getJSONObject("roomstatus").getJSONObject("service_data")
                    .getJSONObject("mur_api"), Configer.useProductAPI);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            dndApi = APIConfig.WEBAPI.build(jsonObject.getJSONObject("roomstatus").getJSONObject("service_data")
                    .getJSONObject("dnd_api"), Configer.useProductAPI);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            RoomStatusModule.setRoomStatusCmdCode(jsonObject.getJSONObject("roomstatus"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onHotReload() {
        applyFetchData();
        // push once at first
        pushCurrentRsToDB();
    }


    private void doByAction(String request){
        try {
            JSONObject jsonObject = new JSONObject(request);
            String uniKey = jsonObject.getJSONArray("loop_codes").getJSONObject(0).getString("code");
            ServerDrivenHelper.ServerMessage serverMessage = ServerDrivenHelper.request(DeviceInfo.hotelName, DeviceInfo.roomType, uniKey, -1);
            if (serverMessage.getResultAction().contains(errorMsg))
                throw new Exception(serverMessage.getResultAction());
            ActionManager.getInstance().prepare(serverMessage.getResultAction(), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void requestClean(boolean request, String from, boolean doAction) {
        android.util.Log.d(TAG, "from = " + from + ", requestClean: " + request + ", current Status = " + roomStatus);
        synchronized (mutex) {
            try {
                if (request && roomStatus != RoomStatus.Clean) {
                    if (!from.equals("DB")) sendToDB(true, murApi);
                    // call action-api
                    if (doAction) {
                        doByAction(RoomStatusModule.CMD_MUR_ON);
                    }
                    setRoomStatus(RoomStatus.Clean, from);
                } else if (!request && roomStatus == RoomStatus.Clean) {
                    if (!from.equals("DB")) sendToDB(false, murApi);
                    if (doAction) {
                        doByAction(RoomStatusModule.CMD_MUR_OFF);
                    }
                    setRoomStatus(RoomStatus.None, from);
                }
                EventBus.getDefault().post(new RoomStatusStateChange(RoomStatusStateChange.SUCCESS));
            } catch (Exception e) {
                e.printStackTrace();
                EventBus.getDefault().post(new RoomStatusStateChange(RoomStatusStateChange.ERROR));
            }
        }
    }

    private final String errorMsg = "no any devices can be control";

    public boolean listenRCU;

    public void dontDisturb(boolean request, String from, boolean doAction) {
        android.util.Log.d(TAG, "from = " + from + ", dontDisturb: " + request + ", current Status = " + roomStatus);
        synchronized (mutex) {
            try {
                if (request && roomStatus != RoomStatus.DontDisturb) {
                    if (!from.equals("DB")) sendToDB(true, dndApi);
                    if (doAction) {
                        doByAction(RoomStatusModule.CMD_DND_ON);
                    }
                    setRoomStatus(RoomStatus.DontDisturb, from);
                } else if (!request && roomStatus == RoomStatus.DontDisturb) {
                    if (!from.equals("DB")) sendToDB(false, dndApi);
                    if (doAction) {
                        doByAction(RoomStatusModule.CMD_DND_OFF);
                    }
                    setRoomStatus(RoomStatus.None, from);
                }
                EventBus.getDefault().post(new RoomStatusStateChange(RoomStatusStateChange.SUCCESS));
            } catch (Exception e) {
                e.printStackTrace();
                EventBus.getDefault().post(new RoomStatusStateChange(RoomStatusStateChange.ERROR));
            }
        }
    }

    // ====================================================================

    // for push compatible
    public void requestClean(boolean request, boolean sendToPush, boolean sendToDB, String from) {
        android.util.Log.d(TAG, "from = " + from + ", requestClean: " + request + ", current Status = " + roomStatus);
        synchronized (mutex) {
            if (request && roomStatus != RoomStatus.Clean) {
                if (sendToDB) sendToDB(true, murApi);
                if (sendToPush)
                    sendCmdToPush(RoomStatusModule.CMD_MUR_ON, "mur_mode", "on");
                setRoomStatus(RoomStatus.Clean, from);
            } else if (!request && roomStatus == RoomStatus.Clean) {
                if (sendToDB) sendToDB(false, murApi);
                if (sendToPush)
                    sendCmdToPush(RoomStatusModule.CMD_MUR_OFF, "mur_mode", "off");
                setRoomStatus(RoomStatus.None, from);
            }
            EventBus.getDefault().post(new RoomStatusStateChange(RoomStatusStateChange.SUCCESS));
        }
    }

    public void dontDisturb(boolean request, boolean sendToPush, boolean sendToDB, String from) {
        android.util.Log.d(TAG, "from = " + from + ", dontDisturb: " + request + ", current Status = " + roomStatus);
        synchronized (mutex) {
            if (request && roomStatus != RoomStatus.DontDisturb) {
                if (sendToDB) sendToDB(true, dndApi);
                if (sendToPush)
                    sendCmdToPush(RoomStatusModule.CMD_DND_ON, "dnd_mode", "on");
                setRoomStatus(RoomStatus.DontDisturb, from);
            } else if (!request && roomStatus == RoomStatus.DontDisturb) {
                if (sendToDB) sendToDB(false, dndApi);
                if (sendToPush)
                    sendCmdToPush(RoomStatusModule.CMD_DND_OFF, "dnd_mode", "off");
                setRoomStatus(RoomStatus.None, from);
            }
            EventBus.getDefault().post(new RoomStatusStateChange(RoomStatusStateChange.SUCCESS));
        }
    }

    // ====================================================================

    public void pushCurrentRsToDB() {
        Log.d(TAG, "pushCurrentRsToDB");
        switch (roomStatus) {
            case Clean:
                sendToDB(true, murApi);
                break;
            case DontDisturb:
                sendToDB(true, dndApi);
                break;
            case None:
                sendToDB(false, murApi);
                sendToDB(false, dndApi);
        }
    }


    private void sendToDB(boolean request, APIConfig.WEBAPI api) {
        ChatApplication.worker.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    String url = api.getUrl();
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("hotelName", DeviceInfo.hotelName);
                    jsonObject.put("roomName", DeviceInfo.roomName);
                    jsonObject.put("isEnable", request);
                    String s = Http.put(url, jsonObject, 5000, api.getKey()).response;
                    JSONObject jObj = new JSONObject(s);
                    boolean isSucceeded = jObj.getBoolean("success");
                    Log.d(TAG, "sendToDB = " + isSucceeded);
                    if (!isSucceeded) throw new Exception("fail");
                } catch (Exception e) {
                    SpotDeviceLog.exception(TAG, "", "sendToDB:" + e.getMessage());
                    Log.e(TAG, "sendToDB error: " + e.toString());
                }
            }
        });
    }

    private void sendIntentToPush(String device, String action) {
        String intentName = "Dialogflow.IoT.RoomStatus";
        Map requestMap = new HashMap<>();
        requestMap.put("intent", intentName);
        requestMap.put("location", "");
        requestMap.put("device", device);
        requestMap.put("control", "set_switch");
        requestMap.put("value", action);
        JSONObject jb = JsonUtils.mapToJson(requestMap);
        IoTModuleManager.getInstance().push_iot_intent_to_remoteService(jb.toString(), intentName, null);
    }

    private void sendCmdToPush(String cmd, String device, String action) {
        Log.e(TAG, "sendCmdToPush = " + device + ", " + action);
        String intentName = "Dialogflow.IoT.RoomStatus";
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("intent", intentName);
            jsonObject.put("location", "");
            jsonObject.put("device", device);
            jsonObject.put("control", "set_switch");
            jsonObject.put("value", action);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            jsonObject.put("loop_codes", new JSONObject(cmd).getJSONArray("loop_codes"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        IoTModuleManager.getInstance().push_iot_intent_to_remoteService(jsonObject.toString(), intentName, null);
    }

    public enum RoomStatus {
        None, DontDisturb, Clean,
    }

    public enum CardStatus {
        None, KEYIN, KEYOUT
    }


    public void setRoomStatus(RoomStatus status, String from) {
        if (from.equals("DB") && listenRCU) {
            return;
        }
        if (from.equals("UI") || from.equals("VOICE")) {
            return;
        }
        Log.e(TAG, "setRoomStatus = " + status);
        roomStatus = status;
        stateRepository.put(StateRepository.KEY_ROOM_STATUS, status.name());
    }

    public void setCardStatus(CardStatus status) {
        try {
            if (status == CardStatus.KEYIN) {
                ServerDrivenHelper.ServerMessage serverMessage = ServerDrivenHelper.request(DeviceInfo.hotelName, DeviceInfo.roomType, "KYID", -1);
                ActionManager.getInstance().prepare(serverMessage.getResultAction(), true);
            } else if (status == CardStatus.KEYOUT) {
                ServerDrivenHelper.ServerMessage serverMessage = ServerDrivenHelper.request(DeviceInfo.hotelName, DeviceInfo.roomType, "CDO", -1);
                ActionManager.getInstance().prepare(serverMessage.getResultAction(), true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        cardStatus = status;
        stateRepository.put(StateRepository.KEY_CARD_STATUS, status.name());
    }

    private void clearRoomStatus(){
        stateRepository.put(StateRepository.KEY_ROOM_STATUS, RoomStatusModuleManager.RoomStatus.None.name());
    }

    //push current room status to db
    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onMessageEvent(SystemEvent event) {
        super.onMessageEvent(event);
        try {
            switch (event.getType()) {
                case WIFI_CONNECTED:
                    pushCurrentRsToDB();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onMessageEvent(ServerEvent event) {
        try {
            switch (event.getType()) {
                case DND:
                    JSONObject msg_dnd = new JSONObject(event.getData());
                    boolean action_dnd = msg_dnd.getJSONObject("DATA").getBoolean("ACTION");
                    // push compatible
                    if (SystemConfig.OTA.support_action) {
                        dontDisturb(action_dnd, "DB", true);
                    } else {
                        dontDisturb(action_dnd, true, false, "DB");
                    }
                    break;
                case MUR:
                    JSONObject msg_mur = new JSONObject(event.getData());
                    boolean action_mur = msg_mur.getJSONObject("DATA").getBoolean("ACTION");
                    // push compatible
                    if (SystemConfig.OTA.support_action) {
                        requestClean(action_mur, "DB", true);
                    } else {
                        requestClean(action_mur, true, false, "DB");
                    }
                    break;
                case CHECK_OUT:
                case APPREBOOT:
                case DEVICEREBOOT:
                    clearRoomStatus();
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static class RoomStatusModule {
        public static String CMD_DND_ON = "";
        public static String CMD_DND_OFF = "";
        public static String CMD_MUR_ON = "";
        public static String CMD_MUR_OFF = "";
        public static boolean supportDnd = false;
        public static boolean supportMur = false;

        static void setRoomStatusCmdCode(JSONObject jsonObject) {
            try {
                JSONObject dndCode = jsonObject.getJSONObject("service_data").getJSONObject("dnd_code");
                CMD_DND_ON = dndCode.getString("on");
                CMD_DND_OFF = dndCode.getString("off");
                supportDnd = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                JSONObject dndCode = jsonObject.getJSONObject("service_data").getJSONObject("mur_code");
                CMD_MUR_ON = dndCode.getString("on");
                CMD_MUR_OFF = dndCode.getString("off");
                supportMur = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
            android.util.Log.i("RoomStatusModule", String.format("setRoomStatusCmdCode: supportDnd=%s, supportMur=%s", supportDnd, supportMur));
        }
    }

}
