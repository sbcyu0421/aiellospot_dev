package ai.aiello.aiellospot.modules.showcase.radio

import ai.aiello.aiellospot.ChatApplication
import ai.aiello.aiellospot.events.system.SystemEvent
import ai.aiello.aiellospot.modules.AbstractModuleManager
import ai.aiello.aiellospot.modules.showcase.ShowcaseManager
import ai.aiello.aiellospot.views.activity.showcase.beans.RadioAlbumData
import ai.aiello.aiellospot.views.activity.showcase.beans.RadioCardViewBean
import ai.aiello.aiellospot.views.activity.showcase.beans.RadioTrackData
import android.content.Context
import android.media.AudioManager
import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import androidx.core.net.toUri
import androidx.lifecycle.MutableLiveData
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.ConcatenatingMediaSource
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

object HotelMusicManager : AbstractModuleManager() {

    val TAG: String = HotelMusicManager::class.java.simpleName

    var player: SimpleExoPlayer? = null
    private var updateProgressRunnable: UpdateProgressRunnable = UpdateProgressRunnable()
    private lateinit var dataSourceFactory: DefaultDataSourceFactory
    private var trackList: List<RadioTrackData>? = null
    var trackIndex = MutableLiveData<Int>(-1)
    var isPlaying = MutableLiveData(false)
    var trackProgress = MutableLiveData<Int>()
    var albumIndex = MutableLiveData(-1)
    lateinit var album : RadioAlbumData
    private var handlerThread: HandlerThread
    var mainHandler: Handler
    var handler: Handler? = null
    var currentPosition: Long = 0
    var contentDuration: Long = 0
    var progressUpdateRunnable = ProgressUpdateRunnable()

    private var holding: Boolean = false

    private var isVolDown = false

    enum class MusicState {PLAY, STOP, HOLD}
    var playStatus: MusicState = MusicState.STOP


    init {
        initDataSourceFactory()
        handlerThread = HandlerThread("HotelMusicHandler")
        handlerThread.start()
        handler = Handler(handlerThread.looper)
        mainHandler = Handler(Looper.getMainLooper())
    }


    // player control relative
    fun play() {
        if (!ShowcaseManager.getInstance().enable) return
        playStatus = MusicState.PLAY
        // send notification with eventbus
        startProcess()
    }

    fun pause() {
        if (!ShowcaseManager.getInstance().enable) return
        playStatus = MusicState.HOLD
        player?.playWhenReady = false
        isPlaying.postValue(false)
        stopProgressCallback()
    }

    fun stop() {
        playStatus = MusicState.STOP
        player?.playWhenReady = false
        isPlaying.postValue(false)
        stopProgressCallback()
    }

    fun pauseOrPlay() {
        if (isPlaying.value!!) {
            pause()
        } else {
            if (trackIndex.value == -1) {
                playMusic(0)
            } else {
                play()
            }
        }
    }

    fun playMusic(goToIndex: Int) {
        android.util.Log.d(TAG, "playMusic: $goToIndex")
        initPlayerAndMediaSource()
        try {
            trackList?.get(trackIndex.value!!)?.onSelect = false
        } catch (e: Exception) {
        }
        trackList?.get(goToIndex)?.onSelect = true
        trackIndex.postValue(goToIndex)
        player?.seekTo(goToIndex, C.TIME_UNSET)
        play()
    }

    fun jumpToProgress(int: Int) {
        requestAudioControl()
        initPlayerAndMediaSource()

        if(trackIndex.value == -1) {
            trackList?.get(0)?.onSelect = true
            trackIndex.postValue(0)
        }
        val progressMilli = player!!.contentDuration * int / 100
        player?.seekTo(progressMilli)
//        play()
//        player?.playWhenReady = true
        isPlaying.postValue(true)
        play()
    }


    // player and music source setup
    fun initPlayerAndMediaSource() {

        if (player == null) {

            mainHandler.post(object : Runnable {
                override fun run() {
                    player = SimpleExoPlayer
                        .Builder(ChatApplication.context)
                        .build()

                    initMediaSource()

                    player!!.addListener(object : Player.EventListener {

                        override fun onTracksChanged(
                            trackGroups: TrackGroupArray,
                            trackSelections: TrackSelectionArray
                        ) {
                            super.onTracksChanged(trackGroups, trackSelections)
                            val songIndex = player!!.currentWindowIndex
                            if (songIndex != 0) {
                                trackList?.get(songIndex)?.onSelect = true
                                trackList?.get(songIndex - 1)?.onSelect = false
                                trackIndex.postValue(songIndex)

                            }
                        }

                        override fun onPlayerStateChanged(
                            playWhenReady: Boolean,
                            playbackState: Int
                        ) {
                            super.onPlayerStateChanged(playWhenReady, playbackState)

                            if (playbackState == ExoPlayer.STATE_ENDED) {
                                playMusic(0)
                            }
                        }

                        override fun onPlayerError(error: ExoPlaybackException) {
                            super.onPlayerError(error)
                            player!!.release()
                            player = null
                        }
                    })
                }
            })
        }
    }

    fun shutdownPlayer() {
        isPlaying.value = player?.isPlaying
        stopProgressCallback()
        trackProgress.postValue(0)
        player?.release()
        player = null
        trackList?.get(trackIndex.value!!)?.onSelect = false
        trackIndex.postValue(-1)
    }

    fun setAlbum(inputAlbumIndex: Int) {

        albumIndex.postValue(inputAlbumIndex)

        if (trackList == null) {
            for ((_, v) in ShowcaseManager.getInstance().showcaseData) {
                if (v.feature_type == ShowcaseManager.FeatureType.SIMPLE_RADIO_VIEW.name) {
                    album = (v as RadioCardViewBean).radioAlbums[inputAlbumIndex]
                    trackList = album.tracks
                }
            }
        }
    }

    private fun initMediaSource() {
        val mediaSources = mutableListOf<MediaSource>()
        if (trackList != null) {
            for (track in trackList!!) {
                val mediaSource = ExtractorMediaSource.Factory(dataSourceFactory)
                    .createMediaSource((track.song_url).toUri())
                mediaSources.add(mediaSource)
            }
        }
        val concatenatingMediaSource = ConcatenatingMediaSource()
        concatenatingMediaSource.addMediaSources(mediaSources)
        player!!.prepare(concatenatingMediaSource)
    }

    private fun initDataSourceFactory() {
        dataSourceFactory =
            DefaultDataSourceFactory(
                ChatApplication.context,
                Util.getUserAgent(ChatApplication.context, "Exoplayer"),
                DefaultBandwidthMeter()
            )
    }

    var playerProgress= false
    var seekBarProgress= false
    var playENDED= false

    private fun startProcess() {
        requestAudioControl()
        player?.playWhenReady = true
        isPlaying.postValue(true)

        playerProgress= true
        seekBarProgress = true
        playENDED = false

        mainHandler.post(progressUpdateRunnable)
        handler!!.post(updateProgressRunnable)
    }

    private fun stopProgressCallback() {
        playerProgress = false
        seekBarProgress = false
    }

    class ProgressUpdateRunnable: Runnable {
        override fun run() {
            currentPosition = player?.currentPosition!!
            contentDuration = player?.contentDuration!!
            if (playerProgress)mainHandler.postDelayed(this, 1000)
        }
    }

    class UpdateProgressRunnable: Runnable {
        override fun run() {
            val p  = (((currentPosition.toFloat()).div(contentDuration.toFloat())).times(
                100
            )).toInt()
            trackProgress.postValue(p)
            if (seekBarProgress) handler?.postDelayed(this, 1000)
            if (playENDED) trackProgress.postValue(0)
        }
    }

    // pause other music service
    private fun requestAudioControl() {
        EventBus.getDefault().post(
            SystemEvent(SystemEvent.Type.MEDIA_OCCUPY, null, TAG)
        )
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    override fun onMessageEvent(event: SystemEvent) {
        super.onMessageEvent(event)
        when (event.type) {
            SystemEvent.Type.ALL_CHANNEL_STOP,
            SystemEvent.Type.TEMP_MEDIA_OCCUPY ->
                when (playStatus) {
                    MusicState.PLAY -> pause()
                    MusicState.HOLD -> stop()
                }
            SystemEvent.Type.TEMP_MEDIA_RELEASE ->
                if (playStatus == MusicState.HOLD) play()
            SystemEvent.Type.MEDIA_OCCUPY ->
                {
                    if (event.musicSource === TAG) return
                    when (playStatus) {
                        MusicState.PLAY,
                        MusicState.HOLD -> stop()
                    }
                }
        }

    }


    override fun create() {
    }

    override fun onStart() {
    }

    override fun onHotReload() {
    }


}