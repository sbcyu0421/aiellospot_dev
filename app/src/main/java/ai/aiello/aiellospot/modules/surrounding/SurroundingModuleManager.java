package ai.aiello.aiellospot.modules.surrounding;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.core.config.APIConfig;
import ai.aiello.aiellospot.core.config.Configer;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.modules.AbstractModuleManager;
import ai.aiello.aiellospot.utlis.Http;

public class SurroundingModuleManager extends AbstractModuleManager {

    private static String TAG = SurroundingModuleManager.class.getSimpleName();
    private static SurroundingModuleManager instance;

    private MainCategoryBean mainCategoryBean;
    private SubCategoryBean cuisineBean;
    private SubCategoryBean sightsBean;
    private SubCategoryBean transportBean;
    private SubCategoryBean lifeBean;

    public static final String CUISINE = "Cuisine";
    public static final String SIGHTS = "Sights";
    public static final String TRANSPORT = "Transport";
    public static final String BUILDING = "ServiceBuilding";

    public static APIConfig.WEBAPI typesApi = null;
    public static APIConfig.WEBAPI categoriesApi = null;
    public static APIConfig.WEBAPI itemsApi = null;
    private String hotelName;
    private String domainName;
    private String deviceUuid;

    private CountDownLatch latch;
    private ExecutorService surroundingWorker = Executors.newFixedThreadPool(4);
    public static boolean surroundingDataFetching = false;

    public static SurroundingModuleManager getInstance() {
        if (instance == null) {
            instance = new SurroundingModuleManager(DeviceInfo.hotelName, DeviceInfo.domain, DeviceInfo.uuid);
            Log.d(TAG, "DeviceInfo.hotelName = " + DeviceInfo.hotelName + ",  DeviceInfo.domain = " + DeviceInfo.domain);
        }
        return instance;
    }

    private SurroundingModuleManager(String hotelName, String domainName, String hotelUuid) {
        this.hotelName = hotelName;
        this.domainName = domainName;
        this.deviceUuid = hotelUuid;
        android.util.Log.d(TAG, "initialized");
    }


    @Override
    public void create() {
        //setup data
        try {
            typesApi = APIConfig.WEBAPI.build(Configer.moduleConfigData.getJSONObject("surrounding")
                    .getJSONObject("service_data").getJSONObject("api_types"), Configer.useProductAPI);
            categoriesApi = APIConfig.WEBAPI.build(Configer.moduleConfigData.getJSONObject("surrounding")
                    .getJSONObject("service_data").getJSONObject("api_categories"), Configer.useProductAPI);
            itemsApi = APIConfig.WEBAPI.build(Configer.moduleConfigData.getJSONObject("surrounding")
                    .getJSONObject("service_data").getJSONObject("api_items_v2"), Configer.useProductAPI);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private MainCategoryBean getMainCategory() {
        MainCategoryBean mainCategoryBean = null;
        int counter = 0;
        String response = "";
        do {
            try {
                JSONObject jsonBody = new JSONObject();
                jsonBody.put("hotel_name", hotelName);
                jsonBody.put("domain_name", domainName);
                response = Http.post(typesApi.getUrl(), jsonBody, 5000, typesApi.getKey()).response;
                Log.d(TAG, "getMainCategory = " + response);
                counter++;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } while (response.isEmpty() && counter < 5);

        try {
            JSONObject jsonObject = new JSONObject(response);
            int c = jsonObject.getInt(CUISINE);
            int s = jsonObject.getInt(SIGHTS);
            int t = jsonObject.getInt(TRANSPORT);
//            int t = 0;
            int b = jsonObject.getInt(BUILDING);
            mainCategoryBean = new MainCategoryBean(c, s, t, b);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mainCategoryBean;
    }

    private String getSubCategory(String type) {
        int counter = 0;
        String response = "";
        do {
            try {
                JSONObject jsonBody = new JSONObject();
                jsonBody.put("hotel_name", hotelName);
                jsonBody.put("domain_name", domainName);
                jsonBody.put("type", type);
                response = Http.post(categoriesApi.getUrl(), jsonBody, 5000, categoriesApi.getKey()).response;
                Log.d(TAG, "getSubCategory = " + response);
                counter++;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } while (response.isEmpty() && counter < 5);
        return response;
    }

    /**
     * @param sentence           org_sentence, ex How to get to the sports center
     * @param translate_sentence translate_sentence, ex. 運動中心怎麼去
     * @param language
     * @param actionBy     ０：UI 觸發, 1:語音觸發
     * @return Items of Store
     * @throws Exception
     */
    public Items getSurroundingItems(String sentence, String translate_sentence, String language, int actionBy) throws Exception {
        int counter = 0;
        String response = "";
        Items items = null;
        Http.HttpRes res = null;
        String queryText = "?hotel=%s&device=%s&language=%s&sentence=%s&actionBy=%s&translate_sentence=%s";
        do {
            try {
                res = Http.getByQuery(String.format(itemsApi.getUrl() + queryText, hotelName, deviceUuid, language, sentence, actionBy, translate_sentence), 5000, itemsApi.getKey());
                response = res.response;
                Log.d(TAG, "getSurroundingItems = " + response);
                counter++;
            } catch (Exception e) {
                Thread.sleep(1000);
                Log.e(TAG, e.toString());
            }
        } while (response.isEmpty() && counter < 2);

        if (res.exception instanceof SocketTimeoutException) {
            throw res.exception;
        }

        //資料後處理 : images, and distance, and sorting by recommand and distance
        JSONObject jsonObject = new JSONObject(response);
        if (jsonObject.getJSONArray("items").length() > 0) { // value of items will be [] if no value
            Gson gson = new Gson();
            items = gson.fromJson(response, Items.class);
            TreeSet<Integer> recommendScore = new TreeSet<>();
            for (Items.ItemsBean tmp : items.getItems()) {
                //set images
                if (tmp.getDetail().getImages() == null || tmp.getDetail().getImages().size() == 0) {
                    switch (items.getType()) {
                        case SurroundingModuleManager.CUISINE:
                            tmp.getDetail().getImages().add(SubCategoryBean.getCuisineHolderImage());
                            break;
                        case SurroundingModuleManager.SIGHTS:
                            tmp.getDetail().getImages().add(SubCategoryBean.getSightsImage());
                            break;
                        case SurroundingModuleManager.TRANSPORT:
                            tmp.getDetail().getImages().add(SubCategoryBean.getTransportHolderImage());
                            break;
                        case SurroundingModuleManager.BUILDING:
                            tmp.getDetail().getImages().add(SubCategoryBean.getBuildingsImage());
                            break;
                    }
                }
                //set distance (with new geo data format, v2)
//                String[] targetGeo = tmp.getDetail().getGeo().split(",");
//                float targetLat = tmp.getDetail().getGeo().getLat();
//                float targetLng = tmp.getDetail().getGeo().getLng();
//                if (String.valueOf((int) targetLat).equals("0") && String.valueOf((int) targetLng).equals("0")) {
//                    tmp.getDetail().setDistance(0f);
//                } else {
//                    float distance = (float) getDistance(targetLat, targetLng, DeviceInfo.geo_lat, DeviceInfo.geo_lon);
//                    tmp.getDetail().setDistance(distance);
//                }


                // record the recommand score
                recommendScore.add(tmp.getDetail().getRecommand());

                //set opening time
                if (tmp.getDetail().getOpeningHoursType().equals("Plaintext")) {
                    String openingHours = (String) tmp.getDetail().getOpeningHours();
                    tmp.getDetail().setOpeningHours(openingHours);
                }
            }
            List<Items.ItemsBean> sortedList = sortByCriteria(items, recommendScore);
            items.setItems(sortedList);
            //debug use
            for (Items.ItemsBean tmp : items.getItems()) {
                Log.d(TAG, tmp.getDetail().getName() + ", distance = " + tmp.getDetail().getDistance() + ", recommend = " + tmp.getDetail().getRecommand());
            }


        }
        return items;
    }

    private List<Items.ItemsBean> sortByCriteria(Items items, TreeSet<Integer> recommendScore) throws Exception {

        List<Items.ItemsBean> sortedList = new ArrayList<>(items.getItems());

//        //1. 先依距離排序
//        List<Items.ItemsBean> distanceList = new ArrayList<>(items.getItems());
//        Comparator<Items.ItemsBean> distanceComparator = new Comparator<Items.ItemsBean>() {
//            @Override
//            public int compare(Items.ItemsBean o1, Items.ItemsBean o2) {
//                int c = Math.round(10000 * (o1.getDetail().getDistance() - o2.getDetail().getDistance()));
//                return c;
//            }
//        };
//        //2. 按星等分群
//        Collections.sort(distanceList, distanceComparator);
//        HashMap<Integer, ArrayList<Items.ItemsBean>> itemMap = new HashMap<>();
//        SortedSet<Integer> deRecommandScore = recommendScore.descendingSet();
//        for (Integer score: deRecommandScore) {
//            itemMap.put(score, new ArrayList<>());
//        }
//
//        for (int i = 0; i < distanceList.size(); i++) {
//            itemMap.get(distanceList.get(i).getDetail().getRecommand()).add(distanceList.get(i));
//        }
//
//        //3. 合併產出最後排序
//        List<Items.ItemsBean> sortedList = new ArrayList<>();
//        for (Integer integer : deRecommandScore) {
//            try {
//                sortedList.addAll(itemMap.get(integer));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }

        // 4.加上dummies(懶人包)
        try {
            if (items.getDummies().getName() == null)
                throw new Exception("items.getDummies is null");
//            if (DeviceInfo.accuRedirectUrl.isEmpty())
//                throw new Exception("AccuRedirectUrl is empty");
            Items.ItemsBean itemsBean = new Items.ItemsBean();
            Items.ItemsBean.DetailBean detailBean = new Items.ItemsBean.DetailBean();
            detailBean.setName(items.getDummies().getName());
            detailBean.setQrcode(items.getDummies().getQrcode());
            detailBean.setImages(items.getDummies().getImages());
            itemsBean.setDetail(detailBean);
            sortedList.add(Math.max(items.getDummies().getIndex(), 0), itemsBean);
        } catch (Exception e) {
            Log.e(TAG, "skip dummies, cause " + e.toString());
//            e.printStackTrace();
        }

        return sortedList;
    }


    public MainCategoryBean getMainCategoryBean() {
        return mainCategoryBean;
    }

    public SubCategoryBean getCuisineBean() {
        return cuisineBean;
    }

    public SubCategoryBean getSightsBean() {
        return sightsBean;
    }

    public SubCategoryBean getTransportBean() {
        return transportBean;
    }

    public SubCategoryBean getLifeBean() {
        return lifeBean;
    }

    public void fetchData(){

        if (surroundingDataFetching) {
            Log.d(TAG, "data is fetching now, ignoring UI input temporary");
            return;
        }

        surroundingDataFetching = true;
        Log.d(TAG, "Start fetching data process, start ignoring UI input");

        surroundingWorker.submit(new Runnable() {
            @Override
            public void run() {

                latch = new CountDownLatch(4);

                if (mainCategoryBean == null) {
                    mainCategoryBean = getMainCategory();
                }

                if (mainCategoryBean != null) {

                    try {
                        surroundingWorker.submit(new Runnable() {
                            @Override
                            public void run() {
                                if (mainCategoryBean.getCuisine() > 0 && cuisineBean == null) {
                                    cuisineBean = SubCategoryBean.build(getSubCategory(CUISINE));
                                }
                                latch.countDown();
                            }
                        });

                        surroundingWorker.submit(new Runnable() {
                            @Override
                            public void run() {
                                if (mainCategoryBean.getSights() > 0 && sightsBean == null) {
                                    sightsBean = SubCategoryBean.build(getSubCategory(SIGHTS));
                                }
                                latch.countDown();
                            }
                        });

                        surroundingWorker.submit(new Runnable() {
                            @Override
                            public void run() {
                                if (mainCategoryBean.getTransport() > 0 && transportBean == null) {
                                    transportBean = SubCategoryBean.build(getSubCategory(TRANSPORT));
                                }
                                latch.countDown();
                            }
                        });

                        surroundingWorker.submit(new Runnable() {
                            @Override
                            public void run() {
                                if (mainCategoryBean.getServiceBuilding() > 0 && lifeBean == null) {
                                    lifeBean = SubCategoryBean.build(getSubCategory(BUILDING));
                                }
                                latch.countDown();
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        latch.await(60, TimeUnit.SECONDS);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                surroundingDataFetching = false;
                Log.d(TAG, "Done fetching data process, start accepting UI input");
            }
        });
    }

    @Override
    public void onStart() {
        fetchData();
    }

    @Override
    public void onHotReload() {
        fetchData();
    }


    public interface APICallback {
        void onSuccess(String successMsg);

        void onFail(String errorMsg);
    }


    private String currentType;
    private Items currentItems;
    private int currentBeanIndex;

    public void setCurrentItems(Items currentItems) {
        this.currentItems = currentItems;
    }

    public Items getCurrentItems() {
        return currentItems;
    }

    public String getCurrentType() {
        return currentType;
    }

    public void setCurrentType(String currentType) {
        this.currentType = currentType;
    }

    public int getCurrentBeanIndex() {
        return currentBeanIndex;
    }

    public void setCurrentBeanIndex(int currentBeanIndex) {
        this.currentBeanIndex = currentBeanIndex;
    }


    private double getDistance(double lat1, double lng1, double lat2, double lng2) {
        double s = 0.0;
        try {
            double EARTH_RADIUS = 6378137;
            double radLat1 = rad(lat1);
            double radLat2 = rad(lat2);
            double a = radLat1 - radLat2;
            double b = rad(lng1) - rad(lng2);
            s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2)
                    + Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)));
            s = s * EARTH_RADIUS;
            s = Math.round(s * 10000) / 10000.0 / 1000.0;
            BigDecimal bg = new BigDecimal(s).setScale(2, BigDecimal.ROUND_HALF_UP);
            s = bg.doubleValue();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return s;
    }

    private double rad(double d) {
        return d * Math.PI / 180.0;
    }

}
