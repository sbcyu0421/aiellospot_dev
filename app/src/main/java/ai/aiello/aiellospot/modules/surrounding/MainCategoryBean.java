package ai.aiello.aiellospot.modules.surrounding;

public class MainCategoryBean {

    public MainCategoryBean(int cuisine, int sights, int transport, int serviceBuilding) {
        Cuisine = cuisine;
        Sights = sights;
        Transport = transport;
        ServiceBuilding = serviceBuilding;
    }

    /**
     * Cuisine : 100
     * Sights : 267
     * Transport : 100
     * ServiceBuilding : 389
     */

    private int Cuisine;
    private int Sights;
    private int Transport;
    private int ServiceBuilding;

    public int getCuisine() {
        return Cuisine;
    }

    public int getSights() {
        return Sights;
    }

    public int getTransport() {
        return Transport;
    }

    public int getServiceBuilding() {
        return ServiceBuilding;
    }


}
