package ai.aiello.aiellospot.modules.showcase;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.HandlerThread;

import com.litesuits.android.log.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.core.config.APIConfig;
import ai.aiello.aiellospot.core.config.Configer;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.modules.AbstractModuleManager;
import ai.aiello.aiellospot.utlis.Http;
import ai.aiello.aiellospot.views.activity.showcase.beans.HotelFeatureBean;
import ai.aiello.aiellospot.views.activity.showcase.beans.RadioCardViewBean;
import ai.aiello.aiellospot.views.activity.showcase.beans.SimpleCardViewBean;

public class ShowcaseManager extends AbstractModuleManager {
    public static String TAG = ShowcaseManager.class.getSimpleName();

    private static ShowcaseManager instance = null;
    public static APIConfig.WEBAPI configFileApi;
    private Bitmap image;
    private String imageUrl;
    private JSONObject nameWithLocale;

    public String showcaseRawData;
    public HashMap<String, HotelFeatureBean> showcaseData = new HashMap<>();

    public boolean fetchingShowcaseData = false;
    public boolean enable = false;

    private ExecutorService worker = Executors.newSingleThreadExecutor();

    private HandlerThread handlerThread;

    public enum LayoutName { // matches layout name from config data
        showcase,
    }

    public enum  FeatureType {
        SIMPLE_CARD_VIEW,
        SIMPLE_RADIO_VIEW
    }


    public static ShowcaseManager getInstance() {
        if (instance == null) {
            instance = new ShowcaseManager();
        }
        return instance;
    }

    public ShowcaseManager() {
        android.util.Log.d(TAG, "initialized");
    }

    @Override
    public void create() {
        initData();
    }

    @Override
    public void onStart() {
    }

    @Override
    public void onHotReload() {
        initData();
    }

    public void initData() {
        // setup data
        try {
            enable = Configer.moduleConfigData.getJSONObject("showcase").getBoolean("enable");

            JSONObject serviceData = Configer.moduleConfigData.getJSONObject("showcase").getJSONObject("service_data");

            configFileApi = APIConfig.WEBAPI.build(
                    "showcase_config",
                    serviceData.getJSONObject("api_dataset"),
                    Configer.useProductAPI);
            imageUrl = serviceData.getString("btn_image");
            nameWithLocale = serviceData.getJSONObject("btn_name");
//            image = downloadImage(imageUrl, 10);
        } catch (Exception e) {
            e.printStackTrace();
        }

        worker.submit(new mRunnable());
    }

    class mRunnable implements Runnable {

        @Override
        public void run() {
            fetchShowcaseData();
            android.util.Log.d(TAG, "run: done initializing");
        }
    }

    public void fetchShowcaseData() {

        fetchingShowcaseData = true;
        showcaseData = new HashMap<>();
        int retry = 0;
        boolean success = false;
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("hotel", DeviceInfo.hotelName);
            jsonBody.put("moduleName", "HotelFeature");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        while (!success && retry < 10) {
            try {
                if (retry != 0) Thread.sleep(200);
                Http.HttpRes httpRes = Http.post(configFileApi.getUrl(), jsonBody, 60 * 1000, configFileApi.getKey());
                if (httpRes.responseCode == 200) {
                    success = true;
                    showcaseRawData = httpRes.response;
                    android.util.Log.d(TAG, "fetchShowcaseData: " + showcaseRawData);
                }
                retry++;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (showcaseRawData != null) {
            try {

                HashMap<String, HotelFeatureBean> showcaseDataMap = new HashMap<>();

                JSONArray dataList = new JSONObject(showcaseRawData).getJSONArray("result");

                for (int i = 0; i < dataList.length(); i++) {
                    Thread.sleep(1000);
                    String data = dataList.getJSONObject(i).toString();
                    HotelFeatureBean hotelFeatureBean = hotelFeatureClassifyFactory(data);
                    if (hotelFeatureBean != null) {
                        showcaseDataMap.put(hotelFeatureBean.getUnikey(), hotelFeatureBean);
                    }
                    if (i == dataList.length()-1) {
                        showcaseData = showcaseDataMap;
                    }
                }
            } catch (Exception e) {
                showcaseData = new HashMap<>();
                android.util.Log.d(TAG, "fetchShowcaseData: fail to parse Showcase data to HotelFeatureBean");
                e.printStackTrace();
            }
        }
        fetchingShowcaseData = false;
    }

    private HotelFeatureBean hotelFeatureClassifyFactory(String rawData) throws JSONException {
        HotelFeatureBean hotelFeatureBean = null;

        JSONObject data = new JSONObject(rawData);
        FeatureType type = FeatureType.valueOf(data.getString("feature_type"));

        switch (type) {
            case SIMPLE_CARD_VIEW : {
                try {
                    hotelFeatureBean = new SimpleCardViewBean(
                            data.getString("feature_type"),
                            data.getString("unikey"),
                            data.getString("title"),
                            data.getString("data")
                    );
                    Log.d( TAG,"hotelFeatureClassifyFactory: hotelFeatureBean = " + hotelFeatureBean.getUnikey() );
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            break;

            case SIMPLE_RADIO_VIEW : {
                try {
                    hotelFeatureBean = new RadioCardViewBean(
                            data.getString("feature_type"),
                            data.getString("unikey"),
                            data.getString("title"),
                            data.getString("data")
                    );
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            break;

            default:
                break;
        }

        return hotelFeatureBean;
    }

    private Bitmap downloadImage(String url, int tryCount) {
        int count = 0;
        do {
            try {
                URL urlConnection = new URL(url);
                HttpURLConnection connection = (HttpURLConnection) urlConnection.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                return BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                count ++;
                e.printStackTrace();
                if (count < tryCount) {
                    Log.d(TAG, "downloadImage fail, retry " + count);
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException interruptedException) {
                        interruptedException.printStackTrace();
                    }
                }
                if (count == tryCount) Log.d(TAG, "downloadImage fail, end trying");
            }

        } while (count < 5);
        return null;
    }

    public String getNameByLocale() {
        String string = null;
        if (nameWithLocale != null) {
            try {
                switch (ChatApplication.system_lang) {
                    case en_US:
                        string = nameWithLocale.getString("en");
                        break;
                    case zh_TW:
                        string = nameWithLocale.getString("tw");
                        break;
                    case zh_CN:
                        string = nameWithLocale.getString("cn");
                        break;
                    case japanese:
                        string = nameWithLocale.getString("ja");
                        break;
                    default:
                        string = "飯店服務";
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return string;
    }

    public Bitmap getImage() {return image;}

    public String getImageUrl() {
        return imageUrl;
    }
}
