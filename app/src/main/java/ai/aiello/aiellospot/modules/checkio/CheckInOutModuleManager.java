package ai.aiello.aiellospot.modules.checkio;

import androidx.annotation.Nullable;

import com.litesuits.android.log.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.core.config.APIConfig;
import ai.aiello.aiellospot.core.config.Configer;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.core.info.UserInfo;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.system.SystemControl;
import ai.aiello.aiellospot.events.module.CheckInOutStateChange;
import ai.aiello.aiellospot.events.server.ServerEvent;
import ai.aiello.aiellospot.modules.AbstractModuleManager;
import ai.aiello.aiellospot.utlis.Http;

public class CheckInOutModuleManager extends AbstractModuleManager {

    private static final String TAG = CheckInOutModuleManager.class.getSimpleName();
    public Date checkInOutTime;

    private static CheckInOutModuleManager instance = null;
    public static APIConfig.WEBAPI checkout = null;
    public static APIConfig.WEBAPI checkoutWithInvoice = null;

    public static CheckInOutModuleManager getInstance() {
        if (instance == null) {
            instance = new CheckInOutModuleManager();
        }
        return instance;
    }

    public enum LayoutName { // matches layout name from config data
        Checkio,
    }

    private CheckInOutModuleManager() {
        android.util.Log.d(TAG, "initialized");
    }

    @Override
    public void create() {
        try {
            checkout = APIConfig.WEBAPI.build(Configer.moduleConfigData.getJSONObject("checkio")
                    .getJSONObject("service_data").getJSONObject("checkout_action"), Configer.useProductAPI);
            checkoutWithInvoice = APIConfig.WEBAPI.build(Configer.moduleConfigData.getJSONObject("checkio")
                    .getJSONObject("service_data").getJSONObject("checkout_invoice"), Configer.useProductAPI);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onHotReload() {
        try {
            checkout = APIConfig.WEBAPI.build(Configer.moduleConfigData.getJSONObject("checkio")
                    .getJSONObject("service_data").getJSONObject("checkout_action"), Configer.useProductAPI);
            checkoutWithInvoice = APIConfig.WEBAPI.build(Configer.moduleConfigData.getJSONObject("checkio")
                    .getJSONObject("service_data").getJSONObject("checkout_invoice"), Configer.useProductAPI);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public boolean checkOut() {
        Log.d(TAG, "checkOut");
        boolean success = false;
        try {
            JSONObject coObj = new JSONObject();
            coObj.put("hotelName",DeviceInfo.hotelName);
            coObj.put("roomNames",new JSONArray().put(DeviceInfo.roomName));
            coObj.put("username","system");
            coObj.put("fullname","system");
            int count = 0, maxTry = 5;
            while (!success && count < maxTry) {
                Http.HttpRes httpRes = Http.put(checkout.getUrl(), coObj, 5000, checkout.getKey());
                if (httpRes.responseCode / 100 == 2) success = true;
                else {
                    // retry
                    Thread.sleep(200);
                    count++;
                }
            }
        } catch (Exception e) {
            SpotDeviceLog.exception(TAG, "", "sendCheckOut" + e.getMessage());
            Log.e(TAG, "checkOut error: " + e.toString());
        }
        if (success) {
            EventBus.getDefault().post(new ServerEvent(ServerEvent.Type.DEVICEREBOOT));
            SystemControl.rebootDevice(ChatApplication.context, SystemControl.StageAction.CLEAR);
        }
        return success;
    }

    public void checkIn(JSONObject jsonObject) {
        Log.d(TAG, "checkIn");
        try {
            JSONObject jObj = jsonObject.getJSONObject("DATA");
            UserInfo.userName = jObj.getString("NAME");
            UserInfo.userGender = jObj.getString("GENDER");
            checkInOutTime = Calendar.getInstance().getTime();

            try {
                String tmp = jObj.getString("NATIONALITY");
                if (tmp.equals("")) {
                    Log.e(TAG, "no user nationality");
                } else {
                    UserInfo.userNationality = tmp;
                }
            } catch (Exception e) {
                Log.e(TAG, e.toString());
                SpotDeviceLog.exception(TAG, "", e.getMessage());
            }

            try {
                String tmp = jObj.getString("LANGUAGE");
                if (tmp.equals("")) {
                    Log.e(TAG, "no user language");
                } else {
                    UserInfo.userLanguage = tmp;
                }
            } catch (Exception e) {
                Log.e(TAG, e.toString());
                SpotDeviceLog.exception(TAG, "", e.getMessage());
            }

            Log.d(TAG, "checkIn name = " + UserInfo.userName +
                    " / checkIn gender = " + UserInfo.userGender +
                    " / checkIn language = " + UserInfo.userLanguage +
                    " / checkIn nationality = " + UserInfo.userNationality);

        } catch (JSONException e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }

    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onMessageEvent(ServerEvent event) {
        try {
            switch (event.getType()) {
                case CHECK_IN:
                    checkIn(new JSONObject(event.getData()));
                    EventBus.getDefault().post(new CheckInOutStateChange(CheckInOutStateChange.Target.ENTRY_HOME_PAGE, event.getData()));
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
