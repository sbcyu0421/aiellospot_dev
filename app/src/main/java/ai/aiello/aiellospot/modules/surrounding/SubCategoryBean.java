package ai.aiello.aiellospot.modules.surrounding;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;

import ai.aiello.chinese.ChineseUtils;

public class SubCategoryBean {

    private HashMap<String, SubTypes> titleSet = new HashMap<>();

    private static String TAG = SubCategoryBean.class.getSimpleName();


    private static int transportHolderId = 0;
    private static int buildingHolderId = 0;
    private static int cuisineHolderId = 0;
    private static int sightsHolderId = 0;

    static String transport = "http://cloud.aiello.ai:8021/spot/assets/PlaceImageHolder/transport_%s.jpg";
    static String cuisine = "http://cloud.aiello.ai:8021/spot/assets/PlaceImageHolder/cuisine_%s.jpg";
    static String building = "http://cloud.aiello.ai:8021/spot/assets/PlaceImageHolder/building_%s.jpg";
    static String sights = "http://cloud.aiello.ai:8021/spot/assets/PlaceImageHolder/sights_%s.jpg";

    public static String getTransportHolderImage() {
        transportHolderId++;
        int c = transportHolderId % 4 + 1;
        return String.format(transport, c);
    }

    public static String getCuisineHolderImage() {
        cuisineHolderId++;
        int c = cuisineHolderId % 4 + 1;
        return String.format(cuisine, c);
    }

    public static String getBuildingsImage() {
        buildingHolderId++;
        int c = buildingHolderId % 4 + 1;
        return String.format(building, c);
    }

    public static String getSightsImage() {
        sightsHolderId++;
        int c = sightsHolderId % 4 + 1;
        return String.format(sights, c);
    }


    public SubCategoryBean(String data) {
        try {
            JSONObject jsonObject = new JSONObject(data);
            for (int i = 0; i < jsonObject.names().length(); i++) {
                try {
                    String key = jsonObject.names().getString(i);
                    JSONObject attr = jsonObject.getJSONObject(jsonObject.names().getString(i));
                    SubTypes tmp = new SubTypes(
                            attr.getString("name_tw"),
                            attr.getString("name_tw"),
                            attr.getString("name_en"),
                            attr.getString("name_ja"),
                            attr.getInt("count"));
                    titleSet.put(key, tmp);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static SubCategoryBean build(String data) {
        return new SubCategoryBean(data);
    }


    public HashMap<String, SubTypes> getTitleSet() {
        return titleSet;
    }


    public static class SubTypes implements Serializable {

        private String name_tw;
        private String name_cn;
        private String name_en;
        private String name_ja;
        private int capacity;

        SubTypes(String name_tw, String name_cn, String name_en, String name_ja, int capacity) {
            this.name_tw = name_tw;
            this.name_cn = ChineseUtils.toSimplified(name_tw, false);
            this.name_en = name_en;
            this.name_ja = name_ja;
            this.capacity = capacity;
        }

        public int getCapacity() {
            return capacity;
        }

        public String getName(Locale locale) {
            if (locale.equals(Locale.JAPANESE)) {
                return name_ja;
            } else if (locale.equals(Locale.CHINA)) {
                return name_cn;
            } else if (locale.equals(Locale.TAIWAN)) {
                return name_tw;
            } else {
                return name_en;
            }
        }

        //workaround for ja
        public String getQuery(Locale locale){
            if (locale.equals(Locale.JAPANESE)) {
                return name_en;
            } else if (locale.equals(Locale.CHINA)) {
                return name_cn;
            } else if (locale.equals(Locale.TAIWAN)) {
                return name_tw;
            } else {
                return name_en;
            }
        }
    }

}
