package ai.aiello.aiellospot.modules.music;

import org.apache.commons.lang.StringEscapeUtils;

public class AlbumObject {

    private String id;
    private String title;
    private String description;
    private String img;
    private String url;

    public AlbumObject(String id, String title, String description, String img, String url) {
        this.id = id;
        this.title = StringEscapeUtils.unescapeJava(title);
        this.description = StringEscapeUtils.unescapeJava(description);
        this.img = StringEscapeUtils.unescapeJava(img);
        this.url = StringEscapeUtils.unescapeJava(url);
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getImg() {
        return img;
    }

    public String getUrl() {
        return url;
    }


}
