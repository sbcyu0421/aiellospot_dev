package ai.aiello.aiellospot.modules;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.events.system.SystemEvent;

public abstract class AbstractModuleManager {

    /**
     * Set to "true" for the idle state activity/module manager -> will "hot reload at busy state"
     */
    public boolean isIdleModule = false;

    public AbstractModuleManager() {
        if (EventBus.getDefault().isRegistered(this)) return;
        EventBus.getDefault().register(this);
    }

    public abstract void create();

    public abstract void onStart();

    /**
     * fire function while hot reload event be triggered
     */
    public abstract void onHotReload();

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onMessageEvent(SystemEvent event) {
        switch (event.getType()) {
            case MODULE_START:
                onStart();
                break;
            case BUSY_STATE_HOT_RELOAD:
                if (isIdleModule) onHotReload();
                break;
            case IDLE_STATE_HOT_RELOAD:
                if(!isIdleModule) onHotReload();
                break;
        }
    }

}
