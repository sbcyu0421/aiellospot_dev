package ai.aiello.aiellospot.modules.alarm;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.litesuits.android.log.Log;
import com.litesuits.common.data.DataKeeper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.core.MCountdownTimer;
import ai.aiello.aiellospot.core.SystemAlarmReceiver;
import ai.aiello.aiellospot.core.device.DeviceControl;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.media.AielloSoundPool;
import ai.aiello.aiellospot.events.module.AlarmStateChange;
import ai.aiello.aiellospot.events.server.ServerEvent;
import ai.aiello.aiellospot.events.system.SystemEvent;
import ai.aiello.aiellospot.modules.AbstractModuleManager;
import ai.aiello.aiellospot.utlis.Formatter;

public class UserAlarmModuleManager extends AbstractModuleManager {

    private static final String TAG = UserAlarmModuleManager.class.getSimpleName();
    private static android.app.AlarmManager manager;
    public final int alarmLimit = 20;
    private static int alarmId = -1;
    public static String alarm_msg = "";
    private static Date moduleInitialTime;
    public static final String USER_ALARM = "USER_ALARM";

    private static Map<String, Date> alarmMap;

    public enum LayoutName { // matches layout name from config data
        Alarm,
    }

    private static UserAlarmModuleManager instance = null;

    public static UserAlarmModuleManager getInstance() {
        if (instance == null) {
            instance = new UserAlarmModuleManager();
        }
        return instance;
    }

    private UserAlarmModuleManager() {
        android.util.Log.d(TAG, "initialized");
    }

    @Override
    public void create() {
        moduleInitialTime = new Date();
    }

    @Override
    public void onStart() {
        Formatter.ISO8601DATEFORMAT.setTimeZone(TimeZone.getTimeZone(DeviceInfo.timeZone));
        android.util.Log.d(TAG, "Set config TimeZone to alarm formatter with: " + DeviceInfo.timeZone);
        android.util.Log.d(TAG, "AielloLog, device TimeZone: " + TimeZone.getDefault());
        manager = (android.app.AlarmManager) ChatApplication.context.getSystemService(Context.ALARM_SERVICE);
        alarmMap = new HashMap<>();
        ArrayList<Date> list = loadFromPreference(ChatApplication.context);
        if (list.size() > 0) {
            for (Date date : list) {
                alarmMap.put(Formatter.ISO8601DATEFORMAT.format(date), date);
            }
        }
        for (Map.Entry<String, Date> entry : alarmMap.entrySet()) {
            addAlarmToManager(entry.getValue(), ChatApplication.context);
        }
        Log.d(TAG, "UserAlarmMap.size() = " + alarmMap.size());
    }

    @Override
    public void onHotReload() {
        Formatter.ISO8601DATEFORMAT.setTimeZone(TimeZone.getTimeZone(DeviceInfo.timeZone));
        android.util.Log.d(TAG, "Set config TimeZone to alarm formatter with: " + DeviceInfo.timeZone);
        android.util.Log.d(TAG, "AielloLog, device TimeZone: " + TimeZone.getDefault());
    }

    public Map<String, Date> getAlarmMap() {
        return alarmMap;
    }

    public void clearUserAlarmData() {
        android.util.Log.d(TAG, "clearUserAlarmData");
        removeAllAlarm();
        saveToPreference(ChatApplication.context);
    }


    public boolean addAlarm(Date date) {
        boolean response = false;
        if (alarmMap.size() < alarmLimit) {
            alarmMap.put(Formatter.ISO8601DATEFORMAT.format(date), date);
            addAlarmToManager(date, ChatApplication.context);
            response = true;
        }
        Log.d(TAG, "addAlarm : " + Formatter.ISO8601DATEFORMAT.format(date) + " is " + response);
        return response;
    }


    public void removeAllAlarm() {
        if (alarmMap == null) {
            return;
        }
        for (String timeKey : alarmMap.keySet()) {
            cancelAlarmFromManager(alarmMap.get(timeKey), ChatApplication.context);
        }
        alarmMap.clear();
        Log.d(TAG, "removeAllAlarm");
    }

    public int getFreeAlarm() {
        return alarmLimit - alarmMap.size();
    }

    public boolean removeOneAlarm(Date date, Context context) {
        Log.d(TAG, "removeOneAlarm = " + date.toString());
        Date rm = alarmMap.remove(Formatter.ISO8601DATEFORMAT.format(date));
        cancelAlarmFromManager(date, context);
        return rm != null;
    }

    public boolean removeAlarmByPeriod(Date startDate, Date endDate, Context context) {
        Log.d(TAG, "removeAlarmByPeriod = " + startDate.toString());
        ArrayList<Date> candidate = new ArrayList<>();
        for (Map.Entry<String, Date> entry : alarmMap.entrySet()) {
            Date tmp = entry.getValue();
            if (tmp.after(startDate) && tmp.before(endDate)) {
                candidate.add(tmp);
            } else if (startDate.getTime() == endDate.getTime() && tmp.getTime() == startDate.getTime()) {
                candidate.add(tmp);
            }
        }
        for (Date tmp : candidate) {
            Date rm = alarmMap.remove(Formatter.ISO8601DATEFORMAT.format(tmp));
            cancelAlarmFromManager(rm, context);
        }
        return candidate.size() > 0;
    }


    public boolean isAlarmExistByTime(Date date) {
        return alarmMap.containsKey(Formatter.ISO8601DATEFORMAT.format(date));
    }

    public ArrayList<Date> isAlarmExistByPeriod(@Nullable Date startTime, @Nullable Date endTime) {
        Log.d(TAG, "isAlarmExistByPeriod start = " + Formatter.ISO8601DATEFORMAT.format(startTime) + ", end = " + Formatter.ISO8601DATEFORMAT.format(endTime));
        ArrayList<Date> list = new ArrayList<>();
        for (String timeKey : alarmMap.keySet()) {
            try {
                if (startTime == null || endTime == null) {
                    list.add(Formatter.ISO8601DATEFORMAT.parse(timeKey));
                } else {
                    if (Formatter.ISO8601DATEFORMAT.parse(timeKey).after(startTime) && Formatter.ISO8601DATEFORMAT.parse(timeKey).before(endTime)) {
                        list.add(Formatter.ISO8601DATEFORMAT.parse(timeKey));
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return list;
    }


    public void addAlarmToManager(Date date, Context context) {
        int requestCode = Integer.valueOf(String.valueOf(date.getTime()).substring(0, 9));
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        Intent intent = new Intent(context, SystemAlarmReceiver.class);
        intent.setAction(USER_ALARM);
        intent.putExtra(USER_ALARM, Formatter.ISO8601DATEFORMAT.format(date));
        PendingIntent sender = PendingIntent.getBroadcast(context, requestCode, intent, PendingIntent.FLAG_ONE_SHOT);
        android.app.AlarmManager manager = (android.app.AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        manager.set(android.app.AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), sender);
        Log.d(TAG, "addAlarmToManager, requestCode = " + requestCode + ", value = " + calendar.getTimeInMillis());
    }


    public void cancelAlarmFromManager(Date date, Context context) {
        int requestCode = Integer.valueOf(String.valueOf(date.getTime()).substring(0, 9));
        Log.d(TAG, "cancelAlarmFromManager, requestCode = " + requestCode);
        Intent intent = new Intent(context, SystemAlarmReceiver.class);
        intent.setAction(USER_ALARM);
        intent.putExtra(USER_ALARM, Formatter.ISO8601DATEFORMAT.format(date));
        PendingIntent sender = PendingIntent.getBroadcast(context, requestCode, intent, PendingIntent.FLAG_ONE_SHOT);
        manager.cancel(sender);
    }


    public void saveToPreference(Context context) {
        if (alarmMap == null) {
            return;
        }
        DataKeeper dataKeeper = new DataKeeper(context, "user_alarm");
        dataKeeper.put("alarm_list", null); // clear data
        StringBuilder sb = new StringBuilder();
        if (alarmMap.size() > 0) {
            int i = 0;
            for (String key : alarmMap.keySet()) {
                i++;
                sb.append(key);
                if (i == alarmMap.size())
                    break;
                sb.append(",");
            }
        }
        dataKeeper.put("alarm_list", sb.toString());
        Log.d(TAG, "save alarm to preference = " + sb.toString());

    }

    public ArrayList<Date> loadFromPreference(Context context) {
        ArrayList<Date> alarmList = new ArrayList<>();
        DataKeeper dataKeeper = new DataKeeper(context, "user_alarm");
        String alarm_list_string = (String) dataKeeper.get("alarm_list", "");
        Log.d(TAG, "load from preference, user alarm : " + alarm_list_string);
        String[] alarmListArray = alarm_list_string.split(",");
        for (String s : alarmListArray) {
            Date date = null;
            try {
                date = Formatter.ISO8601DATEFORMAT.parse(s);
                alarmList.add(date);
            } catch (ParseException e) {
                //Log.e(TAG, "date covert error = " + e.toString());
            }
        }
        return alarmList;
    }


    private View mView = null;
    private WindowManager mWindowManager = null;
    private Boolean isShown = false;
    private String sysMsg = "";

    public void showAlarmPopup(String msg) {
        msg = msg + "的鬧鈴已經逾時，請點擊關閉";

        if (!sysMsg.equals(msg)) {
            hideAlarmPopup(2);
            // 获取WindowManager
            mWindowManager = (WindowManager) ChatApplication.context.getSystemService(Context.WINDOW_SERVICE);
            mView = setAlarmPopupView(ChatApplication.context, msg);

            final WindowManager.LayoutParams params = new WindowManager.LayoutParams();

            // 类型
            params.type = WindowManager.LayoutParams.TYPE_PHONE;
            params.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL; // FLAG_NOT_TOUCH_MODAL不阻塞事件传递到后面的窗口
            params.format = PixelFormat.TRANSLUCENT;

            params.width = WindowManager.LayoutParams.WRAP_CONTENT;
            params.height = WindowManager.LayoutParams.WRAP_CONTENT;
            params.gravity = Gravity.TOP;

            mWindowManager.addView(mView, params);

            sysMsg = msg;
            isShown = true;
            android.util.Log.i(TAG, "show Alarm popup");

        }
    }


    /**
     * 隐藏弹出框
     */
    public void hideAlarmPopup(int from) {
        android.util.Log.i(TAG, "hide " + isShown + ", " + mView);
        if (isShown && mView != null) {
            android.util.Log.i(TAG, "hide alarm popup");
            mWindowManager.removeView(mView);
            isShown = false;
            mView = null;

            if (from == 1 && isAlarmRing()) {
                stopRing("stop alarm ring from popup click");
            }
        }

    }

    private View setAlarmPopupView(final Context context, String msg) {

        View view = LayoutInflater.from(context).inflate(R.layout.popupwindow, null);
        TextView global_msg = view.findViewById(R.id.global_msg);
        LinearLayout global_msg_block = view.findViewById(R.id.global_msg_block);
        global_msg.setText(msg);
        global_msg_block.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                hideAlarmPopup(1);

            }
        });

        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN;
        view.setSystemUiVisibility(uiOptions);

        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                hideAlarmPopup(1);
                return false;
            }
        });


        Animation amTranslate = new TranslateAnimation(0.0f, 0.0f, -50.0f, 10.0f);
        amTranslate.setDuration(500);
        amTranslate.setRepeatCount(0);
        amTranslate.setFillAfter(true);
        global_msg_block.startAnimation(amTranslate);//开始动画

        return view;

    }


    private static final Object mutex = new Object();
    private Thread ringTimer = null;

    public void stopRing(String message) {
        synchronized (mutex) {
            AielloSoundPool.stopAlarm(UserAlarmModuleManager.alarmId);
            UserAlarmModuleManager.alarmId = -1;
            if (ringTimer != null) {
                ringTimer.interrupt();
                ringTimer = null;
            }
            Log.d(TAG, message);
            EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.TEMP_MEDIA_RELEASE));
        }
    }

    public void startRing() {
        synchronized (mutex) {
            //pause music
            //stop old if exist
            AielloSoundPool.stopAlarm(UserAlarmModuleManager.alarmId);
            UserAlarmModuleManager.alarmId = -1;
            if (ringTimer != null) {
                ringTimer.interrupt();
                ringTimer = null;
            }
            UserAlarmModuleManager.alarmId = AielloSoundPool.playDefaultVolSound(AielloSoundPool.morning_call);
            EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.TEMP_MEDIA_OCCUPY));
        }

        ringTimer = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3 * 60 * 1000);
//                    Thread.sleep(10 * 1000);
                    EventBus.getDefault().post(new AlarmStateChange(AlarmStateChange.Event.ALARM_TIMEOUT));
                    stopRing("stop ring from auto pass");
                } catch (InterruptedException e) {
                    Log.e(TAG, "ringTimer force stop");
                }
            }
        });
        ringTimer.start();

    }


    public boolean isAlarmRing() {
        boolean is = false;
        if (UserAlarmModuleManager.alarmId != -1) {
            is = true;
        }
        return is;
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onMessageEvent(SystemEvent event) {
        super.onMessageEvent(event);
        switch (event.getType()) {
            case ALARM_RECEIVER:
                Intent intent = event.getIntent();
                if (USER_ALARM.equals(intent.getAction())) {
                    try {
                        String key = intent.getStringExtra(UserAlarmModuleManager.USER_ALARM);
                        Date date = Formatter.ISO8601DATEFORMAT.parse(key);
                        boolean success = removeOneAlarm(date, ChatApplication.context);
                        if (success) {
                            UserAlarmModuleManager.getInstance().saveToPreference(ChatApplication.context);
                        }
                        DeviceControl.backLightOn();
                        MCountdownTimer.stopTimer();
                        alarm_msg = Formatter.binaryFormat(date.getHours()) + " : " + Formatter.binaryFormat(date.getMinutes());
                        if (date.before(moduleInitialTime)) {
                            android.util.Log.d(TAG, "Skip expired alarm");
                        }

                        if (date.after(moduleInitialTime)) {
                            EventBus.getDefault().post(new AlarmStateChange(AlarmStateChange.Event.ALARM_RISE));
                            startRing();
                        }
                    } catch (Exception e) {
                        SpotDeviceLog.exception(TAG, "", e.getMessage());
                        Log.e(TAG, e.toString());
                    }
                }
                break;
        }
    }


    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onMessageEvent(ServerEvent event) {
        android.util.Log.d(TAG, "onMessageEvent: "+ event.getType());
        try {
            switch (event.getType()) {
                case APPREBOOT:
                case DEVICEREBOOT:
                case CHECK_OUT:
                    clearUserAlarmData();
                break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
