package ai.aiello.aiellospot.modules.iot;

import ai.aiello.aiellospot.core.config.Configer;
import ai.aiello.aiellospot.core.config.SystemConfig;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.messager.MessageManager;
import ai.aiello.aiellospot.core.messager.message.MassageCode;
import ai.aiello.aiellospot.core.messager.message.MessageFactory;
import ai.aiello.aiellospot.modules.roomstatus.RoomStatusModuleManager;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.litesuits.android.log.Log;

import ai.aiello.aiellospot.core.info.DeviceInfo;
import io.reactivex.annotations.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class IoTConn implements ServiceConnection {
    private static Messenger messenger;
    private static Messenger get_reply_messenger;

    //to pushService
    static final int TX_INIT_CONNECTION = 101;
    private static final int TX_CHANGE_DATABASE = 102;
    private static final int TX_IOTControl = 103;
    private static final int RX_IOTUIControl = 1035;
    private static final int TX_IOT_RCU_ACTION = 1036;

    private static final int TX_SHUTDOWN = 104;
    private static final int TX_CHECKOUT = 105;
    private static final int TX_CHECKIN = 106;

    private static final int TX_IOT_TEST_START = 955;
    private static final int TX_IOT_TEST_STOP = 956;

    private static final int TX_IOT_KEYIN = 400;
    private static final int TX_IOT_KEYOUT = 401;
    private static final int TX_IOT_DNDON = 402;
    private static final int TX_IOT_DNDOFF = 403;
    private static final int TX_IOT_CLEANON = 404;
    private static final int TX_IOT_CLEANOFF = 405;
    private static final int TX_IOT_STATUS = 406;

    private static final int TX_IOT_Bed = 407;
    private static final int TX_IOT_AC_STATUS = 408;
    private static final int TX_IOT_LISTEN_RS = 499;

    public static final int RX_CHECK_IN = 411;
    public static final int RX_CHECK_OUT = 412;

    public static final int RX_SYNC_DEVICES_TO_SPOT = 413;
    private IoTConnCallback callback;


    private static String KEY_BUNDLE = "data";
    private HandlerThread handlerThread;

    public static String TAG = IoTConn.class.getSimpleName();
    private static boolean isBound = false;
    private final Gson gson = new Gson();
    private Context context;

    IoTConn(Context ctx, IoTConnCallback callback) {
        context = ctx;
        this.callback = callback;
        if (handlerThread == null) {
            handlerThread = new HandlerThread("PushServiceConn Worker");
            handlerThread.start();
        }
        get_reply_messenger = new Messenger(new Handler(handlerThread.getLooper()) {
            @Override
            public void handleMessage(Message msg) {
                Log.d(TAG, "get_message, msg.what = " + msg.what);
                switch (msg.what) {
                    case TX_IOT_KEYIN:
                        RoomStatusModuleManager.getInstance().setCardStatus(RoomStatusModuleManager.CardStatus.KEYIN);
                        break;

                    case TX_IOT_KEYOUT:
                        RoomStatusModuleManager.getInstance().setCardStatus(RoomStatusModuleManager.CardStatus.KEYOUT);
                        break;

                    case TX_IOT_DNDON:
                        // push compatible
                        if (SystemConfig.OTA.support_action) {
                            RoomStatusModuleManager.getInstance().dontDisturb(true, "RCU", false);
                        } else {
                            RoomStatusModuleManager.getInstance().dontDisturb(true, false, true, "RCU");
                        }
                        Log.d(TAG, "TX_IOT_DNDON");
                        break;

                    case TX_IOT_DNDOFF:
                        // push compatible
                        if (SystemConfig.OTA.support_action) {
                            RoomStatusModuleManager.getInstance().dontDisturb(false,  "RCU", false);
                        } else {
                            RoomStatusModuleManager.getInstance().dontDisturb(false, false, true, "RCU");
                        }
                        Log.d(TAG, "TX_IOT_DNDOFF");
                        break;

                    case TX_IOT_CLEANON:
                        // push compatible
                        if (SystemConfig.OTA.support_action) {
                            RoomStatusModuleManager.getInstance().requestClean(true, "RCU", false);
                        } else {
                            RoomStatusModuleManager.getInstance().requestClean(true, false, true, "RCU");
                        }
                        Log.d(TAG, "TX_IOT_CLEANON");
                        break;

                    case TX_IOT_CLEANOFF:
                        // push compatible
                        if (SystemConfig.OTA.support_action) {
                            RoomStatusModuleManager.getInstance().requestClean(false, "RCU", false);
                        } else {
                            RoomStatusModuleManager.getInstance().requestClean(false, false, true, "RCU");
                        }
                        Log.d(TAG, "TX_IOT_CLEANOFF");
                        break;

                    case TX_IOT_STATUS:
                        int iotStatus = msg.getData().getInt(KEY_BUNDLE);
                        if (iotStatus == 50) {
                            Log.d(TAG, "client receive, iot works");
                        } else {
                            Log.e(TAG, "client receive, iot dont work");
                        }
                        break;
                    case TX_IOT_Bed:
                        int bedResp = Integer.valueOf(msg.getData().getString(KEY_BUNDLE));
                        if (listener != null) {
                            listener.onIOTDone(bedResp);
                        } else {
                            Log.e(TAG, "listener = null");
                        }
                        break;

                    case TX_IOT_AC_STATUS:
                        String ac_status = msg.getData().getString(KEY_BUNDLE);
                        callback.onAcDataUpdate(ac_status);
                        break;

                    case TX_IOT_LISTEN_RS:
                        RoomStatusModuleManager.getInstance().listenRCU = Boolean.parseBoolean(msg.getData().getString(KEY_BUNDLE));
                        break;

                    case RX_CHECK_IN:
                        try {
                            JSONObject dataObj = new JSONObject();
                            dataObj.put("ROOM", DeviceInfo.roomName);
                            dataObj.put("UUID", DeviceInfo.uuid);
                            try {
                                // if push has more info from 3rd rcu
                                String guest_status = msg.getData().getString(KEY_BUNDLE);
                                dataObj.put("GUEST", new JSONObject(guest_status));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            MessageManager.getInstance().send(MessageManager.ProducerType.MANAGEMENT, MessageFactory.create3rdMsg(MassageCode.THIRD_PARTY_CHECK_IN, dataObj));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;

                    case RX_CHECK_OUT:
                        try {
                            JSONObject dataObj = new JSONObject();
                            dataObj.put("ROOM", DeviceInfo.roomName);
                            dataObj.put("UUID", DeviceInfo.uuid);
                            dataObj.put("DEVICE", DeviceInfo.MAC);
                            MessageManager.getInstance().send(MessageManager.ProducerType.MANAGEMENT, MessageFactory.create3rdMsg(MassageCode.THIRD_PARTY_CHECK_OUT, dataObj));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case RX_SYNC_DEVICES_TO_SPOT:
                        try {
                            Bundle data = msg.getData();
                            JSONObject jsonData = new JSONObject(data.getString("data"));
                            JSONArray jsonIotDevices = jsonData.getJSONArray("devices");
//                            android.util.Log.d(TAG, "IotDevicesUpdate received: data =" + jsonIotDevices.toString());
                            for (int i = 0; i < jsonIotDevices.length(); i++) {
                                try {
                                    JSONObject jsonDeviceData = jsonIotDevices.getJSONObject(i);
                                    IoTModuleManager.updateIotDeviceList(jsonDeviceData);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

//                        android.util.Log.d(TAG, "IotDeviceList: " + gson.toJson( IoTModuleManager.iotDeviceInfo ));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        break;

                    default:
                        Log.e(TAG, "get undefined message");
                        break;
                }
            }
        });
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        Log.d(TAG, "onServiceConnected");
        messenger = new Messenger(service);
        isBound = true;
        initService();
    }


    @Override
    public void onServiceDisconnected(ComponentName name) {
        Log.e(TAG, "onServiceDisconnected");
        messenger = null;
        isBound = false;
        bindService();
    }

    public boolean getConnectionState() {
        return isBound;
    }

    private IoTModuleManager.IotDoneListener listener;

    private void initService() {
        Message m = Message.obtain();
        Log.e(TAG, "PushService started");
        m.what = TX_INIT_CONNECTION;
        Bundle _bundle = new Bundle();
        _bundle.putString("mac", DeviceInfo.MAC);
        m.setData(_bundle);
        m.replyTo = get_reply_messenger;
        try {
            messenger.send(m);
        } catch (RemoteException e) {
            Log.e(TAG, "Connection Fail:" + e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }
    }

    public boolean deliveryIntent_IOT(String intent_json, String intent, @Nullable IoTModuleManager.IotDoneListener listener) {
        this.listener = listener;
        Message msg = Message.obtain();
        msg.what = TX_IOTControl;
        Bundle _bundle = new Bundle();
        _bundle.putString("intent", intent_json);
        _bundle.putString("IntentName", intent);
        msg.setData(_bundle);
        try {
            messenger.send(msg);
            Log.d(TAG, "IOT messenger send, " + msg.toString());
        } catch (RemoteException e) {
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e("Intent Delivery", "Error on Messenger Send:" + e.getMessage());
            e.printStackTrace();
            _bundle.clear();
            _bundle = null;
            msg.recycle();
            msg = null;

            return false;
        }
        return true;
    }

    public void deliveryRCUAction(String actions) {
        Message msg = Message.obtain();
        msg.what = TX_IOT_RCU_ACTION;
        Bundle _bundle = new Bundle();
        _bundle.putString("actions", actions);
        msg.setData(_bundle);
        try {
            messenger.send(msg);
            Log.d(TAG, "IOT messenger send RCUAction, " + msg.toString());
        } catch (RemoteException e) {
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e("RCUAction", "Error on Messenger Send:" + e.getMessage());
            e.printStackTrace();
            _bundle.clear();
            _bundle = null;
            msg.recycle();
            msg = null;
        }
    }


    boolean delivery_IOTUIControl(String iot_ui_control) {
        Message msg = Message.obtain();
        msg.what = RX_IOTUIControl;
        Bundle _bundle = new Bundle();
        _bundle.putString("iot_ui_control", iot_ui_control);
        msg.setData(_bundle);
        try {
            messenger.send(msg);
        } catch (RemoteException e) {
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e(TAG, e.toString());
            _bundle.clear();
            _bundle = null;
            msg.recycle();
            msg = null;
        }
        return true;
    }


    void bindService() {
        Intent intent = new Intent();
        intent.putExtra("mac", DeviceInfo.MAC);
        intent.putExtra("signalR", SystemConfig.aielloHub);
        intent.putExtra("iotService", SystemConfig.iotHub);
        intent.putExtra("hotelId", DeviceInfo.hotelID);
        intent.putExtra("hotelName", DeviceInfo.hotelName);
        intent.putExtra("roomName", DeviceInfo.roomName);
        intent.putExtra("uuid", DeviceInfo.uuid);
        intent.putExtra(Configer.KEY_CONFIG, Configer.getClientServiceConfig());
        intent.setAction("service.pushservice");
        intent.setPackage("com.aiello.pushservicebackground");
        context.bindService(intent, this, Context.BIND_AUTO_CREATE);
    }

    boolean shutdownService() {
        Message msg = Message.obtain();
        msg.what = TX_SHUTDOWN;
        try {
            messenger.send(msg);
        } catch (RemoteException e) {
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e(TAG, e.toString());
        }
        return true;
    }


    void checkin() {
        Message msg = Message.obtain();
        msg.what = TX_CHECKIN;
        try {
            messenger.send(msg);
        } catch (RemoteException e) {
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e(TAG, e.toString());
        }
    }

    void checkout() {
        Message msg = Message.obtain();
        msg.what = TX_CHECKOUT;
        try {
            messenger.send(msg);
        } catch (RemoteException e) {
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e(TAG, e.toString());
        }
    }


    void startIoTTest() {
        Message msg = Message.obtain();
        msg.what = TX_IOT_TEST_START;
        try {
            messenger.send(msg);
        } catch (RemoteException e) {
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e(TAG, e.toString());
        }
    }

    void stopIoTTest() {
        Message msg = Message.obtain();
        msg.what = TX_IOT_TEST_STOP;
        try {
            messenger.send(msg);
        } catch (RemoteException e) {
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e(TAG, e.toString());
        }
    }

}
