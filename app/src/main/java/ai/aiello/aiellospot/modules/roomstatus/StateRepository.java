package ai.aiello.aiellospot.modules.roomstatus;

import android.content.Context;

import com.litesuits.common.data.DataKeeper;

public class StateRepository extends DataKeeper{

    public StateRepository(Context context) {
        super(context, "spot_state");
    }

    public static final String KEY_ROOM_STATUS = "KEY_ROOM_STATUS";
    public static final String KEY_CARD_STATUS = "KEY_CARD_STATUS";

}
