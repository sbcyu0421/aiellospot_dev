package ai.aiello.aiellospot.modules.ads;

public enum AdsGroup {
    Self, Partner, Trip
}
