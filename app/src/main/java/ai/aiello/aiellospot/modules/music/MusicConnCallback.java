package ai.aiello.aiellospot.modules.music;

public interface MusicConnCallback {
    void onGetAlbumsCompleted();

    void onLoading();

    void onPlay();

    void onPause();

    void onError();

    void onTrackInfoLoaded();
}
