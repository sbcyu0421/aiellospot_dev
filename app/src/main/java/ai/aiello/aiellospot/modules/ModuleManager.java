package ai.aiello.aiellospot.modules;

import ai.aiello.aiellospot.modules.ads.AdsModuleManager;
import ai.aiello.aiellospot.modules.alarm.UserAlarmModuleManager;
import ai.aiello.aiellospot.modules.checkio.CheckInOutModuleManager;
import ai.aiello.aiellospot.modules.iot.IoTModuleManager;
import ai.aiello.aiellospot.modules.music.MusicServiceModuleManager;
import ai.aiello.aiellospot.modules.notification.NotificationModuleManager;
import ai.aiello.aiellospot.modules.roomstatus.RoomStatusModuleManager;
import ai.aiello.aiellospot.modules.showcase.ShowcaseManager;
import ai.aiello.aiellospot.modules.surrounding.SurroundingModuleManager;
import ai.aiello.aiellospot.modules.taxi.TaxiModuleManager;
import ai.aiello.aiellospot.modules.voip.VoipModuleManager;

public class ModuleManager {

    public static final String TAG = ModuleManager.class.getSimpleName();

    public static boolean createModules() {
        try {
            // built-in module
            AdsModuleManager.getInstance().create();
            NotificationModuleManager.getInstance().create();
            // option module
            UserAlarmModuleManager.getInstance().create();
            CheckInOutModuleManager.getInstance().create();
            IoTModuleManager.getInstance().create();
            MusicServiceModuleManager.getInstance().create();
            RoomStatusModuleManager.getInstance().create();
            SurroundingModuleManager.getInstance().create();
            VoipModuleManager.getInstance().create();
            TaxiModuleManager.getInstance().create();
            ShowcaseManager.getInstance().create();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}