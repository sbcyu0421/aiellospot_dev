package ai.aiello.aiellospot.modules.notification;

import android.content.Context;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by a1990 on 2019/5/15.
 */

public class NotifyObject implements Serializable {

    public static final String INTENT = "NotifyObject";
    private String intentName;
    private String title;
    private String content;
    private Date date;
    private boolean isRead;
    private String taskID;

    NotifyObject(String intentName, String titleId, String content, Date date) {
        this.title = titleId;
        this.content = content;
        this.date = date;
        this.intentName = intentName;
        this.isRead = false;
    }

    NotifyObject(String intentName, String titleId, String content, Date date, String taskID) {
        this.title = titleId;
        this.content = content;
        this.date = date;
        this.intentName = intentName;
        this.isRead = false;
        this.taskID = taskID;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public Date getDate() {
        return date;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public String getIntentName() {
        return intentName;
    }

    public String getTaskID() {
        return taskID;
    }
}
