package ai.aiello.aiellospot.core.ota;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageInstaller;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.litesuits.common.io.IOUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.core.config.APIConfig;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.utlis.AielloFileUtils;
import ai.aiello.aiellospot.utlis.Http;
import ai.aiello.aiellospot.views.activity.new_ota.VersionCheckerT;
import ai.aiello.aiellospot.views.activity.ota.OTAProcessActivity;

public class APKUpdateService {

    private static final String TAG = APKUpdateService.class.getSimpleName();

    public static void forceOTA() throws Exception {
        String url = APIConfig.WEBAPI_SPOT_NOTE.getUrl();
        String key = APIConfig.WEBAPI_SPOT_NOTE.getKey();
        String version = VersionCheckerT.getLocalVersion(ChatApplication.context, VersionCheckerT.SPOT_NAME);
        String res = Http.get(String.format(url, DeviceInfo.hotelID, version.contains("beta") ? "release" : "test"), 5000, key).response;
        JSONObject jsonObject = new JSONObject(res);
        JSONArray apk_array = jsonObject.getJSONArray("data");
        JSONObject obj2 = apk_array.getJSONObject(apk_array.length() - 1);
        Log.d(TAG, "forceOTA -> " + obj2.toString());
        String versionName = obj2.getString("versionName");
        String note = obj2.getString("note");
        String releaseDate = obj2.getString("releaseDate");
        String apkUrl = obj2.getString("url");
        ApkItem apkItem = new ApkItem(versionName, note, releaseDate, apkUrl, 1, 0);
        DownloadAsync downloadAsync = new DownloadAsync();
        downloadAsync.execute(ChatApplication.context, apkItem.getUrl());

    }

    public static class DownloadAsync extends AsyncTask<Object, Integer, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            if (success) {
                Intent intent = new Intent(ChatApplication.context, OTAProcessActivity.class);
                ChatApplication.context.startActivity(intent);
            } else {
                Toast.makeText(ChatApplication.context, "下載錯誤，請重新嘗試", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected Boolean doInBackground(Object... objects) {
            boolean success = false;
            com.litesuits.android.log.Log.d(TAG, "downloading...");
            String urlStr = (String) objects[1];
            com.litesuits.android.log.Log.d(TAG, urlStr);
            InputStream in = null;
            FileOutputStream out = null;
            try {
                URL url = new URL(urlStr);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");
                urlConnection.setDoOutput(false);
                urlConnection.setConnectTimeout(10 * 1000);
                urlConnection.setReadTimeout(10 * 1000);
                urlConnection.setRequestProperty("Connection", "Keep-Alive");
                urlConnection.setRequestProperty("Charset", "UTF-8");
                urlConnection.setRequestProperty("Accept-Encoding", "gzip, deflate");
                urlConnection.setRequestProperty(APIConfig.KEY_SUB_LABEL, APIConfig.WEBAPI_OTA.getKey());

                urlConnection.connect();
                long bytetotal = urlConnection.getContentLength();
                long bytesum = 0;
                int byteread = 0;
                in = urlConnection.getInputStream();

                File dir = AielloFileUtils.getCacheDirectory(ChatApplication.context);
                String apkName = urlStr.substring(urlStr.lastIndexOf("/") + 1, urlStr.length());
                File apkFile = new File(dir, apkName);
                out = new FileOutputStream(apkFile);
                byte[] buffer = new byte[10 * 1024];
                int progress = 0;
                while ((byteread = in.read(buffer)) != -1) {
                    bytesum += byteread;
                    out.write(buffer, 0, byteread);
                    progress = (int) (bytesum * 100L / bytetotal);
                    com.litesuits.android.log.Log.d(TAG, "download progress = " + progress);
                    publishProgress(progress);
                }
                // 下载完成
                if (bytesum == bytetotal) {
                    installAPkSilently(ChatApplication.context, apkFile, 0);
                    success = true;
                }


            } catch (Exception e) {
                SpotDeviceLog.exception(TAG,"",e.getMessage());
                com.litesuits.android.log.Log.e(TAG, "download apk file error:" + e.getMessage());
            } finally {
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException ignored) {

                    }
                }
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException ignored) {

                    }
                }
            }

            return success;
        }
    }

    public static void installAPkSilently(Context context, File apkFile, int type) {

        String packageName = "";

        switch (type) {
            case 0:
                packageName = VersionCheckerT.SPOT_NAME;
                break;
            case 1:
                packageName = VersionCheckerT.MUSIC_SERVICE_NAME;
                break;
            case 2:
                packageName = VersionCheckerT.PUSH_SERVICE_NAME;
                break;
            case 3:
                packageName = VersionCheckerT.SAI_SERVICE_NAME;
                break;
        }

        com.litesuits.android.log.Log.e(TAG, "ready to install package, name = " + packageName);

        // httpGet PackageInstaller from PackageManager
        PackageInstaller packageInstaller = context.getPackageManager().getPackageInstaller();
        // Prepare params for installing one APK file with MODE_FULL_INSTALL
        PackageInstaller.SessionParams params = new PackageInstaller.SessionParams(PackageInstaller.SessionParams.MODE_FULL_INSTALL);
        params.setAppPackageName(packageName);

        // Create PackageInstaller.Session for performing the actual update
        try {
            int sessionID = packageInstaller.createSession(params);
            PackageInstaller.Session session = packageInstaller.openSession(sessionID);

            // Copy APK file bytes into OutputStream provided by install Session
            OutputStream out = session.openWrite(packageName, 0, -1);
            FileInputStream fis = new FileInputStream(apkFile);
            IOUtils.copy(fis, out);
            fis.close();
            out.close();

            com.litesuits.android.log.Log.d(TAG, "apk install type = " + type);
            if (type == 0) {

                PendingIntent pendingIntent = PendingIntent.getBroadcast(
                        context,
                        sessionID,
                        new Intent(),
                        0);
                IntentSender intentSender = pendingIntent.getIntentSender();
                session.commit(intentSender);

            } else {
                Intent i = new Intent("SERVICE_INSTALL_COMPLETE");
                i.putExtra("service_name", packageName);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(
                        context,
                        sessionID,
                        i,
                        0);

                IntentSender intentSender = pendingIntent.getIntentSender();
                session.commit(intentSender);
            }

        } catch (Exception e) {
            com.litesuits.android.log.Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG,"",e.getMessage());
        }

    }
}
