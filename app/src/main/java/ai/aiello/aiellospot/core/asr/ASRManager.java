package ai.aiello.aiellospot.core.asr;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.core.config.SystemConfig;
import ai.aiello.aiellospot.core.info.DeviceInfo;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.system.Pinger;

import android.content.Context;
import android.os.Message;
import android.os.Process;

import com.litesuits.android.log.Log;
import com.aiello.localasr.ASRClientService;
import com.aiello.localasr.AielloInterfaceListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * init soundAI lib
 * consume data from emitQueue to ensure feed data sequentially
 * listener can pass wakeup event, saiClient,  from sai to application
 * Created by a1990 on 2019/5/17.
 */

public class ASRManager implements AielloInterfaceListener {

    private static String TAG = ASRManager.class.getSimpleName();
    private ExecutorService executor;
    private static ASRManager instance;
    private SocketManagerListener socketManagerListener;
    private ASRClientService msrv;
    public static boolean onEmit = false;
    public static boolean onASR = false;
    public static Map<String, String> googleLangToAccentMap = new HashMap<>();

    public void setOnASR(boolean active) {
        synchronized (this) {
            if (onASR != active) {
                onASR = active;
                Log.d(TAG, "onASR = " + active);
            }
        }
    }

    public boolean getOnASR() {
        synchronized (this) {
            return onASR;
        }
    }


    public interface SocketManagerListener {
        void onAsrResponse(Message message);

        void onCancel();
    }


    public void setOnEmitListener(SocketManagerListener listener) {
        socketManagerListener = listener;
    }

    public static ASRManager getInstance() {
        if (instance == null) {
            instance = new ASRManager();
        }
        return instance;
    }


    @Override
    public void onPartialASRResult(String part_result) {
        try {
            JSONObject json = new JSONObject(part_result);
            String updated_text = json.getString("interim");
            ArrayList msg = new ArrayList();
            msg.add("interim");
            msg.add(updated_text);
            Message message = Message.obtain();
            message.obj = msg;
            if (socketManagerListener != null) {
                socketManagerListener.onAsrResponse(message);
            }
        } catch (Exception ex) {
            SpotDeviceLog.exception("ASRManager", "", ex.getMessage());
            Log.e("ASRManager", ex.getMessage());
        }
    }

    @Override
    public void onFinalResult(String final_result) {

        if (final_result.isEmpty()) {
            if (socketManagerListener != null) {
                socketManagerListener.onCancel();
            }
        } else {
            try {
                JSONObject final_obj = new JSONObject(final_result);
                String sen_final = final_obj.getString("final");
                String mi_encoded = final_obj.getString("mi");
                JSONObject mi_wrap = new JSONObject(mi_encoded);
                String mi = mi_wrap.getString("mi");
                ArrayList msg = new ArrayList();
                msg.add("final");
                msg.add(sen_final);
                msg.add(mi);
                Message message = Message.obtain();
                message.obj = msg;
                if (socketManagerListener != null) {
                    socketManagerListener.onAsrResponse(message);
                }
            } catch (Exception ex) {
                SpotDeviceLog.exception("ASRManager", "", ex.getMessage());
                Log.e("ASRManager", ex.toString());
            }
        }

    }

    @Override
    public void onError(String error_msg) {

    }


    @Override
    public void onCancel() {
        if (socketManagerListener != null) {
            socketManagerListener.onCancel();
        }
    }

    public String getASRModel() {
        return msrv.getCurrentModel();
    }

    public int switchASRModel(){
        return msrv.switchASRModel();
    }

    private static final int EVENT_INITIAL = 0;
    private static final int EVENT_EMIT = 1;

    private ASRManager() {
    }

    public void initASRService(Context context) {
        msrv = new ASRClientService(
                context,
                SystemConfig.multiService,
                SystemConfig.AsrBean.asrObject,
                SystemConfig.AsrBean.credentials_google,
                SystemConfig.AsrBean.credentials_xfyun,
                ASRManager.this,DeviceInfo.hotelName,DeviceInfo.roomName,
                SystemConfig.AsrBean.customerAccentSupportList);
    }

    public void setASRModel() {
        msrv.setASRModel(SystemConfig.AsrBean.asrObject);
    }


    public void startEmit(boolean advanced) {
        msrv.start(ChatApplication.system_lang.name(), advanced, Pinger.avgSpeed);
        Log.d(TAG, "onEmit = true");
        onEmit = true;

        if (executor == null) {
            Log.d(TAG, "newSingleThreadExecutor for asr take");
            executor = Executors.newSingleThreadExecutor();
        }
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Process.setThreadPriority(Process.THREAD_PRIORITY_DEFAULT);
                    Thread.currentThread().setName("ASR Thread");
                    byte[] buffer;
                    while (onEmit) {
                        buffer = ChatApplication.emitQueue.take();
                        //writeFile(o, "/sdcard/emit_dequeue.pcm", true);
                        msrv.inputAudio(buffer);
                    }
                    Log.d(TAG, "emit stopped");
                } catch (Exception ignore) {

                }
            }
        });
    }

    public void stopEmit() {
        Log.d(TAG, "onEmit = false");
        onEmit = false;
        if (executor != null) {
            executor.shutdownNow();
        }
        executor = null;
        msrv.stop();
        ChatApplication.emitQueue.clear();
    }
}
