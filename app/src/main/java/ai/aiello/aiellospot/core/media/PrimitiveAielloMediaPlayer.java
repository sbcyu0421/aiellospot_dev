package ai.aiello.aiellospot.core.media;

import android.os.Handler;
import android.os.Looper;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Player;
import com.litesuits.android.log.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ai.aiello.aiellospot.modules.iot.IoTModuleManager;

public class PrimitiveAielloMediaPlayer {

    private static PrimitiveAielloMediaPlayer instance;
    private String TAG = PrimitiveAielloMediaPlayer.class.getSimpleName();
    private static Handler handler;

    public static PrimitiveAielloMediaPlayer getInstance() {
        if (instance == null) {
            instance = new PrimitiveAielloMediaPlayer();
            handler = new Handler(Looper.getMainLooper());
        }
        return instance;
    }

    private PrimitiveAielloMediaPlayer() {
        consumer = Executors.newFixedThreadPool(1);
        consumer.submit(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        MediaHolder mediaHolder = playQueue.take();
                        String execute = mediaHolder.getExecute();
                        try {
                            switch (execute) {

                                case "delay":
                                    int delayTime = Integer.parseInt(mediaHolder.getValue());
                                    Thread.sleep(delayTime);
                                    break;

                                case "play":
                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                mediaHolders.add(mediaHolder);
                                                mediaHolder.getSimpleExoPlayer().prepare(mediaHolder.getMediaSource(), false, false);
                                                mediaHolder.getSimpleExoPlayer().addListener(new Player.EventListener() {
                                                    @Override
                                                    public void onPlayerError(ExoPlaybackException error) {
                                                        Log.e(TAG, error.toString());
                                                        doCompleteAction(mediaHolder);
                                                    }

                                                    @Override
                                                    public void onIsPlayingChanged(boolean isPlaying) {
                                                        if (!isPlaying) {
                                                            doCompleteAction(mediaHolder);
                                                        }
                                                    }
                                                });
                                            } catch (Exception e) {
                                                Log.e(TAG, e.toString());
                                            }
                                        }
                                    });
                                    break;

                                case "stop":
                                    stopAll(true);
                                    break;
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    private ArrayList<MediaHolder> mediaHolders = new ArrayList<>();

    //播完或停止時，即結束
    //喚醒時，需判斷狀況才結束

    public void stopAll(boolean force) {
        android.util.Log.d(TAG, "stopAll, size = " + mediaHolders.size());
        try {
            for (MediaHolder mediaHolder : mediaHolders) {
                stop(mediaHolder, force);
            }
        } catch (Exception ignore) {

        } finally {
            if (force) mediaHolders.clear();
        }
        try {
            if (force) playQueue.clear();
        } catch (Exception ignore) {

        }
    }


    private void stop(MediaHolder mediaHolder, boolean force) {
        if (mediaHolder.getSimpleExoPlayer() == null) {
            android.util.Log.d(TAG, mediaHolder.getId() + " is already completed ");
            return;
        }
        if (force) {
            android.util.Log.d(TAG, "stop " + mediaHolder.getId() + " by force... ");
            mediaHolder.getSimpleExoPlayer().stop(); // stop would auto trigger onplaying change event
            return;
        }
        if (mediaHolder.isForeground()) {
            android.util.Log.d(TAG, "stop " + mediaHolder.getId() + " by user... ");
            mediaHolder.getSimpleExoPlayer().stop(); // stop would auto trigger onplaying change event
        }
    }


    private void doCompleteAction(MediaHolder mediaHolder) {
        try {
            mediaHolder.getSimpleExoPlayer().release();
            JSONArray finalArray = new JSONArray();
            JSONObject tmp = new JSONObject();
            tmp.put("code", "festivalStop");
            finalArray.put(tmp);
            IoTModuleManager.getInstance().push_iot_intent_to_remoteService(finalArray, "ServerDrivenIntent");
        } catch (Exception e) {
            e.printStackTrace();
            android.util.Log.d(TAG, mediaHolder.getId() + " error ");
        } finally {
            mediaHolder.setSimpleExoPlayer(null);
            mediaHolders.remove(mediaHolder);
            android.util.Log.d(TAG, mediaHolder.getId() + " stopped completed");
        }
    }

    private ExecutorService consumer;

    private ArrayBlockingQueue<MediaHolder> playQueue = new ArrayBlockingQueue<>(30);

    public void enqueue(MediaHolder mediaHolder) {
        try {
            boolean result = playQueue.offer(mediaHolder);
            android.util.Log.d(TAG, "enqueue playerMeta " + mediaHolder.getValue() + " is " + result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
