package ai.aiello.aiellospot.core.messager.kafka;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

public class SpotConsumer extends Thread  {
    protected String TAG = SpotConsumer.class.getSimpleName();
    private String topic;
    private String consumerInst;
    private String createUrl;
    private String subscribeUrl;
    private String topicUrl;
    protected ArrayBlockingQueue<JSONObject> consumerQueue;
    private boolean isGroupBind;
    private int freq;
    private String subKey;


    /**
     *
     * @param consumerGroup
     * @param consumerInstance
     * @param kafkaTopic get the message from which kafka topic
     * @param baseUrl kafka url
     * @param dataLength get collected messages while timeout period
     * @param freq Frequency to  get messages from kafka
     * @param consumerQueue put received messages in this queue
     */
    public SpotConsumer(String consumerGroup, String consumerInstance, String kafkaTopic, String baseUrl, int dataLength, int freq, ArrayBlockingQueue<JSONObject> consumerQueue, String subKey) {
        this.topic = kafkaTopic;
        this.consumerInst = consumerInstance;
        this.subKey = subKey;
        this.createUrl = baseUrl + "/consumers/" + consumerGroup;
        this.subscribeUrl = baseUrl + "/consumers/" + consumerGroup + "/instances/" + consumerInstance + "/subscription";
        this.topicUrl = baseUrl + "/consumers/" + consumerGroup + "/instances/" + consumerInstance + "/records?timeout=" + dataLength;

        this.consumerQueue = consumerQueue;
        this.isGroupBind = false;
        this.freq = freq;
        this.execute = true;
        this.setName(TAG);
        Log.d(TAG, "SpotConsumer: " + topicUrl);
    }

    private boolean execute;

    public void pausePolling() {
        synchronized (this) {
            Log.d(TAG, "pause consume message");
            execute = false;
        }
    }

    public void resumePolling() {
        synchronized (this) {
            Log.d(TAG, "resume consume message");
            execute = true;
        }
    }

    @Override
    public void run() {
        super.run();
        while (true) {
            if (execute) {
                if (!isGroupBind) {
                    bindConsumeGroup();
                } else {
                    consume();
                }
            }
            try {
                Thread.sleep(freq);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    // pull messages from subscribed topic
    public void consume() {
        String msg = "";
        try {
            int[] resp = {-1};
//            Log.d(TAG, "send get msg url = " + topicUrl);
            msg = KafkaUtils.get(topicUrl, resp, TAG, subKey);
            if (resp[0] == HttpURLConnection.HTTP_OK) {
//                Log.i(TAG, "Get msg, " + msg);
                if (!msg.isEmpty()) {
                    JSONArray resJsonAry = new JSONArray(msg);
                    for (int i = 0; i < resJsonAry.length(); i++) {
                        JSONObject value = resJsonAry.getJSONObject(i).getJSONObject("value");
                        consumerQueue.offer(value, 100, TimeUnit.MILLISECONDS);
                    }
                }
            } else {
                Log.e(TAG, "[Consume Error] Get return " + resp[0]);
                if (resp[0] == 40403) {
                    //try bind again
                    bindConsumeGroup();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // init the consumer to kafka
    private void bindConsumeGroup() {
        createInstanceInConsumerGroup();
        subscribeTopic();
    }

    private void createInstanceInConsumerGroup() {
        try {
            JSONObject msg = new JSONObject();
            msg.put("name", consumerInst);
            msg.put("format", "json");
            msg.put("auto.offset.reset", "earliest");
            msg.put("auto.commit.enable", "true");

            int[] resp = {-1};
            Log.d(TAG, "send creating instance, msg = " + msg);
            KafkaUtils.post(createUrl, msg, resp, TAG, subKey);
            if (resp[0] == HttpURLConnection.HTTP_OK || resp[0] == 409 ) {  // 409 is acceptable
                Log.i(TAG, "Post creating instance message, msg: " + msg);
            } else {
                Log.e(TAG, "[Create Instance Error] Post return " + resp[0]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void subscribeTopic() {
        try {
            JSONArray topicAry = new JSONArray();
            topicAry.put(topic);
            JSONObject msg = new JSONObject();
            msg.put("topics", topicAry);

            int[] resp = {-1};
            Log.d(TAG, "send subscribing topic, msg = " + msg);
            KafkaUtils.post(subscribeUrl, msg, resp, TAG, subKey);
            if (resp[0] == HttpURLConnection.HTTP_OK || resp[0] == 204) { // 204 is acceptable
                Log.i(TAG, "Post subscribing topic message, msg: " + msg);
                isGroupBind = true;
            } else {
                Log.e(TAG, "[Subscribe topic Error] Post return " + resp[0]);
                isGroupBind = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}