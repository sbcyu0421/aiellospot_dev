package ai.aiello.aiellospot.core.info;

import ai.aiello.aiellospot.core.config.Configer;
import ai.aiello.aiellospot.views.activity.new_ota.VersionCheckerT;

public class DeviceInfo {

    public static String uuid = "";
    public static String timeZone = "";

    public static int majorVersion = -1;  //service version
    public static String MAC = "";
    public static String roomName = "";
    public static String roomType = "";
    public static String hotelName = "";
    public static int hotelID = -1;
    public static String city = "";
    public static String domain = "";
    public static double geo_lat = 0.0;
    public static double geo_lon = 0.0;

    public static String assistantName;
    public static String assistantName_voice;
    public static String saiModel;

    //hotel
    public static String hotelWelcome0_en = "Dear %s, ";
    public static String hotelWelcome1_en = "";
    public static String hotelWelcome2_en = "";

    public static String hotelWelcome0_tw = "親愛的%s，";
    public static String hotelWelcome1_tw = "您好";
    public static String hotelWelcome2_tw = "";

    public static String hotelWelcome0_cn = "亲爱的%s，";
    public static String hotelWelcome1_cn = "您好";
    public static String hotelWelcome2_cn = "";

    public static String city_tw = "";
    public static String city_en = "";
    public static String city_cn = "";
    public static String city_ja = "";

    public static String accuRedirectUrl = "";

    public static String getDebugInfo(){
        String info = String.format("mac : %s \n" +
                        "============================== \n" +
                        "hotelName : %s \n" +
                        "roomType : %s \n" +
                        "roomName : %s \n" +
                        "============================== \n" +
                        "img_ver : %s \n" +
                        "apk_ver : %s \n" +
                        "push_ver : %s \n" +
                        "music_ver : %s \n" +
                        "sai_ver : %s",
                DeviceInfo.MAC,
                DeviceInfo.hotelName,
                DeviceInfo.roomType,
                DeviceInfo.roomName,
                VersionCheckerT.img_ver,
                VersionCheckerT.apk_ver,
                VersionCheckerT.push_ver,
                VersionCheckerT.music_ver,
                VersionCheckerT.sai_ver);
        return info;
    }




}
