package ai.aiello.aiellospot.core.ota;


import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.utlis.Http;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.litesuits.android.log.Log;

import android.view.WindowManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;


/**
 * @author feicien (ithcheng@gmail.com)
 * @since 2016-07-05 19:21
 */
public class GetUpdateListTask extends AsyncTask<Void, Void, String> {

    private static final String TAG = GetUpdateListTask.class.getSimpleName();
    private Activity activity;
    private String url;
    private String currName = "";
    private ProgressDialog dialog;
    private String key;
    private String type;

    public GetUpdateListTask(Activity activity, String url, String key, int hotelId, String type, String cloud, int majorVer) {
        this.activity = activity;
        this.key = key;
        this.type = type;
        this.url = String.format(url, hotelId, type, cloud, majorVer);
        dialog = new ProgressDialog(activity);
        dialog.setTitle("版本資料庫查詢中");
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog.show();
    }

    @Override
    protected String doInBackground(Void... voids) {
        currName = getVersionName(activity);
        return Http.get(url, 5000, key).response;
    }

    @Override
    protected void onPostExecute(String result) {
        dialog.dismiss();
        if (!TextUtils.isEmpty(result)) {
            parseJson_multi(result);
        }
    }

    private void parseJson_multi(String result) {
        try {
            ArrayList<ApkItem> apkList = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(result);
            JSONArray apk_array = jsonObject.getJSONArray("data");
            for (int s = 0; s < apk_array.length(); s++) {
                try {
                    JSONObject obj2 = new JSONObject(apk_array.get(s).toString());
                    String versionName = obj2.getString("versionName");
                    String note = obj2.getString("note");
                    String releaseDate = obj2.getString("releaseDate");
                    String apkUrl = obj2.getString("url");
                    ApkItem apkItem = null;
                    if (versionName.equals(currName)) {
                        apkItem = new ApkItem(versionName, note, releaseDate, apkUrl, 1, s + 1);
                    } else {
                        apkItem = new ApkItem(versionName, note, releaseDate, apkUrl, 0, s + 1);
                    }
                    apkList.add(apkItem);
                    Log.d("apkItem=", apkItem.getVersionName());
                } catch (Exception ignore) {
                    //ignore
                }
            }

            //reverse, latest on top
            Collections.reverse(apkList);

            SPOTUpdateListDialog SPOTUpdateListDialog = new SPOTUpdateListDialog(activity, apkList);
            if (type.equals("release")) {
                SPOTUpdateListDialog.setTitle("請選擇欲使用的正式版本");
            } else {
                SPOTUpdateListDialog.setTitle("請選擇欲使用的測試版本");
            }
            SPOTUpdateListDialog.show();

        } catch (JSONException e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e(TAG, "parse json error");
        }
    }


    public String getVersionName(Context mContext) {
        if (mContext != null) {
            try {
                return mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0).versionName;
            } catch (Exception e) {
                Log.e(TAG, e.toString());
                SpotDeviceLog.exception(TAG, "", e.getMessage());
            }
        }
        return "";
    }

}
