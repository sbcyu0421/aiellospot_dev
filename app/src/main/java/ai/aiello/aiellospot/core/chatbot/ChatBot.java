package ai.aiello.aiellospot.core.chatbot;

import com.litesuits.android.log.Log;

import org.apache.commons.lang.StringEscapeUtils;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.views.activity.new_ota.VersionCheckerT;
import ai.aiello.aiellospot.core.info.UserInfo;
import ai.aiello.aiellospot.intents.IntentManager;
import ai.aiello.aiellospot.core.config.APIConfig;
import ai.aiello.aiellospot.core.config.SystemConfig;
import ai.aiello.aiellospot.core.info.DeviceInfo;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.system.WifiReceiver;
import ai.aiello.aiellospot.utlis.Http;
import ai.aiello.aiellospot.utlis.TestHub;
import ai.aiello.chinese.ChineseUtils;


/**
 * Created by a1990 on 2019/3/19.
 */

public class ChatBot {

    private static String TAG = ChatBot.class.getSimpleName();
    public static boolean IntegrationMode = false; //tag for integration

    public static class ChatBotRunnable implements Runnable {

        private String asr_word;
        private String session_id;
        private int id;
        private String specialIntent = "";
        private CountDownLatch latch;
        private String language = "";


        public ChatBotRunnable(String asr_word, String session_id, int id, int specialIntent, CountDownLatch latch) {
            this.asr_word = asr_word;
            //chatbot use tradition only
            if (ChatApplication.locale == Locale.CHINA) {
                asr_word = ChineseUtils.toTraditional(asr_word, true);
            }
            this.asr_word = asr_word;
            this.session_id = session_id;
            this.id = id;
            if (specialIntent > -1) {
                this.specialIntent = String.valueOf(specialIntent);
            }
            this.latch = latch;
        }

        public ChatBotRunnable(String asr_word, String session_id, int id, int specialIntent, CountDownLatch latch, String language) {
            this.asr_word = asr_word;
            //chatbot use tradition only
            if (ChatApplication.locale == Locale.CHINA) {
                asr_word = ChineseUtils.toTraditional(asr_word, true);
            }
            this.asr_word = asr_word;
            this.session_id = session_id;
            this.id = id;
            this.language = language;
            if (specialIntent > -1) {
                this.specialIntent = String.valueOf(specialIntent);
            }
            this.latch = latch;
        }


        @Override
        public void run() {
            JSONObject jsonObject = requestChatbot(id, asr_word, session_id, specialIntent);
            IntentManager.chatbotResult.add(jsonObject);
            latch.countDown();
        }
    }

    @NotNull
    public static JSONObject requestChatbot(int id, String asr_word, String session_id, String specialIntent) {
        JSONObject jsonObject = null;
        URL url;
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        try {
            Thread.currentThread().setName("askChatBotInCacheThread_single, id = " + id);
            Thread.sleep(id * 10);
            Log.d(TAG, "sleep=" + Thread.currentThread().getId());
            Log.d(TAG, "string=" + asr_word);
            String[] timezone_s = DeviceInfo.timeZone.split("/");
            JSONObject sendData = new JSONObject();
            JSONObject metaData = new JSONObject();
            JSONObject userInfo = new JSONObject();
            JSONObject locationInfo = new JSONObject();
            JSONObject versionInfo = new JSONObject();
            sendData.put("sender", DeviceInfo.uuid + "_" + id);
            sendData.put("message", asr_word);
            sendData.put("metadata", metaData);
            metaData.put("userInfo", userInfo);
            metaData.put("locationInfo", locationInfo);
            metaData.put("versionInfo", versionInfo);
            try {
                metaData.put("wifi_rssi", WifiReceiver.WifiRSSI);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //send info
            if (!session_id.isEmpty()) {
                sendData.put("sender", session_id);
            }
            metaData.put("session", session_id);
            metaData.put("language_code", ChatApplication.dbLanguageCode);
            metaData.put("device_timezone_1", timezone_s[0]);
            metaData.put("device_timezone_2", timezone_s[1]);
            metaData.put("device_uuid", DeviceInfo.uuid);
            metaData.put("hotel_id", DeviceInfo.hotelID + "");
            metaData.put("hotelName", DeviceInfo.hotelName);
            metaData.put("roomName", DeviceInfo.roomName);
            metaData.put("roomType", DeviceInfo.roomType);
            metaData.put("device_city", DeviceInfo.city);
            metaData.put("special_intent", specialIntent);
            if (IntegrationMode) {
                Log.i(TAG, "IntegrationMode");
                metaData.put("mode", new JSONArray().put("integration"));
            }

            try {
                userInfo.put("userName", UserInfo.userName);
                userInfo.put("userGender", UserInfo.userGender);
                userInfo.put("userNationality", UserInfo.userNationality);
                userInfo.put("userLanguage", UserInfo.userLanguage);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                locationInfo.put("lat", DeviceInfo.geo_lat);
                locationInfo.put("lon", DeviceInfo.geo_lon);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                versionInfo.put("spot_version", VersionCheckerT.apk_ver);
                versionInfo.put("music_version", VersionCheckerT.music_ver);
                versionInfo.put("push_version", VersionCheckerT.push_ver);
                versionInfo.put("sai_version", VersionCheckerT.sai_ver);
                versionInfo.put("img_version", VersionCheckerT.img_ver);
            } catch (JSONException e) {
                e.printStackTrace();
            }


            Log.d(TAG, "request_chatbot = " + sendData.toString());
            url = new URL(SystemConfig.ChatbotBean.chatbotUrl);
//                url = new URL("http://192.168.2.39:3001/api/chatbot");
//                URL url = new URL("https://aiello-gate.azure-api.net/test-chatbot/api/chatbot/" + "{\"version\":\"v2\"}"); //for test
            Log.d(TAG, url.toString());
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty(APIConfig.KEY_SUB_LABEL, SystemConfig.ChatbotBean.chatbotKey); // new key
            connection.setConnectTimeout(7000);
            connection.setReadTimeout(7000);

            //params
            DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());
            byte[] t = sendData.toString().getBytes("utf-8");
            dataOutputStream.write(t);
            dataOutputStream.flush();
            dataOutputStream.close();
            connection.connect();
            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK || responseCode == HttpURLConnection.HTTP_CREATED) {
                inputStream = connection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
                String title2 = StringEscapeUtils.unescapeJava(result.toString());
                Log.d(TAG, "chatbot_response=" + result.toString());

                //取代小美犀、小犀管家
                title2 = title2.replace("小美犀", DeviceInfo.assistantName_voice);
                title2 = title2.replace("小犀管家", DeviceInfo.assistantName_voice);
                //繁轉簡 (有慣用詞mapping)
                if (ChatApplication.system_lang.equals(ChatApplication.Language.zh_CN)) {
                    title2 = ChineseUtils.toSimplified(title2, true);
                }

                jsonObject = new JSONObject(title2);
                if (TestHub.debug_mode) {
                    try {
                        TestHub.SendChatbotEvent(jsonObject);
                    } catch (Exception ex) {
                        Log.e("AutoTest", "Fail:" + ex.toString());
                    }
                }
            } else {
                throw new Exception("" + responseCode);
            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());

            try {
                jsonObject = new JSONObject();
                jsonObject.put("res", new JSONArray().put(""));
                jsonObject.put("data", new JSONArray().put(""));
                jsonObject.put("metadata", new JSONObject().put("session", session_id).put("queryText", asr_word));
                jsonObject.put("end_dialogue", true);
                jsonObject.put("recipient_id", "");
                if (e instanceof SocketTimeoutException) {
                    jsonObject.put("intent_name", "Dialogflow.TimeoutError");
                } else {
                    jsonObject.put("intent_name", "UnHandleIntent");
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            }

        } finally {
            try {
                if (connection != null) {
                    try {
                        connection.getInputStream().close();
                    } catch (Exception ignore) {

                    }
                    connection.disconnect();
                    connection = null;
                }
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }
        }
        return jsonObject;
    }

    public static IntentManager.IntentStandardFormat parse(JSONObject jObj) {
        IntentManager.IntentStandardFormat intentStandardFormat = null;
        String intent;
        String chat_response;
        String session;
        boolean end_of_dialog;
        String chatbot_data;
        String querytext;
        String org_querytext;
        String agentActions;
        try {
            intent = jObj.getString("intent_name");
            chatbot_data = jObj.getJSONArray("data").get(0).toString();
            chat_response = jObj.getJSONArray("res").get(0).toString();
            session = jObj.getString("recipient_id");
            end_of_dialog = jObj.getBoolean("end_dialogue");
            querytext = jObj.getJSONObject("metadata").getString("queryText");
            try {
                org_querytext = jObj.getJSONObject("metadata").getString("original_queryText");
            } catch (JSONException e) {
                org_querytext = "";
                e.printStackTrace();
            }
            try {
                agentActions = jObj.getJSONObject("agentActions").toString();
            }catch (Exception e){
                agentActions = "";
            }
            // new format for agent
            try {
                String agentRes = jObj.getJSONObject("agentData").getString("response");
                if (!agentRes.isEmpty()) chat_response = agentRes;
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                String agentIntent = jObj.getString("agentIntent");
                if (!agentIntent.isEmpty()) intent = agentIntent;
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                end_of_dialog = jObj.getJSONObject("agentData").getBoolean("end_dialogue");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            intent = "Dialogflow.Unknown";
            chat_response = "";
            session = "";
            end_of_dialog = true;
            chatbot_data = "";
            querytext = "";
            org_querytext = "";
            agentActions = "";
        }
        intentStandardFormat = new IntentManager.IntentStandardFormat(intent, chat_response, session, end_of_dialog, chatbot_data, querytext, org_querytext, agentActions);
        return intentStandardFormat;
    }


    public static void cleanForm() {
        Log.d(TAG, "cleanForm");
        ChatApplication.worker.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("sender_id", DeviceInfo.uuid);
                    Http.post(SystemConfig.ChatbotBean.chatbotCleanFormUrl, jsonObject, 5000, SystemConfig.ChatbotBean.chatbotKey);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

}

