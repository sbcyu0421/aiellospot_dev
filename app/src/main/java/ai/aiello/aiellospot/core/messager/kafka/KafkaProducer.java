package ai.aiello.aiellospot.core.messager.kafka;

import android.util.Log;

import com.github.f4b6a3.uuid.UuidCreator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

public class KafkaProducer extends Thread {
    private String TAG;
    private String producerTopicUrl;
    private int queueOfferTimeout;

    // message id, prefix_0~999, which used for server log
    private static int msgCount = 0;
    private static final int MSG_MAX_COUNT = 1000;

    private ArrayBlockingQueue<JSONObject> sendQueue;

    private boolean guarantee;
    private String subKey;

    /**
     * @param producerName      Producer Name
     * @param topic             Kafka topic
     * @param baseUrl           Kafka Url
     * @param queueSize         max Message in queue
     * @param queueOfferTimeout if queue is full, how long to wait to enqueue
     */
    public KafkaProducer(String producerName, String topic, String baseUrl, int queueSize, int queueOfferTimeout, boolean guarantee, String subKey) {
        TAG = producerName;
        this.producerTopicUrl = baseUrl + "/topics/" + topic;
        this.sendQueue = new ArrayBlockingQueue<>(queueSize);
        this.queueOfferTimeout = queueOfferTimeout;
        this.guarantee = guarantee;
        this.subKey = subKey;
    }

    @Override
    public void run() {
        super.run();
        while (true) {
            try {
                JSONObject msg = sendQueue.take();
                msgCount = (msgCount + 1) % MSG_MAX_COUNT;
                msg.put("ID", "SPOT_" + UuidCreator.getTimeBasedWithMac().toString());
                produce(msg);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void produce(JSONObject jsonObject) {
        int[] resp = {-1};
        try {
            JSONObject valJson = new JSONObject();
            valJson.put("value", jsonObject);
            JSONArray recordsAry = new JSONArray();
            recordsAry.put(valJson);
            JSONObject msg = new JSONObject();
            msg.put("records", recordsAry);
            KafkaUtils.post(producerTopicUrl, msg, resp, TAG, subKey);
            if (resp[0] == HttpURLConnection.HTTP_OK) {
                Log.i(TAG, "Produce message, val: " + msg);
            } else {
                Log.e(TAG, "[Error] Post return " + resp[0]);
                if (guarantee) {
                    Log.d(TAG, "important queue, retry after 2s");
                    Thread.sleep(2000);
                    enqueueMessage(jsonObject);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void enqueueMessage(JSONObject jsonObject) {
        try {
            if (!sendQueue.offer(jsonObject, queueOfferTimeout, TimeUnit.MILLISECONDS)) {
                Log.e(TAG, "sendQueue enqueue timeout");
                sendQueue.clear();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}