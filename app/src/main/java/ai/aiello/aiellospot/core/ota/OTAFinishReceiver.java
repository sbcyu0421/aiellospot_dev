package ai.aiello.aiellospot.core.ota;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class OTAFinishReceiver extends BroadcastReceiver {

    public static String TAG = OTAFinishReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        System.exit(0);
    }

}
