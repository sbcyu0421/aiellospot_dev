package ai.aiello.aiellospot.core.device;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.core.asr.ASRManager;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.core.media.MsttsManager;
import ai.aiello.aiellospot.events.system.SystemEvent;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.utlis.Formatter;

import android.app.AlarmManager;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;

import com.litesuits.android.log.Log;
import com.cochenct.device_ctrl.BluetoothCtrl;
import com.cochenct.device_ctrl.BrightCtrl;
import com.cochenct.device_ctrl.VolumeCtrl;

import org.greenrobot.eventbus.EventBus;

import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by a1990 on 2019/5/14.
 */

public class DeviceControl {

    private static int currentVolume = 0;
    private static int currentBacklight = 0;
    public static BTState blueToothState = BTState.OFF;
    public static String bondedDevicesName = "";
    private static String BTName = "";

    public static int tempBacklight = -1;
    private final static int maxVol = 10;
    private final static int minVol = 0;

    private final static int maxBrightness = 100;
    private final static int minBrightness = 10;
    private final static int offBrightness = 0;

    private static VolumeCtrl volumeCtrl;
    private static BluetoothCtrl bluetoothCtrl;
    private static BrightCtrl brightCtrl;
    private static boolean isBtRegisteredReceiver = false;
    public static AudioManager audioManager;

    public enum BTState {
        ON, OFF, CONNECTED
    }

    private static String TAG = DeviceControl.class.getSimpleName();


    public static void execute(String actionCode) {
        switch (actionCode) {
            case SystemActionCode.SCREEN_ON:
                backLightOn();
                break;
            case SystemActionCode.SCREEN_OFF:
                backLightOff();
                break;
            default:
                android.util.Log.d(TAG, "no handle actionCode: " + actionCode);
        }
    }

    public static void initDeviceControlManager(Context ctx, boolean printLog) {
        Log.isPrint = printLog;
        bluetoothCtrl = new BluetoothCtrl(ctx);
        volumeCtrl = new VolumeCtrl(ctx);
        brightCtrl = new BrightCtrl(ctx);
        audioManager = (AudioManager) ChatApplication.context.getApplicationContext().getSystemService(Context.AUDIO_SERVICE);

        if (bluetoothCtrl.isEnable()) {
            blueToothState = BTState.ON;
        } else {
            blueToothState = BTState.OFF;
        }
        doRegisterBTReceiver(ctx);

        currentVolume = volumeCtrl.getCurrentVolume();
        currentBacklight = brightCtrl.getBrightness(); //init
    }

    public static void enableBT() {
        bluetoothCtrl.enableBT(BTName);
        blueToothState = BTState.ON;
    }

    public static void disableBT() {
        bluetoothCtrl.disableBT();
        blueToothState = BTState.OFF;
        bondedDevicesName = "";
    }

    public static String getBluetoothName() {
        return BTName;
    }

    public static void setBluetoothName(String custom_name) {
        BTName = custom_name;
    }


    // Create a BroadcastReceiver for ACTION_FOUND
    public static final BroadcastReceiver mBroadcastReceiver1 = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // When discovery finds a device

            Log.d(TAG, "onReceive = " + action);

            switch (action) {

                case BluetoothAdapter.ACTION_STATE_CHANGED:
                    final int action_state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                    switch (action_state) {
                        case BluetoothAdapter.STATE_OFF:
                            Log.d(TAG, "mBroadcastReceiver1: STATE OFF");
                            break;
                        case BluetoothAdapter.STATE_TURNING_OFF:
                            Log.d(TAG, "mBroadcastReceiver1: STATE TURNING OFF");
                            break;
                        case BluetoothAdapter.STATE_ON:
                            Log.d(TAG, "mBroadcastReceiver1: STATE ON");
                            bluetoothCtrl.discover(DeviceControl.BTName);
                            break;
                        case BluetoothAdapter.STATE_TURNING_ON:
                            Log.d(TAG, "mBroadcastReceiver1: STATE TURNING ON");
                            break;
                    }
                    EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.BT_STATE_CHANGE));
                    break;
                case BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED:
                    final int connection_state = intent.getIntExtra(BluetoothAdapter.EXTRA_CONNECTION_STATE, BluetoothAdapter.ERROR);
                    Log.d(TAG, "mBroadcastReceiver1: connection_state = " + connection_state);
                    switch (connection_state) {
                        case BluetoothAdapter.STATE_CONNECTED:
                            Log.d(TAG, "mBroadcastReceiver1: STATE CONNECTED");
                            if (bluetoothCtrl.getBondedDevices() != null) {
                                bondedDevicesName = bluetoothCtrl.getBondedDevices();
                                blueToothState = BTState.CONNECTED;
                                // userLog3
                                SpotUserTraceLog3.getInstance().buildSystemEventLog(
                                        SpotUserTraceLog3.EventSubject.BLUETOOTH,
                                        SpotUserTraceLog3.EventAction.CONNECTION,
                                        bondedDevicesName
                                );

                            } else {
                                blueToothState = BTState.ON;
                            }
                            break;
                        case BluetoothAdapter.STATE_DISCONNECTED:
                            Log.d(TAG, "mBroadcastReceiver1: STATE STATE_DISCONNECTED");
                            bondedDevicesName = "";
                            blueToothState = BTState.ON;
                            // userLog3
                            SpotUserTraceLog3.getInstance().buildSystemEventLog(
                                    SpotUserTraceLog3.EventSubject.BLUETOOTH,
                                    SpotUserTraceLog3.EventAction.DISCONNECTION,
                                    bondedDevicesName
                            );
                            break;
                    }
                    EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.BT_STATE_CHANGE));
                    break;

            }
        }
    };


    public static void doRegisterBTReceiver(Context ctx) {
        if (!isBtRegisteredReceiver) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
            intentFilter.addAction(BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED);
            ctx.registerReceiver(mBroadcastReceiver1, intentFilter);
        }
        isBtRegisteredReceiver = true;
    }

    public static void unRegisterReceiver(Context ctx) {
        if (isBtRegisteredReceiver) {
            ctx.unregisterReceiver(mBroadcastReceiver1);
        }
        isBtRegisteredReceiver = false;
    }

    //----------

    //light control
    public static int getBrightness() {
        return currentBacklight;
    }

    public static void setBrightness(int val) {
        brightCtrl.setBrightness(val);
        currentBacklight = val;
        Log.d(TAG, "currentBacklight =" + currentBacklight);
    }

    public static void maxBrightness() {
        setBrightness(maxBrightness);
    }

    public static void minBrightness() {
        setBrightness(minBrightness);
    }

    public static void upBrightness() {
        currentBacklight = currentBacklight + 10;
        if (currentBacklight > maxBrightness) {
            currentBacklight = maxBrightness;
        }
        setBrightness(currentBacklight);
    }

    public static void downBrightness() {
        currentBacklight = currentBacklight - 10;
        if (currentBacklight < minBrightness) {
            currentBacklight = minBrightness;
        }
        setBrightness(currentBacklight);
    }

    public static void defaultBackLight() {
        DeviceControl.setBrightness(50);
        tempBacklight = -1;
    }

    public static void backLightOff() {
        Log.d(TAG, "tempBacklight= " + tempBacklight + ",currentBacklight=" + currentBacklight);
        if (tempBacklight == -1) {
            tempBacklight = currentBacklight;
        }
        setBrightness(offBrightness);
    }

    public static void backLightOn() {
        Log.d(TAG, "tempBacklight= " + tempBacklight + ",currentBacklight=" + currentBacklight);
        if (tempBacklight != -1) {
            setBrightness(tempBacklight);
            tempBacklight = -1;
        }
    }


    //----------------------music volume-------------------


    public static int getCurrentVolume() {
        return currentVolume;
    }

    public static void setVolume(int v) {
        volumeCtrl.setVolume(v);
        currentVolume = v;
    }

    public static void upVolume() {
        currentVolume++;
        if (currentVolume > maxVol) {
            currentVolume = maxVol;
        }
        setVolume(currentVolume);
    }

    public static void downVolume() {
        currentVolume--;
        if (currentVolume < minVol) {
            currentVolume = minVol;
        }
        setVolume(currentVolume);
    }

    public static void maxVolume() {
        setVolume(maxVol);
    }

    public static void minVolume() {
        setVolume(minVol + 1);
    }

    public static void muteVol() {
        setVolume(minVol);
    }

    public static void defaultVol() {
        setVolume(5);
    }

    public static void tutorVol() {
        setVolume(7);
    }

    //----------------------alarm volume-------------------

    private static int volume_alarm = 5;
    private static final int min_volume_alarm = 1;
    private static final int max_volume_alarm = 10;

    public static int getCurrentVol_alarm() {
        return volume_alarm;
    }

    public static int setVol_alarm(int i) {
        volume_alarm = i;
        return volume_alarm;
    }

    public static void maxVolume_alarm() {
        volume_alarm = max_volume_alarm;
    }

    public static void minVolume_alarm() {
        volume_alarm = min_volume_alarm;
    }

    public static void muteVol_alarm() {
        volume_alarm = 0;
    }

    public static void upVolume_alarm() {
        volume_alarm++;
        if (volume_alarm > max_volume_alarm) {
            volume_alarm = max_volume_alarm;
        }
    }

    public static void downVolume_alarm() {
        volume_alarm--;
        if (volume_alarm < min_volume_alarm) {
            volume_alarm = min_volume_alarm;
        }
    }

    private static boolean isVolDown = false;


    public static void volumeDown() {
        if (isVolDown) {
            return;
        }
        android.util.Log.d(TAG, "volumeDown");
        int mediaVol = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        int reduceVolTo = 0;
        if (mediaVol > 3) {
            reduceVolTo = 1;
        }
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, reduceVolTo, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
        isVolDown = true;

    }

    public static void volumeResume() {
        if (ASRManager.getInstance().getOnASR()) {
            return;
        }
        if (MsttsManager.getInstance().isPlaying()) {
            return;
        }
        if (!isVolDown) {
            return;
        }
        android.util.Log.d(TAG, "volumeResume");
        isVolDown = false;
        int max = audioManager.getStreamVolume(AudioManager.STREAM_RING);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, max * 2, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
    }


    //system time setting

    public static void set24Format(Context context) {
        android.provider.Settings.System.putString(context.getContentResolver(), android.provider.Settings.System.TIME_12_24, "24");
    }

    public static void setTimeZone() {
        TimeZone.setDefault(TimeZone.getTimeZone(DeviceInfo.timeZone));
        Formatter.ISO8601DATEFORMAT.setTimeZone(TimeZone.getTimeZone(DeviceInfo.timeZone));
        android.util.Log.d(TAG, "Set config TimeZone to alarm formatter with: " + DeviceInfo.timeZone);
        android.util.Log.d(TAG, "AielloLog, device TimeZone: " + TimeZone.getDefault());
//        AlarmManager alarm = (AlarmManager)ChatApplication.ctx.getSystemService(Context.ALARM_SERVICE);
//        alarm.setTimeZone(DeviceInfo.timeZone);
    }


    public static void updateTimezone(Context context) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.setTimeZone(DeviceInfo.timeZone);
        Log.d(TAG, "DeviceInfo.timeZone = " + DeviceInfo.timeZone);
    }


    public static void updateLanguage(Locale locale) {

        try {
            Class<?> activityManagerNative = Class.forName("android.app.ActivityManagerNative");
            Log.i("amnType", activityManagerNative.toString());

            Object am = activityManagerNative.getMethod("getDefault").invoke(activityManagerNative);
            Log.i("amType", am.getClass().toString());

            Object config = am.getClass().getMethod("getConfiguration").invoke(am);
            Log.i("configType", config.getClass().toString());
            config.getClass().getDeclaredField("locale").set(config, locale);
            config.getClass().getDeclaredField("userSetLocale").setBoolean(config, true);

            am.getClass().getMethod("updateConfiguration", android.content.res.Configuration.class).invoke(am, config);

        } catch (Exception e) {
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e(TAG, e.toString());
        }

    }


}

