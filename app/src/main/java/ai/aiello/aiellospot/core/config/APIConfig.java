package ai.aiello.aiellospot.core.config;

import org.json.JSONException;
import org.json.JSONObject;

public class APIConfig {

    //api name (for ota)
    static final String KEY_POST_VERSION = "upload_version";
    static final String KEY_OTA = "latest_version";
    static final String KEY_SPOT_NOTE = "version_list";

    static final String KEY_DEVICEINFO = "device_info";
    static final String KEY_POSTRATING = "rating";

    static final String KEY_DC_STATE = "dc";
    static final String KEY_MSTTS = "mstts";

    static final String KEY_CHATBOT_INTEGRATE = "chatbot_integrate";

    static final String KEY_INTENT_ACTION = "intent_action";
    static final String KEY_MARKETING_ACTION_ELFIN = "marketing_action_elfin";

    //for core other

    static final String KEY_CREATETASK = "create_task";

    public static WEBAPI WEBAPI_DEVICEINFO = null;
    public static WEBAPI WEBAPI_POSTRATING = null;
    public static WEBAPI WEBAPI_POST_VERSION = null;

    public static WEBAPI WEBAPI_DC_STATE = null;
    public static WEBAPI WEBAPI_MSTTS = null;

    public static WEBAPI WEBAPI_CHATBOT_INTEGRATE = null;

    public static WEBAPI WEBAPI_OTA = null;
    public static WEBAPI WEBAPI_SPOT_NOTE = null;

    public static WEBAPI WEBAPI_INTENT_ACTION = null;
    public static WEBAPI WEBAPI_MARKETING_ACTION_ELFIN = null;

    public static final String KEY_SUB_LABEL = "Ocp-Apim-Subscription-Key";

    public static class WEBAPI {

        private String method;
        private String url;
        private String key;

        /**
         * Build webapi object and share with sub service
         * @param name
         * @param jsonObject
         * @param production
         * @return
         * @throws Exception
         */
        public static WEBAPI build(String name, JSONObject jsonObject, boolean production) throws Exception {
            String url = jsonObject.getString("url");
            if (!production) {
                url = jsonObject.getString("urlTest");
            }
            String key = jsonObject.getString("key");
            buildServiceConfig("PUSH_SAI_MUSIC", name, jsonObject.getString("url"), key, "", "");
            buildServiceConfig("PUSH_SAI_MUSIC", name + "_test", jsonObject.getString("urlTest"), key, "", "");
            return new APIConfig.WEBAPI("", url, key);
        }

        /**
         * Build webapi object for private use
         * @param jsonObject
         * @param production
         * @return
         * @throws Exception
         */
        public static WEBAPI build(JSONObject jsonObject, boolean production) throws Exception {
            String url = jsonObject.getString("url");
            if (!production) {
                url = jsonObject.getString("urlTest");
            }
            String key = jsonObject.getString("key");
            return new APIConfig.WEBAPI("", url, key);
        }

        private static void buildServiceConfig(String id, String api_name, String url, String key, String username, String password) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject();
                jsonObject.put("id", id);
                jsonObject.put("api_name", api_name);
                jsonObject.put("url", url);
                jsonObject.put("key", key);
                jsonObject.put("username", username);
                jsonObject.put("password", password);
                Configer.jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        WEBAPI(String method, String url, String key) {
            this.method = method;
            this.url = url;
            this.key = key;
        }

        public String getMethod() {
            return method;
        }

        public String getUrl() {
            return url;
        }

        public String getKey() {
            return key;
        }

    }

}
