package ai.aiello.aiellospot.core.media;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.litesuits.android.log.Log;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.core.device.DeviceControl;
import ai.aiello.aiellospot.events.system.SystemEvent;
import ai.aiello.aiellospot.events.system.TTSEvent;
import ai.aiello.aiellospot.core.asr.ASRManager;
import ai.aiello.aiellospot.core.config.APIConfig;
import ai.aiello.aiellospot.core.config.SystemConfig;
import ai.aiello.aiellospot.modules.music.MusicServiceModuleManager;
import ai.aiello.aiellospot.utlis.Http;
import ai.aiello.chinese.ChineseUtils;

public class MsttsManager {

    private static String TAG = MsttsManager.class.getSimpleName();
    public static Status status = Status.Idle;
    private static MsttsManager instance;
    private List<SimpleExoPlayer> players = new CopyOnWriteArrayList<>();
    private Handler handler;

    private MsttsManager() {
        handler = new Handler(Looper.getMainLooper());
    }

    public static MsttsManager getInstance() {
        if (instance == null) {
            instance = new MsttsManager();
        }
        return instance;
    }


    public enum Status {
        Idle, Playing
    }

    public boolean isPlaying() {
        return status == Status.Playing;
    }

    public void stopTTS() {
        PrimitiveAielloMediaPlayer.getInstance().stopAll(false);
        if (!isPlaying())
            return;
        Log.d(TAG, "stop TTS");
        releaseExo();
        updateStatus();
    }

    public void stopTtsAndMedia() {
        PrimitiveAielloMediaPlayer.getInstance().stopAll(true);
        if (!isPlaying())
            return;
        Log.d(TAG, "stop TTS");
        releaseExo();
        updateStatus();
    }

    private static MediaSource buildMediaSource(Uri uri, Context context) {
        DataSource.Factory dataSourceFactory =
                new DefaultDataSourceFactory(context, "exoplayer-aiello");
        return new ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
    }
    public void reportNews(String url, boolean playInASR, Context context){
        releaseExo();
        status = Status.Playing;
        ChatApplication.worker.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    if (ASRManager.getInstance().getOnASR() && !playInASR) {
                        Log.d(TAG, "can not play when asr active");
                    } else {
                        playExo(context, url);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void updateStatus(){
        if (players.size() == 0) {
            status = Status.Idle;
        } else {
            status = Status.Playing;
        }
        android.util.Log.d(TAG, "updateStatus: " + status);
    }

    public void speakWord(String playword, boolean playInASR, Context context) {
        Log.d(TAG, "playword = " + playword);
        if (playword.isEmpty()) {
            Log.d(TAG, "playword is empty, vol resume");
            DeviceControl.volumeResume();
            return;
        }
        releaseExo();
        status = Status.Playing;
        ChatApplication.worker.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    String playwordCorrection = ttsCorrection(playword);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("lang", ChatApplication.tts_lang);
                    jsonObject.put("speech", playwordCorrection);
                    jsonObject.put("model", SystemConfig.TTS.getSpeechModel(ChatApplication.locale));
                    String res = Http.post(APIConfig.WEBAPI_MSTTS.getUrl(), jsonObject, 5000, APIConfig.WEBAPI_MSTTS.getKey()).response;
                    if (ASRManager.getInstance().getOnASR() && !playInASR) {
                        Log.d(TAG, "can not play when asr active");
                    } else {
                        playExo(context, res);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public void podCastAds(String playword, boolean playInASR, String adsLocale, Context context) {
        Log.d(TAG, "podCastAds = " + playword);
        if (playword.isEmpty()) {
            DeviceControl.volumeResume();
            return;
        }
        releaseExo();
        status = Status.Playing;
        String pol = "en";
        if (adsLocale.equals("tw") || adsLocale.equals("cn")) {
            pol = "tw";
        } else if(adsLocale.equals("ja") ){
            pol = "ja";
        }
        String finalPol = pol;
        ChatApplication.worker.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    String playwordCorrection = ttsCorrection(playword);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("lang", finalPol);
                    jsonObject.put("speech", playwordCorrection);
                    jsonObject.put("model", SystemConfig.TTS.getSpeechModel(ChatApplication.locale));
                    String res = Http.post(APIConfig.WEBAPI_MSTTS.getUrl(), jsonObject, 5000, APIConfig.WEBAPI_MSTTS.getKey()).response;
                    if (ASRManager.getInstance().getOnASR() && !playInASR) {
                        Log.d(TAG, "can not play when asr active");
                    } else {
                        playExo(context, res);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void playExo(Context context, String res) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "playExo");
                SimpleExoPlayer player = new SimpleExoPlayer.Builder(context).build();
                Uri uri = Uri.parse(res);
                MediaSource mediaSource = buildMediaSource(uri, context);
                player.setAudioStreamType(C.STREAM_TYPE_RING);
                player.setPlayWhenReady(true);
                player.prepare(mediaSource, false, false);
                player.addListener(new Player.EventListener() {
                    @Override
                    public void onPlayerError(ExoPlaybackException error) {
                        Log.e(TAG, "onPlayerError = "+error.toString());
                        release(player);
                        updateStatus();
                        DeviceControl.volumeResume();
                        EventBus.getDefault().post(new TTSEvent(TTSEvent.TTS_COMPLETED));
                    }

                    @Override
                    public void onIsPlayingChanged(boolean isPlaying) {
                        Log.i(TAG, "onTtsPlayingChanged=" + isPlaying);
                        if (!isPlaying) { //complete
                            release(player);
                            updateStatus();
                            DeviceControl.volumeResume();
                            EventBus.getDefault().post(new TTSEvent(TTSEvent.TTS_COMPLETED));
                        } else {
                            updateStatus();
                        }
                    }

                    private void release(SimpleExoPlayer player) {
                        players.remove(player);
                        try {
                            if (player.isPlaying())
                                player.stop();
                            player.release();
                            player = null;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                players.add(player);
                updateStatus();
            }
        });
    }


    private synchronized void releaseExo() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "releaseExo");
                for (SimpleExoPlayer player : players) {
                    try {
                        if (player != null) {
                            if (player.isPlaying())
                                player.stop();
                            player.release();
                            player = null;
                        }
                    } catch (Exception e) {
                        Log.e(TAG, e.toString());
                    }
                }
                players.clear();
            }
        });
    }

    private String ttsCorrection(String playword) {
        playword = playword.replace("%", "％");
        playword = playword.replace("?", "？");

        //replace character having certain speaking
        playword = playword.replace("美豪管家", "美豪管家");
        playword = playword.replace("美豪管家", "美豪管家");


        //舊版 (null or zh-CN-XiaoxiaoNeural)轉換，新版不轉換
        if (SystemConfig.TTS.getSpeechModel(ChatApplication.locale) == null ||
                SystemConfig.TTS.getSpeechModel(ChatApplication.locale).equals("zh-CN-XiaoxiaoNeural")) {
            //for 繁體有破音字，需轉換為簡體 (直接轉，無慣用詞mapping)
            if (ChatApplication.system_lang.equals(ChatApplication.Language.zh_TW)) {
                android.util.Log.d(TAG, "translate to simplify for polyphone on zh-CN-XiaoxiaoNeural");
                playword = ChineseUtils.toSimplified(playword, false);
            }
        }
        return playword;
    }

}

