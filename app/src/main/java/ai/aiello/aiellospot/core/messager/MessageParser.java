package ai.aiello.aiellospot.core.messager;

import android.content.Context;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.Instant;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.core.ota.APKUpdateService;
import ai.aiello.aiellospot.core.soundai.SoundAIManager;
import ai.aiello.aiellospot.events.server.ServerEvent;
import ai.aiello.aiellospot.events.system.SystemEvent;
import ai.aiello.aiellospot.intents.sdui.ActionManager;
import ai.aiello.aiellospot.intents.sdui.ServerDrivenHelper;
import ai.aiello.aiellospot.core.config.Configer;
import ai.aiello.aiellospot.core.config.SystemConfig;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.core.messager.kafka.KafkaMsg;
import ai.aiello.aiellospot.core.messager.message.MassageCode;
import ai.aiello.aiellospot.core.system.SystemControl;
import ai.aiello.aiellospot.utlis.AielloFileUtils;

public class MessageParser {

    private static final String TAG = MessageParser.class.getSimpleName();
    private Context context;

    public MessageParser(Context context) {
        this.context = context;
        EventBus.getDefault().register(this);
    }

    //main business model
    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onMessageEvent(KafkaMsg event) {
        try {
            JSONObject msg = event.getMsg();
            int code = msg.getInt("TYPE");
            int codeType = code / 1000 * 1000; // get 1000 or 2000 or...
            switch (codeType) {
                case MassageCode.SYSTEM_CATEGORY:
                    executeSystemMsg(msg);
                    break;
                case MassageCode.CUSTOMER_CATEGORY:
                    executeCustomerMsg(msg);
                    break;
                case MassageCode.ROOM_CATEGORY:
                    executeRoomMsg(msg);
                    break;
                case MassageCode.EMPLOYEE_CATEGORY:
                case MassageCode.TASK_CATEGORY:
                case MassageCode.ALIVE_CATEGORY:
                    Log.i(TAG, "device does not use this action category: " + codeType);
                    break;
                default:
                    Log.e(TAG, "no such action category: " + codeType);
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void executeSystemMsg(JSONObject msg) {
        try {
            switch (msg.getInt("TYPE")) {
                case MassageCode.SYSTEMTYPELIST_IMAGECHECK:
                case MassageCode.SYSTEMTYPELIST_IMAGEUPDATE:
                    SystemControl.imageCheckAndUpdate(context);
                    break;
                case MassageCode.SYSTEMTYPELIST_APPREBOOT:
                    EventBus.getDefault().post(new ServerEvent(ServerEvent.Type.APPREBOOT));
                    SystemControl.rebootAPK(context, SystemControl.StageAction.CLEAR);
                    break;
                case MassageCode.SYSTEMTYPELIST_SILENTRESTART:
                    EventBus.getDefault().post(new ServerEvent(ServerEvent.Type.SILENTRESTART));
                    SystemControl.rebootDevice(context, SystemControl.StageAction.SAVE);
                    break;
                case MassageCode.SYSTEMTYPELIST_DEVICEREBOOT:
                    EventBus.getDefault().post(new ServerEvent(ServerEvent.Type.DEVICEREBOOT));
                    SystemControl.rebootDevice(context, SystemControl.StageAction.CLEAR);
                    break;
                case MassageCode.SYSTEMTYPELIST_LOGUPLOAD:
                    SystemControl.uploadLogFromClient(new AielloFileUtils.UploadFinishListener() {
                        @Override
                        public void onSuccess() {
                            Log.d(TAG, "onSuccess");
                        }

                        @Override
                        public void onFail() {
                            Log.d(TAG, "onFail");
                        }
                    });
                    break;
                case MassageCode.SYSTEMTYPELIST_SPOT_CONFIG_HOT_RELOAD:
                    Log.i(TAG, "SPOT_CONFIG_HOT_RELOAD");
                    ChatApplication.worker.submit(new Runnable() {
                        @Override
                        public void run() {
                            Configer configer = new Configer();
                            configer.reloadConfig(context);

                            ChatApplication.initMessageService(ChatApplication.context);
                            ChatApplication.initPing(ChatApplication.context);

                            ChatApplication.hotReloadOnNextStateChange = true;
                            switch (ChatApplication.currentAppState) {
                                case BUSY:
                                    EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.BUSY_STATE_HOT_RELOAD));
                                    break;
                                case IDLE:
                                    EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.IDLE_STATE_HOT_RELOAD));
                                    break;
                            }
                        }
                    });
                    break;

                case MassageCode.FORCE_OTA:
                    APKUpdateService.forceOTA();
                    break;

                case MassageCode.SYSTEMTYPELIST_IOTCONFIG_RELOAD:
                    Log.i(TAG, "IOT_CONFIG_RELOAD");
                    EventBus.getDefault().post(new ServerEvent(ServerEvent.Type.IOTCONFIG_RELOAD));
                    break;

                case MassageCode.REMOTE_WAKE_UP:
                    SoundAIManager.startBeam();
                    break;

                default:
                    Log.e(TAG, "device does not have this System Action, code = " + msg.getInt("TYPE"));
                    break;
            }
        } catch (Exception e) {
            Log.e(TAG, "doSystemMsg error = " + e);
        }
    }

    private void executeCustomerMsg(JSONObject msg) {
        try {
            switch (msg.getInt("TYPE")) {
                case MassageCode.CUSTOMERTYPELIST_CHECKIN:
                    EventBus.getDefault().post(new ServerEvent(ServerEvent.Type.CHECK_IN, msg.toString()));
                    break;

                case MassageCode.CUSTOMERTYPELIST_CHECKOUT:
                    EventBus.getDefault().post(new ServerEvent(ServerEvent.Type.CHECK_OUT));
                    SystemControl.rebootDevice(ChatApplication.context, SystemControl.StageAction.CLEAR);
                    break;

                case MassageCode.CUSTOMERTYPELIST_NOTIFYSPOT:
                case MassageCode.CUSTOMERTYPELIST_NOTIFYROOMGROUP:
                    EventBus.getDefault().post(new ServerEvent(ServerEvent.Type.NOTIFICATION, msg.toString()));
                    break;

                case MassageCode.CUSTOMERTYPELIST_IOT:
                    try {
                        JSONArray loop_codes_array = msg.getJSONObject("DATA").getJSONArray("loop_codes");
                        String uniKey = loop_codes_array.getJSONObject(0).getString("code");
                        if (SystemConfig.OTA.support_action) {  // push compatible
                            ServerDrivenHelper.ServerMessage serverMessage = ServerDrivenHelper.request(DeviceInfo.hotelName, DeviceInfo.roomType, uniKey, -1);
                            ActionManager.getInstance().prepare(serverMessage.getResultAction(), true);
                        } else {
                            EventBus.getDefault().post(new ServerEvent(ServerEvent.Type.IOT_EVENT, loop_codes_array.toString()));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case MassageCode.CUSTOMERTYPELIST_PBR_AUTHORIZED:
                    EventBus.getDefault().post(new ServerEvent(ServerEvent.Type.PBR_AUTH));
                    break;

                default:
                    Log.e(TAG, "device does not have this Customer Action, code = " + msg.getInt("TYPE"));
                    break;
            }
        } catch (JSONException e) {
            Log.e(TAG, "Parser CustomerMsg error = " + e);
        }
    }

    private void executeRoomMsg(JSONObject msg) {
        try {
            long now = 0;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                now = Instant.now().getEpochSecond();
                Log.d(TAG, "executeRoomMsg now = "+ now);
            }
            switch (msg.getInt("TYPE")) {
                case MassageCode.ROOMTYPELIST_SATISFACTION:
                    EventBus.getDefault().post(new ServerEvent(ServerEvent.Type.SATISFACTION, msg.toString()));
                    break;

                case MassageCode.ROOMTYPELIST_NODISTURB:
                    //ignore 2s before
                    if (now - msg.getLong("TS") > kafkaMaxWait) {
                        Log.e(TAG, "ignore ROOMTYPELIST_NODISTURB > " + kafkaMaxWait + "s");
                        return;
                    }
                    EventBus.getDefault().post(new ServerEvent(ServerEvent.Type.DND, msg.toString()));
                    break;
                case MassageCode.ROOMTYPELIST_CLEAN:
                    //ignore 2s before
                    if (now - msg.getLong("TS") > kafkaMaxWait) {
                        Log.e(TAG, "ignore ROOMTYPELIST_CLEAN > " + kafkaMaxWait + "s");
                        return;
                    }
                    EventBus.getDefault().post(new ServerEvent(ServerEvent.Type.MUR, msg.toString()));
                    break;

                default:
                    Log.e(TAG, "device does not have this Room Action, code = " + msg.getInt("TYPE"));
                    break;
            }
        } catch (JSONException e) {
            Log.e(TAG, "Parser RoomMsg error = " + e);
        }
    }

    private int kafkaMaxWait = 15;

}
