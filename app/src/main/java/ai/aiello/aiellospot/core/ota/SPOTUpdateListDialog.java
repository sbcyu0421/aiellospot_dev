package ai.aiello.aiellospot.core.ota;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.ota.OTAProcessActivity;
import ai.aiello.aiellospot.views.componets.toast.MToaster;
import ai.aiello.aiellospot.core.config.APIConfig;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.soundai.SoundAIManager;
import ai.aiello.aiellospot.utlis.AielloFileUtils;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import com.github.johnpersano.supertoasts.library.Style;
import com.litesuits.android.log.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

class SPOTUpdateListDialog extends Dialog {

    private static String TAG = SPOTUpdateListDialog.class.getSimpleName();
    private static ListAdapter adapter;
    private static ListView listview;
    private static Button btn_confirm, btn_cancel;
    private static ProgressDialog progressDialog;
    private LayoutInflater inflater;
    private static final int BUFFER_SIZE = 10 * 1024; // 8k ~ 32K
    private static Context mContext;
    String select_url = "";
    ApkItem select_Item = null;
    int select_id;
    private ArrayList<ApkItem> apkList;


    public SPOTUpdateListDialog(Activity activity, ArrayList<ApkItem> apkList) {
        super(activity);
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = activity;
        this.apkList = apkList;
        setContentView(R.layout.listdialog);
        listview = findViewById(R.id.listview);
        btn_confirm = findViewById(R.id.btn_confirm);
        btn_cancel = findViewById(R.id.btn_cancel);

        adapter = new ListAdapter(apkList, inflater);
        listview.setAdapter(adapter);
        listview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listview.setSelector(new ColorDrawable(Color.rgb(41, 76, 97)));
        listview.setOnItemClickListener(onClickListView);

        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!select_url.isEmpty()) {
                    SoundAIManager.setWakeUp(null, false);
                    goToDownload(mContext, select_url);
                } else {
//                    Toast.makeText(activity, "請選擇要更新的版本", Toast.LENGTH_SHORT).show();
                    MToaster.showButtonToast(activity, "請選擇要更新的版本", Style.TYPE_STANDARD);
                }


            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

    }

    private AdapterView.OnItemClickListener onClickListView = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            select_url = apkList.get(position).getUrl();
            select_id = apkList.get(position).getId();
            select_Item = apkList.get(position);
//            Log.d(TAG, "select_url=" + select_url);
        }
    };


    @Override
    public void show() {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN;
        super.show();
        this.getWindow().setLayout(1100, 650);
        this.getWindow().getDecorView().setSystemUiVisibility(uiOptions);
    }


    private void goToDownload(Context context, String apkUrl) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setMessage("下載中...，下載完成後，系統約需10秒進行自動安裝作業，敬請耐心等待");
        progressDialog.setMax(100);
        progressDialog.setCancelable(false);
        DownloadAsync downloadAsync = new DownloadAsync();
        downloadAsync.execute(context, apkUrl);
        SPOTUpdateListDialog.this.dismiss();
    }


    public static class DownloadAsync extends AsyncTask<Object, Integer, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            progressDialog.dismiss();
            if (success) {
                Intent intent = new Intent(mContext, OTAProcessActivity.class);
                mContext.startActivity(intent);
            } else {
                Toast.makeText(mContext, "下載錯誤，請重新嘗試", Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progressDialog.setProgress(values[0]);
        }

        @Override
        protected Boolean doInBackground(Object... objects) {
            boolean success = false;
            Activity activity = (Activity) objects[0];
            Log.d(TAG, "downloading...");
            String urlStr = (String) objects[1];
            Log.d(TAG, urlStr);
            InputStream in = null;
            FileOutputStream out = null;
            try {
                URL url = new URL(urlStr);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");
                urlConnection.setDoOutput(false);
                urlConnection.setConnectTimeout(10 * 1000);
                urlConnection.setReadTimeout(10 * 1000);
                urlConnection.setRequestProperty("Connection", "Keep-Alive");
                urlConnection.setRequestProperty("Charset", "UTF-8");
                urlConnection.setRequestProperty("Accept-Encoding", "gzip, deflate");
                urlConnection.setRequestProperty(APIConfig.KEY_SUB_LABEL, APIConfig.WEBAPI_OTA.getKey());

                urlConnection.connect();
                long bytetotal = urlConnection.getContentLength();
                long bytesum = 0;
                int byteread = 0;
                in = urlConnection.getInputStream();

                File dir = AielloFileUtils.getCacheDirectory(activity);
                String apkName = urlStr.substring(urlStr.lastIndexOf("/") + 1, urlStr.length());
                File apkFile = new File(dir, apkName);
                out = new FileOutputStream(apkFile);
                byte[] buffer = new byte[BUFFER_SIZE];
                int progress = 0;
                while ((byteread = in.read(buffer)) != -1) {
                    bytesum += byteread;
                    out.write(buffer, 0, byteread);
                    progress = (int) (bytesum * 100L / bytetotal);
                    Log.d(TAG, "download progress = " + progress);
                    publishProgress(progress);
                }
                // 下载完成
                if (bytesum == bytetotal) {
                    ApkInstaller.installAPkSilently(activity, apkFile, 0);
                    success = true;
                }


            } catch (Exception e) {
              SpotDeviceLog.exception(TAG,"",e.getMessage());
                Log.e(TAG, "download apk file error:" + e.getMessage());
            } finally {
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException ignored) {

                    }
                }
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException ignored) {

                    }
                }
            }

            return success;
        }
    }


}

