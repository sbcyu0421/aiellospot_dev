package ai.aiello.aiellospot.core.system;

import ai.aiello.aiellospot.ChatApplication;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.litesuits.android.log.Log;

public class InterruptService extends Service {


    private static final String TAG = InterruptService.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(TAG, "onCreate(): Service Started.");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
      SpotDeviceLog.info(TAG,"","onStarCommand(): Received id " + startId + ": " + intent);
        Log.e(TAG, "onStarCommand(): Received id " + startId + ": " + intent);
        return START_STICKY; // run until explicitly stopped.
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.e(TAG, "onTaskRemoved");
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "InterruptService onDestroy");
        ChatApplication.releaseResource();
        super.onDestroy();
    }


}
