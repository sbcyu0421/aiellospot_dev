package ai.aiello.aiellospot.core.media;

public interface MediaCompletedListener {

    void onInterrupted();

    void onCompleted();
}
