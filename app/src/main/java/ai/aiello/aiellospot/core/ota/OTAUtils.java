package ai.aiello.aiellospot.core.ota;

import ai.aiello.aiellospot.core.config.APIConfig;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;

import com.litesuits.android.log.Log;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * @author feicien (ithcheng@gmail.com)
 * @since 2016-07-05 19:25
 */
public class OTAUtils {

    private static String TAG = OTAUtils.class.getSimpleName();


    public static String httpGet(String urlStr) {
        HttpURLConnection uRLConnection = null;
        InputStream is = null;
        BufferedReader buffer = null;
        String result = null;
        try {
            URL url = new URL(urlStr);
            uRLConnection = (HttpURLConnection) url.openConnection();
            uRLConnection.setRequestMethod("GET");
            uRLConnection.setRequestProperty(APIConfig.KEY_SUB_LABEL, APIConfig.WEBAPI_OTA.getKey());

            is = uRLConnection.getInputStream();
            buffer = new BufferedReader(new InputStreamReader(is));
            StringBuilder strBuilder = new StringBuilder();
            String line;
            while ((line = buffer.readLine()) != null) {
                strBuilder.append(line);
            }
            result = strBuilder.toString();
        } catch (Exception e) {
            Log.e(TAG, "http  error");
          SpotDeviceLog.exception(TAG,"",e.getMessage());
        } finally {
            if (buffer != null) {
                try {
                    buffer.close();
                } catch (IOException ignored) {

                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ignored) {

                }
            }
            if (uRLConnection != null) {
                uRLConnection.disconnect();
            }
        }
        return result;
    }


}
