package ai.aiello.aiellospot.core.system;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;


import ai.aiello.aiellospot.events.system.SystemEvent;
import ai.aiello.aiellospot.core.config.SystemConfig;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;

public class Pinger extends Thread {

    private ConnectivityManager connManager;
    private NetworkInfo info;
    private int freq;
    private int threshold;
    private String ip;
    private int avgTimes;
    private static String TAG = Pinger.class.getSimpleName();
    public static double avgSpeed = 10.0;
    private static ArrayList<Double> speedList = new ArrayList<>();
    private static int pingResetCount;
    public static double speed;
    private String[] ipPool;
    private int ipIndex = 0;

    private void addSpeed(double speed) {
        if (speed == threshold) {
            pingResetCount++;
            Log.d(TAG, "pingResetCount = " + pingResetCount);
        } else {
            pingResetCount = 0;
        }
        if (pingResetCount > 10) {
            pingResetCount = 0;
            AielloWifiManager.reInitWifi();
            Log.d(TAG, "wait for wifi reinit");
            while ((AielloWifiManager.iskWifiOnAndConnected() != 2)) {
                Log.d(TAG, "check reinit status ... ");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            //connected
            EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.WIFI_CONNECTED));
        }
        if (speed == 0) {
            return;
        }
        if (speedList.size() == avgTimes) {
            speedList.remove(0);
        }
        speedList.add(speed);
        float total = 0;
        for (int i = 0; i < speedList.size(); i++) {
            total += speedList.get(i);
        }
        avgSpeed = total / speedList.size();
        Log.d(TAG, String.format("avgSpeed = %s", avgSpeed));
    }

    public Pinger(Context context) {
        connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        this.freq = SystemConfig.WifiPingBean.freq;
        this.threshold = SystemConfig.WifiPingBean.threshold;
        this.ip = SystemConfig.WifiPingBean.ip; // has default value in Beam
        this.ipPool = this.ip.split(";");
        this.ipIndex = 0;
//        this.freq = 5;
//        this.threshold = 300;
//        this.ip = "8.8.8.8";
        this.avgTimes = 48; //4 min
        Log.d(TAG, String.format("ipPool = %s, threshold = %s, freq = %s", Arrays.toString(ipPool), threshold, freq));
    }

    public void updateConfig(){
        this.freq = SystemConfig.WifiPingBean.freq;
        this.threshold = SystemConfig.WifiPingBean.threshold;
        this.ip = SystemConfig.WifiPingBean.ip;
        this.ipPool = this.ip.split(";");
        this.ipIndex = 0;
        Log.d(TAG, String.format("ipPool = %s, threshold = %s, freq = %s", Arrays.toString(ipPool), threshold, freq));
    }

    @Override
    public void run() {
        while (true) {
            try {
                info = connManager.getActiveNetworkInfo();
                if (info == null || !info.isConnected()) {
                    Log.e(TAG, "NetworkInfo error");
                    addSpeed(threshold);
                } else {
                    if (info.isAvailable()) {
                        startPing(ipPool[ipIndex]);
                        ipIndex = (ipIndex == ipPool.length - 1) ? 0 : (ipIndex + 1);
                    } else {
                        Log.e(TAG, "network is not available");
                        reportStatus("network is not available", "");
                        addSpeed(threshold);
                    }
                }

                Thread.sleep(freq * 1000);

            } catch (InterruptedException e) {

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void startPing(String ip) {
        Process process = null;
        try {
            // 2 次 取平均, 參數 -c 2
            process = new ProcessBuilder().command("/system/bin/ping", "-c 2", ip)
                    .redirectErrorStream(true)
                    .start();

            String strPing = "";
            InputStream in = process.getInputStream();
            OutputStream out = process.getOutputStream();
            InputStreamReader reader = new InputStreamReader(in, "utf-8");
            int i;
            while ((i = in.read()) != -1) {
                strPing = strPing + (char) i;
            }
            out.close();
            in.close();
            reader.close();
            if (strPing.contains("ttl") && strPing.contains("min/avg/max/mdev")) {
                try {
                    String re = strPing.substring(strPing.lastIndexOf("=") + 1, strPing.lastIndexOf("ms"));
                    String[] resplit = re.split("/");
                    speed = Double.parseDouble(resplit[1]);
                    Log.d(TAG, "run ping : " + speed + " ,with ip:" + ip);
                    addSpeed(speed);
                    if (speed > threshold) {
                        reportStatus("long response time", String.valueOf(speed));
                    }
                } catch (Exception e) {

                }
            } else {
                throw new Exception("no resp");
            }
        } catch (Exception e) {
            Log.e(TAG, "ping error = " + e.getMessage());
            reportStatus("ping error", e.getMessage());
            addSpeed(threshold);
        } finally {
            if (process != null) {
                process.destroy();
                process = null;
            }
        }
    }

    private static int uploadCount;

    private static void reportStatus(String result, String message) {
        Log.d(TAG, "reportStatus, result = " + result + ", message = " + message);
        uploadCount++;
        if (uploadCount % 7 == 0) {
            SpotDeviceLog.error(TAG, result, message);
            uploadCount = 0;
        }
    }
}
