package ai.aiello.aiellospot.core;

import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.events.system.SystemEvent;
import ai.aiello.aiellospot.modules.ads.AdsModuleManager;

import android.os.CountDownTimer;

import com.litesuits.android.log.Log;

import org.greenrobot.eventbus.EventBus;

public class MCountdownTimer {

    private static final String TAG = MCountdownTimer.class.getSimpleName();


    private static CountDownTimer countDownTimer;
    private static final int standbyViewSecond = 10 * 60; // unit: second
    public static int defaultSilentBackSecond = 60;
    private static CountDownState current = CountDownState.standby;

    enum CountDownState {
        standby, dismiss
    }

    public static void startStandbyTimer() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        countDownTimer = null;
        android.util.Log.d(TAG, "startStandbyTimer : 600s");
        current = CountDownState.standby;
        if (ActivityLauncher.isPocastActivityOnFront() || ActivityLauncher.isStandbyActivityOnFront()) {
            return;
        }
        countDownTimer = new CountDownTimer(standbyViewSecond * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
//                Log.d(TAG, "go to standby after " + millisUntilFinished / 1000 + "s");
                if ((millisUntilFinished / 1000) % 30 == 0) {
                    Log.d(TAG, "go to standby after " + millisUntilFinished / 1000 + "s");
                }
            }

            @Override
            public void onFinish() {
                Log.d(TAG, "sm.getPlayList().size() = " + AdsModuleManager.getInstance().getPlayList().size());
                if (AdsModuleManager.getInstance().isReady()) {
                    EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.TIME_DISMISS));
                } else {
                    startStandbyTimer();
                }
//                StandbyManager sm = StandbyManager.getInstance();
//                sm.getAds(new StandbyManager.OnQueryFinishListener() {
//                    @Override
//                    public void onFinish() {
//                        Log.d(TAG, "sm.getPlayList().size() = " + sm.getPlayList().size());
//                        if (sm.getPlayList().size() > 0) {
//                            EventBus.getDefault().post(new CounterEvent(CounterEvent.STANDBY));
//                        } else {
//                            startStandbyTimer();
//                        }
//                    }
//                });
            }
        }.start();

    }

    private static int savedSilentBackSecond;

    public static void startDismissTimer(int silentBackSecond) {
        savedSilentBackSecond = silentBackSecond;
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        countDownTimer = null;
        android.util.Log.d(TAG, "startDismissTimer : " + silentBackSecond +"s");
        current = CountDownState.dismiss;
        if (ActivityLauncher.isLaundryPriceActivityOnFront()) {
            return;
        }
        countDownTimer = new CountDownTimer(silentBackSecond * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
//                Log.d(TAG, "go to dismiss after " + millisUntilFinished / 1000 + "s");
                if ((millisUntilFinished / 1000) % 10 == 0) {
                    Log.d(TAG, "go to dismiss after " + millisUntilFinished / 1000 + "s");
                }
            }

            @Override
            public void onFinish() {
                EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.TIME_DISMISS));
            }
        }.start();
    }

    public static void startTimer() {
        android.util.Log.d(TAG, "resumeTimer");
        if (current == CountDownState.standby) {
            startStandbyTimer();
        } else {
            startDismissTimer(savedSilentBackSecond);
        }
    }

    public static void stopTimer() {
        android.util.Log.d(TAG, "stopTimer");
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        countDownTimer = null;
    }

}
