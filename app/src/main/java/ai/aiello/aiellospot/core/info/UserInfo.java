package ai.aiello.aiellospot.core.info;

import com.litesuits.android.log.Log;
import com.litesuits.common.io.FileUtils;

import org.json.JSONObject;

import java.io.File;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.modules.alarm.UserAlarmModuleManager;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;

public class UserInfo {

    private static final String TAG = UserInfo.class.getSimpleName();
    public static String userName = "default_user_name";
    public static String userGender = "default_user_gender";
    public static String userNationality = "default_user_nationality";
    public static String userLanguage = "default_user_language";

    private static final String TAG_ACTIVITY = "TAG_ACTIVITY";
    private static final String TAG_Name = "TAG_Name";
    private static final String TAG_Gender = "TAG_Gender";
    private static final String TAG_Nationality = "TAG_Nationality";
    private static final String TAG_Language = "TAG_Language";

    public static ChatApplication.Language curLang;

    private static File userStage = FileUtils.getFile("/sdcard/userStage.txt");
    public static String curActivity;

    //load after success init
    public static boolean restoreStage() {
        boolean restored = false;
        try {
            String stage_msg = FileUtils.readFileToString(userStage);
            Log.d(TAG, "restoreStage = " + stage_msg);
            if (stage_msg != null) {
                JSONObject userInfoHistory = new JSONObject(stage_msg);
                curActivity = "ai.aiello.aiellospot.views.activity.home.MainHomeActivity";
                if (userInfoHistory.getString(TAG_Language).isEmpty() || userInfoHistory.getString(TAG_Language).equals("None")) {
                    throw new Exception("no user language info");
                }
                curLang = ChatApplication.Language.valueOf(userInfoHistory.getString(TAG_Language));
                userName = userInfoHistory.getString(TAG_Name);
                userNationality = userInfoHistory.getString(TAG_Nationality);
                userGender = userInfoHistory.getString(TAG_Gender);
                FileUtils.writeStringToFile(userStage, userInfoHistory.toString());
                restored = true;
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        } finally {
            FileUtils.deleteQuietly(userStage);
        }
        return restored;
    }

    public static void saveStage() {

        if (ChatApplication.system_lang == null) {
            return;
        }

        try {
            String activity_name = ActivityLauncher.currentHomeName.getName();
            JSONObject userInfoHistory = new JSONObject();
            userInfoHistory.put(TAG_ACTIVITY, activity_name);
            userInfoHistory.put(TAG_Language, ChatApplication.system_lang);
            userInfoHistory.put(TAG_Name, userName);
            userInfoHistory.put(TAG_Nationality, userNationality);
            userInfoHistory.put(TAG_Gender, userGender);
            android.util.Log.d(TAG, "saveStage:"+userInfoHistory);
            FileUtils.writeStringToFile(userStage, userInfoHistory.toString());
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }

    }

    public static void clearStage() {
        android.util.Log.d(TAG, "clearStage");
        FileUtils.deleteQuietly(userStage);
        curActivity = null;
        curLang = null;
        try {
            Thread.sleep(500); // wait shared pref io
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
