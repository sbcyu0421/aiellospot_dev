package ai.aiello.aiellospot.core.ota;

import ai.aiello.aiellospot.R;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class ListAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<ApkItem> apkList;


    public ListAdapter(ArrayList<ApkItem> apkList, LayoutInflater inflater) {
        this.inflater = inflater;
        this.apkList = apkList;
    }

    @Override
    public int getCount() {
        return apkList.size();
    }

    @Override
    public Object getItem(int position) {
        return apkList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.apk_list_item, null);
            holder.tv_version = convertView.findViewById(R.id.tv_version);
            holder.tv_note = convertView.findViewById(R.id.tv_note);
            holder.item_box = convertView.findViewById(R.id.item_box);
            holder.tv_currentVersion = convertView.findViewById(R.id.tv_currentVersion);
            holder.releaseDate = convertView.findViewById(R.id.releaseDate);
            holder.tv_id = convertView.findViewById(R.id.tv_id);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tv_version.setText(apkList.get(position).getVersionName());
        holder.tv_note.setText(apkList.get(position).getNote());
        holder.releaseDate.setText(apkList.get(position).getReleaseDate());
        holder.tv_id.setText(apkList.get(position).getId()+"");

        if (apkList.get(position).getCurrentVersion() == 1) {
            holder.tv_currentVersion.setText("現行版本");
        } else {
            holder.tv_currentVersion.setText("");
        }


        return convertView;

    }


    static class ViewHolder {
        TextView tv_version;
        TextView tv_note;
        TextView releaseDate;
        TextView tv_currentVersion;
        TextView tv_id;
        LinearLayout item_box;

    }


}
