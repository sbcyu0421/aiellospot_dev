package ai.aiello.aiellospot.core.messager.daemon;

import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import org.json.JSONObject;

import java.time.Instant;

import ai.aiello.aiellospot.core.messager.message.MessageFactory;
import ai.aiello.aiellospot.core.messager.MessageManager;
import ai.aiello.aiellospot.core.system.BatteryListener;

public class KeepAliver {

    private static final String TAG = KeepAliver.class.getSimpleName();
    Handler handler;
    private int freq;
    private boolean isWorking = false;

    private static final int EXPIRED_TIME_BIAS = 15;

    public KeepAliver(int freq) {
        Log.d(TAG, "init KeepAliver freq = " + freq);
        this.freq = freq;
        MessageManager.getInstance().initAliveProducer();
        HandlerThread handlerThread = new HandlerThread("keepAlive");
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());
        handler.post(new AliveRunnable());
    }

    public void updateFreq(int freq){
        this.freq = freq;
    }

    public void start() {
        isWorking = true;
    }

    public void stop() {
        isWorking = false;
    }

    class AliveRunnable implements Runnable {

        @Override
        public void run() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                try {
                    JSONObject dataObj = new JSONObject();
                    dataObj.put("POWER", BatteryListener.powerState);
                    dataObj.put("VALUE", BatteryListener.currentElectricity);
                    MessageManager.getInstance().send(
                            MessageManager.ProducerType.KEEP_ALIVE,
    //                        MessageFactory.createAliveMsg(Instant.now().getEpochSecond() + EXPIRED_TIME_BIAS));
                            MessageFactory.createAliveMsg(Instant.now().getEpochSecond(), dataObj));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (isWorking) {
                handler.postDelayed(this, freq);
            }
        }
    }

}