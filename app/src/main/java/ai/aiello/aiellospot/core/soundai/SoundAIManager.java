package ai.aiello.aiellospot.core.soundai;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.views.activity.new_ota.VersionCheckerT;
import ai.aiello.aiellospot.events.system.SystemEvent;
import ai.aiello.aiellospot.events.system.WakeUpEvent;
import ai.aiello.aiellospot.core.asr.ASRManager;
import ai.aiello.aiellospot.core.config.Configer;
import ai.aiello.aiellospot.core.info.DeviceInfo;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.utlis.TestHub;
import ai.aiello.soundaiservice.ISAIClientCallback;
import ai.aiello.soundaiservice.ISoundAIService;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;

import com.litesuits.android.log.Log;

import org.greenrobot.eventbus.EventBus;


public class SoundAIManager {

    private static ISoundAIService iSoundAIService;
    private static final String TAG = SoundAIManager.class.getSimpleName();

    //old function
    private static OnVoipListener onVoipListener;

    private static boolean wakeUpEnable = true;
    private static boolean wakeupBackUp;
    public static boolean onVoip = false;
    private static float s_wakeupAngle = 0;
    private static boolean imgChannelAligned = false;
    private Context context;
    public static ServiceConnection saiServiceConn;

    public static void spotOnASR(boolean b) {
        try {
            iSoundAIService.spotOnASR(b);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public interface OnVoipListener {
        void onVoip(byte[] buffer, int size);
    }

    private static Thread timeoutThread = null;

    private static void startOnASRCallbackMonitor() {
        stopOnASRCallbackMonitor();
        timeoutThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3 * 1000);
                    if (!ASRManager.getInstance().getOnASR()) {
                        stopBeam();
                        SpotDeviceLog.info(TAG, "", "stop beam because asr card not showing");
                        Log.e(TAG, "stop beam because asr card not showing");
                    }
                } catch (Exception ignore) {

                }
            }
        });
        timeoutThread.start();
    }

    private static void stopOnASRCallbackMonitor() {
        if (timeoutThread != null) {
            Log.d(TAG, "OnASRCallbackMonitor");
            ChatApplication.emitQueue.clear();
            timeoutThread.interrupt();
            timeoutThread = null;
        }
    }


    public static void setOnSAIVoipListener(OnVoipListener listener) {
        onVoipListener = listener;
    }


    public SoundAIManager(Context context, String saiModel) {
        this.context = context;
        String img_ver = VersionCheckerT.getSystemImageVersion();
        String major_str = img_ver.substring(0, img_ver.indexOf('.'));
        img_ver = img_ver.substring(major_str.length() + 1);
        String minor_str = img_ver.substring(0, img_ver.indexOf('.'));
        img_ver = img_ver.substring(minor_str.length() + 1);
        String patch_str = img_ver.substring(0, img_ver.indexOf('_'));
        int major_ver = Integer.parseInt(major_str);
        int minor_ver = Integer.parseInt(minor_str);
        int patch_ver = Integer.parseInt(patch_str);

        try {
            imgChannelAligned = (major_ver > 1 || minor_ver > 0 || patch_ver >= 4);
            Log.d(TAG, "imgChannelAligned = " + imgChannelAligned);
        } catch (Exception ex) {
            Log.e("Image Version", "Error parsing Image version");
            SpotDeviceLog.exception(TAG, "", ex.getMessage());
        }

        saiServiceConn = new ServiceConnection() {

            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                Log.d(TAG, "onServiceConnected");
                iSoundAIService = ISoundAIService.Stub.asInterface(iBinder);
                try {
                    Log.d(TAG, "saiModel = " + saiModel);
                    iSoundAIService.registerCallback(mCallback);
                    iSoundAIService.initSaiClient(imgChannelAligned, DeviceInfo.MAC, saiModel);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                Log.e(TAG, "onSAIServiceDisconnected");
                iSoundAIService = null;
                bindService();
            }
        };

        bindService();
    }

    public void bindService() {
        Intent intent = new Intent("ai.aiello.soundaiservice.ISoundAIService");
        intent.putExtra(Configer.KEY_CONFIG, Configer.getClientServiceConfig());
        intent.setPackage("ai.aiello.soundaiservice");
        context.bindService(intent, saiServiceConn, Context.BIND_AUTO_CREATE);
    }


    private final ISAIClientCallback.Stub mCallback = new ISAIClientCallback.Stub() {

        @Override
        public void onAsrDataCallback(byte[] buffer, int size) throws RemoteException {
            byte[] onasr_temp = buffer.clone();
            ChatApplication.emitQueue.offer(onasr_temp);
        }

        @Override
        public void onWakeupCallback(float wakeup_angle, String wakeup_word, float score) throws RemoteException {
            Log.d(TAG, "client onWakeupCallback");
            ChatApplication.emitQueue.clear();
            if (TestHub.debug_mode) {
                try {
                    TestHub.SendWakeUpEvent();
                } catch (Exception ex) {
                    Log.e("AutoTest", "FAIL:" + ex.toString());
                    SpotDeviceLog.exception(TAG, "", ex.getMessage());
                }
            }
            s_wakeupAngle = wakeup_angle;
            if (wakeUpEnable) {
                EventBus.getDefault().post(new WakeUpEvent());
                startOnASRCallbackMonitor();
                Log.e(TAG, "wake up success");
            } else {
                stopBeam();
                Log.e(TAG, "WAKEUP_BANNED BY SPOT");
                SpotDeviceLog.info(TAG, "", "WAKEUP_BANNED BY SPOT");
            }

        }

        @Override
        public void onVoipDataCallback(byte[] buffer, int size) throws RemoteException {
            Log.d(TAG, "client onVoipDataCallback");
            byte[] voip_temp = buffer.clone();
            if (onVoipListener != null) {
                onVoipListener.onVoip(voip_temp, size);
            }
        }

        @Override
        public void onVadCallback(int type) throws RemoteException {
            Log.d(TAG, "client onVadCallback, type =" + type);
        }
    };


    public static void startBeam() {
        try {
            Log.d(TAG, "startBeam");
            iSoundAIService.startBeam(s_wakeupAngle);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public static void stopBeam() {
        try {
            Log.d(TAG, "stopBeam");
            iSoundAIService.stopBeam();
            stopOnASRCallbackMonitor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void startVoip() {
        if (onVoip) return;
        wakeupBackUp = isWakeUpEnable();
        try {
            iSoundAIService.startVoip();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        onVoip = true;
        setWakeUp(null, false);
    }

    public static void stopVoip() {
        if (!onVoip) return;
        try {
            iSoundAIService.stopVoip();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        onVoip = false;
        setWakeUp(null, wakeupBackUp);
    }

    public void release() {
        try {
            iSoundAIService.release();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "saiClient has been released");
    }


    public static void setWakeUp(Activity activity, boolean enable) {
        if (enable) {
            wakeUpEnable = true;
            if (activity != null) {
                Log.w(TAG, activity.getClass().getSimpleName() + " setWakeUpEnable");
                EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.WAKEUP_ENABLE));
            }
        } else {
            wakeUpEnable = false;
            if (activity != null) {
                Log.w(TAG, activity.getClass().getSimpleName() + " setWakeUpDisable");
                EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.WAKEUP_DISABLE));
            }
        }
    }

    public static boolean isWakeUpEnable() {
        return wakeUpEnable;
    }


    public static void changeWakeupWord(String saiModel) {
        try {
            Log.d(TAG, "change saiModel to = " + saiModel);
            iSoundAIService.changeWakeupWord(saiModel);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

}
