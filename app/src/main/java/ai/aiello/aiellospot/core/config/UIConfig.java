package ai.aiello.aiellospot.core.config;

public class UIConfig {

       public static class SurroundingModule {
    }

    public static WakUpConfigBean wakupConfigBean;

    public static class WakUpConfigBean {
        /**
         * wakeupWord : {"tw":"小美犀","cn":"小美犀","ja":"小美犀","en":"Hello AI"}
         * wakeupWordSelf : {"tw":"小犀管家","cn":"小犀管家","ja":"Aiello","en":"Aiello"}
         * saiModel : {"tw":"xiaomeixi","cn":"xiaomeixi","ja":"helloai","en":"helloai"}
         * tutorAudio : {"tw":"xiaomeixi","cn":"xiaomeixi","ja":"helloai","en":"helloai"}
         * tutorText : {"tw":"xiaomeixi","cn":"xiaomeixi","ja":"helloai","en":"helloai"}
         */

        private WakeupWordBean wakeupWord;
        private WakeupWordSelfBean wakeupWordSelf;
        private SaiModelBean saiModel;
        private TutorAudioBean tutorAudio;
        private TutorTextBean tutorText;

        public WakeupWordBean getWakeupWord() {
            return wakeupWord;
        }

        public void setWakeupWord(WakeupWordBean wakeupWord) {
            this.wakeupWord = wakeupWord;
        }

        public WakeupWordSelfBean getWakeupWordSelf() {
            return wakeupWordSelf;
        }

        public void setWakeupWordSelf(WakeupWordSelfBean wakeupWordSelf) {
            this.wakeupWordSelf = wakeupWordSelf;
        }

        public SaiModelBean getSaiModel() {
            return saiModel;
        }

        public void setSaiModel(SaiModelBean saiModel) {
            this.saiModel = saiModel;
        }

        public TutorAudioBean getTutorAudio() {
            return tutorAudio;
        }

        public void setTutorAudio(TutorAudioBean tutorAudio) {
            this.tutorAudio = tutorAudio;
        }

        public TutorTextBean getTutorText() {
            return tutorText;
        }

        public void setTutorText(TutorTextBean tutorText) {
            this.tutorText = tutorText;
        }

        public static class WakeupWordBean {
            /**
             * tw : 小美犀
             * cn : 小美犀
             * ja : 小美犀
             * en : Hello AI
             */

            private String tw;
            private String cn;
            private String ja;
            private String en;

            public String getTw() {
                return tw;
            }

            public void setTw(String tw) {
                this.tw = tw;
            }

            public String getCn() {
                return cn;
            }

            public void setCn(String cn) {
                this.cn = cn;
            }

            public String getJa() {
                return ja;
            }

            public void setJa(String ja) {
                this.ja = ja;
            }

            public String getEn() {
                return en;
            }

            public void setEn(String en) {
                this.en = en;
            }
        }

        public static class WakeupWordSelfBean {
            /**
             * tw : 小犀管家
             * cn : 小犀管家
             * ja : Aiello
             * en : Aiello
             */

            private String tw;
            private String cn;
            private String ja;
            private String en;

            public String getTw() {
                return tw;
            }

            public void setTw(String tw) {
                this.tw = tw;
            }

            public String getCn() {
                return cn;
            }

            public void setCn(String cn) {
                this.cn = cn;
            }

            public String getJa() {
                return ja;
            }

            public void setJa(String ja) {
                this.ja = ja;
            }

            public String getEn() {
                return en;
            }

            public void setEn(String en) {
                this.en = en;
            }
        }

        public static class SaiModelBean {
            /**
             * tw : xiaomeixi
             * cn : xiaomeixi
             * ja : helloai
             * en : helloai
             */

            private String tw;
            private String cn;
            private String ja;
            private String en;

            public String getTw() {
                return tw;
            }

            public void setTw(String tw) {
                this.tw = tw;
            }

            public String getCn() {
                return cn;
            }

            public void setCn(String cn) {
                this.cn = cn;
            }

            public String getJa() {
                return ja;
            }

            public void setJa(String ja) {
                this.ja = ja;
            }

            public String getEn() {
                return en;
            }

            public void setEn(String en) {
                this.en = en;
            }
        }

        public static class TutorAudioBean {
            /**
             * tw : xiaomeixi
             * cn : xiaomeixi
             * ja : helloai
             * en : helloai
             */

            private String tw;
            private String cn;
            private String ja;
            private String en;

            public String getTw() {
                return tw;
            }

            public void setTw(String tw) {
                this.tw = tw;
            }

            public String getCn() {
                return cn;
            }

            public void setCn(String cn) {
                this.cn = cn;
            }

            public String getJa() {
                return ja;
            }

            public void setJa(String ja) {
                this.ja = ja;
            }

            public String getEn() {
                return en;
            }

            public void setEn(String en) {
                this.en = en;
            }
        }

        public static class TutorTextBean {
            /**
             * tw : xiaomeixi
             * cn : xiaomeixi
             * ja : helloai
             * en : helloai
             */

            private String tw;
            private String cn;
            private String ja;
            private String en;

            public String getTw() {
                return tw;
            }

            public void setTw(String tw) {
                this.tw = tw;
            }

            public String getCn() {
                return cn;
            }

            public void setCn(String cn) {
                this.cn = cn;
            }

            public String getJa() {
                return ja;
            }

            public void setJa(String ja) {
                this.ja = ja;
            }

            public String getEn() {
                return en;
            }

            public void setEn(String en) {
                this.en = en;
            }

        }
    }

}
