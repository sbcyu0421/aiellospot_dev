package ai.aiello.aiellospot.core.messager;

import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ArrayBlockingQueue;

import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.core.messager.kafka.KafkaMsg;

public class MessageFilter extends Thread {
    private String TAG = MessageFilter.class.getSimpleName();
    private ArrayBlockingQueue<JSONObject> msgQueue;


    public MessageFilter(ArrayBlockingQueue<JSONObject> msgQueue) {
        this.msgQueue = msgQueue;
    }

    @Override
    public void run() {
        super.run();
        while (true) {
            try {
                JSONObject tmp = msgQueue.take();
                if (!filterMsg(tmp)) continue;
                Log.d(TAG, "get msg = " + tmp);
                EventBus.getDefault().post(new KafkaMsg(tmp));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean filterMsg(JSONObject msg) {
        try {
            String roomName = msg.getString("ROOM");
            String mac = msg.getString("DEVICE");

            return roomName.equals("ALL") ||  //for broadcast
                    roomName.equals(DeviceInfo.roomName) && mac.equals("ALL") ||  //for room devices
                    roomName.equals(DeviceInfo.roomName) && mac.equals(DeviceInfo.MAC); //for this device only
        } catch (JSONException e) {
            Log.e(TAG, "Filter msg error = " + e);
            return false;
        }
    }
}
