package ai.aiello.aiellospot.core.messager.message;

import android.os.Build;

import androidx.annotation.RequiresApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.Instant;
import java.util.Date;

import ai.aiello.aiellospot.core.info.DeviceInfo;

public class MessageFactory {
    private static String msgIDPrefix = "";
    private static String mac = "";
    private static String source = "";
    private static String hotelName = "";
    private static String roomName = "";

    public static void init(String msgIDPrefix, String mac, String source, String hotelName,
                            String roomName) {
        MessageFactory.msgIDPrefix = msgIDPrefix;
        MessageFactory.mac = mac;
        MessageFactory.source = source;
        MessageFactory.hotelName = hotelName;
        MessageFactory.roomName = roomName;
    }

    public static class AielloData extends JSONObject {
        protected AielloData() {

        }
    }

    /**
     * topic: Alive, code: AliveTypeList_spotConnected = 6000
     *
     * @param expiredTime If there are no new alive message and reach expiredTime, server will think Spot shutdown
     * @return Alive message
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    public static AielloData createAliveMsg(long expiredTime, JSONObject data) {
        AielloData msg = initMsg(MassageCode.ALIVETYPELIST_SPOTCONNECTED);
        try {
            msg.put("DEVICE", mac);
            msg.put("HOTEL", hotelName);
            msg.put("ROOM", roomName);
            msg.put("EXPIREDTIME", expiredTime);
            msg.put("DATA", data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return msg;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private static AielloData initMsg(int code) {
        AielloData msg = new AielloData();
        try {
            msg.put("ID", msgIDPrefix);
            msg.put("TS", Instant.now().getEpochSecond()); // timeStamp
            msg.put("TYPE", code);
            msg.put("SOURCE", source);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return msg;
    }

    /**
     * @param iotData IoT status information
     * @return Iot Log Message
     */
    public static AielloData createIoTLogMsg(JSONArray iotData) {
        AielloData logMsg = new AielloData();
        try {
            logMsg.put("HotelName", hotelName);
            logMsg.put("RoomName", roomName);
            logMsg.put("LogTime", new Date());
            logMsg.put("Devices", iotData);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return logMsg;
    }


    //region ==== other message example ====
    // topic: <Hotel>_management, code:100X
    public static AielloData createSystemMgmtMsg(int type) {
        AielloData msg = initMsg(type);
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("DEVICE", mac);
            dataJson.put("ROOM", roomName);
            msg.put("DATA", dataJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return msg;
    }

    // topic: <Hotel>_device, code:100X
    public static AielloData createSystemDeviceMsg(int type) {
        AielloData msg = initMsg(type);
        JSONObject dataJson = new JSONObject();
        try {
            msg.put("DEVICE", mac);
            msg.put("ROOM", roomName);
            msg.put("DATA", dataJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return msg;
    }

    // topic: <Hotel>_management, code:400x
    public static AielloData createRoomMgmtMsg(int type, boolean action) {
        AielloData msg = initMsg(type);
        JSONObject dataJson = new JSONObject();
        try {
            dataJson.put("DEVICE", mac);
            dataJson.put("ROOM", roomName);
            dataJson.put("ACTION", action);
            msg.put("DATA", dataJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return msg;
    }

    // topic: <Hotel>_device code:400x
    public static AielloData createRoomDeviceMsg(int type, String action) {
        AielloData msg = initMsg(type);
        JSONObject dataJson = new JSONObject();
        try {
            msg.put("DEVICE", mac);
            msg.put("ROOM", roomName);
            dataJson.put("ACTION", action);
            msg.put("DATA", dataJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return msg;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static AielloData create3rdMsg(int code, JSONObject dataObject) {
        AielloData msg = initMsg(code);
        try {
            msg.put("DATA", dataObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return msg;
    }
    //endregion

}