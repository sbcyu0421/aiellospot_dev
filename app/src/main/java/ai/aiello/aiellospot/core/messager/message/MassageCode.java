package ai.aiello.aiellospot.core.messager.message;

public class MassageCode {
    // Action Category
    public static final int SYSTEM_CATEGORY = 1000;
    public static final int EMPLOYEE_CATEGORY = 2000;
    public static final int CUSTOMER_CATEGORY = 3000;
    public static final int ROOM_CATEGORY = 4000;
    public static final int TASK_CATEGORY = 5000;
    public static final int ALIVE_CATEGORY = 6000;
    public static final int THIRD_PARTY_CATEGORY = 7000;

    // System Action code 1000
    public static final int SYSTEMTYPELIST_IMAGECHECK = 1000;
    public static final int SYSTEMTYPELIST_IMAGEUPDATE = 1001;
    public static final int SYSTEMTYPELIST_APPREBOOT = 1002;
    public static final int SYSTEMTYPELIST_SILENTRESTART = 1003;
    public static final int SYSTEMTYPELIST_DEVICEREBOOT = 1004;
    public static final int SYSTEMTYPELIST_LOGUPLOAD = 1005;
    public static final int SYSTEMTYPELIST_RELOAD = 1006;
    public static final int SYSTEMTYPELIST_IOTCONFIG_RELOAD = 1007;
    public static final int SYSTEMTYPELIST_SPOT_CONFIG_HOT_RELOAD = 1101;
    public static final int REMOTE_WAKE_UP = 1102;
    public static final int FORCE_OTA = 1103;

    // Employee Action code 2000
    public static final int EMPLOYEETYPELIST_REGISTER = 2000;
    public static final int EMPLOYEETYPELIST_UNREGISTER = 2001;
    public static final int EMPLOYEETYPELIST_LOGIN = 2002;
    public static final int EMPLOYEETYPELIST_LOGOUT = 2003;

    // Customer Action code 3000
    public static final int CUSTOMERTYPELIST_CHECKIN = 3000;
    public static final int CUSTOMERTYPELIST_CHECKOUT = 3001;
    public static final int CUSTOMERTYPELIST_NOTIFYSPOT = 3002;
    public static final int CUSTOMERTYPELIST_NOTIFYROOMGROUP = 3003;
    public static final int CUSTOMERTYPELIST_NOTIFYPORTAL = 3004;
    public static final int CUSTOMERTYPELIST_NOTIFYBOTH = 3005;
    public static final int CUSTOMERTYPELIST_IOT = 3006;
    public static final int CUSTOMERTYPELIST_PBR_AUTHORIZED = 3007;

    // Room Action code 4000
    public static final int ROOMTYPELIST_UNAVAILABLE = 4000;
    public static final int ROOMTYPELIST_AVAILABLE = 4001;
    public static final int ROOMTYPELIST_OCCUPIED = 4002;
    public static final int ROOMTYPELIST_NODISTURB = 4003;
    public static final int ROOMTYPELIST_CLEAN = 4004;
    public static final int ROOMTYPELIST_SERVICE = 4005;
    public static final int ROOMTYPELIST_INSERVICE = 4006;
    public static final int ROOMTYPELIST_REPAIR = 4007;
    public static final int ROOMTYPELIST_COMPLAINT = 4008;
    public static final int ROOMTYPELIST_ELECTRICITY = 4009;
    public static final int ROOMTYPELIST_SATISFACTION = 4010;
    public static final int ROOMTYPELIST_PORTALSENDDATA = 4011;

    // Task Action code 5000
    public static final int TASKTYPELIST_ASSIGNTASK = 5000;
    public static final int TASKTYPELIST_COMPLETEDTASK = 5001;

    // Alive Action code 6000
    public static final int ALIVETYPELIST_SPOTCONNECTED = 6000;
    public static final int ALIVETYPELIST_SPOTDISCONNECTED = 6001;

    // Third Party Case code 7000
    public static final int THIRD_PARTY_CHECK_IN = 7000;
    public static final int THIRD_PARTY_CHECK_OUT = 7004;

}
