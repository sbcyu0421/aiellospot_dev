package ai.aiello.aiellospot.core.chatbot;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ai.aiello.aiellospot.intents.Classifier;
import ai.aiello.aiellospot.intents.IntentManager;
import ai.aiello.aiellospot.intents.IntentObject;
import ai.aiello.aiellospot.core.config.APIConfig;
import ai.aiello.aiellospot.core.info.DeviceInfo;

import ai.aiello.aiellospot.core.messager.MessageManager;
import ai.aiello.aiellospot.utlis.Http;

public class ChatbotValidator extends Thread {

    private static final String TAG = ChatbotValidator.class.getSimpleName();
    private Context context;


    public ChatbotValidator(Context context) {
        this.context = context;
    }

    private CountDownLatch latch;

    private static boolean active = false;

    public void setActive(boolean enable) {
        active = enable;
    }

    @Override
    public void run() {
        super.run();
        try {
            Log.i(TAG, "chatbot test start");
            setActive(true);
            ExecutorService cachedThreadExecutor = Executors.newFixedThreadPool(10);

            //get test words
            String res = Http.get(
                    String.format(APIConfig.WEBAPI_CHATBOT_INTEGRATE.getUrl(), DeviceInfo.hotelID),
//                    String.format(APIConfig.WEBAPI_CHATBOT_INTEGRATE.getUrl(), 17),
                    10000, APIConfig.WEBAPI_CHATBOT_INTEGRATE.getKey()).response;

            JSONObject jsonObject = new JSONObject(res);
            int delay = jsonObject.getInt("delay");
            JSONArray jsonArray = jsonObject.getJSONArray("corpus");
            int total = jsonArray.length();
            for (int s = 0; s < total; s++) {
                try {

                    String test_words = jsonArray.getJSONObject(s).getString("text");
                    String language = jsonArray.getJSONObject(s).getString("languageCode");
                    boolean enable = jsonArray.getJSONObject(s).getBoolean("enable");

                    Log.d(TAG, String.format("execute test words ( %s / %s ) = %s ", s, total, test_words));

                    //start query
                    latch = new CountDownLatch(1);
                    cachedThreadExecutor.execute(new ChatBot.ChatBotRunnable(test_words, "", s, -1, latch, language));

                    //wait for chatbot result
                    try {
                        latch.await();
                    } catch (InterruptedException e) {
                        Log.e(TAG, e.toString());
                    }

                    //build intent
                    latch = new CountDownLatch(IntentManager.chatbotResult.size());
                    for (JSONObject jObj : IntentManager.chatbotResult) {
                        cachedThreadExecutor.execute(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    IntentManager.IntentStandardFormat standardIO = null;
                                    standardIO = ChatBot.parse(jObj);
                                    IntentObject io = Classifier.classify(standardIO);
                                    io.setEnable(enable);
                                    IntentManager.addTask(io);
                                } catch (Exception e) {
                                    Log.e(TAG, "future stop : " + e.toString());
                                } finally {
                                    latch.countDown();
                                }
                            }
                        });
                    }
                    //wait for build intent instance
                    try {
                        latch.await();
                    } catch (Exception e) {
                        Log.e(TAG, e.toString());
                    }

                    //execute intent
                    latch = new CountDownLatch(IntentManager.taskQueue.size());
                    for (IntentObject io : IntentManager.taskQueue) {
                        cachedThreadExecutor.execute(new Runnable() {
                            @Override
                            public void run() {
                                Log.i(TAG, "chatbot test execute " + io.getQuerytext());
                                io.execute(context, latch);
                            }
                        });
                    }

                    //wait for intent execute completed
                    try {
                        latch.await();
                    } catch (Exception e) {
                        Log.e(TAG, e.toString());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        Thread.sleep(delay);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    IntentManager.finishIntentList.clear();
                    IntentManager.chatbotResult.clear();
                    IntentManager.taskQueue.clear();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "chatbot test exception = " + e.toString());
        } finally {
            Log.i(TAG, "chatbot test completed");
            setActive(false);
        }
    }

    public static synchronized void exception(IntentObject intentObject) {
        if (!active) {
            return;
        }
        try {
            JSONObject exceptObject = buildData(intentObject);
            MessageManager.getInstance().send(MessageManager.ProducerType.CHATBOT_VALIDATOR, exceptObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static JSONObject buildData(IntentObject intentObject) throws Exception {
        JSONObject exceptObject = new JSONObject();
        exceptObject.put("query_test", intentObject.getQuerytext());
        exceptObject.put("device", DeviceInfo.MAC);
        exceptObject.put("hotel", DeviceInfo.hotelName);
        if (intentObject.getException() == null) {
            exceptObject.put("result", "success");
            exceptObject.put("exception", "");
        } else {
            exceptObject.put("result", "fail");
            exceptObject.put("exception", intentObject.getException());
            Log.e(TAG, String.format("test fail on = %s, exception = %s, isExpected = %s ",
                    intentObject.getQuerytext(),
                    intentObject.getException(),
                    intentObject.isEnable()));
        }
        exceptObject.put("ignore", intentObject.isEnable());
        exceptObject.put("rawData", intentObject.getChatbot_data());
        exceptObject.put("interactive", intentObject.get_intent_result());
        return exceptObject;
    }

}
