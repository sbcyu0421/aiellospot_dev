package ai.aiello.aiellospot.core.log;

import com.google.gson.annotations.SerializedName;

class UserLogBean {

    @SerializedName("event_trigger")
    private EventTriggerBean eventTrigger;
    @SerializedName("log_time")
    private String logTime;
    @SerializedName("state_overview")
    private StateOverviewBean stateOverview;
    @SerializedName("device_info")
    private DeviceInfoBean deviceInfo;
    @SerializedName("ID")
    private String id;

    public EventTriggerBean getEventTrigger() {
        return eventTrigger;
    }

    public void setEventTrigger(EventTriggerBean eventTrigger) {
        this.eventTrigger = eventTrigger;
    }

    public String getLogTime() {
        return logTime;
    }

    public void setLogTime(String logTime) {
        this.logTime = logTime;
    }

    public StateOverviewBean getStateOverview() {
        return stateOverview;
    }

    public void setStateOverview(StateOverviewBean stateOverview) {
        this.stateOverview = stateOverview;
    }

    public DeviceInfoBean getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfoBean deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static class EventTriggerBean {
        @SerializedName("intent")
        private IntentBean intent;
        @SerializedName("type")
        private String type;
        @SerializedName("request")
        private String request;
        @SerializedName("response")
        private String response;
        @SerializedName("data")
        private String data;

        public IntentBean getIntent() {
            return intent;
        }

        public void setIntent(IntentBean intent) {
            this.intent = intent;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getRequest() {
            return request;
        }

        public void setRequest(String request) {
            this.request = request;
        }

        public String getResponse() {
            return response;
        }

        public void setResponse(String response) {
            this.response = response;
        }

        public String getData() {
            return data;
        }

        public void setData(String data) {
            this.data = data;
        }

        public static class IntentBean {
            @SerializedName("subject")
            private String subject;
            @SerializedName("action")
            private String action;

            public String getSubject() {
                return subject;
            }

            public void setSubject(String subject) {
                this.subject = subject;
            }

            public String getAction() {
                return action;
            }

            public void setAction(String action) {
                this.action = action;
            }
        }
    }

    public static class StateOverviewBean {
        @SerializedName("Voip")
        private VoipBean voip;
        @SerializedName("Music")
        private MusicBean music;
        @SerializedName("device")
        private DeviceBean device;
        @SerializedName("user_info")
        private UserInfoBean userInfo;

        public VoipBean getVoip() {
            return voip;
        }

        public void setVoip(VoipBean voip) {
            this.voip = voip;
        }

        public MusicBean getMusic() {
            return music;
        }

        public void setMusic(MusicBean music) {
            this.music = music;
        }

        public DeviceBean getDevice() {
            return device;
        }

        public void setDevice(DeviceBean device) {
            this.device = device;
        }

        public UserInfoBean getUserInfo() {
            return userInfo;
        }

        public void setUserInfo(UserInfoBean userInfo) {
            this.userInfo = userInfo;
        }

        public static class VoipBean {
            @SerializedName("state_mode")
            private String stateMode;
            @SerializedName("info")
            private String info;
            @SerializedName("ip")
            private String ip;
            @SerializedName("connect")
            private String connect;

            public String getStateMode() {
                return stateMode;
            }

            public void setStateMode(String stateMode) {
                this.stateMode = stateMode;
            }

            public String getInfo() {
                return info;
            }

            public void setInfo(String info) {
                this.info = info;
            }

            public String getIp() {
                return ip;
            }

            public void setIp(String ip) {
                this.ip = ip;
            }

            public String getConnect() {
                return connect;
            }

            public void setConnect(String connect) {
                this.connect = connect;
            }
        }

        public static class MusicBean {
            @SerializedName("state_mode")
            private String stateMode;
            @SerializedName("source")
            private String source;
            @SerializedName("album_info")
            private String albumInfo;
            @SerializedName("track_info")
            private String trackInfo;

            public String getStateMode() {
                return stateMode;
            }

            public void setStateMode(String stateMode) {
                this.stateMode = stateMode;
            }

            public String getSource() {
                return source;
            }

            public void setSource(String source) {
                this.source = source;
            }

            public String getAlbumInfo() {
                return albumInfo;
            }

            public void setAlbumInfo(String albumInfo) {
                this.albumInfo = albumInfo;
            }

            public String getTrackInfo() {
                return trackInfo;
            }

            public void setTrackInfo(String trackInfo) {
                this.trackInfo = trackInfo;
            }
        }

        public static class DeviceBean {
            @SerializedName("power")
            private PowerBean power;
            @SerializedName("wifi")
            private WifiBean wifi;
            @SerializedName("sound")
            private SoundBean sound;
            @SerializedName("screen")
            private ScreenBean screen;
            @SerializedName("bluetooth")
            private BluetoothBean bluetooth;
            @SerializedName("wake_up")
            private WakeUpBean wakeUp;

            public PowerBean getPower() {
                return power;
            }

            public void setPower(PowerBean power) {
                this.power = power;
            }

            public WifiBean getWifi() {
                return wifi;
            }

            public void setWifi(WifiBean wifi) {
                this.wifi = wifi;
            }

            public SoundBean getSound() {
                return sound;
            }

            public void setSound(SoundBean sound) {
                this.sound = sound;
            }

            public ScreenBean getScreen() {
                return screen;
            }

            public void setScreen(ScreenBean screen) {
                this.screen = screen;
            }

            public BluetoothBean getBluetooth() {
                return bluetooth;
            }

            public void setBluetooth(BluetoothBean bluetooth) {
                this.bluetooth = bluetooth;
            }

            public WakeUpBean getWakeUp() {
                return wakeUp;
            }

            public void setWakeUp(WakeUpBean wakeUp) {
                this.wakeUp = wakeUp;
            }

            public static class PowerBean {
                @SerializedName("battery")
                private Integer battery;
                @SerializedName("charging")
                private Boolean charging;

                public Integer getBattery() {
                    return battery;
                }

                public void setBattery(Integer battery) {
                    this.battery = battery;
                }

                public Boolean getCharging() {
                    return charging;
                }

                public void setCharging(Boolean charging) {
                    this.charging = charging;
                }
            }

            public static class WifiBean {
                @SerializedName("wifiRSSI")
                private Integer wifiRSSI;
                @SerializedName("wifi_speed")
                private Double wifiSpeed;
                @SerializedName("wifiSSID")
                private String wifiSSID;

                public Integer getWifiRSSI() {
                    return wifiRSSI;
                }

                public void setWifiRSSI(Integer wifiRSSI) {
                    this.wifiRSSI = wifiRSSI;
                }

                public Double getWifiSpeed() {
                    return wifiSpeed;
                }

                public void setWifiSpeed(Double wifiSpeed) {
                    this.wifiSpeed = wifiSpeed;
                }

                public String getWifiSSID() {
                    return wifiSSID;
                }

                public void setWifiSSID(String wifiSSID) {
                    this.wifiSSID = wifiSSID;
                }
            }

            public static class SoundBean {
                @SerializedName("volume")
                private Integer volume;

                public Integer getVolume() {
                    return volume;
                }

                public void setVolume(Integer volume) {
                    this.volume = volume;
                }
            }

            public static class ScreenBean {
                @SerializedName("brightness")
                private Integer brightness;

                public Integer getBrightness() {
                    return brightness;
                }

                public void setBrightness(Integer brightness) {
                    this.brightness = brightness;
                }
            }

            public static class BluetoothBean {
                @SerializedName("enable")
                private String enable;
                @SerializedName("connection")
                private String connection;

                public String getEnable() {
                    return enable;
                }

                public void setEnable(String enable) {
                    this.enable = enable;
                }

                public String getConnection() {
                    return connection;
                }

                public void setConnection(String connection) {
                    this.connection = connection;
                }
            }

            public static class WakeUpBean {
                @SerializedName("enable")
                private Boolean enable;

                public Boolean getEnable() {
                    return enable;
                }

                public void setEnable(Boolean enable) {
                    this.enable = enable;
                }
            }
        }

        public static class UserInfoBean {
            @SerializedName("name")
            private String name;
            @SerializedName("gender")
            private String gender;
            @SerializedName("check_in")
            private Boolean checkIn;
            @SerializedName("country")
            private String country;
            @SerializedName("language")
            private String language;
            @SerializedName("check_io_time")
            private String checkIoTime;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public Boolean getCheckIn() {
                return checkIn;
            }

            public void setCheckIn(Boolean checkIn) {
                this.checkIn = checkIn;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getLanguage() {
                return language;
            }

            public void setLanguage(String language) {
                this.language = language;
            }

            public String getCheckIoTime() {
                return checkIoTime;
            }

            public void setCheckIoTime(String checkIoTime) {
                this.checkIoTime = checkIoTime;
            }
        }
    }

    public static class DeviceInfoBean {
        @SerializedName("APK_version")
        private APKVersionBean apkVersion;
        @SerializedName("image_version")
        private String imageVersion;
        @SerializedName("MAC")
        private String mac;
        @SerializedName("uuid")
        private String uuid;
        @SerializedName("room")
        private RoomBean room;

        public APKVersionBean getApkVersion() {
            return apkVersion;
        }

        public void setApkVersion(APKVersionBean apkVersion) {
            this.apkVersion = apkVersion;
        }

        public String getImageVersion() {
            return imageVersion;
        }

        public void setImageVersion(String imageVersion) {
            this.imageVersion = imageVersion;
        }

        public String getMac() {
            return mac;
        }

        public void setMac(String mac) {
            this.mac = mac;
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        public RoomBean getRoom() {
            return room;
        }

        public void setRoom(RoomBean room) {
            this.room = room;
        }

        public static class APKVersionBean {
            @SerializedName("spot_version")
            private String spotVersion;
            @SerializedName("music_version")
            private String musicVersion;
            @SerializedName("sai_version")
            private String saiVersion;
            @SerializedName("push_version")
            private String pushVersion;

            public String getSpotVersion() {
                return spotVersion;
            }

            public void setSpotVersion(String spotVersion) {
                this.spotVersion = spotVersion;
            }

            public String getMusicVersion() {
                return musicVersion;
            }

            public void setMusicVersion(String musicVersion) {
                this.musicVersion = musicVersion;
            }

            public String getSaiVersion() {
                return saiVersion;
            }

            public void setSaiVersion(String saiVersion) {
                this.saiVersion = saiVersion;
            }

            public String getPushVersion() {
                return pushVersion;
            }

            public void setPushVersion(String pushVersion) {
                this.pushVersion = pushVersion;
            }
        }

        public static class RoomBean {
            @SerializedName("hotel_name")
            private String hotelName;
            @SerializedName("room_no")
            private String roomNo;
            @SerializedName("room_type")
            private String roomType;

            public String getHotelName() {
                return hotelName;
            }

            public void setHotelName(String hotelName) {
                this.hotelName = hotelName;
            }

            public String getRoomNo() {
                return roomNo;
            }

            public void setRoomNo(String roomNo) {
                this.roomNo = roomNo;
            }

            public String getRoomType() {
                return roomType;
            }

            public void setRoomType(String roomType) {
                this.roomType = roomType;
            }
        }
    }
}
