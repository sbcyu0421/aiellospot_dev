package ai.aiello.aiellospot.core.config;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.utlis.Http;

public class Configer {

    private static final String TAG = Configer.class.getSimpleName();
    private final String url = "https://aiello-gate.azure-api.net/spot/config/v1/%s";
    private final String key = "c342b7abc99f46ae91206ff99345966a";
    public static final String KEY_CONFIG = "API_CONFIG_CONTENT";
    public static JSONArray jsonArray = new JSONArray();
    private static JSONObject subServiceData;
    private Context context;
    public static JSONObject jsonObject;
    public static boolean useProductAPI;

    public static JSONObject moduleConfigData;
    public static JSONObject layoutConfigData;

    /**
     * rebuild json for sub service use,
     */
    public static String getClientServiceConfig() {
        if (subServiceData == null) {
            subServiceData = new JSONObject();
            try {
                //upload api
                JSONObject uploadFtp = new JSONObject()
                        .put("id", "PUSH_SAI_MUSIC")
                        .put("api_name", "wakeup_file_record")
                        .put("url", SystemConfig.WakeupFtpBean.url)
                        .put("key", "")
                        .put("username", SystemConfig.WakeupFtpBean.username)
                        .put("password", SystemConfig.WakeupFtpBean.password);
                jsonArray.put(uploadFtp);

                //other apis
                JSONObject data = new JSONObject();
                data.put("api", jsonArray);
                subServiceData.put("data", data);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Log.d(TAG, "subServiceData = " + subServiceData.toString());
        return subServiceData.toString();
    }


    public interface ConfigStatusListener {

        void onStart();

        void onSuccess();

        void onRetry();

        void onDeviceNoFound();

        void onFail(String reason);
    }

    public enum Type {
        ota, service
    }

    public void getConfigs(String mac, int releaseType, Context context, ConfigStatusListener configStatusListener, Type type) {
        this.context = context;
        boolean production = (releaseType == 0 || releaseType == 2);
        useProductAPI = production;
        configStatusListener.onStart();
        try {
            String res = "";
            if (releaseType == 3) {
                res = getDevConfigFromLocal();
            } else {
                res = Http.get(String.format(url, mac), 60 * 1000, key).response;
            }
            if (res.isEmpty()) {
                configStatusListener.onRetry();
                return;
            }
            //check device verified
            jsonObject = new JSONObject(res);
            if (!jsonObject.getBoolean("success")) {
                configStatusListener.onDeviceNoFound();
                return;
            }
            //setup all configs
            switch (type) {
                case ota:
                    DeviceInfo.hotelID = jsonObject.getJSONObject("result").getInt("hotel_id");
                    enableOtaData(jsonObject.getJSONObject("result").getJSONObject("ota"), production);
                    break;
                case service:
                    DeviceInfo.hotelID = jsonObject.getJSONObject("result").getInt("hotel_id");
                    enableSystemData(jsonObject.getJSONObject("result").getJSONObject("system").toString(), production);
                    moduleConfigData = jsonObject.getJSONObject("result").getJSONObject("modules");
                    layoutConfigData = jsonObject.getJSONObject("result").getJSONObject("layout");
                    break;
            }
            configStatusListener.onSuccess();
        } catch (Exception e) {
            configStatusListener.onFail(e.toString());
            e.printStackTrace();
        }

    }

    private void enableOtaData(JSONObject jsonObject, boolean production) throws Exception {
        APIConfig.WEBAPI_OTA = APIConfig.WEBAPI.build(APIConfig.KEY_OTA,  jsonObject.getJSONObject(APIConfig.KEY_OTA), production);
        APIConfig.WEBAPI_POST_VERSION = APIConfig.WEBAPI.build(APIConfig.KEY_POST_VERSION, jsonObject.getJSONObject(APIConfig.KEY_POST_VERSION), production);
        APIConfig.WEBAPI_SPOT_NOTE = APIConfig.WEBAPI.build(APIConfig.KEY_SPOT_NOTE, jsonObject.getJSONObject(APIConfig.KEY_SPOT_NOTE), production);
        SystemConfig.OTA.apk_cloud = jsonObject.getString("apk_cloud");
        SystemConfig.OTA.apk_push_name = jsonObject.getString("apk_push_name");
        try {
            SystemConfig.OTA.support_action = SystemConfig.OTA.apk_push_name.contains("Ultron");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void reloadConfig(Context context) {
        Log.d(TAG, "reloadConfig");
        getConfigs(DeviceInfo.MAC, ChatApplication.releaseType, context, new Configer.ConfigStatusListener() {

            @Override
            public void onStart() {
                Log.d(TAG, "reloadConfig onStart");
            }

            @Override
            public void onRetry() {
                Log.d(TAG, "reloadConfig onRetry");
            }

            @Override
            public void onDeviceNoFound() {
                Log.d(TAG, "reloadConfig onDeviceNoFound");
            }

            @Override
            public void onFail(String reason) {
                Log.d(TAG, "reloadConfig onFail");
            }

            @Override
            public void onSuccess() {
                Log.d(TAG, "reloadConfig onSuccess");
                refreshAPI(useProductAPI);
            }

        }, Configer.Type.service);
    }

    public void refreshAPI(boolean useProductAPI) {
        Log.d(TAG, "refreshAPI");
        try {
            //todo refresh all module
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void enableSystemData(String res, boolean production) throws Exception {

        JSONObject jsonObject = new JSONObject(res);
        //1.1 serviceUrl - chatbot
        JSONObject obj_chatbot = jsonObject.getJSONObject("chatbot");
        SystemConfig.ChatbotBean.chatbotUrl = obj_chatbot.getString("url");
        SystemConfig.ChatbotBean.chatbotKey = obj_chatbot.getString("key");
        SystemConfig.ChatbotBean.chatbotCleanFormUrl = obj_chatbot.getString("cleanForm");
        APIConfig.WEBAPI_CHATBOT_INTEGRATE = APIConfig.WEBAPI.build(APIConfig.KEY_CHATBOT_INTEGRATE, jsonObject.getJSONObject(APIConfig.KEY_CHATBOT_INTEGRATE), production);
        if (!production) {
            SystemConfig.ChatbotBean.chatbotUrl = jsonObject.getJSONObject("chatbot").getString("urlTest");
            SystemConfig.ChatbotBean.chatbotCleanFormUrl = jsonObject.getJSONObject("chatbot").getString("cleanFormTest");
        }

        //1.4 serviceUrl - multi-server
        SystemConfig.multiService = jsonObject.getString("multipleIntent");

        //---------------------------------------------------------------------

        //2.1 wakeup_ftp
        JSONObject obj_wakeup_ftp = jsonObject.getJSONObject("wakeup_ftp");
        SystemConfig.WakeupFtpBean.url = obj_wakeup_ftp.getString("url");
        SystemConfig.WakeupFtpBean.username = obj_wakeup_ftp.getString("username");
        SystemConfig.WakeupFtpBean.password = obj_wakeup_ftp.getString("password");

        //2.1 log_ftp
        JSONObject obj_log_ftp = jsonObject.getJSONObject("log_ftp");
        SystemConfig.LogFtpBean.url = obj_log_ftp.getString("url");
        SystemConfig.LogFtpBean.username = obj_log_ftp.getString("username");
        SystemConfig.LogFtpBean.password = obj_log_ftp.getString("password");

        //2.1 asr - credential
        JSONObject asr = jsonObject.getJSONObject("asr");
        SystemConfig.AsrBean.asrObject = asr;
        SystemConfig.AsrBean.credentials_google = asr.getString("credentials_google");
        SystemConfig.AsrBean.credentials_xfyun = asr.getString("credentials_xfyun");
        SystemConfig.AsrBean.credentials_baidu = asr.getString("credentials_baidu");

        //2.1 asr = accent_map
        SystemConfig.AsrBean.customerAccentSupportList = asr.getString("accent_map");

        //2.2 asr - replace_phrases
        JSONObject obj_replace_phrases_tw = asr.getJSONObject("replace_phrases").getJSONObject("tw");
        JSONObject obj_replace_phrases_custom_tw = asr.getJSONObject("replace_phrases_custom").getJSONObject("tw");
        putReplacePhrases(obj_replace_phrases_tw, SystemConfig.AsrBean.replace_phrases_tw);
        putReplacePhrases(obj_replace_phrases_custom_tw, SystemConfig.AsrBean.replace_phrases_tw);

        JSONObject obj_replace_phrases_cn = asr.getJSONObject("replace_phrases").getJSONObject("cn");
        JSONObject obj_replace_phrases_custom_cn = asr.getJSONObject("replace_phrases_custom").getJSONObject("cn");
        putReplacePhrases(obj_replace_phrases_cn, SystemConfig.AsrBean.replace_phrases_cn);
        putReplacePhrases(obj_replace_phrases_custom_cn, SystemConfig.AsrBean.replace_phrases_cn);

        JSONObject obj_replace_phrases_en = asr.getJSONObject("replace_phrases").getJSONObject("en");
        JSONObject obj_replace_phrases_custom_en = asr.getJSONObject("replace_phrases_custom").getJSONObject("en");
        putReplacePhrases(obj_replace_phrases_en, SystemConfig.AsrBean.replace_phrases_en);
        putReplacePhrases(obj_replace_phrases_custom_en, SystemConfig.AsrBean.replace_phrases_en);

        JSONObject obj_replace_phrases_ja = asr.getJSONObject("replace_phrases").getJSONObject("japanese");
        JSONObject obj_replace_phrases_custom_ja = asr.getJSONObject("replace_phrases_custom").getJSONObject("japanese");
        putReplacePhrases(obj_replace_phrases_ja, SystemConfig.AsrBean.replace_phrases_ja);
        putReplacePhrases(obj_replace_phrases_custom_ja, SystemConfig.AsrBean.replace_phrases_ja);

        //2.3 kafka
        JSONObject kafka = jsonObject.getJSONObject("kafka");
        SystemConfig.Kafka.kafkaAPI = APIConfig.WEBAPI.build("kafka", kafka.getJSONObject("server"), Configer.useProductAPI);
        try {
            SystemConfig.Kafka.consumer_freq = kafka.getJSONObject("consumer").getInt("freq");
            SystemConfig.Kafka.consumer_data_length = kafka.getJSONObject("consumer").getInt("data_length");
            SystemConfig.Kafka.keepalive_freq = kafka.getJSONObject("keepalive").getInt("freq");
        } catch (JSONException e) {
            SystemConfig.Kafka.consumer_freq = 4 * 1000;
            SystemConfig.Kafka.consumer_data_length = 30;
            SystemConfig.Kafka.keepalive_freq = 10 * 1000;
        }

        //4.1 wifi_country


        //5.1 wifi_ping
        JSONObject obj_wifi = jsonObject.getJSONObject("wifi");
        try {
            SystemConfig.WifiPingBean.freq = obj_wifi.getInt("freq");  // if not int then use default value
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            SystemConfig.WifiPingBean.threshold = obj_wifi.getInt("threshold");  // if not int then use default value
        } catch (JSONException e) {
            e.printStackTrace();
        }
        SystemConfig.WifiPingBean.ip = (obj_wifi.getString("ip").isEmpty()) ? "8.8.8.8" : obj_wifi.getString("ip");
        SystemConfig.wifi_country = obj_wifi.getString("country");

        //tts model
        try {
            JSONObject obj_tts = jsonObject.getJSONObject("mstts");
            SystemConfig.TTS.speechModel_tw = obj_tts.getJSONObject("speech_model").getString("tw");
            SystemConfig.TTS.speechModel_cn = obj_tts.getJSONObject("speech_model").getString("cn");
            SystemConfig.TTS.speechModel_en = obj_tts.getJSONObject("speech_model").getString("en");
            SystemConfig.TTS.speechModel_ja = obj_tts.getJSONObject("speech_model").getString("ja");
            Log.d(TAG, String.format("tts tw:%s, cn:%s, en:%s, ja:%s",
                    SystemConfig.TTS.speechModel_tw,
                    SystemConfig.TTS.speechModel_cn,
                    SystemConfig.TTS.speechModel_en,
                    SystemConfig.TTS.speechModel_ja));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //available locale
        ChatApplication.supportLocaleList.clear();
        try {
            JSONObject localesObj = jsonObject.getJSONObject("locale_available");
            if (localesObj.getBoolean("tw")) ChatApplication.supportLocaleList.add(ChatApplication.Language.zh_TW);
            if (localesObj.getBoolean("cn")) ChatApplication.supportLocaleList.add(ChatApplication.Language.zh_CN);
            if (localesObj.getBoolean("ja")) ChatApplication.supportLocaleList.add(ChatApplication.Language.japanese);
            if (localesObj.getBoolean("en")) ChatApplication.supportLocaleList.add(ChatApplication.Language.en_US);
            Log.d(TAG, "available locale: " + ChatApplication.supportLocaleList.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (ChatApplication.supportLocaleList.isEmpty()) {
            ChatApplication.supportLocaleList.add(ChatApplication.Language.zh_TW);
            ChatApplication.supportLocaleList.add(ChatApplication.Language.zh_CN);
            ChatApplication.supportLocaleList.add(ChatApplication.Language.japanese);
            ChatApplication.supportLocaleList.add(ChatApplication.Language.en_US);
            Log.d(TAG, "no available locale, set to device locale: " + ChatApplication.supportLocaleList.toString());
        }

        //wakeup
        Gson gson = new Gson();
        UIConfig.wakupConfigBean = gson.fromJson(jsonObject.getJSONObject("wakeup").toString(), UIConfig.WakUpConfigBean.class);


        APIConfig.WEBAPI_DEVICEINFO = APIConfig.WEBAPI.build(APIConfig.KEY_DEVICEINFO, jsonObject.getJSONObject(APIConfig.KEY_DEVICEINFO), production);
        APIConfig.WEBAPI_POSTRATING = APIConfig.WEBAPI.build(APIConfig.KEY_POSTRATING, jsonObject.getJSONObject(APIConfig.KEY_POSTRATING), production);
        APIConfig.WEBAPI_DC_STATE = APIConfig.WEBAPI.build(APIConfig.KEY_DC_STATE, jsonObject.getJSONObject(APIConfig.KEY_DC_STATE), production);
        APIConfig.WEBAPI_MSTTS = APIConfig.WEBAPI.build(APIConfig.KEY_MSTTS, jsonObject.getJSONObject(APIConfig.KEY_MSTTS), production);
        APIConfig.WEBAPI_INTENT_ACTION = APIConfig.WEBAPI.build(APIConfig.KEY_INTENT_ACTION, jsonObject.getJSONObject(APIConfig.KEY_INTENT_ACTION), production);
        APIConfig.WEBAPI_MARKETING_ACTION_ELFIN =APIConfig.WEBAPI.build(APIConfig.KEY_MARKETING_ACTION_ELFIN, jsonObject.getJSONObject(APIConfig.KEY_MARKETING_ACTION_ELFIN), production);

        // for compatible
        JSONObject compatibleData = new JSONObject();
        compatibleData.put("urlTest", "");
        compatibleData.put("url", "");
        compatibleData.put("key", "");
        APIConfig.WEBAPI.build("redeem_complete_report", compatibleData, production);
        APIConfig.WEBAPI.build("kkbox_redeem", compatibleData, production);
        APIConfig.WEBAPI.build("get_room_status", compatibleData, production);
        APIConfig.WEBAPI.build("checkout_invoice", compatibleData, production);

    }


    private String getDevConfigFromLocal() throws Exception {
        AssetManager am = context.getAssets();
        InputStream inputStream = am.open("config_dev_use.json");
        int size = inputStream.available();
        byte[] buffer = new byte[size];
        inputStream.read(buffer);
        inputStream.close();
        return new String(buffer, "UTF-8");
    }

    private HashMap<String, String> putReplacePhrases(JSONObject data, HashMap<String, String> storeMap){
        try {
            Iterator<String> keys_tw = data.keys();
            while (keys_tw.hasNext()) {
                String key = keys_tw.next();
                storeMap.put(key, data.getString(key));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return storeMap;
    }

}
