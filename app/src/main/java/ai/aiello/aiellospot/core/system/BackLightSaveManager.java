package ai.aiello.aiellospot.core.system;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.core.SystemAlarmReceiver;
import ai.aiello.aiellospot.core.asr.ASRManager;
import ai.aiello.aiellospot.core.device.DeviceControl;
import ai.aiello.aiellospot.events.system.SystemEvent;
import ai.aiello.aiellospot.modules.alarm.UserAlarmModuleManager;
import ai.aiello.aiellospot.modules.music.MusicServiceModuleManager;
import ai.aiello.aiellospot.modules.voip.VoipModuleManager;

public class BackLightSaveManager {

    private static final String TAG = BackLightSaveManager.class.getSimpleName();
    public static final String BACK_LIGHT_OFF = "BACK_LIGHT_OFF";
    public static final String BACK_LIGHT_ON = "BACK_LIGHT_ON";
    private static SimpleDateFormat ISO8601DATEFORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    private static BackLightSaveManager instance;

    public static BackLightSaveManager getInstance() {
        if (instance == null) {
            instance = new BackLightSaveManager();
        }
        return instance;
    }

    public void register(Context context, boolean hosting) {
        register(context, 1, 0, BACK_LIGHT_OFF);
        register(context, 10, 30, BACK_LIGHT_ON);
        if (!hosting) return;
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    private static void register(Context context, int triggerHour, int triggerMinute, String actionName) {
        Calendar calendar_now = Calendar.getInstance();
        Calendar calendar_trigger = Calendar.getInstance();
        calendar_trigger.set(Calendar.HOUR_OF_DAY, triggerHour);
        calendar_trigger.set(Calendar.MINUTE, triggerMinute);
        calendar_trigger.set(Calendar.SECOND, 0);
        calendar_trigger.set(Calendar.MILLISECOND, 0);

        //set tomorrow while over the time
        if (calendar_now.getTimeInMillis() > calendar_trigger.getTimeInMillis()) {
            calendar_trigger.add(Calendar.DATE, 1);
        }
        Intent intent = new Intent(context, SystemAlarmReceiver.class);
        intent.setAction(actionName);
        PendingIntent sender = PendingIntent.getBroadcast(context, 900, intent, PendingIntent.FLAG_ONE_SHOT);
        android.app.AlarmManager manager = (android.app.AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        manager.set(android.app.AlarmManager.RTC_WAKEUP, calendar_trigger.getTimeInMillis(), sender);
        Log.d(TAG, actionName + " at " + ISO8601DATEFORMAT.format(new Date(calendar_trigger.getTimeInMillis())) + " is register successfully ");
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onMessageEvent(SystemEvent event) {
        switch (event.getType()) {
            case ALARM_RECEIVER:
                Intent intent = event.getIntent();
                switch (intent.getAction()) {
                    case BACK_LIGHT_ON:
                        DeviceControl.backLightOn();
                        register(ChatApplication.context, true); // register again for the next time
                        break;

                    case BACK_LIGHT_OFF:
                        // skip condition;
                        if (UserAlarmModuleManager.getInstance().isAlarmRing()) return;
                        if (VoipModuleManager.getInstance().currentState.getName() != VoipModuleManager.StateName.Idle)
                            return;
                        if (MusicServiceModuleManager.playerStatus == MusicServiceModuleManager.PlayerStatus.onPlay)
                            return;
                        if (ASRManager.getInstance().getOnASR()) return;
                        DeviceControl.backLightOff();
                        register(ChatApplication.context, true); // register again for the next time
                        break;
                }
                break;
        }
    }

}
