package ai.aiello.aiellospot.core.messager.kafka;

import android.util.Base64;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class KafkaUtils {

    public static String KTAG = KafkaUtils.class.getSimpleName() + ",";

    /*
     * TCP Headers:
     * "Content-Type":"application/vnd.kafka.json.v2+json"   // only for POST
     * "Accept":"application/vnd.kafka.json.v2+json" //for GET
     * "Accept":"application/vnd.kafka.v2+json" // for POST
     */

    public static String get(String path, int[] respCode, String TAG, String subKey) {
        if (path.equals("")) {
            return "";
        }
        DataOutputStream out = null;
        BufferedReader in = null;
        StringBuilder result = new StringBuilder();
        String authorization = "admin:Yhz%Df9ao~[r";
        HttpURLConnection conn = null;
        URL url = null;
        try {
            url = new URL(path);
//            Log.d(TAG, "get url: " + path);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            // set timeout
            conn.setConnectTimeout(5000);
            conn.setReadTimeout(5000);
            conn.setRequestProperty("Ocp-Apim-Subscription-Key", subKey);
            conn.setRequestProperty("Accept", "application/vnd.kafka.json.v2+json");
            // authorization
            byte[] encodedBytes = Base64.encode(authorization.getBytes(), 0);
            authorization = "Basic " + new String(encodedBytes);
            conn.setRequestProperty("Authorization", authorization);

            conn.connect();

            //response
            respCode[0] = conn.getResponseCode();
            //Log.i(TAG, "Get response code: " + respCode[0]);
            if (respCode[0] == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = conn.getInputStream();

                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
                String response = result.toString();
                if (!response.equals("[]")) {
                    Log.d(KTAG + TAG, response);
                }
                reader.close();
                inputStream.close();
            } else {
                InputStream inputStream = conn.getErrorStream();
                String response = "";

                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
                response = result.toString();
                Log.d(KTAG + TAG, response);

                reader.close();
                inputStream.close();
                if (response.contains("Consumer instance not found")) {
                    respCode[0] = 40403;
                }
            }
        } catch (Exception e) {
            Log.e(KTAG + TAG, e.toString());
        } finally {
            if (conn != null) {
                try {
                    conn.getInputStream().close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                conn.disconnect();
            }
        }

        return result.toString();
    }

    public static String post(String path, JSONObject jsonObject, int[] respCode, String TAG, String subKey) {
        if (path.equals("")) {
            return "";
        }
        DataOutputStream out = null;
        BufferedReader in = null;
        StringBuilder result = new StringBuilder();
        String authorization = "admin:Yhz%Df9ao~[r";
        URL url = null;
        HttpURLConnection conn = null;
        try {
            url = new URL(path);
//            Log.d(TAG, "post url: " + path);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            // must set true
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // set timeout
            conn.setConnectTimeout(5000);
            conn.setReadTimeout(5000);
            conn.setRequestProperty("Ocp-Apim-Subscription-Key", subKey);
            conn.setRequestProperty("Content-Type", "application/vnd.kafka.json.v2+json");
            conn.setRequestProperty("Accept", "application/vnd.kafka.v2+json");
            // authorization
            byte[] encodedBytes = Base64.encode(authorization.getBytes(), 0);
            authorization = "Basic " + new String(encodedBytes);
            //Log.d(TAG, "post auth: " + authorization);
            conn.setRequestProperty("Authorization", authorization);

            out = new DataOutputStream(conn.getOutputStream());
            out.write(jsonObject.toString().getBytes("utf-8"));
//            Log.d(TAG, "post string: " + jsonObject.toString());
            out.flush();
            out.close();
            conn.connect();

            //response
            respCode[0] = conn.getResponseCode();
            //Log.i(TAG, "Post response code: " + respCode[0]);
            if (respCode[0] == HttpURLConnection.HTTP_OK) {

                InputStream inputStream = conn.getInputStream();
                String response = "";

                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
                response = result.toString();
                Log.d(KTAG + TAG, response);

                reader.close();
                inputStream.close();
            }
        } catch (Exception e) {
            Log.e(KTAG + TAG, e.toString());
        } finally {
            if (conn != null) {
                try {
                    conn.getInputStream().close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                conn.disconnect();
            }
        }

        return result.toString();

    }

}
