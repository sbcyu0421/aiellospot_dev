package ai.aiello.aiellospot.core.system;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.litesuits.android.log.Log;

import ai.aiello.aiellospot.views.activity.new_ota.VersionCheckerT;

public class ServiceInstallReceiver extends BroadcastReceiver {

    private static final String TAG = ServiceInstallReceiver.class.getSimpleName();
    public static int push_apk_flag = 0;
    public static int music_apk_flag = 0;
    public static int sai_apk_flag = 0;
    public static int check = 0;

    @Override
    public void onReceive(Context context, Intent intent) {

        String name = intent.getStringExtra("service_name");
        Log.e(TAG, "ServiceInstallReceiver, name = " + name);
        check = 0;
        if (name.equals(VersionCheckerT.PUSH_SERVICE_NAME)) {
            push_apk_flag = 0;
        } else if (name.equals(VersionCheckerT.MUSIC_SERVICE_NAME)) {
            music_apk_flag = 0;
        } else {
            sai_apk_flag = 0;
        }

    }
}
