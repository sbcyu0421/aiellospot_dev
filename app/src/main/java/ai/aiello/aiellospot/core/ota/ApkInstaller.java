package ai.aiello.aiellospot.core.ota;

import ai.aiello.aiellospot.views.activity.new_ota.VersionCheckerT;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageInstaller;
import com.litesuits.android.log.Log;
import com.litesuits.common.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

public class ApkInstaller {


    private static final String TAG = ApkInstaller.class.getSimpleName();

    public static void installAPkSilently(Activity activity, File apkFile, int type) {

        String packageName = "";

        switch (type) {
            case 0:
                packageName = VersionCheckerT.SPOT_NAME;
                break;
            case 1:
                packageName = VersionCheckerT.MUSIC_SERVICE_NAME;
                break;
            case 2:
                packageName = VersionCheckerT.PUSH_SERVICE_NAME;
                break;
            case 3:
                packageName = VersionCheckerT.SAI_SERVICE_NAME;
                break;
        }

        Log.e(TAG, "ready to install package, name = " + packageName);

        // httpGet PackageInstaller from PackageManager
        PackageInstaller packageInstaller = activity.getPackageManager().getPackageInstaller();
        // Prepare params for installing one APK file with MODE_FULL_INSTALL
        PackageInstaller.SessionParams params = new PackageInstaller.SessionParams(PackageInstaller.SessionParams.MODE_FULL_INSTALL);
        params.setAppPackageName(packageName);

        // Create PackageInstaller.Session for performing the actual update
        try {
            int sessionID = packageInstaller.createSession(params);
            PackageInstaller.Session session = packageInstaller.openSession(sessionID);

            // Copy APK file bytes into OutputStream provided by install Session
            OutputStream out = session.openWrite(packageName, 0, -1);
            FileInputStream fis = new FileInputStream(apkFile);
            IOUtils.copy(fis, out);
            fis.close();
            out.close();

            Log.d(TAG, "apk install type = " + type);
            if (type == 0) {

                PendingIntent pendingIntent = PendingIntent.getBroadcast(
                        activity,
                        sessionID,
                        new Intent(),
                        0);
                IntentSender intentSender = pendingIntent.getIntentSender();
                session.commit(intentSender);

            } else {
                Intent i = new Intent("SERVICE_INSTALL_COMPLETE");
                i.putExtra("service_name", packageName);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(
                        activity,
                        sessionID,
                        i,
                        0);

                IntentSender intentSender = pendingIntent.getIntentSender();
                session.commit(intentSender);
            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
          SpotDeviceLog.exception(TAG,"",e.getMessage());
        }

    }

}
