package ai.aiello.aiellospot.core.media;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.litesuits.android.log.Log;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ai.aiello.aiellospot.events.system.SystemEvent;

public class AielloMediaPlayer {

    private static AielloMediaPlayer instance;
    private Context context;
    private String TAG = AielloMediaPlayer.class.getSimpleName();
    private Handler handler;
    private HashMap<String, SimpleExoPlayer> simpleExoPlayers = new HashMap<>();
    private HashMap<String, MPlayerEventListener> simpleExoPlayerListeners = new HashMap<>();

    public static AielloMediaPlayer getInstance() {
        if (instance == null) {
            instance = new AielloMediaPlayer();
        }
        return instance;
    }

    public void init(Context context) {
        this.context = context;
        this.handler = new Handler(Looper.getMainLooper());
    }

    private AielloMediaPlayer() {
    }


    private void release(SimpleExoPlayer player) {
        android.util.Log.d(TAG, "release");
        handler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    if (player != null) {
                        player.stop();
                        player.release();
                        android.util.Log.d(TAG, "player stopped");
                    } else {
                        android.util.Log.d(TAG, "player is not playing");
                    }
                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }
            }
        });

    }

    private static String previousSession = "";

    public void play(String majorSession, int index, JSONObject info, MediaCompletedListener mediaCompletedListener){
        if (!previousSession.contains(majorSession)) {  // if different majorSession, need to stop old, ensure only one at play, but mulit in intent
            android.util.Log.d(TAG, String.format("previousSession = %s,  majorSession = %s ", previousSession, majorSession));
            stopByNextSession(previousSession);
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    String fullSession = majorSession + "_" + index;
                    String audioSource = info.getString("value");
                    String volume = info.getString("volume");
                    SimpleExoPlayer player = new SimpleExoPlayer.Builder(context).build();
                    Uri uri = Uri.parse(audioSource);
                    MediaSource mediaSource = buildMediaSource(uri, context);
                    player.setAudioStreamType(C.STREAM_TYPE_RING);
                    try {
                        player.setVolume(Integer.parseInt(volume) / 100.0f);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    player.setPlayWhenReady(true);
                    player.prepare(mediaSource, false, false);
                    MPlayerEventListener mPlayerEventListener = new MPlayerEventListener(player, false, mediaCompletedListener,fullSession, majorSession);
                    player.addListener(mPlayerEventListener);
                    simpleExoPlayers.put(fullSession, player);
                    simpleExoPlayerListeners.put(fullSession, mPlayerEventListener);
                    android.util.Log.i(TAG, "play: " + fullSession);
                    previousSession = majorSession;
                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }
            }
        });

    }


    class MPlayerEventListener implements Player.EventListener{

        SimpleExoPlayer player;
        boolean interrupted;
        MediaCompletedListener mediaCompletedListener;
        String key;
        String session;

        void setInterrupted(boolean interrupted) {
            this.interrupted = interrupted;
        }

        MPlayerEventListener(SimpleExoPlayer player, boolean interrupted, MediaCompletedListener mediaCompletedListener, String key, String session) {
            this.player = player;
            this.interrupted = interrupted;
            this.mediaCompletedListener = mediaCompletedListener;
            this.key = key;
            this.session = session;
        }

        @Override
        public void onIsPlayingChanged(boolean isPlaying) {
            android.util.Log.d(TAG, "onPlayerStateChanged: isPlaying = " + isPlaying);
            if (!isPlaying) callback();
            else EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.TEMP_MEDIA_OCCUPY));
        }

        @Override
        public void onPlayerError(ExoPlaybackException error) {
            android.util.Log.d(TAG, "onPlayerStateChanged: error = " + error);
            callback();
        }

        private void callback() {
            try {
                EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.TEMP_MEDIA_RELEASE));
                player.release();
                simpleExoPlayers.remove(key);
                // callback while no any player in the session
                if (mediaCompletedListener == null) return;
                boolean completed = true;
                for (String key : simpleExoPlayers.keySet()) {
                    if (key.contains(session)) {
                        completed = false;
                        break;
                    }
                }
                if (interrupted) {
                    mediaCompletedListener.onInterrupted();
                    interrupted = false;
                    return;
                }
                if (completed) {
                    mediaCompletedListener.onCompleted();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    /**
     * stop the music by session, such as action_15
     * @param session
     */
    public void stop(String session) {
        android.util.Log.d(TAG, "stop");
        try {
            for (Map.Entry<String, SimpleExoPlayer> entry : simpleExoPlayers.entrySet()) {
                String key = entry.getKey();
                SimpleExoPlayer player = entry.getValue();
                if (key.contains(session)) release(player);
            }
        } catch (Exception ignore) {
            ignore.printStackTrace();
        }
    }

    public void stopByNextSession(String previousMajorSession) {
        android.util.Log.d(TAG, "stop");
        try {
            for (Map.Entry<String, SimpleExoPlayer> entry : simpleExoPlayers.entrySet()) {
                String fullSession = entry.getKey();
                SimpleExoPlayer player = entry.getValue();
                simpleExoPlayerListeners.get(fullSession).setInterrupted(true);
                if (fullSession.contains(previousMajorSession)) release(player);
            }
        } catch (Exception ignore) {
            ignore.printStackTrace();
        }
    }

    private MediaSource buildMediaSource(Uri uri, Context context) {
        DataSource.Factory dataSourceFactory =
                new DefaultDataSourceFactory(context, "exoplayer-aiello");
        return new ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
    }


}
