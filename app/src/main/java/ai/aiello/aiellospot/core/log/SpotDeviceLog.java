package ai.aiello.aiellospot.core.log;

import org.json.JSONObject;

import java.util.Date;

import ai.aiello.aiellospot.views.activity.new_ota.VersionCheckerT;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.core.messager.MessageManager;
import ai.aiello.aiellospot.core.system.WifiReceiver;

public class SpotDeviceLog {

    enum Type {
        info, exception, warning, error
    }

    public static void info(String activity, String result, String message) {
//        try {
//            MessageManager.getInstance().send(MessageManager.ProducerType.DEVICE_LOG, build(activity, result, message, Type.info));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    public static void error(String activity, String result, String message) {
//        try {
//            MessageManager.getInstance().send(MessageManager.ProducerType.DEVICE_LOG, build(activity, result, message, Type.error));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    public static void exception(String activity, String result, String message) {
//        try {
//            MessageManager.getInstance().send(MessageManager.ProducerType.DEVICE_LOG, build(activity, result, message, Type.exception));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    public static void warning(String activity, String result, String message) {
//        try {
//            MessageManager.getInstance().send(MessageManager.ProducerType.DEVICE_LOG, build(activity, result, message, Type.warning));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    public static JSONObject build(String activity, String result, String message, Type type) throws Exception {
        JSONObject _obj = new JSONObject();
        _obj.put("Logtime", new Date().getTime());
        _obj.put("Hotelname", DeviceInfo.hotelName);
        _obj.put("Roomno", DeviceInfo.roomName);
        _obj.put("Deviceid", DeviceInfo.MAC);
        _obj.put("Status", type.name());
        _obj.put("Wifirssi", WifiReceiver.WifiRSSI);
        _obj.put("Activity", activity);
        _obj.put("Domain", DeviceInfo.domain);
        _obj.put("Result", result);
        _obj.put("Spot_version", VersionCheckerT.apk_ver);
        _obj.put("Music_version", VersionCheckerT.music_ver);
        _obj.put("Sai_version", VersionCheckerT.sai_ver);
        _obj.put("Push_version", VersionCheckerT.push_ver);
        _obj.put("Message", message);
        return _obj;
    }


}
