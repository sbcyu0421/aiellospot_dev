package ai.aiello.aiellospot.core.device;

public class SystemActionCode {

    public static final String SCREEN_ON = "ScreenOn";
    public static final String SCREEN_OFF = "ScreenOff";

}
