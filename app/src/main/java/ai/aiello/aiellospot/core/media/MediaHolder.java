package ai.aiello.aiellospot.core.media;

import android.content.Context;
import android.net.Uri;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;

public class MediaHolder {

    private String id;
    private int state;
    private String value;
    private boolean foreground;
    private String execute;
    private SimpleExoPlayer simpleExoPlayer;
    private MediaSource mediaSource;

    public static final int MARKETING = 0;

    public MediaHolder(String id, int state, String value, boolean foreground, String execute, Context context) {
        this.id = id;
        this.state = state;
        this.value = value;
        this.foreground = foreground;
        this.execute = execute;

        this.simpleExoPlayer =  new SimpleExoPlayer.Builder(context).build();
        Uri uri = Uri.parse(value);
        this.mediaSource = buildMediaSource(uri, context);
        this.simpleExoPlayer.setAudioStreamType(C.STREAM_TYPE_RING);
        this.simpleExoPlayer.setPlayWhenReady(true);
    }



    public MediaSource getMediaSource() {
        return mediaSource;
    }

    private MediaSource buildMediaSource(Uri uri, Context context) {
        DataSource.Factory dataSourceFactory =
                new DefaultDataSourceFactory(context, "exoplayer-aiello");
        return new ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
    }

    public SimpleExoPlayer getSimpleExoPlayer() {
        return simpleExoPlayer;
    }

    public String getValue() {
        return value;
    }

    public String getExecute() {
        return execute;
    }

    public boolean isForeground() {
        return foreground;
    }

    public void setSimpleExoPlayer(SimpleExoPlayer simpleExoPlayer) {
        this.simpleExoPlayer = simpleExoPlayer;
    }

    public int getState() {
        return state;
    }

    public String getId() {
        return "State_"+ MARKETING + "_" + id;
    }
}
