package ai.aiello.aiellospot.core.messager;

import android.content.Context;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.concurrent.ArrayBlockingQueue;

import ai.aiello.aiellospot.core.config.SystemConfig;
import ai.aiello.aiellospot.core.messager.daemon.KeepAliver;
import ai.aiello.aiellospot.core.messager.kafka.KafkaProducer;
import ai.aiello.aiellospot.core.messager.kafka.SpotConsumer;
import ai.aiello.aiellospot.core.messager.message.MessageFactory;
import ai.aiello.aiellospot.events.system.SystemEvent;

public class MessageManager {

    private static MessageManager ourInstance = null;
    private String TAG = MessageManager.class.getSimpleName();
    private String hotelName = "";
    private String mac = "";


    private ArrayBlockingQueue<JSONObject> msgQueue;

    private SpotConsumer spotConsumer;
    private MessageFilter messageFilter;
    private KafkaProducer aliveProducer;
    private KafkaProducer spotLogProducer;
    private KafkaProducer userTraceProducer;
    private KafkaProducer managementProducer ;

    private HashMap<ProducerType, KafkaProducer> producerMap = new HashMap<>();

    private MessageManager() {
    }

    public static MessageManager getInstance() {
        if (ourInstance == null) {
            ourInstance = new MessageManager();
        }
        return ourInstance;
    }

    private KeepAliver keepAliver;
    private MessageParser messageParser;

    /**
     * @param id     Message ID, could be "HotelName_RoomName"
     * @param mac    Device MAC address
     * @param source Sending Msg Source, "SPOT"
     */
    public void init(Context context, String id, String mac, String source,
                     String hotelName, String roomName, int freq, int dataLength, int keepFreq) {

        this.hotelName = hotelName;
        this.mac = mac;
        MessageFactory.init(id, mac, source, hotelName, roomName);

        // init parser. skip hot reload
        if (messageParser == null) messageParser = new MessageParser(context);


        if (keepAliver == null) {
            //init keepaliver
            keepAliver = new KeepAliver(keepFreq);
            keepAliver.start();
        } else {
            //hot reload
            keepAliver.updateFreq(keepFreq);
        }


        //init or hotreload SpotLogProducer
        initSpotLogProducer();

        //init or hotreload UserTrace
        initSpotUserTraceProducer();

        //init or hotreload Management
        initManagementProducer();

        //init or hotreload SpotConsumer
        initSpotConsumer(freq, dataLength);

    }

    public void initSpotConsumer(int freq, int dataLength) {
        Log.d(TAG, "initSpotConsumer, freq = " + freq + ",dataLength = " + dataLength);
        if (msgQueue == null)
            msgQueue = new ArrayBlockingQueue<>(200);
        if (spotConsumer != null) {
            spotConsumer.interrupt();
            spotConsumer = null;
        }
        spotConsumer = new SpotConsumer(
                "androidCG_" + mac,
                "androidCI",
                hotelName + "_device",
                SystemConfig.Kafka.kafkaAPI.getUrl(),
                dataLength,
                freq,
                msgQueue,
                SystemConfig.Kafka.kafkaAPI.getKey()
        );
        spotConsumer.start();
        if (messageFilter == null) {
            messageFilter = new MessageFilter(msgQueue);
            messageFilter.start();
        }
    }

    public void pause(){
        spotConsumer.pausePolling();
    }

    public void resume(){
        spotConsumer.resumePolling();
    }

    public void initAliveProducer() {
        if (aliveProducer != null) {
            aliveProducer.interrupt();
            aliveProducer = null;
        }
        aliveProducer = new KafkaProducer(
                "AliveProducer",
                "Alive",
                SystemConfig.Kafka.kafkaAPI.getUrl(),
                100,
                100,
                false,
                SystemConfig.Kafka.kafkaAPI.getKey());
        producerMap.put(ProducerType.KEEP_ALIVE, aliveProducer);
        aliveProducer.start();
    }

    public void initSpotLogProducer() {
        if (spotLogProducer != null) {
            spotLogProducer.interrupt();
            spotLogProducer = null;
        }
        spotLogProducer = new KafkaProducer(
                "SpotLogProducer",
                "SpotLog",
                SystemConfig.Kafka.kafkaAPI.getUrl(),
                100,
                100,
                false,
                SystemConfig.Kafka.kafkaAPI.getKey());
        producerMap.put(ProducerType.DEVICE_LOG, spotLogProducer);
        spotLogProducer.start();
    }


    private void initSpotUserTraceProducer() {
        if (userTraceProducer != null) {
            userTraceProducer.interrupt();
            userTraceProducer = null;
        }
        userTraceProducer = new KafkaProducer(
                "SpotUserTraceProducer",
                "SpotUserLog",
                SystemConfig.Kafka.kafkaAPI.getUrl(),
                100,
                100,
                false,
                SystemConfig.Kafka.kafkaAPI.getKey());
        producerMap.put(ProducerType.USER_TRACE, userTraceProducer);
        userTraceProducer.start();
    }

    private void initManagementProducer() {
        if (managementProducer != null) {
            managementProducer.interrupt();
            managementProducer = null;
        }
        managementProducer = new KafkaProducer(
                "ManagementProducer",
                hotelName + "_management",
                SystemConfig.Kafka.kafkaAPI.getUrl(),
                500,
                100,
                true,
                SystemConfig.Kafka.kafkaAPI.getKey());
        producerMap.put(ProducerType.MANAGEMENT, managementProducer);
        managementProducer.start();
    }


    // topics of producer
    public enum ProducerType {
        DEVICE_LOG,
        USER_TRACE,
        KEEP_ALIVE,
        MANAGEMENT,
        CHATBOT_VALIDATOR
    }

    /**
     * @param type Topic Type of producer
     * @param data Use MessageFactory to create sending data
     */
    public synchronized void send(ProducerType type, MessageFactory.AielloData data) {
        if (data == null) {
            Log.e(TAG, "data is null");
            return;
        }

        if (producerMap.get(type) != null) {
            try {
                producerMap.get(type).enqueueMessage(data);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Log.e(TAG, "this producer is not initialized");
        }
    }

    /**
     * @param type Topic Type of producer
     * @param data For Debug Use Only
     */
    public synchronized void send(ProducerType type, JSONObject data) {
        if (data == null) {
            Log.e(TAG, "data is null");
            return;
        }

        if (producerMap.get(type) != null) {
            try {
                producerMap.get(type).enqueueMessage(data);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Log.e(TAG, "this producer is not initialized");
        }
    }

}
