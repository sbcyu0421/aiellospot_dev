package ai.aiello.aiellospot.core.system;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;

import com.litesuits.android.log.Log;
import com.litesuits.common.io.FileUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


import fr.arnaudguyon.xmltojsonlib.XmlToJson;
import androidx.annotation.Nullable;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import android.annotation.SuppressLint;
import java.net.InetAddress;
import java.util.ArrayList;
import java.lang.reflect.InvocationTargetException;
import java.net.UnknownHostException;
import java.lang.reflect.Method;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

public class AielloWifiManager {

    private static final String TAG = AielloWifiManager.class.getSimpleName();
    private static WifiManager wifiManager;
    private static File confFile;
    private static ConnectivityManager conManager;

    public static void setWifiEnable(Context context) {
        conManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifiManager.setWifiEnabled(true);
    }

    public static void reInitWifi() {
        wifiManager.setWifiEnabled(false);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        wifiManager.setWifiEnabled(true);
    }

    private static final String defaultNetworkSSID = "aiello_ez_si";
    private static final String defaultNetworkPass = "helloaiello";

    public static void connectWithRoomInfo(String networkSSID, String networkPass, int keyMgmt, boolean hiddenSSID, String ipAssignment, @Nullable String ip, @Nullable String maskLength, @Nullable String gateway, @Nullable String dns1, @Nullable String dns2, @Nullable String proxy) throws Exception {
        try {
            android.util.Log.d(TAG, "connectWithRoomInfo");
            if (!isSSIDDuplicateInManagement(networkSSID, networkPass, hiddenSSID, ipAssignment, ip, maskLength, gateway, dns1, dns2, proxy)) {
                Log.d(TAG, "remove and add new room ssid to manager, then reconnect");
                removeNetWork(networkSSID);
                addNetWorkToWifiManager(networkSSID, networkPass, keyMgmt, false, hiddenSSID, ipAssignment, ip, maskLength, gateway, dns1, dns2, proxy);
            } else {
                Log.i(TAG, "connect wifi with manager setting");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static final String AUTHORIZE_ERROR = "AUTHORIZE_ERROR";

    public static void connectWithConfig() {
        android.util.Log.d(TAG, "connectWithConfig");
        try {
            if (isWifiConfigFileExists()) {
                String fileToString = FileUtils.readFileToString(confFile);
                String[] configArray = fileToString.split(",");
                String networkSSID = configArray[0];
                String networkPass = configArray[1];
                int keyMgmt = Integer.parseInt(configArray[2]);
                Log.d(TAG, "add config file ssid to manager, then reconnect");
                try {
                    if (!isSSIDDuplicateInManagement(networkSSID, networkPass, false, "DHCP", null, null, null, null, null, null)) {
                        Log.d(TAG, "try remove same ssid in manager");
                        removeNetWork(networkSSID);
                        addNetWorkToWifiManager(networkSSID, networkPass, keyMgmt, true, false, "DHCP", null, null, null, null, null, null);
                    } else {
                        Log.i(TAG, "connectWithConfig is duplicate, do nothing");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (e.toString().contains(AUTHORIZE_ERROR)) { // for backward compact
                        android.util.Log.d(TAG, "backward compact, keep add conf to management");
                        addNetWorkToWifiManager(networkSSID, networkPass, keyMgmt, true, false, "DHCP", null, null, null, null, null, null);
                    }
                }
            } else {
                Log.i(TAG, "connect wifi with manager setting");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static void removeNetWork(String networkSSID) {
        android.util.Log.e(TAG, "remove old network config : " + networkSSID);
        try {
            List<WifiConfiguration> configuredNetworks = wifiManager.getConfiguredNetworks();
            if (configuredNetworks != null) {
                for (WifiConfiguration existingConfig : configuredNetworks) {
                    if (existingConfig.SSID.equals("\"" + networkSSID + "\"")) {
                        wifiManager.removeNetwork(existingConfig.networkId);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void checkDefaultWifi() {
        try {
            android.util.Log.d(TAG, "checkDefaultWifi");
            if (!isSSIDDuplicateInManagement(defaultNetworkSSID, defaultNetworkPass, false, "DHCP", null, null, null, null, null, null)) {
                Log.d(TAG, "add default ssid to manager");
                addNetWorkToWifiManager(defaultNetworkSSID, defaultNetworkPass, 1, true, false, "DHCP", null, null, null, null, null, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static boolean isSSIDDuplicateInManagement(String SSID, String pwd, boolean hiddenSSID, String ipAssignment, @Nullable String ip, @Nullable String maskLength, @Nullable String gateway, @Nullable String dns1, @Nullable String dns2, @Nullable String proxy) throws Exception {
        String path = "/data/misc/wifi/WifiConfigStore.xml";
        String wifiConfigStore = "";
        HashMap<String, HashMap<String, String>> wifiConfigXml = new HashMap<>();
        try {
            wifiConfigStore = FileUtils.readFileToString(new File(path));
        } catch (Exception e) {
            throw new Exception("AUTHORIZE_ERROR");
        }
        XmlToJson xmlToJson = new XmlToJson.Builder(wifiConfigStore).build();
        JSONObject jsonObject = xmlToJson.toJson();
        // WIFI CONFIG >1
        try {
            JSONArray jsonArray = jsonObject.getJSONObject("WifiConfigStoreData").getJSONObject("NetworkList").getJSONArray("Network");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject confObj = jsonArray.getJSONObject(i).getJSONObject("WifiConfiguration");
                JSONObject ipAssignmentObj = jsonArray.getJSONObject(i).getJSONObject("IpConfiguration");
                parserDOM(wifiConfigXml, confObj, ipAssignmentObj);
            }
        } catch (JSONException e) {
            // WIFI CONFIG == 1
            try {
                JSONObject confObj = jsonObject.getJSONObject("WifiConfigStoreData").getJSONObject("NetworkList").getJSONObject("Network").getJSONObject("WifiConfiguration");
                JSONObject ipAssignmentObj = jsonObject.getJSONObject("WifiConfigStoreData").getJSONObject("NetworkList").getJSONObject("Network").getJSONObject("IpConfiguration");
                parserDOM(wifiConfigXml, confObj, ipAssignmentObj);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }

        // Checking wifi configs from server and device
        try {
            HashMap<String, String> item = wifiConfigXml.get(SSID);
            if (!item.get("SSID").equals(SSID)) return false;
            if (!item.get("PreSharedKey").equals(pwd)) return false;
            if (!item.get("HiddenSSID").equals(String.valueOf(hiddenSSID))) return false;
            if (!item.get("IpAssignment").equals(ipAssignment)) return false;
            android.util.Log.d(TAG, "mockWifi: " + SSID + ":" + ipAssignment);

            // if all 4 main settings on both device and DB of wifi are same, check if is static IP then check detail information.
            if (item.get("IpAssignment").equals("STATIC")) {
                if (!item.get("ip").equals(ip)) return false;
                if (!item.get("maskLength").equals(maskLength)) return false;
                if (!item.get("gateway").equals(gateway)) return false;
                if (!item.get("dns1").equals(dns1)) return false;
                if (!item.get("dns2").equals(dns2)) return false;
                if (!item.get("proxy").equalsIgnoreCase(proxy)) return false;
            }
            // if not static IP and isSSIDDuplicate == true
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private static void parserDOM(HashMap<String, HashMap<String, String>> wifiConfigXml, JSONObject confObj, JSONObject ipAssignObj) throws JSONException {
        try {
            String _ssid = "";
            String _prefKey = "";
            String _hiddenSSID = "";

            String _IpAssignment = "";
            String _ip = "";
            String _maskLength = "";
            String _gateway = "";
            String _dns1 = "";
            String _dns2 = "";
            String _proxy = "";

            JSONArray conf = null;
            JSONArray ipAssignmentConf = null;
            JSONObject dnsConf = null;
            JSONObject maskConf = null;

            conf = confObj.getJSONArray("string");
            try {
                ipAssignmentConf = ipAssignObj.getJSONArray("string");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                maskConf = ipAssignObj.getJSONObject("int");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                dnsConf = ipAssignObj.getJSONObject("string-array");
            } catch (JSONException e) {
                e.printStackTrace();
            }


            // parsing
            for (int j = 0; j < conf.length(); j++) {
                String name = conf.getJSONObject(j).getString("name");
                String content = conf.getJSONObject(j).getString("content");
                if (name.equals("SSID")) {
                    _ssid = content.substring(1, content.length() - 1);
                } else if (name.equals("PreSharedKey")) {
                    _prefKey = content.substring(1, content.length() - 1);
                }
            }
            JSONArray conf_hidden = confObj.getJSONArray("boolean");
            for (int j = 0; j < conf_hidden.length(); j++) {
                String name = conf_hidden.getJSONObject(j).getString("name");
                String content = conf_hidden.getJSONObject(j).getString("value");
                if (name.equals("HiddenSSID")) {
                    _hiddenSSID = content;
                }
            }

            // parsing static network settings - 1
            for (int j = 0; j < ipAssignmentConf.length(); j++) {
                try {
                    String name = ipAssignmentConf.getJSONObject(j).getString("name");
                    String content = ipAssignmentConf.getJSONObject(j).getString("content");
                    if (name.equals("IpAssignment")) {
                        _IpAssignment = content;
                    } else if (name.equals("LinkAddress")) {
                        _ip = content;
                    } else if (name.equals("GatewayAddress")) {
                        _gateway = content;
                    } else if (name.equals("ProxySettings")) {
                        _proxy = content;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            // parsing for static network settings - 2
            if (dnsConf != null) {
                //only dns1 was provided -> data as a jsonObject
                try {
                    JSONObject dnsObj = dnsConf.getJSONObject("item");
                    _dns1 = dnsObj.getString("value");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //data with both dns1 & dns2 -> data as a jsonArray
                try {
                    JSONArray dnsArray = dnsConf.getJSONArray("item");
                    _dns1 = dnsArray.getJSONObject(0).getString("value");
                    _dns2 = dnsArray.getJSONObject(1).getString("value");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            // parsing for static network settings - 3
            if (maskConf != null) {
                try {
                    _maskLength = maskConf.getString("value");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


            if (!_ssid.isEmpty() && !_prefKey.isEmpty() && !_hiddenSSID.isEmpty()) {
                HashMap<String, String> tmp = new HashMap<>();
                tmp.put("SSID", _ssid);
                tmp.put("PreSharedKey", _prefKey);
                tmp.put("HiddenSSID", _hiddenSSID);
                if (_IpAssignment.equals("STATIC")) {

                    try {
                        tmp.put("IpAssignment", _IpAssignment);
                        tmp.put("ip", _ip);
                        tmp.put("maskLength", _maskLength);
                        tmp.put("gateway", _gateway);
                        tmp.put("dns1", _dns1);
                        tmp.put("dns2", _dns2);
                        tmp.put("proxy", _proxy);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    android.util.Log.d(TAG, String.format("active conf : ssid: %s, pwd : %s, hidden : %s, ipAssignments : %s, ip : %s, maskLength : %s, gateway: %s, dns1 : %s, dns2 : %s, proxySetting : %s"
                            , _ssid, _prefKey, _hiddenSSID, _IpAssignment, _ip, _maskLength, _gateway, _dns1, _dns2, _proxy));
                } else if (_IpAssignment.equals("DHCP")) {
                    try {
                        tmp.put("IpAssignment", _IpAssignment);
                        tmp.put("proxy", _proxy);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    android.util.Log.d(TAG, String.format("active conf : ssid: %s, pwd : %s, hidden : %s, ipAssignments : %s, proxySetting : %s", _ssid, _prefKey, _hiddenSSID, _IpAssignment, _proxy));
                }
                wifiConfigXml.put(_ssid, tmp);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private static String getTagValue(String sTag, Element eElement) {
        NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();
        Node nValue = nlList.item(0);
        return nValue.getNodeValue();
    }


    public static boolean isWifiConfigFileExists() {
        boolean fileExits = false;
        confFile = FileUtils.getFile("/sdcard/bluetooth/wifi_config.txt");
        if (confFile.exists()) {
            fileExits = true;
            try {
                InputStream fis = new FileInputStream("text.txt");
                BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
                String line = reader.readLine();
                android.util.Log.d(TAG, "isWifiConfigFileExists: " + line);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return fileExits;
    }


    @SuppressLint("PrivateApi")
    public static void addNetWorkToWifiManager(String networkSSID, String networkPass, int keyMgmt, boolean attemptConnect, boolean hiddenSSID, String ipAssignment, @Nullable String ip, @Nullable String maskLength, @Nullable String gateway, @Nullable String dns1, @Nullable String dns2, @Nullable String proxyUrl) throws IllegalAccessException, NoSuchMethodException, ClassNotFoundException, NoSuchFieldException, InvocationTargetException, InstantiationException, UnknownHostException {
        Log.d(TAG, "addNetWorkToWifiManager = " + networkSSID + "," + networkPass + "," + keyMgmt);
        WifiConfiguration conf = new WifiConfiguration();
        conf.SSID = "\"" + networkSSID + "\"";
        if (keyMgmt != 0) {
            conf.preSharedKey = "\"" + networkPass + "\"";
        }
        conf.allowedKeyManagement.set(keyMgmt);
        conf.hiddenSSID = hiddenSSID;

        // Code for setting up proxy to wifiManager (Activate if required)
//        if (proxyUrl != null) {
//
//            Class proxySettings = Class.forName("android.net.IpConfiguration$ProxySettings");
//
//            Class[] setProxyParams = new Class[2];
//            setProxyParams[0] = proxySettings;
//            setProxyParams[1] = ProxyInfo.class;
//
//            Method setProxy = conf.getClass().getDeclaredMethod("setProxy", setProxyParams);
//            setProxy.setAccessible(true);
//
//            ProxyInfo desiredProxy = ProxyInfo.buildDirectProxy(proxyUrl, 10);
//
//            Object[] methodParams = new Object[2];
//            methodParams[0] = Enum.valueOf(proxySettings, "STATIC");
//            methodParams[1] = desiredProxy;
//
//            setProxy.invoke(conf, methodParams);
//        }

        // setup static ip assignment settings
        if (ipAssignment.equals("STATIC")) {
            Class<Enum> enumClz = (Class<Enum>) Class.forName("android.net.IpConfiguration$IpAssignment");
            Object ipAssignmentEnum = Enum.valueOf(enumClz, "STATIC");

            callMethod(conf, "setIpAssignment", new String[]{"android.net.IpConfiguration$IpAssignment"}, new Object[]{ipAssignmentEnum});

            Object staticIpConfig = newInstance("android.net.StaticIpConfiguration", new Class<?>[0], new Object[0]);
            InetAddress ipAddress = InetAddress.getByName(ip);
            Object linkAddress = newInstance("android.net.LinkAddress", new Class<?>[]{InetAddress.class, int.class}, new Object[]{ipAddress, Integer. parseInt(maskLength)});

            setField(staticIpConfig, "ipAddress", linkAddress);
            setField(staticIpConfig, "gateway", InetAddress.getByName(gateway));
            getField(staticIpConfig, "dnsServers", ArrayList.class).clear();
            if (!dns1.equals("")) getField(staticIpConfig, "dnsServers", ArrayList.class).add(InetAddress.getByName(dns1));
            if (!dns2.equals("")) getField(staticIpConfig, "dnsServers", ArrayList.class).add(InetAddress.getByName(dns2));
            //adding blank value to dns will provide "::1" here


            callMethod(conf, "setStaticIpConfiguration", new String[]{"android.net.StaticIpConfiguration"}, new Object[]{staticIpConfig});
        }

        int s = wifiManager.addNetwork(conf);
        wifiManager.enableNetwork(s, attemptConnect);
    }

    public static int iskWifiOnAndConnected() {
        int status;
        if (wifiManager.isWifiEnabled()) {
            NetworkInfo netInfo = conManager.getActiveNetworkInfo();
            if (netInfo != null && netInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                Log.e(TAG, "Have Wifi Connection");
                status = 2;
            } else {
                Log.e(TAG, "Don't have Wifi Connection");
                status = 1;
            }
        } else {
            Log.e(TAG, "Wi-Fi OFF");
            status = -1;
        }
        return status;
    }


    public static String getWifiMacAddress() {
        String defaultMac = "02:00:00:00:00:00";

        do {
            Log.d(TAG, "get mac...");
            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
                for (NetworkInterface ntwInterface : interfaces) {

                    if (ntwInterface.getName().equalsIgnoreCase("wlan0")) {//之前是p2p0，修正为wlan
                        byte[] byteMac = ntwInterface.getHardwareAddress();
                        if (byteMac == null) {
                            com.litesuits.android.log.Log.d(TAG, "byteMac == null ");
                        } else {
                            StringBuilder strBuilder = new StringBuilder();
                            for (int i = 0; i < byteMac.length; i++) {
                                strBuilder.append(String
                                        .format("%02X:", byteMac[i]));
                            }
                            if (strBuilder.length() > 0) {
                                strBuilder.deleteCharAt(strBuilder.length() - 1);
                            }
                            defaultMac = strBuilder.toString();
                        }
                    }
                }
            } catch (Exception e) {
                com.litesuits.android.log.Log.d(TAG, e.getMessage());
            }
        } while (defaultMac.equals("02:00:00:00:00:00"));

        return defaultMac;

    }

    public static void reconnect() {
        wifiManager.disconnect();
        wifiManager.reconnect();
    }

    private static Thread testThread;

    public static void testWifi(boolean enable) {
        if (enable) {
            if (testThread != null) {
                return;
            }
            Log.d(TAG, "start testWifi");
            testThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        while (true) {
                            Log.d(TAG, "reconnect wifi...");
                            AielloWifiManager.reconnect();
                            Thread.sleep(15000);
                        }
                    } catch (InterruptedException e) {
                        Log.i(TAG, "testing stopped");
                    }
                }
            });
            testThread.start();
        } else {
            if (testThread == null || testThread.isInterrupted()) {
                Log.i(TAG, "test wifi reconnect already finished");
                return;
            }
            Log.d(TAG, "stop testWifi");
            try {
                testThread.interrupt();
                testThread = null;
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }
        }
    }

    // tool functions for setting up static ip wifi config
    private static void callMethod(Object object, String methodName, String[] parameterTypes, Object[] parameterValues) throws ClassNotFoundException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        Class<?>[] parameterClasses = new Class<?>[parameterTypes.length];
        for (int i = 0; i < parameterTypes.length; i++)
            parameterClasses[i] = Class.forName(parameterTypes[i]);

        Method method = object.getClass().getDeclaredMethod(methodName, parameterClasses);
        method.invoke(object, parameterValues);
    }

    private static Object newInstance(String className, Class<?>[] parameterClasses, Object[] parameterValues) throws NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, ClassNotFoundException, InvocationTargetException {
        Class<?> clz = Class.forName(className);
        Constructor<?> constructor = clz.getConstructor(parameterClasses);
        return constructor.newInstance(parameterValues);
    }

    private static void setField(Object object, String fieldName, Object value) throws IllegalAccessException, IllegalArgumentException, NoSuchFieldException {
        Field field = object.getClass().getDeclaredField(fieldName);
        field.set(object, value);
    }

    private static <T> T getField(Object object, String fieldName, Class<T> type) throws IllegalAccessException, IllegalArgumentException, NoSuchFieldException {
        Field field = object.getClass().getDeclaredField(fieldName);
        return type.cast(field.get(object));
    }

    // functions for setup proxy wifi config
    public static void setProxySettings(String assign , WifiConfiguration wifiConf)
            throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException{
        setEnumField(wifiConf, assign, "proxySettings");
    }
    public static void setEnumField(Object obj, String value, String name)
            throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException{
        Field f = obj.getClass().getField(name);
        f.set(obj, Enum.valueOf((Class<Enum>) f.getType(), value));
    }


}
