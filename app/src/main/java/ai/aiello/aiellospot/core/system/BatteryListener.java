package ai.aiello.aiellospot.core.system;

import ai.aiello.aiellospot.R;

import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PixelFormat;
import android.os.BatteryManager;
import android.view.*;
import android.view.animation.*;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.litesuits.android.log.Log;

public class BatteryListener {

    private Context mContext;
    private static String TAG = BatteryListener.class.getSimpleName();
    private static BatteryBroadcastReceiver receiver;
    private static int shutdownThreshold = 5;

    public BatteryListener(Context context) {
        mContext = context;
        receiver = new BatteryBroadcastReceiver();
    }

    public void register() {
//        mBatteryStateListener = listener;
        if (receiver != null) {
            IntentFilter filter = new IntentFilter();
            filter.addAction(Intent.ACTION_BATTERY_CHANGED);
            filter.addAction(Intent.ACTION_BATTERY_LOW);
            filter.addAction(Intent.ACTION_BATTERY_OKAY);
            filter.addAction(Intent.ACTION_POWER_CONNECTED);
            filter.addAction(Intent.ACTION_POWER_DISCONNECTED);
            Intent intent = mContext.registerReceiver(receiver, filter);

            //first sync
            int plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, 0);
            currentElectricity = intent.getIntExtra("level", 0);
            if (plugged == BatteryManager.BATTERY_PLUGGED_AC || plugged == BatteryManager.BATTERY_PLUGGED_USB) {
                BatteryListener.postDCState(true);
            } else {
                BatteryListener.postDCState(false);
            }
        }
    }

//    public void unregister(Context context) {
//        if (receiver != null) {
//            context.unregisterReceiver(receiver);
//        }
//    }


    private class BatteryBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String acyion = intent.getAction();
                switch (acyion) {
                    case Intent.ACTION_BATTERY_CHANGED://电量发生改变
//                        Log.e(TAG, "BatteryBroadcastReceiver --> onReceive--> ACTION_BATTERY_CHANGED");
                        currentElectricity = intent.getIntExtra("level", 0);
//                        Log.d(TAG, "currentElectricity = " + currentElectricity);
                        updateCurrentBattery();

                        if (intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, 0) == BatteryManager.BATTERY_PLUGGED_AC
                                || intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, 0) == BatteryManager.BATTERY_PLUGGED_USB) {
                            hideChargePopup();
                        } else {
                            showChargePopup(mContext);
                            if (currentElectricity <= shutdownThreshold) {
                                Log.w(TAG, "battery below " + shutdownThreshold + " -> shutdown device");
                                SystemControl.shutdownDevice(mContext);
                            }
                        }

                        break;
                    case Intent.ACTION_BATTERY_LOW://电量低
//                        Log.d(TAG, "BatteryBroadcastReceiver --> onReceive--> ACTION_BATTERY_LOW");
                        break;
                    case Intent.ACTION_BATTERY_OKAY://电量充满
//                        Log.d(TAG, "BatteryBroadcastReceiver --> onReceive--> ACTION_BATTERY_OKAY");
                        break;
                    case Intent.ACTION_POWER_CONNECTED://接通电源
                        Log.e(TAG, "BatteryBroadcastReceiver --> onReceive--> ACTION_POWER_CONNECTED");
                        BatteryListener.postDCState(true);
//                        hideChargePopup();
//                        hideBatteryPopup();
                        break;
                    case Intent.ACTION_POWER_DISCONNECTED://拔出电源
                        Log.e(TAG, "BatteryBroadcastReceiver --> onReceive--> ACTION_POWER_DISCONNECTED");
                        BatteryListener.postDCState(false);
//                        showChargePopup(mContext);
                        break;
                }
            }
        }
    }


    private static View mPowerView = null;
    private static WindowManager mWindowManager = null;
    private static Boolean isShown = false;

    private static void showChargePopup(final Context context) {

        if (!isShown) {
            Context mContext = context.getApplicationContext();
            // 获取WindowManager
            mWindowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
            mPowerView = setPowerPopupView(context);
            final WindowManager.LayoutParams params = new WindowManager.LayoutParams();

            // 类型
            params.type = WindowManager.LayoutParams.TYPE_PHONE;
            params.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL; // FLAG_NOT_TOUCH_MODAL不阻塞事件传递到后面的窗口
            params.format = PixelFormat.TRANSLUCENT;

            params.width = WindowManager.LayoutParams.MATCH_PARENT;
            params.height = WindowManager.LayoutParams.MATCH_PARENT;
            params.gravity = Gravity.CENTER;

            mWindowManager.addView(mPowerView, params);
            isShown = true;
            Log.i(TAG, "show charge popup");
        }

    }

    private static void hideChargePopup() {
        if (isShown && mPowerView != null) {
            Log.i(TAG, "hide charge popup");
            mWindowManager.removeView(mPowerView);
            isShown = false;
            mPowerView = null;
        }
    }

    private static View setPowerPopupView(final Context context) {
        //previous code
//        View view = LayoutInflater.from(context).inflate(R.layout.popupwindow_power, null);
//        LinearLayout ly_powerbox = view.findViewById(R.id.ly_powerbox);
        View view = LayoutInflater.from(context).inflate(R.layout.item_power_popup, null);
        ConstraintLayout ly_powerbox = view.findViewById(R.id.ly_powerbox_2);
        TextView tv_plug_alert_title = view.findViewById(R.id.tv_plug_alert_title);
        TextView tv_plug_alert_content = view.findViewById(R.id.tv_plug_alert_ai_web);
        tv_plug_alert_title.setText(R.string.pls_plug_powerplug);
        tv_plug_alert_content.setText(R.string.contact_aiello_website);

        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN;
        view.setSystemUiVisibility(uiOptions);

        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
        fadeIn.setDuration(1000);

//        Animation fadeOut = new AlphaAnimation(1, 0);
//        fadeOut.setInterpolator(new AccelerateInterpolator()); //and this
//        fadeOut.setStartOffset(1000);
//        fadeOut.setDuration(1000);
//        fadeOut.setAnimationListener(new Animation.AnimationListener() {
//            @Override
//            public void onAnimationStart(Animation animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animation animation) {
//                hideChargePopup();
//                showBatteryPopup(context);
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) {
//
//            }
//        });

        AnimationSet animationSet = new AnimationSet(false); //change to false
        animationSet.addAnimation(fadeIn);
//        animationSet.addAnimation(fadeOut);
        ly_powerbox.startAnimation(animationSet);

        return view;

    }


    private static void updateCurrentBattery() {
        if (isBatteryShown) {
            if (currentElectricity < 25) {
                img_battery.setImageResource(R.drawable.battery_25);
            } else if (currentElectricity < 50) {
//                img_battery.setImageResource(R.drawable.battery_50);
                img_battery.setImageResource(R.drawable.unplug);
            } else if (currentElectricity < 75) {
//                img_battery.setImageResource(R.drawable.battery_75);
                img_battery.setImageResource(R.drawable.unplug);
            } else {
//                img_battery.setImageResource(R.drawable.battery_100);
                img_battery.setImageResource(R.drawable.unplug);
            }
        }
    }

    private static View mBatteryView = null;
    private static Boolean isBatteryShown = false;
    private static ImageView img_battery = null;
    public static int currentElectricity = 0;

    private static void showBatteryPopup(final Context context) {

        if (!isBatteryShown) {
            Context mContext = context.getApplicationContext();
            // 获取WindowManager
            mWindowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
            mBatteryView = setBatteryPopupView(context);
            final WindowManager.LayoutParams params = new WindowManager.LayoutParams();

            // 类型
            params.type = WindowManager.LayoutParams.TYPE_PHONE;
            params.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL; // FLAG_NOT_TOUCH_MODAL不阻塞事件传递到后面的窗口
            params.format = PixelFormat.TRANSLUCENT;
            params.width = WindowManager.LayoutParams.WRAP_CONTENT;
            params.height = WindowManager.LayoutParams.WRAP_CONTENT;
            params.gravity = Gravity.TOP | Gravity.RIGHT;


            mWindowManager.addView(mBatteryView, params);
            isBatteryShown = true;
            updateCurrentBattery();
            Log.i(TAG, "show charge popup");
        }

    }

    private static void hideBatteryPopup() {
        if (isBatteryShown && mBatteryView != null) {
            Log.i(TAG, "hide charge popup");
            mWindowManager.removeView(mBatteryView);
            isBatteryShown = false;
            mBatteryView = null;
            img_battery = null;
        }
    }

    private static View setBatteryPopupView(final Context context) {

        View view = LayoutInflater.from(context).inflate(R.layout.popupwindow_battery, null);
        LinearLayout ly_batterybox = view.findViewById(R.id.ly_batterybox);
        img_battery = view.findViewById(R.id.img_battery);
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN;
        view.setSystemUiVisibility(uiOptions);

        Animation amTranslate = new TranslateAnimation(0.0f, 0.0f, -50.0f, 10.0f);
        amTranslate.setDuration(500);
        amTranslate.setRepeatCount(0);
        amTranslate.setFillAfter(true);
        ly_batterybox.startAnimation(amTranslate);

        return view;

    }

    public static boolean powerState;

    private static void postDCState(boolean isConnected) {
        powerState = isConnected;
        SpotUserTraceLog3.getInstance().buildHardwareEventLog(
                SpotUserTraceLog3.EventSubject.POWER,
                isConnected ? SpotUserTraceLog3.EventAction.PLUG_IN : SpotUserTraceLog3.EventAction.PLUG_OUT,
                ""
        );
        /*
        ChatApplication.worker.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    Log.d(TAG, "postDCState = " + isConnected);
                    String url = APIConfig.WEBAPI_DC_STATE.getUrl();
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("mac", DeviceInfo.MAC);
                    jsonObject.put("statusName", "Power");
                    jsonObject.put("isEnabled", isConnected);
                    String s = Http.put(url, jsonObject, 5000, APIConfig.WEBAPI_DC_STATE.getKey()).response;
                    JSONObject jObj = new JSONObject(s);
                    boolean isSucceeded = jObj.getBoolean("success");
                    Log.e(TAG, "postDCState success = " + isSucceeded);
                } catch (Exception e) {
                    SpotDeviceLog.exception(TAG, "", "postDCState" + e.getMessage());
                    Log.e(TAG, "postDC error: " + e.toString());
                }
            }
        });
        */
    }


}
