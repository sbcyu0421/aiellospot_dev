package ai.aiello.aiellospot.core.messager.kafka;

import org.json.JSONObject;

public class KafkaMsg {
    private JSONObject msg;

    public KafkaMsg(JSONObject msg) {
        this.msg = msg;
    }

    public JSONObject getMsg() {
        return msg;
    }
}