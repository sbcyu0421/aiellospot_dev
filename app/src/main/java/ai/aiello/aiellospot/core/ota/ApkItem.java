package ai.aiello.aiellospot.core.ota;

public class ApkItem {

    String versionName;
    String note;
    String releaseDate;
    String url;
    int currentVersion = 0;
    int id;

    public ApkItem(String versionName, String note, String releaseDate, String url, int currentVersion, int id) {
        this.versionName = versionName;
        this.note = note;
        this.releaseDate = releaseDate;
        this.url = url;
        this.currentVersion = currentVersion;
        this.id = id;
    }

    public String getVersionName() {
        return versionName;
    }

    public String getNote() {
        return note;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public String getUrl() {
        return url;
    }

    public int getCurrentVersion() {
        return currentVersion;
    }

    public int getId() {
        return id;
    }
}
