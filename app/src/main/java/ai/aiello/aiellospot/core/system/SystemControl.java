package ai.aiello.aiellospot.core.system;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.core.info.DeviceInfo;

import ai.aiello.aiellospot.core.info.UserInfo;
import ai.aiello.aiellospot.core.ota.GetUpdateListTask;
import ai.aiello.aiellospot.utlis.AielloFileUtils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.PowerManager;

import com.litesuits.android.log.Log;

import java.io.DataOutputStream;
import java.io.OutputStream;

public class SystemControl {

    public enum StageAction {
        SAVE, CLEAR, NONE
    }

    public static String TAG = SystemControl.class.getSimpleName();

    public static void rebootAPK(Context context, StageAction stageAction) {
        ChatApplication.worker.submit(() -> {
            try {
                Thread.sleep(3000);
                Log.d(TAG, "rebootAPK");
                switch (stageAction){
                    case SAVE:
                        UserInfo.saveStage();
                        break;
                    case CLEAR:
                        UserInfo.clearStage();
                        break;
                }
                ChatApplication.releaseResource();
                System.exit(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public static void rebootDevice(Context context, StageAction stageAction) {
        ChatApplication.worker.submit(() -> {
            try {
                Thread.sleep(3000);
                Log.d(TAG, "rebootDevice");
                switch (stageAction){
                    case SAVE:
                        UserInfo.saveStage();
                        break;
                    case CLEAR:
                        UserInfo.clearStage();
                        break;
                }
                ChatApplication.releaseResource();
                PowerManager pm = (PowerManager) context.getApplicationContext().getSystemService(Context.POWER_SERVICE);
                if (pm != null) {
                    pm.reboot(null);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public static void shutdownDevice(Context context) {
        Intent intent = new Intent("com.android.internal.intent.action.REQUEST_SHUTDOWN");
        intent.putExtra("android.intent.extra.KEY_CONFIRM", false);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void imageCheckAndUpdate(Context context) {
        //send broadcast
        Intent intent = new Intent("android.rockchip.update.service.action.ota"); //設定廣播識別碼
        intent.putExtra("req", 2); //設定廣播夾帶參數
        context.sendBroadcast(intent); //發送廣播訊息
        Log.d(TAG, "imageCheckAndUpdate");
        //go to foreground after 200ms
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        rebootAPK(context);
        inputHomeKeyEvent();
    }

    private static void inputHomeKeyEvent() {
        try {
            Process p = Runtime.getRuntime().exec("sh");  //許可權設定, su為root使用者,sh普通使用者
            //獲取輸出流
            OutputStream outputStream = p.getOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
            //將命令寫入
            dataOutputStream.writeBytes("input keyevent 3");
            //提交命令
            dataOutputStream.flush();
            //關閉流操作
            dataOutputStream.close();
            outputStream.close();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }


    //only for Aiello
    public static void manualOTA(Activity activity, String url, String key, int hotelId, String type, String cloud, int majorVer) {
        new GetUpdateListTask(activity, url, key, hotelId, type, cloud, majorVer).execute();
//        Log.d(TAG, "manualOTA, url = " + url);
    }

    public static void uploadLogFromClient(AielloFileUtils.UploadFinishListener listener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "upload_log_from_client");
                AielloFileUtils.startUploadFile(DeviceInfo.MAC, listener);
            }
        }).start();
    }


}
