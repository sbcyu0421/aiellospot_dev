package ai.aiello.aiellospot.core.media;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

import ai.aiello.aiellospot.R;

public class AielloSoundPool {

    private static SoundPool soundPool;
    private static SoundPool soundPoolSolid;

    public final static int wakeup = 1;
    public final static int vad_end = 2;
    public final static int card_in = 3;
    public final static int card_out = 4;
    public final static int broadcast_ring = 5;

    public final static int morning_call = 1;
    public final static int pbr_success = 2;

    public static void initSoundPool(Context context) {
        //sound pool for normal use
        soundPool = new SoundPool(10, AudioManager.STREAM_RING, 3);
        soundPool.load(context, R.raw.iamhere, wakeup);
        soundPool.load(context, R.raw.vad_end2, vad_end);
        soundPool.load(context, R.raw.effect_keyin, card_in);
        soundPool.load(context, R.raw.effect_keyout, card_out);
        soundPool.load(context, R.raw.broadcast_ring, broadcast_ring);

        //sound pool for alarm
        soundPoolSolid = new SoundPool(10, AudioManager.STREAM_ALARM, 3);
        soundPoolSolid.load(context, R.raw.alarm_wakeup2, morning_call);
        soundPoolSolid.load(context, R.raw.pbr_success, pbr_success);
    }


    public static int play(int sound) {
        int sessionId;
        switch (sound) {
            case wakeup:
                sessionId = soundPool.play(sound, 0.3F, 0.3F, 0, 0, 1.0F);
                break;
            case vad_end:
                sessionId = soundPool.play(sound, 0.5F, 0.5F, 0, 0, 1.0F);
                break;
            default:
                sessionId = soundPool.play(sound, 1.0F, 1.0F, 0, 0, 1.0F);
        }
        return sessionId;
    }

    public static int playDefaultVolSound(int sound){
        int sessionId = 0;
        switch (sound) {
            case morning_call:
                sessionId = soundPoolSolid.play(sound, 0.6F, 0.6F, 0, -1, 1.0F);
                break;
            case pbr_success:
                sessionId = soundPoolSolid.play(sound, 0.2F, 0.2F, 0, 0, 1.0F);
                break;
        }
        return sessionId;
    }

    public static void stopAlarm(int sessionId) {
        soundPoolSolid.stop(sessionId);
    }

}
