package ai.aiello.aiellospot.core.system;


import ai.aiello.aiellospot.events.system.SystemEvent;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;

import com.litesuits.android.log.Log;

import org.greenrobot.eventbus.EventBus;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class WifiReceiver extends BroadcastReceiver {

    public static String TAG = WifiReceiver.class.getSimpleName();
    public static boolean wifiStatus = false;
    public static WifiLevel wifiLevel = WifiLevel.NULL;
    public static int linkSpeed = 0;
    public static int WifiRSSI = 0;
    public static String WifiSSID = "";
    public static ExecutorService executorService = Executors.newFixedThreadPool(1);


    public enum WifiLevel {
        NULL,
        POOR,
        FAIR,
        GOOD,
        EXCELLENT
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        Log.e(TAG, "onReceive = "+ intent.getAction());
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = conMan.getActiveNetworkInfo();
                if (netInfo != null && netInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                    Log.e(TAG, "Have Wifi Connection");
                    if (!wifiStatus) {
                        wifiStatus = true;
                        EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.WIFI_CONNECTED));
                    }
                } else {
                    Log.e(TAG, "Don't have Wifi Connection");
                    if (wifiStatus) {
                        wifiStatus = false;
                        EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.WIFI_DISCONNECTED));
                    }
                }
                linkSpeed = ((WifiManager) context.getSystemService(Context.WIFI_SERVICE)).getConnectionInfo().getLinkSpeed();
                WifiRSSI = ((WifiManager) context.getSystemService(Context.WIFI_SERVICE)).getConnectionInfo().getRssi();
                WifiSSID = ((WifiManager) context.getSystemService(Context.WIFI_SERVICE)).getConnectionInfo().getSSID();

                if (WifiRSSI < -89) {
                    wifiLevel = WifiLevel.NULL;
                } else if (WifiRSSI < -75) {
                    wifiLevel = WifiLevel.POOR;
                } else if (WifiRSSI < -67) {
                    wifiLevel = WifiLevel.FAIR;
                } else if (WifiRSSI < -55) {
                    wifiLevel = WifiLevel.GOOD;
                } else {
                    wifiLevel = WifiLevel.EXCELLENT;
                }
                Log.d(TAG, "WifiRSSI = " + WifiRSSI + ", level = " + wifiLevel.name() + ", linkSpeed = " + linkSpeed);
                EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.WIFI_SIGNAL_CHANGE));
            }
        });

    }

}
