package ai.aiello.aiellospot.core.log;

import android.util.Log;

public class MusicLogItem {

    public static SpotUserTraceLog3.EventSubject logSubject;
    public static SpotUserTraceLog3.EventAction logAction;
    public static String logRequest = "";
    public static String logResponse = "";
    public static String logData = "";
    public static SpotUserTraceLog3.EventInputType type = null;

    public static boolean header = false;
    public static boolean footer = false;

    public static void buildMusicItem (SpotUserTraceLog3.EventSubject logSubject,
                                       SpotUserTraceLog3.EventAction logAction,
                                       String logRequest,
                                       String logResponse,
                                       String logData,
                                       SpotUserTraceLog3.EventInputType type) {

        try {
            if (type != null) {
                MusicLogItem.logSubject = logSubject;
                MusicLogItem.logRequest = logRequest;
                MusicLogItem.logAction = logAction;
                MusicLogItem.logResponse = logResponse;
                MusicLogItem.type = type;
                MusicLogItem.logData = "";
                header = true;
                footer = false;
            } else {
                MusicLogItem.logData = logData;
                footer = true;
            }
            Log.d("MusicLogItem", String.format("buildMusicItem: type = %s, action = %s, MusicLogItem.logRequest = %s", type, logAction, MusicLogItem.logRequest));
            if (header && footer){
                switch (MusicLogItem.type){
                    case VOICE:
                        SpotUserTraceLog3.getInstance().buildVoiceEventLog(
                                MusicLogItem.logSubject,
                                MusicLogItem.logAction,
                                MusicLogItem.logRequest,
                                MusicLogItem.logResponse,
                                MusicLogItem.logData
                        );
                        break;

                    case UI:
                        SpotUserTraceLog3.getInstance().buildUIEventLog(
                                MusicLogItem.logSubject,
                                MusicLogItem.logAction,
                                MusicLogItem.logData
                        );
                        break;
                }
                MusicLogItem.clear();
                Log.d("MusicLogItem", "send");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void clear() {
        MusicLogItem.logSubject = null;
        MusicLogItem.logRequest = "";
        MusicLogItem.logAction = null;
        MusicLogItem.logResponse = "";
        MusicLogItem.logData = "";
        MusicLogItem.type = null;
        header = false;
        footer = false;
    }


}
