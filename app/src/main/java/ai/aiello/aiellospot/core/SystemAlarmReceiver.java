package ai.aiello.aiellospot.core;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import ai.aiello.aiellospot.events.system.SystemEvent;

public class SystemAlarmReceiver extends BroadcastReceiver {

    private String TAG = SystemAlarmReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive: " + intent.getAction());
        EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.ALARM_RECEIVER, intent));
    }

}
