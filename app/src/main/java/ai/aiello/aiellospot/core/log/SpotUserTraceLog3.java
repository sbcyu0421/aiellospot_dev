package ai.aiello.aiellospot.core.log;

import android.util.Log;

import androidx.annotation.Nullable;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.views.activity.new_ota.VersionCheckerT;
import ai.aiello.aiellospot.core.info.UserInfo;
import ai.aiello.aiellospot.core.device.DeviceControl;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.modules.checkio.CheckInOutModuleManager;
import ai.aiello.aiellospot.modules.music.MusicServiceModuleManager;
import ai.aiello.aiellospot.core.messager.MessageManager;
import ai.aiello.aiellospot.core.soundai.SoundAIManager;
import ai.aiello.aiellospot.core.system.BatteryListener;
import ai.aiello.aiellospot.core.system.Pinger;
import ai.aiello.aiellospot.core.system.WifiReceiver;
import ai.aiello.aiellospot.utlis.AielloFileUtils;
import ai.aiello.aiellospot.utlis.Formatter;
import ai.aiello.aiellospot.modules.voip.VoipModuleManager;

public class SpotUserTraceLog3 {

    private String TAG = this.getClass().getSimpleName();

    private static SpotUserTraceLog3 instance;
    private UserLogBean userLogBean;
    private String userbeanTemplate;
    private ExecutorService logger = Executors.newFixedThreadPool(5);
    private SpotUserTraceLog3() {
        userbeanTemplate = AielloFileUtils.readFileFromAssets("userbean.json");
    }

    public static SpotUserTraceLog3 getInstance() {
        if (instance == null) {
            instance = new SpotUserTraceLog3();
        }
        return instance;
    }

    private void send() {
        try {
            setState();
            setDeviceInfo();
            userLogBean.setLogTime(Formatter.formatElsDate(Calendar.getInstance().getTime()));
            MessageManager.getInstance().send(MessageManager.ProducerType.USER_TRACE, new JSONObject(new Gson().toJson(userLogBean)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setDeviceInfo() {
        Log.d(TAG, "setDeviceInfo: ");
        try {
            userLogBean.getDeviceInfo().getApkVersion().setSpotVersion(VersionCheckerT.apk_ver);
            userLogBean.getDeviceInfo().getApkVersion().setMusicVersion(VersionCheckerT.music_ver);
            userLogBean.getDeviceInfo().getApkVersion().setPushVersion(VersionCheckerT.push_ver);
            userLogBean.getDeviceInfo().getApkVersion().setSaiVersion(VersionCheckerT.sai_ver);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            userLogBean.getDeviceInfo().getRoom().setHotelName(DeviceInfo.hotelName);
            userLogBean.getDeviceInfo().getRoom().setRoomNo(DeviceInfo.roomName);
            userLogBean.getDeviceInfo().getRoom().setRoomType(DeviceInfo.roomType);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            userLogBean.getDeviceInfo().setImageVersion(VersionCheckerT.img_ver);
            userLogBean.getDeviceInfo().setMac(DeviceInfo.MAC);
            userLogBean.getDeviceInfo().setUuid(DeviceInfo.uuid);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void buildUIEventLog(EventSubject subject, EventAction action, @Nullable String data) {
        logger.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    userLogBean = new Gson().fromJson(userbeanTemplate, UserLogBean.class);
                    userLogBean.getEventTrigger().getIntent().setSubject(subject.name());
                    userLogBean.getEventTrigger().getIntent().setAction(action.name());
                    userLogBean.getEventTrigger().setType(EventInputType.UI.name());
                    userLogBean.getEventTrigger().setData(data);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                send();
            }
        });
    }

    public void buildVoiceEventLog(EventSubject subject, EventAction action, String request, String response, @Nullable String data) {
        logger.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    userLogBean = new Gson().fromJson(userbeanTemplate, UserLogBean.class);
                    userLogBean.getEventTrigger().getIntent().setSubject(subject.name());
                    userLogBean.getEventTrigger().getIntent().setAction(action.name());
                    userLogBean.getEventTrigger().setType(EventInputType.VOICE.name());
                    userLogBean.getEventTrigger().setRequest(request);
                    userLogBean.getEventTrigger().setResponse(response);
                    userLogBean.getEventTrigger().setData(data);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                send();
            }
        });
    }

    public void buildSystemEventLog(EventSubject subject, EventAction action, @Nullable String data) {
        logger.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    userLogBean = new Gson().fromJson(userbeanTemplate, UserLogBean.class);
                    userLogBean.getEventTrigger().getIntent().setSubject(subject.name());
                    userLogBean.getEventTrigger().getIntent().setAction(action.name());
                    userLogBean.getEventTrigger().setType(EventInputType.SYSTEM.name());
                    userLogBean.getEventTrigger().setData(data);
                    send();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void buildHardwareEventLog(EventSubject subject, EventAction action, @Nullable String data) {
        logger.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    userLogBean = new Gson().fromJson(userbeanTemplate, UserLogBean.class);
                    userLogBean.getEventTrigger().getIntent().setSubject(subject.name());
                    userLogBean.getEventTrigger().getIntent().setAction(action.name());
                    userLogBean.getEventTrigger().setType(EventInputType.HARDWARE.name());
                    userLogBean.getEventTrigger().setData(data);
                    send();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setState() {

        try {
            userLogBean.getStateOverview().getVoip().setStateMode(VoipModuleManager.getInstance().currentState.getName().name());
            userLogBean.getStateOverview().getVoip().setInfo("");
            userLogBean.getStateOverview().getVoip().setIp(VoipModuleManager.voipUrl);
            userLogBean.getStateOverview().getVoip().setConnect(VoipModuleManager.getInstance().connStatus.name());
        } catch (Exception ignore) {
        }


        try {
            userLogBean.getStateOverview().getMusic().setStateMode(MusicServiceModuleManager.playerStatus.name());
            userLogBean.getStateOverview().getMusic().setSource(MusicServiceModuleManager.vendor.name());
            userLogBean.getStateOverview().getMusic().setAlbumInfo(MusicServiceModuleManager.trackObject.getAlbumName());
            userLogBean.getStateOverview().getMusic().setTrackInfo(MusicServiceModuleManager.trackObject.getTrackName());
        } catch (Exception ignore) {
        }

        try {
            userLogBean.getStateOverview().getDevice().getPower().setBattery(BatteryListener.currentElectricity);
            userLogBean.getStateOverview().getDevice().getPower().setCharging(BatteryListener.powerState);
        } catch (Exception ignore) {
        }


        try {
            userLogBean.getStateOverview().getDevice().getWifi().setWifiRSSI(WifiReceiver.WifiRSSI);
            userLogBean.getStateOverview().getDevice().getWifi().setWifiSpeed(Pinger.speed);
            userLogBean.getStateOverview().getDevice().getWifi().setWifiSSID(WifiReceiver.WifiSSID);
        } catch (Exception ignore) {
        }

        try {
            userLogBean.getStateOverview().getDevice().getSound().setVolume(DeviceControl.getCurrentVolume());
        } catch (Exception ignore) {
        }

        try {
            userLogBean.getStateOverview().getDevice().getScreen().setBrightness(DeviceControl.getBrightness());
        } catch (Exception ignore) {
        }

        try {
            userLogBean.getStateOverview().getDevice().getBluetooth().setEnable(DeviceControl.blueToothState.name());
            userLogBean.getStateOverview().getDevice().getBluetooth().setConnection(DeviceControl.bondedDevicesName);
        } catch (Exception ignore) {
        }

        try {
            userLogBean.getStateOverview().getDevice().getWakeUp().setEnable(SoundAIManager.isWakeUpEnable());
        } catch (Exception ignore) {
        }


        boolean checkInState = true;
        if (UserInfo.userName.equals("default_user_name") || UserInfo.userName.equals(""))
            checkInState = false;

        try {
            userLogBean.getStateOverview().getUserInfo().setName(UserInfo.userName);
            userLogBean.getStateOverview().getUserInfo().setGender(UserInfo.userGender);
            userLogBean.getStateOverview().getUserInfo().setCheckIn(checkInState);
            userLogBean.getStateOverview().getUserInfo().setCheckIoTime(Formatter.formatToISO8601(CheckInOutModuleManager.getInstance().checkInOutTime));
            userLogBean.getStateOverview().getUserInfo().setCountry(UserInfo.userNationality);
            userLogBean.getStateOverview().getUserInfo().setLanguage(ChatApplication.system_lang.name());
        } catch (Exception ignore) {
        }

    }


    public enum EventInputType {
        UI,
        SYSTEM,
        VOICE,
        HARDWARE
    }

    public enum EventSubject {
        NONE,
        //Basic Module
        ROOM_STATUS,

        // Advanced Module (default enable)
        CLOCK,
        WEATHER,
        CALCULATOR,
        WORLD_CLOCK,
        CONSTELLATION,
        IDIOM,
        CHAT,
        HOTEL_FEATURE,
        MODULE_NOT_SUPPORT,

        // Advanced Module (default disable)
        IOT,
        TAXI,
        VOIP,
        CHECKOUT,
        SURROUNDING,
        MARKETING,
        FAQ,
        COMPLAINT,
        MUSIC, DELIVERY,
        UNSUPPORT_SERVICE,
        NEWS, REPAIRMENT,
        TIME_OUT,
        UNHANDLE_TNTENT,
        VIEW_CONTROL,
        JOKE,
        SCREEN,
        BLUETOOTH,
        VOLUME, LOCALE, ADS, WAKEUP,
        POWER,
        LAUNDRY,
    }

    public enum EventAction {
        NONE,
        CANCEL,
        SETUP,
        DELETE,
        ON,
        OFF,
        SELECT,
        CALL,
        DIAL,
        QUERY,
        PLAY,
        PAUSE,
        NEXT,
        PREV,
        UNKNOWN,
        DOWN,
        UP,
        MIN,
        MAX,
        ACCEPT,
        DECLINE,
        REPLY,
        ERROR,
        SET_VALUE,
        OFFLINE, SET, CONNECTION, DISCONNECTION,
        PLUG_IN,
        DND_ON, DND_OFF, MUR_ON, MUR_OFF, PLUG_OUT
    }
}
