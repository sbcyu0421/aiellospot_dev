package ai.aiello.aiellospot.core.config;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;

public class SystemConfig {

    public static String aielloHub = "";
    public static String iotHub = "";
    public static String multiService;
    public static String wifi_country;

    public static class OTA {
        public static String apk_cloud;
        public static String apk_push_name;
        public static boolean support_action;
    }

    public static class ChatbotBean {
        public static String chatbotUrl;
        public static String chatbotCleanFormUrl;
        public static String chatbotKey;
    }

    public static class WakeupFtpBean {
        public static String url;
        public static String username;
        public static String password;
    }

    public static class LogFtpBean {
        public static String url;
        public static String username;
        public static String password;
    }

    public static class Kafka {
        public static int consumer_freq;
        public static int consumer_data_length;
        public static int keepalive_freq;
        public static APIConfig.WEBAPI kafkaAPI;
    }


    public static class AsrBean {
        public static JSONObject asrObject;
        public static String credentials_xfyun;
        public static String credentials_baidu;
        public static String credentials_google;

        public static HashMap<String, String> replace_phrases_tw = new HashMap<>();
        public static HashMap<String, String> replace_phrases_cn = new HashMap<>();
        public static HashMap<String, String> replace_phrases_en = new HashMap<>();
        public static HashMap<String, String> replace_phrases_ja = new HashMap<>();
        public static String customerAccentSupportList;
    }


    public static class WifiPingBean {
        public static int freq = 10;
        public static int threshold = 300;
        public static String ip = "8.8.8.8";
        public static int avgTimes = 24;
    }

    public static class TTS {
        static String speechModel_tw;
        static String speechModel_cn;
        static String speechModel_en;
        static String speechModel_ja;

        public static String getSpeechModel(Locale locale){
            if (Locale.JAPANESE.equals(locale)) {
                return speechModel_ja;
            } else if (Locale.TAIWAN.equals(locale)) {
                return speechModel_tw;
            } else if (Locale.CHINA.equals(locale)) {
                return speechModel_cn;
            }
            return speechModel_en;
        }

    }


}
