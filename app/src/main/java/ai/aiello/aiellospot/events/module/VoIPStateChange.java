package ai.aiello.aiellospot.events.module;

public class VoIPStateChange {


    public static final String INCOMING_CALL = "inComingCall";
    public static final String INCOMING_CALL_CANCELED = "inComingCallCanceled";
    public static final String ACCEPT_CALL = "acceptCall";
    public static final String DECLINE_CALL = "declineCall";
    public static final String CANCEL_MAKE_CALL = "cancelMakeCall";
    public static final String DISABLE = "disable";

    public static final String MAKE_CALL_RESPONSE = "makeCallRes";
    public static final String ON_MAKE_CALL_WAITING = "onMakeCallWaiting";
    public static final String NO_ANSWER = "noAnswer";
    public static final String CALL_END = "callEnd";

    public static final String VOIP_STATUS = "voipStatus";

    public static final String UPDATE_ON_CALL_TIME = "UPDATE_ON_CALL_TIME";

    private String status;
    private String message;

    public VoIPStateChange(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

}
