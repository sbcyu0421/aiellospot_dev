package ai.aiello.aiellospot.events.module;

/**
 * Created by a1990 on 2019/3/13.
 */

public class CheckInOutStateChange {

    public enum Target {
        ENTRY_HOME_PAGE
    }

    private Target target;
    private String data;

    public CheckInOutStateChange(Target target, String data) {
        this.target = target;
        this.data = data;
    }

    public Target getTarget() {
        return target;
    }

    public String getData() {
        return data;
    }
}
