package ai.aiello.aiellospot.events.module;

public class IoTStateChange {

    public enum Target {
        ON_AC_PANEL_UPDATE
    }

    public Target target;
    private String data;

    public IoTStateChange(Target target, String data) {
        this.target = target;
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public Target getTarget() {
        return target;
    }


}
