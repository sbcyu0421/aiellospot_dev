package ai.aiello.aiellospot.events.module;

public class MusicStateChange {

    public enum Target {
        ON_FETCH_MENU_COMPLETED,
        ON_TRACK_INFO_LOADED,
        ON_PLAY,
        ON_PAUSE,
        ON_PROGRESS_BAR_UPDATE,
        ON_PBR_SUCCESS,
        LOADING,
        ERROR,
    }

    private Target target;

    public MusicStateChange(Target target) {
        this.target = target;
    }

    public Target getTarget() {
        return target;
    }
}
