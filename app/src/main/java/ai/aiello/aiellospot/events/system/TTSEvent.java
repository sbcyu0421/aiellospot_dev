package ai.aiello.aiellospot.events.system;

/**
 * Created by a1990 on 2019/3/13.
 */

public class TTSEvent {

    public static final int TTS_COMPLETED = 1;
    private int state;
    public TTSEvent(int state) {
        this.state = state;
    }

    public int getState() {
        return state;
    }
}
