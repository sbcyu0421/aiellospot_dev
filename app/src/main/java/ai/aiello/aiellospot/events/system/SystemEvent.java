package ai.aiello.aiellospot.events.system;

import android.content.Intent;
import android.util.Log;

/**
 * Created by a1990 on 2019/3/13.
 */

public class SystemEvent {

    private static String TAG = SystemEvent.class.getSimpleName();

    public enum Type {

        // wifi
        WIFI_CONNECTED,
        WIFI_DISCONNECTED,
        WIFI_SIGNAL_CHANGE,

        // bluetooth
        BT_STATE_CHANGE,

        //
        TIME_DISMISS,

        // alert
        ALARM_RECEIVER,

        // wakeup
        WAKEUP_DISABLE,
        WAKEUP_ENABLE,

        // idle, busy
        APP_STATE_CHANGED,

        // media
        ALL_CHANNEL_STOP,
        TEMP_MEDIA_OCCUPY, // sound from voip, alarm...
        TEMP_MEDIA_RELEASE,

        /*
         * For long term audio occupy, mostly music source.
         * Will set other playing music to "STOP" state by all means.
         * Please always provide "musicSource" value in SystemEvent when sending "MEDIA_OCCUPY"
         */
        MEDIA_OCCUPY,

        // module
        MODULE_START,
        BUSY_STATE_HOT_RELOAD,
        IDLE_STATE_HOT_RELOAD,

    }

    private Type type;
    private String data;
    private Intent intent;
    private String musicSource = "";

    public SystemEvent(Type type) {
        this.type = type;
        Log.d(TAG, "SystemEvent: " + type);
    }

    public SystemEvent(Type type, String data) {
        this.type = type;
        this.data = data;
    }

    public SystemEvent(Type type, Intent intent) {
        this.type = type;
        this.intent = intent;
    }

    public SystemEvent(Type type, Intent intent, String musicSource) {
        this.type = type;
        this.intent = intent;
        this.musicSource = musicSource;
    }

    public Intent getIntent() {
        return intent;
    }

    public Type getType() {
        return type;
    }

    public String getData() {
        return data;
    }

    public String getMusicSource() {
        return musicSource;
    }
}
