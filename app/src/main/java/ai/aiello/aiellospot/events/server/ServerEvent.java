package ai.aiello.aiellospot.events.server;

/**
 * Created by a1990 on 2019/3/13.
 */

public class ServerEvent {

    public enum Type {
        PBR_AUTH, DND, MUR,
        IMAGECHECK, IMAGEUPDATE, APPREBOOT, SILENTRESTART, DEVICEREBOOT,
        LOGUPLOAD, UICONFIG_RELOAD, IOTCONFIG_RELOAD,
        SATISFACTION, NOTIFICATION,
        CHECK_IN, CHECK_OUT,
        IOT_EVENT
    }

    private Type type;
    private String data;

    public ServerEvent(Type type) {
        this.type = type;
    }

    public ServerEvent(Type type, String data) {
        this.type = type;
        this.data = data;
    }

    public Type getType() {
        return type;
    }

    public String getData() {
        return data;
    }
}
