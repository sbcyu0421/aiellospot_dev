package ai.aiello.aiellospot.events.module;

public class RoomStatusStateChange {

    public static final int ERROR = 1;
    public static final int SUCCESS = 2;
    private int stateCode = -1;

    public RoomStatusStateChange(int stateCode) {
        this.stateCode = stateCode;
    }

    public int getStateCode() {
        return stateCode;
    }
}
