package ai.aiello.aiellospot.events.module;

public class AlarmStateChange {

    private Event state;

    public enum Event {
        ALARM_RISE, ALARM_TIMEOUT
    }

    public AlarmStateChange(Event state) {
        this.state = state;
    }

    public Event getState() {
        return state;
    }
}
