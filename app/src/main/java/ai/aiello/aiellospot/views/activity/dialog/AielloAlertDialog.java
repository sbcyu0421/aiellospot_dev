package ai.aiello.aiellospot.views.activity.dialog;

import ai.aiello.aiellospot.R;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by a1990 on 2019/4/9.
 */


public class AielloAlertDialog extends Dialog {

    private TextView tv_title;
    private TextView tv_content;
    private ImageView img_content;

    private LinearLayout btn_area;
    private LinearLayout image_area;
    private Context context;
    private Button btn_confirm;
    private Button btn_cancel;
    private Bitmap bitmap;
    private static boolean showing = false;
    private boolean showTitle = false;

    public AielloAlertDialog(Context context, int theme) {
        super(context, theme);
        this.context = context;
        setOption();
    }


    private void setOption() {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setCancelable(false);
        this.setContentView(R.layout.alert_dialog);
        tv_title = this.findViewById(R.id.ad_tv_title);
        tv_content = this.findViewById(R.id.ad_tv_content);
        btn_area = this.findViewById(R.id.btn_area);
        image_area = this.findViewById(R.id.image_area);

    }

    public AielloAlertDialog setTitle(String msg) {
        showTitle = true;
        tv_title.setText(msg);
        return this;
    }

    public AielloAlertDialog setTitle(String msg, int color) {
        showTitle = true;
        tv_title.setText(msg);
        tv_title.setTextColor(color);
        return this;
    }


    public AielloAlertDialog setTextContent(String msg) {
        if (tv_content != null) {
            tv_content.setText(msg);
        }
        return this;
    }


    public AielloAlertDialog setTextContent(String msg, int color) {
        if (tv_content != null) {
            tv_content.setText(msg);
            tv_content.setTextColor(color);
        }
        return this;
    }

    public AielloAlertDialog setImageContent(Bitmap bitmap) {
        this.bitmap = bitmap;
        img_content = new ImageView(context);
        img_content.setImageBitmap(bitmap);
        img_content.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

        ViewGroup.LayoutParams param = new LinearLayout.LayoutParams(
                250, 250, 1.0f);
        img_content.setLayoutParams(param);
        image_area.addView(img_content);
        return this;
    }


    public AielloAlertDialog setOnConfirmListener(String message, View.OnClickListener listener) {

        btn_confirm = new Button(context);

        btn_confirm.setBackground(context.getDrawable(R.drawable.dia_btn_round));
        btn_confirm.setText(message);
        btn_confirm.setTextSize(26);
        ViewGroup.LayoutParams param = new LinearLayout.LayoutParams(
                46, 130, 1.0f);
        btn_confirm.setLayoutParams(param);

        ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(param);
        marginLayoutParams.leftMargin = 5;
        btn_area.addView(btn_confirm);
        btn_confirm.setOnClickListener(listener);

        return this;
    }

    public AielloAlertDialog setOnConfirmListener(View.OnClickListener listener) {

        if (listener == null) {
            try {
                btn_area.removeView(btn_confirm);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return this;
        }

        btn_confirm = new Button(context);

        btn_confirm.setBackground(context.getDrawable(R.drawable.dia_btn_round));
        btn_confirm.setText(context.getString(R.string.confirm));
        btn_confirm.setTextSize(26);
        ViewGroup.LayoutParams param = new LinearLayout.LayoutParams(
                46, 130, 1.0f);
        btn_confirm.setLayoutParams(param);

        ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(param);
        marginLayoutParams.leftMargin = 5;
        btn_area.addView(btn_confirm);
        btn_confirm.setOnClickListener(listener);

        return this;
    }

    public AielloAlertDialog setOnCancelListener(View.OnClickListener listener) {

        btn_cancel = new Button(context);
        btn_cancel.setBackground(context.getDrawable(R.drawable.dia_btn_round));
        btn_cancel.setText(context.getString(R.string.cancel));
        btn_cancel.setTextSize(26);
        ViewGroup.LayoutParams param = new LinearLayout.LayoutParams(
                46, 130, 1.0f);
        btn_cancel.setLayoutParams(param);

        ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(param);
        marginLayoutParams.rightMargin = 5;
        btn_area.addView(btn_cancel);
        btn_cancel.setOnClickListener(listener);

        return this;
    }


    @Override
    public void show() {

        if (btn_cancel != null) {
            ViewGroup.LayoutParams param_confirm = btn_confirm.getLayoutParams();
            ((LinearLayout.LayoutParams) param_confirm).setMargins(20, 0, 0, 0);
            btn_confirm.setLayoutParams(param_confirm);

            ViewGroup.LayoutParams param_cancel = btn_cancel.getLayoutParams();
            ((LinearLayout.LayoutParams) param_cancel).setMargins(0, 0, 20, 0);
            btn_cancel.setLayoutParams(param_cancel);
        }

        if (!showTitle) {
            ViewGroup.LayoutParams param_confirm = tv_title.getLayoutParams();
            param_confirm.height = 0;
            tv_title.setLayoutParams(param_confirm);
        }

        Window window = this.getWindow();
        window.setLayout(760, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN;
        super.show();
        window.getDecorView().setSystemUiVisibility(uiOptions);
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        showing = true;
    }

    @Override
    public void dismiss() {
        if (bitmap != null) {
            bitmap.recycle();
        }
        bitmap = null;
        showing = false;
        super.dismiss();
    }
}

