package ai.aiello.aiellospot.views.activity.home;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.databinding.DataBindingUtil;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.databinding.ActivityDynamicConstraintHomeBinding;
import ai.aiello.aiellospot.views.activity.BaseActivity;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.HomeLayoutManager;

public abstract class AbstractHomeActivity extends BaseActivity {

    public String TAG = AbstractHomeActivity.class.getSimpleName();

    private ActivityDynamicConstraintHomeBinding binding;

    int marginBetweenButtons = 3;

    abstract int getCurrentPage();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_dynamic_constraint_home);

        invalidate(false);
    }

    private ArrayList<View> btns = new ArrayList<>();
    private ArrayList<View> unLocateBtns = new ArrayList<>();

    private void invalidate(boolean updateView) {
        for (View btn : btns) {
            binding.clDynamicHomeBase.removeView(btn);
        }
        btns = new ArrayList<>();
        unLocateBtns = new ArrayList<>();
        for (AbstractViewItem viewItem : HomeLayoutManager.getInstance().getLayoutItems()) {
            try {
                renderUI(viewItem);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (updateView) updateView();
    }

    private void renderUI(AbstractViewItem viewItem) {

        if (viewItem.page != getCurrentPage()) return;

        String layoutName = viewItem.layoutName;

        int column = viewItem.columnIndex;
        int row = viewItem.rowIndex;
        int width = viewItem.width;
        int height = viewItem.height;

        View button = viewItem.buildButton(this);

        locatePositionAndAddToView(button, column, row, width, height);
        if (btns.contains(button)) setButtonSize(button);

        viewItem.bindView(new WeakReference<>(this), new WeakReference<>(button));
        viewItem.handelBtnClickLogic();

    }

    @Override
    public void onHotReload() {
        invalidate(true);
    }

    private void locatePositionAndAddToView(View button, int columnIndex, int rowIndex, int width, int height) {
        LinearLayout leftView = locateColumnCoordinate(columnIndex);
        LinearLayout topView = locateRowCoordinate(rowIndex);
        LinearLayout rightView = locateColumnCoordinate(columnIndex + width);
        LinearLayout bottomView = locateRowCoordinate(rowIndex + height);
        if (leftView == null || topView == null || rightView == null || bottomView == null) {
            Log.d(TAG, "invalid column / row index / width / height, button not added to view");
            unLocateBtns.add(button); // make sure button was referenced, else will be gc since only reference to this button is weakref at ViewItem
            return;
        }

        // setting up constraints with if provide correct coordinate
        button.setId(viewIdGenerator(columnIndex, rowIndex));

        ConstraintSet set = new ConstraintSet();
        binding.clDynamicHomeBase.addView(button);
        btns.add(button);
        set.clone(binding.clDynamicHomeBase);
        set.connect(button.getId(), ConstraintSet.START, leftView.getId(), ConstraintSet.START, marginBetweenButtons);
        set.connect(button.getId(), ConstraintSet.TOP, topView.getId(), ConstraintSet.TOP, marginBetweenButtons);
        set.connect(button.getId(), ConstraintSet.END, rightView.getId(), ConstraintSet.START, marginBetweenButtons);
        set.connect(button.getId(), ConstraintSet.BOTTOM, bottomView.getId(), ConstraintSet.TOP, marginBetweenButtons);
        set.applyTo(binding.clDynamicHomeBase);
    }

    private int viewIdGenerator(int columnIndex, int rowIndex) {
        String c = String.valueOf(columnIndex);
        String r = String.valueOf(rowIndex);
        return Integer.parseInt(c + r);
    }

    private LinearLayout locateColumnCoordinate(int columnIndex) {
        LinearLayout columnView = null;
        switch (columnIndex) {
            case 0:
                columnView = binding.coordinateC0;
                break;
            case 1:
                columnView = binding.coordinateC1;
                break;
            case 2:
                columnView = binding.coordinateC2;
                break;
            case 3:
                columnView = binding.coordinateC3;
                break;
            case 4:
                columnView = binding.coordinateC4;
                break;
            case 5:
                columnView = binding.coordinateC5;
                break;
            case 6:
                columnView = binding.coordinateC6;
                break;
            case 7:
                columnView = binding.coordinateC7;
                break;
            case 8:
                columnView = binding.coordinateC8;
                break;
        }
        return columnView;
    }

    private LinearLayout locateRowCoordinate(int rowIndex) {
        LinearLayout rowView = null;
        switch (rowIndex) {
            case 0:
                rowView = binding.coordinateR0;
                break;
            case 1:
                rowView = binding.coordinateR1;
                break;
            case 2:
                rowView = binding.coordinateR2;
                break;
            case 3:
                rowView = binding.coordinateR3;
                break;
            case 4:
                rowView = binding.coordinateR4;
                break;
        }
        return rowView;
    }


    private void setButtonSize(View button) {
        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) button.getLayoutParams();
        params.width = ConstraintLayout.LayoutParams.MATCH_CONSTRAINT;
        params.height = ConstraintLayout.LayoutParams.MATCH_CONSTRAINT;
        button.setLayoutParams(params);
    }


    @Override
    protected void onResume() {
        super.onResume();
        updateView();
    }


    public void updateView() {
        for (AbstractViewItem viewItem : HomeLayoutManager.getInstance().getLayoutItems()) {
            if (viewItem.page == getCurrentPage()) {
                viewItem.updateView();
            }
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        unbindView(this);

        try {
            btns.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            unLocateBtns.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void unbindView(Activity activity) {
        for (AbstractViewItem viewItem : HomeLayoutManager.getInstance().getLayoutItems()) {
            if (viewItem.page == getCurrentPage()) {
                viewItem.unbindView(activity);
            }
        }
    }

}

