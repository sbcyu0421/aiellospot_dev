package ai.aiello.aiellospot.views.activity.showcase.fragments

import ai.aiello.aiellospot.ChatApplication
import ai.aiello.aiellospot.R
import ai.aiello.aiellospot.views.activity.showcase.beans.HotelFeatureDataBean
import android.app.Dialog
import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.MultiFormatWriter
import com.journeyapps.barcodescanner.BarcodeEncoder


class HotelFeatureDetailDialog(context: Context, val item: HotelFeatureDataBean): Dialog(context) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        val window: Window = this.getWindow()
        window.attributes.gravity = Gravity.BOTTOM
        this.setContentView(R.layout.dialog_hotel_feature_detail)
        setOption()

    }


    private fun setOption() {
        val tv_title = this.findViewById<TextView>(R.id.tv_dialog_hotel_feature_title)
        val tv_content = this.findViewById<TextView>(R.id.tv_dialog_hotel_feature_content)
        val tv_take_me_to = this.findViewById<TextView>(R.id.tv_hotel_feature_take_me_to)
        val image_area = this.findViewById<ImageView>(R.id.image_hotel_feature_detail)
        val linear_qr = this.findViewById<LinearLayout>(R.id.linear_qr)

        tv_title?.text = item.getTitle(ChatApplication.system_lang)

        tv_content?.text = item.getContent(ChatApplication.system_lang)

        tv_take_me_to?.setText(context.resources.getText(R.string.take_me))
        tv_take_me_to.setBackgroundResource(R.color.take_me_to_qr)
        tv_take_me_to?.setTextColor(context.resources.getColor(R.color.white))
        tv_take_me_to?.setTextSize(TypedValue.COMPLEX_UNIT_PX, 28F)

        if (!item.getQRCodeURL(ChatApplication.system_lang).isNullOrBlank()) {
            image_area.setImageBitmap(item.getQRCodeURL(ChatApplication.system_lang)?.let {
                getQRcodeBitmap(it)
            })
        }

//        item.qr_code_url?.let { image_area.setImageBitmap(getQRcodeBitmap(it)) }
        if (item.getQRCodeURL(ChatApplication.system_lang).isNullOrBlank()) {
            linear_qr.visibility = View.GONE
        }

        tv_title?.postDelayed({ tv_title.setSelected(true) },1500)

    }

    private fun getQRcodeBitmap(url: String): Bitmap {
        val multiFormatWriter = MultiFormatWriter()
        val hints = mutableMapOf<EncodeHintType, Int>()
        hints[EncodeHintType.MARGIN] = 0
        val bitMatrix = multiFormatWriter.encode(url, BarcodeFormat.QR_CODE, 223, 223, hints)
        val barcodeEncoder = BarcodeEncoder()
        val bitmap = barcodeEncoder.createBitmap(bitMatrix)
        return bitmap
    }

    override fun show() {

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        val uiOptions = (View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or View.SYSTEM_UI_FLAG_FULLSCREEN)
        window?.decorView?.systemUiVisibility = uiOptions
        window?.setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        super.show()
        window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
    }
}