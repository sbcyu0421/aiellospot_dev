package ai.aiello.aiellospot.views.activity.new_ota;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.core.config.APIConfig;
import ai.aiello.aiellospot.core.config.SystemConfig;
import ai.aiello.aiellospot.core.info.DeviceInfo;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.system.ServiceInstallReceiver;
import ai.aiello.aiellospot.utlis.AielloFileUtils;
import ai.aiello.aiellospot.utlis.Http;

import android.app.Activity;
import android.os.AsyncTask;

import com.litesuits.android.log.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class OtaAsyncTask extends AsyncTask<Void, Integer, Void> {

    private static final String TAG = OtaAsyncTask.class.getSimpleName();
    private OtaProgressListener listener;
    private Activity activity;
    private Map<String, ServiceAPK> serviceAPKMap;
    private int msgCode = 0;
    private String otaMsg = "";

    public OtaAsyncTask(Activity activity, OtaProgressListener listener, Map<String, ServiceAPK> serviceAPKMap) {
        this.activity = activity;
        this.listener = listener;
        this.serviceAPKMap = serviceAPKMap;
        this.msgCode = 0;
        File dir = AielloFileUtils.getCacheDirectory(activity);
        boolean success = AielloFileUtils.deleteDirContent(dir);
        Log.d(TAG, "clear cache dir " + success);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        listener.onOtaStart();
    }


    @Override
    protected Void doInBackground(Void... voids) {
        try {

            //------------------------get online latest apk (service) and initial apk obj------------------------

            //api v1
            //String res = Http.get(String.format(APIConfig.WEBAPI_OTA.getUrl(), DeviceInfo.hotelID), 5000, APIConfig.WEBAPI_OTA.getKey());
            //api v2
            String res = Http.get(String.format(APIConfig.WEBAPI_OTA.getUrl(), DeviceInfo.hotelID, SystemConfig.OTA.apk_push_name, SystemConfig.OTA.apk_cloud, DeviceInfo.majorVersion), 5000, APIConfig.WEBAPI_OTA.getKey()).response;
            JSONObject jsonObject = new JSONObject(res);
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject tmp = jsonArray.getJSONObject(i);
                String cname = tmp.getString("cname");
                String packageName = tmp.getString("packageName");
                String release_version = tmp.getString("release_version");
                String test_version = tmp.getString("test_version");
                String release_version_url = tmp.getString("release_version_url");
                String test_version_url = tmp.getString("test_version_url");
                Log.d(TAG, "latest_apk=" + tmp);
                VersionCheckerT.serviceApkMap.put(packageName, new ServiceAPK(cname, release_version, release_version_url, test_version, test_version_url, packageName));
            }

            //------------------------check latest apk (service)------------------------

            int i = 0;
            msgCode = 0;
            otaMsg = "";
            for (Map.Entry<String, ServiceAPK> entry : serviceAPKMap.entrySet()) {
                ServiceAPK serviceAPK = entry.getValue();
                boolean latest = VersionCheckerT.isLatestVersion(activity, serviceAPK);
                if (!ChatApplication.forceUpdate) {
                    latest = true;
                }
                int unit = 100 / serviceAPKMap.size();
                int min = i * unit;
                int max = (i + 1) * unit;
                if (!latest) {
                    //remove old apk
                    ApkInstallerT.uninstallApk(activity, serviceAPK.getPackageName());
                    download(activity, serviceAPK.getDownloadUrl(), new OnDownLoadListener() {
                        @Override
                        public void onProgress(int progress) {
                            listener.onDownloadingProgress(serviceAPK.getName(), progress);
                            listener.onOtaProgress(min + unit * progress / 110);
                        }

                        @Override
                        public void onSuccess(File apkFile) {
                            //install apk
                            ApkInstallerT.installAPkSilently(activity, apkFile, serviceAPK);
                            if (!serviceAPK.getPackageName().equals(activity.getPackageName())) {
                                //other service
                                int wait_installed_counter = 0;
                                ServiceInstallReceiver.check = 1;
                                while (wait_installed_counter < 100000000 && ServiceInstallReceiver.check == 1) {
                                    Log.d(TAG, serviceAPK.getName() + " is being installing");
                                    wait_installed_counter++;
                                    listener.onInstalling(serviceAPK.getName());
                                }
                                Log.d(TAG, serviceAPK.getName() + " is be installed");
                                listener.onInstalled(serviceAPK.getName());
                            } else {
                                //spot apk, wait for auto restart
                                msgCode = 2;
                                otaMsg = "wait for spot apk auto restart";
                                Log.d(TAG, otaMsg);
                                listener.onInstalling(serviceAPK.getName());
                            }
                        }

                        @Override
                        public void onFail() {
                            msgCode = 1;
                            otaMsg = serviceAPK.getName() + " download fail. ";
                        }
                    });
                }
                //break loop when get wrong msgCode
                if (msgCode == 1 || msgCode == 2) {
                    break;
                }
                Log.e(TAG, serviceAPK.getName() + " check ota finished, keep going, progress = " + max);
                listener.onOtaProgress(max);
                i++;
            }
        } catch (Exception e) {
            msgCode = 3;
            otaMsg = e.toString();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void voids) {
        Log.e(TAG, "onPostExecute, otaMsg = " + otaMsg);
        switch (msgCode) {
            case 0:
                listener.onOtaCompleted();//完成所有程式更新
                break;

            case 1:
                listener.onDownloadingFail(otaMsg);
                break;

            case 2:
                //nothing, wait for spot auto install
                break;

            case 3:
                listener.onDownloadingFail("ota error");
                break;
        }
    }

    interface OnDownLoadListener {
        void onProgress(int progress);

        void onSuccess(File apkFile);

        void onFail();
    }

    private void download(Activity activity, String urlStr, OnDownLoadListener listener) {
        InputStream in = null;
        FileOutputStream out = null;
        try {
            URL url = new URL(urlStr);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(false);
            urlConnection.setConnectTimeout(3 * 1000);
            urlConnection.setReadTimeout(3 * 1000);
            urlConnection.setRequestProperty("Connection", "Keep-Alive");
            urlConnection.setRequestProperty("Charset", "UTF-8");
            urlConnection.setRequestProperty("Accept-Encoding", "gzip, deflate");
//            urlConnection.setRequestProperty(Configs.API_SUB_KEY_LABEL, Configs.SPOT_UPDATE_PRODUCTION_URL.getKey());

            urlConnection.connect();
            long bytetotal = urlConnection.getContentLength();
            long bytesum = 0;
            int byteread = 0;
            in = urlConnection.getInputStream();

            File dir = AielloFileUtils.getCacheDirectory(activity);
            dir.deleteOnExit();
            String filePath = dir.toString();

            String apkName = urlStr.substring(urlStr.lastIndexOf("/") + 1, urlStr.length());
            File apkFile = new File(dir, apkName);
            out = new FileOutputStream(apkFile);
            byte[] buffer = new byte[10 * 1024];// 8k ~ 32K
            int progress = 0;
            while ((byteread = in.read(buffer)) != -1) {
                bytesum += byteread;
                out.write(buffer, 0, byteread);

                progress = (int) (bytesum * 100L / bytetotal);
                Log.d(TAG, "download progress = " + progress);
                listener.onProgress(progress);
            }
            if (bytesum == bytetotal) {
                listener.onSuccess(apkFile);
            } else {
                listener.onFail();
            }
        } catch (Exception e) {
            Log.e(TAG, "download apk file error:" + e.getMessage());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            listener.onFail();
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ignored) {

                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ignored) {

                }
            }
        }
    }

}
