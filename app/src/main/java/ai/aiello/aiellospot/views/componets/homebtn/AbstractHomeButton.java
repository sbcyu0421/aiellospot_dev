package ai.aiello.aiellospot.views.componets.homebtn;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.nex3z.notificationbadge.NotificationBadge;

public abstract class AbstractHomeButton extends ConstraintLayout {

    public ImageView imageView;
    public TextView textView;
    public NotificationBadge badge;
    public ImageView asrStateImage;
    public View btn;

    public AbstractHomeButton(Context context) {
        super(context);
    }

    public void initParams(View btn) {}

    public abstract void updateImage(int res);

    public abstract void updateText(int res);

    public NotificationBadge getBadge() {
        return badge;
    }

    public ImageView getAsrStateImage() {
        return asrStateImage;
    }
}
