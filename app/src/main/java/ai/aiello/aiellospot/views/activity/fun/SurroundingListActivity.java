package ai.aiello.aiellospot.views.activity.fun;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.johnpersano.supertoasts.library.Style;

import java.util.ArrayList;
import java.util.Locale;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.BaseActivity;
import ai.aiello.aiellospot.modules.surrounding.Items;
import ai.aiello.aiellospot.modules.surrounding.SurroundingModuleManager;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.componets.toast.MToaster;
import ai.aiello.chinese.ChineseUtils;

public class SurroundingListActivity extends BaseActivity {

    private Items items;
    private String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            items = SurroundingModuleManager.getInstance().getCurrentItems();
            type = items.getType();
            initView(items);
        } catch (Exception e) {
            MToaster.showButtonToast(this, getString(R.string.surrounding_format_error), Style.TYPE_STANDARD);
            ActivityLauncher.launchActivityByClass(this, ActivityLauncher.currentHomeName, R.anim.fade_in, R.anim.fade_out);
            SurroundingModuleManager.getInstance().create();
            e.printStackTrace();
        }
    }

    private void initView(Items items) {
        //data prepare
        setContentView(R.layout.surrounding_list);

        //set recycle view
        RecyclerView rv_list = findViewById(R.id.rv_list);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_list.setLayoutManager(layoutManager);
        CuisineRecyclerViewAdapter adapter =
                new CuisineRecyclerViewAdapter(this, items, new MCuisineAdapterListener() {
                    @Override
                    public void onClick(int itemsBeanIndex) {
                        Log.d(TAG, "onClick : " + items.getItems().get(itemsBeanIndex).getDetail().getName());
                        showSingleView(itemsBeanIndex);
                    }

                    @Override
                    public void onLoadFail(String msg) {
                        Log.d(TAG, "onLoadFail : " + msg);
                    }
                }, ChatApplication.locale);
        rv_list.setAdapter(adapter);

        LinearLayout ly_back = findViewById(R.id.ly_back);
        ly_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exit(1);
                return;
            }
        });

        TextView surrounding_title = findViewById(R.id.surrounding_title);
        switch (type) {
            case SurroundingModuleManager.CUISINE:
                surrounding_title.setText(R.string.ui_cuisine_service);
                break;

            case SurroundingModuleManager.SIGHTS:
                surrounding_title.setText(R.string.ui_sights_service);
                break;

            case SurroundingModuleManager.TRANSPORT:
                surrounding_title.setText(R.string.ui_transport_service);
                break;

            case SurroundingModuleManager.BUILDING:
                surrounding_title.setText(R.string.ui_building_service);
                break;

        }

        //scroll to focus position
        try {
            rv_list.scrollToPosition(SurroundingModuleManager.getInstance().getCurrentBeanIndex());
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private MCuisineAdapterListener mCuisineAdapterListener = null;

    public interface MCuisineAdapterListener {
        void onClick(int itemsBeanIndex);

        void onLoadFail(String msg);
    }

    private void exit(int index) {
        if (index == 0) {
            Intent intent = new Intent(SurroundingListActivity.this, ActivityLauncher.currentHomeName);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        } else {
            Intent intent = new Intent(SurroundingListActivity.this, SurroundingActivity.class);
            SurroundingModuleManager.getInstance().setCurrentType(items.getType());
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        }
    }

    private void showSingleView(int itemsBeanIndex) {
        Intent intent = new Intent(SurroundingListActivity.this, SurroundingDetailActivity.class);
        SurroundingModuleManager.getInstance().setCurrentItems(items);
        SurroundingModuleManager.getInstance().setCurrentBeanIndex(itemsBeanIndex);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }

    public static class CuisineRecyclerViewAdapter extends RecyclerView.Adapter<CuisineRecyclerViewAdapter.ViewHolder> {

        private final String TAG = "CuisineRecyclerViewAdapter";
        Items items;
        Context mContext;
        Locale locale;

        private MCuisineAdapterListener mCuisineAdapterListener = null;

        public CuisineRecyclerViewAdapter(Context context, Items items, MCuisineAdapterListener onClickListener, Locale locale) {
            this.mCuisineAdapterListener = onClickListener;
            this.items = items;
            this.mContext = context;
            this.locale = locale;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.surrounding_item_card_, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            String imgurl = "";
            try {
                imgurl = items.getItems().get(position).getDetail().getImages().get(0);
            } catch (Exception e) {
                imgurl = "https://bnextmedia.s3.hicloud.net.tw/image/album/2019-05/img-1557992946-48852@900.jpg";
            }

            Glide.with(holder.cuisine_card_img.getContext())
                    .load(imgurl)
                    .thumbnail(0.5f)
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.cuisine_card_img);

            //set recommend icon
            if (items.getItems().get(position).getDetail().getRecommand() > 0) {
                holder.recommend_text.setText(items.getItems().get(position).getDetail().getRecommand() + "");
                holder.recommend_tag.setVisibility(View.VISIBLE);
            } else {
                holder.recommend_tag.setVisibility(View.GONE);
            }

            if (locale.equals(Locale.CHINA)) {
                String words = ChineseUtils.toSimplified(items.getItems().get(position).getDetail().getName(), false);
                holder.cuisine_card_title.setText(words);
            } else if (locale.equals(Locale.TAIWAN)) {
                holder.cuisine_card_title.setText(items.getItems().get(position).getDetail().getName());
            } else {
                holder.cuisine_card_title.setText(items.getItems().get(position).getDetail().getName_en());
            }

            if (items.getItems().get(position).getDetail().getDistance() != 0f) {
                holder.cuisine_card_distance.setText(String.format(mContext.getString(R.string.ui_distance), items.getItems().get(position).getDetail().getDistance() + ""));
                holder.cuisine_card_distance.setVisibility(View.VISIBLE);
            } else {
                holder.cuisine_card_distance.setVisibility(View.GONE);
            }
            String type = items.getType();
            Log.d(TAG, "type = " + type);


            try {
                ArrayList<String> tag = new ArrayList<>();
                for (String category : items.getItems().get(position).getCategory()) {
                    String words = "";
                    try {
                        switch (type) {
                            case SurroundingModuleManager.CUISINE:
                                words = SurroundingModuleManager.getInstance().getCuisineBean().getTitleSet().get(category).getName(ChatApplication.locale);
                                break;

                            case SurroundingModuleManager.SIGHTS:
                                words = SurroundingModuleManager.getInstance().getSightsBean().getTitleSet().get(category).getName(ChatApplication.locale);
                                break;

                            case SurroundingModuleManager.TRANSPORT:
                                words = SurroundingModuleManager.getInstance().getTransportBean().getTitleSet().get(category).getName(ChatApplication.locale);
                                break;

                            case SurroundingModuleManager.BUILDING:
                                words = SurroundingModuleManager.getInstance().getLifeBean().getTitleSet().get(category).getName(ChatApplication.locale);
                                break;
                        }
                    } catch (Exception e) {
                        //                    e.printStackTrace();
                    }
                    if (words.isEmpty()) words = category;
                    if (tag.size() < 3) {
                        tag.add(words);
                    } else {
                        break;
                    }
                }

                holder.cuisine_card_tag_1.setText("");
                holder.cuisine_card_tag_2.setText("");
                holder.cuisine_card_tag_3.setText("");
                holder.cuisine_card_tag_1.setVisibility(View.GONE);
                holder.cuisine_card_tag_2.setVisibility(View.GONE);
                holder.cuisine_card_tag_3.setVisibility(View.GONE);
                switch (tag.size()){
                    case 1:
                        holder.cuisine_card_tag_1.setText(tag.get(0));
                        holder.cuisine_card_tag_1.setVisibility(View.VISIBLE);
                        break;

                    case 2:
                        holder.cuisine_card_tag_1.setText(tag.get(0));
                        holder.cuisine_card_tag_1.setVisibility(View.VISIBLE);
                        holder.cuisine_card_tag_2.setText(tag.get(1));
                        holder.cuisine_card_tag_2.setVisibility(View.VISIBLE);
                        break;

                    case 3:
                        holder.cuisine_card_tag_1.setText(tag.get(0));
                        holder.cuisine_card_tag_1.setVisibility(View.VISIBLE);
                        holder.cuisine_card_tag_2.setText(tag.get(1));
                        holder.cuisine_card_tag_2.setVisibility(View.VISIBLE);
                        holder.cuisine_card_tag_3.setText(tag.get(2));
                        holder.cuisine_card_tag_3.setVisibility(View.VISIBLE);
                        break;
                }




            } catch (Exception e) {
                holder.cuisine_card_tag_1.setText("");
                holder.cuisine_card_tag_2.setText("");
                holder.cuisine_card_tag_3.setText("");
                holder.cuisine_card_tag_1.setVisibility(View.GONE);
                holder.cuisine_card_tag_2.setVisibility(View.GONE);
                holder.cuisine_card_tag_3.setVisibility(View.GONE);
            }

            holder.cuisine_card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int s = holder.getAdapterPosition();
                    mCuisineAdapterListener.onClick(s);
                }
            });

        }


        @Override
        public int getItemCount() {
            return items.getItems().size();
        }

        static class ViewHolder extends RecyclerView.ViewHolder {

            CardView cuisine_card;
            ImageView cuisine_card_img;
            TextView cuisine_card_title;
            TextView cuisine_card_tag_1, cuisine_card_tag_2, cuisine_card_tag_3;
            TextView cuisine_card_distance;

            RelativeLayout recommend_tag;
            TextView recommend_text;


            ViewHolder(View itemView) {
                super(itemView);
                cuisine_card = itemView.findViewById(R.id.cuisine_card);
                cuisine_card_img = itemView.findViewById(R.id.cuisine_card_img);
                recommend_tag = itemView.findViewById(R.id.recommend_tag);
                recommend_text = itemView.findViewById(R.id.recommend_text);
                cuisine_card_title = itemView.findViewById(R.id.cuisine_card_title);
                cuisine_card_tag_1 = itemView.findViewById(R.id.cuisine_card_tag_1);
                cuisine_card_tag_2 = itemView.findViewById(R.id.cuisine_card_tag_2);
                cuisine_card_tag_3 = itemView.findViewById(R.id.cuisine_card_tag_3);
                cuisine_card_distance = itemView.findViewById(R.id.cuisine_card_distance);

            }
        }

        @Override
        public void onViewRecycled(@NonNull ViewHolder holder) {
            super.onViewRecycled(holder);
            Glide.get(holder.cuisine_card_img.getContext()).clearMemory();
        }

    }

}