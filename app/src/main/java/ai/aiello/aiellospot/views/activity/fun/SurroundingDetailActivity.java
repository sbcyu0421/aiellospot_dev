package ai.aiello.aiellospot.views.activity.fun;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.JsonObject;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.activity.BaseActivity;
import ai.aiello.aiellospot.modules.surrounding.Items;
import ai.aiello.aiellospot.modules.surrounding.SurroundingModuleManager;
import ai.aiello.chinese.ChineseUtils;

public class SurroundingDetailActivity extends BaseActivity {

    private static final String TAG = SurroundingDetailActivity.class.getSimpleName();

    private ImageView itemDataQrcode;
    private RelativeLayout recommend_tag;
    private TextView recommend_text;
    private LinearLayout side_imgs;
    private ArrayList<ImageView> imgList = new ArrayList<>();

    private int index;
    private Items items;
    private Items.ItemsBean itemsBean;
    private static final String splitMark = "^@^";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        items = SurroundingModuleManager.getInstance().getCurrentItems();
        index = SurroundingModuleManager.getInstance().getCurrentBeanIndex();
        itemsBean = items.getItems().get(index);
        setContent();

        SpotUserTraceLog3.getInstance().buildUIEventLog(
                SpotUserTraceLog3.EventSubject.SURROUNDING,
                SpotUserTraceLog3.EventAction.SELECT,
                items.getType() + splitMark + itemsBean.getDetail().getName()
        );
    }

    private void exit(int index) {
        if (index == 0) {
            Intent intent = new Intent(SurroundingDetailActivity.this, ActivityLauncher.currentHomeName);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        } else if (index == 1) {
            Intent intent = new Intent(SurroundingDetailActivity.this, SurroundingActivity.class);
            SurroundingModuleManager.getInstance().setCurrentType(items.getType());
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        } else {
            Intent intent = new Intent(SurroundingDetailActivity.this, SurroundingListActivity.class);
            SurroundingModuleManager.getInstance().setCurrentType(items.getType());
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        }
    }

    private void setContent() {
        setContentView(R.layout.surrounding_detail);
        TextView cuisine_item_title = findViewById(R.id.cuisine_item_title);
        TextView cuisine_item_tag_1 = findViewById(R.id.cuisine_item_tag_1);
        TextView cuisine_item_tag_2 = findViewById(R.id.cuisine_item_tag_2);
        TextView cuisine_item_tag_3 = findViewById(R.id.cuisine_item_tag_3);
        itemDataQrcode = findViewById(R.id.itemDataQrcode);
        recommend_tag = findViewById(R.id.recommend_tag);
        recommend_text = findViewById(R.id.recommend_text);
        side_imgs = findViewById(R.id.side_imgs);
        LinearLayout googleOpentiemView = findViewById(R.id.ll_google_open_time);
        TextView superTasteOpentiemView = findViewById(R.id.tv_super_taste_open_time);

        cuisine_item_title.setText(itemsBean.getDetail().getName_en());
        if (ChatApplication.locale.equals(Locale.TAIWAN)) {
            cuisine_item_title.setText(itemsBean.getDetail().getName());
        } else if (ChatApplication.locale.equals(Locale.CHINA)) {
            String words = ChineseUtils.toSimplified(itemsBean.getDetail().getName(), false);
            cuisine_item_title.setText(words);
        }

        TextView surrounding_title = findViewById(R.id.surrounding_title);
        switch (items.getType()) {
            case SurroundingModuleManager.CUISINE:
                surrounding_title.setText(R.string.ui_cuisine_service);
                break;

            case SurroundingModuleManager.SIGHTS:
                surrounding_title.setText(R.string.ui_sights_service);
                break;

            case SurroundingModuleManager.TRANSPORT:
                surrounding_title.setText(R.string.ui_transport_service);
                break;

            case SurroundingModuleManager.BUILDING:
                surrounding_title.setText(R.string.ui_building_service);
                break;

        }

        //parse tags
        try {
            for (String category : itemsBean.getCategory()) {
                try {
                    cuisine_item_title.setText(itemsBean.getDetail().getName_en());
                    if (ChatApplication.locale.equals(Locale.TAIWAN)) {
                        cuisine_item_title.setText(itemsBean.getDetail().getName());
                    } else if (ChatApplication.locale.equals(Locale.CHINA)) {
                        String words = ChineseUtils.toSimplified(itemsBean.getDetail().getName(), false);
                        cuisine_item_title.setText(words);
                    }
                    String words = "";
                    try {
                        switch (items.getType()) {
                            case SurroundingModuleManager.CUISINE:
                                words = SurroundingModuleManager.getInstance().getCuisineBean().getTitleSet().get(category).getName(ChatApplication.locale);
                                break;

                            case SurroundingModuleManager.SIGHTS:
                                words = SurroundingModuleManager.getInstance().getSightsBean().getTitleSet().get(category).getName(ChatApplication.locale);
                                break;
                            case SurroundingModuleManager.TRANSPORT:
                                words = SurroundingModuleManager.getInstance().getTransportBean().getTitleSet().get(category).getName(ChatApplication.locale);
                                break;

                            case SurroundingModuleManager.BUILDING:
                                words = SurroundingModuleManager.getInstance().getLifeBean().getTitleSet().get(category).getName(ChatApplication.locale);
                                break;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (words.isEmpty()) words = category;
                    //                if (words.length() < 8) {
                    if (cuisine_item_tag_1.getText().toString().isEmpty()) {
                        cuisine_item_tag_1.setText(words);
                        cuisine_item_tag_1.setVisibility(View.VISIBLE);
                    } else if (cuisine_item_tag_2.getText().toString().isEmpty()) {
                        cuisine_item_tag_2.setText(words);
                        cuisine_item_tag_2.setVisibility(View.VISIBLE);
                    } else if (cuisine_item_tag_3.getText().toString().isEmpty()) {
                        cuisine_item_tag_3.setText(words);
                        cuisine_item_tag_3.setVisibility(View.VISIBLE);
                    }
    //                }


                } catch (Exception e) {
    //                e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }



        //parse openingTime
        String openingHoursType = "";

        try {
            openingHoursType = itemsBean.getDetail().getOpeningHoursType();
        } catch (Exception e) {
            Log.d(TAG, "openingHoursType not provided (this might be a dummy item)");
        }

        if (Objects.nonNull(openingHoursType) && openingHoursType.equals("PeriodTimes")){
            try {
                String openingHours = itemsBean.getDetail().getOpeningHours().toString();
                JSONObject jsonOpeningHours = new JSONObject(openingHours);
//                Items.ItemsBean.DetailBean.GoogleOpeningTimeBean googleOpeningHours = itemsBean.getDetail().getGoogleOpeningHours();
                openingDayMap.get(Mon).setTime(jArrayToList(jsonOpeningHours.getJSONArray(Mon)));
                openingDayMap.get(Tue).setTime(jArrayToList(jsonOpeningHours.getJSONArray(Tue)));
                openingDayMap.get(Wed).setTime(jArrayToList(jsonOpeningHours.getJSONArray(Wed)));
                openingDayMap.get(Thu).setTime(jArrayToList(jsonOpeningHours.getJSONArray(Thu)));
                openingDayMap.get(Fri).setTime(jArrayToList(jsonOpeningHours.getJSONArray(Fri)));
                openingDayMap.get(Sat).setTime(jArrayToList(jsonOpeningHours.getJSONArray(Sat)));
                openingDayMap.get(Sun).setTime(jArrayToList(jsonOpeningHours.getJSONArray(Sun)));
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                //show result

                TextView mon_day_period = findViewById(R.id.mon_day_period);
                mon_day_period.setText(format(openingDayMap.get(Mon).getDayPeriods()));

                TextView tue_day_period = findViewById(R.id.tue_day_period);
                tue_day_period.setText(format(openingDayMap.get(Tue).getDayPeriods()));

                TextView wed_day_period = findViewById(R.id.wed_day_period);
                wed_day_period.setText(format(openingDayMap.get(Wed).getDayPeriods()));

                TextView thu_day_period = findViewById(R.id.thu_day_period);
                thu_day_period.setText(format(openingDayMap.get(Thu).getDayPeriods()));

                TextView fri_day_period = findViewById(R.id.fri_day_period);
                fri_day_period.setText(format(openingDayMap.get(Fri).getDayPeriods()));

                TextView sat_day_period = findViewById(R.id.sat_day_period);
                sat_day_period.setText(format(openingDayMap.get(Sat).getDayPeriods()));

                TextView sun_day_period = findViewById(R.id.sun_day_period);
                sun_day_period.setText(format(openingDayMap.get(Sun).getDayPeriods()));

            }
            googleOpentiemView.setVisibility(View.VISIBLE);
            superTasteOpentiemView.setVisibility(View.GONE);
        } else {
            // show opening time from SUPER_TASTE
            try {
                superTasteOpentiemView.setText((String) itemsBean.getDetail().getOpeningHours());
            } catch (Exception e) {
                superTasteOpentiemView.setText("");
                e.printStackTrace();
            }

            googleOpentiemView.setVisibility(View.GONE);
            superTasteOpentiemView.setVisibility(View.VISIBLE);
        }

        //parse qrcode
        try {
            String url = itemsBean.getDetail().getQrcode();
            Hashtable hints = new Hashtable();
            hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
            try {
                BitMatrix bitMatrix = multiFormatWriter.encode(url, BarcodeFormat.QR_CODE, 500, 500, hints);
                BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
                itemDataQrcode.setImageBitmap(bitmap);
            } catch (WriterException e) {
                Log.e(TAG, e.toString());
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        //set recommend icon
        if (itemsBean.getDetail().getRecommand() > 0) {
            recommend_text.setText(itemsBean.getDetail().getRecommand() + "");
            recommend_tag.setVisibility(View.VISIBLE);
        } else {
            recommend_tag.setVisibility(View.GONE);
        }

        //parse images
        for (String url : itemsBean.getDetail().getImages()) {
            try {
                ImageView imageView = new ImageView(this);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                LinearLayout.LayoutParams params =
                        new LinearLayout.LayoutParams(520, 520);
                params.bottomMargin = 10;
                imageView.setLayoutParams(params);
                Glide.with(this)
                        .load(url)
                        .thumbnail(0.5f)
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
                imgList.add(imageView);
                side_imgs.addView(imageView);
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }
        }


        LinearLayout ly_back = findViewById(R.id.ly_back);
        ly_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (items.getItems().size() == 1) {
                    exit(1);
                } else {
                    exit(2);
                }
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (imgList.size() > 1) {
            LinearLayout side_imgs = findViewById(R.id.side_imgs);
            Animation scrollAnim = new TranslateAnimation(0.0f, 0.0f, -2000.0f, 0.0f);
            scrollAnim.setDuration(1200);
            scrollAnim.setRepeatCount(0);
            scrollAnim.setFillAfter(true);
            side_imgs.startAnimation(scrollAnim);
//            side_img_scroll.post(new Runnable() {
//                @Override
//                public void run() {
//                    side_img_scroll.smoothScrollTo(0, 5000);
//                }
//            });
        }
    }


    private static final String Mon = "Mon";
    private static final String Tue = "Tue";
    private static final String Wed = "Wed";
    private static final String Thu = "Thu";
    private static final String Fri = "Fri";
    private static final String Sat = "Sat";
    private static final String Sun = "Sun";

    private Map<String, Items.OpeningDay> openingDayMap = new HashMap<String, Items.OpeningDay>() {
        {
            put(Mon, new Items.OpeningDay());
            put(Tue, new Items.OpeningDay());
            put(Wed, new Items.OpeningDay());
            put(Thu, new Items.OpeningDay());
            put(Fri, new Items.OpeningDay());
            put(Sat, new Items.OpeningDay());
            put(Sun, new Items.OpeningDay());
        }
    };


    @Override
    protected void onDestroy() {
        Glide.get(itemDataQrcode.getContext()).clearMemory();
        side_imgs.removeAllViews();
        for (ImageView view : imgList) {
            Glide.get(view.getContext()).clearMemory();
        }
        super.onDestroy();
    }


    private String format(ArrayList<Items.Period> dayPeriods) {
        if (dayPeriods.size() == 0) {
            return " - ";
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < dayPeriods.size(); i++) {
            try {
                SimpleDateFormat ISO8601DATEFORMAT = new SimpleDateFormat("HH:mm");
                String opening_start = ISO8601DATEFORMAT.format(dayPeriods.get(i).getStart());
                String opening_end = ISO8601DATEFORMAT.format(dayPeriods.get(i).getEnd());
                sb.append(opening_start + " - " + opening_end);
                if (i == dayPeriods.size() - 1) {
                    break;
                }
                sb.append("\n");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    private ArrayList<String> jArrayToList(JSONArray jArray) {
        ArrayList<String> li = new ArrayList<String>();
        for (int i = 0; i < jArray.length(); i++) {
            try {
                li.add(jArray.getString(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return li;
    }

}
