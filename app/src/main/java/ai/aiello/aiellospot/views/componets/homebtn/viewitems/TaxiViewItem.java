package ai.aiello.aiellospot.views.componets.homebtn.viewitems;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import java.lang.ref.WeakReference;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.activity.taxi.TaxiActivity;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractHomeButton;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.HomeBasicButton;

public class TaxiViewItem extends AbstractViewItem {

    public static Thread taxiThread;
    UIHandler uiHandler;

    private static TaxiViewItem instance;

    public static TaxiViewItem getInstance() {
        if (instance == null) {
            instance = new TaxiViewItem();
        }
        return instance;
    }

    private TaxiViewItem() {}

    @Override
    public void updateView() {

    }

    @Override
    public void handelBtnClickLogic() {
        wefBtn.get().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityLauncher.launchActivityByClass(wefActivity.get(), TaxiActivity.class, R.anim.fade_in, R.anim.fade_out);
                wefActivity.get().finish();
            }
        });
    }

    @Override
    public void unbindView(Activity activity) {
        super.unbindView(activity);
        if (taxiThread != null) {
            taxiThread.interrupt();
            taxiThread = null;
        }
        if (uiHandler != null) {
            uiHandler.removeCallbacksAndMessages(null);
            uiHandler = null;
        }
    }

    @Override
    public View buildButton(Context context) {
        HomeBasicButton homeBasicButton = new HomeBasicButton(context, this.width, this.height);
        homeBasicButton.updateImage(R.drawable.ic_taxi);
        homeBasicButton.updateText(R.string.taxi_intent);
        return homeBasicButton;
    }

    static class UIHandler extends Handler {

        Activity activity;

        UIHandler(WeakReference<Activity> reference) {
            activity = reference.get();
        }

        @Override
        public void handleMessage(Message msg) {
        }
    }
}
