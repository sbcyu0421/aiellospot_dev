package ai.aiello.aiellospot.views.activity.iot;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.core.chatbot.ChatBot;
import ai.aiello.aiellospot.intents.Classifier;
import ai.aiello.aiellospot.intents.IntentManager;
import ai.aiello.aiellospot.intents.IntentObject;
import ai.aiello.aiellospot.intents.sdui.ActionManager;
import ai.aiello.aiellospot.intents.sdui.ServerDrivenHelper;
import ai.aiello.aiellospot.core.config.SystemConfig;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.modules.iot.IoTModuleManager;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

public class UiDeviceSwitch extends UiDevice {


    public UiDeviceSwitch(Context context, JSONObject obj, int iconRes) throws Exception {
        super(context, obj, iconRes);
    }

    public UiDeviceSwitch(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public UiDeviceSwitch(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public UiDeviceSwitch(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private boolean initialized = false;

    @Override
    void doAction() {
        ChatApplication.worker.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    JSONObject jsonObject = ChatBot.requestChatbot(0, getIot_name().getText().toString(), "", "");
                    IntentObject io = Classifier.classify(ChatBot.parse(jsonObject));
                    if (SystemConfig.OTA.support_action) {
                        ServerDrivenHelper.ServerMessage serverMessage = ServerDrivenHelper.parse(io.getAgentActions());
                        ActionManager.getInstance().prepare(serverMessage.getResultAction(), true);
                    } else {
                        IoTModuleManager.getInstance().push_iot_intent_to_remoteService(io.getChatbot_data(), io.getIntent(), null);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
//                try {
//                    SpotUserTraceLog3.getInstance().buildUIEventLog(
//                            SpotUserTraceLog3.EventSubject.IOT,
//                            (cmdData.contains("off")) ? SpotUserTraceLog3.EventAction.OFF : SpotUserTraceLog3.EventAction.ON,
//                            new JSONObject(cmdData).getJSONArray("loop_codes").getJSONObject(0).getString("code")
//                    );
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
            }
        });

    }

    @Override
    void stopAction() {

    }
}
