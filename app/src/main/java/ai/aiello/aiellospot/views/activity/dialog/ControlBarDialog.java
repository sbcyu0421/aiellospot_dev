package ai.aiello.aiellospot.views.activity.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.CountDownTimer;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.litesuits.android.log.Log;
import com.willy.ratingbar.BaseRatingBar;
import com.willy.ratingbar.ScaleRatingBar;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.MyActivity;
import ai.aiello.aiellospot.core.MCountdownTimer;
import ai.aiello.aiellospot.core.device.DeviceControl;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;

/**
 * Created by a1990 on 2019/4/9.
 */


public class ControlBarDialog extends Dialog implements BaseRatingBar.OnRatingChangeListener {

    private String TAG = ControlBarDialog.class.getSimpleName();
    private LinearLayout lyControlBar;
    private Animation amTranslate;
    private ScaleRatingBar rb_volume_music;
    private TextView tv_volume_music;
    private ImageView img_volume_music;
    private static CountDownTimer countDownTimer;
    private ScaleRatingBar rb_bright;
    private TextView tv_bright;


    public ControlBarDialog(Context context, int theme) {
        super(context, theme);
        initView();
    }

    private void initView() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_setting_vb);
        tv_volume_music = findViewById(R.id.tv_volume_music);
        tv_bright = findViewById(R.id.tv_bright);
        lyControlBar = findViewById(R.id.lyControlBar);
        img_volume_music = findViewById(R.id.img_volume_music);
        rb_volume_music = findViewById(R.id.rb_volume_music);
        rb_bright = findViewById(R.id.rb_bright);
        rb_volume_music.setOnRatingChangeListener(this);
        rb_bright.setOnRatingChangeListener(this);
        lyControlBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playCardOutAnimation();
            }
        });
    }


    public void show() {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN;
        super.show();
        this.getWindow().getDecorView().setSystemUiVisibility(uiOptions);
        this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        this.getWindow().setGravity(Gravity.CENTER);
        this.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT); //Controlling width and height.
        this.setCancelable(true);

        playCardInAnimate();
        update_music();
        update_bright();
        startEndingTimer();
        MCountdownTimer.stopTimer();

        this.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                Log.d(TAG, "input = " + keyCode);
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case MyActivity.KEYCODE_AIELLO_VOLUME_UP:
                            DeviceControl.upVolume();
                            update_music();
                            update_bright();
                            startEndingTimer();
                            SpotUserTraceLog3.getInstance().buildHardwareEventLog(
                                    SpotUserTraceLog3.EventSubject.VOLUME,
                                    SpotUserTraceLog3.EventAction.UP,
                                    ""
                            );
                            break;

                        case MyActivity.KEYCODE_AIELLO_VOLUME_DOWN:
                            DeviceControl.downVolume();
                            update_music();
                            update_bright();
                            startEndingTimer();
                            SpotUserTraceLog3.getInstance().buildHardwareEventLog(
                                    SpotUserTraceLog3.EventSubject.VOLUME,
                                    SpotUserTraceLog3.EventAction.DOWN,
                                    ""
                            );
                            break;
                    }
                }
                return true;
            }
        });

    }

    private void playCardInAnimate() {
        amTranslate = new TranslateAnimation(0.0f, 0.0f, -1000.0f, 0.0f);
        amTranslate.setDuration(375);
        amTranslate.setRepeatCount(0);
        lyControlBar.startAnimation(amTranslate);
    }

    private void playCardOutAnimation() {
        Animation dismissTranslate = new TranslateAnimation(0.0f, 0.0f, 0.0f, -1000.0f);
        dismissTranslate.setDuration(375);
        dismissTranslate.setRepeatCount(0);
        dismissTranslate.setFillAfter(true);

        lyControlBar.startAnimation(dismissTranslate);
        lyControlBar.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                MCountdownTimer.startTimer();
                dismiss();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }


    private void update_music() {
        int s = DeviceControl.getCurrentVolume();
        rb_volume_music.setRating(s);
        if (s == 0) { //mute
            img_volume_music.setImageResource(R.drawable.icon_volume_mute);
            tv_volume_music.setText(0 + "");
        } else {
            img_volume_music.setImageResource(R.drawable.icon_volume);
            tv_volume_music.setText(s + "");
        }

    }


    private void update_bright() {
        int b = DeviceControl.getBrightness() / 10;
        rb_bright.setRating(b);
        tv_bright.setText(String.valueOf(b));
    }


    private void startEndingTimer() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
        countDownTimer = new CountDownTimer(3 * 1000, 1 * 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                Log.d(TAG, "vb_Timer =" + millisUntilFinished / 1000);
            }

            @Override
            public void onFinish() {
                playCardOutAnimation();
            }
        }.start();
    }

    @Override
    public void cancel() {
        playCardOutAnimation();
    }

    @Override
    public void onRatingChange(BaseRatingBar ratingBar, float rating, boolean fromUser) {
        switch (ratingBar.getId()) {

            case R.id.rb_volume_music:
                int c = Math.round(rating);
                Log.d(TAG, "Volume c = " + c);
                DeviceControl.setVolume(c);
                update_music();
                break;

            case R.id.rb_bright:
                int set = Math.round(rating);
                if (set != 0) {
                    DeviceControl.setBrightness(set * 10);
                    update_bright();
                }
                break;
        }
        startEndingTimer();
    }

}

