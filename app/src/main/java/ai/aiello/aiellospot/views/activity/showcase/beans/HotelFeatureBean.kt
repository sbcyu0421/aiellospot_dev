package ai.aiello.aiellospot.views.activity.showcase.beans

import ai.aiello.aiellospot.ChatApplication
import ai.aiello.aiellospot.views.activity.showcase.beans.LocaleParser.Companion.parser

abstract class HotelFeatureBean(val feature_type: String, val unikey: String, val title: String) {

    abstract fun getData(): Any


    fun getTitle(locale: ChatApplication.Language): String {
        return parser(locale, title)
    }
}