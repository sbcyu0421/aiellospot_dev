package ai.aiello.aiellospot.views.activity.music;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.github.johnpersano.supertoasts.library.Style;
import com.google.zxing.BarcodeFormat;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.litesuits.android.log.Log;
import com.tomer.fadingtextview.FadingTextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.time.Instant;
import java.util.Base64;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.activity.BaseActivity;
import ai.aiello.aiellospot.views.componets.hints.WakeupHintManager;
import ai.aiello.aiellospot.views.activity.dialog.AielloAlertDialog;
import ai.aiello.aiellospot.views.activity.dialog.AielloProgressDialog;
import ai.aiello.aiellospot.views.activity.multi.MultiIntentActivity;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.core.log.MusicLogItem;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.events.module.MusicStateChange;
import ai.aiello.aiellospot.intents.IntentManager;
import ai.aiello.aiellospot.modules.music.MusicServiceModuleManager;
import ai.aiello.aiellospot.modules.music.TrackObject;
import ai.aiello.aiellospot.views.componets.toast.MToaster;


public class MusicPlayer3Activity extends BaseActivity implements View.OnClickListener, View.OnTouchListener {

    static String TAG = MusicPlayer3Activity.class.getSimpleName();

    private TextView buttonPlayPause;
    private SeekBar seekBarProgress;
    private ImageView img_album_url;
    private LinearLayout ly_back;

    private TextView tv_songName;
    private TextView tv_singer;
    private TextView tv_albumName;

    private TextView tv_playprevious;
    private TextView tv_playnext;
    private AielloProgressDialog aielloProgressDialog = null;
    private TrackObject trackInfo = null;
    private ImageView music_logo;
    private String qrcode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }

    private void initView() {
        setContentView(R.layout.activity_music_player);
        tv_songName = findViewById(R.id.tv_songName);
        tv_singer = findViewById(R.id.tv_singer);
        tv_albumName = findViewById(R.id.tv_albumName);
        ly_back = findViewById(R.id.ly_back);
        music_logo = findViewById(R.id.music_logo);
        buttonPlayPause = findViewById(R.id.img_playpause);
        tv_playprevious = findViewById(R.id.tv_playprevious);
        tv_playnext = findViewById(R.id.tv_playnext);
        seekBarProgress = findViewById(R.id.SeekBarTestPlay);
        img_album_url = findViewById(R.id.img_album_url);
    }


    @Override
    protected void onStart() {
        super.onStart();
        showHInts();
        switch (MusicServiceModuleManager.vendor) {
            case KKBOX:
                qrcode = MusicServiceModuleManager.getInstance().getKkboxQRcode();
                music_logo.setImageResource(R.drawable.kkbox_logo);
                break;
            case QQ:
                music_logo.setImageResource(R.drawable.qq_logo);
                break;
            default:
                music_logo.setImageResource(R.drawable.qq_logo);
        }
    }

    @Override
    public void onHotReload(){
        showHInts();
    }

    private void showHInts() {
        FadingTextView tv_slide_text = findViewById(R.id.sl_hint_player);
        String[] hints = new String[]{
                String.format(getResources().getString(R.string.hint_player1), DeviceInfo.assistantName),
                String.format(getResources().getString(R.string.hint_player2), DeviceInfo.assistantName)};
        WakeupHintManager.getInstance().showHints(tv_slide_text, hints);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!MusicServiceModuleManager.isPbrAuthorized) {
            showPbrDialog();
            return;
        }
        MusicServiceModuleManager.getInstance().registerAudioSync();
        seekBarProgress.setProgress(MusicServiceModuleManager.progress);
        showProgressDialog(2);
        switch (MusicServiceModuleManager.playerStatus) {

            case Offline:
                back();
                break;

            case Online:
                break;

            case onPause:
                trackInfo = MusicServiceModuleManager.getInstance().getCurrentTrackInfo();
                updateView(trackInfo);
                setListener();
                hideProgressDialog();
                break;

            default:
                trackInfo = MusicServiceModuleManager.getInstance().getCurrentTrackInfo();
                updateView(trackInfo);
                setListener();
        }
    }

    AielloAlertDialog pbrDialog = null;

    private void showPbrDialog() {
        BarcodeEncoder encoder = new BarcodeEncoder();
        try {
            // Show Qr code for redirecting accu
            JSONObject queryParamsJobj = new JSONObject();
            queryParamsJobj.put("hotel", DeviceInfo.hotelName);
            queryParamsJobj.put("device", DeviceInfo.uuid);
            queryParamsJobj.put("TS", Instant.now().getEpochSecond());

            String queryText = queryParamsJobj.toString();
            Base64.Encoder b64Encoder = Base64.getEncoder();
            byte[] textByte = queryText.getBytes();

            String encodedDauth = b64Encoder.encodeToString(textByte);

            String accuRedirectUrl = DeviceInfo.accuRedirectUrl + "&dauth=" + encodedDauth;
            android.util.Log.d(TAG, "getAccuDeviceInfo = " + accuRedirectUrl);


            Bitmap bitmap = encoder.encodeBitmap(accuRedirectUrl, BarcodeFormat.QR_CODE, 250, 250);
            pbrDialog = new AielloAlertDialog(this, R.style.Theme_AppCompat_Dialog_Alert);
            pbrDialog.setTitle(getString(R.string.pbr_dialog_title));
            pbrDialog.setTextContent(getString(R.string.pbr_dialog_content))
                    .setImageContent(bitmap)
                    .setOnConfirmListener(getString(R.string.pbr_dialog_cancel), v -> back());
            pbrDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void hidePbrDialog() {
        try {
            if (pbrDialog != null) {
                pbrDialog.dismiss();
                pbrDialog = null;
                android.util.Log.d(TAG, "pbrDialog destroyed");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setListener() {
        ly_back.setOnClickListener(this);
        buttonPlayPause.setOnClickListener(this);
        tv_playprevious.setOnClickListener(this);
        tv_playnext.setOnClickListener(this);
        seekBarProgress.setMax(99);
        seekBarProgress.setOnTouchListener(this);
    }


    private void updateView(TrackObject cur_track) {
        tv_singer.setText(cur_track.getArtistName());
        tv_albumName.setText(cur_track.getAlbumName());
        tv_albumName.setTextColor(getColor(R.color.dim_white));
        tv_songName.setText(cur_track.getTrackName());
        curImageState = ImageState.None;
        imgChangeTimer = 0;
        setAlbumImage();
        if (MusicServiceModuleManager.playerStatus != MusicServiceModuleManager.PlayerStatus.onPlay) {
            buttonPlayPause.setBackgroundResource(R.drawable.btn_play_ripple);
        } else {
            buttonPlayPause.setBackgroundResource(R.drawable.btn_pause_ripple);
        }
    }


    SpotUserTraceLog3.EventAction userLogEventAction = SpotUserTraceLog3.EventAction.NONE;

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.img_playpause:
                if (MusicServiceModuleManager.playerStatus != MusicServiceModuleManager.PlayerStatus.onPlay) {
                    userLogEventAction = SpotUserTraceLog3.EventAction.PLAY;
                } else {
                    userLogEventAction = SpotUserTraceLog3.EventAction.PAUSE;
                }
                playOrPause(0);
                break;

            case R.id.ly_back:
                back();
                break;

            case R.id.tv_playnext:
                MusicServiceModuleManager.getInstance().playNext();
                userLogEventAction = SpotUserTraceLog3.EventAction.NEXT;
                break;

            case R.id.tv_playprevious:
                MusicServiceModuleManager.getInstance().playPrev();
                userLogEventAction = SpotUserTraceLog3.EventAction.PREV;
                break;
        }

        MusicLogItem.buildMusicItem(
                SpotUserTraceLog3.EventSubject.MUSIC,
                userLogEventAction,
                "",
                "",
                "",
                SpotUserTraceLog3.EventInputType.UI
        );
    }

    private void back() {
        if (ActivityLauncher.isFromMultiActivity() && IntentManager.finishIntentList.size() != 0) {
            ActivityLauncher.launchActivityByClass(this, MultiIntentActivity.class, R.anim.fade_in, R.anim.fade_out);
        } else {
            if (MusicServiceModuleManager.playerStatus != MusicServiceModuleManager.PlayerStatus.onPlay) {//如果沒有播放，回menu
                Intent intent2 = new Intent(MusicPlayer3Activity.this, MusicMenuActivity.class);
                startActivity(intent2);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            } else {  //如果正在播放，回首頁
                Intent intent2 = new Intent(MusicPlayer3Activity.this, ActivityLauncher.currentHomeName);
                startActivity(intent2);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        }
    }

    private void playOrPause(int f) {
        if (MusicServiceModuleManager.playerStatus != MusicServiceModuleManager.PlayerStatus.onPlay) {
            MusicServiceModuleManager.getInstance().play();
        } else {
            MusicServiceModuleManager.getInstance().pause();
        }
    }



    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if (v.getId() == R.id.SeekBarTestPlay && event.getAction() == MotionEvent.ACTION_UP) {
            Log.e(TAG, "seekBarProgress.getProgress() = " + seekBarProgress.getProgress());
            Log.d(TAG, "trackInfo.getDuration() = " + trackInfo.getDuration());

            int seekToMilli = seekBarProgress.getProgress() * trackInfo.getDuration() / 100;
            Log.d(TAG, "seekToMilli = " + seekToMilli);
            MusicServiceModuleManager.getInstance().seekTo(seekToMilli);
        }

        return false;
    }


    //todo check the vol control
    @Override
    protected void onPause() {
        WakeupHintManager.getInstance().cancel();
        MusicServiceModuleManager.getInstance().unRegisterAudioSync();
        super.onPause();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MusicStateChange event) {
        Log.d(TAG, "UI_MusicEvent: " + event.getTarget());
        hideProgressDialog();
        switch (event.getTarget()) {
            case ON_TRACK_INFO_LOADED:
                trackInfo = MusicServiceModuleManager.getInstance().getCurrentTrackInfo();
                updateView(trackInfo);
                break;
            case ON_PLAY:
                setListener();
                buttonPlayPause.setBackgroundResource(R.drawable.btn_pause_ripple);
                break;
            case ON_PAUSE:
                updateView(trackInfo);
                break;
            case LOADING: //TARGET_MUSIC_REQUEST
                showProgressDialog(1);
                break;
            case ERROR:
                MToaster.showButtonToast(MusicPlayer3Activity.this, getString(R.string.sth_error), Style.TYPE_STANDARD);
                new Handler().postDelayed(()->{
                    ActivityLauncher.launchActivityByClass(this, ActivityLauncher.currentHomeName, R.anim.fade_in, R.anim.fade_out);
                }, 500);
                break;
            case ON_PROGRESS_BAR_UPDATE: //TARGET_MUSIC_SYNC
                Log.d(TAG, "PROGRESS_BAR, UPDATE PROGRESS= " + MusicServiceModuleManager.progress);
                seekBarProgress.setProgress(MusicServiceModuleManager.progress);
                imgChangeTimer++;
                //play qrcode or album
                if (MusicServiceModuleManager.vendor == MusicServiceModuleManager.Vendor.KKBOX && !MusicServiceModuleManager.getInstance().getKkboxQRcode().isEmpty()) {
                    playImageCarousel();
                }
                break;
            case ON_PBR_SUCCESS:
                if (pbrDialog != null) {
                    pbrDialog.setTextContent(getString(R.string.pbr_dialog_completed));
                }
                hidePbrDialog();
                break;
        }
    }


    enum ImageState {
        None, IMAGE, QRCODE
    }

    private long imgChangeTimer;

    public void playImageCarousel() {
        if (imgChangeTimer % 10 <= 4) {
            setAlbumImage();
        } else {
            setKKBOXImage();
        }
    }

    private ImageState curImageState = ImageState.None;

    public void setAlbumImage() {
        if (curImageState == ImageState.IMAGE) {
            return;
        }
        Glide.with(img_album_url.getContext())
                .load(MusicServiceModuleManager.getInstance().getCurrentTrackInfo().getTrackImages())
                .transition(new DrawableTransitionOptions().crossFade())
                .thumbnail(0.5f)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(img_album_url);

        tv_albumName.setText(MusicServiceModuleManager.getInstance().getCurrentTrackInfo().getAlbumName());
        tv_albumName.setTextColor(getColor(R.color.dim_white));
        curImageState = ImageState.IMAGE;
    }

    public void setKKBOXImage() {
        if (curImageState == ImageState.QRCODE) {
            return;
        }
        Glide.with(img_album_url.getContext())
                .load(qrcode)
                .transition(new DrawableTransitionOptions().crossFade())
                .thumbnail(0.5f)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(img_album_url);
        tv_albumName.setText(getText(R.string.kkbox_qrcode_hint));
        tv_albumName.setTextColor(getColor(R.color.kkbox));
        curImageState = ImageState.QRCODE;
    }


    public void showProgressDialog(int from) {
        android.util.Log.d(TAG, "showProgressDialog: from = " + from);
        if (aielloProgressDialog == null) {
            aielloProgressDialog = new AielloProgressDialog(new WeakReference<Activity>(this), R.style.CustomProgressDialog);
            aielloProgressDialog.setText(getResources().getString(R.string.please_wait));
        }
        aielloProgressDialog.show();
    }


    public void hideProgressDialog() {
        if (aielloProgressDialog != null && aielloProgressDialog.isShowing()) {
            aielloProgressDialog.dismiss();
            aielloProgressDialog = null;
        }
    }

    @Override
    protected void onDestroy() {
        Glide.get(img_album_url.getContext()).clearMemory();
        hidePbrDialog();
        super.onDestroy();
    }

}
