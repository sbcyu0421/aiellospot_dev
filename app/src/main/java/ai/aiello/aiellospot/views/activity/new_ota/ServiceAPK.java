package ai.aiello.aiellospot.views.activity.new_ota;

public class ServiceAPK {
    private static final String TAG = ServiceAPK.class.getSimpleName();
    private String name;
    private String releaseVersion;
    private String releaseVersionUrl;
    private String testVersion;
    private String testVersionUrl;
    private String localVersion;
    private String packageName;
    private String downloadUrl;

    public ServiceAPK(String name, String packageName) {
        this.name = name;
        this.packageName = packageName;
    }

    public ServiceAPK(String name, String releaseVersion, String releaseVersionUrl, String testVersion, String testVersionUrl, String packageName) {
        this.name = name;
        this.releaseVersion = releaseVersion;
        this.releaseVersionUrl = releaseVersionUrl;
        this.testVersion = testVersion;
        this.testVersionUrl = testVersionUrl;
        this.packageName = packageName;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReleaseVersion() {
        return releaseVersion;
    }

    public String getLocalVersion() {
        return localVersion;
    }

    public void setLocalVersion(String localVersion) {
        this.localVersion = localVersion;
    }

    public String getReleaseVersionUrl() {
        return releaseVersionUrl;
    }

    public String getTestVersion() {
        return testVersion;
    }

    public String getTestVersionUrl() {
        return testVersionUrl;
    }

    public void setReleaseVersion(String releaseVersion) {
        this.releaseVersion = releaseVersion;
    }

    public void setReleaseVersionUrl(String releaseVersionUrl) {
        this.releaseVersionUrl = releaseVersionUrl;
    }

    public void setTestVersion(String testVersion) {
        this.testVersion = testVersion;
    }

    public void setTestVersionUrl(String testVersionUrl) {
        this.testVersionUrl = testVersionUrl;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }



}

