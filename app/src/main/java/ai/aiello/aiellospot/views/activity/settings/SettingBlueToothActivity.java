package ai.aiello.aiellospot.views.activity.settings;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.activity.BaseActivity;
import ai.aiello.aiellospot.views.activity.multi.MultiIntentActivity;
import ai.aiello.aiellospot.core.device.DeviceControl;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.events.system.SystemEvent;
import ai.aiello.aiellospot.intents.IntentManager;

public class SettingBlueToothActivity extends BaseActivity implements View.OnClickListener {

    private LinearLayout ly_back;
    private TextView tv_bt_status, tv_btn_msg;
    private ImageView img_bt_status;
    private String TAG = SettingBlueToothActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_bluetooth);
        initView();
        updateView();
    }

    private void initView() {
        ly_back = findViewById(R.id.ly_back);
        img_bt_status = findViewById(R.id.img_clean);
        tv_btn_msg = findViewById(R.id.tv_btn_msg);
        tv_bt_status = findViewById(R.id.tv_bt_status);
        ly_back.setOnClickListener(this);
        tv_btn_msg.setOnClickListener(this);
    }

    private void updateView() {

        switch (DeviceControl.blueToothState) {
            case ON:
                img_bt_status.setImageResource(R.drawable.icon_home1_bt_on);
                String msg = getResources().getString(R.string.status_bluetooth_on) + DeviceControl.getBluetoothName();
                tv_bt_status.setText(msg);
                tv_btn_msg.setText(getResources().getString(R.string.btn_bluetooth_off));
                break;

            case CONNECTED:
                img_bt_status.setImageResource(R.drawable.icon_home1_bt_connect);
                tv_bt_status.setText(getResources().getString(R.string.status_bluetooth_connected) + DeviceControl.bondedDevicesName);
                tv_btn_msg.setText(getResources().getString(R.string.btn_bluetooth_off));
                break;

            case OFF:
                img_bt_status.setImageResource(R.drawable.icon_home1_bt_off);
                tv_bt_status.setText(getResources().getString(R.string.status_bluetooth_off));
                tv_btn_msg.setText(getResources().getString(R.string.btn_bluetooth_on));
                break;
        }


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ly_back:

                if (ActivityLauncher.isFromMultiActivity() && IntentManager.finishIntentList.size() != 0) {
                    ActivityLauncher.launchActivityByClass(this, MultiIntentActivity.class, R.anim.fade_in, R.anim.fade_out);
                } else if (ActivityLauncher.isFromStandbyActivity() || ActivityLauncher.isFromHomeActivity()) {
                    ActivityLauncher.launchActivityByClass(this, ActivityLauncher.currentHomeName, R.anim.fade_in, R.anim.fade_out);
                } else {
                    ActivityLauncher.launchActivityByClass(this, SettingsActivity.class, R.anim.fade_in, R.anim.fade_out);
                }
                break;

            case R.id.tv_btn_msg:
                if (DeviceControl.blueToothState.equals(DeviceControl.BTState.OFF)) {
                    DeviceControl.enableBT();
                    SpotUserTraceLog3.getInstance().buildUIEventLog(
                            SpotUserTraceLog3.EventSubject.BLUETOOTH,
                            SpotUserTraceLog3.EventAction.ON,
                            ""
                    );
                } else {
                    DeviceControl.disableBT();
                    SpotUserTraceLog3.getInstance().buildUIEventLog(
                            SpotUserTraceLog3.EventSubject.BLUETOOTH,
                            SpotUserTraceLog3.EventAction.OFF,
                            ""
                    );
                }
                break;
        }

    }


    @Override
    public void onMessageEvent(SystemEvent event) {
        super.onMessageEvent(event);
        switch (event.getType()){
            case BT_STATE_CHANGE:
                updateView();
                break;
        }
    }

}
