package ai.aiello.aiellospot.views.componets.homebtn.viewitems;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;

import com.github.johnpersano.supertoasts.library.Style;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.ref.WeakReference;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.events.module.RoomStatusStateChange;
import ai.aiello.aiellospot.views.componets.homebtn.HomeLeftStateButton;
import ai.aiello.aiellospot.views.componets.toast.MToaster;
import ai.aiello.aiellospot.views.activity.dialog.AielloAlertDialog;
import ai.aiello.aiellospot.core.config.SystemConfig;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractViewItem;
import ai.aiello.aiellospot.modules.roomstatus.RoomStatusModuleManager;

public class RoomStatusMurViewItem extends AbstractViewItem {

    private static String TAG = RoomStatusMurViewItem.class.getSimpleName();

    public static Thread roomStatusMurThread;
    UIHandler uiHandler;

    private static RoomStatusMurViewItem instance;

    public static RoomStatusMurViewItem getInstance() {
        if (instance == null) {
            instance = new RoomStatusMurViewItem();
        }
        return instance;
    }

    private RoomStatusMurViewItem() {}


    @Override
    public void updateView() {
        updateRoomStatusClean();
    }

    @Override
    public void handelBtnClickLogic() {
        wefBtn.get().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onMakeRoomClick();
            }
        });
    }

    private void onMakeRoomClick() {
        if (!RoomStatusModuleManager.RoomStatusModule.supportMur) {
            MToaster.showButtonToastLong(wefActivity.get(), wefActivity.get().getString(R.string.vsp_not_support_title), Style.TYPE_STANDARD);
            return;
        }
        if (RoomStatusModuleManager.getInstance().roomStatus.equals(RoomStatusModuleManager.RoomStatus.Clean)) {
            showRSConfirmDialog(
                    ChatApplication.context.getString(R.string.rs_dialog_title),
                    ChatApplication.context.getString(R.string.rs_mur_off),
                    () -> {
                        if (SystemConfig.OTA.support_action) {  // push compatible
                            RoomStatusModuleManager.getInstance().requestClean(false, "UI", false);
                        } else {
                            RoomStatusModuleManager.getInstance().requestClean(false, false, true, "UI");
                        }
                        SpotUserTraceLog3.getInstance().buildUIEventLog(
                                SpotUserTraceLog3.EventSubject.ROOM_STATUS,
                                SpotUserTraceLog3.EventAction.MUR_OFF,
                                ""
                        );
                    }
            );
        } else {
            showRSConfirmDialog(
                    ChatApplication.context.getString(R.string.rs_dialog_title),
                    ChatApplication.context.getString(R.string.rs_mur_on),
                    () -> {
                        // push compatible
                        if (SystemConfig.OTA.support_action) {
                            RoomStatusModuleManager.getInstance().requestClean(true, "UI", false);
                        } else {
                            RoomStatusModuleManager.getInstance().requestClean(true, false, true, "UI");
                        }
                        SpotUserTraceLog3.getInstance().buildUIEventLog(
                                SpotUserTraceLog3.EventSubject.ROOM_STATUS,
                                SpotUserTraceLog3.EventAction.MUR_ON,
                                ""
                        );
                    }
            );
        }
    }

    @Override
    public void unbindView(Activity activity) {
       super.unbindView(activity);
        if (roomStatusMurThread != null) {
            roomStatusMurThread.interrupt();
            roomStatusMurThread = null;
        }
        if (uiHandler != null) {
            uiHandler.removeCallbacksAndMessages(null);
            uiHandler = null;
        }
    }

    @Override
    public View buildButton(Context context) {
        HomeLeftStateButton homeLeftStateButton = new HomeLeftStateButton(context, this.width, this.height);
        homeLeftStateButton.updateImage(R.drawable.img_mur);
        homeLeftStateButton.updateText(R.string.askclean);
        return homeLeftStateButton;
    }

    static class UIHandler extends Handler {

        Activity activity;

        UIHandler(WeakReference<Activity> reference) {
            activity = reference.get();
        }

        @Override
        public void handleMessage(Message msg) {
        }
    }

    private AielloAlertDialog rsConfirmDialog;

    private void showRSConfirmDialog(String title, String message, RSCallback rsCallback) {
        rsConfirmDialog = new AielloAlertDialog(wefActivity.get(), R.style.Theme_AppCompat_Dialog_Alert);
        rsConfirmDialog.setTitle(title)
                .setTextContent(message)
                .setOnCancelListener((View.OnClickListener) v -> {
                    destroyRSConfirmDialog();
                })
                .setOnConfirmListener(v -> {
                    rsCallback.onConfirm();
                    MToaster.showButtonToastLong(wefActivity.get(), ChatApplication.context.getString(R.string.setting_room_status), Style.TYPE_PROGRESS_BAR);
                    destroyRSConfirmDialog();
                });
        rsConfirmDialog.show();
    }

    private void destroyRSConfirmDialog(){
        Log.d(TAG, "RS ConfirmDialog destroyed");
        try {
            if (rsConfirmDialog != null) {
                rsConfirmDialog.dismiss();
                rsConfirmDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    interface RSCallback {
        void onConfirm();
    }

    public void updateRoomStatusClean() {
        switch (RoomStatusModuleManager.getInstance().roomStatus) {
            case None:
            case DontDisturb:
                ((HomeLeftStateButton)this.wefBtn.get()).getTv_left_status().setVisibility(View.GONE);
                break;
            case Clean:
                ((HomeLeftStateButton)this.wefBtn.get()).getTv_left_status().setVisibility(View.VISIBLE);
                ((HomeLeftStateButton)this.wefBtn.get()).getTv_left_status().setBackgroundColor(ChatApplication.context.getResources().getColor(R.color.asr_enable));
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(RoomStatusStateChange event) {
//        switch (event.getStateCode()){
//            case RoomStatusStateChange.SUCCESS:
//                MToaster.showButtonToastLong(wefActivity.get(), "success", Style.TYPE_PROGRESS_BAR);
//                break;
//            case RoomStatusStateChange.ERROR:
//                MToaster.showButtonToastLong(wefActivity.get(), "error", Style.TYPE_PROGRESS_BAR);
//                break;
//        }
        updateView();
    }


}
