package ai.aiello.aiellospot.views.activity

import android.view.View
import androidx.fragment.app.Fragment

open class BaseFragment: Fragment() {

    override fun onResume() {
        super.onResume()
        hideActivityController()
    }

    open fun hideActivityController() {
        //hideNavController
        val decorView: View = requireActivity().getWindow().getDecorView()
        val uiOptions = (View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or View.SYSTEM_UI_FLAG_FULLSCREEN)
        decorView.systemUiVisibility = uiOptions
    }
}