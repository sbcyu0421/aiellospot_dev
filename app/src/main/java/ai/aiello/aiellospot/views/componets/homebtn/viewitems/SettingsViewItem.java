package ai.aiello.aiellospot.views.componets.homebtn.viewitems;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.activity.settings.SettingsActivity;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractHomeButton;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.HomeBasicButton;

public class SettingsViewItem extends AbstractViewItem {

    public static Thread settingsThread;
    UIHandler uiHandler;

    private static SettingsViewItem instance;

    public static SettingsViewItem getInstance() {
        if (instance == null) {
            instance = new SettingsViewItem();
        }
        return instance;
    }

    private SettingsViewItem() {}

    @Override
    public void updateView() {

    }

    @Override
    public void handelBtnClickLogic() {
        wefBtn.get().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityLauncher.launchActivityByClass(wefActivity.get(), SettingsActivity.class, R.anim.fade_in, R.anim.fade_out);
            }
        });
    }

    @Override
    public void unbindView(Activity activity) {
        super.unbindView(activity);
        if (settingsThread != null) {
            settingsThread.interrupt();
            settingsThread = null;
        }
        if (uiHandler != null) {
            uiHandler.removeCallbacksAndMessages(null);
            uiHandler = null;
        }
    }

    @Override
    public View buildButton(Context context) {
        HomeBasicButton homeBasicButton = new HomeBasicButton(context, this.width, this.height);
        homeBasicButton.updateImage(R.drawable.icon_home2_setting);
        homeBasicButton.updateText(R.string.setting);
        return homeBasicButton;
    }

    static class UIHandler extends Handler {

        Activity activity;

        UIHandler(WeakReference<Activity> reference) {
            activity = reference.get();
        }

        @Override
        public void handleMessage(Message msg) {
        }
    }
}
