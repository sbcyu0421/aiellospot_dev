package ai.aiello.aiellospot.views.componets.homebtn.viewitems;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import com.github.johnpersano.supertoasts.library.Style;

import java.lang.ref.WeakReference;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.componets.homebtn.HomeBasicButton;
import ai.aiello.aiellospot.views.componets.toast.MToaster;
import ai.aiello.aiellospot.views.activity.fun.SurroundingActivity;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractHomeButton;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractViewItem;
import ai.aiello.aiellospot.modules.surrounding.SurroundingModuleManager;

public class SurroundingCuisineViewItem extends AbstractViewItem {
    private final String noFound = "沒有資訊";
    private final String underConstruct = "建構中，敬請期待";

    public static Thread surroundingThread;
    UIHandler uiHandler;

    private static SurroundingCuisineViewItem instance;

    public static SurroundingCuisineViewItem getInstance() {
        if (instance == null) {
            instance = new SurroundingCuisineViewItem();
        }
        return instance;
    }

    private SurroundingCuisineViewItem() {}

    @Override
    public void updateView() {

    }

    @Override
    public void handelBtnClickLogic() {
        wefBtn.get().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SurroundingModuleManager.getInstance().getCuisineBean() == null) {
                    MToaster.showButtonToast(wefActivity.get(), underConstruct, Style.TYPE_STANDARD);
                    SurroundingModuleManager.getInstance().fetchData();
                } else if (SurroundingModuleManager.getInstance().getCuisineBean().getTitleSet().size() == 0) {
                    MToaster.showButtonToast(wefActivity.get(), noFound, Style.TYPE_STANDARD);
                } else {
                    ActivityLauncher.launchActivityByClass(wefActivity.get(), SurroundingActivity.class, R.anim.fade_in, R.anim.fade_out);
                    SurroundingModuleManager.getInstance().setCurrentType(SurroundingModuleManager.CUISINE);
                    wefActivity.get().finish();
                }

            }
        });
    }

    @Override
    public void unbindView(Activity activity) {
        super.unbindView(activity);
        if (surroundingThread != null) {
            surroundingThread.interrupt();
            surroundingThread = null;
        }
        if (uiHandler != null) {
            uiHandler.removeCallbacksAndMessages(null);
            uiHandler = null;
        }
    }

    @Override
    public View buildButton(Context context) {
        HomeBasicButton homeBasicButton = new HomeBasicButton(context, this.width, this.height);
        homeBasicButton.updateImage(R.drawable.ic_cuisine);
        homeBasicButton.updateText(R.string.ui_cuisine_service);
        return homeBasicButton;
    }

    static class UIHandler extends Handler {

        Activity activity;

        UIHandler(WeakReference<Activity> reference) {
            activity = reference.get();
        }

        @Override
        public void handleMessage(Message msg) {
        }
    }
}
