package ai.aiello.aiellospot.views.activity.showcase.beans

import ai.aiello.aiellospot.ChatApplication

class RadioTrackData(
    val uuid: String,
    val title: String,
    val singer: String,
    val song_url: String,
    val duration: String,
    var onSelect: Boolean = false
) {

    fun getSingerWithLocale(local: ChatApplication.Language): String {
        return LocaleParser.parser(local, singer)
    }

    fun getTitleWithLocale(local: ChatApplication.Language): String {
        return LocaleParser.parser(local, title)
    }

}