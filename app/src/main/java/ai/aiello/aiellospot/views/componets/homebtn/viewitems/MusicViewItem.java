package ai.aiello.aiellospot.views.componets.homebtn.viewitems;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import java.lang.ref.WeakReference;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.activity.music.MusicMenuActivity;
import ai.aiello.aiellospot.views.activity.music.MusicPlayer3Activity;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractHomeButton;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractViewItem;
import ai.aiello.aiellospot.modules.music.MusicServiceModuleManager;
import ai.aiello.aiellospot.views.componets.homebtn.HomeBasicButton;

public class MusicViewItem extends AbstractViewItem {

    public static Thread musicThread;
    UIHandler uiHandler;

    private static MusicViewItem instance;

    public static MusicViewItem getInstance() {
        if (instance == null) {
            instance = new MusicViewItem();
        }
        return instance;
    }

    private MusicViewItem() {}

    @Override
    public void updateView() {
    }

    @Override
    public void handelBtnClickLogic() {
        wefBtn.get().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MusicServiceModuleManager.playerStatus != MusicServiceModuleManager.PlayerStatus.onPlay) {//如果沒有播放，去menu
                    ActivityLauncher.launchActivityByClass(wefActivity.get(), MusicMenuActivity.class, R.anim.fade_in, R.anim.fade_out);
                    wefActivity.get().finish();
                } else {  //如果正在播放，去音樂
                    ActivityLauncher.launchActivityByClass(wefActivity.get(),MusicPlayer3Activity.class, R.anim.fade_in, R.anim.fade_out);
                    wefActivity.get().finish();
                }
            }
        });
    }

    @Override
    public void unbindView(Activity activity) {
        super.unbindView(activity);
        if (musicThread != null) {
            musicThread.interrupt();
            musicThread = null;
        }
        if (uiHandler != null) {
            uiHandler.removeCallbacksAndMessages(null);
            uiHandler = null;
        }
    }

    @Override
    public View buildButton(Context context) {
        HomeBasicButton homeBasicButton = new HomeBasicButton(context, this.width, this.height);
        homeBasicButton.updateImage(R.drawable.icon_home2_music);
        homeBasicButton.updateText(R.string.askmusic);
        return homeBasicButton;
    }

    static class UIHandler extends Handler {

        Activity activity;

        UIHandler(WeakReference<Activity> reference) {
            activity = reference.get();
        }

        @Override
        public void handleMessage(Message msg) {
        }
    }
}
