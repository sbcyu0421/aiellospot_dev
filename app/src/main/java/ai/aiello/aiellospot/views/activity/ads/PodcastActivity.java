package ai.aiello.aiellospot.views.activity.ads;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.CardActivity;
import ai.aiello.aiellospot.intents.sdui.ActionManager;
import ai.aiello.aiellospot.core.MCountdownTimer;
import ai.aiello.aiellospot.core.media.MsttsManager;
import ai.aiello.aiellospot.modules.ads.AdsObject;
import ai.aiello.aiellospot.modules.ads.AdsModuleManager;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.core.soundai.SoundAIManager;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.io.File;

public class PodcastActivity extends CardActivity {

    private static final String TAG = PodcastActivity.class.getSimpleName();
    private AdsObject infoObject;
    ImageView item_background;
    TextView tv_title;
    TextView tv_desc;
    ImageView img_qrcode, detail_exit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideActivityController();
        setContentView(R.layout.activity_podcast);
        initView();
        infoObject = AdsModuleManager.getInstance().getPlayList().get(AdsModuleManager.getInstance().getCursor());
        String title = infoObject.getTitle();
        String desc = infoObject.getDescription();
        String url = infoObject.getLocalUrl();
        String lang = infoObject.getLang();
        updateView(title, desc, url, lang);

    }

    @Override
    protected void onResume() {
        super.onResume();
        MCountdownTimer.startDismissTimer(300);
    }

    private void updateView(String title, String desc, String url, String lang) {
        try {
            tv_title.setText(title);
            tv_desc.setText(desc);
            String speakMsg = title + "!!!" + desc;
            MsttsManager.getInstance().podCastAds(speakMsg, false, lang, this);
        } catch (Exception e) {
            Log.e(TAG, "desc load error = " + e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }

        try {
            File adsFile = new File(url);
            Glide.with(this)
                    .load(adsFile)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(item_background);
        } catch (Exception e) {
            Log.e(TAG, "image load error = " + e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }

        try {
            if (!infoObject.getQrcode().equals("-") && !infoObject.getQrcode().isEmpty()) {
                String text = infoObject.getQrcode();
                MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
                BitMatrix bitMatrix = multiFormatWriter.encode(text, BarcodeFormat.QR_CODE, 200, 200);
                BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
                img_qrcode.setImageBitmap(bitmap);
                img_qrcode.setVisibility(View.VISIBLE);
            } else {
                img_qrcode.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            img_qrcode.setVisibility(View.GONE);
        }

        detail_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmAndGoBack();
            }
        });
    }


    private void initView() {
        item_background = findViewById(R.id.item_background);
        tv_title = findViewById(R.id.tv_title);
        tv_desc = findViewById(R.id.tv_desc);
        img_qrcode = findViewById(R.id.img_qrcode);
        detail_exit = findViewById(R.id.detail_exit);
    }


    public void hideActivityController() {
        //hideNavController
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }

    @Override
    public void confirmAndGoBack() {
        ActionManager.getInstance().stopForegroundAction();
        MsttsManager.getInstance().stopTTS();
        Intent intent = new Intent();
        intent.setClass(PodcastActivity.this, StandbyActivity.class);
        startActivity(intent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KEYCODE_AIELLO_MUTE:
                if (SoundAIManager.isWakeUpEnable()) {
                    SpotUserTraceLog3.getInstance().buildHardwareEventLog(
                            SpotUserTraceLog3.EventSubject.WAKEUP,
                            SpotUserTraceLog3.EventAction.OFF,
                            ""
                    );
                    SoundAIManager.setWakeUp(this, false);
                } else {
                    SpotUserTraceLog3.getInstance().buildHardwareEventLog(
                            SpotUserTraceLog3.EventSubject.WAKEUP,
                            SpotUserTraceLog3.EventAction.ON,
                            ""
                    );
                    SoundAIManager.setWakeUp(this, true);
                }
                break;
            case KEYCODE_AIELLO_VOLUME_UP:
            case KEYCODE_AIELLO_VOLUME_DOWN:
            case KEYCODE_AIELLO_BLUETOOTH:
                confirmAndGoBack();
                break;
        }

        return true;
    }

}
