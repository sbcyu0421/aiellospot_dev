package ai.aiello.aiellospot.views.activity.multi;

import android.content.Context;
import android.graphics.Bitmap;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.List;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.intents.IntentManager;
import ai.aiello.aiellospot.intents.IntentObject;

/**
 *
 */
public class ViewPagerAdapter extends PagerAdapter {
    private Animation amTranslate;
    private static String TAG = ViewPagerAdapter.class.getSimpleName();
    //private List<View> bitmapList;
    private List<Bitmap> bitmapList;
    private Context context;
    private static int viewX = 0; //(int) event.getRawX();
    private static int viewY = 78;

    private static float sRawX = 0;
    private static float sRawY = 0;

    private static int pRawY = 0;
    private final int child_width = 0;

    private ItemEventListener itemEventListener;

    interface ItemEventListener {
        void itemClick(IntentObject io);
        void itemSwipe(int position);
        void itemEmpty();
    }


    public ViewPagerAdapter(List<Bitmap> bitmapList, Context context, ItemEventListener itemEventListener) {
        this.bitmapList = bitmapList;//把上面类中的list传进来
        this.context = context;
        this.itemEventListener = itemEventListener;
    }

    @Override
    public int getCount() {
        return bitmapList.size();
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View pg_view = inflater.inflate(R.layout.item_viewpager, container, false);
        LinearLayout fm = pg_view.findViewById(R.id.fm);
        TextView tv_title = pg_view.findViewById(R.id.tv_title);
        ImageView img_sc = pg_view.findViewById(R.id.img_sc);

        fm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemEventListener.itemClick(IntentManager.finishIntentList.get(position));
            }
        });


        //1280 X 720
        fm.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent event) {

                viewX = (int) view.getX();
                viewY = (int) view.getY();
//                Log.d(TAG, "position=" + position);

                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        pRawY = (int) event.getRawY();//紀錄原始按的位置
                        sRawX = event.getRawX();
                        sRawY = event.getRawY();


                        return true;

                    case MotionEvent.ACTION_MOVE:
                        view.setX(0 + child_width * position);//one-width 1080-83-83=914
                        int deltaY = (int) event.getRawY() - pRawY;
                        view.setY(viewY + deltaY);//將最後的位置減去原始位置
                        pRawY = (int) event.getRawY();//更新位置
                        return true;

                    case MotionEvent.ACTION_UP:
                        //檢測移動的距離，如果很微小可以認為是點擊事件
                        if (Math.abs(event.getRawX() - sRawX) < 10 && Math.abs(event.getRawY() - sRawY) < 10) {
                            try {
                                Field field = View.class.getDeclaredField("mListenerInfo");
                                field.setAccessible(true);
                                Object object = field.get(view);
                                field = object.getClass().getDeclaredField("mOnClickListener");
                                field.setAccessible(true);
                                object = field.get(object);
                                if (object != null && object instanceof View.OnClickListener) {
                                    ((View.OnClickListener) object).onClick(view);
                                }
                            } catch (Exception e) {

                            }
                        } else {
                            if (view.getY() < -120) {
                                amTranslate = new TranslateAnimation(0.0f, 0.0f, view.getY(), -2000);
                                amTranslate.setDuration(500);
                                amTranslate.setRepeatCount(0);
                                view.startAnimation(amTranslate);
                                view.setX(0 + child_width * position);
                                view.setY(-720);

                                view.getAnimation().setAnimationListener(new Animation.AnimationListener() {
                                    @Override
                                    public void onAnimationStart(Animation animation) {
                                    }

                                    @Override
                                    public void onAnimationEnd(Animation animation) {
                                        view.getAnimation().cancel();
                                        //update ui
                                        if (getCount() == 1) {
                                            itemEventListener.itemEmpty();
//                                            EventBus.getDefault().post(new GoLastUIEvent("go back"));
                                        } else {
                                            itemEventListener.itemSwipe(position);
//                                            EventBus.getDefault().post(new DeleteIntentAdapterItemEvent(position));
                                        }
                                    }

                                    @Override
                                    public void onAnimationRepeat(Animation animation) {

                                    }
                                });

                            } else {
                                view.setX(0 + child_width * position);
                                view.setY(80);
                            }

//                            Log.d(TAG, "viewX=" + viewX + ";viewY=" + viewY);

                        }

                        return true;
                }

                return false;

            }


        });


        img_sc.setImageBitmap(bitmapList.get(position));
        tv_title.setText(IntentManager.finishIntentList.get(position).getcName());

        container.addView(pg_view);
        return pg_view;

    }


    @Override//销毁view
    public void destroyItem(ViewGroup container, int position, Object object) {
        // ((ViewGroup) bitmapList.httpGet(position).getParent()).removeView(bitmapList.httpGet(position));
        container.removeView((View) object);
        ((ImageView)((View) object).findViewById(R.id.img_sc)).setImageBitmap(null);
    }




}