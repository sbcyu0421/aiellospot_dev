package ai.aiello.aiellospot.views.activity.voip;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.BaseActivity;
import ai.aiello.aiellospot.core.asr.ASRManager;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.activity.dialog.AielloAlertDialog;
import ai.aiello.aiellospot.views.componets.toast.MToaster;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.modules.voip.Phonebook;
import ai.aiello.aiellospot.modules.voip.VoipModuleManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.github.johnpersano.supertoasts.library.Style;

public class VoipHomeActivity extends BaseActivity implements View.OnClickListener {

    static String TAG = VoipHomeActivity.class.getSimpleName();
    private LinearLayout ly_call_frontdesk, ly_call_restaurant, ly_call_sos, ly_call_room;
    private RelativeLayout ly_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voip_home);
        initView();
        updateView();

    }

    private void initView() {
        ly_back = findViewById(R.id.ly_back);
        ly_call_frontdesk = findViewById(R.id.ly_call_frontdesk);
        ly_call_restaurant = findViewById(R.id.ly_call_restaurant);
        ly_call_sos = findViewById(R.id.ly_call_sos);
        ly_call_room = findViewById(R.id.ly_call_room);

        ly_back.setOnClickListener(this);
        ly_call_frontdesk.setOnClickListener(this);
        ly_call_restaurant.setOnClickListener(this);
        ly_call_sos.setOnClickListener(this);
        ly_call_room.setOnClickListener(this);
    }

    public void updateView() {

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.ly_back:
                ActivityLauncher.launchActivityByClass(this, ActivityLauncher.currentHomeName, R.anim.fade_in, R.anim.fade_out);
                break;

            case R.id.ly_call_frontdesk:
                if (VoipModuleManager.getInstance().getInstance().currentState.getName() == VoipModuleManager.StateName.Disable) {
                    MToaster.showButtonToast(VoipHomeActivity.this,getString(R.string.voip_forbidden_action_on_disable_state), Style.TYPE_STANDARD);
                    return;
                }
                VoipModuleManager.getInstance().getInstance().makeCall(Phonebook.FRONT_DESK_NUMBER);

                SpotUserTraceLog3.getInstance().buildUIEventLog(
                        SpotUserTraceLog3.EventSubject.VOIP,
                        SpotUserTraceLog3.EventAction.DIAL,
                        "Front desk"
                );

                break;

            case R.id.ly_call_restaurant:
                if (VoipModuleManager.getInstance().getInstance().currentState.getName() == VoipModuleManager.StateName.Disable) {
                    MToaster.showButtonToast(VoipHomeActivity.this,getString(R.string.voip_forbidden_action_on_disable_state), Style.TYPE_STANDARD);
                    return;
                }
                VoipModuleManager.getInstance().getInstance().makeCall(Phonebook.RESTAURANT_NUMBER);

                SpotUserTraceLog3.getInstance().buildUIEventLog(
                        SpotUserTraceLog3.EventSubject.VOIP,
                        SpotUserTraceLog3.EventAction.DIAL,
                        "Restaurant"
                );

                break;

            case R.id.ly_call_sos:
                AielloAlertDialog sys_dialog = new AielloAlertDialog(this, R.style.Theme_AppCompat_Dialog_Alert);
                sys_dialog.setTitle(getString(R.string.make_sos_title))
                        .setTextContent(getString(R.string.make_sos_content))
                        .setOnCancelListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                sys_dialog.dismiss();
                            }
                        }).setOnConfirmListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (VoipModuleManager.getInstance().currentState.getName() == VoipModuleManager.StateName.Disable) {
                            MToaster.showButtonToast(VoipHomeActivity.this,getString(R.string.voip_forbidden_action_on_disable_state), Style.TYPE_STANDARD);
                            return;
                        }
                        VoipModuleManager.getInstance().makeCall(Phonebook.SOS_NUMBER);
                        SpotUserTraceLog3.getInstance().buildUIEventLog(
                                SpotUserTraceLog3.EventSubject.VOIP,
                                SpotUserTraceLog3.EventAction.DIAL,
                                "Emergency"
                        );

                        sys_dialog.dismiss();
                    }
                }).show();


                break;

            case R.id.ly_call_room:
                ActivityLauncher.launchActivityByClass(this, VoipDialActivity.class, R.anim.fade_in, R.anim.fade_out);
                break;


        }

    }


    @Override
    public void handleMakeCallResAction(String message) {
        if (ASRManager.getInstance().getOnASR()) {
            return;
        }
        if (message.equals(VoipModuleManager.ResponseStatus.SUCCESS.toString())) {
            Intent intent_makeCall = new Intent(VoipHomeActivity.this, VoipOnCallActivity.class);
            startActivity(intent_makeCall);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        } else if (message.equals(VoipModuleManager.ResponseStatus.SELF.toString())) {
            MToaster.showButtonToast(VoipHomeActivity.this, getString(R.string.call_self_error), Style.TYPE_STANDARD);
        } else if (VoipModuleManager.getInstance().makeCallRes.equals(VoipModuleManager.ResponseStatus.OFFLINE.name())) {
            MToaster.showButtonToast(VoipHomeActivity.this, getString(R.string.call_offline), Style.TYPE_STANDARD);
        } else if (message.equals(VoipModuleManager.ResponseStatus.BUSY.toString())) {
            MToaster.showButtonToast(VoipHomeActivity.this, getString(R.string.call_other_busy), Style.TYPE_STANDARD);
        } else {
            MToaster.showButtonToast(VoipHomeActivity.this, getString(R.string.makecall_fail), Style.TYPE_STANDARD);
        }
    }

}
