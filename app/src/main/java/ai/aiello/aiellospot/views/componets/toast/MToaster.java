package ai.aiello.aiellospot.views.componets.toast;

import ai.aiello.aiellospot.R;
import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;
import com.github.johnpersano.supertoasts.library.Style;
import com.github.johnpersano.supertoasts.library.SuperActivityToast;
import com.github.johnpersano.supertoasts.library.SuperToast;
import com.github.johnpersano.supertoasts.library.utils.PaletteUtils;

import java.lang.ref.WeakReference;

public class MToaster {

    public static void showButtonToast(Activity activity, String msg, int type) {

        WeakReference<Activity> weakReference = new WeakReference<>(activity);
        SuperActivityToast.create(weakReference.get(), new Style(), type)
                .setText(msg)
                .setTextSize(Style.TEXTSIZE_VERY_LARGE)
                .setFrame(Style.FRAME_LOLLIPOP)
                .setColor(PaletteUtils.getSolidColor(PaletteUtils.BLACK))
                .setAnimations(Style.ANIMATIONS_POP)
                .setDuration(Style.DURATION_VERY_SHORT)
                .show();

    }


    public static void showProgressToast(Activity activity, String msg) {

        WeakReference<Activity> weakReference = new WeakReference<>(activity);
        SuperActivityToast.create(weakReference.get(), new Style(), Style.TYPE_PROGRESS_BAR)
                .setText(msg)
                .setTextSize(Style.TEXTSIZE_VERY_LARGE)
                .setFrame(Style.FRAME_LOLLIPOP)
                .setColor(PaletteUtils.getSolidColor(PaletteUtils.MATERIAL_PURPLE))
                .setAnimations(Style.ANIMATIONS_POP)
                .setDuration(Style.DURATION_VERY_SHORT)
                .show();
    }


    public static void showButtonToastLong(Activity activity, String msg, int type) {

        WeakReference<Activity> weakReference = new WeakReference<>(activity);
        SuperActivityToast.create(weakReference.get(), new Style(), type)
                .setText(msg)
                .setTextSize(Style.TEXTSIZE_VERY_LARGE)
                .setFrame(Style.FRAME_LOLLIPOP)
                .setColor(PaletteUtils.getSolidColor(PaletteUtils.BLACK))
                .setAnimations(Style.ANIMATIONS_POP)
                .setDuration(Style.DURATION_VERY_SHORT)
                .show();

    }

}
