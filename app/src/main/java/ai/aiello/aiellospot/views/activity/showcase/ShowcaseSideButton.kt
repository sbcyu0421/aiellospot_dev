package ai.aiello.aiellospot.views.activity.showcase

import ai.aiello.aiellospot.ChatApplication
import ai.aiello.aiellospot.R
import ai.aiello.aiellospot.views.activity.showcase.beans.HotelFeatureBean
import android.annotation.SuppressLint
import android.content.Context
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView


@SuppressLint("WrongConstant")
class ShowcaseSideButton(hotelFeatureBean: HotelFeatureBean, context: Context?) :
    RelativeLayout(context) {

    var unikey: String? = null
    var title = ""
    val focusIcon = ImageView(context)
    val textView = TextView(context)
    var position = 0

    init {
        title = hotelFeatureBean.getTitle(ChatApplication.system_lang)
        unikey = hotelFeatureBean.unikey

        val focusIconParams = RelativeLayout.LayoutParams(10 , LayoutParams.MATCH_PARENT)
        focusIcon.layoutParams = focusIconParams
        focusIcon.setBackgroundColor(resources.getColor(R.color.hotel_feature_btn_focus))
        focusIcon.visibility = View.GONE

        val textParams = RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT , LayoutParams.MATCH_PARENT)
        textParams.setMargins(12, 2,12,2)
        textView.layoutParams = textParams
        textView.text = title
        textView.setTextColor(resources.getColor(R.color.white))
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, 46F)
        textView.letterSpacing =0.1f
        textView.gravity = Gravity.CENTER
        textParams.addRule(RelativeLayout.CENTER_IN_PARENT);

        this.setBackgroundColor(resources.getColor(R.color.listener_btn))
//        this.orientation = LinearLayout.HORIZONTAL
        this.addView(focusIcon)
        this.addView(textView)
        this.gravity = Gravity.CENTER

        val layoutParams = RelativeLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 140)
        layoutParams.setMargins(0, 0,0,6)
        this.layoutParams = layoutParams

    }

}