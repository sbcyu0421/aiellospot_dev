package ai.aiello.aiellospot.views.componets.homebtn;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import ai.aiello.aiellospot.R;

public class HomeStateButton extends HomeBaseButton {

    static String TAG = HomeStateButton.class.getSimpleName();

    private ImageView stateImage;
    RelativeLayout.LayoutParams stateImageParams;

    public HomeStateButton(Context context, int width, int height) {
        super(context, width, height);
    }

    @Override
    int getLayoutXml() { return R.layout.button_home_state; }

    @Override
    public void initViews(Context context, int width, int height) {
        super.initViews(context, width, height);
        this.stateImage = btn.findViewById(R.id.im_state_image);
        this.stateImageParams = (RelativeLayout.LayoutParams) stateImage.getLayoutParams();
    }

    @Override
    public void applyLayoutParams() {
        super.applyLayoutParams();
        int size = (int)(height * 160 * 0.15);
        stateImageParams.width = size;
        stateImageParams.height = size;
        stateImage.setLayoutParams(stateImageParams);

    }

    public ImageView getStateImage() {
        return this.stateImage;
    }
}
