package ai.aiello.aiellospot.views.componets.homebtn;

import android.app.Activity;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import ai.aiello.aiellospot.events.system.SystemEvent;

public abstract class AbstractViewItem implements IViewLogic {

    private static String TAG = AbstractViewItem.class.getSimpleName();

    public String type;
    public String moduleName;
    public String layoutName;
    public int page;
    public ArrayList<Integer> position;
    public int columnIndex;
    public int rowIndex;
    public int width;
    public int height;

    public WeakReference<Activity> wefActivity;
    public WeakReference<View> wefBtn;
    public String bgColorCode;

    public String getModuleName() {
        return moduleName;
    }

    public String getLayoutName() {
        return layoutName;
    }

    public int getPage() {
        return page;
    }

    public ArrayList<Integer> getPosition() {
        return position;
    }

    public void buildLayoutInfo(String moduleName, String layoutName, int page, ArrayList<Integer> position, String bgColorCode) {
        this.moduleName = moduleName;
        this.layoutName = layoutName;
        this.page = page;
        this.columnIndex = position.get(0);
        this.rowIndex = position.get(1);
        this.width = position.get(2);
        this.height = position.get(3);
        this.bgColorCode = bgColorCode;
    }

    public void bindView(WeakReference<Activity> wefActivity, WeakReference<View> wefBtn) {
        if (!wefActivity.get().isTaskRoot()) return;
        this.wefActivity = wefActivity;
        this.wefBtn = wefBtn;
        HomeLayoutManager.setBackground(this.wefBtn.get(), this.bgColorCode);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    public void unbindView(Activity activity) {
        if (activity != wefActivity.get()) {
            Log.d(TAG, "unbindView: skip, was called by other activity.");
            return;
        }
        this.wefActivity.clear();
        this.wefBtn.clear();
        this.wefActivity = null;
        this.wefBtn = null;
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @NonNull
    @Override
    public String toString() {
        return String.format("type = %s, moduleName = %s, layoutName = %s |", type, moduleName, layoutName);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(SystemEvent event) {
        if (this.wefActivity == null || this.wefBtn == null) return;
    }

}
