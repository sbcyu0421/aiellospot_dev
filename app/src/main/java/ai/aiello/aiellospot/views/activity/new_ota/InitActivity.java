package ai.aiello.aiellospot.views.activity.new_ota;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.componets.homebtn.HomeLayoutManager;
import ai.aiello.aiellospot.views.activity.oobe.LocaleActivity;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.activity.dialog.AielloAlertDialog;
import ai.aiello.aiellospot.modules.ModuleManager;
import ai.aiello.aiellospot.core.config.APIConfig;
import ai.aiello.aiellospot.core.config.Configer;
import ai.aiello.aiellospot.core.device.DeviceControl;
import ai.aiello.aiellospot.core.info.DeviceInfo;

import ai.aiello.aiellospot.core.system.AielloWifiManager;
import ai.aiello.aiellospot.utlis.AESEncryptor;
import ai.aiello.aiellospot.utlis.Http;
import ai.aiello.aiellospot.modules.voip.Phonebook;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.view.View;
import android.widget.*;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.litesuits.android.log.Log;
import com.litesuits.common.io.FileUtils;

import io.reactivex.annotations.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class InitActivity extends Activity {

    private static String TAG = InitActivity.class.getSimpleName();
    private TextView tv_init_loading, tv_showdia, tv_safemode;
    private ProgressBar init_progress_bar, init_progress_bar2;
    private ImageView icon;
    private TextView tv_progress_bar_msg, tv_progress_bar2_msg;
    private static int tryTime = 0;
    private static CountDownTimer reTryTimer;
    private static CountDownTimer connectTimer;
    private final int connect_timeout = 40;
    private Process process = Process.WifiConnect;
    private Configer configer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init_system2);
        initView();
        startProcess(Process.WifiConnect);
        ChatApplication.system_lang = ChatApplication.Language.None;
    }


    private void initView() {
        setContentView(R.layout.activity_init_system2);
        tv_init_loading = findViewById(R.id.init_progress_msg);
        tv_showdia = findViewById(R.id.tv_showdia);
        tv_safemode = findViewById(R.id.tv_safemode);
        init_progress_bar = findViewById(R.id.init_progress_bar);
        init_progress_bar2 = findViewById(R.id.init_progress_bar2);
        tv_progress_bar_msg = findViewById(R.id.tv_progress_bar_msg);
        tv_progress_bar2_msg = findViewById(R.id.tv_progress_bar2_msg);
        icon = findViewById(R.id.icon);

        init_progress_bar.setMax(100);
        init_progress_bar2.setMax(100);
        init_progress_bar2.setVisibility(View.INVISIBLE);
        showSubProgressBar(0);
        hideSubProgressBar();
        hideActivityController();


    }

    public void hideEngineerBtn() {
        tv_showdia.setOnClickListener(null);
        tv_safemode.setOnClickListener(null);
        tv_showdia.setVisibility(View.INVISIBLE);
        tv_safemode.setVisibility(View.INVISIBLE);
    }

    public void showEngineerBtn() {
        tv_showdia.setVisibility(View.VISIBLE);
        tv_safemode.setVisibility(View.VISIBLE);
        tv_showdia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (reTryTimer != null) {
                    reTryTimer.cancel();
                    reTryTimer = null;
                }
                if (connectTimer != null) {
                    connectTimer.cancel();
                    connectTimer = null;
                }
                showWifiConfigDialog();
            }
        });

        tv_safemode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (reTryTimer != null) {
                    reTryTimer.cancel();
                    reTryTimer = null;
                }

                if (connectTimer != null) {
                    connectTimer.cancel();
                    connectTimer = null;
                }

                AlertDialog.Builder editDialog = new AlertDialog.Builder(InitActivity.this);
                editDialog.setTitle(getString(R.string.psd_input));
                final EditText editText = new EditText(InitActivity.this);
                editDialog.setView(editText);

                editDialog.setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                        if (editText.getText().toString().equals(Phonebook.MN_WEBVIEW)) {
                            Intent intent = new Intent();
                            intent.setClassName("org.chromium.webview_shell", "org.chromium.webview_shell.WebViewBrowserActivity");
                            startActivity(intent);
                        } else if (editText.getText().toString().equals(Phonebook.MN_SYSDIAG_TEST)) {
                            Intent launchSysdiagApk = getPackageManager().getLaunchIntentForPackage("com.example.sysdiag");
                            if (launchSysdiagApk != null) {
                                startActivity(launchSysdiagApk);
                            }
                        } else if (editText.getText().toString().equals(Phonebook.MN_SETTING)) {
                            Intent intent3 = new Intent(Settings.ACTION_SETTINGS);
                            startActivity(intent3);
                        } else {
                            Toast.makeText(getApplicationContext(), "Invalid password", Toast.LENGTH_SHORT).show();
                            startProcess(Process.WifiConnect);
                        }
                    }
                });
                editDialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                        startProcess(Process.WifiConnect);
                    }
                });
                editDialog.show();
            }
        });

    }

    public void hideActivityController() {
        //hideNavController
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }

    enum Process {
        WifiConnect, OTA, ConfigAPI, CountryCode, DeviceInfo
    }

    private final int WIFI_CONNECTING = 1;
    private final int WIFI_CONNECT_TIMEOUT = 2;
    private final int WIFI_CONNECTED = 3;

    private final int CONFIG_FETCH_START = 4;
    private final int CONFIG_FETCH_RETRY = 5;
    private final int CONFIG_FETCH_FAIL = 6;
    private final int CONFIG_FETCH_NOAUTH = 7;
    private final int CONFIG_FETCH_SUCCESS = 8;

    private final int DEVICEINFO_SUCCESS = 9;
    private final int DEVICEINFO_NO_AUTH = 10;
    private final int DEVICEINFO_NO_NETWORK = 11;
    private final int DEVICEINFO_TIMEOUT = 12;
    private final int DEVICEINFO_FORMAT_ERROR = 13;
    private final int CONFIG_OTA_FETCH_START = 14;

    private final int WIFI_COUNTRY_CODE_SETTING = 80;
    private final int WIFI_COUNTRY_CODE_TIMEOUT = 90;
    private final int WIFI_COUNTRY_CODE_CONNECTED = 100;

    private final int RETRY_PROCESS = 200;


    //WifiConnect->ConfigAPI->CountryCode->DeviceInfo->OTA
    private void startProcess(Process status) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //flow
                switch (status) {
                    case WifiConnect:
                        connectWifi();
                        break;
                    case ConfigAPI:
                        configApi();
                        break;
                    case CountryCode:
                        setCountryCode();
                        break;
                    case DeviceInfo:
                        getDeviceInfo();
                        break;
                    case OTA:
                        executeOta();
                        break;
                }
            }
        });

    }

    Handler uiHandler = new Handler() {
        //handle ui status
        @Override
        public void handleMessage(Message message) {
            super.handleMessage(message);
            switch (message.what) {
                case WIFI_CONNECTING:
                    tv_init_loading.setText("Connect to wifi, already spend time : " + (connect_timeout - message.arg1) + " seconds");
                    tv_showdia.setVisibility(View.VISIBLE);
                    tv_safemode.setVisibility(View.VISIBLE);
                    tv_showdia.setEnabled(true);
                    tv_safemode.setEnabled(true);
                    showMainProgressBar(0);
                    showEngineerBtn();
                    break;

                case WIFI_CONNECT_TIMEOUT:
                    showWifiConfigDialog();
                    break;

                case WIFI_CONNECTED:
                    startProcess(Process.OTA);
                    break;

                case WIFI_COUNTRY_CODE_SETTING:
                    tv_init_loading.setText("Optimizing network...");
                    break;

                case WIFI_COUNTRY_CODE_TIMEOUT:
                    reTryProcess(getString(R.string.api_connect_timeout));
                    break;

                case WIFI_COUNTRY_CODE_CONNECTED:
                    startProcess(Process.DeviceInfo);
                    break;

                case CONFIG_OTA_FETCH_START:
                    tv_init_loading.setText("OTA Data fetching ...");
                    showMainProgressBar(0);
                    break;

                case CONFIG_FETCH_START:
                    tv_init_loading.setText("Service fetching ...");
                    showMainProgressBar(0);
                    break;

                case CONFIG_FETCH_RETRY:
                    reTryProcess("Fail to fetch data");
                    break;

                case CONFIG_FETCH_NOAUTH:
                    tv_init_loading.setText(DeviceInfo.MAC + getString(R.string.device_is_not_authorized));
                    hideMainProgressBar();
                    showAuthorizedDialog();
                    break;

                case CONFIG_FETCH_FAIL:
                    String error = message.getData().getString("config_error");
                    tv_init_loading.setText(error);
                    tv_init_loading.setVisibility(View.VISIBLE);
                    hideMainProgressBar();
                    break;

                case CONFIG_FETCH_SUCCESS:
                    try {
                        tv_init_loading.setText("Service Fetch Completed");
                        showMainProgressBar(100);
                        startProcess(Process.CountryCode);
                    } catch (Exception e) {
                        Log.e(TAG, "Initial fail = " + e.toString());
                        tv_init_loading.setText("Initial fail，please notify the administrator\n" + e.getMessage());
                    }
                    break;

                case DEVICEINFO_SUCCESS:
                    tv_init_loading.setText(getString(R.string.device_is_authorized));
                    showMainProgressBar(100);
                    //setup the correct info, and go to locale activity
                    DeviceControl.setBluetoothName(DeviceInfo.hotelName + "R" + DeviceInfo.roomName + "M" + DeviceInfo.MAC.substring(15));
                    DeviceControl.disableBT();
                    new AsyncTask<Void, Void,Boolean>(){
                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            dismissConnectingDialog();
                            //todo play module create animation
                        }

                        @Override
                        protected Boolean doInBackground(Void... voids) {
                            boolean success = ModuleManager.createModules();
                            HomeLayoutManager.getInstance().initLayoutData();
                            return success;
                        }

                        @Override
                        protected void onPostExecute(Boolean aBoolean) {
                            super.onPostExecute(aBoolean);
                            //todo stop module create animation
                            if (aBoolean)
                                ActivityLauncher.launchActivityByClass(InitActivity.this, LocaleActivity.class, R.anim.fade_in, R.anim.fade_out);
                            else {
                                tv_init_loading.setText("module config error");
                            }
                        }
                    }.execute();
                    break;

                case DEVICEINFO_NO_AUTH:
                    tv_init_loading.setText(DeviceInfo.MAC + getString(R.string.device_is_not_authorized));
                    hideMainProgressBar();
                    showAuthorizedDialog();
                    break;

                case DEVICEINFO_NO_NETWORK:
                    Log.d(TAG, "DEVICEINFO_NO_NETWORK");
                    reTryProcess(getString(R.string.wifi_no_network));
                    break;

                case DEVICEINFO_TIMEOUT:
                    Log.d(TAG, "DEVICEINFO_TIMEOUT");
                    reTryProcess(getString(R.string.api_connect_timeout));
                    break;

                case DEVICEINFO_FORMAT_ERROR:
                    Log.d(TAG, "DEVICEINFO_SERVER_ERROR");
                    reTryProcess(getString(R.string.api_server_error));
                    break;

                case OTA_MAIN_PROGRESS:
                    int main_progress = message.getData().getInt("main_progress");
                    showMainProgressBar(main_progress);
                    break;

                case OTA_OTA_START:
                    tv_showdia.setVisibility(View.INVISIBLE);
                    tv_safemode.setVisibility(View.INVISIBLE);
                    tv_showdia.setEnabled(false);
                    tv_safemode.setEnabled(false);
                    tv_init_loading.setText("Version Checking...");
                    showMainProgressBar(0);
                    hideSubProgressBar();
                    hideEngineerBtn();
                    break;

                case OTA_DOWNLOAD_PROGRESS:
                    int sub_progress = message.getData().getInt("sub_progress");
                    String downloadApk = message.getData().getString("data");
                    tv_init_loading.setText(downloadApk + " is downloading...");
                    showSubProgressBar(sub_progress);
                    break;

                case OTA_DOWNLOAD_FAIL:
                    try {
                        if (otaAsyncTask != null) {
                            otaAsyncTask.cancel(true);
                            otaAsyncTask = null;
                        }
                    } catch (Exception e) {
                        Log.e(TAG, e.toString());
                    }
                    String downloadFailApk = message.getData().getString("data");
                    tv_init_loading.setText(downloadFailApk);
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    reTryProcess("OTA_RETRY");
                    break;

                case OTA_INSTALLING:
                    String installingApk = message.getData().getString("data");
                    tv_init_loading.setText(installingApk + " installing...");
                    tv_init_loading.setVisibility(View.VISIBLE);
                    showSubProgressBar(-1);
                    break;


                case OTA_INSTALLED:
                    String installedApk = message.getData().getString("data");
                    tv_init_loading.setText(installedApk + " is been installed. ");
                    break;

                case OTA_COMPLETED:
                    //upload the latest version when completed
                    tv_init_loading.setText("Good Job! You app is the latest one! ");
                    VersionCheckerT.img_ver = VersionCheckerT.getSystemImageVersion();
                    VersionCheckerT.apk_ver = VersionCheckerT.getLocalVersion(InitActivity.this, VersionCheckerT.SPOT_NAME);
                    VersionCheckerT.push_ver = VersionCheckerT.getLocalVersion(InitActivity.this, VersionCheckerT.PUSH_SERVICE_NAME);
                    VersionCheckerT.music_ver = VersionCheckerT.getLocalVersion(InitActivity.this, VersionCheckerT.MUSIC_SERVICE_NAME);
                    VersionCheckerT.sai_ver = VersionCheckerT.getLocalVersion(InitActivity.this, VersionCheckerT.SAI_SERVICE_NAME);
                    uploadVersion(VersionCheckerT.img_ver, VersionCheckerT.apk_ver, VersionCheckerT.push_ver, VersionCheckerT.music_ver, VersionCheckerT.sai_ver);
                    startProcess(Process.ConfigAPI);
                    break;

                case RETRY_PROCESS:
                    String smsg = message.getData().getString("msg");
                    int retrySleep = message.getData().getInt("retrySleep");
                    Log.d(TAG, "retry process, retrySleep = " + retrySleep);
                    reTryTimer = new CountDownTimer(retrySleep, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            //Log.d(TAG, millisUntilFinished / 1000);
                            if (smsg.equals("OTA_RETRY")) {
                                tv_init_loading.setText(String.format(getString(R.string.ota_retry), millisUntilFinished / 1000));
                            } else {
                                tv_init_loading.setText(smsg + "\n" + String.format(getString(R.string.ota_retry), millisUntilFinished / 1000));
                            }
                        }

                        @Override
                        public void onFinish() {
                            startProcess(Process.WifiConnect);
                        }
                    };
                    reTryTimer.start();
                    break;

            }
        }
    };


    private final int OTA_ON_CHECK_RESULT = 1222;
    private final int OTA_DOWNLOAD_PROGRESS = 1122;
    private final int OTA_INSTALLING = 1223;
    private final int OTA_INSTALLED = 1224;
    private final int OTA_MAIN_PROGRESS = 1228;
    private final int OTA_DOWNLOAD_FAIL = 1229;

    private final int OTA_OTA_START = 922;
    private final int OTA_COMPLETED = 1225;


    public void reTryProcess(@Nullable String msg) {
        Log.d(TAG, "reTryProcess = " + msg);
        hideSubProgressBar();

        tv_safemode.setVisibility(View.VISIBLE);
        tv_showdia.setVisibility(View.VISIBLE);
        tv_safemode.setEnabled(true);
        tv_showdia.setEnabled(true);
        tryTime++;
        int retrySleep;
        if (tryTime < 20) {
            retrySleep = tryTime * 3 * 1000;
        } else {
            retrySleep = 60 * 1000;
        }
        Message message = Message.obtain();
        message.what = RETRY_PROCESS;
        Bundle bundle = new Bundle();
        bundle.putString("msg", msg);
        bundle.putInt("retrySleep", retrySleep);
        message.setData(bundle);
        uiHandler.sendMessage(message);
    }

    private OtaAsyncTask otaAsyncTask;
    private Thread configThread = null;

    public void executeOta() {
        //1. get hotelid for ota
        //2. success and start ota
        deleteTmpFile();
        icon.setOnClickListener(null);
        if (configThread != null && configThread.isInterrupted()) {
            configThread.interrupt();
        }
        configThread = new Thread(new Runnable() {
            @Override
            public void run() {
                //get configs api
                configer = new Configer();
                configer.getConfigs(DeviceInfo.MAC, ChatApplication.releaseType, getApplicationContext(), new Configer.ConfigStatusListener() {

                    @Override
                    public void onStart() {
                        Log.d(TAG, "config fetch onStart");
                        uiHandler.sendEmptyMessage(CONFIG_OTA_FETCH_START);
                    }

                    @Override
                    public void onRetry() {
                        Log.d(TAG, "config fetch onRetry");
                        uiHandler.sendEmptyMessage(CONFIG_FETCH_RETRY);
                    }

                    @Override
                    public void onDeviceNoFound() {
                        Log.d(TAG, "config fetch onDeviceNoFound");
                        uiHandler.sendEmptyMessage(CONFIG_FETCH_NOAUTH);
                    }

                    @Override
                    public void onFail(String reason) {
                        Message message = Message.obtain();
                        message.what = CONFIG_FETCH_FAIL;
                        Bundle bundle = new Bundle();
                        bundle.putString("config_error", reason);
                        message.setData(bundle);
                        uiHandler.sendMessage(message);
                    }

                    @Override
                    public void onSuccess() {
                        ota();
                    }

                }, Configer.Type.ota);
            }
        });
        configThread.start();
    }

    public void ota() {

        otaAsyncTask = new OtaAsyncTask(this, new OtaProgressListener() {
            @Override
            public void onOtaStart() {
                uiHandler.sendEmptyMessage(OTA_OTA_START);
            }


            @Override
            public void onCheckResult(String apk, boolean isLatest) {
                Message message = Message.obtain();
                message.what = OTA_ON_CHECK_RESULT;
                Bundle b = new Bundle();
                b.putString("data", apk);
                b.putBoolean("latest", isLatest);
                message.setData(b);
                uiHandler.sendMessage(message);
            }


            @Override
            public void onOtaProgress(int progress) {
                Message message = Message.obtain();
                message.what = OTA_MAIN_PROGRESS;
                Bundle b = new Bundle();
                b.putInt("main_progress", progress);
                message.setData(b);
                uiHandler.sendMessage(message);
            }

            @Override
            public void onDownloadingProgress(String apk, int progress) {
                Message message = Message.obtain();
                message.what = OTA_DOWNLOAD_PROGRESS;
                Bundle b = new Bundle();
                b.putString("data", apk);
                b.putInt("sub_progress", progress);
                message.setData(b);
                uiHandler.sendMessage(message);
            }

            @Override
            public void onDownloadingFail(String apk) {
                Message message = Message.obtain();
                message.what = OTA_DOWNLOAD_FAIL;
                Bundle b = new Bundle();
                b.putString("data", apk);
                message.setData(b);
                uiHandler.sendMessage(message);
            }

            @Override
            public void onInstalling(String apk) {
                Message message = Message.obtain();
                message.what = OTA_INSTALLING;
                Bundle b = new Bundle();
                b.putString("data", apk);
                message.setData(b);
                uiHandler.sendMessage(message);
            }

            @Override
            public void onInstalled(String apk) {
                Message message = Message.obtain();
                message.what = OTA_INSTALLED;
                Bundle b = new Bundle();
                b.putString("data", apk);
                message.setData(b);
                uiHandler.sendMessage(message);
            }

            @Override
            public void onOtaCompleted() {
                uiHandler.sendEmptyMessage(OTA_COMPLETED);
            }

        }, VersionCheckerT.serviceApkMap);
        otaAsyncTask.execute();
    }

    private void getDeviceInfo() {
        ChatApplication.worker.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    String url = String.format(APIConfig.WEBAPI_DEVICEINFO.getUrl(), DeviceInfo.MAC + "&ver=2");
                    String res = Http.get(url, 10000, APIConfig.WEBAPI_DEVICEINFO.getKey()).response;
                    if (res.isEmpty()) {
                        Log.e(TAG, getString(R.string.wifi_no_network));
                        uiHandler.sendEmptyMessage(DEVICEINFO_TIMEOUT);
                        return;
                    }
                    JSONObject jsonObject = new JSONObject(res);
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    if (jsonArray.length() == 0) {
                        Log.d(TAG, DeviceInfo.MAC + getString(R.string.device_is_not_authorized));
                        uiHandler.sendEmptyMessage(DEVICEINFO_NO_AUTH);
                        return;
                    }
                    JSONObject device = jsonArray.getJSONObject(0);
                    DeviceInfo.uuid = device.getString("uuid");

                    JSONObject room = device.getJSONObject("room");
                    DeviceInfo.roomName = room.getString("name");
                    try {
                        DeviceInfo.roomType = room.getString("roomType");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    JSONObject hotel = room.getJSONObject("hotel");
                    DeviceInfo.hotelName = hotel.getString("name");
                    DeviceInfo.hotelID = hotel.getInt("hid");
                    DeviceInfo.domain = hotel.getString("name");
                    DeviceInfo.geo_lat = Double.parseDouble(hotel.getString("geo").split(",")[1]);
                    DeviceInfo.geo_lon = Double.parseDouble(hotel.getString("geo").split(",")[0]);
                    DeviceInfo.hotelWelcome2_en = hotel.getString("welcomeIntroduction");
                    DeviceInfo.hotelWelcome2_tw = hotel.getString("welcomeIntroduction_tw");
                    DeviceInfo.hotelWelcome2_cn = hotel.getString("welcomeIntroduction_cn");

                    JSONObject city = hotel.getJSONObject("city");
                    DeviceInfo.timeZone = city.getString("timeZone");
                    DeviceInfo.city_tw = city.getString("name_tw");
                    DeviceInfo.city_en = city.getString("name");
                    DeviceInfo.city_cn = city.getString("name_cn");
                    try {
                        DeviceInfo.city_ja = city.getString("name_ja");
                    } catch (JSONException e) {
                        DeviceInfo.city_ja = "";
                        e.printStackTrace();
                    }

                    try {
                        DeviceInfo.accuRedirectUrl = device.getJSONArray("urlRedirection").getJSONObject(0).getString("url");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    android.util.Log.d(TAG, "AccuRedirectUrl: " + DeviceInfo.accuRedirectUrl);

                    //get room ssid and password
                    try {
                        String networkSSID = room.getString("ssid");
                        String networkPass = AESEncryptor.decrypt(room.getString("password"));
                        int keyMgmt = room.getInt("keyMgmt");
                        boolean hiddenSSID = room.getBoolean("hiddenSSID");

                        JSONObject networkObj = room.getJSONObject("network");
                        String assignment = "";
                        try {
                            assignment = networkObj.getString("assignment");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        switch (assignment) {
                            case "STATIC":
                                try {
                                    String ip = networkObj.getString("ip");
                                    String maskLength = networkObj.getString("maskLength");
                                    String gateway = networkObj.getString("gateway");
                                    String dns1 = "";
                                    try {
                                        dns1 = networkObj.getJSONArray("dns").getString(0);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    String dns2 = "";
                                    try {
                                        dns2 = networkObj.getJSONArray("dns").getString(1);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    String proxy = null;
                                    try {
                                        proxy = networkObj.getString("proxy");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                    AielloWifiManager.connectWithRoomInfo(networkSSID, networkPass, keyMgmt, hiddenSSID, assignment, ip, maskLength, gateway, dns1, dns2, proxy);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                break;

                            case "":
                            case "DHCP":
                                AielloWifiManager.connectWithRoomInfo(networkSSID, networkPass, keyMgmt, hiddenSSID, assignment, null, null, null, null, null, null);
                                break;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.d(TAG, getString(R.string.device_is_authorized));
                    uiHandler.sendEmptyMessage(DEVICEINFO_SUCCESS);
                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                    uiHandler.sendEmptyMessage(DEVICEINFO_FORMAT_ERROR);
                }
            }
        });
    }

    private void configApi() {
        process = Process.ConfigAPI;
        deleteTmpFile();
        icon.setOnClickListener(null);
        if (configThread != null && configThread.isInterrupted()) {
            configThread.interrupt();
        }
        configThread = new Thread(new Runnable() {
            @Override
            public void run() {
                //get configs api
                configer = new Configer();
                configer.getConfigs(DeviceInfo.MAC, ChatApplication.releaseType, getApplicationContext(), new Configer.ConfigStatusListener() {
                    @Override
                    public void onStart() {
                        Log.d(TAG, "config fetch onStart");
                        uiHandler.sendEmptyMessage(CONFIG_FETCH_START);
                    }

                    @Override
                    public void onRetry() {
                        Log.d(TAG, "config fetch onRetry");
                        uiHandler.sendEmptyMessage(CONFIG_FETCH_RETRY);
                    }

                    @Override
                    public void onDeviceNoFound() {
                        Log.d(TAG, "config fetch onDeviceNoFound");
                        uiHandler.sendEmptyMessage(CONFIG_FETCH_NOAUTH);
                    }

                    @Override
                    public void onFail(String reason) {
                        Message message = Message.obtain();
                        message.what = CONFIG_FETCH_FAIL;
                        Bundle bundle = new Bundle();
                        bundle.putString("config_error", reason);
                        message.setData(bundle);
                        uiHandler.sendMessage(message);
                    }

                    @Override
                    public void onSuccess() {
                        Log.d(TAG, "config fetch onSuccess");
                        uiHandler.sendEmptyMessage(CONFIG_FETCH_SUCCESS);
                    }
                }, Configer.Type.service);
            }
        });
        configThread.start();


    }

    private void setCountryCode() {
        process = Process.CountryCode;
        ChatApplication.setUpWifiCountryCode();
        startConnectTimer(process);
    }


    private void connectWifi() {
        process = Process.WifiConnect;
        AielloWifiManager.setWifiEnable(getApplicationContext());
        DeviceInfo.MAC = AielloWifiManager.getWifiMacAddress();
        DeviceControl.setBluetoothName(DeviceInfo.MAC);
        DeviceControl.defaultBackLight();
        AielloWifiManager.checkDefaultWifi();
        AielloWifiManager.connectWithConfig();
        startConnectTimer(process);
        uiHandler.sendEmptyMessage(WIFI_CONNECTING);
    }


    public void startConnectTimer(Process process) {
        Log.d(TAG, "startConnectTimer");
        if (connectTimer != null) {
            connectTimer.cancel();
            connectTimer = null;
        }
        connectTimer = new CountDownTimer(connect_timeout * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int i = AielloWifiManager.iskWifiOnAndConnected();
                if (i != 2) {  //connect counter
                    if (process == Process.WifiConnect) {
                        Message message = Message.obtain();
                        message.what = WIFI_CONNECTING;
                        message.arg1 = (int) millisUntilFinished / 1000;
                        uiHandler.sendMessage(message);
                    } else {
                        uiHandler.sendEmptyMessage(WIFI_COUNTRY_CODE_SETTING);
                    }
                } else { //success
                    hideWifiConfigDialog();
                    if (process == Process.WifiConnect) {
                        uiHandler.sendEmptyMessage(WIFI_CONNECTED);
                        this.cancel();
                    } else {
                        uiHandler.sendEmptyMessage(WIFI_COUNTRY_CODE_CONNECTED);
                        this.cancel();
                    }
                }
            }

            @Override
            public void onFinish() {
                if (process == Process.WifiConnect) { //retry WifiConnect
                    uiHandler.sendEmptyMessage(WIFI_CONNECT_TIMEOUT);
                } else { //retry CountryCode
                    uiHandler.sendEmptyMessage(WIFI_COUNTRY_CODE_TIMEOUT);
                }
            }
        }.start();
    }


    private void deleteTmpFile() {
        try {
            File file = FileUtils.getFile("/sdcard/bluetooth/wifi_config.txt");
            boolean t = FileUtils.deleteQuietly(file);
            if (t) {
                Log.e(TAG, "deleteQuietly success");
            } else {
                Log.e(TAG, "deleteQuietly fail");
            }
        } catch (Exception ex) {
            Log.e(TAG, ex.toString());
        }
    }


    public void uploadVersion(String img_ver, String spot_ver, String push_ver, String music_ver, String sai_ver) {
        ChatApplication.worker.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    Log.d(TAG, "uploadVersion");
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("mac", DeviceInfo.MAC);
                    jsonObject.put("versionImage", img_ver);
                    jsonObject.put("versionAPK", spot_ver);
                    jsonObject.put("versionPushService", push_ver);
                    jsonObject.put("versionMusicService", music_ver);
                    jsonObject.put("versionSAIService", sai_ver);

                    String s = Http.put(APIConfig.WEBAPI_POST_VERSION.getUrl(), jsonObject, 5000, APIConfig.WEBAPI_POST_VERSION.getKey()).response;
                    JSONObject jObj = new JSONObject(s);
                    boolean isSucceeded = jObj.getBoolean("success");
                    Log.e(TAG, "uploadVersion success = " + isSucceeded);
                } catch (Exception e) {
                    Log.e(TAG, "uploadVersion error: " + e.toString());
                }
            }
        });
    }


    private AielloAlertDialog timeoutDialog;

    private void hideWifiConfigDialog() {
        if (timeoutDialog != null && timeoutDialog.isShowing()) {
            timeoutDialog.dismiss();
        }
    }


    private void showWifiConfigDialog() {
        startProcess(Process.WifiConnect);
        if (timeoutDialog == null || !timeoutDialog.isShowing()) {
            deleteTmpFile();
            DeviceControl.enableBT();
            BarcodeEncoder encoder = new BarcodeEncoder();
            try {
                Bitmap bitmap = encoder.encodeBitmap(DeviceInfo.MAC, BarcodeFormat.QR_CODE,
                        250, 250);

                timeoutDialog = new AielloAlertDialog(this, R.style.Theme_AppCompat_Dialog_Alert);
                timeoutDialog.setTextContent(getString(R.string.wifi_connect_timeout))
                        .setImageContent(bitmap)
                        .setOnConfirmListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                timeoutDialog.dismiss();
                                startProcess(Process.WifiConnect);
                            }
                        }).show();

            } catch (WriterException e) {
                Log.e(TAG, e.toString());
            }
        }
    }


    private void dismissConnectingDialog() {
        if (timeoutDialog != null && timeoutDialog.isShowing()) {
            timeoutDialog.dismiss();
        }
    }

    private void showAuthorizedDialog() {
        deleteTmpFile();
        DeviceControl.enableBT();
        BarcodeEncoder encoder = new BarcodeEncoder();
        try {
            Bitmap bitmap = encoder.encodeBitmap(DeviceInfo.MAC, BarcodeFormat.QR_CODE,
                    250, 250);

            AielloAlertDialog dialog = new AielloAlertDialog(this, R.style.Theme_AppCompat_Dialog_Alert);
            dialog.setTextContent(DeviceInfo.MAC + "\n" + getString(R.string.device_is_not_authorized))
                    .setImageContent(bitmap)
                    .setOnConfirmListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startProcess(Process.WifiConnect);
                            dialog.dismiss();
                        }
                    }).show();

        } catch (WriterException e) {
            Log.e(TAG, e.toString());
        }

    }


    private void showMainProgressBar(@Nullable int progress) {
        init_progress_bar.setVisibility(View.VISIBLE);
        tv_progress_bar_msg.setVisibility(View.VISIBLE);

        if (progress == -1) {
            return;
        }
        init_progress_bar.setProgress(progress);
        if (progress == 0) {
            tv_progress_bar_msg.setText("");
        } else {
            tv_progress_bar_msg.setText("" + progress);
        }
    }

    private void hideMainProgressBar() {
        init_progress_bar.setVisibility(View.INVISIBLE);
        tv_progress_bar_msg.setVisibility(View.INVISIBLE);
    }

    private void showSubProgressBar(@Nullable int progress) {
        init_progress_bar2.setVisibility(View.VISIBLE);
        tv_progress_bar2_msg.setVisibility(View.VISIBLE);

        if (progress == -1) {
            return;
        }
        init_progress_bar2.setProgress(progress);
        if (progress == 0) {
            tv_progress_bar2_msg.setText("");
        } else {
            tv_progress_bar2_msg.setText("" + progress);
        }
    }

    private void hideSubProgressBar() {
        init_progress_bar2.setVisibility(View.INVISIBLE);
        tv_progress_bar2_msg.setVisibility(View.INVISIBLE);
    }

}
