package ai.aiello.aiellospot.views.componets.homebtn.viewitems;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.ref.WeakReference;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.events.module.VoIPStateChange;
import ai.aiello.aiellospot.modules.voip.VoipModuleManager;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.activity.voip.VoipHomeActivity;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.HomeStateButton;

public class VoipViewItem extends AbstractViewItem {

    private static final String TAG = VoipViewItem.class.getSimpleName();
    public static Thread voipThread;
    UIHandler uiHandler;

    private static VoipViewItem instance;

    public static VoipViewItem getInstance() {
        if (instance == null) {
            instance = new VoipViewItem();
        }
        return instance;
    }

    private VoipViewItem() {}

    @Override
    public void updateView() {
        updateVoipStatus();
    }

    @Override
    public void handelBtnClickLogic() {
        wefBtn.get().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityLauncher.launchActivityByClass(wefActivity.get(), VoipHomeActivity.class, R.anim.fade_in, R.anim.fade_out);
            }
        });
    }

    @Override
    public void unbindView(Activity activity) {
       super.unbindView(activity);
        if (voipThread != null) {
            voipThread.interrupt();
            voipThread = null;
        }
        if (uiHandler != null) {
            uiHandler.removeCallbacksAndMessages(null);
            uiHandler = null;
        }
    }

    @Override
    public View buildButton(Context context) {
        HomeStateButton homeStateButton = new HomeStateButton(context, this.width, this.height);
        homeStateButton.updateImage(R.drawable.icon_home1_call);
        homeStateButton.updateText(R.string.call);
        homeStateButton.getStateImage().setVisibility(View.VISIBLE);
        return homeStateButton;
    }

    static class UIHandler extends Handler {

        Activity activity;

        UIHandler(WeakReference<Activity> reference) {
            activity = reference.get();
        }

        @Override
        public void handleMessage(Message msg) {
        }
    }

    public void updateVoipStatus() {
        Log.d(TAG, "updateVoipStatus: " + VoipModuleManager.getInstance().connStatus);
        if (VoipModuleManager.getInstance().connStatus == VoipModuleManager.ConnStatus.online) {
            if (VoipModuleManager.getInstance().currentState.getName() == VoipModuleManager.StateName.Disable) {
                ((HomeStateButton) this.wefBtn.get()).getStateImage().setImageDrawable(ChatApplication.context.getDrawable(R.drawable.ic_disable));
            } else {
                ((HomeStateButton) this.wefBtn.get()).getStateImage().setImageDrawable(ChatApplication.context.getDrawable(R.drawable.ic_online));
            }
        } else {
            ((HomeStateButton) this.wefBtn.get()).getStateImage().setImageDrawable(ChatApplication.context.getDrawable(R.drawable.ic_offline));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(VoIPStateChange event) {
        if (event.getStatus().equals(VoIPStateChange.VOIP_STATUS)) {
            updateVoipStatus();
        }
    }


}
