package ai.aiello.aiellospot.views.activity.clock;

import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.componets.toast.MToaster;
import ai.aiello.aiellospot.views.componets.hints.WakeupHintManager;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.modules.alarm.UserAlarmModuleManager;
import ai.aiello.aiellospot.intents.IntentManager;
import ai.aiello.aiellospot.core.info.DeviceInfo;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.utlis.Formatter;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.*;

import com.github.johnpersano.supertoasts.library.Style;
import com.tomer.fadingtextview.FadingTextView;
import com.zyyoona7.wheel.WheelView;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.BaseActivity;
import ai.aiello.aiellospot.views.activity.multi.MultiIntentActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;


public class ClockActivity extends BaseActivity {

    final String TAG = ClockActivity.class.getSimpleName();
    HorizontalScrollView map_scrollview;
    TextView wc_cityname, wc_msg, alarm_editor_title;
    ImageView tz_mask;
    LinearLayout hint_worldclock_box, alarm_empty;
    ListView alarm_listview;
    AlarmAdapter alarmAdapter;
    ArrayList<Date> alarmList;
    RelativeLayout fm_hook_alarm, fm_hook_editor;

    //editor
    ImageView alarm_cancel, alarm_confirm;

    //wheel test
    WheelView wheel_date, wheel_hour, wheel_minute;
    ArrayList<String> arrayListDate, arrayListHour, arrayListMinute;
    ArrayList<String> dateWheelList;
    Date activeDate = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        initEditor();
        updateTimezone();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateView();
    }

    private void initView() {
        setContentView(R.layout.activity_clock);
        map_scrollview = findViewById(R.id.map_scrollview);
        wc_cityname = findViewById(R.id.wc_cityname);
        wc_msg = findViewById(R.id.wc_msg);
        tz_mask = findViewById(R.id.tz_mask);
        alarm_editor_title = findViewById(R.id.alarm_editor_title);
        hint_worldclock_box = findViewById(R.id.hint_worldclock);
        alarm_listview = findViewById(R.id.alarm_list_view);
        alarm_empty = findViewById(R.id.alarm_empty);
        fm_hook_alarm = findViewById(R.id.fm_hook_alarm);

        //editor
        fm_hook_editor = findViewById(R.id.fm_hook_editor);
        alarm_cancel = findViewById(R.id.alarm_cancel);
        alarm_confirm = findViewById(R.id.alarm_confirm);

        //wheel
        wheel_date = findViewById(R.id.wheel_week);
        wheel_hour = findViewById(R.id.wheel_hour);
        wheel_minute = findViewById(R.id.wheel_minute);

        alarm_empty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEditor(Calendar.getInstance().getTime(), false);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        showHints();
    }

    @Override
    public void onHotReload(){
        showHints();
    }

    private void showHints() {
        FadingTextView tv_slide_text = findViewById(R.id.sl_hint_clock);
        String[] hints = new String[]{
                String.format(getResources().getString(R.string.hint_clock), DeviceInfo.assistantName),
                String.format(getResources().getString(R.string.hint_clock2), DeviceInfo.assistantName)};
        WakeupHintManager.getInstance().showHints(tv_slide_text, hints);
    }

    @Override
    protected void onStop() {
        WakeupHintManager.getInstance().cancel();
        super.onStop();
    }

    private void deleteClock(int position) {
        try {
            boolean response = UserAlarmModuleManager.getInstance().removeOneAlarm(alarmList.get(position), ClockActivity.this);
            if (response) {
                alarmList.remove(position);
                UserAlarmModuleManager.getInstance().saveToPreference(ClockActivity.this);
                SpotUserTraceLog3.getInstance().buildUIEventLog(
                        SpotUserTraceLog3.EventSubject.CLOCK,
                        SpotUserTraceLog3.EventAction.DELETE,
                        ""
                );
            }
            updateView();
        } catch (Exception e) {
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e(TAG, e.toString());
        }
    }

    private void initEditor() {

        alarm_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideEditor();
            }
        });

        alarm_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNewAlarm();
            }
        });

        //week
        Calendar c = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("EEE");
        arrayListDate = new ArrayList<>();
        dateWheelList = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            Date tmpDate = new Date();
            tmpDate.setHours(0);
            tmpDate.setMinutes(0);
            tmpDate.setSeconds(0);
            c.setTime(tmpDate);
            c.add(Calendar.DATE, i);
            dateWheelList.add(Formatter.ISO8601DATEFORMAT.format(c.getTime()));
            arrayListDate.add(String.format("%s ( %s / %s )",
                    dateFormat.format(c.getTime()),
                    Formatter.binaryFormat(c.get(Calendar.MONTH) + 1),
                    Formatter.binaryFormat(c.get(Calendar.DATE))));
        }
        wheel_date.setData(arrayListDate);
        wheel_date.setSelectedItemPosition(0);

        //hour
        arrayListHour = new ArrayList<>();
        for (int i = 0; i < 24; i++) {
            arrayListHour.add(Formatter.binaryFormat(i));
        }
        wheel_hour.setData(arrayListHour);
        wheel_hour.setSelectedItemPosition(0);

        //minute
        arrayListMinute = new ArrayList<>();
        for (int i = 0; i < 60; i++) {
            arrayListMinute.add(Formatter.binaryFormat(i));
        }
        wheel_minute.setData(arrayListMinute);
        wheel_minute.setSelectedItemPosition(0);

    }


    private void showEditor(Date date, boolean update) {
        if (update) {
            alarm_editor_title.setText(getString(R.string.alarm_updating));
        } else {
            alarm_editor_title.setText(getString(R.string.alarm_adding));
        }
        activeDate = date;
        Log.d(TAG, "showEditor : " + date.toString());


        Date purDate = (Date) date.clone();
        purDate.setHours(0);
        purDate.setMinutes(0);
        purDate.setSeconds(0);

        fm_hook_alarm.setVisibility(View.GONE);
        fm_hook_editor.setVisibility(View.VISIBLE);

        int dd = dateWheelList.indexOf(Formatter.ISO8601DATEFORMAT.format(purDate));
        int hh = arrayListHour.indexOf(Formatter.binaryFormat(date.getHours()));
        int mm = arrayListMinute.indexOf(Formatter.binaryFormat(date.getMinutes()));

        wheel_date.setSelectedItemPosition(dd);
        wheel_hour.setSelectedItemPosition(hh);
        wheel_minute.setSelectedItemPosition(mm);

    }

    private void hideEditor() {
        activeDate = null;
        fm_hook_alarm.setVisibility(View.VISIBLE);
        fm_hook_editor.setVisibility(View.GONE);
    }


    private void setNewAlarm() {
        if (activeDate != null) {
            UserAlarmModuleManager.getInstance().removeOneAlarm(activeDate, this);
        }
        int nd = wheel_date.getSelectedItemPosition();
        int nh = wheel_hour.getSelectedItemPosition();
        int nm = wheel_minute.getSelectedItemPosition();

        Date new_date = null;
        try {
            new_date = Formatter.ISO8601DATEFORMAT.parse(dateWheelList.get(nd));
            new_date.setHours(nh);
            new_date.setMinutes(nm);
            new_date.setSeconds(0);
            if (UserAlarmModuleManager.getInstance().addAlarm(new_date)) {
                UserAlarmModuleManager.getInstance().saveToPreference(this);
                SpotUserTraceLog3.getInstance().buildUIEventLog(
                        SpotUserTraceLog3.EventSubject.CLOCK,
                        SpotUserTraceLog3.EventAction.SETUP,
                        ""
                );
                updateView();
            } else {
                throw new Exception("add alarm fail");
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            MToaster.showButtonToast(this, getString(R.string.alarm_fail), Style.TYPE_STANDARD);
        }
    }

    public void updateView() {

        alarmList = UserAlarmModuleManager.getInstance().loadFromPreference(this);
        Collections.sort(alarmList);

        map_scrollview.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                updateTimezone();
            }
        });

        map_scrollview.post(new Runnable() {
            @Override
            public void run() {
                map_scrollview.scrollTo(944, 0);
            }
        });


        if (alarmList.size() != 0) {
            alarmAdapter = new AlarmAdapter(alarmList, this, UserAlarmModuleManager.getInstance().alarmLimit, new AlarmAdapter.OnItemClick() {
                @Override
                public void onDelete(int position) {
                    deleteClock(position);
                }

                @Override
                public void onBlockClick(int position) {
                    if (position < alarmList.size()) {
                        showEditor(alarmList.get(position), true);
                    } else {
                        showEditor(Calendar.getInstance().getTime(), false);
                    }
                }
            });
            alarm_listview.setAdapter(alarmAdapter);
            alarm_empty.setVisibility(View.GONE);
            alarm_listview.setVisibility(View.VISIBLE);
        } else {
            alarm_empty.setVisibility(View.VISIBLE);
            alarm_listview.setVisibility(View.GONE);
        }

        hideEditor();

    }


    public void clockBack(View view) {
        if (ActivityLauncher.isFromMultiActivity() && IntentManager.finishIntentList.size() != 0) {
            ActivityLauncher.launchActivityByClass(this, MultiIntentActivity.class, R.anim.fade_in, R.anim.fade_out);
        } else {
            ActivityLauncher.launchActivityByClass(this, ActivityLauncher.currentHomeName, R.anim.fade_in, R.anim.fade_out);
        }
    }


    public void updateTimezone() {

        int scrollY = map_scrollview.getScrollY();
        int scrollX = map_scrollview.getScrollX();


        try {

            Resources res = getResources();
            String[] city = res.getStringArray(R.array.cities);
            int[] city_coord = res.getIntArray(R.array.cities_coordinate);
            String[] city_gmt = res.getStringArray(R.array.cities_gmt);

            hint_worldclock_box.setVisibility(View.INVISIBLE);
            for (int i = 0; i < city_coord.length; i++) {

                int s = city_coord[i];

                if (scrollX > s - 10 && scrollX < s + 10) {

                    hint_worldclock_box.setVisibility(View.VISIBLE);
                    Calendar ncal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
                    ncal.setTimeZone(TimeZone.getTimeZone(city_gmt[i]));

                    int month = ncal.get(Calendar.MONTH);
                    int date = ncal.get(Calendar.DATE);
                    int hour = ncal.get(Calendar.HOUR_OF_DAY);
                    int min = ncal.get(Calendar.MINUTE);

                    wc_msg.setText(String.format("%s/%s  %s:%s", Formatter.binaryFormat(month + 1), Formatter.binaryFormat(date), Formatter.binaryFormat(hour), Formatter.binaryFormat(min)));
                    wc_cityname.setText(city[i]);

                }
            }

        } catch (Exception e) {
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e(TAG, "Timezone error = " + e.toString());
        }

    }

}
