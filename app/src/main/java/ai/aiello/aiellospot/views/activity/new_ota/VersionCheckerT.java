package ai.aiello.aiellospot.views.activity.new_ota;

import ai.aiello.aiellospot.ChatApplication;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;

import com.litesuits.android.log.Log;

import java.util.LinkedHashMap;

public class VersionCheckerT {

    public static LinkedHashMap<String, ServiceAPK> serviceApkMap = new LinkedHashMap<>();

    public static final String PUSH_SERVICE_NAME = "com.aiello.pushservicebackground";
    public static final String MUSIC_SERVICE_NAME = "ai.aiello.musicservice";
    public static final String SAI_SERVICE_NAME = "ai.aiello.soundaiservice";
    public static final String SPOT_NAME = "ai.aiello.aiellospot";

    public static String img_ver = "";
    public static String apk_ver = "";
    public static String music_ver = "";
    public static String push_ver = "";
    public static String sai_ver = "";

    private static final String TAG = VersionCheckerT.class.getSimpleName();

    public static String getSystemImageVersion() {
        String img_ver = Build.VERSION.INCREMENTAL;
        Log.d(TAG, "img_ver = " + img_ver);
        return img_ver;
    }

    public static String getLocalVersion(Context context, String packageName) {
        ServiceAPK serviceAPK = VersionCheckerT.serviceApkMap.get(packageName);
        PackageManager packageManager = context.getPackageManager();
        PackageInfo packageInfo;
        String localVersion = "";
        try {
            packageInfo = packageManager.getPackageInfo(serviceAPK.getPackageName(), 0);
            localVersion = packageInfo.versionName;
            Log.d(TAG, serviceAPK.getName() + ": localVersion = " + packageInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, e.toString());
        }
        return localVersion;
    }

    static boolean isLatestVersion(Context context, ServiceAPK serviceAPK) throws Exception {
        //local version
        String localVersion = getLocalVersion(context, serviceAPK.getPackageName());
        serviceAPK.setLocalVersion(localVersion);

        //remote version
        String remoteVersion = "";
        if (ChatApplication.releaseType == 0) {
            remoteVersion = serviceAPK.getReleaseVersion();
        } else {
            remoteVersion = serviceAPK.getTestVersion();
        }
        Log.d(TAG, String.format("localVersion = %s, remoteVersion = %s", serviceAPK.getLocalVersion(), remoteVersion));

        //compare version
        boolean isLatest = false;
        if (remoteVersion.equals(serviceAPK.getLocalVersion())) {
            isLatest = true;
        } else {
            if (ChatApplication.releaseType == 0) {
                serviceAPK.setDownloadUrl(serviceAPK.getReleaseVersionUrl());
            } else {
                serviceAPK.setDownloadUrl(serviceAPK.getTestVersionUrl());
            }
            isLatest = false;
        }
        Log.d(TAG, serviceAPK.getPackageName() + " is latest = " + isLatest);
        return isLatest;
    }

}
