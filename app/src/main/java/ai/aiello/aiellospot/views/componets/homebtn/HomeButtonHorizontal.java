package ai.aiello.aiellospot.views.componets.homebtn;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import androidx.constraintlayout.widget.ConstraintLayout;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;

public class HomeButtonHorizontal extends AbstractHomeButton {

    ConstraintLayout.LayoutParams iconViewParams;
    LinearLayout.LayoutParams titleViewParams;


    public HomeButtonHorizontal(Context context) {
        super(context);
        View btn = LayoutInflater.from(context).inflate(R.layout.home_button_horizontal, null);
        this.imageView = btn.findViewById(R.id.iv_home_icon);
        this.textView = btn.findViewById(R.id.tv_home_title);
        this.badge = btn.findViewById(R.id.badge);
        this.asrStateImage = btn.findViewById(R.id.img_asrStatus);
        this.btn = btn;

        this.addView(btn);

        initParams(btn);
    }

    //icon logic
    public void setIcon(int drawableRId, Context context) {
        this.imageView.setImageDrawable(context.getDrawable(drawableRId));
    }

    public void setIconSize(int width, int height) {
        iconViewParams.width = width;
        iconViewParams.height = height;
        this.imageView.setLayoutParams(iconViewParams);
    }

    //title logic
    public void setTitle(int stringRId, Context context) {
        this.textView.setText(context.getString(stringRId));
    }

    public void setTitleTextSize(float textSize) {
        this.textView.setTextSize(textSize);
    }

    public void initParams(View btn) {
        iconViewParams = (ConstraintLayout.LayoutParams) this.imageView.getLayoutParams();
        titleViewParams = (LinearLayout.LayoutParams) this.textView.getLayoutParams();

        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) btn.getLayoutParams();
        params.width = ConstraintLayout.LayoutParams.MATCH_PARENT;
        params.height = ConstraintLayout.LayoutParams.MATCH_PARENT;
        btn.setLayoutParams(params);
    }

    @Override
    public void updateImage(int res){
        imageView.setImageDrawable(ChatApplication.context.getDrawable(res));
    }

    @Override
    public void updateText(int res){
        textView.setText(ChatApplication.context.getString(res));
    }

}
