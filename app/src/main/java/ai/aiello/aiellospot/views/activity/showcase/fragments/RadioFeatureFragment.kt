package ai.aiello.aiellospot.views.activity.showcase.fragments

import ai.aiello.aiellospot.ChatApplication
import ai.aiello.aiellospot.R
import ai.aiello.aiellospot.databinding.FragmentHotelFeatureRadioBinding
import ai.aiello.aiellospot.modules.showcase.ShowcaseManager
import ai.aiello.aiellospot.modules.showcase.radio.HotelMusicManager
import ai.aiello.aiellospot.views.activity.BaseFragment
import ai.aiello.aiellospot.views.activity.showcase.beans.RadioAlbumData
import ai.aiello.aiellospot.views.activity.showcase.beans.RadioCardViewBean
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/* 2021.03.24
Observing HotelMusicManager to play Hotel Feature Music

A. Play condition:
    1. MusicIntent: N/A, not decided yet
    1. This fragment: a.Selecting Music List b.Dragging SeekBar c.Play button

B. Pause condition:
    1. KKBOX MusicServiceManager a.playNext(), b.playPrev(), c.playAlbum(), d.resume(this will not be reach since playAlbum always come first)
    2. VOIP: a.pauseMusic()
    3. MusicIntent: a.PauseMusic()
    4. This Fragment: Pause button

C. Resume condition:
    1. VOIP: a.resumeMusic(KKBOX has higher priority)
    2. MusicIntent: a.resumeMusic(KKBOX only)

D. Shutdown Service:
    1. ChatApplication: releaseResource
    2. Hotel Music done playing last song
 */


class RadioFeatureFragment(val unikey: String? = null) : BaseFragment() {

    private val TAG: String = RadioFeatureFragment::class.java.simpleName
    lateinit var binding: FragmentHotelFeatureRadioBinding
    private val albumList: List<RadioAlbumData>? = (ShowcaseManager.getInstance().showcaseData.get(unikey) as RadioCardViewBean).radioAlbums
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var adapter: RadioFeatureAdapter

    companion object {
        var intent: Intent? = null
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        // setup layout and its recyclerview with decorator
        binding = FragmentHotelFeatureRadioBinding.inflate(inflater, container, false)
        linearLayoutManager = LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerView.layoutManager = linearLayoutManager
        binding.recyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(
                outRect: Rect,
                view: View,
                parent: RecyclerView,
                state: RecyclerView.State
            ) {
                super.getItemOffsets(outRect, view, parent, state)
                outRect.right = 16
            }
        })


    //// Music Control
        // setup adapter and onClick function
        adapter = RadioFeatureAdapter(RadioFeatureAdapter.OnClickListener { item, position ->
            HotelMusicManager.playMusic(position)
        }, ChatApplication.context)

        // Change music progress by seekBar
        binding.seekBarHotelMusic.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener {

            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {}

            override fun onStartTrackingTouch(p0: SeekBar?) {
                if (p0 != null) {
                    HotelMusicManager.jumpToProgress(p0.progress)
                }
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
                if (p0 != null) {
                    HotelMusicManager.jumpToProgress(p0.progress)
                }
            }
        })

        // setup music control panel
        binding.imageHotelRadioPanelBtn.setOnClickListener {
            HotelMusicManager.pauseOrPlay()
        }

    //// UI update
        // Update track recyclerview color on play
        HotelMusicManager.trackIndex.observe(viewLifecycleOwner, Observer {
            adapter.notifyDataSetChanged()
            if (it != -1) linearLayoutManager.scrollToPositionWithOffset(it,0)
        })

        // SeekBar progress update
        HotelMusicManager.trackProgress.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                binding.seekBarHotelMusic.setProgress(it, false)
            }
        })

        // Music player play/pause bottom icon
        HotelMusicManager.isPlaying.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.imageHotelRadioPanelBtn.setImageResource(R.drawable.icon_music_pause)
            } else {
                binding.imageHotelRadioPanelBtn.setImageResource(R.drawable.icon_music_play)
            }
        })

        // Render UI when album selected or changed
        HotelMusicManager.albumIndex.observe(viewLifecycleOwner, Observer {
            if (HotelMusicManager.albumIndex.value!! != -1) {
                updateAlbumInfoAndTracks(HotelMusicManager.albumIndex.value!!) // Render UI
            }
        })

        // Init MusicManager Tracks
        if (HotelMusicManager.albumIndex.value == -1) {
            HotelMusicManager.setAlbum(0) // set Album on MusicManager
            HotelMusicManager.initPlayerAndMediaSource() //has to go before selectAlbumIndex
        }

        binding.recyclerView.adapter = adapter

        return binding.root
    }

    private fun updateAlbumInfoAndTracks(albumIndex: Int) {
        val defaultAlbum = this.albumList!![albumIndex]

        binding.imageHotelRadioCover.setImageBitmap(defaultAlbum.image)
        binding.tvHotelRadioTitle.text = defaultAlbum.getTitle(ChatApplication.system_lang)
        binding.tvHotelRadioContent.text = defaultAlbum.getContent(ChatApplication.system_lang)
        adapter.submitList(defaultAlbum.tracks)

    }

}