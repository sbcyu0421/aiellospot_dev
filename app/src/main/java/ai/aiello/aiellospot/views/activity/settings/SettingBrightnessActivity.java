package ai.aiello.aiellospot.views.activity.settings;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.BaseActivity;
import ai.aiello.aiellospot.core.device.DeviceControl;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.willy.ratingbar.BaseRatingBar;
import com.willy.ratingbar.ScaleRatingBar;

public class SettingBrightnessActivity extends BaseActivity implements View.OnClickListener, BaseRatingBar.OnRatingChangeListener {

    private LinearLayout ly_back, btn_backlight_off;
    private ScaleRatingBar ratingBar_light;
    private TextView tv_light;
    private String TAG = SettingBrightnessActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_light);
        initView();
    }

    private void initView() {
        ly_back = findViewById(R.id.ly_back);
        btn_backlight_off = findViewById(R.id.btn_backlight_off);
        ly_back.setOnClickListener(this);
        ratingBar_light = findViewById(R.id.ratingBar_light);
        tv_light = findViewById(R.id.tv_light);

        int s = DeviceControl.getBrightness() / 10;
        ratingBar_light.setRating(s);
        tv_light.setText(String.valueOf(s));
        ratingBar_light.setOnRatingChangeListener(this);

        btn_backlight_off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DeviceControl.backLightOff();

                SpotUserTraceLog3.getInstance().buildUIEventLog(
                        SpotUserTraceLog3.EventSubject.SCREEN,
                        SpotUserTraceLog3.EventAction.OFF,
                        ""
                );
            }
        });

    }


    @Override
    public void onRatingChange(BaseRatingBar ratingBar, float rating, boolean fromUser) {
        int set = Math.round(rating);
        if (set != 0) {
            tv_light.setText(String.valueOf(set));
            DeviceControl.setBrightness(set * 10);
            SpotUserTraceLog3.getInstance().buildUIEventLog(
                    SpotUserTraceLog3.EventSubject.SCREEN,
                    SpotUserTraceLog3.EventAction.SET_VALUE,
                    String.valueOf((set * 10))
            );
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ly_back:
                Intent intent2 = new Intent(SettingBrightnessActivity.this, SettingsActivity.class);
                startActivity(intent2);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
                break;
        }
    }

}
