package ai.aiello.aiellospot.views.activity.news;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.core.device.DeviceControl;
import ai.aiello.aiellospot.events.system.SystemEvent;
import ai.aiello.aiellospot.views.activity.CardActivity;
import ai.aiello.aiellospot.views.activity.multi.MultiIntentActivity;
import ai.aiello.aiellospot.intents.IntentManager;
import ai.aiello.aiellospot.intents.sdui.ActionManager;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.core.MCountdownTimer;
import ai.aiello.aiellospot.core.media.MsttsManager;
import ai.aiello.aiellospot.modules.music.MusicServiceModuleManager;

public class NewsActivity extends CardActivity {


    private TextView tv_news_title, tv_news_msg;
    private LinearLayout btn_ok;
    private ScrollView scrollView_msg;
    private String rooboTitle;
    private String rooboMsg;
    public int duration = 10 * 60;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_roobo);

        tv_news_title = findViewById(R.id.tv_roobo_title);
        tv_news_msg = findViewById(R.id.tv_roobo_msg);
        scrollView_msg = findViewById(R.id.scrollView_msg);
        btn_ok = findViewById(R.id.btn_ok);

        rooboTitle = intentObject.getMsg1();
        rooboMsg = intentObject.getMsg2();
        try {
            duration = Integer.parseInt(intentObject.getMsg3());
        } catch (Exception e) {
            e.printStackTrace();
        }
        int ml = rooboMsg.length();

        if (ml <= 17) {
            tv_news_msg.setHeight(180);
            scrollView_msg.getLayoutParams().height = 180;
        } else if (ml <= 34) {
            tv_news_msg.setHeight(220);
            scrollView_msg.getLayoutParams().height = 220;
        } else if (ml <= 51) {
            tv_news_msg.setHeight(260);
            scrollView_msg.getLayoutParams().height = 260;
        } else {
            scrollView_msg.getLayoutParams().height = 260;
        }

        tv_news_title.setText(rooboTitle);
        tv_news_msg.setText(rooboMsg);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmAndGoBack();

            }
        });


    }
    @Override
    protected void onResume() {
        super.onResume();
        ActivityLauncher.pushToTrace(this, ActivityLauncher.CARD_ACTIVITY);
        MCountdownTimer.startDismissTimer(duration);
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            MCountdownTimer.startDismissTimer(duration);
            return true;
        } else {
            return false;
        }
    }
    @Override
    public void confirmAndGoBack() {
        if (ActivityLauncher.isFromMultiActivity() && IntentManager.finishIntentList.size() != 0) {
            ActivityLauncher.launchActivityByClass(this, MultiIntentActivity.class, R.anim.fade_in, R.anim.fade_out);
        } else if (ActivityLauncher.isFromStandbyActivity()) {
            ActivityLauncher.launchActivityByClass(this, ActivityLauncher.currentHomeName, R.anim.fade_in, R.anim.fade_out);
            ActionManager.getInstance().stopForegroundAction();
            MsttsManager.getInstance().stopTTS();
            DeviceControl.volumeResume();
        } else {
            ActivityLauncher.launchActivityByName(this, ActivityLauncher.getBaseActivity(), R.anim.fade_in, R.anim.fade_out);
            ActionManager.getInstance().stopForegroundAction();
            MsttsManager.getInstance().stopTTS();
            DeviceControl.volumeResume();
        }

    }


}
