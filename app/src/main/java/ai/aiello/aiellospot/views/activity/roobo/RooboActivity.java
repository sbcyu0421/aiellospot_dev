package ai.aiello.aiellospot.views.activity.roobo;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.CardActivity;

public class RooboActivity extends CardActivity {


    private TextView tv_roobo_title, tv_roobo_msg;
    private LinearLayout btn_ok;
    private ScrollView scrollView_msg;
    private String rooboTitle;
    private String rooboMsg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_roobo);

        tv_roobo_title = findViewById(R.id.tv_roobo_title);
        tv_roobo_msg = findViewById(R.id.tv_roobo_msg);
        scrollView_msg = findViewById(R.id.scrollView_msg);
        btn_ok = findViewById(R.id.btn_ok);

        rooboTitle = intentObject.getMsg1();
        rooboMsg = intentObject.getMsg2();

        int ml = rooboMsg.length();

        if (ml <= 17) {
            tv_roobo_msg.setHeight(180);
            scrollView_msg.getLayoutParams().height = 180;
        } else if (ml <= 34) {
            tv_roobo_msg.setHeight(220);
            scrollView_msg.getLayoutParams().height = 220;
        } else if (ml <= 51) {
            tv_roobo_msg.setHeight(260);
            scrollView_msg.getLayoutParams().height = 260;
        } else {
            scrollView_msg.getLayoutParams().height = 260;
        }

        tv_roobo_title.setText(rooboTitle);
        tv_roobo_msg.setText(rooboMsg);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmAndGoBack();
            }
        });

    }


}
