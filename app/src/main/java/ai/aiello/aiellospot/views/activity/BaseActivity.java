package ai.aiello.aiellospot.views.activity;

import android.app.Activity;
import android.os.Bundle;

import java.lang.ref.WeakReference;

/**
 * Created by a1990 on 2019/3/13.
 */

public abstract class BaseActivity extends MyActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ActivityLauncher.pushToTrace(new WeakReference<Activity>(this).get(), ActivityLauncher.BASE_ACTIVITY);
    }


}
