package ai.aiello.aiellospot.views.activity.oobe;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import com.github.johnpersano.supertoasts.library.Style;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Locale;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.events.system.SystemEvent;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.componets.toast.MToaster;
import ai.aiello.aiellospot.views.activity.home.MainHomeActivity;
import ai.aiello.aiellospot.views.activity.dialog.AielloProgressDialog;
import ai.aiello.aiellospot.core.device.DeviceControl;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.core.info.UserInfo;
import ai.aiello.aiellospot.core.soundai.SoundAIManager;
import ai.aiello.aiellospot.core.system.BackLightSaveManager;
import ai.aiello.aiellospot.events.module.CheckInOutStateChange;

public class LocaleActivity extends Activity {

    private AielloProgressDialog aielloProgressDialog = null;
    private TextView tv_ct, tv_cn, tv_en, tv_jp;
    private ChatApplication app;
    private ImageView img_ready, img_smoothleft, img_smoothright;
    private TextView tv_welcome0, tv_welcome1, tv_welcome2;
    private HorizontalScrollView hscrollView;
    private static String TAG = LocaleActivity.class.getSimpleName();
    private boolean skipTutor = true;
    private ArrayList<LocaleButton> buttonList = new ArrayList<>();
    private LinearLayout buttonHolderView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);
        app = (ChatApplication) getApplication();
        hideActivityController();
        initView();
        updateView();

        //init user and restore info if need
        boolean restored = UserInfo.restoreStage();
        if (restored) {
            ChatApplication.system_lang = UserInfo.curLang;
            loadingSystem(UserInfo.curActivity, -1); //go to original activity
        }

        //start push service, and wait for checkIn event
        app.startPowerListener();
        app.initSocketManager();
        app.initMessageService(this);

        //init status and push to db
        BackLightSaveManager.getInstance().register(LocaleActivity.this.getApplicationContext(), false);
    }


    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public void initView() {
        //dialog
        aielloProgressDialog = new AielloProgressDialog(new WeakReference<Activity>(this), R.style.CustomProgressDialog);
        aielloProgressDialog.setText(getResources().getString(R.string.initialing_1));
        renderLocaleButton();
    }

    private void renderLocaleButton() {
        buttonList.clear();
        buttonHolderView = findViewById(R.id.ly_btn_holder);
        buttonHolderView.removeAllViews();
        // init buttons and their functions
        for (ChatApplication.Language language: ChatApplication.supportLocaleList) {
            String title = "";
            switch (language) {
                case zh_TW:
                    title = "繁體中文";
                    break;
                case zh_CN:
                    title = "简体中文";
                    break;
                case japanese:
                    title = "日本語";
                    break;
                case en_US:
                    title = "English";
                    break;
            }
            LocaleButton button = new LocaleButton(this, title, language);
            buttonHolderView.addView(button);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) button.getLayoutParams();
            params.width = (int) convertDpToPx(190, this);
            params.height = LinearLayout.LayoutParams.WRAP_CONTENT;
            params.setMargins((int) convertDpToPx(6, this), (int) convertDpToPx(6, this), (int) convertDpToPx(6, this), (int) convertDpToPx(6, this));
            button.setPadding((int) convertDpToPx(10, this), (int) convertDpToPx(10, this), (int) convertDpToPx(10, this), (int) convertDpToPx(10, this));
            button.setLayoutParams(params);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ChatApplication.system_lang = button.buttonLanguage;
                    switch (button.buttonLanguage) {
                        case zh_TW:
                            ChatApplication.system_lang = ChatApplication.Language.zh_TW;
                            ChatApplication.locale = Locale.TAIWAN;
                            break;
                        case zh_CN:
                            ChatApplication.system_lang = ChatApplication.Language.zh_CN;
                            ChatApplication.locale = Locale.CHINA;
                            break;
                        case japanese:
                            ChatApplication.system_lang = ChatApplication.Language.japanese;
                            ChatApplication.locale = Locale.JAPANESE;
                            break;
                        case en_US:
                            ChatApplication.system_lang = ChatApplication.Language.en_US;
                            ChatApplication.locale = Locale.ENGLISH;
                            break;
                    }
                    updateView();
                }
            });

            buttonList.add(button);
        }
    }

    private void login() {
        if (ChatApplication.locale != null) {
            loadingSystem(null, 1);
        } else {
            MToaster.showButtonToast(LocaleActivity.this, getString(R.string.locale_again), Style.TYPE_STANDARD);
        }
    }

    public void updateView() {

        img_smoothleft = findViewById(R.id.img_smoothleft);
        img_smoothright = findViewById(R.id.img_smoothright);
        tv_welcome0 = findViewById(R.id.tv_welcome0);
        tv_welcome1 = findViewById(R.id.tv_welcome1);
        tv_welcome2 = findViewById(R.id.tv_welcome2);
        hscrollView = findViewById(R.id.hscrollView);

        boolean getUser = !UserInfo.userName.equals("default_user_name");

        if (getUser) {
            tv_welcome0.setText(String.format(DeviceInfo.hotelWelcome0_tw, UserInfo.userName));
        } else {
            tv_welcome0.setText(String.format(DeviceInfo.hotelWelcome0_tw, "來賓"));
        }
        tv_welcome1.setText(DeviceInfo.hotelWelcome1_tw);
        tv_welcome2.setText(DeviceInfo.hotelWelcome2_tw);

        if (ChatApplication.system_lang == ChatApplication.Language.None)
            return;

        // update image of check and uncheck for each button
        for (LocaleButton button: buttonList) {
            updateButtonBackground(button, ChatApplication.system_lang);
        }

        switch (ChatApplication.system_lang) {
            case japanese:
                if (getUser) {
                    tv_welcome0.setText(String.format(DeviceInfo.hotelWelcome0_en, UserInfo.userName));
                } else {
                    tv_welcome0.setText(String.format(DeviceInfo.hotelWelcome0_en, "Guest"));
                }
                tv_welcome1.setText(DeviceInfo.hotelWelcome1_en);
                tv_welcome2.setText(DeviceInfo.hotelWelcome2_en);
                break;

            case en_US:
                if (getUser) {
                    tv_welcome0.setText(String.format(DeviceInfo.hotelWelcome0_en, UserInfo.userName));
                } else {
                    tv_welcome0.setText(String.format(DeviceInfo.hotelWelcome0_en, "Guest"));
                }
                tv_welcome1.setText(DeviceInfo.hotelWelcome1_en);
                tv_welcome2.setText(DeviceInfo.hotelWelcome2_en);
                break;


            case zh_TW:
                if (getUser) {
                    tv_welcome0.setText(String.format(DeviceInfo.hotelWelcome0_tw, UserInfo.userName));
                } else {
                    tv_welcome0.setText(String.format(DeviceInfo.hotelWelcome0_tw, "來賓"));
                }
                tv_welcome1.setText(DeviceInfo.hotelWelcome1_tw);
                tv_welcome2.setText(DeviceInfo.hotelWelcome2_tw);
                break;

            case zh_CN:
                if (getUser) {
                    tv_welcome0.setText(String.format(DeviceInfo.hotelWelcome0_cn, UserInfo.userName));
                } else {
                    tv_welcome0.setText(String.format(DeviceInfo.hotelWelcome0_cn, "来宾"));
                }
                tv_welcome1.setText(DeviceInfo.hotelWelcome1_cn);
                tv_welcome2.setText(DeviceInfo.hotelWelcome2_cn);
                break;
        }
        login();

    }

    private void updateButtonBackground(LocaleButton button, ChatApplication.Language language) {
        if (language == button.buttonLanguage) {
            button.setBackgroundResource(R.drawable.btn_locale_enable);
        } else {
            button.setBackgroundResource(R.drawable.btn_locale_disable);
        }
    }

    private void autoLoginWithOneLocaleAvailable() {
        switch (ChatApplication.supportLocaleList.get(0)) {
            case zh_TW:
                ChatApplication.system_lang = ChatApplication.Language.zh_TW;
                ChatApplication.locale = Locale.TAIWAN;
                break;
            case zh_CN:
                ChatApplication.system_lang = ChatApplication.Language.zh_CN;
                ChatApplication.locale = Locale.CHINA;
                break;
            case japanese:
                ChatApplication.system_lang = ChatApplication.Language.japanese;
                ChatApplication.locale = Locale.JAPANESE;
                break;
            case en_US:
                ChatApplication.system_lang = ChatApplication.Language.en_US;
                ChatApplication.locale = Locale.ENGLISH;
                break;
        }
        updateView();
        login();
    }


    private SetupTask setupTask = null;

    public void loadingSystem(@Nullable String aclass, int from) {
        if (setupTask != null) return;
        setupTask = new SetupTask(this, aclass, from);
        setupTask.execute();
    }

    private class SetupTask extends AsyncTask<Void, Void, Void> {
        WeakReference<LocaleActivity> activityReference;
        String aclass;
        int from;

        SetupTask(LocaleActivity activity, String aclass, int from) {
            this.activityReference = new WeakReference<>(activity);
            this.aclass = aclass;
            this.from = from;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            activityReference.get().aielloProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            activityReference.get().app.initSPOTSystem();
            SoundAIManager.setWakeUp(activityReference.get(), true);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //            activityReference.get().aielloProgressDialog.dismiss();
            BackLightSaveManager.getInstance().register(LocaleActivity.this.getApplicationContext(), true);
            if (from == -1) {
                ActivityLauncher.launchActivityByName(activityReference.get(), aclass, R.anim.fade_in, R.anim.fade_out);
            } else {
                Intent intent = null;
                if (skipTutor) {
                    intent = new Intent(LocaleActivity.this, MainHomeActivity.class);
                    DeviceControl.defaultVol();
                } else {
                    intent = new Intent(LocaleActivity.this, TutorActivity.class);
                }
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (aielloProgressDialog != null && aielloProgressDialog.isShowing()) {
            aielloProgressDialog.dismiss();
        }
    }

    public void hideActivityController() {
        //hideNavController
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(CheckInOutStateChange event) {
        try {
            switch (event.getTarget()) {
                case ENTRY_HOME_PAGE:
                    String checkInLanguage = UserInfo.userLanguage;
                    String checkInName = UserInfo.userName;
                    if (!checkInLanguage.equals("default_user_language")) {
                        switch (checkInLanguage) {
                            case "en":
                                if (!ChatApplication.supportLocaleList.contains(ChatApplication.Language.en_US)) {
                                    Log.d(TAG, "CheckInOutEvent block: " + checkInLanguage + " not support");
                                    return;
                                }
                                ChatApplication.system_lang = ChatApplication.Language.en_US;
                                ChatApplication.locale = Locale.ENGLISH;
                                break;

                            case "zh-CN":
                                if (!ChatApplication.supportLocaleList.contains(ChatApplication.Language.zh_CN)) {
                                    Log.d(TAG, "CheckInOutEvent block: " + checkInLanguage + " not support");
                                    return;
                                }
                                ChatApplication.system_lang = ChatApplication.Language.zh_CN;
                                ChatApplication.locale = Locale.CHINA;
                                break;

                            case "ja":
                                if (!ChatApplication.supportLocaleList.contains(ChatApplication.Language.japanese)) {
                                    Log.d(TAG, "CheckInOutEvent block: " + checkInLanguage + " not support");
                                    return;
                                }
                                ChatApplication.system_lang = ChatApplication.Language.japanese;
                                ChatApplication.locale = Locale.JAPANESE;
                                hscrollView.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        hscrollView.smoothScrollTo(2000, 0);
                                    }
                                });
                                break;

                            case "zh-TW":
                                if (!ChatApplication.supportLocaleList.contains(ChatApplication.Language.zh_TW)) {
                                    Log.d(TAG, "CheckInOutEvent block: " + checkInLanguage + " not support");
                                    return;
                                }
                                ChatApplication.system_lang = ChatApplication.Language.zh_TW;
                                ChatApplication.locale = Locale.TAIWAN;
                                break;

                            default:
                                ChatApplication.system_lang = ChatApplication.Language.zh_TW;
                                ChatApplication.locale = Locale.TAIWAN;
                                break;
                        }
                    }

                    if (!checkInName.equals("default_user_name")) {
                        updateView();
                    }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


//
//    //註冊重啟服務
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onMessageEvent(RestartEvent event) {
//        int action = event.getAction();
//        switch (action) {
//            case PushServiceConn.RX_APK_REBOOT:
//                SystemControl.rebootAPK(this);
//                break;
//            case PushServiceConn.RX_DEVICE_REBOOT:
//                SystemControl.rebootDevice();
//                break;
//            case PushServiceConn.RX_SILENT_RESTART:
//                SystemControl.restartSilently(this);
//                break;
//        }
//
//    }

//    @Subscribe(threadMode = ThreadMode.ASYNC)
//    public void onMessageEvent(AlarmState event) {
//        Log.d(TAG, "onMessageEvent: " + event.getEvent());
//        if (event.getEvent().equals(M_AlarmEvent.BACK_LIGHT_ON)) {
//            DeviceControl.defaultBackLight();
//            BackLightSaveManager.getInstance().register(LocaleActivity.this.getApplicationContext(), false);
//        } else if (event.getEvent().equals(M_AlarmEvent.BACK_LIGHT_OFF)) {
//            DeviceControl.setBrightness(1);
//            BackLightSaveManager.getInstance().register(LocaleActivity.this.getApplicationContext(), false);
//        }
//    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        DeviceControl.defaultBackLight();
        return super.dispatchTouchEvent(event);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        DeviceControl.defaultBackLight();
        return true;
    }

    private static class LocaleButton extends AppCompatTextView {

        ChatApplication.Language buttonLanguage;

        public LocaleButton(Context context, String text, ChatApplication.Language language) {
            super(context);
            buttonLanguage = language;
            this.setBackground(context.getDrawable(R.drawable.btn_locale_disable));
            this.setTextSize(42);
            this.setTextAlignment(TEXT_ALIGNMENT_CENTER);
            this.setText(text);
        }
    }

    private static float convertDpToPx(float dp, Context context) {
        return dp * context.getResources().getDisplayMetrics().density;
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(SystemEvent event) {
        switch (event.getType()) {
            case BUSY_STATE_HOT_RELOAD:
            case IDLE_STATE_HOT_RELOAD:
                renderLocaleButton();
                break;
        }
    }

}

