package ai.aiello.aiellospot.views.componets.homebtn.viewitems;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.johnpersano.supertoasts.library.Style;

import org.json.JSONException;

import java.lang.ref.WeakReference;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.modules.showcase.ShowcaseManager;
import ai.aiello.aiellospot.modules.surrounding.SurroundingModuleManager;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.activity.clock.ClockActivity;
import ai.aiello.aiellospot.views.activity.fun.SurroundingActivity;
import ai.aiello.aiellospot.views.activity.showcase.ShowcaseActivity;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.HomeHeroButton;
import ai.aiello.aiellospot.views.componets.toast.MToaster;

public class ShowCaseViewItem extends AbstractViewItem {

    private static ShowCaseViewItem instance;

    private static String TAG = ShowCaseViewItem.class.getSimpleName();

    private ExecutorService showcaseWorker = Executors.newSingleThreadExecutor();

    public static ShowCaseViewItem getInstance() {
        if (instance == null) {
            instance = new ShowCaseViewItem();
        }
        return instance;
    }

    private ShowCaseViewItem() {}


    @Override
    public void handelBtnClickLogic() {
        wefBtn.get().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ShowcaseManager.getInstance().showcaseData.isEmpty()) {
                    MToaster.showButtonToast(wefActivity.get(), ChatApplication.context.getString(R.string.showcase_not_ready), Style.TYPE_STANDARD);
                    if (!ShowcaseManager.getInstance().fetchingShowcaseData) {
                        showcaseWorker.submit(new Runnable() {
                            @Override
                            public void run() {
                                ShowcaseManager.getInstance().fetchShowcaseData();
                            }
                        });
                    } else {
                        Log.d(TAG, "fetchShowcaseData: skipping, handling last request");
                    }

                    return;
                }
                ActivityLauncher.launchActivityByClass(wefActivity.get(), ShowcaseActivity.class, R.anim.fade_in, R.anim.fade_out);
                wefActivity.get().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                wefActivity.get().finish();
            }
        });
    }

    @Override
    public void updateView() {
        ((HomeHeroButton) this.wefBtn.get()).updateText(ShowcaseManager.getInstance().getNameByLocale());
        if (ShowcaseManager.getInstance().getImage() != null) { ((HomeHeroButton) this.wefBtn.get()).updateHeroImage(ShowcaseManager.getInstance().getImage()); }
        Glide.with(this.wefActivity.get())
                .load(ShowcaseManager.getInstance().getImageUrl())
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(((HomeHeroButton) this.wefBtn.get()).getHeroBackgroundImageView());
    }

    @Override
    public void unbindView(Activity activity) {
        super.unbindView(activity);
    }

    @Override
    public View buildButton(Context context) {
        return new HomeHeroButton(context, this.width, this.height);
    }
}
