package ai.aiello.aiellospot.views.componets.homebtn.viewitems;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import com.github.johnpersano.supertoasts.library.Style;

import java.lang.ref.WeakReference;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.modules.iot.IoTModuleManager;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.activity.iot.IOTActivity;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractHomeButton;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.HomeBasicButton;
import ai.aiello.aiellospot.views.componets.toast.MToaster;

public class IoTViewItem extends AbstractViewItem {

    public static Thread iotThread;
    UIHandler uiHandler;

    private static IoTViewItem instance;

    public static IoTViewItem getInstance() {
        if (instance == null) {
            instance = new IoTViewItem();
        }
        return instance;
    }

    private IoTViewItem() {}

    @Override
    public void updateView() {

    }

    @Override
    public void handelBtnClickLogic() {
        wefBtn.get().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (IoTModuleManager.devicesAmount == 0) {
                    MToaster.showButtonToastLong(wefActivity.get(), wefActivity.get().getString(R.string.vsp_not_support_title), Style.TYPE_STANDARD);
                    return;
                }
                ActivityLauncher.launchActivityByClass(wefActivity.get(), IOTActivity.class, R.anim.fade_in, R.anim.fade_out);
            }
        });
    }

    @Override
    public void unbindView(Activity activity) {
        super.unbindView(activity);
        if (iotThread != null) {
            iotThread.interrupt();
            iotThread = null;
        }
        if (uiHandler != null) {
            uiHandler.removeCallbacksAndMessages(null);
            uiHandler = null;
        }
    }

    @Override
    public View buildButton(Context context) {
        HomeBasicButton homeBasicButton = new HomeBasicButton(context, this.width, this.height);
        homeBasicButton.updateImage(R.drawable.icon_home2_control);
        homeBasicButton.updateText(R.string.askiot);
        return homeBasicButton;
    }

    static class UIHandler extends Handler {

        Activity activity;

        UIHandler(WeakReference<Activity> reference) {
            activity = reference.get();
        }

        @Override
        public void handleMessage(Message msg) {
        }
    }
}
