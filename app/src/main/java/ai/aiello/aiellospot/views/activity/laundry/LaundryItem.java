package ai.aiello.aiellospot.views.activity.laundry;

/**
 * Created by a1990 on 2019/5/6.
 */

public class LaundryItem {

    private String name,price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public LaundryItem(String name, String price) {
        this.name = name;
        this.price = price;
    }
}
