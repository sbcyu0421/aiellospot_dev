package ai.aiello.aiellospot.views.activity.showcase.beans

import org.json.JSONArray

class SimpleCardViewBean(
    feature_type: String,
    unikey: String,
    title: String,
    detailRawData: String
) : HotelFeatureBean(feature_type, unikey, title) {

    val hotelFeatureDetails = ArrayList<HotelFeatureDataBean>()

    override fun getData(): ArrayList<HotelFeatureDataBean> {
        return hotelFeatureDetails
    }


    init {
        val jsonArray = JSONArray(detailRawData)
        for (i in 0 until jsonArray.length()) {
            val rawDetailData = jsonArray.getJSONObject(i)
            val hotelFeatureDetail = HotelFeatureDataBean(
                rawDetailData.getString("uuid"),
                rawDetailData.getString("title"),
                rawDetailData.getString("content"),
                rawDetailData.getString("img"),
                rawDetailData.getJSONObject("qr_code_url").toString()
            )
            hotelFeatureDetails.add(hotelFeatureDetail)
        }
    }
}