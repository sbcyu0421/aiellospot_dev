package ai.aiello.aiellospot.views.activity.oobe;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.activity.home.MainHomeActivity;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.core.soundai.SoundAIManager;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.HorizontalScrollView;
import android.widget.TextView;

public class Tutor2Activity extends Activity {

    private static String TAG = Tutor2Activity.class.getSimpleName();
    private TextView tutor_use;
    private Animation readyAnim, scrollAnim;
    private HorizontalScrollView scv_tu;

    private TextView tv_tu_music, tv_tu_faq, tv_tu_clock, tv_tu_weather, tv_tu_call, tv_tu_clean, tv_tu_dnd, tv_tu_cloth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutor2);
        hideActivityController();
        initView();
    }

    public void initView() {
        tutor_use = findViewById(R.id.tutor_use);
        scv_tu = findViewById(R.id.scv_tu);

        tv_tu_music = findViewById(R.id.tv_tu_music);
        tv_tu_faq = findViewById(R.id.tv_tu_faq);
        tv_tu_clock = findViewById(R.id.tv_tu_clock);
        tv_tu_weather = findViewById(R.id.tv_tu_weather);
        tv_tu_call = findViewById(R.id.tv_tu_call);
        tv_tu_clean = findViewById(R.id.tv_tu_clean);
        tv_tu_dnd = findViewById(R.id.tv_tu_dnd);
        tv_tu_cloth = findViewById(R.id.tv_tu_cloth);


        readyAnim = new TranslateAnimation(0.0f, 0.0f, 1500.0f, 0.0f);
        readyAnim.setDuration(1200);
        readyAnim.setRepeatCount(0);
        readyAnim.setFillAfter(true);

        scrollAnim = new TranslateAnimation(1500.0f, 0.0f, 0.0f, 0.0f);
        scrollAnim.setDuration(1200);
        scrollAnim.setRepeatCount(0);
        scrollAnim.setFillAfter(true);

        scv_tu.setVisibility(View.VISIBLE);
        scv_tu.startAnimation(scrollAnim);

        tutor_use.startAnimation(readyAnim);
        tutor_use.setVisibility(View.VISIBLE);
        tutor_use.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SoundAIManager.setWakeUp(Tutor2Activity.this, true);
                ChatApplication.tutor = false;
                ActivityLauncher.launchActivityByClass(Tutor2Activity.this, MainHomeActivity.class, R.anim.fade_in, R.anim.fade_out);
            }
        });

        tv_tu_music.setText(String.format(getResources().getString(R.string.tutor_service_music_msg), DeviceInfo.assistantName));
        tv_tu_faq.setText(String.format(getResources().getString(R.string.tutor_service_faq_msg), DeviceInfo.assistantName));
        tv_tu_clock.setText(String.format(getResources().getString(R.string.tutor_service_clock_msg), DeviceInfo.assistantName));
        tv_tu_weather.setText(String.format(getResources().getString(R.string.tutor_service_weather_msg), DeviceInfo.assistantName));
        tv_tu_call.setText(String.format(getResources().getString(R.string.tutor_service_call_msg), DeviceInfo.assistantName));
        tv_tu_clean.setText(String.format(getResources().getString(R.string.tutor_service_clean_msg), DeviceInfo.assistantName));
        tv_tu_dnd .setText(String.format(getResources().getString(R.string.tutor_service_dont_disturb_msg), DeviceInfo.assistantName));
        tv_tu_cloth .setText(String.format(getResources().getString(R.string.tutor_service_clothes_msg), DeviceInfo.assistantName));


    }


    public void hideActivityController() {
        //hideNavController
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }


//    //註冊重啟服務
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onMessageEvent(RestartEvent event) {
//
//        int action = event.getAction();
//        switch (action) {
//            case PushServiceConn.RX_APK_REBOOT:
//                SystemControl.rebootAPK(this);
//                break;
//            case PushServiceConn.RX_DEVICE_REBOOT:
//                SystemControl.rebootDevice();
//                break;
//            case PushServiceConn.RX_SILENT_RESTART:
//                SystemControl.restartSilently(this);
//                break;
//        }
//
//    }
//
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onMessageEvent(CheckInOutEvent event) {
//        if (event.getStatus() == CheckInOutEvent.checkOut) {
//            SystemControl.rebootAPK(this);
//        }
//    }

}

