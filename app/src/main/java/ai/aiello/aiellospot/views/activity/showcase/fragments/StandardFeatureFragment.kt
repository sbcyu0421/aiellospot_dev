package ai.aiello.aiellospot.views.activity.showcase.fragments

import ai.aiello.aiellospot.databinding.FragmentHotelFeatureStandardBinding
import ai.aiello.aiellospot.modules.showcase.ShowcaseManager
import ai.aiello.aiellospot.views.activity.BaseFragment
import ai.aiello.aiellospot.views.activity.showcase.beans.SimpleCardViewBean
import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class StandardFeatureFragment(var unikey: String? = null) : BaseFragment() {

    val TAG = StandardFeatureFragment::class.java.simpleName

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView: $unikey")
        val binding = FragmentHotelFeatureStandardBinding
            .inflate(inflater, container, false)
        val dataList = (ShowcaseManager.getInstance().showcaseData.get(unikey) as SimpleCardViewBean).hotelFeatureDetails
        val adapter = HotelFeatureDetailAdapter(HotelFeatureDetailAdapter.OnClickListener() {
            val dialog = context?.let { it1 -> HotelFeatureDetailDialog(it1, it) }
            dialog?.show()
        })


        // Adding divider for recyclerview
        binding.recyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(
                outRect: Rect,
                view: View,
                parent: RecyclerView,
                state: RecyclerView.State
            ) {
                super.getItemOffsets(outRect, view, parent, state)
                outRect.right = 6
            }
        })

        val layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerView.layoutManager = layoutManager
        binding.recyclerView.adapter = adapter
        adapter.submitList(dataList)

        return binding.root
    }

}