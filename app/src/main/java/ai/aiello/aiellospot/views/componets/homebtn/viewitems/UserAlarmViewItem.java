package ai.aiello.aiellospot.views.componets.homebtn.viewitems;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import java.lang.ref.WeakReference;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.activity.clock.ClockActivity;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractHomeButton;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.HomeBasicButton;

public class UserAlarmViewItem extends AbstractViewItem {

    public static Thread alarmThread;
    UIHandler uiHandler;

    private static UserAlarmViewItem instance;

    public static UserAlarmViewItem getInstance() {
        if (instance == null) {
            instance = new UserAlarmViewItem();
        }
        return instance;
    }

    private UserAlarmViewItem() {}

    @Override
    public void updateView() {

    }

    @Override
    public void handelBtnClickLogic() {
        wefBtn.get().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityLauncher.launchActivityByClass(wefActivity.get(), ClockActivity.class, R.anim.fade_in, R.anim.fade_out);
                wefActivity.get().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                wefActivity.get().finish();
            }
        });
    }

    @Override
    public void unbindView(Activity activity) {
        super.unbindView(activity);
        if (alarmThread != null) {
            alarmThread.interrupt();
            alarmThread = null;
        }
        if (uiHandler != null) {
            uiHandler.removeCallbacksAndMessages(null);
            uiHandler = null;
        }
    }

    @Override
    public View buildButton(Context context) {
        HomeBasicButton homeBasicButton = new HomeBasicButton(context, this.width, this.height);
        homeBasicButton.updateImage(R.drawable.icon_tu_clock);
        homeBasicButton.updateText(R.string.alarm_home);
        return homeBasicButton;
    }

    static class UIHandler extends Handler {

        Activity activity;

        UIHandler(WeakReference<Activity> reference) {
            activity = reference.get();
        }

        @Override
        public void handleMessage(Message msg) {
        }
    }
}
