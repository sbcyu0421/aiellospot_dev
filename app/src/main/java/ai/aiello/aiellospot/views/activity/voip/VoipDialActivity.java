package ai.aiello.aiellospot.views.activity.voip;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.events.server.ServerEvent;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.activity.BaseActivity;
import ai.aiello.aiellospot.views.componets.toast.MToaster;
import ai.aiello.aiellospot.views.activity.dialog.AielloAlertDialog;
import ai.aiello.aiellospot.views.activity.dialog.AielloProgressDialog;
import ai.aiello.aiellospot.views.activity.new_ota.VersionCheckerT;
import ai.aiello.aiellospot.core.info.UserInfo;
import ai.aiello.aiellospot.core.*;
import ai.aiello.aiellospot.core.asr.ASRManager;
import ai.aiello.aiellospot.events.system.SystemEvent;
import ai.aiello.aiellospot.modules.ads.AdsModuleManager;
import ai.aiello.aiellospot.core.chatbot.ChatBot;
import ai.aiello.aiellospot.core.chatbot.ChatbotValidator;
import ai.aiello.aiellospot.core.config.APIConfig;
import ai.aiello.aiellospot.core.config.Configer;
import ai.aiello.aiellospot.core.config.SystemConfig;
import ai.aiello.aiellospot.core.info.DeviceInfo;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.modules.iot.IoTModuleManager;
import ai.aiello.aiellospot.core.system.AielloWifiManager;
import ai.aiello.aiellospot.core.system.SystemControl;
import ai.aiello.aiellospot.utlis.AielloFileUtils;
import ai.aiello.aiellospot.utlis.TestHub;
import ai.aiello.aiellospot.modules.voip.Phonebook;
import ai.aiello.aiellospot.modules.voip.VoipModuleManager;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Gravity;
import android.view.View;
import android.widget.*;

import com.github.johnpersano.supertoasts.library.Style;
import com.litesuits.android.log.Log;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

public class VoipDialActivity extends BaseActivity implements View.OnClickListener {

    static String TAG = VoipDialActivity.class.getSimpleName();

    private RelativeLayout ly_back, ly_dial;
    private EditText et_dial;
    private Button nkey_1, nkey_2, nkey_3, nkey_4, nkey_5, nkey_6, nkey_7, nkey_8, nkey_9, nkey_0, nkey_asterisk, nkey_pound;
    private String dial_Number = "";
    private ImageView img_delete;
    private String makeCallNo = "";
    private AielloProgressDialog aielloProgressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voip_dial);
        initView();
        updateView();

    }

    private void initView() {
        ly_back = findViewById(R.id.ly_back);
        ly_dial = findViewById(R.id.ly_dial);
        et_dial = findViewById(R.id.et_dial);

        nkey_1 = findViewById(R.id.nkey_1);
        nkey_2 = findViewById(R.id.nkey_2);
        nkey_3 = findViewById(R.id.nkey_3);
        nkey_4 = findViewById(R.id.nkey_4);
        nkey_5 = findViewById(R.id.nkey_5);
        nkey_6 = findViewById(R.id.nkey_6);
        nkey_7 = findViewById(R.id.nkey_7);
        nkey_8 = findViewById(R.id.nkey_8);
        nkey_9 = findViewById(R.id.nkey_9);
        nkey_0 = findViewById(R.id.nkey_0);
        nkey_asterisk = findViewById(R.id.nkey_asterisk);
        nkey_pound = findViewById(R.id.nkey_pound);

        img_delete = findViewById(R.id.img_delete);

        nkey_1.setOnClickListener(this);
        nkey_2.setOnClickListener(this);
        nkey_3.setOnClickListener(this);
        nkey_4.setOnClickListener(this);
        nkey_5.setOnClickListener(this);
        nkey_6.setOnClickListener(this);
        nkey_7.setOnClickListener(this);
        nkey_8.setOnClickListener(this);
        nkey_9.setOnClickListener(this);
        nkey_0.setOnClickListener(this);
        nkey_asterisk.setOnClickListener(this);
        nkey_pound.setOnClickListener(this);
        img_delete.setOnClickListener(this);

        ly_back.setOnClickListener(this);
        ly_dial.setOnClickListener(this);
        et_dial.setFocusableInTouchMode(false);
        et_dial.setFocusable(true);

        img_delete.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                et_dial.setText("");
                return false;
            }
        });

    }

    public void updateView() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ly_back:
                ActivityLauncher.launchActivityByClass(this, VoipHomeActivity.class, R.anim.fade_in, R.anim.fade_out);
                break;
            case R.id.ly_dial:
                makeCall();

                SpotUserTraceLog3.getInstance().buildUIEventLog(
                        SpotUserTraceLog3.EventSubject.VOIP,
                        SpotUserTraceLog3.EventAction.DIAL,
                        makeCallNo
                );
                break;

            case R.id.nkey_1:
                typeIn(1);
                break;
            case R.id.nkey_2:
                typeIn(2);
                break;
            case R.id.nkey_3:
                typeIn(3);
                break;
            case R.id.nkey_4:
                typeIn(4);
                break;
            case R.id.nkey_5:
                typeIn(5);
                break;
            case R.id.nkey_6:
                typeIn(6);
                break;
            case R.id.nkey_7:
                typeIn(7);
                break;
            case R.id.nkey_8:
                typeIn(8);
                break;
            case R.id.nkey_9:
                typeIn(9);
                break;
            case R.id.nkey_0:
                typeIn(0);
                break;
            case R.id.nkey_asterisk:
                typeIn(-1);
                break;
            case R.id.nkey_pound:
                typeIn(-2);
                break;

            case R.id.img_delete:
                deleteNum();
                break;
        }
    }

    public void typeIn(int inputNumber) {
        String br = et_dial.getText().toString();
        if (inputNumber >= 0) {
            dial_Number = br + inputNumber;
        } else if (inputNumber == -1) {
            dial_Number = br + "*";
        } else {
            dial_Number = br + "#";
        }

        et_dial.setText(dial_Number);
    }

    public void deleteNum() {

        makeCallNo = et_dial.getText().toString();
        if (makeCallNo.isEmpty())
            return;

        makeCallNo = makeCallNo.substring(0, makeCallNo.length() - 1);
        et_dial.setText(makeCallNo);

    }

    public void makeCall() {
        makeCallNo = et_dial.getText().toString();

//        if (makeCallNo.equals(DeviceInfo.roomName)) {
//            MToaster.showButtonToast(VoipDialActivity.this, getString(R.string.call_self_error), Style.TYPE_STANDARD);
//            return;
//        }

        switch (makeCallNo) {

            case "":
//                Toast.makeText(getApplicationContext(), R.string.et_is_empty, Toast.LENGTH_SHORT).show();
                MToaster.showButtonToast(VoipDialActivity.this, getString(R.string.et_is_empty), Style.TYPE_STANDARD);
                break;

            case Phonebook.MN_SETTING:
                Intent intent3 = new Intent(Settings.ACTION_SETTINGS);
                startActivity(intent3);
                break;

            case Phonebook.MN_APK_REBOOT:
                AielloAlertDialog sys_dialog = new AielloAlertDialog(this, R.style.Theme_AppCompat_Dialog_Alert);
                sys_dialog.setTitle(getString(R.string.apk_reboot_title))
                        .setTextContent(getString(R.string.apk_reboot_content))
                        .setOnCancelListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                sys_dialog.dismiss();
                            }
                        })
                        .setOnConfirmListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                EventBus.getDefault().post(new ServerEvent(ServerEvent.Type.APPREBOOT));
                                SystemControl.rebootAPK(VoipDialActivity.this, SystemControl.StageAction.CLEAR);
                                sys_dialog.dismiss();
                            }
                        }).show();
                break;

            case Phonebook.MN_APK_ASR:
                int asrModel = ASRManager.getInstance().switchASRModel();
                MToaster.showButtonToast(VoipDialActivity.this, String.valueOf(asrModel), Style.TYPE_PROGRESS_BAR);
                break;
            case Phonebook.MM_DEBUG_AUTO:
                if (TestHub.debug_mode == false) {
                    TestHub.debug_mode = true;
                    if (!TestHub.ConnectionState())
                        TestHub.Connect();
                    MToaster.showButtonToast(VoipDialActivity.this, "Switch on Auto Debug mode", Style.TYPE_STANDARD);
                } else {
                    TestHub.debug_mode = false;
                    TestHub.Disable();
                }
                break;
            case Phonebook.MN_WEBVIEW:
                Intent intent = new Intent();
                intent.setClassName("org.chromium.webview_shell", "org.chromium.webview_shell.WebViewBrowserActivity");
                startActivity(intent);
                break;


            case Phonebook.MN_UPLOAD:

                showProgressDialog();
                SystemControl.uploadLogFromClient(new AielloFileUtils.UploadFinishListener() {
                    @Override
                    public void onSuccess() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideProgressDialog();
                                MToaster.showButtonToast(VoipDialActivity.this, "上傳成功", Style.TYPE_STANDARD);
                            }

                        });
                    }

                    @Override
                    public void onFail() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideProgressDialog();
                                MToaster.showButtonToast(VoipDialActivity.this, "上傳失敗", Style.TYPE_STANDARD);
                            }
                        });

                    }
                });
                break;

            case Phonebook.MN_APK_OTA_PRODUCTION:
                SystemControl.manualOTA(this,
                        APIConfig.WEBAPI_SPOT_NOTE.getUrl(),
                        APIConfig.WEBAPI_SPOT_NOTE.getKey(),
                        DeviceInfo.hotelID, "release", SystemConfig.OTA.apk_cloud, DeviceInfo.majorVersion);
                break;

            case Phonebook.MN_APK_OTA_VIP:
                SystemControl.manualOTA(this,
                        APIConfig.WEBAPI_SPOT_NOTE.getUrl(),
                        APIConfig.WEBAPI_SPOT_NOTE.getKey(),
                        DeviceInfo.hotelID, "test", SystemConfig.OTA.apk_cloud, DeviceInfo.majorVersion);
                break;


            case Phonebook.MN_APK_ADS:
                getAdsAsync();
                break;

            case Phonebook.MN_IMAGE_OTA:
                SystemControl.imageCheckAndUpdate(this);
                break;

            case Phonebook.MN_DEVICE_REBOOT:
                EventBus.getDefault().post(new ServerEvent(ServerEvent.Type.DEVICEREBOOT));
                SystemControl.rebootDevice(this, SystemControl.StageAction.CLEAR);
                break;

            case Phonebook.MN_CHATBOT_AUTOTEST:
                new ChatbotValidator(this).start();
                break;

            case Phonebook.MN_IOT_TEST_START:
                IoTModuleManager.getInstance().testToT(true);
                break;

            case Phonebook.MN_API_DEV_ENV:
                switchAPIEnv();
                break;

            case Phonebook.MN_IOT_TEST_STOP:
                IoTModuleManager.getInstance().testToT(false);
                break;

            case Phonebook.MN_IOT_INTEGRATION_MODE:
                if (ChatBot.IntegrationMode) {
                    MToaster.showButtonToast(VoipDialActivity.this, "關閉IoT測試DB", Style.TYPE_STANDARD);
                    ChatBot.IntegrationMode = false;
                } else {
                    MToaster.showButtonToast(VoipDialActivity.this, "開啟IoT測試DB", Style.TYPE_STANDARD);
                    ChatBot.IntegrationMode = true;
                }
                break;

            case Phonebook.MN_WIFI_TEST_START:
                AielloWifiManager.testWifi(true);
                break;

            case Phonebook.MN_WIFI_TEST_STOP:
                AielloWifiManager.testWifi(false);
                break;

            case Phonebook.MN_CLIENT_INFO:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        String form = String.format("mac : %s \n" +
                                        "============================== \n" +
                                        "hotelName : %s \n" +
                                        "roomName : %s \n" +
                                        "asrOpt : %s \n" +
                                        "============================== \n" +
                                        "img_ver : %s \n" +
                                        "apk_ver : %s \n" +
                                        "push_ver : %s \n" +
                                        "music_ver : %s \n" +
                                        "sai_ver : %s",
                                DeviceInfo.MAC, DeviceInfo.hotelName, DeviceInfo.roomName,
                                ASRManager.getInstance().getASRModel(),
                                VersionCheckerT.img_ver, VersionCheckerT.apk_ver, VersionCheckerT.push_ver, VersionCheckerT.music_ver, VersionCheckerT.sai_ver);

                        Dialog d = new AlertDialog.Builder(VoipDialActivity.this)
                                .setTitle("設備資訊")
                                .setMessage(form)
                                .setNeutralButton("關閉", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //null
                                    }
                                }).create();
                        d.show();
                        d.getWindow().setGravity(Gravity.CENTER);
                        d.getWindow().setLayout(1000, 900); //Controlling width and height.

                    }
                });
                break;

            case Phonebook.MN_SYSDIAG_TEST:
                Log.i(TAG, "Open Sysdiag APK");
                Intent launchSysdiagApk = getPackageManager().getLaunchIntentForPackage("com.example.sysdiag");
                if (launchSysdiagApk != null) {
                    startActivity(launchSysdiagApk);
                }
                break;

            //stress test
            case "***999***123456":
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            JSONObject jb_toilet_on = new JSONObject();
                            jb_toilet_on.put("device", "light");
                            jb_toilet_on.put("success", true);
                            jb_toilet_on.put("location", "toilet");
                            jb_toilet_on.put("control", "on");

                            JSONObject jb_toilet_off = new JSONObject();
                            jb_toilet_off.put("device", "light");
                            jb_toilet_off.put("success", true);
                            jb_toilet_off.put("location", "toilet");
                            jb_toilet_off.put("control", "off");

                            JSONObject jb_room_on = new JSONObject();
                            jb_room_on.put("device", "light");
                            jb_room_on.put("success", true);
                            jb_room_on.put("location", "");
                            jb_room_on.put("control", "on");

                            JSONObject jb_room_off = new JSONObject();
                            jb_room_off.put("device", "light");
                            jb_room_off.put("success", true);
                            jb_room_off.put("location", "");
                            jb_room_off.put("control", "off");

                            JSONObject jb_morning = new JSONObject();
                            jb_morning.put("device", "");
                            jb_morning.put("type", "good morning mode");
                            jb_morning.put("success", true);
                            jb_morning.put("location", "");
                            jb_morning.put("control", "");

                            JSONObject jb_night = new JSONObject();
                            jb_night.put("device", "");
                            jb_night.put("type", "good night mode");
                            jb_night.put("success", true);
                            jb_night.put("location", "");
                            jb_night.put("control", "");

                            int sleepTime = 2000;

                            while (true) {
                                IoTModuleManager.getInstance().push_iot_intent_to_remoteService(jb_toilet_on.toString(), "Dialogflow.IoT.Light", null);
                                Thread.sleep(sleepTime);
                                IoTModuleManager.getInstance().push_iot_intent_to_remoteService(jb_toilet_off.toString(), "Dialogflow.IoT.Light", null);
                                Thread.sleep(sleepTime);
                                IoTModuleManager.getInstance().push_iot_intent_to_remoteService(jb_room_on.toString(), "Dialogflow.IoT.Light", null);
                                Thread.sleep(sleepTime);
                                IoTModuleManager.getInstance().push_iot_intent_to_remoteService(jb_room_off.toString(), "Dialogflow.IoT.Light", null);
                                Thread.sleep(sleepTime);
                                IoTModuleManager.getInstance().push_iot_intent_to_remoteService(jb_morning.toString(), "Dialogflow.IoT.Mode", null);
                                Thread.sleep(sleepTime);
                                IoTModuleManager.getInstance().push_iot_intent_to_remoteService(jb_night.toString(), "Dialogflow.IoT.Mode", null);
                                Thread.sleep(sleepTime * 5);
                            }
                        } catch (Exception e) {
                            SpotDeviceLog.exception(TAG, "", e.getMessage());
                            Log.e(TAG, e.toString());
                        }
                    }
                }).start();
                break;

            default:
                if (VoipModuleManager.getInstance().getInstance().currentState.getName() == VoipModuleManager.StateName.Disable) {
                    MToaster.showButtonToast(VoipDialActivity.this, getString(R.string.voip_forbidden_action_on_disable_state), Style.TYPE_STANDARD);
                    return;
                }
                VoipModuleManager.getInstance().getInstance().makeCall(makeCallNo);
                break;

        }


    }

    private void getAdsAsync() {
        MCountdownTimer.stopTimer();
        showProgressDialog();
        AdsModuleManager.getInstance().fetchAds(0, 0, new AdsModuleManager.OnCompleteListener() {
            @Override
            public void onComplete() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        Log.d(TAG, "sm.getPlayList().size() = " + AdsModuleManager.getInstance().getPlayList().size());
                        if (AdsModuleManager.getInstance().isReady()) {
                            EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.TIME_DISMISS));
                        } else {
                            MCountdownTimer.startStandbyTimer();
                            MToaster.showButtonToast(VoipDialActivity.this, "獲取廣告失敗", Style.TYPE_PROGRESS_CIRCLE);
                        }
                    }
                });
            }
        });
    }

    public void switchAPIEnv() {
        Configer.useProductAPI = !Configer.useProductAPI;
        Configer configer = new Configer();
        configer.refreshAPI(Configer.useProductAPI);
        if (Configer.useProductAPI) {
            MToaster.showButtonToast(VoipDialActivity.this, "useProductAPI", Style.TYPE_PROGRESS_BAR);
        } else {
            MToaster.showButtonToast(VoipDialActivity.this, "useTestAPI", Style.TYPE_PROGRESS_BAR);
        }
    }


    public void showProgressDialog() {
        if (aielloProgressDialog == null) {
            aielloProgressDialog = new AielloProgressDialog(new WeakReference<Activity>(this), R.style.CustomProgressDialog);
            aielloProgressDialog.setText(getResources().getString(R.string.please_wait));
        }
        aielloProgressDialog.show();
    }


    public void hideProgressDialog() {
        if (aielloProgressDialog != null && aielloProgressDialog.isShowing()) {
            aielloProgressDialog.dismiss();
            aielloProgressDialog = null;
        }
    }

    @Override
    public void handleMakeCallResAction(String message) {
        if (ASRManager.getInstance().getOnASR()) {
            return;
        }
        if (message.equals(VoipModuleManager.ResponseStatus.SUCCESS.toString())) {
            Intent intent_makeCall = new Intent(VoipDialActivity.this, VoipOnCallActivity.class);
            startActivity(intent_makeCall);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        } else if (message.equals(VoipModuleManager.ResponseStatus.SELF.toString())) {
            MToaster.showButtonToast(VoipDialActivity.this, getString(R.string.call_self_error), Style.TYPE_STANDARD);
        } else if (message.equals(VoipModuleManager.ResponseStatus.BUSY.toString())) {
            MToaster.showButtonToast(VoipDialActivity.this, getString(R.string.call_other_busy), Style.TYPE_STANDARD);
        } else if (VoipModuleManager.getInstance().getInstance().makeCallRes.equals(VoipModuleManager.ResponseStatus.OFFLINE.name())) {
            MToaster.showButtonToast(VoipDialActivity.this, getString(R.string.call_offline), Style.TYPE_STANDARD);
        } else {
            MToaster.showButtonToast(VoipDialActivity.this, getString(R.string.makecall_fail), Style.TYPE_STANDARD);
        }
    }

}
