package ai.aiello.aiellospot.views.componets.homebtn.viewitems;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;

import com.github.johnpersano.supertoasts.library.Style;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.ref.WeakReference;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.events.module.RoomStatusStateChange;
import ai.aiello.aiellospot.views.componets.homebtn.HomeBadgeButton;
import ai.aiello.aiellospot.views.componets.homebtn.HomeBasicButton;
import ai.aiello.aiellospot.views.componets.homebtn.HomeLeftStateButton;
import ai.aiello.aiellospot.views.componets.toast.MToaster;
import ai.aiello.aiellospot.views.activity.dialog.AielloAlertDialog;
import ai.aiello.aiellospot.core.config.SystemConfig;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractHomeButton;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractViewItem;
import ai.aiello.aiellospot.modules.roomstatus.RoomStatusModuleManager;

public class RoomStatusDndViewItem extends AbstractViewItem {

    private static String TAG = RoomStatusDndViewItem.class.getSimpleName();

    public static Thread roomStatusDndThread;
    UIHandler uiHandler;

    private static RoomStatusDndViewItem instance;

    public static RoomStatusDndViewItem getInstance() {
        if (instance == null) {
            instance = new RoomStatusDndViewItem();
        }
        return instance;
    }

    private RoomStatusDndViewItem() {}

    @Override
    public void updateView() {
        updateRoomStatusDnd();
    }

    @Override
    public void handelBtnClickLogic() {
        wefBtn.get().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNoDisturbClick();
            }
        });
    }

    @Override
    public void unbindView(Activity activity) {
        super.unbindView(activity);
        if (roomStatusDndThread != null) {
            roomStatusDndThread.interrupt();
            roomStatusDndThread = null;
        }
        if (uiHandler != null) {
            uiHandler.removeCallbacksAndMessages(null);
            uiHandler = null;
        }
    }

    @Override
    public View buildButton(Context context) {
        HomeLeftStateButton homeLeftStateButton = new HomeLeftStateButton(context, this.width, this.height);
        homeLeftStateButton.updateImage(R.drawable.img_dnd);
        homeLeftStateButton.updateText(R.string.asksilent);
        return homeLeftStateButton;
    }

    static class UIHandler extends Handler {

        Activity activity;

        UIHandler(WeakReference<Activity> reference) {
            activity = reference.get();
        }

        @Override
        public void handleMessage(Message msg) {
        }
    }

    private AielloAlertDialog rsConfirmDialog;

    private void onNoDisturbClick() {
        if (!RoomStatusModuleManager.RoomStatusModule.supportDnd) {
            MToaster.showButtonToastLong(wefActivity.get(), wefActivity.get().getString(R.string.vsp_not_support_title), Style.TYPE_STANDARD);
            return;
        }
        if (RoomStatusModuleManager.getInstance().roomStatus.equals(RoomStatusModuleManager.RoomStatus.DontDisturb)) {
            showRSConfirmDialog(
                    ChatApplication.context.getString(R.string.rs_dialog_title),
                    ChatApplication.context.getString(R.string.rs_dnd_off),
                    () -> {
                        // push compatible
                        if (SystemConfig.OTA.support_action) {
                            RoomStatusModuleManager.getInstance().dontDisturb(false, "UI", false);
                        } else {
                            RoomStatusModuleManager.getInstance().dontDisturb(false, false, true, "UI");
                        }
                        SpotUserTraceLog3.getInstance().buildUIEventLog(
                                SpotUserTraceLog3.EventSubject.ROOM_STATUS,
                                SpotUserTraceLog3.EventAction.DND_OFF,
                                ""
                        );
                    }
            );
        } else {
            showRSConfirmDialog(
                    ChatApplication.context.getString(R.string.rs_dialog_title),
                    ChatApplication.context.getString(R.string.rs_dnd_on),
                    () -> {
                        // push compatible
                        if (SystemConfig.OTA.support_action) {
                            RoomStatusModuleManager.getInstance().dontDisturb(true,  "UI", false);
                        } else {
                            RoomStatusModuleManager.getInstance().dontDisturb(true, false, true, "UI");
                        }
                        SpotUserTraceLog3.getInstance().buildUIEventLog(
                                SpotUserTraceLog3.EventSubject.ROOM_STATUS,
                                SpotUserTraceLog3.EventAction.DND_ON,
                                ""
                        );
                    }
            );
        }
    }

    private void showRSConfirmDialog(String title, String message, RSCallback rsCallback) {
        rsConfirmDialog = new AielloAlertDialog(wefActivity.get(), R.style.Theme_AppCompat_Dialog_Alert);
        rsConfirmDialog.setTitle(title)
                .setTextContent(message)
                .setOnCancelListener((View.OnClickListener) v -> {
                    destroyRSConfirmDialog();
                })
                .setOnConfirmListener(v -> {
                    rsCallback.onConfirm();
                    MToaster.showButtonToastLong(wefActivity.get(), ChatApplication.context.getString(R.string.setting_room_status), Style.TYPE_PROGRESS_BAR);
                    destroyRSConfirmDialog();
                });
        rsConfirmDialog.show();
    }

    private void destroyRSConfirmDialog(){
        Log.d(TAG, "RS ConfirmDialog destroyed");
        try {
            if (rsConfirmDialog != null) {
                rsConfirmDialog.dismiss();
                rsConfirmDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    interface RSCallback {
        void onConfirm();
    }

    public void updateRoomStatusDnd() {
        switch (RoomStatusModuleManager.getInstance().roomStatus) {
            case None:
            case Clean:
                ((HomeLeftStateButton)this.wefBtn.get()).getTv_left_status().setVisibility(View.GONE);
                break;
            case DontDisturb:
                ((HomeLeftStateButton)this.wefBtn.get()).getTv_left_status().setVisibility(View.VISIBLE);
                ((HomeLeftStateButton)this.wefBtn.get()).getTv_left_status().setBackgroundColor(ChatApplication.context.getResources().getColor(R.color.red));
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(RoomStatusStateChange event) {
        updateView();
    }

}
