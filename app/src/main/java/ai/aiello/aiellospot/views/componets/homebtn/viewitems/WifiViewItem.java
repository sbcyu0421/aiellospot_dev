package ai.aiello.aiellospot.views.componets.homebtn.viewitems;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;

import java.lang.ref.WeakReference;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.core.system.WifiReceiver;
import ai.aiello.aiellospot.events.system.SystemEvent;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.HomeBasicButton;

public class WifiViewItem extends AbstractViewItem {

    private static final String TAG = WifiViewItem.class.getSimpleName();
    public static Thread wifiThread;
    UIHandler uiHandler;

    private static WifiViewItem instance;

    public static WifiViewItem getInstance() {
        if (instance == null) {
            instance = new WifiViewItem();
        }
        return instance;
    }

    private WifiViewItem() {}

    @Override
    public void updateView() {
        updateWifiStatus();
    }

    @Override
    public void handelBtnClickLogic() {
        wefBtn.get().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onWifiClick();
            }
        });
    }

    @Override
    public void bindView(WeakReference<Activity> wefActivity, WeakReference<View> wefBtn) {
        super.bindView(wefActivity, wefBtn);
        uiHandler = new UIHandler(wefActivity);
    }

    @Override
    public void unbindView(Activity activity) { //unbind
        super.unbindView(activity);
        if (wifiThread != null) {
            wifiThread.interrupt();
            wifiThread = null;
        }
        if (uiHandler != null) {
            uiHandler.removeCallbacks(null);//?
            uiHandler.removeCallbacksAndMessages(null);
            uiHandler = null; // todo: released?
        }
    }

    @Override
    public View buildButton(Context context) {
        HomeBasicButton homeBasicButton = new HomeBasicButton(context, this.width, this.height);
        homeBasicButton.updateImage(R.drawable.icon_home1_wifi_4);
        homeBasicButton.updateText(R.string.askwifi);
        return homeBasicButton;
    }

    private void onWifiClick() {
        if (wifiThread != null) {
            wifiThread.interrupt();
            wifiThread = null;
        }
        wifiThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = 0; i < 3; i++) {
                        uiHandler.sendEmptyMessage(0);
                        Thread.sleep(400);
                        uiHandler.sendEmptyMessage(1);
                        Thread.sleep(400);
                        uiHandler.sendEmptyMessage(2);
                        Thread.sleep(400);
                        uiHandler.sendEmptyMessage(3);
                        Thread.sleep(400);
                        uiHandler.sendEmptyMessage(4);
                        Thread.sleep(400);
                        uiHandler.sendEmptyMessage(5);
                    }

                } catch (InterruptedException e) {

                }
            }
        });

        wifiThread.start();
//        AielloWifiManager.reconnect();
    }


    static class UIHandler extends Handler {

        Activity activity;

        UIHandler(WeakReference<Activity> reference) {
            activity = reference.get();
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    //todo run this on ui thread (activity required)
                    ((HomeBasicButton) WifiViewItem.getInstance().wefBtn.get()).updateImage(R.drawable.icon_home1_wifi_0);
                    break;
                case 1: //WifiSignalLevel.POOR
                    ((HomeBasicButton) WifiViewItem.getInstance().wefBtn.get()).updateImage(R.drawable.icon_home1_wifi_1);
                    break;
                case 2: //WifiSignalLevel.FAIR
                    ((HomeBasicButton) WifiViewItem.getInstance().wefBtn.get()).updateImage(R.drawable.icon_home1_wifi_2);
                    break;
                case 3: // WifiSignalLevel.GOOD
                    ((HomeBasicButton) WifiViewItem.getInstance().wefBtn.get()).updateImage(R.drawable.icon_home1_wifi_3);
                    break;
                case 4:  //WifiSignalLevel.EXCELLENT
                    ((HomeBasicButton) WifiViewItem.getInstance().wefBtn.get()).updateImage(R.drawable.icon_home1_wifi_4);
                    break;
                case 5:  //updateView
                    WifiViewItem.getInstance().updateWifiStatus();
                    break;
            }
        }
    }

    public void updateWifiStatus() {
        WifiReceiver.WifiLevel wifiLevel = WifiReceiver.wifiLevel;
        switch (wifiLevel) {
            case NULL:
                ((HomeBasicButton) this.wefBtn.get()).updateImage(R.drawable.icon_home1_wifi_0);
                break;
            case POOR: //WifiSignalLevel.POOR
                ((HomeBasicButton) this.wefBtn.get()).updateImage(R.drawable.icon_home1_wifi_1);
                break;
            case FAIR: //WifiSignalLevel.FAIR
                ((HomeBasicButton) this.wefBtn.get()).updateImage(R.drawable.icon_home1_wifi_2);
                break;
            case GOOD: // WifiSignalLevel.GOOD
                ((HomeBasicButton) this.wefBtn.get()).updateImage(R.drawable.icon_home1_wifi_3);
                break;
            case EXCELLENT:  //WifiSignalLevel.EXCELLENT
                ((HomeBasicButton) this.wefBtn.get()).updateImage(R.drawable.icon_home1_wifi_4);
                break;
        }

    }

    @Override
    public void onMessageEvent(SystemEvent event) {
        super.onMessageEvent(event);
        switch (event.getType()){
            case WIFI_SIGNAL_CHANGE:
                Log.d(TAG, "onMessageEvent: WIFI_SIGNAL_CHANGE");
                updateWifiStatus();
                break;
        }
    }

}
