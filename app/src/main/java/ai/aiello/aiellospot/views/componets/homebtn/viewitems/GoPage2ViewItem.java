package ai.aiello.aiellospot.views.componets.homebtn.viewitems;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import java.lang.ref.WeakReference;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.activity.home.MainHome2Activity;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractHomeButton;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.HomeBasicButton;

public class GoPage2ViewItem extends AbstractViewItem {

    public static Thread toPage2Thread;
    UIHandler uiHandler;

    private static GoPage2ViewItem instance;

    public static GoPage2ViewItem getInstance() {
        if (instance == null) {
            instance = new GoPage2ViewItem();
        }
        return instance;
    }

    private GoPage2ViewItem() {}

    @Override
    public void updateView() {

    }

    @Override
    public void handelBtnClickLogic() {
        wefBtn.get().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityLauncher.launchActivityByClass(wefActivity.get(), MainHome2Activity.class, R.anim.fade_in, R.anim.fade_out);
            }
        });
    }

    @Override
    public void unbindView(Activity activity) {
        super.unbindView(activity);
        if (toPage2Thread != null) {
            toPage2Thread.interrupt();
            toPage2Thread = null;
        }
        if (uiHandler != null) {
            uiHandler.removeCallbacksAndMessages(null);
            uiHandler = null;
        }
    }

    @Override
    public View buildButton(Context context) {
        HomeBasicButton homeBasicButton = new HomeBasicButton(context, this.width, this.height);
        homeBasicButton.updateImage(R.drawable.icon_home1_moreservice);
        homeBasicButton.updateText(R.string.to_page2);
        return homeBasicButton;
    }

    static class UIHandler extends Handler {

        Activity activity;

        UIHandler(WeakReference<Activity> reference) {
            activity = reference.get();
        }

        @Override
        public void handleMessage(Message msg) {
        }
    }
}
