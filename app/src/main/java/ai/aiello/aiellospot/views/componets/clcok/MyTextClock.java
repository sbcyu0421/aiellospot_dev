package ai.aiello.aiellospot.views.componets.clcok;

import android.content.Context;
import android.util.AttributeSet;
import com.litesuits.android.log.Log;

import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by a1990 on 2019/5/3.
 */

public class MyTextClock extends android.widget.TextClock {

    public String TAG = MyTextClock.class.getSimpleName();

    public MyTextClock(Context context) {
        super(context);
    }

    public MyTextClock(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyTextClock(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    public void setLocaleWeekFormat(Locale locale) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Taipei"), locale);
        Log.d(TAG, "locale=" + locale);
        String week = cal.getDisplayName(cal.DAY_OF_WEEK, Calendar.LONG, locale);
        Log.d(TAG, "week=" + week);
//        String monthName = cal.getDisplayName(cal.MONTH, Calendar.LONG, locale);
        this.setFormat24Hour("'" + week + "'");
        this.setFormat12Hour("'" + week + "'");
    }

    public void setLocaleDateFormat(Locale locale) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Taipei"), locale);
        Log.d(TAG, "locale=" + locale);
        String hour_of_day = cal.getDisplayName(cal.HOUR_OF_DAY, Calendar.LONG, locale);
        String minute = cal.getDisplayName(cal.MINUTE, Calendar.LONG, locale);
        this.setFormat24Hour("'" + hour_of_day +':'+ minute + "'");
        this.setFormat12Hour("'" + hour_of_day +':'+ minute + "'");
    }

}