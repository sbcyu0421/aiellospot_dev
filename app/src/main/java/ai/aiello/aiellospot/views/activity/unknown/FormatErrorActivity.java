package ai.aiello.aiellospot.views.activity.unknown;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.CardActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class FormatErrorActivity extends CardActivity {

    TextView tv_title, tv_msg;
    String title, msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activtiy_format_error);

        tv_title = findViewById(R.id.tv_title_format);
        tv_msg = findViewById(R.id.tv_msg_format);
        LinearLayout btn_ok = findViewById(R.id.btn_ok);
        ScrollView scrollView_msg = findViewById(R.id.scrollView_msg);

        title = intentObject.getMsg1();
        msg = intentObject.getMsg2();

        int ml = msg.length();

        if (ml <= 17) {
            tv_msg.setHeight(180);
            scrollView_msg.getLayoutParams().height = 180;
        } else if (ml <= 34) {
            tv_msg.setHeight(220);
            scrollView_msg.getLayoutParams().height = 220;
        } else if (ml <= 51) {
            tv_msg.setHeight(260);
            scrollView_msg.getLayoutParams().height = 260;
        } else {
            scrollView_msg.getLayoutParams().height = 260;
        }

        tv_title.setText(title);
        tv_msg.setText(msg);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmAndGoBack();
            }
        });

    }

}
