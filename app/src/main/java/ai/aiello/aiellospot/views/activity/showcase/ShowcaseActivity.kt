package ai.aiello.aiellospot.views.activity.showcase

import ai.aiello.aiellospot.R
import ai.aiello.aiellospot.databinding.ActivityShowcaseBinding
import ai.aiello.aiellospot.modules.showcase.ShowcaseManager
import ai.aiello.aiellospot.modules.showcase.radio.HotelMusicManager
import ai.aiello.aiellospot.views.activity.ActivityLauncher
import ai.aiello.aiellospot.views.activity.BaseActivity
import ai.aiello.aiellospot.views.activity.home.MainHomeActivity
import ai.aiello.aiellospot.views.activity.showcase.fragments.RadioFeatureFragment
import ai.aiello.aiellospot.views.activity.showcase.fragments.StandardFeatureFragment
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

class ShowcaseActivity : BaseActivity() {

    val TAG: String = ShowcaseActivity::class.java.simpleName

    lateinit var navBar: LinearLayout
    private val selectedIndex = MutableLiveData<Int>()
    var lastSelectedIndex = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = androidx.databinding.DataBindingUtil
            .setContentView<ActivityShowcaseBinding>(this, R.layout.activity_showcase)

        navBar = binding.navigateBar
        setUpNavBarViewItems(this.applicationContext)

        selectedIndex.observe(this, Observer {
            (navBar.getChildAt(lastSelectedIndex) as ShowcaseSideButton).focusIcon.visibility = View.GONE
            (navBar.getChildAt(it) as ShowcaseSideButton).focusIcon.visibility = View.VISIBLE
            lastSelectedIndex = selectedIndex.value!!
        })

        showDefaultFragment()
    }

    private fun setUpNavBarViewItems(context: Context) {
        var index = 0
        for ((k, v) in ShowcaseManager.getInstance().showcaseData) {
            val view = ShowcaseSideButton(v, context)
            view.position = index
            view.setOnClickListener {
                view.unikey?.let { it1 -> fragmentManager(it1) }
                selectedIndex.value = view.position
            }
            navBar.addView(view)
            index ++
        }
    }

    fun onHome2BackClick(view: View) {
        val intent = Intent(this, ActivityLauncher.currentHomeName)
        startActivity(intent)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        finish()
    }

    private fun fragmentManager(unikey: String) {
        val featureType = ShowcaseManager.getInstance().showcaseData.get(unikey)
        val fragment: Fragment? = when (featureType?.feature_type) {
            ShowcaseManager.FeatureType.SIMPLE_CARD_VIEW.name -> StandardFeatureFragment(unikey)
            ShowcaseManager.FeatureType.SIMPLE_RADIO_VIEW.name -> RadioFeatureFragment(unikey)
            else -> null
        }
        val transaction = supportFragmentManager.beginTransaction()
        if (fragment != null) {
            transaction.replace(R.id.hotel_feature_fragment, fragment)
            transaction.commit()
        }
    }

    private fun showDefaultFragment() {
        if (!HotelMusicManager.isPlaying.value!!) {
            for ((k, _) in ShowcaseManager.getInstance().showcaseData) {
                selectedIndex.value = 0
                fragmentManager(k)
                break
            }
        } else {
            var index = 0
            for ((k, v) in ShowcaseManager.getInstance().showcaseData) {
                if (v.feature_type == ShowcaseManager.FeatureType.SIMPLE_RADIO_VIEW.toString()) {
                    selectedIndex.value = index
                    fragmentManager(k)
                    break
                }
                index ++
            }
        }
    }

    private fun initHotelMusicPlayer() {
        HotelMusicManager.initPlayerAndMediaSource()
    }

}
