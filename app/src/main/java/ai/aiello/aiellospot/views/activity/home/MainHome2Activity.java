package ai.aiello.aiellospot.views.activity.home;

import ai.aiello.aiellospot.views.activity.ActivityLauncher;

public class MainHome2Activity extends AbstractHomeActivity {

    public String TAG = MainHome2Activity.class.getSimpleName();

    @Override
    int getCurrentPage() {
        return 2;
    }

    @Override
    protected void onResume() {
        super.onResume();
        ActivityLauncher.currentHomeName = MainHome2Activity.class;
    }

}
