package ai.aiello.aiellospot.views.activity.unknown;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.CardActivity;

public class UnknownActivity extends CardActivity {


    private TextView tv_roobo_title, tv_unknown;
    private LinearLayout btn_ok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activtiy_unknown);

        tv_roobo_title = findViewById(R.id.tv_roobo_title);
        tv_unknown = findViewById(R.id.tv_unknown);
        btn_ok = findViewById(R.id.btn_ok);
        String unknownMsg = getResources().getString(R.string.unknown_intent_msg);


        int ml = unknownMsg.length();

        if (ml <= 17) {
            tv_unknown.setHeight(180);
        } else if (ml <= 34) {
            tv_unknown.setHeight(220);
        } else {
            tv_unknown.setHeight(260);
        }


        tv_unknown.setText(unknownMsg);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmAndGoBack();
            }
        });

    }


}
