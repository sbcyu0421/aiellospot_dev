package ai.aiello.aiellospot.views.componets.homebtn.viewitems;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.AdapterView;
import android.widget.ListView;

import com.github.johnpersano.supertoasts.library.Style;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.events.module.NotificationStateChange;
import ai.aiello.aiellospot.modules.notification.NotificationModuleManager;
import ai.aiello.aiellospot.modules.notification.NotifyObject;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.activity.notification.NotifyAdapter;
import ai.aiello.aiellospot.views.activity.notification.NotifyDialog;
import ai.aiello.aiellospot.views.activity.notification.NotifyServiceRating;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.HomeBadgeButton;
import ai.aiello.aiellospot.views.componets.toast.MToaster;
import ai.aiello.lib_slidedown.SlideUp;
import ai.aiello.lib_slidedown.SlideUpBuilder;

public class NotificationViewItem extends AbstractViewItem {

    public static Thread notificationThread;
    UIHandler uiHandler;

    private NotifyDialog notifyDialog;
    private ArrayList<NotifyObject> tmpNotifyList;
    private NotifyAdapter adapter;

    private static NotificationViewItem instance;

    public static NotificationViewItem getInstance() {
        if (instance == null) {
            instance = new NotificationViewItem();
        }
        return instance;
    }

    private NotificationViewItem() {
    }

    @Override
    public void updateView() {
        updateBadge();
    }

    @Override
    public void handelBtnClickLogic() {
        wefBtn.get().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initNotifyView();
                slideUp.show();
            }
        });
    }


    @Override
    public void bindView(WeakReference<Activity> wefActivity, WeakReference<View> wefBtn) {
        super.bindView(wefActivity, wefBtn);
        setSlideDownView();
    }

    @Override
    public void unbindView(Activity activity) {
        super.unbindView(activity);
        if (notificationThread != null) {
            notificationThread.interrupt();
            notificationThread = null;
        }
        if (uiHandler != null) {
            uiHandler.removeCallbacksAndMessages(null);
            uiHandler = null;
        }

        notifyDialog = null;
    }

    @Override
    public View buildButton(Context context) {
        HomeBadgeButton homeBadgeButton = new HomeBadgeButton(context, this.width, this.height);
        homeBadgeButton.updateImage(R.drawable.icon_home1_notifiy);
        homeBadgeButton.updateText(R.string.notify_title);
        homeBadgeButton.getBadge().setVisibility(View.VISIBLE);
        return homeBadgeButton;
    }

    static class UIHandler extends Handler {

        Activity activity;

        UIHandler(WeakReference<Activity> reference) {
            activity = reference.get();
        }

        @Override
        public void handleMessage(Message msg) {
        }
    }

    private void updateBadge() {
        Animation amTranslate = new ScaleAnimation(0.0f, 1.2f, 0.0f, 1.2f);
        //setDuration (long durationMillis) 設定動畫開始到結束的執行時間
        amTranslate.setDuration(375);
        //setRepeatCount (int repeatCount) 設定重複次數
        amTranslate.setRepeatCount(0);
        amTranslate.setFillAfter(true);
        ((HomeBadgeButton) this.wefBtn.get()).getBadge().startAnimation(amTranslate);
        int mCount = NotificationModuleManager.getUnReadCount();
        ((HomeBadgeButton) this.wefBtn.get()).getBadge().setNumber(mCount);
    }

    private void initNotifyView() {
        ListView listView = wefActivity.get().findViewById(R.id.ntf_listview);
        LayoutInflater inflater = (LayoutInflater) wefActivity.get().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ArrayList<NotifyObject> notifyList = NotificationModuleManager.getNotificationList();
        tmpNotifyList = new ArrayList<>(notifyList);
        Collections.reverse(tmpNotifyList);

        if (tmpNotifyList.size() != 0) {
            adapter = new NotifyAdapter(tmpNotifyList, inflater);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(onClickListView);
        }
    }

    private AdapterView.OnItemClickListener onClickListView = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            switch (tmpNotifyList.get(position).getIntentName()) {

                case NotificationModuleManager.TYPE_BROADCAST_MSG:

                    tmpNotifyList.get(position).setRead(true);
                    adapter.notifyDataSetChanged();
                    NotificationModuleManager.notifyUpdateView();

                    if (notifyDialog != null && notifyDialog.isShowing()) {
                        notifyDialog.dismiss();
                        notifyDialog = null;
                    }

                    notifyDialog = new NotifyDialog(wefActivity.get(), R.style.CustomProgressDialog);
                    notifyDialog.setContent(tmpNotifyList.get(position).getTitle(), tmpNotifyList.get(position).getContent(), -1);
                    notifyDialog.show();

                    break;

                case NotificationModuleManager.TYPE_BROADCAST_RATING:
                    //this is rating msg
                    if (tmpNotifyList.get(position).isRead()) {
                        MToaster.showButtonToast(wefActivity.get(), ChatApplication.context.getString(R.string.notify_rating_ban), Style.TYPE_STANDARD);
                        return;
                    }
                    tmpNotifyList.get(position).setRead(true);
                    adapter.notifyDataSetChanged();
                    NotificationModuleManager.notifyUpdateView();
                    Intent intent = new Intent(wefActivity.get(), NotifyServiceRating.class);
                    intent.putExtra(NotifyObject.INTENT, tmpNotifyList.get(position));
                    wefActivity.get().startActivity(intent);
                    wefActivity.get().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    wefActivity.get().finish();
                    break;

            }
        }
    };

    private SlideUp slideUp;

    public void setSlideDownView() {
        View sliderView = wefActivity.get().findViewById(R.id.slideup);
        sliderView.setVisibility(View.VISIBLE);
        View dim = wefActivity.get().findViewById(R.id.dim);
        dim.setVisibility(View.VISIBLE);
        slideUp = new SlideUpBuilder(sliderView, ChatApplication.printLog)
                .withListeners(new SlideUp.Listener.Events() {
                    @Override
                    public void onSlide(float percent) {
                        dim.setAlpha(1 - (percent / 100));
                    }

                    @Override
                    public void onVisibilityChanged(int visibility) {
                    }
                })
                .withTouchableAreaDp(1000.0f)
                .withStartGravity(Gravity.TOP)
                .withLoggingEnabled(false) //do not show log event
                .withStartState(SlideUp.State.HIDDEN)
                .withSlideFromOtherView(wefActivity.get().findViewById(R.id.rootView))
                .build();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(NotificationStateChange event) {
        updateBadge();
    }

}
