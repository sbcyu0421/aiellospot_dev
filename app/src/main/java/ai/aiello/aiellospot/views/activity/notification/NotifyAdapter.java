package ai.aiello.aiellospot.views.activity.notification;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.modules.notification.NotifyObject;
import ai.aiello.aiellospot.utlis.Formatter;
import android.graphics.Color;
import android.graphics.Typeface;
import com.litesuits.android.log.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by a1990 on 2019/5/15.
 */

public class NotifyAdapter extends BaseAdapter {

    private String TAG = NotifyAdapter.class.getSimpleName();
    private ArrayList<NotifyObject> notifyList;
    private LayoutInflater inflater;
    private String readColor1 = "#7f7f7f";
    private String readColor2 = "#ffffff";


    public NotifyAdapter(ArrayList<NotifyObject> notifyList, LayoutInflater inflater) {
        this.notifyList = notifyList;
        this.inflater = inflater;
    }

    static class ViewHolder {
        TextView ntf_title;
        TextView ntf_content;
        TextView ntf_date;
        TextView ntf_time;
        LinearLayout ly_item;
    }


    @Override
    public int getCount() {
        return notifyList.size();
    }

    @Override
    public Object getItem(int position) {
        return notifyList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        //當ListView被拖拉時會不斷觸發getView，為了避免重複加載必須加上這個判斷
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.activity_notify_item, null);
            holder.ntf_title = convertView.findViewById(R.id.ntf_title);
            holder.ntf_content = convertView.findViewById(R.id.ntf_content);
            holder.ntf_date = convertView.findViewById(R.id.ntf_date);
            holder.ntf_time = convertView.findViewById(R.id.ntf_time);
            holder.ly_item = convertView.findViewById(R.id.ly_item);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        int month = notifyList.get(position).getDate().getMonth();
        int day = notifyList.get(position).getDate().getDate();
        int hour = notifyList.get(position).getDate().getHours();
        int minute = notifyList.get(position).getDate().getMinutes();

        holder.ntf_title.setText(notifyList.get(position).getTitle());
        holder.ntf_content.setText(notifyList.get(position).getContent());
        holder.ntf_date.setText(Formatter.binaryFormat(month + 1) + "/" + Formatter.binaryFormat(day));
        holder.ntf_time.setText(Formatter.binaryFormat(hour) + "：" + Formatter.binaryFormat(minute));

        Log.d(TAG, "position=" + position);

        boolean read = notifyList.get(position).isRead();

        if (read) {
            holder.ntf_title.setTypeface(holder.ntf_title.getTypeface(), Typeface.ITALIC);
            holder.ntf_content.setTypeface(holder.ntf_title.getTypeface(), Typeface.ITALIC);
            holder.ntf_date.setTypeface(holder.ntf_title.getTypeface(), Typeface.ITALIC);
            holder.ntf_time.setTypeface(holder.ntf_title.getTypeface(), Typeface.ITALIC);

            holder.ntf_title.setTextColor(Color.parseColor(readColor1));
            holder.ntf_content.setTextColor(Color.parseColor(readColor1));
            holder.ntf_date.setTextColor(Color.parseColor(readColor1));
            holder.ntf_time.setTextColor(Color.parseColor(readColor1));
        } else {
            holder.ntf_title.setTypeface(holder.ntf_title.getTypeface(), Typeface.NORMAL);
            holder.ntf_content.setTypeface(holder.ntf_title.getTypeface(), Typeface.NORMAL);
            holder.ntf_date.setTypeface(holder.ntf_title.getTypeface(), Typeface.NORMAL);
            holder.ntf_time.setTypeface(holder.ntf_title.getTypeface(), Typeface.NORMAL);

            holder.ntf_title.setTextColor(Color.parseColor(readColor2));
            holder.ntf_content.setTextColor(Color.parseColor(readColor2));
            holder.ntf_date.setTextColor(Color.parseColor(readColor2));
            holder.ntf_time.setTextColor(Color.parseColor(readColor2));
        }


        return convertView;
    }


}
