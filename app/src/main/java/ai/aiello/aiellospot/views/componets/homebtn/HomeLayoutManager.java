package ai.aiello.aiellospot.views.componets.homebtn;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.core.config.Configer;
import ai.aiello.aiellospot.modules.showcase.ShowcaseManager;
import ai.aiello.aiellospot.views.componets.homebtn.viewitems.BluetoothViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.viewitems.GoPage1ViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.viewitems.GoPage2ViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.viewitems.HeroAlarmViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.viewitems.ShowCaseViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.viewitems.MicViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.viewitems.RoomStatusDndViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.viewitems.RoomStatusMurViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.viewitems.ScreenOffViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.viewitems.SettingsViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.viewitems.SurroundingCuisineViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.viewitems.SurroundingLivingViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.viewitems.SurroundingSightsViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.viewitems.SurroundingTransportViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.viewitems.TaxiViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.viewitems.UserAlarmViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.viewitems.CheckOutViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.viewitems.IoTViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.viewitems.MusicViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.viewitems.NotificationViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.viewitems.VoipViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.viewitems.WifiViewItem;

public class HomeLayoutManager {

    private static String TAG = HomeLayoutManager.class.getSimpleName();

    private static HomeLayoutManager instance = null;

    private ArrayList<AbstractViewItem> layoutItems = new ArrayList<>();

    public static Thread reconnectThread;

    public static HomeLayoutManager getInstance () {
        if (instance == null) {
            instance = new HomeLayoutManager();
        }
        return instance;
    }

    public static int heightUnit;
    public static int widthUnit;

    private HomeLayoutManager() {}

    public void initLayoutData() {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        heightUnit = displayMetrics.heightPixels / 4;
        widthUnit = displayMetrics.widthPixels / 8 + 9; // width difference of app window and device window => add extra width

        JSONObject layoutData = Configer.layoutConfigData;
        Log.d(TAG, "initLayoutData: json = " + layoutData.toString());
        JSONArray layoutNames = layoutData.names();
        layoutItems = new ArrayList<>();
        Log.d(TAG, "initLayoutData: layoutNames = " + layoutNames);
        for (int i = 0; i < layoutNames.length(); i ++) {
            AbstractViewItem item = null;
            try {
                item = parseToViewItem(layoutData.getJSONObject((String) layoutNames.get(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (item != null) { layoutItems.add(item); }

        }
        Log.d(TAG, "initLayoutData: " + layoutItems.toString());


    }

    public ArrayList<AbstractViewItem> getLayoutItems() {
        Log.d(TAG, "getLayoutItems size: " + layoutItems.size());
        return layoutItems;
    }

    public static AbstractViewItem parseToViewItem(JSONObject layoutJsonObj) throws JSONException {
        AbstractViewItem item = null;
        boolean display = layoutJsonObj.getBoolean("display");
        if (!display) return null;

        String layoutType = layoutJsonObj.getString("type");
        String moduleName = layoutJsonObj.getString("module");
        String layoutName = layoutJsonObj.getString("name");
        int page = layoutJsonObj.getInt("page");
        String bgColor = layoutJsonObj.getString("bg_color");
        JSONArray tmpJSONArray = layoutJsonObj.getJSONArray("position");

        ArrayList<Integer> position = new ArrayList<>();
        for (int j = 0; j < tmpJSONArray.length(); j ++) {
            position.add(tmpJSONArray.getInt(j));
        }


        if (layoutType.equals("module")){           // module features
            switch (layoutName) {
                case "music":
                    MusicViewItem.getInstance().buildLayoutInfo(moduleName, layoutName, page, position, bgColor);
                    item = MusicViewItem.getInstance();
                    break;


                case "alarm":
                    UserAlarmViewItem.getInstance().buildLayoutInfo(moduleName, layoutName, page, position, bgColor);
                    item = UserAlarmViewItem.getInstance();;
                    break;

                case "hero-alarm":
                    HeroAlarmViewItem.getInstance().buildLayoutInfo(moduleName, layoutName, page, position, bgColor);
                    item = HeroAlarmViewItem.getInstance();;
                    break;

                case "checkout":
                    CheckOutViewItem.getInstance().buildLayoutInfo(moduleName, layoutName, page, position, bgColor);
                    item = CheckOutViewItem.getInstance();
                    break;

                case "iot":
                    IoTViewItem.getInstance().buildLayoutInfo(moduleName, layoutName, page, position, bgColor);
                    item = IoTViewItem.getInstance();
                    break;


                case "notification":
                    NotificationViewItem.getInstance().buildLayoutInfo(moduleName, layoutName, page, position, bgColor);
                    item = NotificationViewItem.getInstance();
                    break;


                case "roomstatus-dnd":
                    RoomStatusDndViewItem.getInstance().buildLayoutInfo(moduleName, layoutName, page, position, bgColor);
                    item = RoomStatusDndViewItem.getInstance();
                    break;

                case "roomstatus-mur":
                    RoomStatusMurViewItem.getInstance().buildLayoutInfo(moduleName, layoutName, page, position, bgColor);
                    item = RoomStatusMurViewItem.getInstance();
                    break;

                case "surrounding-cusine":
                    SurroundingCuisineViewItem.getInstance().buildLayoutInfo(moduleName, layoutName, page, position, bgColor);
                    item = SurroundingCuisineViewItem.getInstance();;
                    break;

                case "surrounding-sights":
                    SurroundingSightsViewItem.getInstance().buildLayoutInfo(moduleName, layoutName, page, position, bgColor);
                    item = SurroundingSightsViewItem.getInstance();;
                    break;

                case "surrounding-transport":
                    SurroundingTransportViewItem.getInstance().buildLayoutInfo(moduleName, layoutName, page, position, bgColor);
                    item = SurroundingTransportViewItem.getInstance();;
                    break;

                case "surrounding-living":
                    SurroundingLivingViewItem.getInstance().buildLayoutInfo(moduleName, layoutName, page, position, bgColor);
                    item = SurroundingLivingViewItem.getInstance();
                    break;

                case "taxi":
                    TaxiViewItem.getInstance().buildLayoutInfo(moduleName, layoutName, page, position, bgColor);
                    item = TaxiViewItem.getInstance();
                    break;

                case "voip":
                    VoipViewItem.getInstance().buildLayoutInfo(moduleName, layoutName, page, position, bgColor);
                    item = VoipViewItem.getInstance();
                    break;

                case "showcase":
                    ShowCaseViewItem.getInstance().buildLayoutInfo(moduleName, layoutName, page, position, bgColor);
                    item = ShowCaseViewItem.getInstance();
                    break;

                case "Comments":
                case "Complaint":
                case "Delivery":
                case "FAQ":
                case "Ads":
                case "Repair":
                case "Roobo":
                case "News":
                    // no btn can be shown
                    break;
            }
        } else if (layoutType.equals("system")) {   // basic(settings) features
            switch (layoutName) {
                case "setting":
                    SettingsViewItem.getInstance().buildLayoutInfo(moduleName, layoutName, page, position, bgColor);
                    item = SettingsViewItem.getInstance();
                    break;
                case "screen-off":
                    ScreenOffViewItem.getInstance().buildLayoutInfo(moduleName, layoutName, page, position, bgColor);
                    item = ScreenOffViewItem.getInstance();
                    break;
                case "wifi":
                    WifiViewItem.getInstance().buildLayoutInfo(moduleName, layoutName, page, position, bgColor);
                    item = WifiViewItem.getInstance();
                    break;
                case "bluetooth":
                    BluetoothViewItem.getInstance().buildLayoutInfo(moduleName, layoutName, page, position, bgColor);
                    item = BluetoothViewItem.getInstance();
                    break;
                case "notification":
                    NotificationViewItem.getInstance().buildLayoutInfo(moduleName, layoutName, page, position, bgColor);
                    item = NotificationViewItem.getInstance();
                    break;
                case "microphone":
                    MicViewItem.getInstance().buildLayoutInfo(moduleName, layoutName, page, position, bgColor);
                    item = MicViewItem.getInstance();
                    break;
                case "go-page1":
                    GoPage1ViewItem.getInstance().buildLayoutInfo(moduleName, layoutName, page, position, bgColor);
                    item = GoPage1ViewItem.getInstance();
                    break;
                case "go-page2":
                    GoPage2ViewItem.getInstance().buildLayoutInfo(moduleName, layoutName, page, position, bgColor);
                    item = GoPage2ViewItem.getInstance();
                    break;
            }
        }

        if (item == null) {
            Log.d(TAG, "parseToViewItem: No module matches");
            return null;
        } else return item;
    }

    public static void setBackground(View view, String color) {
        switch (color) {
            case "color1":
                view.setBackground(ChatApplication.context.getDrawable(R.drawable.btn_ripple1));
                break;

            case "color2":
                view.setBackground(ChatApplication.context.getDrawable(R.drawable.btn_ripple2));
                break;

            case "color3":
                view.setBackground(ChatApplication.context.getDrawable(R.drawable.btn_ripple3));
                break;

            case "color4":
                view.setBackground(ChatApplication.context.getDrawable(R.drawable.btn_ripple4));
                break;

            case "color5":
                view.setBackground(ChatApplication.context.getDrawable(R.drawable.btn_ripple5));
                break;

            case "color6":
                view.setBackground(ChatApplication.context.getDrawable(R.drawable.btn_ripple6));
                break;

        }
    }

}
