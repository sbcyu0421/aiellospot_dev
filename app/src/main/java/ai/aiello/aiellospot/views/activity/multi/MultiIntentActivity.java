package ai.aiello.aiellospot.views.activity.multi;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.core.device.DeviceControl;
import ai.aiello.aiellospot.events.system.SystemEvent;
import ai.aiello.aiellospot.views.activity.CardActivity;
import ai.aiello.aiellospot.views.activity.ads.StandbyActivity;
import ai.aiello.aiellospot.intents.IntentManager;
import ai.aiello.aiellospot.intents.IntentObject;
import ai.aiello.aiellospot.intents.sdui.ActionManager;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.core.media.MsttsManager;
import ai.aiello.aiellospot.modules.music.MusicServiceModuleManager;

import android.app.LocalActivityManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;

import androidx.viewpager.widget.ViewPager;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

public class MultiIntentActivity extends CardActivity {

    private List<Bitmap> mScreenshots;
    private LocalActivityManager manager;
    private Intent intent_member;
    private ViewPagerAdapter viewPagerAdapter;
    private ViewPager viewPager;
    private static String TAG = MultiIntentActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_intent);
        //init screenshot
        viewPager = findViewById(R.id.viewPager);
        manager = new LocalActivityManager(this, true);
        manager.dispatchCreate(savedInstanceState);
        mScreenshots = new ArrayList<>();

        //show multi-preview
        int showIntentNum = IntentManager.finishIntentList.size();
        for (int i = 0; i < showIntentNum; i++) {
            IntentObject io = IntentManager.finishIntentList.get(i);
            intent_member = new Intent(MultiIntentActivity.this, io.getaClass());
            intent_member.putExtra(IntentManager.BUNDLE_TAG, io);
            intent_member.putExtra("Multi_Preview", "");
            Bitmap tmp = getViewBitmap(manager.startActivity(String.valueOf(i), intent_member).getDecorView());
            mScreenshots.add(setShadow(tmp));
            manager.dispatchPause(true);
            manager.dispatchStop();
            manager.destroyActivity(String.valueOf(i), true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (IntentManager.finishIntentList.size() == 0) {
                if (ActivityLauncher.getBaseActivity().equals(StandbyActivity.class.getName())) {
                    ActivityLauncher.launchActivityByClass(this, ActivityLauncher.currentHomeName, R.anim.fade_in, R.anim.fade_out);
                } else {
                    ActivityLauncher.launchActivityByName(this, ActivityLauncher.getBaseActivity(), R.anim.fade_in, R.anim.fade_out);
                }
            } else {
                updateAdapter();
            }
        } catch (Exception e) {
            e.printStackTrace();
            ActivityLauncher.launchActivityByClass(this, ActivityLauncher.currentHomeName, R.anim.fade_in, R.anim.fade_out);
            IntentManager.finishIntentList.clear();
        }

    }

    public void updateAdapter() {

        viewPagerAdapter = new ViewPagerAdapter(mScreenshots, this, new ViewPagerAdapter.ItemEventListener() {
            @Override
            public void itemClick(IntentObject io) {
                IntentManager.finishIntentList.remove(io);
                Intent intent1 = new Intent(MultiIntentActivity.this, io.getaClass());
                intent1.putExtra(getResources().getString(R.string.multi_intent), getResources().getString(R.string.multi_intent));
                intent1.putExtra(IntentManager.BUNDLE_TAG, io);
                startActivityForResult(intent1, 0);
                finish();
            }

            @Override
            public void itemSwipe(int position) {
                IntentManager.finishIntentList.remove(position);
                mScreenshots.get(position).recycle();
                mScreenshots.remove(position);
                updateAdapter();
            }

            @Override
            public void itemEmpty() {
                IntentManager.finishIntentList.clear();
                if (ActivityLauncher.isFromStandbyActivity()) {
                    ActivityLauncher.launchActivityByClass(MultiIntentActivity.this, ActivityLauncher.currentHomeName, R.anim.fade_in, R.anim.fade_out);
                } else {
                    ActivityLauncher.launchActivityByName(MultiIntentActivity.this, ActivityLauncher.getBaseActivity(), R.anim.fade_in, R.anim.fade_out);
                }
                ActionManager.getInstance().stopForegroundAction();
                MsttsManager.getInstance().stopTTS();
            }
        });

        viewPager.setAdapter(viewPagerAdapter);

    }

    public Bitmap getViewBitmap(View view) {
        //Get the dimensions of the view so we can re-layout the view at its current size
        //and create a bitmap of the same size

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        int measuredWidth = View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY);
        int measuredHeight = View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.EXACTLY);

        //Cause the view to re-layout
        view.measure(measuredWidth, measuredHeight);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        //Create a bitmap backed Canvas to draw the view into
        Bitmap b = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);

        //Now that the view is laid out and we have a canvas, ask the view to draw itself into the canvas
        view.draw(c);
        return b;
    }


    @Override
    protected void onDestroy() {
        Log.d(TAG, "multi-intent onDestroy");
        viewPager.setAdapter(null);

        for (Bitmap bm : mScreenshots) {
            bm.recycle();
        }
        mScreenshots.clear();
//        System.gc();
        super.onDestroy();
    }

    public static Bitmap setShadow(Bitmap src) {
        Bitmap bmOut = Bitmap.createBitmap(src.getWidth(), src.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bmOut);
        canvas.drawColor(0, PorterDuff.Mode.CLEAR);
        Paint ptBlur = new Paint();
        ptBlur.setMaskFilter(new BlurMaskFilter(20, BlurMaskFilter.Blur.NORMAL));
        int[] offsetXY = new int[2];
        Bitmap bmAlpha = src.extractAlpha(ptBlur, offsetXY);
        Paint ptAlphaColor = new Paint();
        ptAlphaColor.setColor(Color.rgb(26, 24, 22));
        canvas.drawBitmap(bmAlpha, offsetXY[0], offsetXY[1], ptAlphaColor);
        bmAlpha.recycle();
        canvas.drawBitmap(src, -10, -10, null);
        return bmOut;
    }


    @Override
    public void confirmAndGoBack() {
        ActivityLauncher.launchActivityByClass(this, ActivityLauncher.currentHomeName, R.anim.fade_in, R.anim.fade_out);
        ActionManager.getInstance().stopForegroundAction();
        MsttsManager.getInstance().stopTTS();
        DeviceControl.volumeResume();
    }
}
