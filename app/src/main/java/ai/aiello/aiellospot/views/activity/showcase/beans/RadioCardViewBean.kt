package ai.aiello.aiellospot.views.activity.showcase.beans

import org.json.JSONArray

class RadioCardViewBean(
    feature_type: String,
    unikey: String,
    title: String,
    radioRawData: String
) : HotelFeatureBean(feature_type, unikey, title) {

    val radioAlbums = ArrayList<RadioAlbumData>()

    override fun getData(): ArrayList<RadioAlbumData> {
        return radioAlbums
    }

    init {
        val jsonArray = JSONArray(radioRawData)

        for (i in 0 until jsonArray.length()) {
            val temp = jsonArray.getJSONObject(i)

            val tracksData = temp.getJSONArray("song_list").toString()
            val uuid = temp.getString("uuid")
            val title1 = temp.getJSONObject("title").toString()
            val content = temp.getJSONObject("content").toString()
            val imgURL = temp.getString("img")

            val radioAlbum = RadioAlbumData(uuid, title1, content, tracksData, imgURL)
            radioAlbums.add(radioAlbum)
        }
    }
}