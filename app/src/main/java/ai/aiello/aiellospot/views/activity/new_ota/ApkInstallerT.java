package ai.aiello.aiellospot.views.activity.new_ota;


import ai.aiello.aiellospot.core.log.SpotDeviceLog;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageInstaller;

import com.litesuits.android.log.Log;
import com.litesuits.common.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

public class ApkInstallerT {


    private static final String TAG = ApkInstallerT.class.getSimpleName();

    public static void installAPkSilently(Activity activity, File apkFile, ServiceAPK serviceAPK) {

        String packageName = serviceAPK.getPackageName();
        Log.e(TAG, "ready to install package, name = " + packageName);
        SpotDeviceLog.info(TAG,"","Ready to install package name:"+ packageName);
        // httpGet PackageInstaller from PackageManager
        PackageInstaller packageInstaller = activity.getPackageManager().getPackageInstaller();
        // Prepare params for installing one APK file with MODE_FULL_INSTALL
        PackageInstaller.SessionParams params = new PackageInstaller.SessionParams(PackageInstaller.SessionParams.MODE_FULL_INSTALL);
        params.setAppPackageName(packageName);

        // Create PackageInstaller.Session for performing the actual update
        try {
            int sessionID = packageInstaller.createSession(params);
            PackageInstaller.Session session = packageInstaller.openSession(sessionID);

            // Copy APK file bytes into OutputStream provided by install Session
            OutputStream out = session.openWrite(packageName, 0, -1);
            FileInputStream fis = new FileInputStream(apkFile);
            IOUtils.copy(fis, out);
            fis.close();
            out.close();

            Log.d(TAG, "apk install = " + serviceAPK.getPackageName());
            if (serviceAPK.getPackageName().equals(activity.getApplicationContext().getPackageName())) {

                PendingIntent pendingIntent = PendingIntent.getBroadcast(
                        activity,
                        sessionID,
                        new Intent(),
                        0);
                IntentSender intentSender = pendingIntent.getIntentSender();
                session.commit(intentSender);

            } else {
                Intent i = new Intent("SERVICE_INSTALL_COMPLETE");
                i.putExtra("service_name", packageName);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(
                        activity,
                        sessionID,
                        i,
                        0);

                IntentSender intentSender = pendingIntent.getIntentSender();
                session.commit(intentSender);
            }

        } catch (Exception e) {
          SpotDeviceLog.exception(TAG,"",e.getMessage());
            Log.e(TAG, e.toString());
        }

    }


    public static void uninstallApk(Activity activity, String name) {
        /* ignore ai.aiello.aiellospot*/
        try {
            if (name.equals(activity.getPackageName())) {
                return;
            }
            Log.d(TAG, "uninstallApk=" + name);
            Intent intent = new Intent(Intent.ACTION_DELETE);
            PendingIntent sender = PendingIntent.getActivity(activity, 0, intent, 0);
            PackageInstaller mPackageInstaller = activity.getPackageManager().getPackageInstaller();
            mPackageInstaller.uninstall(name, sender.getIntentSender());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
