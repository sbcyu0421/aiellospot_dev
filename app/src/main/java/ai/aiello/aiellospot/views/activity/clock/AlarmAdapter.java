package ai.aiello.aiellospot.views.activity.clock;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.utlis.Formatter;

/**
 * Created by a1990 on 2019/5/15.
 */

public class AlarmAdapter extends BaseAdapter {

    private String TAG = AlarmAdapter.class.getSimpleName();
    private ArrayList<Date> alarmList;
    private LayoutInflater inflater;
    private Context context;
    private int maxNum;
    private OnItemClick onItemClick;


    public AlarmAdapter(ArrayList<Date> notifyList, Context context, int maxNum, OnItemClick onItemClick) {
        this.alarmList = notifyList;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.maxNum = maxNum;
        this.onItemClick = onItemClick;
    }

    static class ViewHolder {
        TextView tv_alarm_md;
        TextView tv_alarm_hm;
        TextView tv_alarm_week;
        TextView tv_alarm_delete;
        TextView img_alarm_add;
        LinearLayout block;
        RelativeLayout rl_alarm;
    }


    @Override
    public int getCount() {
//        return alarmList.size();
        return maxNum;
    }

    @Override
    public Object getItem(int position) {
        return alarmList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_alarm, null);
            holder.tv_alarm_md = convertView.findViewById(R.id.tv_alarm_md);
            holder.tv_alarm_hm = convertView.findViewById(R.id.tv_alarm_hm);
            holder.tv_alarm_week = convertView.findViewById(R.id.tv_alarm_week);
            holder.tv_alarm_delete = convertView.findViewById(R.id.tv_alarm_delete);
            holder.img_alarm_add = convertView.findViewById(R.id.img_alarm_add);
            holder.block = convertView.findViewById(R.id.block);
            holder.rl_alarm = convertView.findViewById(R.id.rl_alarm);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.rl_alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick.onBlockClick(position);
            }
        });

        holder.tv_alarm_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onDelete(position);
            }
        });

        if (position < alarmList.size()) {
            Date date = alarmList.get(position);
            DateFormat format = new SimpleDateFormat("EEE");
            holder.tv_alarm_md.setText(Formatter.binaryFormat(date.getMonth() + 1) + "/" + Formatter.binaryFormat(date.getDate()));
            holder.tv_alarm_hm.setText(Formatter.binaryFormat(date.getHours()) + "：" + Formatter.binaryFormat(date.getMinutes()));
            holder.tv_alarm_hm.setTextSize(60);
            holder.tv_alarm_week.setText(format.format(date));
            holder.tv_alarm_delete.setVisibility(View.VISIBLE);
            holder.tv_alarm_md.setVisibility(View.VISIBLE);
            holder.img_alarm_add.setVisibility(View.GONE);
            holder.block.setVisibility(View.VISIBLE);
        } else {
            holder.tv_alarm_hm.setTextSize(30);
            holder.tv_alarm_hm.setText(context.getString(R.string.alarm_adding));
            holder.img_alarm_add.setVisibility(View.VISIBLE);
            holder.tv_alarm_delete.setVisibility(View.GONE);
            holder.tv_alarm_md.setVisibility(View.GONE);
            holder.block.setVisibility(View.GONE);
        }

        return convertView;
    }

    interface OnItemClick {
        void onDelete(int position);

        void onBlockClick(int position);
    }

}
