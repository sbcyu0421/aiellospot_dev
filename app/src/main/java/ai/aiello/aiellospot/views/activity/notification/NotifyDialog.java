package ai.aiello.aiellospot.views.activity.notification;

import ai.aiello.aiellospot.R;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;

public class NotifyDialog extends Dialog implements View.OnClickListener {

    private TextView notify_card_title,notify_card_content;
    private ImageView notify_img;
    private TextView tv_notify_confirm;

    public NotifyDialog(Context context, int theme) {
        super(context, theme);
        setOption();

    }

    private void setOption() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.notify_detail_dialog);
        notify_card_title = this.findViewById(R.id.notify_card_title);
        notify_card_content = this.findViewById(R.id.notify_card_content);
        notify_img = this.findViewById(R.id.notify_img);
        tv_notify_confirm = this.findViewById(R.id.tv_notify_confirm);
        setCancelable(true);

        tv_notify_confirm.setOnClickListener(this);
    }


    public void setContent(String title_msg, String content_msg, int imgId) {
        if (imgId != -1) {
            notify_img.setImageResource(imgId);
        }
        notify_card_title.setText(title_msg);
        notify_card_content.setText(content_msg);

        this.getWindow().setGravity(Gravity.CENTER);
        this.getWindow().setLayout(1000, 580); //Controlling width and height.
//        this.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
    }


    @Override
    public void show() {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN;
        super.show();
        this.getWindow().getDecorView().setSystemUiVisibility(uiOptions);
        this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
    }


    @Override
    public void onClick(View v) {
        this.dismiss();
    }
}
