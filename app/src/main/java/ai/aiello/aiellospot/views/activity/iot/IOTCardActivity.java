package ai.aiello.aiellospot.views.activity.iot;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.CardActivity;

public class IOTCardActivity extends CardActivity {


    private TextView tv_qa;
    private LinearLayout btn_ok;
    private String iotMsg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activtiy_iot_card);
        tv_qa = findViewById(R.id.tv_roobo_msg);
        btn_ok = findViewById(R.id.btn_ok);

        iotMsg = intentObject.getMsg1();

        int ml = iotMsg.length();

        if (ml <= 17) {
            tv_qa.setHeight(180);
        } else if (ml <= 34) {
            tv_qa.setHeight(220);
        } else {
            tv_qa.setHeight(260);
        }

        tv_qa.setText(iotMsg);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmAndGoBack();
            }


        });

    }


}
