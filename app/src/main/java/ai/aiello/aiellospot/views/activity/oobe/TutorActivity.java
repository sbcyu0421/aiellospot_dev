package ai.aiello.aiellospot.views.activity.oobe;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.events.system.WakeUpEvent;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.core.media.AielloSoundPool;
import ai.aiello.aiellospot.core.config.UIConfig;
import ai.aiello.aiellospot.core.device.DeviceControl;
import ai.aiello.aiellospot.core.info.DeviceInfo;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;

import ai.aiello.aiellospot.core.soundai.SoundAIManager;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.*;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.litesuits.android.log.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class TutorActivity extends Activity {

    private static String TAG = TutorActivity.class.getSimpleName();

    private Animation translate_a1, translate_a2, translate_b1, translate_hint;
    private ConstraintLayout tutor_a1, tutor_a2;
    private ConstraintLayout tutor_b1;
    private ScrollView scrol;


    private TextView tv_tutor_msg, tv_skip;
    private TextView tv_tutor_a1, tv_tutor_b1;
    private TextView hint1, tv_confirm;

    private RelativeLayout rl_hint1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutor);
        SoundAIManager.setWakeUp(this, false);
        EventBus.getDefault().register(this);
        hideActivityController();
        initView();
    }


    public void initView() {

        tv_tutor_msg = findViewById(R.id.tv_tutor_msg);

        tv_confirm = findViewById(R.id.tv_confirm);
        tv_skip = findViewById(R.id.tv_skip);

        scrol = findViewById(R.id.scrol);
        tutor_a1 = findViewById(R.id.tutor_a1);
        tutor_a2 = findViewById(R.id.tutor_a2);
        tutor_b1 = findViewById(R.id.tutor_b1);
        tv_tutor_a1 = findViewById(R.id.tv_tutor_a1);
        tv_tutor_b1 = findViewById(R.id.tv_tutor_b1);
        rl_hint1 = findViewById(R.id.rl_hint1);
        hint1 = findViewById(R.id.hint1);

        //init status
        tv_tutor_b1.setTextColor(getResources().getColor(R.color.event_color));
        rl_hint1.setVisibility(View.INVISIBLE);
        scrol.setVisibility(View.INVISIBLE);
        tutor_a2.setVisibility(View.INVISIBLE);

        tv_tutor_a1.setText(String.format(getResources().getString(R.string.tutor_a1), DeviceInfo.assistantName));
        tv_tutor_b1.setText(String.format(getResources().getString(R.string.tutor_b1), DeviceInfo.assistantName));
        hint1.setText(String.format(getResources().getString(R.string.tutor_wait_hint), DeviceInfo.assistantName));

        tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPlaying();
                startSession1();
            }
        });


        tv_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPlaying();
                ActivityLauncher.launchActivityByClass(TutorActivity.this, Tutor2Activity.class, R.anim.fade_in, R.anim.fade_out);
            }
        });

        //disable scroll touch
        scrol.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });


        translate_a1 = new AlphaAnimation(0.0f, 1.0f);
        translate_a1.setDuration(1200);
        translate_a1.setRepeatCount(0);
        translate_a1.setFillAfter(true);

        translate_a2 = new AlphaAnimation(0.0f, 1.0f);
        translate_a2.setDuration(1200);
        translate_a2.setRepeatCount(0);
        translate_a2.setFillAfter(true);

        translate_b1 = new AlphaAnimation(0.0f, 1.0f);
        translate_b1.setDuration(1200);
        translate_b1.setRepeatCount(0);
        translate_b1.setFillAfter(true);

        translate_hint = new AlphaAnimation(0.0f, 1.0f);
        translate_hint.setDuration(1200);
        translate_hint.setRepeatCount(0);
        translate_hint.setFillAfter(true);

        playback(1);

    }

    public void playback(int session) {
        DeviceControl.tutorVol();
        try {
            String tutorAudio = null;
            String tutorText = null;

            //
            switch (ChatApplication.system_lang) {
                case en_US:
                    tutorAudio = UIConfig.wakupConfigBean.getTutorAudio().getEn();
                    tutorText = UIConfig.wakupConfigBean.getTutorText().getEn();
                    break;
                case zh_CN:
                    tutorAudio = UIConfig.wakupConfigBean.getTutorAudio().getCn();
                    tutorText = UIConfig.wakupConfigBean.getTutorText().getCn();
                    break;
                case japanese:
                    tutorAudio = UIConfig.wakupConfigBean.getTutorAudio().getJa();
                    tutorText = UIConfig.wakupConfigBean.getTutorText().getJa();
                    break;
                case zh_TW:
                default:
                    tutorAudio = UIConfig.wakupConfigBean.getTutorAudio().getTw();
                    tutorText = UIConfig.wakupConfigBean.getTutorText().getTw();
            }

            tv_tutor_msg.setText(String.format(tutorText, DeviceInfo.assistantName_voice));
//            AielloMediaPlayer.getInstance().play(tutorAudio, 0, this);

        } catch (Exception e) {
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e(TAG, e.toString());
        }
    }

    public void stopPlaying() {
        ChatApplication.tutor = true;
        SoundAIManager.setWakeUp(this, true);
        DeviceControl.defaultVol();
    }

    public void startSession1() {
        tv_tutor_msg.setVisibility(View.INVISIBLE);
        tv_confirm.setVisibility(View.INVISIBLE);
        tv_confirm.setEnabled(false);
        tv_skip.setVisibility(View.INVISIBLE);
        tv_skip.setEnabled(false);
        tutor_a1.startAnimation(translate_a1);
        tutor_b1.startAnimation(translate_a1);

        scrol.setVisibility(View.VISIBLE);
    }


    public void startSession2() {

        SoundAIManager.setWakeUp(this, false);
        ChatApplication.tutor = false;
        tv_tutor_b1.setTextColor(getResources().getColor(R.color.white));
        rl_hint1.setVisibility(View.VISIBLE);
        rl_hint1.startAnimation(translate_hint);
        translate_hint.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                scrol.smoothScrollTo(0, (int) rl_hint1.getY());
                AielloSoundPool.play(AielloSoundPool.vad_end);
                tutor_a2.setVisibility(View.VISIBLE);
                tutor_a2.startAnimation(translate_a2);
                translate_a2.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        ActivityLauncher.launchActivityByClass(TutorActivity.this, Tutor2Activity.class, R.anim.fade_in, R.anim.fade_out);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });


    }


    public void hideActivityController() {
        //hideNavController
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }


    //註冊喚醒
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(WakeUpEvent event) {
        startSession2();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, this.getLocalClassName() + "EventBus UnRegister");
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

//    //註冊重啟服務
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onMessageEvent(RestartEvent event) {
//
//        int action = event.getAction();
//        switch (action) {
//            case PushServiceConn.RX_APK_REBOOT:
//                SystemControl.rebootAPK(this);
//                break;
//            case PushServiceConn.RX_DEVICE_REBOOT:
//                SystemControl.rebootDevice();
//                break;
//            case PushServiceConn.RX_SILENT_RESTART:
//                SystemControl.restartSilently(this);
//                break;
//        }
//
//    }
//
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onMessageEvent(CheckInOutEvent event) {
//        if (event.getStatus() == CheckInOutEvent.checkOut) {
//            SystemControl.rebootAPK(this);
//        }
//    }

}

