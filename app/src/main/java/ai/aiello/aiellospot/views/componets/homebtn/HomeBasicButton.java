package ai.aiello.aiellospot.views.componets.homebtn;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;

import static android.widget.LinearLayout.HORIZONTAL;
import static android.widget.LinearLayout.VERTICAL;

public class HomeBasicButton extends HomeBaseButton {

    static String TAG = HomeBasicButton.class.getSimpleName();


    public HomeBasicButton(Context context, int width, int height) {
        super(context, width, height);
    }

    @Override
    int getLayoutXml() {
        return R.layout.button_home_basic;
    }

}
