package ai.aiello.aiellospot.views.activity.music;

import ai.aiello.aiellospot.modules.music.AlbumObject;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.litesuits.android.log.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import ai.aiello.aiellospot.R;

/**
 * Created by a1990 on 2019/3/21.
 */


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private final String TAG = "RecyclerViewAdapter";
    ArrayList<AlbumObject> albumList;
    Context mContext;

    private MOnClickListener mOnClickListener = null;

    public interface MOnClickListener  {
        void onClick(int position);
    }


    public RecyclerViewAdapter(Context context, ArrayList<AlbumObject> list, MOnClickListener onClickListener) {
        this.albumList = list;
        this.mOnClickListener = onClickListener;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_music_menu_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called.");

        Glide.with(holder.image.getContext())
                .load(albumList.get(position).getImg())
                .thumbnail(0.5f)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.image);

        holder.name.setText(albumList.get(position).getTitle());
        holder.feature.setText(albumList.get(position).getDescription());
        holder.album_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnClickListener.onClick(holder.getAdapterPosition());
            }
        });

    }


    @Override
    public int getItemCount() {
        return albumList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView name;
        TextView feature;
        RelativeLayout rl_grid;
        CardView album_card;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image_view);
            name = itemView.findViewById(R.id.singer_name);
            feature = itemView.findViewById(R.id.feature);
            rl_grid = itemView.findViewById(R.id.rl_grid);
            album_card = itemView.findViewById(R.id.album_card);
        }
    }

    @Override
    public void onViewRecycled(@NonNull ViewHolder holder) {
        super.onViewRecycled(holder);
        Glide.get(holder.image.getContext()).clearMemory();
    }
}