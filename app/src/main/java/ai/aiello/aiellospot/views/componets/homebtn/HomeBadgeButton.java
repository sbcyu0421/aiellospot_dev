package ai.aiello.aiellospot.views.componets.homebtn;

import android.content.Context;
import android.util.Log;
import android.widget.RelativeLayout;

import com.nex3z.notificationbadge.NotificationBadge;

import ai.aiello.aiellospot.R;

public class HomeBadgeButton extends HomeBaseButton {

    static String TAG = HomeBadgeButton.class.getSimpleName();

    private NotificationBadge badge;
    RelativeLayout.LayoutParams badgeParams;

    public HomeBadgeButton(Context context, int width, int height) {
        super(context, width, height);
    }

    @Override
    int getLayoutXml() {
        return R.layout.button_home_badge;
    }

    @Override
    public void initViews(Context context, int width, int height) {
        super.initViews(context, width, height);
        this.badge = btn.findViewById(R.id.badge);
        this.badgeParams = (RelativeLayout.LayoutParams) badge.getLayoutParams();
    }

    @Override
    public void applyLayoutParams() {
        super.applyLayoutParams();
        int size = (int)(height * 160 * 0.25);
        badgeParams.width = size;
        badgeParams.height = size;
        badge.setLayoutParams(badgeParams);
    }

    public NotificationBadge getBadge() {return badge;}
}
