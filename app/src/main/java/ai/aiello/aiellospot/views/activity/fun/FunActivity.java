package ai.aiello.aiellospot.views.activity.fun;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.CardActivity;
import ai.aiello.aiellospot.core.info.DeviceInfo;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FunActivity extends CardActivity {

    private LinearLayout btn_ok;
    private TextView tv_fun_content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fun);
        btn_ok = findViewById(R.id.btn_ok);
        tv_fun_content = findViewById(R.id.tv_fun_content);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmAndGoBack();
            }

        });

        tv_fun_content.setText(String.format(getResources().getString(R.string.fun_content), DeviceInfo.assistantName));

    }

}
