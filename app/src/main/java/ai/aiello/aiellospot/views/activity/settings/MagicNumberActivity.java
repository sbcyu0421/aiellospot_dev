package ai.aiello.aiellospot.views.activity.settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Gravity;
import android.view.View;

import com.github.johnpersano.supertoasts.library.Style;
import com.litesuits.android.log.Log;

import org.greenrobot.eventbus.EventBus;

import java.lang.ref.WeakReference;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.core.MCountdownTimer;
import ai.aiello.aiellospot.core.asr.ASRManager;
import ai.aiello.aiellospot.core.chatbot.ChatBot;
import ai.aiello.aiellospot.core.chatbot.ChatbotValidator;
import ai.aiello.aiellospot.core.config.APIConfig;
import ai.aiello.aiellospot.core.config.Configer;
import ai.aiello.aiellospot.core.config.SystemConfig;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.core.info.UserInfo;
import ai.aiello.aiellospot.core.system.AielloWifiManager;
import ai.aiello.aiellospot.core.system.SystemControl;
import ai.aiello.aiellospot.databinding.ActivityMagicNumberBinding;
import ai.aiello.aiellospot.events.server.ServerEvent;
import ai.aiello.aiellospot.events.system.SystemEvent;
import ai.aiello.aiellospot.modules.ads.AdsModuleManager;
import ai.aiello.aiellospot.modules.iot.IoTModuleManager;
import ai.aiello.aiellospot.modules.voip.Phonebook;
import ai.aiello.aiellospot.utlis.AielloFileUtils;
import ai.aiello.aiellospot.utlis.TestHub;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.activity.BaseActivity;
import ai.aiello.aiellospot.views.activity.dialog.AielloAlertDialog;
import ai.aiello.aiellospot.views.activity.dialog.AielloProgressDialog;
import ai.aiello.aiellospot.views.activity.new_ota.VersionCheckerT;
import ai.aiello.aiellospot.views.activity.voip.VoipDialActivity;
import ai.aiello.aiellospot.views.componets.toast.MToaster;

public class MagicNumberActivity extends BaseActivity implements View.OnClickListener {

    static String TAG = MagicNumberActivity.class.getSimpleName();

    private ActivityMagicNumberBinding binding;
    private String makeCallNo = "";
    private String dial_Number = "";
    private AielloProgressDialog aielloProgressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMagicNumberBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        MToaster.showButtonToast(this, this.getString(R.string.enter_rd_mode), Style.TYPE_STANDARD);

        binding.etDial.setFocusableInTouchMode(false);
        binding.etDial.setFocusable(true);

        binding.lyBack.setOnClickListener(this);
        binding.lyDial.setOnClickListener(this);
        binding.nkey0.setOnClickListener(this);
        binding.nkey1.setOnClickListener(this);
        binding.nkey2.setOnClickListener(this);
        binding.nkey3.setOnClickListener(this);
        binding.nkey4.setOnClickListener(this);
        binding.nkey5.setOnClickListener(this);
        binding.nkey6.setOnClickListener(this);
        binding.nkey7.setOnClickListener(this);
        binding.nkey8.setOnClickListener(this);
        binding.nkey9.setOnClickListener(this);
        binding.nkeyAsterisk.setOnClickListener(this);
        binding.nkeyPound.setOnClickListener(this);
        binding.imgDelete.setOnClickListener(this);

        String info = "Reboot Spot APK: ##886886##\n" +
                "Open Android Wifi setting: ***1000***\n" +
                "Open Android BT Setting: ***0100***\n" +
                "Open Android Setting: ***0010***\n" +
                "Change ASR  ***4801***\n" +
                "APK Product List: ***4802***\n" +
                "APK Beta List: ***4803***\n" +
                "Show Advertisement: ***4804***\n" +
                "Update Firmware: ***5801***\n" +
                "Device Reboot: ***6801***\n" +
                "Auto Debug: ***4805***\n" +
                "Webview: ***1212***\n" +
                "Upload Log: ***8080***\n" +
                "Client Info: ***8081***\n"+
                "Config Data: ***8082***\n" +
                "Chatbot Auto Test: ***8200*** \n"+
                "IoT Auto Test Start: ***8501***\n" +
                "IoT Auto Test Stop: ***8502***\n" +
                "Chatbot API Switch: ***8510***\n" +
                "Chatbot API Switch: ***8610***\n" +
                "Wifi Stress Test Start: ***8505***\n" +
                "Wifi Stress Test Stop: ***8506***\n" +
                "System Testing App: ***8901***";

        binding.debugInfo.setText("");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ly_back:
                ActivityLauncher.launchActivityByClass(this, SettingsActivity.class, R.anim.fade_in, R.anim.fade_out);
                break;
            case R.id.ly_dial:
                makeCall();
                break;
            case R.id.nkey_0:
                typeIn(0);
                break;
            case R.id.nkey_1:
                typeIn(1);
                break;
            case R.id.nkey_2:
                typeIn(2);
                break;
            case R.id.nkey_3:
                typeIn(3);
                break;
            case R.id.nkey_4:
                typeIn(4);
                break;
            case R.id.nkey_5:
                typeIn(5);
                break;
            case R.id.nkey_6:
                typeIn(6);
                break;
            case R.id.nkey_7:
                typeIn(7);
                break;
            case R.id.nkey_8:
                typeIn(8);
                break;
            case R.id.nkey_9:
                typeIn(9);
                break;
            case R.id.nkey_asterisk:
                typeIn(-1);
                break;
            case R.id.nkey_pound:
                typeIn(-2);
                break;
            case R.id.img_delete:
                deleteNum();
                break;
        }
    }

    public void typeIn(int inputNumber) {
        String br = binding.etDial.getText().toString();
        if (inputNumber >= 0) {
            dial_Number = br + inputNumber;
        } else if (inputNumber == -1) {
            dial_Number = br + "*";
        } else {
            dial_Number = br + "#";
        }

        binding.etDial.setText(dial_Number);
    }

    public void deleteNum() {

        makeCallNo = binding.etDial.getText().toString();
        if (makeCallNo.isEmpty())
            return;

        makeCallNo = makeCallNo.substring(0, makeCallNo.length() - 1);
        binding.etDial.setText(makeCallNo);

    }

    public void makeCall() {
        makeCallNo = binding.etDial.getText().toString();

        switch (makeCallNo) {

            case Phonebook.MN_SETTING:
                Intent intent3 = new Intent(Settings.ACTION_SETTINGS);
                startActivity(intent3);
                break;

            case Phonebook.MN_APK_REBOOT:
                AielloAlertDialog sys_dialog = new AielloAlertDialog(this, R.style.Theme_AppCompat_Dialog_Alert);
                sys_dialog.setTitle(getString(R.string.apk_reboot_title))
                        .setTextContent(getString(R.string.apk_reboot_content))
                        .setOnCancelListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                sys_dialog.dismiss();
                            }
                        })
                        .setOnConfirmListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                EventBus.getDefault().post(new ServerEvent(ServerEvent.Type.APPREBOOT));
                                SystemControl.rebootAPK(MagicNumberActivity.this, SystemControl.StageAction.CLEAR);
                                sys_dialog.dismiss();
                            }
                        }).show();
                break;

            case Phonebook.MN_APK_ASR:
                int asrModel = ASRManager.getInstance().switchASRModel();
                MToaster.showButtonToast(MagicNumberActivity.this, String.valueOf(asrModel), Style.TYPE_PROGRESS_BAR);
                break;
            case Phonebook.MM_DEBUG_AUTO:
                if (TestHub.debug_mode == false) {
                    TestHub.debug_mode = true;
                    if (!TestHub.ConnectionState())
                        TestHub.Connect();
                    MToaster.showButtonToast(MagicNumberActivity.this, "Switch on Auto Debug mode", Style.TYPE_STANDARD);
                } else {
                    TestHub.debug_mode = false;
                    TestHub.Disable();
                }
                break;
            case Phonebook.MN_WEBVIEW:
                Intent intent = new Intent();
                intent.setClassName("org.chromium.webview_shell", "org.chromium.webview_shell.WebViewBrowserActivity");
                startActivity(intent);
                break;


            case Phonebook.MN_UPLOAD:

                showProgressDialog();
                SystemControl.uploadLogFromClient(new AielloFileUtils.UploadFinishListener() {
                    @Override
                    public void onSuccess() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideProgressDialog();
                                MToaster.showButtonToast(MagicNumberActivity.this, "上傳成功", Style.TYPE_STANDARD);
                            }

                        });
                    }

                    @Override
                    public void onFail() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideProgressDialog();
                                MToaster.showButtonToast(MagicNumberActivity.this, "上傳失敗", Style.TYPE_STANDARD);
                            }
                        });

                    }
                });
                break;

            case Phonebook.MN_APK_OTA_PRODUCTION:
                SystemControl.manualOTA(this,
                        APIConfig.WEBAPI_SPOT_NOTE.getUrl(),
                        APIConfig.WEBAPI_SPOT_NOTE.getKey(),
                        DeviceInfo.hotelID, "release", SystemConfig.OTA.apk_cloud, DeviceInfo.majorVersion);
                break;

            case Phonebook.MN_APK_OTA_VIP:
                SystemControl.manualOTA(this,
                        APIConfig.WEBAPI_SPOT_NOTE.getUrl(),
                        APIConfig.WEBAPI_SPOT_NOTE.getKey(),
                        DeviceInfo.hotelID, "test", SystemConfig.OTA.apk_cloud, DeviceInfo.majorVersion);
                break;


            case Phonebook.MN_APK_ADS:
                getAdsAsync();
                break;

            case Phonebook.MN_IMAGE_OTA:
                SystemControl.imageCheckAndUpdate(this);
                break;

            case Phonebook.MN_DEVICE_REBOOT:
                EventBus.getDefault().post(new ServerEvent(ServerEvent.Type.DEVICEREBOOT));
                SystemControl.rebootDevice(this, SystemControl.StageAction.CLEAR);
                break;

            case Phonebook.MN_CHATBOT_AUTOTEST:
                new ChatbotValidator(this).start();
                break;

            case Phonebook.MN_IOT_TEST_START:
                IoTModuleManager.testToT(true);
                break;

            case Phonebook.MN_API_DEV_ENV:
                switchAPIEnv();
                break;

            case Phonebook.MN_IOT_TEST_STOP:
                IoTModuleManager.testToT(false);
                break;

            case Phonebook.MN_IOT_INTEGRATION_MODE:
                if (ChatBot.IntegrationMode) {
                    MToaster.showButtonToast(MagicNumberActivity.this, "關閉IoT測試DB", Style.TYPE_STANDARD);
                    ChatBot.IntegrationMode = false;
                } else {
                    MToaster.showButtonToast(MagicNumberActivity.this, "開啟IoT測試DB", Style.TYPE_STANDARD);
                    ChatBot.IntegrationMode = true;
                }
                break;

            case Phonebook.MN_WIFI_TEST_START:
                AielloWifiManager.testWifi(true);
                break;

            case Phonebook.MN_WIFI_TEST_STOP:
                AielloWifiManager.testWifi(false);
                break;

            case Phonebook.MN_CLIENT_INFO:

                String form = String.format("mac : %s \n" +
                                "============================== \n" +
                                "hotelName : %s \n" +
                                "roomName : %s \n" +
                                "asrOpt : %s \n" +
                                "============================== \n" +
                                "img_ver : %s \n" +
                                "apk_ver : %s \n" +
                                "push_ver : %s \n" +
                                "music_ver : %s \n" +
                                "sai_ver : %s",
                        DeviceInfo.MAC, DeviceInfo.hotelName, DeviceInfo.roomName,
                        ASRManager.getInstance().getASRModel(),
                        VersionCheckerT.img_ver, VersionCheckerT.apk_ver, VersionCheckerT.push_ver, VersionCheckerT.music_ver, VersionCheckerT.sai_ver);

                Dialog basicInfoDialog = new AlertDialog.Builder(MagicNumberActivity.this)
                        .setTitle("基礎資訊")
                        .setMessage(DeviceInfo.getDebugInfo())
                        .setNeutralButton("關閉", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //null
                            }
                        }).create();
                basicInfoDialog.show();
                basicInfoDialog.getWindow().setGravity(Gravity.CENTER);
                basicInfoDialog.getWindow().setLayout(1000, 900); //Controlling width and height.
                break;

            case Phonebook.MN_SPOT_CONFIG:
                Dialog spotConfigDialog = new AlertDialog.Builder(MagicNumberActivity.this)
                        .setTitle("參數設定檔")
                        .setMessage(Configer.jsonObject.toString())
                        .setNeutralButton("關閉", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //null
                            }
                        }).create();
                spotConfigDialog.show();
                spotConfigDialog.getWindow().setGravity(Gravity.CENTER);
                spotConfigDialog.getWindow().setLayout(1000, 900); //Controlling width and height.
                break;

            case Phonebook.MN_SYSDIAG_TEST:
                Log.i(TAG, "Open Sysdiag APK");
                Intent launchSysdiagApk = getPackageManager().getLaunchIntentForPackage("com.example.sysdiag");
                if (launchSysdiagApk != null) {
                    startActivity(launchSysdiagApk);
                }
                break;

            default:
                MToaster.showButtonToast(this, getString(R.string.loading_error), Style.TYPE_STANDARD);
                break;

        }
    }

    //todo event bus
    private void getAdsAsync() {
        MCountdownTimer.stopTimer();
        showProgressDialog();
        AdsModuleManager.getInstance().fetchAds(0, 0, new AdsModuleManager.OnCompleteListener() {
            @Override
            public void onComplete() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideProgressDialog();
                        Log.d(TAG, "sm.getPlayList().size() = " + AdsModuleManager.getInstance().getPlayList().size());
                        if (AdsModuleManager.getInstance().isReady()) {
                            EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.TIME_DISMISS));
                        } else {
                            MCountdownTimer.startStandbyTimer();
                            MToaster.showButtonToast(MagicNumberActivity.this, "獲取廣告失敗", Style.TYPE_PROGRESS_CIRCLE);
                        }
                    }
                });
            }
        });
    }

    public void switchAPIEnv() {
        Configer.useProductAPI = !Configer.useProductAPI;
        Configer configer = new Configer();
        configer.refreshAPI(Configer.useProductAPI);
        if (Configer.useProductAPI) {
            MToaster.showButtonToast(this, "useProductAPI", Style.TYPE_PROGRESS_BAR);
        } else {
            MToaster.showButtonToast(this, "useTestAPI", Style.TYPE_PROGRESS_BAR);
        }
    }

    public void showProgressDialog() {
        if (aielloProgressDialog == null) {
            aielloProgressDialog = new AielloProgressDialog(new WeakReference<Activity>(this), R.style.CustomProgressDialog);
            aielloProgressDialog.setText(getResources().getString(R.string.please_wait));
        }
        aielloProgressDialog.show();
    }


    public void hideProgressDialog() {
        if (aielloProgressDialog != null && aielloProgressDialog.isShowing()) {
            aielloProgressDialog.dismiss();
            aielloProgressDialog = null;
        }
    }

}
