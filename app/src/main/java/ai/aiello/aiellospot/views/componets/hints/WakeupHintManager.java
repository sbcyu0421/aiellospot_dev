package ai.aiello.aiellospot.views.componets.hints;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.modules.iot.IoTModuleManager;

import android.content.Context;

import com.tomer.fadingtextview.FadingTextView;

import static com.tomer.fadingtextview.FadingTextView.MILLISECONDS;

public class WakeupHintManager {

    private WakeupHintManager() {
    }

    private static WakeupHintManager instance;
    private FadingTextView tv_slide_text;
    private int slideTime = 4000;

    public static WakeupHintManager getInstance() {
        if (instance == null) {
            instance = new WakeupHintManager();
        }
        return instance;
    }


    public void showHints(FadingTextView tv_slide_text, String[] hints) {
        this.tv_slide_text = tv_slide_text;
        tv_slide_text.setTimeout(slideTime, MILLISECONDS);
        tv_slide_text.setTexts(hints);
    }

    public void cancel() {
        if (tv_slide_text != null) {
            tv_slide_text = null;
        }
    }

    private static int hintsIndex = 0;

    public String getHints(Context context) {
        String[] hints = null;
        if (IoTModuleManager.devicesAmount != 0) {
            hints = new String[]{
                    context.getString(R.string.hint_clock),
                    context.getString(R.string.hint_clock2),
                    context.getString(R.string.hint_iot_control),
                    context.getString(R.string.hint_iot_control2)};
        } else {
            hints = new String[]{
                    context.getString(R.string.hint_clock),
                    context.getString(R.string.hint_clock2),
                    context.getString(R.string.hint_music1),
                    context.getString(R.string.hint_player2)};
        }
        hintsIndex++;
        hintsIndex = (hintsIndex + hints.length) % hints.length;
        return hints[hintsIndex];
    }

}
