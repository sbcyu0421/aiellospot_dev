package ai.aiello.aiellospot.views.activity.roomservice;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.CardActivity;

public class ItemRequireActivity extends CardActivity {

    private static final String TAG = ItemRequireActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activtiy_itemrequire);
        TextView tv_itemrequire = findViewById(R.id.tv_itemrequire);
        LinearLayout btn_ok = findViewById(R.id.btn_ok);

        try {
            String response = intentObject.getMsg1();
            int ml = response.length();
            tv_itemrequire.setText(response);

            if (ml <= 17) {
                tv_itemrequire.setHeight(180);
            } else if (ml <= 34) {
                tv_itemrequire.setHeight(220);
            } else {
                tv_itemrequire.setHeight(260);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmAndGoBack();
            }
        });

    }


}
