package ai.aiello.aiellospot.views.activity.showcase.beans

import ai.aiello.aiellospot.ChatApplication
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import org.json.JSONArray
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

class RadioAlbumData(
    val uuid: String,
    val rawTitleData: String,
    val rawContentData: String,
    trackRaData: String,
    imageURL: String
) {

    var image: Bitmap? = null
    var title = ""
    var content = ""
    val tracks = mutableListOf<RadioTrackData>()

    fun getTitle(local: ChatApplication.Language): String {
        return LocaleParser.parser(local, rawTitleData)
    }

    fun getContent(local: ChatApplication.Language): String {
        return LocaleParser.parser(local, rawContentData)
    }

    init {
        val tracksRawData = JSONArray(trackRaData)

        for (j in 0 until tracksRawData.length()) {
            try {
                val trackObj = tracksRawData.getJSONObject(j)
                val track = RadioTrackData(
                    trackObj.getString("uuid"),
                    trackObj.getJSONObject("title").toString(),
                    trackObj.getJSONObject("singer").toString(),
                    trackObj.getString("song_url"),
                    trackObj.getString("duration")
                )
                tracks.add(track)
            } catch (e: Exception) {
            }
        }
        image = fetchBitmap(imageURL)
    }

    private fun fetchBitmap(url: String): Bitmap? {
        val urlConnection = URL(url)
        return try {
            val connection: HttpURLConnection = urlConnection
                .openConnection() as HttpURLConnection
            connection.setDoInput(true)
            connection.connect()
            val input: InputStream = connection.getInputStream()
            BitmapFactory.decodeStream(input)
        } catch (e: Exception) {
            null
        }
    }



}