package ai.aiello.aiellospot.views.activity.showcase.beans

import ai.aiello.aiellospot.ChatApplication
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.BitmapFactory.decodeFile
import android.os.Environment
import androidx.core.graphics.scale
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

class HotelFeatureDataBean(
    val uuid: String,
    var title: String,
    var content: String,
    imageUrl: String,
    val qr_code_url: String?
) {
    val imageSDCardDirectory = Environment.getExternalStorageDirectory().absolutePath + "/HotelFeatureDetailImages/"
    val imageSDCardPath = Environment.getExternalStorageDirectory().absolutePath + "/HotelFeatureDetailImages/" + uuid

    fun getImageFromSDCard(): Bitmap? {
        return decodeFile(imageSDCardPath)
    }

    fun getQRCodeURL(local: ChatApplication.Language): String? {
        return qr_code_url?.let { LocaleParser.parser(local, it) }
    }


    fun getTitle(local: ChatApplication.Language): String {
        return LocaleParser.parser(local, title)
    }

    fun getContent(local: ChatApplication.Language): String {
        return LocaleParser.parser(local, content)
    }

    init {
        if (!File(imageSDCardDirectory).exists())  File(imageSDCardDirectory).mkdir()
        downloadBitmap(imageUrl)?.scale(480,480,true)?.let { setImageToSDCard(it, imageSDCardPath) }
    }

    private fun downloadBitmap(url: String): Bitmap? {
        val urlConnection = URL(url)
        return try {
            val connection: HttpURLConnection = urlConnection
                .openConnection() as HttpURLConnection
            connection.setDoInput(true)
            connection.connect()
            val input: InputStream = connection.getInputStream()
            BitmapFactory.decodeStream(input)
        } catch (e: Exception) {
            null
        }
    }

    private fun setImageToSDCard(image: Bitmap, imagePath: String) {
        val fileOutPutStream = FileOutputStream(imagePath)
        image.compress(Bitmap.CompressFormat.JPEG, 80, fileOutPutStream)
        fileOutPutStream.close()
    }
}