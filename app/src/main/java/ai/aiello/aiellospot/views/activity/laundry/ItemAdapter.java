package ai.aiello.aiellospot.views.activity.laundry;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import ai.aiello.aiellospot.R;

/**
 * Created by a1990 on 2019/5/6.
 */

public class ItemAdapter extends BaseAdapter {

    private Activity activity;
    private List<LaundryItem> usersList;

    public ItemAdapter(Activity activity, List<LaundryItem> usersList) {
        super();
        this.activity = activity;
        this.usersList = usersList;
    }

    @Override
    public int getCount() {
        return usersList.size();
    }
    @Override
    public Object getItem(int position) {
        return usersList.get(position);
    }
    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            convertView = inflater.inflate(R.layout.activtiy_laundry_listitem, null);
        }
        TextView col1 = (TextView) convertView.findViewById(R.id.column1);
        TextView col2 = (TextView) convertView.findViewById(R.id.column2);
        col1.setText(usersList.get(position).getName());
        col2.setText(usersList.get(position).getPrice());

        return convertView;
    }


}
