package ai.aiello.aiellospot.views.activity.voip;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.library.Style;
import com.litesuits.android.log.Log;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.activity.BaseActivity;
import ai.aiello.aiellospot.views.componets.toast.MToaster;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.soundai.SoundAIManager;
import ai.aiello.aiellospot.events.module.AlarmStateChange;
import ai.aiello.aiellospot.events.module.VoIPStateChange;
import ai.aiello.aiellospot.modules.alarm.UserAlarmModuleManager;
import ai.aiello.aiellospot.modules.voip.Phonebook;
import ai.aiello.aiellospot.modules.voip.VoipModuleManager;

public class VoipOnCallActivity extends BaseActivity {

    static String TAG = VoipOnCallActivity.class.getSimpleName();

    private RelativeLayout ly_a_btn, ly_b_btn, ly_keyboard_switch;
    private String comingMsg, makeCallMsg;
    private LinearLayout ly_tab, ly_status, ly_keyboard;
    private boolean isKeyboardShowing = false;

    private ImageView status_img;
    private TextView status_msg;

    private ImageView img_a_btn, img_b_btn;
    private TextView tv_a_btn, tv_b_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voip_oncall);
        initView();
        registerKeyBoardEvent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateCallStatus();
    }


    private void initView() {
        ly_a_btn = findViewById(R.id.ly_a_btn);
        ly_b_btn = findViewById(R.id.ly_b_btn);

        ly_keyboard_switch = findViewById(R.id.ly_keyboard_switch);
        ly_tab = findViewById(R.id.ly_tab);
        ly_status = findViewById(R.id.ly_status);
        ly_keyboard = findViewById(R.id.ly_keyboard);
        status_img = findViewById(R.id.status_img);
        status_msg = findViewById(R.id.status_msg);

        img_a_btn = findViewById(R.id.img_a_btn);
        img_b_btn = findViewById(R.id.img_b_btn);
        tv_a_btn = findViewById(R.id.tv_a_btn);
        tv_b_btn = findViewById(R.id.tv_b_btn);
    }


    public void updateCallStatus() {
        Log.d(TAG, "updateCallStatus = " + VoipModuleManager.getInstance().currentState.getName());
        switch (VoipModuleManager.getInstance().currentState.getName()) {
            case OnCall:
                onCallUI();
                break;

            case Idle:
                ActivityLauncher.launchActivityByClass(VoipOnCallActivity.this, VoipHomeActivity.class, R.anim.fade_in, R.anim.fade_out);
                break;

            case OnMakeCallWaiting:
                makeCallUI();
                break;

            case OnInComingCallWaiting:
                comingCallUI();
                break;
        }
    }

    public void comingCallUI() {
        setKeyBoardEnable(false);
        hideKeyboard();

        String comingCallNo = VoipModuleManager.targetNum;
        if (comingCallNo.equals(Phonebook.FRONT_DESK_NUMBER)) {
            status_img.setImageDrawable(getDrawable(R.drawable.icon_voip_frontdesk));
            status_msg.setText(R.string.frontdesk_calling);
        } else if (comingCallNo.equals(Phonebook.RESTAURANT_NUMBER)) {
            status_img.setImageDrawable(getDrawable(R.drawable.icon_voip_restaurant));
            status_msg.setText(R.string.restaurant_calling);
        } else {
            status_img.setImageDrawable(getDrawable(R.drawable.icon_voip_room));
            status_msg.setText(comingCallNo + getResources().getString(R.string.on_calling));
        }

        img_a_btn.setImageResource(R.drawable.icon_voip_accept);
        img_b_btn.setImageResource(R.drawable.icon_voip_end);
        tv_a_btn.setText(R.string.call_accept);
        tv_b_btn.setText(R.string.call_decline);
        ly_a_btn.setOnClickListener(accept);
        ly_b_btn.setOnClickListener(decline);

        //resume btn if do use
        ly_b_btn.setVisibility(View.VISIBLE);
        ly_b_btn.setEnabled(true);
    }


    public void exitUI(String msg) {
        ly_a_btn.setOnClickListener(null);
        ly_b_btn.setOnClickListener(null);
        status_msg.setText(msg);
        VoipModuleManager.getInstance().stopMakeCallTimeout();
        ActivityLauncher.launchActivityByClass(VoipOnCallActivity.this, VoipHomeActivity.class, R.anim.fade_in, R.anim.fade_out);
    }

    public void updateMuteStatus() {
        if (!VoipModuleManager.getInstance().isCallMuted()) {
            img_b_btn.setImageResource(R.drawable.icon_unmute);
            tv_b_btn.setText(R.string.call_mute);
        } else {
            img_b_btn.setImageResource(R.drawable.icon_mute);
            tv_b_btn.setText(R.string.call_unmute);
        }
    }

    public void onCallUI() {
        setKeyBoardEnable(true);
        hideKeyboard();
        Log.e(TAG, "onCallUI");
        SpotDeviceLog.info(TAG, "", "onCallUI");
        String onCallNo = VoipModuleManager.targetNum;


        if (onCallNo.equals(Phonebook.FRONT_DESK_NUMBER)) {
            status_img.setImageDrawable(getDrawable(R.drawable.icon_voip_frontdesk));
        } else if (onCallNo.equals(Phonebook.RESTAURANT_NUMBER)) {
            status_img.setImageDrawable(getDrawable(R.drawable.icon_voip_restaurant));
        } else {
            status_img.setImageDrawable(getDrawable(R.drawable.icon_voip_room));
        }

        img_a_btn.setImageResource(R.drawable.icon_voip_end);
        img_b_btn.setImageResource(R.drawable.icon_unmute);
        tv_a_btn.setText(R.string.call_end);
        tv_b_btn.setText(R.string.call_mute);
        ly_a_btn.setOnClickListener(endCall);
        ly_b_btn.setOnClickListener(muteCall);

        //resume btn if do use
        ly_b_btn.setVisibility(View.VISIBLE);
        ly_b_btn.setEnabled(true);

        status_msg.setText("00 : 00");

        updateMuteStatus();

    }


    public void makeCallUI() {
        setKeyBoardEnable(false);
        hideKeyboard();
        String makeCallNo = VoipModuleManager.targetNum;

        if (makeCallNo.equals(Phonebook.FRONT_DESK_NUMBER)) {
            status_img.setImageDrawable(getDrawable(R.drawable.icon_voip_frontdesk));
            status_msg.setText(R.string.call_to_frontdesk);
        } else if (makeCallNo.equals(Phonebook.RESTAURANT_NUMBER)) {
            status_img.setImageDrawable(getDrawable(R.drawable.icon_voip_restaurant));
            status_msg.setText(R.string.call_to_restaurant);
        } else {
            status_img.setImageDrawable(getDrawable(R.drawable.icon_voip_room));
            status_msg.setText(getResources().getString(R.string.call_to_room_prefix) + makeCallNo + getResources().getString(R.string.call_to_room_suffix));
        }

        img_a_btn.setImageResource(R.drawable.icon_voip_end);
        tv_a_btn.setText(R.string.call_end);
        ly_a_btn.setOnClickListener(cancelMakeCall);

        //hide btn if not use
        ly_b_btn.setVisibility(View.INVISIBLE);
        ly_b_btn.setEnabled(false);

    }


    public View.OnClickListener accept = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            VoipModuleManager.getInstance().currentState.handleEvent(VoipModuleManager.Event.accept, "");
        }
    };

    public View.OnClickListener decline = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            VoipModuleManager.getInstance().currentState.handleEvent(VoipModuleManager.Event.decline, VoipModuleManager.TX);
        }
    };


    public View.OnClickListener endCall = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            VoipModuleManager.getInstance().currentState.handleEvent(VoipModuleManager.Event.endCall, VoipModuleManager.TX);
        }
    };

    public View.OnClickListener muteCall = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            VoipModuleManager.getInstance().setCallMute();
            updateMuteStatus();
        }
    };

    public View.OnClickListener cancelMakeCall = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            VoipModuleManager.getInstance().currentState.handleEvent(VoipModuleManager.Event.decline, VoipModuleManager.TX);
        }
    };


    public void registerKeyBoardEvent() {
        Log.d(TAG, "registerKeyBoardEvent");
        ly_keyboard_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateKeyBoardShowStatus();
                Log.d(TAG, "updateKeyBoardShowStatus");
            }
        });
    }

    public void setKeyBoardEnable(boolean enable) {
        if (enable) {
            ly_keyboard_switch.setEnabled(true);
            ly_keyboard_switch.setVisibility(View.VISIBLE);
        } else {
            ly_keyboard_switch.setEnabled(false);
            ly_keyboard_switch.setVisibility(View.INVISIBLE);
        }
    }


    public void updateKeyBoardShowStatus() {
        if (isKeyboardShowing) {
            hideKeyboard();
        } else {
            showKeyboard();
        }
    }

    public void showKeyboard() {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) ly_status.getLayoutParams();
        params.weight = 3;
        ly_status.setLayoutParams(params);

        LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams) ly_keyboard.getLayoutParams();
        params2.weight = 3;
        ly_keyboard.setLayoutParams(params2);
        isKeyboardShowing = true;
    }


    public void hideKeyboard() {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) ly_status.getLayoutParams();
        params.weight = 6;
        ly_status.setLayoutParams(params);

        LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams) ly_keyboard.getLayoutParams();
        params2.weight = 0;
        ly_keyboard.setLayoutParams(params2);
        isKeyboardShowing = false;
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.d(TAG, "keyCode = " + keyCode);
        switch (keyCode) {
            case KEYCODE_AIELLO_VOLUME_UP:
            case KEYCODE_AIELLO_VOLUME_DOWN:
                showControlDialog();
                break;

            case KEYCODE_AIELLO_BLUETOOTH:
                //do not handle
                break;

            case KEYCODE_AIELLO_MUTE:
                if (SoundAIManager.onVoip) {
                    VoipModuleManager.getInstance().setCallMute();
                    updateMuteStatus();
                } else {
                    if (SoundAIManager.isWakeUpEnable()) {
                        SoundAIManager.setWakeUp(this, false);
                        MToaster.showButtonToast(this, getString(R.string.wakeup_disable), Style.TYPE_STANDARD);
                    } else {
                        SoundAIManager.setWakeUp(this, true);
                        MToaster.showButtonToast(this, getString(R.string.wakeup_enable), Style.TYPE_STANDARD);
                    }
                }
                break;
        }

        return true;
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(AlarmStateChange state) {
        switch (state.getState()) {
            case ALARM_RISE:
            case ALARM_TIMEOUT:
                UserAlarmModuleManager.getInstance().showAlarmPopup(UserAlarmModuleManager.alarm_msg);
                break;
        }
    }


    @Override
    public void handleOnCallAction(String status, String message) {

        switch (status) {
            case VoIPStateChange.ACCEPT_CALL:
                hideASRDialog("ACCEPT_CALL", true);
                onCallUI();
                break;

            case VoIPStateChange.DECLINE_CALL:
                hideASRDialog("DECLINE_CALL", true);
                if (message.equals(DeviceInfo.roomName)) {
                    exitUI(getString(R.string.decline_call_status_yourself));
                } else {
                    exitUI(getString(R.string.decline_call_status));
                }
                break;

            case VoIPStateChange.CALL_END:
                exitUI(getString(R.string.call_end_status));
                break;

            case VoIPStateChange.INCOMING_CALL_CANCELED:
                hideASRDialog("INCOMING_CALL_CANCELED", true);
                exitUI(getString(R.string.cancel_call_status));
                break;

            case VoIPStateChange.CANCEL_MAKE_CALL:
                exitUI(getString(R.string.t_decline_call_status));
                break;

            case VoIPStateChange.NO_ANSWER:
                hideASRDialog("NO_ANSWER", true);
                exitUI(getString(R.string.makecall_no_response));
                break;

            case VoIPStateChange.UPDATE_ON_CALL_TIME:
                status_msg.setText(message);
                break;

        }

    }

    @Override
    public void handleCallAliveAction() {
        if (VoipModuleManager.getInstance().connStatus == VoipModuleManager.ConnStatus.online) {
            if (VoipModuleManager.getInstance().currentState.getName() == VoipModuleManager.StateName.Disable) {
                exitUI(getString(R.string.hint_voip_disable));
            }
        } else {
            exitUI(getString(R.string.wifi_off));
        }
    }

}


