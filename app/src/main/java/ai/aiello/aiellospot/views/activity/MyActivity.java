package ai.aiello.aiellospot.views.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.github.johnpersano.supertoasts.library.Style;
import com.litesuits.android.log.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Locale;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.modules.showcase.radio.HotelMusicManager;
import ai.aiello.aiellospot.views.activity.ads.PodcastActivity;
import ai.aiello.aiellospot.views.activity.ads.StandbyActivity;
import ai.aiello.aiellospot.views.activity.clock.AlarmRiseActivity;
import ai.aiello.aiellospot.views.activity.dialog.ASRDialog;
import ai.aiello.aiellospot.views.activity.dialog.AielloAlertDialog;
import ai.aiello.aiellospot.views.activity.dialog.ControlBarDialog;
import ai.aiello.aiellospot.views.activity.multi.MultiIntentActivity;
import ai.aiello.aiellospot.views.activity.settings.SettingBlueToothActivity;
import ai.aiello.aiellospot.views.activity.voip.VoipOnCallActivity;
import ai.aiello.aiellospot.core.MCountdownTimer;
import ai.aiello.aiellospot.core.asr.ASRManager;
import ai.aiello.aiellospot.core.device.DeviceControl;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.core.media.AielloSoundPool;
import ai.aiello.aiellospot.core.media.MsttsManager;
import ai.aiello.aiellospot.core.messager.MessageManager;
import ai.aiello.aiellospot.core.soundai.SoundAIManager;
import ai.aiello.aiellospot.core.system.AielloWifiManager;
import ai.aiello.aiellospot.events.system.WakeUpEvent;
import ai.aiello.aiellospot.events.system.SystemEvent;
import ai.aiello.aiellospot.events.module.AlarmStateChange;
import ai.aiello.aiellospot.events.module.VoIPStateChange;
import ai.aiello.aiellospot.intents.IntentManager;
import ai.aiello.aiellospot.intents.IntentObject;
import ai.aiello.aiellospot.intents.sdui.ActionManager;
import ai.aiello.aiellospot.modules.alarm.UserAlarmModuleManager;
import ai.aiello.aiellospot.modules.music.MusicServiceModuleManager;
import ai.aiello.aiellospot.views.componets.toast.MToaster;

/**
 * Created by a1990 on 2019/5/3.
 */

public abstract class MyActivity extends FragmentActivity {

    public String TAG = MyActivity.class.getSimpleName();
    public ChatApplication app;
    public Intent intent;
    public IntentObject intentObject;

    public static final int KEYCODE_AIELLO_BLUETOOTH = 500;
    public static final int KEYCODE_AIELLO_MUTE = 501;
    public static final int KEYCODE_AIELLO_VOLUME_UP = 502;
    public static final int KEYCODE_AIELLO_VOLUME_DOWN = 503;
    public static final int KEYCODE_POWER = 26;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (ChatApplication) getApplication();
        Log.e(TAG, "UserTrace onCreate = " + this.getClass().getName());
        getIntentFromBundle();
        SpotDeviceLog.info(TAG, "", "UserTrace onCreate = " + this.getClass().getName());
    }

    @Override
    protected void onResume() {
        Log.e(TAG, "UserTrace frontView = " + this.getClass().getName());
        super.onResume();
        ActivityLauncher.thisActivity = this.getClass().getName();
        MCountdownTimer.startStandbyTimer();
        hideActivityController();
        Log.d(TAG, this.getLocalClassName() + ", EventBus Register");
        EventBus.getDefault().register(this);
//        System.gc();
    }


    @Override
    protected void onPause() {
        Log.d(TAG, this.getLocalClassName() + ", EventBus UnRegister");
        EventBus.getDefault().unregister(this);
        ActivityLauncher.lastActivity = ActivityLauncher.thisActivity;
        MCountdownTimer.stopTimer();
        hideWifiDialog();
        super.onPause();
    }


    public void hideActivityController() {
        //hideNavController
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }


    //註冊喚醒
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(WakeUpEvent event) {
        Log.e(TAG, this.getLocalClassName() + ", get WakeUpEvent");
        DeviceControl.backLightOn();
        if (UserAlarmModuleManager.getInstance().isAlarmRing()) {
            UserAlarmModuleManager.getInstance().stopRing("stop alarm on wakeup");
            UserAlarmModuleManager.getInstance().hideAlarmPopup(2);
            SoundAIManager.stopBeam();
            return;
        }
        if (AielloWifiManager.iskWifiOnAndConnected() == 2) {
            ChatApplication.setCurrentAppState(ChatApplication.AppState.BUSY);
            if (!ASRManager.getInstance().getOnASR()) {
                Log.e(TAG, this.getLocalClassName() + "get wakeup event, show asr card");
                DeviceControl.volumeDown();
                AielloSoundPool.play(AielloSoundPool.wakeup);
                showASRCard();
            } else {
                Log.e(TAG, this.getLocalClassName() + "get wakeup event, asr card already show");
            }
        } else {
            Log.e(TAG, "get wakeup event, but no wifi");
            SoundAIManager.stopBeam();
            playNoWifiHint(2);
            //showDialog(getString(R.string.sys_wifi_disable));
        }

    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        //only handler in action down
        if (DeviceControl.getBrightness() == 0) {
            DeviceControl.backLightOn();
            return true;
        } else {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                MCountdownTimer.startTimer();
            }
            return super.dispatchTouchEvent(event);
        }
    }

    public void getIntentFromBundle() {
        intent = getIntent();
        intentObject = (IntentObject) intent.getSerializableExtra(IntentManager.BUNDLE_TAG);
        if (intent.getStringExtra("Multi_Preview") == null && !isTaskRoot()) {
            Log.e(TAG, "this is not task root restart");
            finish();
            return;
        }
    }


    @Override
    protected void onStop() {
        MessageManager.getInstance().resume();
        ASRManager.getInstance().setOnASR(false);
        hideASRDialog("onStop trigger release ASR, skip animation", true);
        super.onStop();
    }


    /**
     * basic dialog
     */

    private AielloAlertDialog wifiDialog;

    public void showWifiDialog(String msg) {
        hideASRDialog("WIFI_DIALOG_SHOW", false);
        hideControlDialog();
        if (wifiDialog == null || !wifiDialog.isShowing()) {
            Log.d(TAG, "showWifiDialog");
            wifiDialog = new AielloAlertDialog(this, R.style.Theme_AppCompat_Dialog_Alert);
            wifiDialog.setTitle(getString(R.string.sys_info_title))
                    .setTextContent(msg)
                    .setOnConfirmListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            wifiDialog.dismiss();
                            wifiDialog = null;
                            stopPlaying();
                        }
                    });
            wifiDialog.show();
        }

    }

    public void hideWifiDialog() {
        if (wifiDialog != null && wifiDialog.isShowing()) {
            Log.d(TAG, "hideWifiDialog");
            wifiDialog.dismiss();
            stopPlaying();
            wifiDialog = null;
        }
    }


    /**
     * show asr card
     */
    private static ASRDialog asrDialog;

    public void showASRCard() {
        if (asrDialog == null || !asrDialog.isShowing()) {
            Log.d(TAG, "showASRDialog");
            if (MyActivity.this instanceof MultiIntentActivity) {
                IntentManager.backupIntents();
            }
            hideControlDialog();
            asrDialog = new ASRDialog(this, R.style.CustomProgressDialog);
            asrDialog.show(new ASRDialog.ASRDialogListener() {
                @Override
                public void onFinish(boolean hiddenControl) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //resume asr state before leave this activity
                            MCountdownTimer.startTimer();
                            MessageManager.getInstance().resume();
                            ASRManager.getInstance().setOnASR(false);
                            DeviceControl.volumeResume();
                            Log.d(TAG, "resume asr state");
                            if (IntentManager.finishIntentList.size() == 1 && IntentManager.finishIntentList.get(0).getMsg3() != null && IntentManager.finishIntentList.get(0).getMsg3().contains("ShutUp")) {
                                cancelASRDialog("ShutUp");
                            } else {
                                showIntentUI(hiddenControl);
                            }
                        }
                    });

                }

                private void showIntentUI(boolean hiddenControl) {
                    try {
                        if (IntentManager.finishIntentList.size() == 0) {
                            if (hiddenControl) {
                                hideASRDialog("USER_VOICE_INTENT", true);
                                showControlDialog();
                                if (MyActivity.this instanceof MultiIntentActivity) {
                                    //because of control bar is a dialog, you must restore intents from list for rendering
                                    Log.d(TAG, "control bar show at multi");
                                    IntentManager.resumeBackupIntents();
                                }
                            } else {
                                throw new Exception("intent list is empty");
                            }
                        }
                        //single intent
                        else if (IntentManager.finishIntentList.size() == 1) {
                            IntentObject sio0 = IntentManager.finishIntentList.get(0);
                            Intent goTo = new Intent(getApplicationContext(), sio0.getaClass());
                            goTo.putExtra(IntentManager.BUNDLE_TAG, sio0);
                            startActivity(goTo);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            finish();
                        } else {

                            IntentObject io_multi = null;
                            IntentObject io_backLightOff = null;

                            for (IntentObject io : IntentManager.finishIntentList) {
                                if (io.getIntent().equals("Dialogflow.Activity.Control")) {
                                    io_multi = io;
                                }
                            }

                            if (io_multi != null) {
                                Intent goTo = new Intent(getApplicationContext(), io_multi.getaClass());
                                startActivity(goTo);
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                finish();
                            } else {
                                Intent goTo = new Intent(getApplicationContext(), MultiIntentActivity.class);
                                startActivity(goTo);
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                finish();
                            }

                        }
                    } catch (Exception e) {
                        Log.d(TAG, e.toString());
                        hideASRDialog(e.toString(), false);
                    }
                }

                @Override
                public void onCancel(String msg) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            cancelASRDialog(msg);
                        }
                    });

                }

                private void cancelASRDialog(String msg) {
                    //resume asr state before leave this activity
                    MCountdownTimer.startTimer();
                    MessageManager.getInstance().resume();
                    ASRManager.getInstance().setOnASR(false);
                    DeviceControl.volumeResume();
                    Log.d(TAG, "resume asr state");
                    hideASRDialog(msg, false);
                    MyActivity origin = MyActivity.this;
                    if (origin instanceof StandbyActivity || origin instanceof PodcastActivity || origin instanceof MultiIntentActivity) {
                        Log.d(TAG, "special cancel switch");
                        ActivityLauncher.launchActivityByClass(MyActivity.this, ActivityLauncher.currentHomeName, R.anim.fade_in, R.anim.fade_out);
                    }
                }
            }, app);
        }
    }


    public void hideASRDialog(String msg, boolean skipAnimation) {
        Log.d(TAG, "hideASRDialog = " + msg + ", skip = " + skipAnimation);
        if (asrDialog != null && asrDialog.isShowing()) {
            asrDialog.dismiss(skipAnimation);
            asrDialog = null;
        }
    }


    /**
     * control bar dialog
     */
    private ControlBarDialog controlBarDialog;

    public void showControlDialog() {
        if (wifiDialog != null && wifiDialog.isShowing()) {
            return;
        }
        if (asrDialog != null && asrDialog.isShowing()) {
            return;
        }
        if (controlBarDialog == null || !controlBarDialog.isShowing()) {
            Log.d(TAG, "showControlBarDialog");
            controlBarDialog = new ControlBarDialog(this, R.style.CustomProgressDialog);
            controlBarDialog.show();
        }

    }

    public void hideControlDialog() {
        if (controlBarDialog != null && controlBarDialog.isShowing()) {
            Log.d(TAG, "hideControlDialog");
            controlBarDialog.dismiss();
            controlBarDialog = null;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(VoIPStateChange event) {
        String status = event.getStatus();
        String message = event.getMessage();

        switch (status) {
            case VoIPStateChange.ACCEPT_CALL:
            case VoIPStateChange.DECLINE_CALL:
            case VoIPStateChange.CALL_END:
            case VoIPStateChange.INCOMING_CALL_CANCELED:
            case VoIPStateChange.NO_ANSWER:
            case VoIPStateChange.UPDATE_ON_CALL_TIME:
            case VoIPStateChange.CANCEL_MAKE_CALL:
                handleOnCallAction(status, message);
                break;
            case VoIPStateChange.MAKE_CALL_RESPONSE:
                handleMakeCallResAction(message);
                break;
            case VoIPStateChange.INCOMING_CALL:
                handleInComingCallAction();
                break;
            case VoIPStateChange.VOIP_STATUS:
            case VoIPStateChange.DISABLE:
                handleCallAliveAction();
                break;
        }
    }

    private void handleInComingCallAction() {
        DeviceControl.backLightOn();
        ActionManager.getInstance().stopAllAction();
        MsttsManager.getInstance().stopTTS();
        hideASRDialog("TX_INCOMING_CALL", true);
        Intent intent = new Intent(getApplicationContext(), VoipOnCallActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }

    public void handleOnCallAction(String status, String message) {
        //for child implement
    }

    public void handleMakeCallResAction(String message) {
        //for child implement
    }

    public void handleCallAliveAction() {
        //for child implement
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(AlarmStateChange state) {
        switch (state.getState()) {
            case ALARM_RISE:
                Intent intent_alarm = new Intent(MyActivity.this, AlarmRiseActivity.class);
                startActivity(intent_alarm);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
                break;
            case ALARM_TIMEOUT:
                UserAlarmModuleManager.getInstance().showAlarmPopup(UserAlarmModuleManager.alarm_msg);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        SpotDeviceLog.info(TAG, "", "UserTrace onDestroy = " + this.getClass().getName());
        Log.e(TAG, "UserTrace onDestroy = " + this.getClass().getName());
        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.d(TAG, "keyCode = " + keyCode);
        if (DeviceControl.getBrightness() == 0) {
            DeviceControl.backLightOn();
            return true;
        }
        switch (keyCode) {
            case KEYCODE_AIELLO_VOLUME_UP:
                SpotUserTraceLog3.getInstance().buildHardwareEventLog(
                        SpotUserTraceLog3.EventSubject.VOLUME,
                        SpotUserTraceLog3.EventAction.UP,
                        ""
                );
                showControlDialog();
                break;
            case KEYCODE_AIELLO_VOLUME_DOWN:
                SpotUserTraceLog3.getInstance().buildHardwareEventLog(
                        SpotUserTraceLog3.EventSubject.VOLUME,
                        SpotUserTraceLog3.EventAction.DOWN,
                        ""
                );
                showControlDialog();
                break;

            case KEYCODE_AIELLO_BLUETOOTH:
                if (DeviceControl.blueToothState.equals(DeviceControl.BTState.OFF)) {
                    SpotUserTraceLog3.getInstance().buildHardwareEventLog(
                            SpotUserTraceLog3.EventSubject.BLUETOOTH,
                            SpotUserTraceLog3.EventAction.ON,
                            ""
                    );
                    DeviceControl.enableBT();
                    Intent intent5 = new Intent(this, SettingBlueToothActivity.class);
                    startActivityForResult(intent5, 0);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();
                } else {
                    SpotUserTraceLog3.getInstance().buildHardwareEventLog(
                            SpotUserTraceLog3.EventSubject.BLUETOOTH,
                            SpotUserTraceLog3.EventAction.OFF,
                            ""
                    );
                    DeviceControl.disableBT();
                }
                break;

            case KEYCODE_AIELLO_MUTE:
                if (SoundAIManager.isWakeUpEnable()) {
                    SpotUserTraceLog3.getInstance().buildHardwareEventLog(
                            SpotUserTraceLog3.EventSubject.WAKEUP,
                            SpotUserTraceLog3.EventAction.OFF,
                            ""
                    );
                    SoundAIManager.setWakeUp(this, false);
                } else {
                    SpotUserTraceLog3.getInstance().buildHardwareEventLog(
                            SpotUserTraceLog3.EventSubject.WAKEUP,
                            SpotUserTraceLog3.EventAction.ON,
                            ""
                    );
                    SoundAIManager.setWakeUp(this, true);
                }
                break;

        }

        return true;
    }

    public void onHotReload() {
        //for child implement
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(SystemEvent event) {
        switch (event.getType()) {

            case BUSY_STATE_HOT_RELOAD:
            case IDLE_STATE_HOT_RELOAD:
                onHotReload();
                break;

            case WIFI_DISCONNECTED:
                hideASRDialog("WIFI_DISCONNECT", false);
                //close voip process
//                if (app.soundAIManager.onVoip) {
//                    ActivityLauncher.launchActivityByClass(this, VoipHomeActivity.class, R.anim.fade_in, R.anim.fade_out);
//                    MToaster.showButtonToast(this, getString(R.string.voip_disconnect), Style.TYPE_STANDARD);
//                }
                showWifiDialog(getString(R.string.sys_wifi_disable));
                break;

            case WIFI_CONNECTED:
                hideWifiDialog();
                break;

            case TIME_DISMISS:
                if (ASRManager.onEmit || SoundAIManager.onVoip) return;
                if (MusicServiceModuleManager.playerStatus == MusicServiceModuleManager.PlayerStatus.onPlay) {return;}
                if (HotelMusicManager.INSTANCE.getPlayStatus() == HotelMusicManager.MusicState.PLAY) {return;}
                ChatApplication.setCurrentAppState(ChatApplication.AppState.IDLE);
                Intent intent1 = new Intent(MyActivity.this, StandbyActivity.class);
                startActivity(intent1);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
                break;

            case WAKEUP_ENABLE:
                MToaster.showButtonToast(MyActivity.this, getString(R.string.wakeup_enable), Style.TYPE_STANDARD);
                break;
            case WAKEUP_DISABLE:
                MToaster.showButtonToast(MyActivity.this, getString(R.string.wakeup_disable), Style.TYPE_STANDARD);
                break;

        }

    }


    private static MediaPlayer wifi_mediaPlayer = null;

    public void playNoWifiHint(int type) {

        MsttsManager.getInstance().stopTTS();
        try {
            wifi_mediaPlayer = new MediaPlayer();
            AssetFileDescriptor file = null;

            if (type == 1) {
                if (ChatApplication.locale.equals(Locale.CHINA) || ChatApplication.locale.equals(Locale.TAIWAN)) {
                    file = getResources().openRawResourceFd(R.raw.slow_wifi_hint_tw);
                } else {
                    file = getResources().openRawResourceFd(R.raw.slow_wifi_hint_en);
                }
            } else {
                if (ChatApplication.locale.equals(Locale.CHINA) || ChatApplication.locale.equals(Locale.TAIWAN)) {
                    file = getResources().openRawResourceFd(R.raw.no_wifi_hint_tw);
                } else {
                    file = getResources().openRawResourceFd(R.raw.no_wifi_hint_en);
                }
            }

            wifi_mediaPlayer.setDataSource(file.getFileDescriptor(), file.getStartOffset(),
                    file.getLength());
            wifi_mediaPlayer.setVolume(1.0f, 1.0f);
            wifi_mediaPlayer.setLooping(false);
            wifi_mediaPlayer.prepare();
            wifi_mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    stopPlaying();
                }
            });
            wifi_mediaPlayer.start();
            file.close();
            file = null;
        } catch (Exception e) {
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e(TAG, e.toString());
        }
    }

    public void stopPlaying() {
        if (wifi_mediaPlayer != null) {
            wifi_mediaPlayer.stop();
            wifi_mediaPlayer.release();
            wifi_mediaPlayer = null;
        }
    }


    public static boolean isDestroy(Activity activity) {
        return activity == null || activity.isFinishing() && activity.isDestroyed();
    }

}
