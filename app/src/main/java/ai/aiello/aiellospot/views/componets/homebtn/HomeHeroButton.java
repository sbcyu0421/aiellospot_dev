package ai.aiello.aiellospot.views.componets.homebtn;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;

import static android.widget.LinearLayout.HORIZONTAL;
import static android.widget.LinearLayout.VERTICAL;


public class HomeHeroButton extends ConstraintLayout {

    static String TAG = HomeHeroButton.class.getSimpleName();

    public int width;
    public int height;
    public View btn;
    private Style style;
    private ImageView heroBackgroundImageView;
    private ConstraintLayout contentHolder;
    private TextView textView;
    private int defaultPlaceholderImage = R.drawable.img_happyhour;

    enum Style {
        VERTICAL, HORIZONTAL
    }

    public HomeHeroButton(Context context, int width, int height) {
        super(context);

        this.width = width;
        this.height = height;
        this.style = setBtnStyle(width, height);

        View btn = LayoutInflater.from(context).inflate(R.layout.button_home_hero, null);

        this.btn = btn;
        this.heroBackgroundImageView = btn.findViewById(R.id.im_hero_icon);
        this.textView = btn.findViewById(R.id.tv_home_btn_hero_title);
        this.contentHolder = btn.findViewById(R.id.cl_content_holder);

        setButtonDisplayScale(null);

        this.addView(this.btn);

        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) this.btn.getLayoutParams();
        params.width = LayoutParams.MATCH_PARENT;
        params.height = LayoutParams.MATCH_PARENT;
        this.btn.setLayoutParams(params);
    }

    public HomeHeroButton(Context context, int width, int height, Bitmap imageSource) {
        super(context);

        this.width = width;
        this.height = height;
        this.style = setBtnStyle(width, height);

        View btn = LayoutInflater.from(context).inflate(R.layout.button_home_hero, null);

        this.btn = btn;
        this.heroBackgroundImageView = btn.findViewById(R.id.im_hero_icon);
        this.textView = btn.findViewById(R.id.tv_home_btn_hero_title);

        setButtonDisplayScale(imageSource);

        this.addView(this.btn);

        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) this.btn.getLayoutParams();
        params.width = LayoutParams.MATCH_PARENT;
        params.height = LayoutParams.MATCH_PARENT;
        this.btn.setLayoutParams(params);
    }

    private Style setBtnStyle(int width, int height) {
        if (width > height) {return Style.HORIZONTAL;}
        if (width <= height) {return Style.VERTICAL;}
        return null;
    }

    private void setButtonDisplayScale(Bitmap imageSource) {

        ConstraintLayout.LayoutParams contentHolderParams = (ConstraintLayout.LayoutParams) this.contentHolder.getLayoutParams();

        int heightUnit = 125;
        int contentHolderHeight = heightUnit * height;

        contentHolderParams.height = contentHolderHeight;
        contentHolder.setLayoutParams(contentHolderParams);


        textView.setTextColor(getContext().getColor(R.color.white));


        if (imageSource != null) {
            heroBackgroundImageView.setImageBitmap(imageSource);
        } else {
            heroBackgroundImageView.setImageDrawable(ChatApplication.context.getDrawable(defaultPlaceholderImage));
        }
    }

    public void updateHeroImage(Bitmap imageSource) {
        this.heroBackgroundImageView.setImageBitmap(imageSource);
    }

    public void updateText(String title) {
        this.textView.setText(title);
    }

    public ImageView getHeroBackgroundImageView() {
        return heroBackgroundImageView;
    }
}
