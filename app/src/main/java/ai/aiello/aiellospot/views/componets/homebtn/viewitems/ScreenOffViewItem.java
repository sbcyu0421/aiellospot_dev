package ai.aiello.aiellospot.views.componets.homebtn.viewitems;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import java.lang.ref.WeakReference;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.core.device.DeviceControl;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractHomeButton;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.HomeBasicButton;

public class ScreenOffViewItem extends AbstractViewItem {

    public static Thread screenOffThread;
    UIHandler uiHandler;

    private static ScreenOffViewItem instance;

    public static ScreenOffViewItem getInstance() {
        if (instance == null) {
            instance = new ScreenOffViewItem();
        }
        return instance;
    }

    private ScreenOffViewItem() {}

    @Override
    public void updateView() {

    }

    @Override
    public void handelBtnClickLogic() {
        wefBtn.get().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DeviceControl.backLightOff();
            }
        });
    }

    @Override
    public void unbindView(Activity activity) {
        super.unbindView(activity);
        if (screenOffThread != null) {
            screenOffThread.interrupt();
            screenOffThread = null;
        }
        if (uiHandler != null) {
            uiHandler.removeCallbacksAndMessages(null);
            uiHandler = null;
        }
    }

    @Override
    public View buildButton(Context context) {
        HomeBasicButton homeBasicButton = new HomeBasicButton(context, this.width, this.height);
        homeBasicButton.updateImage(R.drawable.icon_home1_screenoff);
        homeBasicButton.updateText(R.string.askscreenoff);
        return homeBasicButton;
    }

    static class UIHandler extends Handler {

        Activity activity;

        UIHandler(WeakReference<Activity> reference) {
            activity = reference.get();
        }

        @Override
        public void handleMessage(Message msg) {
        }
    }

}
