package ai.aiello.aiellospot.views.activity.iot;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;

import androidx.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;

public class UiDeviceAC extends UiDevice {

    private static final String TAG = UiDeviceAC.class.getName();
    private int rDegree;
    private int sDegree;
    private WindMode windMode = WindMode.Auto;
    private Power power;
    private HCMode hcMode;


    //before send cmd value
    private int before_rDegree;
    private int before_sDegree;
    private WindMode before_windMode = WindMode.Auto;
    private Power before_power;
    private HCMode before_hcMode;

    private int temperatureMax = 32, temperatureMin = 16;

    private ACPanelView acPanelView;

    private ArrayList<CmdUnit> controlObj = new ArrayList<>();
    private CmdUnit readObj;

    public ArrayList<CmdUnit> getControlObj() {
        return controlObj;
    }

    class CmdUnit {
        private String uuid;

        public CmdUnit(String uuid) {
            this.uuid = uuid;
        }

        public String getUuid() {
            return uuid;
        }
    }

    public UiDeviceAC(Context context, JSONObject obj, int iconRes) throws Exception {
        super(context, obj, iconRes);
        JSONArray controlObjArray = obj.getJSONObject("data").getJSONArray("control");
        JSONArray readObjArray = obj.getJSONObject("data").getJSONArray("read");
        for (int i = 0; i < controlObjArray.length(); i++) {
            String uuid = controlObjArray.getJSONObject(i).getString("uuid");
            controlObj.add(new CmdUnit(uuid));
        }
        // only accept 0 as read obj ( 1 ui )
        readObj = new CmdUnit(
                readObjArray.getJSONObject(0).getString("uuid"));
        //set UI temperature
        setTemperatureMax(temperatureMax);
        setTemperatureMin(temperatureMin);
    }

    public UiDeviceAC(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public UiDeviceAC(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public UiDeviceAC(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private long stopUpdateViewTime = 15 * 1000; // Milli seconds , ms.

    boolean isUpdateAvailable() {
        if (System.currentTimeMillis() - acPanelView.clickTimeStamp > stopUpdateViewTime) {
            return true;
        }
        return false;
    }

    @Override
    void doAction() {
        acPanelView = new ACPanelView(context, R.style.CustomProgressDialog);
        acPanelView.setLayoutParams(1000, 580);
        acPanelView.setContent(this);
        acPanelView.show();
        SpotUserTraceLog3.getInstance().buildUIEventLog(
                SpotUserTraceLog3.EventSubject.IOT,
                SpotUserTraceLog3.EventAction.SELECT,
                name_en
        );
    }

    @Override
    void stopAction() {
        try {
            acPanelView.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public enum WindMode {
        Auto, High, Medium, Low
    }

    public enum Power {
        Off, On
    }

    public enum HCMode {
        Cold, Hot
    }

    public CmdUnit getReadObj() {
        return readObj;
    }

    public WindMode getWindMode() {
        return windMode;
    }

    public int getrDegree() {
        return rDegree;
    }

    public int getsDegree() {
        return sDegree;
    }

    public Power getPower() {
        return power;
    }

    public HCMode getHcMode() {
        return hcMode;
    }


    public void setWindMode(WindMode windMode) {
        Log.d(TAG, "acId(" + readObj.getUuid() + ").setWindMode = " + windMode.name());
        this.windMode = windMode;
    }

    public void setrDegree(int rDegree) {
        Log.d(TAG, "acId(" + readObj.getUuid() + ").setrDegree = " + rDegree);
        this.rDegree = rDegree;
    }

    public void setsDegree(int sDegree) {
        Log.d(TAG, "acId(" + readObj.getUuid() + ").setsDegree = " + sDegree);
        this.sDegree = sDegree;
    }

    public void setPower(Power power) {
        Log.d(TAG, "acId(" + readObj.getUuid() + ").setPower = " + power);
        this.power = power;
    }

    public void setHcMode(HCMode hcMode) {
        Log.d(TAG, "acId(" + readObj.getUuid() + ").setHcMode = " + hcMode);
        this.hcMode = hcMode;
    }

    /**
     * status from Ultron from iot config file
     *
     * @return
     */
    public int getTemperatureMax() {
        return temperatureMax;
    }

    public void setTemperatureMax(int temperatureMax) {
        this.temperatureMax = temperatureMax;
    }

    /**
     * status from Ultron from iot config file
     *
     * @return
     */
    public int getTemperatureMin() {
        return temperatureMin;
    }

    public void setTemperatureMin(int temperatureMin) {
        this.temperatureMin = temperatureMin;
    }

    public void copyStatusBeforeClick() {
        Log.d(TAG, "copyStatusBeforevr3eClick: ");
        setBefore_power(getPower());
        setBefore_windMode(getWindMode());
        setBefore_rDegree(getrDegree());
        setBefore_sDegree(getsDegree());
        setBefore_hcMode(getHcMode());
    }

    //before send cmd value
    public int getBefore_rDegree() {
        return before_rDegree;
    }

    public void setBefore_rDegree(int before_rDegree) {
        this.before_rDegree = before_rDegree;
    }

    public int getBefore_sDegree() {
        return before_sDegree;
    }

    public void setBefore_sDegree(int before_sDegree) {
        this.before_sDegree = before_sDegree;
    }

    public WindMode getBefore_windMode() {
        return before_windMode;
    }

    public void setBefore_windMode(WindMode before_windMode) {
        this.before_windMode = before_windMode;
    }

    public Power getBefore_power() {
        return before_power;
    }

    public void setBefore_power(Power before_power) {
        this.before_power = before_power;
    }

    public HCMode getBefore_hcMode() {
        return before_hcMode;
    }

    public void setBefore_hcMode(HCMode before_hcMode) {
        this.before_hcMode = before_hcMode;
    }
}
