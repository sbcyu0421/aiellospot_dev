package ai.aiello.aiellospot.views.componets.homebtn.viewitems;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;

import java.lang.ref.WeakReference;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.activity.settings.SettingBlueToothActivity;
import ai.aiello.aiellospot.core.device.DeviceControl;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.events.system.SystemEvent;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.HomeBasicButton;

public class BluetoothViewItem extends AbstractViewItem {

    private static final String TAG = BluetoothViewItem.class.getSimpleName();
    public static Thread reconnectThread;
    UIHandler uiHandler;

    private static BluetoothViewItem instance;

    public static BluetoothViewItem getInstance() {
        if (instance == null) {
            instance = new BluetoothViewItem();
        }
        return instance;
    }

    private BluetoothViewItem() {}

    @Override
    public void updateView() {
        updateBTStatus();
    }

    @Override
    public void handelBtnClickLogic() {
        wefBtn.get().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBlueToothClick(wefActivity, wefBtn);
            }
        });
    }

    @Override
    public void unbindView(Activity activity) {
        super.unbindView(activity);
        if (reconnectThread != null) {
            reconnectThread.interrupt();
            reconnectThread = null;
        }
        if (uiHandler != null) {
            uiHandler.removeCallbacksAndMessages(null);
            uiHandler = null;
        }
    }

    @Override
    public View buildButton(Context context) {
        HomeBasicButton homeBasicButton = new HomeBasicButton(context, this.width, this.height);
        homeBasicButton.updateImage(R.drawable.icon_home1_bt_on);
        homeBasicButton.updateText(R.string.bt_home_status_on_detail);
        return homeBasicButton;
    }

    private void onBlueToothClick(WeakReference<Activity> wef, WeakReference<View> btn) {
        if (DeviceControl.blueToothState.equals(DeviceControl.BTState.OFF)) {
            DeviceControl.enableBT();
            ActivityLauncher.launchActivityByClass(wef.get(), SettingBlueToothActivity.class, R.anim.fade_in, R.anim.fade_out);
            SpotUserTraceLog3.getInstance().buildUIEventLog(
                    SpotUserTraceLog3.EventSubject.BLUETOOTH,
                    SpotUserTraceLog3.EventAction.ON,
                    ""
            );
            wef.get().finish();
        } else {
            DeviceControl.disableBT();
            ((HomeBasicButton) btn.get()).updateImage(R.drawable.icon_home1_bt_off);
            SpotUserTraceLog3.getInstance().buildUIEventLog(
                    SpotUserTraceLog3.EventSubject.BLUETOOTH,
                    SpotUserTraceLog3.EventAction.OFF,
                    ""
            );
        }
    }

    public void updateBTStatus() {
        switch (DeviceControl.blueToothState) {
            case ON:
                ((HomeBasicButton) this.wefBtn.get()).updateImage(R.drawable.icon_home1_bt_on);
                ((HomeBasicButton) this.wefBtn.get()).updateText(R.string.bt_home_status_on_detail);
                break;

            case OFF:
                ((HomeBasicButton) this.wefBtn.get()).updateImage(R.drawable.icon_home1_bt_off);
                ((HomeBasicButton) this.wefBtn.get()).updateText(R.string.bt_home_status_off_detail);
                break;

            case CONNECTED:
                ((HomeBasicButton) this.wefBtn.get()).updateImage(R.drawable.icon_home1_bt_connect);
                ((HomeBasicButton) this.wefBtn.get()).updateText(R.string.bt_home_status_on_detail);
                break;
        }
    }

    static class UIHandler extends Handler {

        Activity activity;

        UIHandler(WeakReference<Activity> reference) {
            activity = reference.get();
        }

        @Override
        public void handleMessage(Message msg) {
        }
    }

    @Override
    public void onMessageEvent(SystemEvent event) {
        super.onMessageEvent(event);
        switch (event.getType()){
            case BT_STATE_CHANGE:
                Log.d(TAG, "onMessageEvent: BT_STATE_CHANGE");
                updateBTStatus();
                break;
        }
    }


}
