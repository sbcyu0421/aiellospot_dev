package ai.aiello.aiellospot.views.activity.ads;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import java.util.ArrayList;

public class MPagerAdapter extends PagerAdapter {

    private static final String TAG = MPagerAdapter.class.getSimpleName();
    private ArrayList<ItemView> pageList;

    public MPagerAdapter(ArrayList<ItemView> pageList) {
        this.pageList = pageList;
    }

    @Override
    public int getCount() {
        return pageList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        container.addView(pageList.get(position));
        pageList.get(position).setImage();
        return pageList.get(position);
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return object == view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        Log.d(TAG, "destroyItem ads = " + position);
        pageList.get(position).releaseImage();
        container.removeView((View) object);
    }
}
