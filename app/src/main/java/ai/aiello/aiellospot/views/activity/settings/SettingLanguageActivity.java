package ai.aiello.aiellospot.views.activity.settings;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.library.Style;
import com.litesuits.android.log.Log;

import java.util.ArrayList;
import java.util.Locale;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.core.device.DeviceControl;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.core.soundai.SoundAIManager;
import ai.aiello.aiellospot.modules.ads.AdsModuleManager;
import ai.aiello.aiellospot.modules.music.MusicServiceModuleManager;
import ai.aiello.aiellospot.views.activity.BaseActivity;
import ai.aiello.aiellospot.views.componets.toast.MToaster;

public class SettingLanguageActivity extends BaseActivity implements View.OnClickListener {

    static String TAG = SettingLanguageActivity.class.getSimpleName();

    private LinearLayout ly_back, ly_lang_holder;
    private ArrayList<LanguageButton> buttons;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_language);
        initView();

    }

    private void initView() {
        buttons = new ArrayList<>();

        ly_back = findViewById(R.id.ly_back);
        ly_lang_holder = findViewById(R.id.ly_language_button_holder);

        for (ChatApplication.Language language: ChatApplication.supportLocaleList) {
            LanguageButton languageButton = null;
            switch (language) {
                case zh_TW:
                    languageButton = new LanguageButton(this, this.getString(R.string.zh_TW), language);
                    break;
                case zh_CN:
                    languageButton = new LanguageButton(this, this.getString(R.string.zh_CN), language);
                    break;
                case japanese:
                    languageButton = new LanguageButton(this, this.getString(R.string.jp), language);
                    break;
                case en_US:
                    languageButton = new LanguageButton(this, this.getString(R.string.en_US), language);
                    break;
            }
            if (languageButton != null) {
                buttons.add(languageButton);
                ly_lang_holder.addView(languageButton);
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) languageButton.getLayoutParams();
                params.width = LinearLayout.LayoutParams.MATCH_PARENT;
                params.height = (int) convertDpToPx(110, this);
                params.setMargins((int) convertDpToPx(20, this), 0, (int) convertDpToPx(20, this), 0);
                languageButton.setLayoutParams(params);
            }

//            if (ChatApplication.supportLocaleList.indexOf(language) != ChatApplication.supportLocaleList.size() -1) { // last locale button does not have baseline under it.
            // add baseline
            BaseLine baseLine = new BaseLine(this);
            ly_lang_holder.addView(baseLine);
            LinearLayout.LayoutParams baseLineParams = (LinearLayout.LayoutParams) baseLine.getLayoutParams();
            baseLineParams.width = LinearLayout.LayoutParams.MATCH_PARENT;
            baseLineParams.height = 2;
            baseLineParams.setMargins((int) convertDpToPx(15, this), 0, (int) convertDpToPx(15, this), 0);
            baseLine.setLayoutParams(baseLineParams);
//            }
        }

        ly_back.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateCheck();
    }

    public void updateCheck() {

        for (LanguageButton languageButton: buttons) {
            if (ChatApplication.system_lang == languageButton.language) {
                languageButton.setSelectedImage();
            } else {
                languageButton.setUnselectedImage();
            }
        }



        switch (ChatApplication.system_lang) {
            case en_US:
            case zh_TW:
            case zh_CN:
            case japanese:
                break;
            default:
                Log.d(TAG, "finish it self");
                this.finish();
                break;
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            buttons.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.ly_back:
                Intent intent2 = new Intent(SettingLanguageActivity.this, SettingsActivity.class);
                startActivity(intent2);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
                break;
        }


    }

    public void clickCheckBox(ChatApplication.Language language) {

        switch (language) {

            case japanese:
                if (!ChatApplication.locale.equals(Locale.JAPANESE)) {

                    new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            clearAllCheckBox();
                            MToaster.showButtonToast(SettingLanguageActivity.this, getString(R.string.setting_language_status), Style.TYPE_PROGRESS_BAR);
                        }

                        @Override
                        protected Void doInBackground(Void... voids) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            DeviceControl.updateLanguage(Locale.JAPANESE);
                            ChatApplication.system_lang = ChatApplication.Language.japanese;
                            app.setLang(ChatApplication.system_lang);
                            SoundAIManager.changeWakeupWord(DeviceInfo.saiModel);
                            MusicServiceModuleManager.getInstance().setTerritory();
                            AdsModuleManager.getInstance().fetchAds(5000, 3000, null);

                            SpotUserTraceLog3.getInstance().buildUIEventLog(
                                    SpotUserTraceLog3.EventSubject.LOCALE,
                                    SpotUserTraceLog3.EventAction.SET,
                                    ChatApplication.system_lang.name()
                            );
                            return null;
                        }
                    }.execute();

                } else {
//                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.en_US), Toast.LENGTH_SHORT).show();
                    MToaster.showButtonToast(SettingLanguageActivity.this, getString(R.string.en_US), Style.TYPE_PROGRESS_BAR);
                }

                break;
            case en_US:


                if (!ChatApplication.locale.equals(Locale.ENGLISH)) {

                    new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            clearAllCheckBox();
                            MToaster.showButtonToast(SettingLanguageActivity.this, getString(R.string.setting_language_status), Style.TYPE_PROGRESS_BAR);
                        }

                        @Override
                        protected Void doInBackground(Void... voids) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            DeviceControl.updateLanguage(Locale.ENGLISH);
                            ChatApplication.system_lang = ChatApplication.Language.en_US;
                            app.setLang(ChatApplication.system_lang);
                            SoundAIManager.changeWakeupWord(DeviceInfo.saiModel);
                            MusicServiceModuleManager.getInstance().setTerritory();
                            AdsModuleManager.getInstance().fetchAds(5000, 3000, null);

                            SpotUserTraceLog3.getInstance().buildUIEventLog(
                                    SpotUserTraceLog3.EventSubject.LOCALE,
                                    SpotUserTraceLog3.EventAction.SET,
                                    ChatApplication.system_lang.name()
                            );
                            return null;
                        }
                    }.execute();

                } else {
//                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.en_US), Toast.LENGTH_SHORT).show();
                    MToaster.showButtonToast(SettingLanguageActivity.this, getString(R.string.en_US), Style.TYPE_PROGRESS_BAR);
                }

                break;

            case zh_TW:

                if (!ChatApplication.locale.equals(Locale.TAIWAN)) {

                    new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            clearAllCheckBox();
                            MToaster.showButtonToast(SettingLanguageActivity.this, getString(R.string.setting_language_status), Style.TYPE_PROGRESS_BAR);
                        }

                        @Override
                        protected Void doInBackground(Void... voids) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            DeviceControl.updateLanguage(Locale.TAIWAN);
                            ChatApplication.system_lang = ChatApplication.Language.zh_TW;
                            app.setLang(ChatApplication.system_lang);
                            SoundAIManager.changeWakeupWord(DeviceInfo.saiModel);
                            MusicServiceModuleManager.getInstance().setTerritory();
                            AdsModuleManager.getInstance().fetchAds(5000, 3000, null);

                            SpotUserTraceLog3.getInstance().buildUIEventLog(
                                    SpotUserTraceLog3.EventSubject.LOCALE,
                                    SpotUserTraceLog3.EventAction.SET,
                                    ChatApplication.system_lang.name()
                            );
                            return null;
                        }
                    }.execute();

                } else {
//                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.zh_TW), Toast.LENGTH_SHORT).show();
                    MToaster.showButtonToast(SettingLanguageActivity.this, getString(R.string.zh_TW), Style.TYPE_PROGRESS_BAR);
                }

                break;

            case zh_CN:

                if (!ChatApplication.locale.equals(Locale.CHINA)) {

                    new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            clearAllCheckBox();
                            MToaster.showButtonToast(SettingLanguageActivity.this, getString(R.string.setting_language_status), Style.TYPE_PROGRESS_BAR);
                        }

                        @Override
                        protected Void doInBackground(Void... voids) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            DeviceControl.updateLanguage(Locale.CHINA);
                            ChatApplication.system_lang = ChatApplication.Language.zh_CN;
                            app.setLang(ChatApplication.system_lang);
                            SoundAIManager.changeWakeupWord(DeviceInfo.saiModel);
                            MusicServiceModuleManager.getInstance().setTerritory();
                            AdsModuleManager.getInstance().fetchAds(5000, 3000, null);

                            SpotUserTraceLog3.getInstance().buildUIEventLog(
                                    SpotUserTraceLog3.EventSubject.LOCALE,
                                    SpotUserTraceLog3.EventAction.SET,
                                    ChatApplication.system_lang.name()
                            );
                            return null;
                        }
                    }.execute();

                } else {
//                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.zh_TW), Toast.LENGTH_SHORT).show();
                    MToaster.showButtonToast(SettingLanguageActivity.this, getString(R.string.zh_TW), Style.TYPE_PROGRESS_BAR);
                }


                break;

        }

    }

    public void clearAllCheckBox() {
        for (LanguageButton languageButton: buttons) {
            languageButton.setUnselectedImage();
        }
    }

    private class LanguageButton extends LinearLayout implements View.OnClickListener {

        ChatApplication.Language language;
        ImageView imageView;

        public LanguageButton(Context context, String title, ChatApplication.Language language) {
            super(context);
            this.setOrientation(LinearLayout.HORIZONTAL);
            this.setGravity(Gravity.CENTER_VERTICAL);
            this.language = language;
            this.setBackground(context.getDrawable(R.drawable.btn_ripple2));
            TextView textView = new TextView(context);
            this.addView(textView);
            this.imageView = new ImageView(context);
            this.addView(this.imageView);


            textView.setText(title);
            textView.setTextSize(26);
            LayoutParams textViewParams = (LayoutParams) textView.getLayoutParams();
            textViewParams.width = (int) convertDpToPx(650, context);
            textViewParams.height = LayoutParams.WRAP_CONTENT;
            textViewParams.setMargins((int) convertDpToPx(20, context),0,0,0);
            textView.setLayoutParams(textViewParams);


            this.imageView.setImageDrawable(context.getDrawable(R.drawable.icon_settings_noneselect));
            LayoutParams imageViewParams = (LayoutParams) this.imageView.getLayoutParams();;
            imageViewParams.width = (int) convertDpToPx(59, context);
            imageViewParams.height = (int) convertDpToPx(55, context);
            this.imageView.setLayoutParams(imageViewParams);

            setOnClickListener();
        }

        public void setOnClickListener() {
            this.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            clickCheckBox(this.language);
        }

        public void setUnselectedImage() {
            this.imageView.setImageResource(R.drawable.icon_settings_noneselect);
        }

        public void setSelectedImage() {
            this.imageView.setImageResource(R.drawable.icon_settings_select);
        }
    }

    private class BaseLine extends androidx.appcompat.widget.AppCompatTextView {
        public BaseLine(Context context) {
            super(context);
            this.setBackgroundColor(context.getColor(R.color.white));
        }
    }

    private static float convertDpToPx(float dp, Context context) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

}
