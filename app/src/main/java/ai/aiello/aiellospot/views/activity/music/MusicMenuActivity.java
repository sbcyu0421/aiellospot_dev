package ai.aiello.aiellospot.views.activity.music;

import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.componets.toast.MToaster;
import ai.aiello.aiellospot.views.componets.hints.WakeupHintManager;
import ai.aiello.aiellospot.core.info.DeviceInfo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.johnpersano.supertoasts.library.Style;
import com.litesuits.android.log.Log;
import com.tomer.fadingtextview.FadingTextView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.BaseActivity;
import ai.aiello.aiellospot.views.activity.dialog.AielloProgressDialog;
import ai.aiello.aiellospot.core.log.MusicLogItem;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.events.module.MusicStateChange;
import ai.aiello.aiellospot.modules.music.AlbumObject;
import ai.aiello.aiellospot.modules.music.MusicServiceModuleManager;
import ai.aiello.aiellospot.modules.music.TrackObject;

public class MusicMenuActivity extends BaseActivity {

    private static final String TAG = MusicMenuActivity.class.getSimpleName();
    //vars
    private ArrayList<String> mDescriptionList = new ArrayList<>();
    private ArrayList<String> mTitleList = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();
    private ArrayList<String> mUrlList = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    private RecyclerView recyclerView;
    private RecyclerViewAdapter adapter;
    private AielloProgressDialog aielloProgressDialog = null;
    private ImageView music_logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_menu);
        MusicServiceModuleManager.getInstance().getAlbumList();
        music_logo = findViewById(R.id.music_logo);
        switch (MusicServiceModuleManager.vendor) {
            case KKBOX:
                music_logo.setImageResource(R.drawable.kkbox_logo);
                break;
            case QQ:
                music_logo.setImageResource(R.drawable.qq_logo);
                break;
            default:
                music_logo.setImageResource(R.drawable.qq_logo);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        showHInts();
    }

    @Override
    public void onHotReload() {
        showHInts();
    }

    private void showHInts() {
        FadingTextView tv_slide_text = findViewById(R.id.sl_hint_music);
        String[] hints = new String[]{
                String.format(getResources().getString(R.string.hint_music1), DeviceInfo.assistantName),
                String.format(getResources().getString(R.string.hint_music2), DeviceInfo.assistantName)};

        WakeupHintManager.getInstance().showHints(tv_slide_text, hints);
    }

    private void showAlbums(ArrayList<AlbumObject> list) {
        for (AlbumObject ao : list) {
            mTitleList.add(ao.getTitle());
            mDescriptionList.add(ao.getDescription());
            mUrlList.add(ao.getUrl());
            mImageUrls.add(ao.getImg());
        }

        layoutManager = new LinearLayoutManager(MusicMenuActivity.this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView = findViewById(R.id.rv_music);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new RecyclerViewAdapter(MusicMenuActivity.this, list, new RecyclerViewAdapter.MOnClickListener() {
            @Override
            public void onClick(int position) {
                String albumId = list.get(position).getId();
                Log.d(TAG, "albumId = " + albumId);
                MusicServiceModuleManager.getInstance().playAlbum(albumId);

                MusicLogItem.buildMusicItem(
                        SpotUserTraceLog3.EventSubject.MUSIC,
                        SpotUserTraceLog3.EventAction.PLAY,
                        "",
                        "",
                        "",
                        SpotUserTraceLog3.EventInputType.UI
                );
            }
        });

        recyclerView.setAdapter(adapter);

    }


    public void onMenuBackClick(View view) {
        ActivityLauncher.launchActivityByClass(this, ActivityLauncher.currentHomeName, R.anim.fade_in, R.anim.fade_out);
    }

    private void hideLoadingDialog() {
        if (aielloProgressDialog == null || !aielloProgressDialog.isShowing()) return;
        aielloProgressDialog.dismiss();
        aielloProgressDialog = null;
    }

    private void showLoadingDialog() {
        if (aielloProgressDialog != null && aielloProgressDialog.isShowing())return;
        aielloProgressDialog = new AielloProgressDialog(new WeakReference<Activity>(MusicMenuActivity.this), R.style.CustomProgressDialog);
        aielloProgressDialog.setText(getResources().getString(R.string.searching));
        aielloProgressDialog.show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MusicStateChange event) {
        Log.d(TAG, "UI_MusicEvent: " + event.getTarget());
        hideLoadingDialog();
        switch (event.getTarget()) {

            case ON_FETCH_MENU_COMPLETED:
                ArrayList<AlbumObject> albumList = MusicServiceModuleManager.albumList;
                if (albumList != null) {
                    showAlbums(albumList);
                } else {
                    Toast.makeText(getApplicationContext(), "發生錯誤, 無法取得專輯清單", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(()->{
                        ActivityLauncher.launchActivityByClass(this, ActivityLauncher.currentHomeName, R.anim.fade_in, R.anim.fade_out);
                    }, 500);
                }
                break;

            case ON_PLAY:
                Intent intent = new Intent(MusicMenuActivity.this, MusicPlayer3Activity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
                break;

            case LOADING:
                showLoadingDialog();
                break;

            case ERROR:
                MToaster.showButtonToast(MusicMenuActivity.this, getString(R.string.sth_error), Style.TYPE_STANDARD);
                if (aielloProgressDialog != null && aielloProgressDialog.isShowing()) {
                    aielloProgressDialog.dismiss();
                    aielloProgressDialog = null;
                }
                new Handler().postDelayed(()->{
                    ActivityLauncher.launchActivityByClass(this, ActivityLauncher.currentHomeName, R.anim.fade_in, R.anim.fade_out);
                }, 500);
                break;
        }

    }

    @Override
    protected void onPause() {
        WakeupHintManager.getInstance().cancel();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        hideLoadingDialog();
        super.onDestroy();
    }

}
