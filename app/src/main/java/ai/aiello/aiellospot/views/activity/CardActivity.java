package ai.aiello.aiellospot.views.activity;

import android.os.Bundle;
import android.view.MotionEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.core.device.DeviceControl;
import ai.aiello.aiellospot.views.activity.multi.MultiIntentActivity;
import ai.aiello.aiellospot.core.MCountdownTimer;
import ai.aiello.aiellospot.core.media.MsttsManager;
import ai.aiello.aiellospot.events.system.SystemEvent;
import ai.aiello.aiellospot.intents.IntentManager;
import ai.aiello.aiellospot.intents.sdui.ActionManager;
import ai.aiello.aiellospot.modules.music.MusicServiceModuleManager;


/**
 * Created by a1990 on 2019/3/13.
 */

public abstract class CardActivity extends MyActivity {

    static final String TAG = CardActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    protected void onResume() {
        super.onResume();
        ActivityLauncher.pushToTrace(this, ActivityLauncher.CARD_ACTIVITY);
        MCountdownTimer.startDismissTimer(MCountdownTimer.defaultSilentBackSecond);
    }


    public void confirmAndGoBack() {
        if (ActivityLauncher.isFromMultiActivity() && IntentManager.finishIntentList.size() != 0) {
            ActivityLauncher.launchActivityByClass(this, MultiIntentActivity.class, R.anim.fade_in, R.anim.fade_out);
        } else if (ActivityLauncher.isFromStandbyActivity()) {
            ActivityLauncher.launchActivityByClass(this, ActivityLauncher.currentHomeName, R.anim.fade_in, R.anim.fade_out);
            ActionManager.getInstance().stopForegroundAction();
            MsttsManager.getInstance().stopTTS();
            DeviceControl.volumeResume();
        } else {
            ActivityLauncher.launchActivityByName(this, ActivityLauncher.getBaseActivity(), R.anim.fade_in, R.anim.fade_out);
            ActionManager.getInstance().stopForegroundAction();
            MsttsManager.getInstance().stopTTS();
            DeviceControl.volumeResume();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            MCountdownTimer.startDismissTimer(MCountdownTimer.defaultSilentBackSecond);
            return true;
        } else {
            return false;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(SystemEvent event) {
        switch(event.getType()){
            case TIME_DISMISS:
                confirmAndGoBack();
                break;
        }
    }

}
