package ai.aiello.aiellospot.views.activity.weather;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.CardActivity;

public class WeatherActivity extends CardActivity {


    private TextView tv_weather;
    private LinearLayout btn_ok;
    private String weatherMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activtiy_weather);
        tv_weather = findViewById(R.id.tv_weather);
        btn_ok = findViewById(R.id.btn_ok);

        weatherMsg = intentObject.getMsg1();

        int ml = weatherMsg.length();

        if (ml <= 17) {
            tv_weather.setHeight(180);
        } else if (ml <= 34) {
            tv_weather.setHeight(220);
        } else {
            tv_weather.setHeight(260);
        }

        tv_weather.setText(weatherMsg);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmAndGoBack();
            }
        });

    }


}
