package ai.aiello.aiellospot.views.activity.roomservice;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.CardActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class CleanActivity extends CardActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_clean);


        TextView tv_intent_msg = findViewById(R.id.tv_intent_msg);
        ScrollView scrollView_msg = findViewById(R.id.scrollView_msg);
        LinearLayout btn_ok = findViewById(R.id.btn_ok);

        String msg = intentObject.getMsg1();

        int ml = msg.length();

        if (ml <= 17) {
            tv_intent_msg.setHeight(180);
            scrollView_msg.getLayoutParams().height = 180;
        } else if (ml <= 34) {
            tv_intent_msg.setHeight(220);
            scrollView_msg.getLayoutParams().height = 220;
        } else if (ml <= 51) {
            tv_intent_msg.setHeight(260);
            scrollView_msg.getLayoutParams().height = 260;
        } else {
            scrollView_msg.getLayoutParams().height = 260;
        }

        tv_intent_msg.setText(msg);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmAndGoBack();
            }
        });

    }


}
