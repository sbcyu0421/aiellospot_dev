package ai.aiello.aiellospot.views.activity.notification;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.CardActivity;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.modules.notification.NotifyObject;
import ai.aiello.aiellospot.core.config.APIConfig;
import ai.aiello.aiellospot.core.info.DeviceInfo;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.utlis.Http;

import android.os.Bundle;
import com.litesuits.android.log.Log;
import android.view.View;
import android.widget.TextView;
import com.willy.ratingbar.BaseRatingBar;
import com.willy.ratingbar.ScaleRatingBar;

import org.json.JSONObject;

public class NotifyServiceRating extends CardActivity {

    private ScaleRatingBar service_rating;
    private TextView tv_ok;
    private static String TAG = NotifyServiceRating.class.getSimpleName();
    private NotifyObject notifyObject;
    private int taskId = -1;
    private int cu_rating = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activtiy_notify_rating);

        //httpGet the notifyObject from bundle
        intent = getIntent();
        notifyObject = (NotifyObject) intent.getSerializableExtra(NotifyObject.INTENT);
        if (notifyObject != null) {
            Log.d(TAG, "TaskID =" + notifyObject.getTaskID());
            taskId = Integer.valueOf(notifyObject.getTaskID());
        }

        tv_ok = findViewById(R.id.tv_ok);
        service_rating = findViewById(R.id.service_rating);
        service_rating.setRating(cu_rating);
        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (taskId == -1) {
                  SpotDeviceLog.exception(TAG,"","Invalid TaskID");
                    Log.e(TAG, "invalid TaskID");
                    return;
                }
                sendRating(taskId, cu_rating);
                ActivityLauncher.launchActivityByClass(NotifyServiceRating.this, NotifyServiceRatingThanks.class, R.anim.fade_in, R.anim.fade_out);
            }
        });

        service_rating.setOnRatingChangeListener(new BaseRatingBar.OnRatingChangeListener() {
            @Override
            public void onRatingChange(BaseRatingBar ratingBar, float rating, boolean fromUser) {
                int c = Math.round(rating);
                Log.d(TAG, "click_rating = " + c);
                service_rating.setRating(rating);
                cu_rating = (int) rating;
            }
        });

    }

    public static void sendRating(int taskID, int rating) {
        ChatApplication.worker.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    Log.i(TAG, "sendRating");
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("id", taskID);
                    jsonObject.put("rate", rating);
                    String url = String.format(APIConfig.WEBAPI_POSTRATING.getUrl(), DeviceInfo.MAC);
                    String s = Http.post(url, jsonObject, 5000, APIConfig.WEBAPI_POSTRATING.getKey()).response;
                    JSONObject jObj = new JSONObject(s);
                    boolean isSucceeded = jObj.getBoolean("success");
                    Log.e(TAG, "sendRating success = " + isSucceeded);
                } catch (Exception e) {
                    SpotDeviceLog.exception(TAG, "", "sendRating" + e.getMessage());
                    Log.e(TAG, "sendRating error: " + e.toString());
                }
            }
        });
    }

}
