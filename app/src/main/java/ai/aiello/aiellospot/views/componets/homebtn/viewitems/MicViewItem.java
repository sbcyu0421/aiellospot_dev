package ai.aiello.aiellospot.views.componets.homebtn.viewitems;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import com.github.johnpersano.supertoasts.library.Style;

import java.lang.ref.WeakReference;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.componets.homebtn.HomeBasicButton;
import ai.aiello.aiellospot.views.componets.toast.MToaster;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.core.soundai.SoundAIManager;
import ai.aiello.aiellospot.events.system.SystemEvent;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractHomeButton;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractViewItem;

public class MicViewItem extends AbstractViewItem {
    public static Thread micThread;
    UIHandler uiHandler;

    private static MicViewItem instance;

    public static MicViewItem getInstance() {
        if (instance == null) {
            instance = new MicViewItem();
        }
        return instance;
    }

    private MicViewItem() {}

    @Override
    public void updateView() {
        updateMicStatus();
    }

    @Override
    public void handelBtnClickLogic() {
        wefBtn.get().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SoundAIManager.isWakeUpEnable()) {
                    SoundAIManager.setWakeUp(wefActivity.get(), false);
                    MToaster.showButtonToast(wefActivity.get(), ChatApplication.context.getString(R.string.wakeup_disable), Style.TYPE_STANDARD);
                    SpotUserTraceLog3.getInstance().buildUIEventLog(
                            SpotUserTraceLog3.EventSubject.WAKEUP,
                            SpotUserTraceLog3.EventAction.OFF,
                            ""
                    );
                } else {
                    SoundAIManager.setWakeUp(wefActivity.get(), true);
                    MToaster.showButtonToast(wefActivity.get(), ChatApplication.context.getString(R.string.wakeup_enable), Style.TYPE_STANDARD);
                    SpotUserTraceLog3.getInstance().buildUIEventLog(
                            SpotUserTraceLog3.EventSubject.WAKEUP,
                            SpotUserTraceLog3.EventAction.ON,
                            ""
                    );
                }
            }
        });
    }

    @Override
    public void unbindView(Activity activity) {
        super.unbindView(activity);
        if (micThread != null) {
            micThread.interrupt();
            micThread = null;
        }
        if (uiHandler != null) {
            uiHandler.removeCallbacksAndMessages(null);
            uiHandler = null;
        }
    }

    @Override
    public View buildButton(Context context) {
        HomeBasicButton homeBasicButton = new HomeBasicButton(context, this.width, this.height);
        homeBasicButton.updateImage(R.drawable.icon_home1_micon);
        homeBasicButton.updateText(R.string.mic_off);
        return homeBasicButton;
    }


    static class UIHandler extends Handler {

        Activity activity;

        UIHandler(WeakReference<Activity> reference) {
            activity = reference.get();
        }

        @Override
        public void handleMessage(Message msg) {
        }
    }


    public void updateMicStatus() {
        if (SoundAIManager.isWakeUpEnable()) {
            ((HomeBasicButton) this.wefBtn.get()).updateImage(R.drawable.icon_home1_micon);
            ((HomeBasicButton) this.wefBtn.get()).updateText(R.string.mic_off);
        } else {
            ((HomeBasicButton) this.wefBtn.get()).updateImage(R.drawable.icon_home1_micoff);
            ((HomeBasicButton) this.wefBtn.get()).updateText(R.string.mic_on);
        }
    }

    @Override
    public void onMessageEvent(SystemEvent event) {
        super.onMessageEvent(event);
        switch (event.getType()){
            case WAKEUP_ENABLE:
            case WAKEUP_DISABLE:
                updateMicStatus();
                break;
        }
    }
}
