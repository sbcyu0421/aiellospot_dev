package ai.aiello.aiellospot.views.activity.ads;


import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.modules.ads.AdsObject;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;

public class ItemView extends RelativeLayout {

    private static final String TAG = ItemView.class.getSimpleName();
    private final View mView;
    private AdsObject adsObject;
    private Context context;

    private TextView tv_title;
    private TextView tv_desc;
    private ImageView item_background;
    private VideoView item_video;

    private RelativeLayout content_area;
    private RelativeLayout podcast_area;

    public AdsObject getAdsObject() {
        return adsObject;
    }

    public TextView getTv_title() {
        return tv_title;
    }

    public RelativeLayout getContent_area() {
        return content_area;
    }

    public TextView getTv_desc() {
        return tv_desc;
    }

    public ImageView getItem_background() {
        return item_background;
    }

    public VideoView getItem_video() {
        return item_video;
    }

    public RelativeLayout getPodcast_area() {
        return podcast_area;
    }

    public ItemView(Context context, AdsObject io) {
        super(context);
        this.context = context;
        LayoutInflater inflater = LayoutInflater.from(context);
        mView = inflater.inflate(R.layout.ads_item_view, null);
        this.adsObject = io;
        setInfoMsg();
    }

    public void setInfoMsg() {
        String title = adsObject.getTitle();
        String desc = adsObject.getDescription();

        tv_title = mView.findViewById(R.id.page_title);
        tv_desc = mView.findViewById(R.id.page_desc);
        item_background = mView.findViewById(R.id.item_background);
        item_video = mView.findViewById(R.id.item_video);

        content_area = mView.findViewById(R.id.content_area);
        podcast_area = mView.findViewById(R.id.podcast_area);

        tv_title.setText(title);
        tv_desc.setText(desc);

//        if (adsObject.getAdsGroup() == AdsGroup.Partner) {
//            tv_ads.setText("Partner_Ads");
//            tv_ads.setVisibility(View.VISIBLE);
//        } else if (adsObject.getAdsGroup() == AdsGroup.Self) {
//            tv_ads.setText("Hotel_Ads");
//            tv_ads.setVisibility(View.VISIBLE);
//        } else {
//            tv_ads.setText("Travel_Ads");
//            tv_ads.setVisibility(View.VISIBLE);
//        }
        addView(mView, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
    }

    public void setImage() {
        //set Image, SuperImage, Video layout
        switch (adsObject.getType()) {
            case Image:
                tv_title.setVisibility(View.VISIBLE);
                tv_desc.setVisibility(View.VISIBLE);
                item_background.setVisibility(View.VISIBLE);
                item_video.setVisibility(View.GONE);
                try {
                    File adsFile = new File(adsObject.getLocalUrl());
                    Glide.with(this)
                            .load(adsFile)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(item_background);
                } catch (Exception e) {
                    Log.e(TAG, "image load error = " + e.toString());
                    SpotDeviceLog.exception(TAG, "", e.getMessage());
                }
                break;

            case SuperImage:
                tv_title.setVisibility(View.GONE);
                tv_desc.setVisibility(View.GONE);
                item_background.setVisibility(View.VISIBLE);
                item_video.setVisibility(View.GONE);
                try {
                    File adsFile = new File(adsObject.getLocalUrl());
                    Glide.with(this)
                            .load(adsFile)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(item_background);
                } catch (Exception e) {
                    Log.e(TAG, "image load error = " + e.toString());
                    SpotDeviceLog.exception(TAG, "", e.getMessage());
                }
                break;

            case Video:
                tv_title.setVisibility(View.VISIBLE);
                tv_desc.setVisibility(View.VISIBLE);
                item_background.setVisibility(GONE);
                item_video.setVisibility(VISIBLE);
                break;
        }
    }

    public void releaseImage() {
        Glide.with(this).clear(item_background);
    }

    public void stopPlaying() {
        try {
            if (item_video.isPlaying()) {
                item_video.stopPlayback();
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }

    }

    public void startPlaying() {
        Log.d(TAG, "startPlaying");
        try {
            Log.d(TAG, "adsObject.getUrl() = " + adsObject.getUrl());
            Uri uri = Uri.parse(adsObject.getUrl());
            item_video.setVideoURI(uri);
            item_video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    if (mediaPlayer.isPlaying()) {
                        mediaPlayer.stop();
                        mediaPlayer.release();
                        mediaPlayer = new MediaPlayer();
                    }
                    mediaPlayer.setVolume(0f, 0f);
                    mediaPlayer.setLooping(true);
                    item_video.start();
                }
            });
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }
    }
}
