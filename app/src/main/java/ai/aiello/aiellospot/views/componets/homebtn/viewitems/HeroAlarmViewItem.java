package ai.aiello.aiellospot.views.componets.homebtn.viewitems;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.imknown.bettertextclockbackportlibrary.TextClock;

import java.lang.ref.WeakReference;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.activity.clock.ClockActivity;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractHomeButton;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractViewItem;
import ai.aiello.aiellospot.views.componets.homebtn.HomeClockButton;
import ai.aiello.aiellospot.views.componets.homebtn.HomeHeroButton;
import ai.aiello.aiellospot.views.componets.homebtn.HomeStateButton;

public class HeroAlarmViewItem extends AbstractViewItem {

    public static Thread heroAlarmThread;
    UIHandler uiHandler;

    private static HeroAlarmViewItem instance;

    public static HeroAlarmViewItem getInstance() {
        if (instance == null) {
            instance = new HeroAlarmViewItem();
        }
        return instance;
    }

    private HeroAlarmViewItem() {}

    @Override
    public void updateView() {

    }

    @Override
    public void handelBtnClickLogic() {
        wefBtn.get().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityLauncher.launchActivityByClass(wefActivity.get(), ClockActivity.class, R.anim.fade_in, R.anim.fade_out);
            }
        });
    }

    @Override
    public void unbindView(Activity activity) {
        super.unbindView(activity);
        if (heroAlarmThread != null) {
            heroAlarmThread.interrupt();
            heroAlarmThread = null;
        }
        if (uiHandler != null) {
            uiHandler.removeCallbacksAndMessages(null);
            uiHandler = null;
        }
    }

    @Override
    public View buildButton(Context context) {
        return new HomeClockButton(context, this.width, this.height);
    }

    static class UIHandler extends Handler {

        Activity activity;

        UIHandler(WeakReference<Activity> reference) {
            activity = reference.get();
        }

        @Override
        public void handleMessage(Message msg) {
        }
    }
}
