package ai.aiello.aiellospot.views.activity.settings;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.activity.BaseActivity;
import ai.aiello.aiellospot.views.activity.new_ota.VersionCheckerT;
import ai.aiello.aiellospot.core.device.DeviceControl;
import ai.aiello.aiellospot.events.system.SystemEvent;

public class SettingsActivity extends BaseActivity implements View.OnClickListener {

    static String TAG = SettingsActivity.class.getSimpleName();
    private TextView tv_bt_status;
    private ImageView img_setting_bt;
    private int clickCount = 0;
    private CountDownTimer timer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        initView();
        updateView();
    }

    private void initView() {
        LinearLayout ly_set_bt = findViewById(R.id.ly_set_bt);
        LinearLayout ly_set_lang = findViewById(R.id.ly_set_lang);
        LinearLayout ly_set_screen = findViewById(R.id.ly_set_screen);
        LinearLayout ly_back = findViewById(R.id.ly_back);
        LinearLayout ly_title = findViewById(R.id.ly_title);

        tv_bt_status = findViewById(R.id.tv_bt_status);
        img_setting_bt = findViewById(R.id.img_setting_bt);

        TextView tv_version = findViewById(R.id.tv_version);

        ly_set_bt.setOnClickListener(this);
        ly_set_lang.setOnClickListener(this);
        ly_set_screen.setOnClickListener(this);
        ly_back.setOnClickListener(this);
        ly_title.setOnClickListener(this);

        tv_version.setText("版本:  " + VersionCheckerT.apk_ver + " | " + VersionCheckerT.push_ver + " | " + VersionCheckerT.music_ver + " | " + VersionCheckerT.sai_ver);

    }

    public void updateView() {

        switch (DeviceControl.blueToothState) {

            case ON:
                tv_bt_status.setText(R.string.bt_status_open);
                img_setting_bt.setImageResource(R.drawable.icon_home1_bt_on);
                break;
            case CONNECTED:
                tv_bt_status.setText(getResources().getString(R.string.status_bluetooth_connected) + DeviceControl.bondedDevicesName);
                img_setting_bt.setImageResource(R.drawable.icon_home1_bt_connect);
                break;
            case OFF:
                tv_bt_status.setText(R.string.bt_status_close);
                img_setting_bt.setImageResource(R.drawable.icon_home1_bt_off);
                break;
        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.ly_back:
                Intent intent2 = new Intent(SettingsActivity.this, ActivityLauncher.currentHomeName);
                startActivity(intent2);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
                break;

            case R.id.ly_set_screen:
                Intent intent3 = new Intent(SettingsActivity.this, SettingBrightnessActivity.class);
                startActivity(intent3);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
                break;

            case R.id.ly_set_lang:
                Intent intent5 = new Intent(SettingsActivity.this, SettingLanguageActivity.class);
                startActivity(intent5);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
                break;


            case R.id.ly_set_bt:
                Intent intent6 = new Intent(SettingsActivity.this, SettingBlueToothActivity.class);
                startActivity(intent6);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
                break;

            case R.id.ly_title:
                if (clickCount == 0) {
                    timer = new CountDownTimer(5000, 1000) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            clickCount = 0;
                        }
                    };

                    timer.start();
                }
                clickCount += 1;
                if (clickCount >= 5) {
                    timer.onFinish();
                    Intent intent7 = new Intent(SettingsActivity.this, MagicNumberActivity.class);
                    startActivity(intent7);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();
                }
                break;

        }


    }

    @Override
    protected void onDestroy() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        super.onDestroy();
    }

    @Override
    public void onMessageEvent(SystemEvent event) {
        super.onMessageEvent(event);
        switch (event.getType()){
            case BT_STATE_CHANGE:
                updateView();
                break;
        }
    }

}
