package ai.aiello.aiellospot.views.activity.showcase.fragments

import ai.aiello.aiellospot.ChatApplication
import ai.aiello.aiellospot.databinding.ItemHotelFeatureStadardBinding
import ai.aiello.aiellospot.views.activity.showcase.beans.HotelFeatureDataBean
import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class HotelFeatureDetailAdapter(val onClickListener: OnClickListener): ListAdapter<HotelFeatureDataBean, RecyclerView.ViewHolder>(CategoryDiffCallback) {

    class ViewHolder(val binding: ItemHotelFeatureStadardBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(item: HotelFeatureDataBean) {
            binding.hotelFeatureData = item
            binding.tvHotelFeatureTitle.text = item.getTitle(ChatApplication.system_lang)
            binding.tvHotelFeatureContent.text = item.getContent(ChatApplication.system_lang)
            binding.imgHotelFeatureHero.setImageBitmap(item.getImageFromSDCard())
            binding.executePendingBindings()
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(ItemHotelFeatureStadardBinding.inflate(inflater, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val hotelFeatureDetail = getItem(position)

        if (holder is ViewHolder) {
            holder.bind(hotelFeatureDetail)
            holder.itemView.setOnClickListener { onClickListener.onClick(hotelFeatureDetail) }
        }
    }


    companion object CategoryDiffCallback : DiffUtil.ItemCallback<HotelFeatureDataBean>() {
        override fun areItemsTheSame(oldItem: HotelFeatureDataBean, newItem: HotelFeatureDataBean): Boolean {
            return oldItem === newItem
        }

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: HotelFeatureDataBean, newItem: HotelFeatureDataBean): Boolean {
            return oldItem == newItem
        }
    }

    class OnClickListener(val clickListener: (item: HotelFeatureDataBean) -> Unit) {
        fun onClick(item: HotelFeatureDataBean) = clickListener(item)
    }
}