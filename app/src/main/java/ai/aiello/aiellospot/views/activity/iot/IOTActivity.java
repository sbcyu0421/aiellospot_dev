package ai.aiello.aiellospot.views.activity.iot;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.GridLayout;
import android.widget.HorizontalScrollView;

import com.litesuits.android.log.Log;
import com.tomer.fadingtextview.FadingTextView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.events.module.IoTStateChange;
import ai.aiello.aiellospot.intents.IntentManager;
import ai.aiello.aiellospot.modules.iot.IoTModuleManager;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.activity.BaseActivity;
import ai.aiello.aiellospot.views.activity.multi.MultiIntentActivity;
import ai.aiello.aiellospot.views.componets.hints.WakeupHintManager;


public class IOTActivity extends BaseActivity {


    final String TAG = IOTActivity.class.getSimpleName();
    ArrayList<UiDevice> list;
    GridLayout gridLayout;
    private HorizontalScrollView scrollView;

    static int rowNum = 0;
    static int colNum = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iot);


        gridLayout = findViewById(R.id.gridLayout);
        scrollView = findViewById(R.id.scrollView);
        activeDevice();

    }


    private void activeDevice() {
        //init
        list = new ArrayList<>();
        try {
            JSONArray devices_array = IoTModuleManager.devices_array;
            for (int s = 0; s < devices_array.length(); s++) {
                try {
                    JSONObject obj = devices_array.getJSONObject(s);
                    String deviceType = obj.getString("deviceType");
                    UiDevice uiDevice = null;
                    switch (deviceType) {
                        case "ac":
                            uiDevice = new UiDeviceAC(this, obj, R.drawable.ic_iot_ac);
                            break;
                        case "light":
                            uiDevice = new UiDeviceSwitch(this, obj, R.drawable.ic_iot_light);
                            break;
                        case "mode":
                            uiDevice = new UiDeviceSwitch(this, obj, R.drawable.ic_iot_mode);
                            break;
                        case "curtain":
                            uiDevice = new UiDeviceSwitch(this, obj, R.drawable.ic_iot_curtain);
                            break;
//                        default:
//                            uiDevice = new UiDeviceLight(this, name_tw, name_cn, name_ja, name_en, id, cmdData, cmdIntent, deviceType, R.drawable.ic_iot_default);
//                            break;
                    }
                    list.add(uiDevice);
                    rowNum = s % 2;
                    colNum = s / 2;
                    uiDevice.setLayoutParams(new GridLayout.LayoutParams(GridLayout.spec(rowNum), GridLayout.spec(colNum)));
                    gridLayout.addView(uiDevice);
                } catch (Exception e) {
                    SpotDeviceLog.exception(TAG, "", e.getMessage());
                    Log.e(TAG, "iot index ( " + s + " ) is error = " + e.toString());
                }
            }

            //update cell size
            for (UiDevice uiDevice : list) {
                if (list.size() > 8) {
                    uiDevice.updateCellSize(350);
                } else if (list.size() > 6) { //freeze layout
                    uiDevice.updateCellSize(310);
                    disableScroll();
                } else { //freeze layout
                    uiDevice.updateCellSize(413);
                    disableScroll();
                }
            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        }

    }

    private void disableScroll() {
        //disable scroll
        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        showHints();
    }

    @Override
    public void onHotReload() {
        showHints();
    }

    private void showHints() {
        FadingTextView tv_slide_text = findViewById(R.id.sl_hint_iot);
        String[] hints = new String[]{
                String.format(getResources().getString(R.string.hint_iot_control), DeviceInfo.assistantName),
                String.format(getResources().getString(R.string.hint_iot_control2), DeviceInfo.assistantName)};
        WakeupHintManager.getInstance().showHints(tv_slide_text, hints);
    }

    @Override
    protected void onStop() {
        WakeupHintManager.getInstance().cancel();
        super.onStop();
    }

    public void onIOTBackClick(View view) {
        if (ActivityLauncher.isFromMultiActivity() && IntentManager.finishIntentList.size() != 0) {
            ActivityLauncher.launchActivityByClass(this, MultiIntentActivity.class, R.anim.fade_in, R.anim.fade_out);
        } else {
            ActivityLauncher.launchActivityByClass(this, ActivityLauncher.currentHomeName, R.anim.fade_in, R.anim.fade_out);
        }
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "iot onDestroy");
        for (int i = 0; i < gridLayout.getChildCount(); i++) {
            ((UiDevice) gridLayout.getChildAt(i)).stopAction();
        }
        super.onDestroy();
    }

    //would be active by ac, so far
    public ArrayList updateACStatus(String status) {
        Log.d(TAG, "updateACStatus = " + status);
        try {
            JSONObject deviceInfo = new JSONObject(status);
            JSONObject deviceStatus = deviceInfo.getJSONObject("other_info").getJSONObject("status");
            boolean power = deviceStatus.getBoolean("switch");
            String wind = deviceStatus.getString("fan");
            int roomDegree = deviceStatus.getInt("temperature");
            int setDegree = deviceStatus.getInt("set_temperature");
            String hcmode = deviceStatus.getString("mode");

            JSONObject profile = deviceInfo.getJSONObject("other_info").getJSONObject("profile");
            String uuid = profile.getString("uid");

            for (UiDevice uiDevice : list) {
                if (uiDevice instanceof UiDeviceAC) {
                    UiDeviceAC tmp_device = (UiDeviceAC) uiDevice;
                    if (tmp_device.getReadObj().getUuid().equals(uuid)) { // ***

                        try {
                            int tempMax = profile.getInt("temperatureMax");
                            int tempMin = profile.getInt("temperatureMin");

                            if (tmp_device.getTemperatureMax() != tempMax)
                                tmp_device.setTemperatureMax(tempMax);
                            if (tmp_device.getTemperatureMin() != tempMin)
                                tmp_device.setTemperatureMin(tempMin);
                        } catch (JSONException e) {
                            android.util.Log.d(TAG, String.format("Please check PushService vendor ACPanel device profile data" +
                                    ", get temperature limit error : %s ", e.toString()));
                        }

                        if (!tmp_device.isUpdateAvailable()) {
                            return null;
                        }

                        if (power) {
                            tmp_device.setPower(UiDeviceAC.Power.On);
                        } else {
                            tmp_device.setPower(UiDeviceAC.Power.Off);
                        }

                        switch (wind) {
                            case "A":
                                tmp_device.setWindMode(UiDeviceAC.WindMode.Auto);
                                break;
                            case "H":
                                tmp_device.setWindMode(UiDeviceAC.WindMode.High);
                                break;
                            case "M":
                                tmp_device.setWindMode(UiDeviceAC.WindMode.Medium);
                                break;
                            case "L":
                                tmp_device.setWindMode(UiDeviceAC.WindMode.Low);
                                break;
                        }

                        tmp_device.setrDegree(roomDegree);
                        tmp_device.setsDegree(setDegree);

                        switch (hcmode) {
                            case "C":
                                tmp_device.setHcMode(UiDeviceAC.HCMode.Cold);
                                break;
                            case "H":
                                tmp_device.setHcMode(UiDeviceAC.HCMode.Hot);
                                break;
                        }
                        tmp_device.copyStatusBeforeClick();
                    }
                }
            }
        } catch (Exception e) {
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e(TAG, e.toString());
        }
        return null;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(IoTStateChange event) {
        switch (event.getTarget()) {
            case ON_AC_PANEL_UPDATE:
                try {
                    String ac_status = event.getData();
                    updateACStatus(ac_status);
                } catch (Exception e) {
                    SpotDeviceLog.exception(TAG, "", e.getMessage());
                    Log.e(TAG, e.toString());
                }
                break;
        }
    }


}
