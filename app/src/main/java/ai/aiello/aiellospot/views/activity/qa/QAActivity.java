package ai.aiello.aiellospot.views.activity.qa;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.CardActivity;

public class QAActivity extends CardActivity {


    private TextView tv_qa;
    private LinearLayout btn_ok;
    private String qaMsg;
    private ScrollView scrollView_msg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activtiy_qa);
        tv_qa = findViewById(R.id.tv_roobo_msg);
        btn_ok = findViewById(R.id.btn_ok);
        scrollView_msg = findViewById(R.id.scrollView_msg);

        qaMsg = intentObject.getMsg1();

        int ml = qaMsg.length();

        if (ml <= 17) {
            tv_qa.setHeight(180);
            scrollView_msg.getLayoutParams().height = 180;
        } else if (ml <= 34) {
            tv_qa.setHeight(220);
            scrollView_msg.getLayoutParams().height = 220;
        } else if (ml <= 51) {
            tv_qa.setHeight(260);
            scrollView_msg.getLayoutParams().height = 260;
        }else {
            scrollView_msg.getLayoutParams().height = 260;
        }

        tv_qa.setText(qaMsg);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmAndGoBack();
            }
        });

    }

}
