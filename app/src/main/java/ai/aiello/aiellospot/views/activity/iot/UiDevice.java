package ai.aiello.aiellospot.views.activity.iot;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.Nullable;

import org.json.JSONObject;

public abstract class UiDevice extends LinearLayout {
    private static final String TAG = UiDevice.class.getSimpleName();

    public String name_tw;
    public String name_cn;
    public String name_ja;
    public String name_en;
    public ImageView iot_image;
    public TextView iot_name;
    public Context context;

    private LinearLayout menuBound;

    public UiDevice(Context context, JSONObject obj, int iconRes) throws Exception{
        super(context);
        LayoutInflater.from(context).inflate(R.layout.iot_menu_button, this);
        this.name_tw = obj.getJSONObject("display_name").getString("name_tw");
        this.name_cn = obj.getJSONObject("display_name").getString("name_cn");
        this.name_ja = obj.getJSONObject("display_name").getString("name_ja");
        this.name_en = obj.getJSONObject("display_name").getString("name_en");
        this.context = context;

        iot_image = this.findViewById(R.id.iot_image);
        iot_name = this.findViewById(R.id.iot_name);
        menuBound = this.findViewById(R.id.menuBound);

        switch (ChatApplication.system_lang) {

            case japanese:
                iot_name.setText(name_ja);
                break;

            case zh_CN:
                iot_name.setText(name_cn);
                break;

            case zh_TW:
                iot_name.setText(name_tw);
                break;

            case en_US:
            default:
                iot_name.setText(name_en);
                break;
        }

        iot_image.setImageResource(iconRes);

        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                doAction();
            }
        });

    }

    public TextView getIot_name() {
        return iot_name;
    }

    public UiDevice(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public UiDevice(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public UiDevice(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void updateCellSize(int width) {
        LayoutParams layoutParams = new LayoutParams(width, 250);
        layoutParams.width = width;
        layoutParams.setMargins(3, 3, 3, 3);
        menuBound.setLayoutParams(layoutParams);
    }

    abstract void doAction();

    abstract void stopAction();

}
