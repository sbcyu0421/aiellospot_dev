package ai.aiello.aiellospot.views.activity.laundry;

import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.CardActivity;
import ai.aiello.aiellospot.views.activity.multi.MultiIntentActivity;

public class LaundryPriceActivity extends CardActivity {

    private List<LaundryItem> itemList1, itemList2;
    private ItemAdapter adapter1, adapter2;
    private ListView listView1, listView2;
    private LinearLayout btn_ok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laundry_price);
        itemList1 = new ArrayList<LaundryItem>();
        itemList2 = new ArrayList<LaundryItem>();

        adapter1 = new ItemAdapter(this, itemList1);
        adapter2 = new ItemAdapter(this, itemList2);

        listView1 = (ListView) findViewById(R.id.listview1);
        listView2 = (ListView) findViewById(R.id.listview2);

        itemList1.add(new LaundryItem("外套", "120元"));
        itemList1.add(new LaundryItem("長褲", "40元"));
        itemList1.add(new LaundryItem("襯衫", "32元"));
        itemList1.add(new LaundryItem("禮服襯衫", "48元"));
        itemList1.add(new LaundryItem("內褲", "40元"));
        itemList1.add(new LaundryItem("長褲", "40元"));
        itemList1.add(new LaundryItem("手帕", "20元"));
        itemList1.add(new LaundryItem("睡衣(套)", "100元"));
        itemList1.add(new LaundryItem("運動衣", "100元"));
        itemList1.add(new LaundryItem("毛衣", "120元"));
        itemList1.add(new LaundryItem("牛仔褲", "40元"));
        itemList1.add(new LaundryItem("牛仔裙", "40元"));
        itemList1.add(new LaundryItem("半截裙", "40元"));
        itemList1.add(new LaundryItem("連衣裙", "54元"));
        itemList1.add(new LaundryItem("短袖汗衫", "40元"));
        itemList1.add(new LaundryItem("長襪", "30元"));
        itemList1.add(new LaundryItem("胸圍", "60元"));


        listView1.setAdapter(adapter1);

        //------------------------------------------

        itemList2.add(new LaundryItem("上衣", "60元"));
        itemList2.add(new LaundryItem("羊毛上衣", "80元"));
        itemList2.add(new LaundryItem("外套", "60元"));
        itemList2.add(new LaundryItem("西褲", "60元"));
        itemList2.add(new LaundryItem("短襪", "40元"));
        itemList2.add(new LaundryItem("羊毛上衣", "80元"));
        itemList2.add(new LaundryItem("襯衫", "80元"));
        itemList2.add(new LaundryItem("大衣", "160元"));
        itemList2.add(new LaundryItem("短襪", "40元"));
        itemList2.add(new LaundryItem("圍巾", "140元"));
        itemList2.add(new LaundryItem("羊毛上衣", "80元"));
        itemList2.add(new LaundryItem("睡袍", "120元"));
        itemList2.add(new LaundryItem("連衣裙", "100元"));
        itemList2.add(new LaundryItem("牛仔衫", "120元"));
        itemList2.add(new LaundryItem("牛仔褲", "40元"));
        itemList2.add(new LaundryItem("西裝裙", "40元"));
        itemList2.add(new LaundryItem("領帶", "40元"));
        itemList2.add(new LaundryItem("皮衣", "320元"));

        listView2.setAdapter(adapter2);

        btn_ok = findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ActivityLauncher.isFromStandbyActivity()) {
                    ActivityLauncher.launchActivityByClass(LaundryPriceActivity.this, ActivityLauncher.currentHomeName, R.anim.fade_in, R.anim.fade_out);
                } else {
                    ActivityLauncher.launchActivityByClass(LaundryPriceActivity.this, MultiIntentActivity.class, R.anim.fade_in, R.anim.fade_out);
                }
            }
        });

    }


}
