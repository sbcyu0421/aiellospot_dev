package ai.aiello.aiellospot.views.activity;

import ai.aiello.aiellospot.views.activity.ads.PodcastActivity;
import ai.aiello.aiellospot.views.activity.ads.StandbyActivity;
import ai.aiello.aiellospot.views.activity.clock.AlarmRiseActivity;
import ai.aiello.aiellospot.views.activity.home.MainHome2Activity;
import ai.aiello.aiellospot.views.activity.home.MainHomeActivity;
import ai.aiello.aiellospot.views.activity.laundry.LaundryPriceActivity;
import ai.aiello.aiellospot.views.activity.multi.MultiIntentActivity;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.litesuits.android.log.Log;

public class ActivityLauncher {

    public static String TAG = ActivityLauncher.class.getSimpleName();

    private static String baseActivity = "";
    private static String cardActivity = "";

    public static final int BASE_ACTIVITY = 1;
    public static final int CARD_ACTIVITY = 2;

    private static final String className_StandbyActivity = StandbyActivity.class.getName();
    private static final String className_MultiIntentActivity = MultiIntentActivity.class.getName();
    private static final String className_HomeActivity = MainHomeActivity.class.getName();
    private static final String className_Home2Activity = MainHome2Activity.class.getName();
    private static final String className_AlarmRiseActivity = AlarmRiseActivity.class.getName();
    private static final String className_LaundryPriceActivity = LaundryPriceActivity.class.getName();
    private static final String className_PocastActivity = PodcastActivity.class.getName();

    public static String lastActivity = "";
    public static String thisActivity = "";
    public static Class currentHomeName = MainHomeActivity.class;



    public static void pushToTrace(Activity activity, int saveType) {

        switch (saveType) {
            case 1:
                baseActivity = activity.getClass().getName();
                Log.d(TAG, "baseActivity=" + baseActivity);
                break;

            case 2: //cardActivity
                cardActivity = activity.getClass().getName();
                Log.d(TAG, "cardActivity=" + cardActivity);
                break;
        }

    }

    public static String getBaseActivity() {
        return baseActivity;
    }


    // ================= from =================

    public static boolean isFromStandbyActivity() {
        boolean t = false;
        if (lastActivity.equals(className_StandbyActivity)) {
            t = true;
        }
        return t;
    }


    public static boolean isFromHomeActivity() {
        boolean t = false;
        if (lastActivity.equals(className_HomeActivity) || lastActivity.equals(className_Home2Activity)) {
            t = true;
        }
        return t;
    }

    public static boolean isFromMultiActivity() {
        boolean t = false;
        if (lastActivity.equals(className_MultiIntentActivity)) {
            t = true;
        }
        return t;
    }


    // ================= on front =================


    public static boolean isHomeActivityOnFront() {
        boolean t = false;
        if (thisActivity.equals(className_HomeActivity)) {
            t = true;
        }
        return t;
    }


    public static boolean isStandbyActivityOnFront() {
        boolean t = false;
        if (thisActivity.equals(className_StandbyActivity)) {
            t = true;
        }
        return t;
    }


    public static boolean isAlarmOnFront() {
        boolean t = false;
        if (thisActivity.equals(className_AlarmRiseActivity)) {
            t = true;
        }
        return t;
    }

    public static boolean isLaundryPriceActivityOnFront() {
        boolean t = false;
        if (thisActivity.equals(className_LaundryPriceActivity)) {
            t = true;
        }
        return t;
    }

    public static boolean isPocastActivityOnFront() {
        boolean t = false;
        if (thisActivity.equals(className_PocastActivity)) {
            t = true;
        }
        return t;
    }

    private static long lastClickTime = 0;

    public static void launchActivityByName(Activity activity, String className, int enterAnim, int exitAnim) {
        long currentTime = System.currentTimeMillis();
        if ((currentTime - lastClickTime) < 1000) return;
        lastClickTime = currentTime;

        Intent intent = null;
        Log.d(TAG, "launchActivity from = " + activity.getClass().getSimpleName() + ", to = " + className);
        try {
            intent = new Intent(activity.getApplicationContext(), getActivityClass(className, activity));
        } catch (Exception e) {
            e.printStackTrace();
          SpotDeviceLog.exception(TAG,"","launchActivityByName error = "+e.getMessage());
            Log.e(TAG, "launchActivityByName error = " + e.toString());
        }
        activity.startActivity(intent);
        activity.overridePendingTransition(enterAnim, exitAnim);
        activity.finish();
    }


    public static void launchActivityByClass(Activity activity, Class goClass, int enterAnim, int exitAnim) {
        Intent intent = new Intent(activity.getApplicationContext(), goClass);
        activity.startActivity(intent);
        activity.overridePendingTransition(enterAnim, exitAnim);
        activity.finish();
    }


    private static Class<? extends Activity> getActivityClass(String target, Context context) throws Exception {
        ClassLoader classLoader = context.getClassLoader();
        @SuppressWarnings("unchecked")
        Class<? extends Activity> activityClass = (Class<? extends Activity>) classLoader.loadClass(target);
        return activityClass;
    }


}
