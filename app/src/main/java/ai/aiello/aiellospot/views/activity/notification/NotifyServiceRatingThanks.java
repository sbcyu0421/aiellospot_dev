package ai.aiello.aiellospot.views.activity.notification;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.CardActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class NotifyServiceRatingThanks extends CardActivity {

    private static String TAG = NotifyServiceRatingThanks.class.getSimpleName();
    private TextView tv_ok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activtiy_notify_rating_thanks);
        tv_ok = findViewById(R.id.tv_ok);

        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmAndGoBack();
            }
        });

    }

}
