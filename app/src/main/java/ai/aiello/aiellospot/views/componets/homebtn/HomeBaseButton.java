package ai.aiello.aiellospot.views.componets.homebtn;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;

import static android.widget.LinearLayout.HORIZONTAL;
import static android.widget.LinearLayout.VERTICAL;


public abstract class HomeBaseButton extends ConstraintLayout {

    static String TAG = HomeBasicButton.class.getSimpleName();

    public int width;
    public int height;
    public View btn;
    public Style style;
    public ImageView imageView;
    public TextView textView;
    public RelativeLayout imageHolder;
    public LinearLayout viewHolder;

    RelativeLayout.LayoutParams imageParams;
    LinearLayout.LayoutParams imageHolderParams;
    LinearLayout.LayoutParams textParams;
    RelativeLayout.LayoutParams viewHolderParams;

    final static int widthUnit = 160;
    final static int heightUnit = 180;

    enum Style {
        VERTICAL, HORIZONTAL, TINY
    }

    public HomeBaseButton(Context context, int width, int height) {
        super(context);

        initViews(context, width, height);

        imageParams = (RelativeLayout.LayoutParams) this.imageView.getLayoutParams();
        imageHolderParams = (LinearLayout.LayoutParams) this.imageHolder.getLayoutParams();
        textParams = (LinearLayout.LayoutParams) this.textView.getLayoutParams();
        viewHolderParams = (RelativeLayout.LayoutParams) this.viewHolder.getLayoutParams();

        setButtonDisplayOrientation();
        setButtonDisplayScale();

        this.addView(this.btn);

        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) this.btn.getLayoutParams();
        params.width = LayoutParams.MATCH_PARENT;
        params.height = LayoutParams.MATCH_PARENT;
        this.btn.setLayoutParams(params);
    }

    abstract int getLayoutXml();

    private Style setBtnStyle(int width, int height) {
        if (width == height && width == 1) {return Style.TINY;}
        if (width > height) {return Style.HORIZONTAL;}
        if (width <= height) {return Style.VERTICAL;}
        return null;
    }

    private void setButtonDisplayOrientation() {

        switch (style) {
            case HORIZONTAL:
                this.viewHolder.setOrientation(HORIZONTAL);
                break;

            case TINY:

            case VERTICAL:
                this.viewHolder.setOrientation(VERTICAL);
                break;
        }
    }

    public void setButtonDisplayScale() {
        switch (style) {
            case HORIZONTAL:
                switch (height) {
                    case 1:
                        ontHorizontalHeight1Layout();
                        break;
                    case 2:
                        ontHorizontalHeight2Layout();
                        break;
                    case 3:
                        ontHorizontalHeight3Layout();
                        break;
                    case 4:
                        ontHorizontalHeight4Layout();
                        break;
                }
                break;

            case VERTICAL:
                switch (width) {
                    case 1:
                        onVerticalWidth1Layout();
                        break;
                    case 2:
                        onVerticalWidth2Layout();
                        break;
                    case 3:
                        onVerticalWidth3Layout();
                        break;
                    case 4:
                        onVerticalWidth4Layout();
                        break;
                }
                break;

            case TINY: // width 1 * height 1
                onTinyLayout();
                break;
        }
        applyLayoutParams();
    }


    /*
     *  Horizontal layout type of base button represents the a horizontal order of imageView(icon) and textView(title).
     *  This type of layout is used when button's " Width > Height ".
     *  Under this setup, the image size and text size depend on button's " Height ", since it is the smaller parameter and will restrict the size of elements in it.
     *  The following condition are name by the order of "on"-"layout orientation"-"size depending parameter"-"parameter size".
     */
    public void ontHorizontalHeight1Layout() { }

    public void ontHorizontalHeight2Layout() { }

    public void ontHorizontalHeight3Layout() { }

    public void ontHorizontalHeight4Layout() { }

    /*
     *  Vertical layout type of base button represents the a vertical order of imageView(icon) and textView(title).
     *  This type of layout is used when button's " Width <= Height ", but both width and height can't be one a the same time.
     *  Under this setup, the image size and text size depend on button's " Width ", since it is the smaller parameter and will restrict the size of elements in it.
     *  The following condition are name by the order of "on"-"layout orientation"-"size depending parameter"-"parameter size".
     */
    public void onVerticalWidth1Layout() { }

    public void onVerticalWidth2Layout() { }

    public void onVerticalWidth3Layout() { }

    public void onVerticalWidth4Layout() { }

    /*
     *  Tiny layout type of base button represents a button with imageView(O) but textView(X).
     *  This type of layout is used when button's " Width = Height = 1 ".
     */
    public void onTinyLayout() { }

    public void initViews(Context context, int width, int height) {
        this.width = width;
        this.height = height;
        this.style = setBtnStyle(width, height);

        View btn = LayoutInflater.from(context).inflate(getLayoutXml(), null);

        this.btn = btn;
        this.imageView = btn.findViewById(R.id.im_button_icon);
        this.textView = btn.findViewById(R.id.tv_button_title);
        this.imageHolder = btn.findViewById(R.id.rv_image_holder);
        this.viewHolder = btn.findViewById(R.id.ll_content_holder);
    };

    public void applyLayoutParams() {

        switch (style) {
            case VERTICAL:
                int innerHeight = height > 2 ? (heightUnit * 2) : (int)(( heightUnit * height ) / 1.2);
                viewHolderParams.height = innerHeight ;
                viewHolderParams.width = ( widthUnit * width ) ;
                imageHolderParams.weight = 1;
                textParams.weight = 2;
                imageParams.setMargins(0,0,0,0);
                break;

            case HORIZONTAL:
                viewHolderParams.height = (heightUnit * height);
                int innerWidth = width > 4 ? (widthUnit * 4) : (int)(( widthUnit * width ) / 1.2);
                viewHolderParams.width = innerWidth;
                imageHolderParams.weight = 2;
                imageParams.setMargins(0,0,0,0);
                textParams.weight = 1;
                break;

            case TINY:
                viewHolderParams.height = (int) (heightUnit * height * 0.8);
                viewHolderParams.width = (int) (widthUnit * width * 0.8);
                textView.setVisibility(GONE);
                break;
        }

        textView.setTextColor(getContext().getColor(R.color.white));
        textView.setLayoutParams(textParams);

        imageHolder.setLayoutParams(imageHolderParams);

        imageView.setLayoutParams(imageParams);
    }

    public void updateImage(int imageResource) {
        this.imageView.setImageDrawable(ChatApplication.context.getDrawable(imageResource));
    }

    public void updateText(int title) {
        this.textView.setText(title);
    }

}
