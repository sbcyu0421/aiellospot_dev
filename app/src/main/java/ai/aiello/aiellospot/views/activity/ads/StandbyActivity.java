package ai.aiello.aiellospot.views.activity.ads;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.activity.BaseActivity;
import ai.aiello.aiellospot.core.media.MsttsManager;
import ai.aiello.aiellospot.views.componets.hints.WakeupHintManager;
import ai.aiello.aiellospot.modules.ads.AdsMediaType;
import ai.aiello.aiellospot.modules.ads.AdsObject;
import ai.aiello.aiellospot.modules.ads.AdsModuleManager;
import ai.aiello.aiellospot.core.info.DeviceInfo;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.core.soundai.SoundAIManager;
import miaoyongjun.pagetransformer.MagicTransformer;
import miaoyongjun.pagetransformer.TransitionEffect;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class StandbyActivity extends BaseActivity {


    private String TAG = StandbyActivity.class.getSimpleName();
    private RelativeLayout ads_container;
    private ViewPager viewPager;
    private ViewPagerScroller viewPagerScroller;
    private MPagerAdapter mPagerAdapter;
    private MGuidePageChangeListener changeListener;
    private TextView page_hint;
    private ArrayList<AdsObject> list;
    private ArrayList<ItemView> viewList;
    private AdsModuleManager sm;
    private LinearLayout tv_fade_out;

    private LinearLayout ads_mic_ly;
    private ImageView ads_mic_img;

    //    private BitmapDrawable bitmapDrawable = null;
    private static AdsHandler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            sm = AdsModuleManager.getInstance();
            list = sm.getPlayList();
            viewList = new ArrayList<>();
            initView();
            for (AdsObject io : list) {
                viewList.add(new ItemView(StandbyActivity.this, io));
            }
            mPagerAdapter = new MPagerAdapter(viewList);
            changeListener = new MGuidePageChangeListener();
            viewPagerScroller = new ViewPagerScroller(StandbyActivity.this);
            viewPagerScroller.setScrollDuration(1600);
            viewPagerScroller.initViewPagerScroll(viewPager);
            viewPager.setAdapter(mPagerAdapter);
            viewPager.setOnPageChangeListener(changeListener);
            viewPager.setPageTransformer(true, MagicTransformer.getPageTransformer(TransitionEffect.Fade));

            SpotUserTraceLog3.getInstance().buildSystemEventLog(
                    SpotUserTraceLog3.EventSubject.ADS,
                    SpotUserTraceLog3.EventAction.ON,
                    ""
            );

        } catch (Exception e) {
            Log.e(TAG, "catch crush ," + e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            System.exit(0);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        handler = new AdsHandler(this);
        //custom cursor
        try {
            cursor = AdsModuleManager.getInstance().getCursor();
        } catch (Exception e) {
            cursor = 0;
        }
        Log.d(TAG, "resume ad index = " + cursor);
        viewPager.setCurrentItem(cursor);
        focusOnItem(cursor);
        updateMic();
    }


    private void updateMic() {
        if (SoundAIManager.isWakeUpEnable()) {
            ads_mic_img.setImageResource(R.drawable.icon_home1_micon);
            page_hint.setVisibility(View.VISIBLE);
        } else {
            ads_mic_img.setImageResource(R.drawable.icon_home1_micoff);
            page_hint.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KEYCODE_AIELLO_MUTE:
                if (SoundAIManager.isWakeUpEnable()) {
                    SpotUserTraceLog3.getInstance().buildHardwareEventLog(
                            SpotUserTraceLog3.EventSubject.WAKEUP,
                            SpotUserTraceLog3.EventAction.OFF,
                            ""
                    );
                    SoundAIManager.setWakeUp(this, false);
                    updateMic();
                } else {
                    SpotUserTraceLog3.getInstance().buildHardwareEventLog(
                            SpotUserTraceLog3.EventSubject.WAKEUP,
                            SpotUserTraceLog3.EventAction.ON,
                            ""
                    );
                    SoundAIManager.setWakeUp(this, true);
                    updateMic();
                }
                break;
            case KEYCODE_AIELLO_VOLUME_UP:
            case KEYCODE_AIELLO_VOLUME_DOWN:
            case KEYCODE_AIELLO_BLUETOOTH:
                leave();
                break;
        }

        return true;
    }

    @Override
    protected void onPause() {
        handler.removeCallbacks(null);
        handler = null;
        stopTimer();
        super.onPause();
    }

    private static CountDownTimer timer;

    public void initView() {
        hideActivityController();
        setContentView(R.layout.activity_standby);
        viewPager = findViewById(R.id.viewPager);
        page_hint = findViewById(R.id.page_hint);
        tv_fade_out = findViewById(R.id.fade_out);
        ads_container = findViewById(R.id.ads_container);
        ads_mic_ly = findViewById(R.id.ads_mic_ly);
        ads_mic_img = findViewById(R.id.ads_mic_img);


        viewPager.setOnTouchListener(new View.OnTouchListener() {
            float oldY = 0, newY = 0;
            float moveY = 0;
            float deY = 1.0f;

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        oldY = motionEvent.getRawY();
                        stopTimer();
                        break;

                    case MotionEvent.ACTION_MOVE:
                        moveY = motionEvent.getRawY();
                        //fade out animation
                        if (oldY - moveY > 200) {
                            deY = 1.0f - (Math.abs(oldY - moveY) / 600.0f);
                            slideUp(-(oldY - moveY), deY);
//                            viewPager.setScrollX(cursor * 720);
                        } else {
                            slideUp(0, 1);
                        }
                        break;

                    case MotionEvent.ACTION_UP:
                        newY = motionEvent.getRawY();
                        Log.d(TAG, "Y_Offset = " + (oldY - newY));
                        if ((oldY - newY) > 150) {
                            leave();
                        } else {
                            slideUp(0, 1);
                            viewPager.setCurrentItem(cursor);
                            focusOnItem(cursor);
                        }
                        break;
                }
                return false;
            }
        });

    }

    private void slideUp(float offset, float alpha) {
//        ads_container.setY(offset);
//        viewList.get(cursor).getPodcast_area().setY(offset + 270);
        ads_container.setAlpha(alpha);
        viewList.get(cursor).getPodcast_area().setAlpha(alpha);
//        tv_fade_out.setY(offset);
        tv_fade_out.setAlpha(alpha);
    }

    private void leave() {
        //getTopActivityInfo(StandbyActivity.this);
        Log.d(TAG, "leave");
        ChatApplication.setCurrentAppState(ChatApplication.AppState.BUSY);
        MsttsManager.getInstance().stopTTS();
        Intent i = new Intent(StandbyActivity.this, ActivityLauncher.currentHomeName);
        startActivity(i);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }

    private void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    private void startTimer(int index) {
//        Log.d(TAG, "slide to next page after = " + list.get(index).getPeriod() + "s");
        stopTimer();
        timer = new CountDownTimer(list.get(index).getPeriod() * 1000, 1000) {
            @Override
            public void onTick(long remain) {
//                Log.d(TAG, "remain = " + remain);
            }

            @Override
            public void onFinish() {
                try {
                    handler.sendEmptyMessage(0);
                } catch (Exception e) {

                }
            }
        }.start();
    }


    public void startTitleAnimation(int position) {

        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setDuration(1300);
        fadeIn.setFillAfter(true);

        Animation moveLeft = new TranslateAnimation(600.0f, 0.0f, 0.0f, 0.0f);
        moveLeft.setDuration(1300);
        moveLeft.setFillAfter(true);

        AnimationSet animationSet = new AnimationSet(true);
        animationSet.addAnimation(fadeIn);
        animationSet.addAnimation(moveLeft);

        viewList.get(cursor).getTv_title().setAlpha(1.0f);
        viewList.get(cursor).getTv_desc().setAlpha(1.0f);

        viewList.get(position).getTv_title().startAnimation(animationSet);
        viewList.get(position).getTv_desc().startAnimation(animationSet);

        viewList.get(position).getPodcast_area().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AdsObject ao = list.get(cursor);
                podcastItem(ao, cursor);
                SpotUserTraceLog3.getInstance().buildUIEventLog(
                        SpotUserTraceLog3.EventSubject.ADS,
                        SpotUserTraceLog3.EventAction.SELECT,
                        ao.getTitle()
                );
            }
        });

        viewList.get(position).getPodcast_area().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        v.setAlpha(0.5f);
                        break;
                    case MotionEvent.ACTION_OUTSIDE:
                    case MotionEvent.ACTION_MOVE:
                        v.setAlpha(1.0f);
                        break;
                }
                return false;
            }

        });

    }

    public void startHintAnimation(int position) {
        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setDuration(1300);
        fadeIn.setFillAfter(true);
        page_hint.startAnimation(fadeIn);
    }

    public void startPlayVideo(int position) {
        try {
            if (viewList.get(position).getAdsObject().getType() == AdsMediaType.Video) {
                viewList.get(position).startPlaying();
            } else {
                if (position != viewList.size() - 1) {
                    viewList.get(position + 1).stopPlaying();
                }
                if (position != 0) {
                    viewList.get(position - 1).stopPlaying();
                }
            }
        } catch (Exception e) {
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e(TAG, e.toString());
        }

    }

    class MGuidePageChangeListener implements ViewPager.OnPageChangeListener {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//            Log.d(TAG, "onPageScrolled");
        }

        @Override
        public void onPageSelected(int position) {
            Log.d(TAG, "onPageSelected = " + position);
            focusOnItem(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {
//            if (cursor == 0) {
//                viewPager.setCurrentItem(viewList.size() - 2, false);
//            }
//            if (cursor == viewList.size() - 1) {
//                viewPager.setCurrentItem(1, false);
//            }
        }

    }

    private void focusOnItem(int position) {
        cursor = position;
        AdsModuleManager.getInstance().setCursor(cursor);

        startTitleAnimation(position);
//        startHintAnimation(position);
        startTimer(position);

        String hint = WakeupHintManager.getInstance().getHints(this);
        hint = String.format(hint, DeviceInfo.assistantName);
        page_hint.setVisibility(View.VISIBLE);
        page_hint.setText(hint);
        page_hint.setElevation(10.0f);

        if (cursor == viewList.size() - 1) {
            viewPagerScroller.setScrollDuration(0);
        } else {
            viewPagerScroller.setScrollDuration(600);
        }
    }

    class MPageTransformer implements ViewPager.PageTransformer {
        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();
            //Log.d(TAG, "position = " + position);
            if (position < -1) { // [-Infinity,-1)
                view.setAlpha(0);
            } else if (position <= 0) { // [-1,0]
                view.setAlpha(1);
                view.setTranslationX(0);
            } else if (position <= 1) { // (0,1]
                // Fade the page out.
                view.setAlpha(1 - position);
                // Counteract the default slide transition
                view.setTranslationX(pageWidth * -position);
                // Scale the page down (between MIN_SCALE and 1)
            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }
        }
    }


    private int cursor = 0;

    private static class AdsHandler extends Handler {

        private WeakReference<StandbyActivity> ref;

        AdsHandler(StandbyActivity activity) {
            this.ref = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
            if (msg.what == 0) {
                if (ref.get().cursor == ref.get().viewList.size() - 1) {
                    ref.get().cursor = 0;
                } else {
                    ref.get().cursor++;
                }
//                Log.e(TAG, "AutoScroll to " + list.get(currentItem).getTitle());
                ref.get().viewPager.setCurrentItem(ref.get().cursor);
            }
        }
    }

    ;

    public void hideActivityController() {
        //hideNavController
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }

    private void podcastItem(AdsObject tio, int index) {
        Intent intent = new Intent();
        intent.setClass(this, PodcastActivity.class);
        Log.d(TAG, "choose " + tio.getTitle());
        startActivity(intent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        finish();
    }

    @Override
    protected void onDestroy() {
        SpotUserTraceLog3.getInstance().buildSystemEventLog(
                SpotUserTraceLog3.EventSubject.ADS,
                SpotUserTraceLog3.EventAction.OFF,
                ""
        );
        super.onDestroy();
    }
}
