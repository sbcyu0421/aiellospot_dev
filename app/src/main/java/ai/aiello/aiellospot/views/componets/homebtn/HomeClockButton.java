package ai.aiello.aiellospot.views.componets.homebtn;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import net.imknown.bettertextclockbackportlibrary.TextClock;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.modules.alarm.UserAlarmModuleManager;

public class HomeClockButton extends ConstraintLayout {

    public int width;
    public int height;
    public View btn;
    private RelativeLayout textInfoViewsHolder;
    private RelativeLayout timeViewHolder;
    private TextView roomNumber;
    private TextView dayView;
    private TextView cityView;
    private TextClock clock;
    private LinearLayout mainViewHolder;
    private LinearLayout dayCityInfoHolder;
    private LayoutType layoutType;
    private ImageView img_ring;

    enum LayoutType {
        LARGE, NORMAL
    }

    public HomeClockButton(Context context, int width, int height) {
        super(context);

        this.width = width;
        this.height = height;

        View btn = LayoutInflater.from(context).inflate(R.layout.view_item_hero_clock, null);
        this.btn = btn;

        textInfoViewsHolder = this.btn.findViewById(R.id.ll_home_m1_clock_room_city);
        timeViewHolder = this.btn.findViewById(R.id.rl_home2_m1_time);
        roomNumber = this.btn.findViewById(R.id.tv_home_m1_room_number);
        dayView = this.btn.findViewById(R.id.tc_home_m1_day);
        cityView = this.btn.findViewById(R.id.tv_home_m1_city);
        clock = this.btn.findViewById(R.id.tv_home_m1_room_time);
        mainViewHolder = this.btn.findViewById(R.id.ll_home_m1_clock_child);
        dayCityInfoHolder = this.btn.findViewById(R.id.ll_home_city_set);
        img_ring = this.btn.findViewById(R.id.iv_home_m1_alarm);

        this.addView(btn);
        resetViewToMatchParent(btn);

        updateUiParameters();
    }

    private void resetViewToMatchParent(View btn) {
        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) btn.getLayoutParams();
        params.width = ConstraintLayout.LayoutParams.MATCH_PARENT;
        params.height = ConstraintLayout.LayoutParams.MATCH_PARENT;
        btn.setLayoutParams(params);
    }

    private void updateUiParameters() {

        int textColorInt = R.color.white;

        setLayoutType(this);
        setLayoutOrientation(layoutType);
        setWeightByLayoutType(layoutType);

        setCityText(cityView);
        setTextSize();
        setTextColor(textColorInt);

        RelativeLayout.LayoutParams clockParams = (RelativeLayout.LayoutParams) clock.getLayoutParams();
        clockParams.setMargins(0, -80 * height , 0 , 0);
        clock.setLayoutParams(clockParams);

        if (UserAlarmModuleManager.getInstance().getAlarmMap().size() > 0) {
            img_ring.setVisibility(View.VISIBLE);
        } else {
            img_ring.setVisibility(View.INVISIBLE);
        }
    }



    private void setLayoutOrientation(LayoutType type) {
        switch (type) {
            case LARGE:
                mainViewHolder.setOrientation(LinearLayout.HORIZONTAL);
                dayCityInfoHolder.setOrientation(LinearLayout.VERTICAL);
                break;
            case NORMAL:
                mainViewHolder.setOrientation(LinearLayout.VERTICAL);
                dayCityInfoHolder.setOrientation(LinearLayout.HORIZONTAL);
                break;
        }
    }

    private void setTextSize() {

        int roomTextSize = 30;
        int clockTextSize = 1;

        switch (layoutType) {
            case NORMAL:
                roomTextSize = 30;
                clockTextSize = 140;
                break;
            case LARGE:
                roomTextSize = 36;
                clockTextSize = 170;
                break;
        }

        roomNumber.setTextSize(roomTextSize);
        dayView.setTextSize(roomTextSize);
        cityView.setTextSize(roomTextSize);

        clock.setTextSize(clockTextSize);

    }

    private void setViewWeight(int roomViewWeight, int clockViewWeight) {
        LinearLayout.LayoutParams textInfoViewsHolderParams = (LinearLayout.LayoutParams) textInfoViewsHolder.getLayoutParams();
        LinearLayout.LayoutParams timeViewHolderParams = (LinearLayout.LayoutParams) timeViewHolder.getLayoutParams();

        textInfoViewsHolderParams.weight = roomViewWeight;
        textInfoViewsHolder.setLayoutParams(textInfoViewsHolderParams);

        timeViewHolderParams.weight = clockViewWeight;
        timeViewHolder.setLayoutParams(timeViewHolderParams);
    }

    private void setLayoutType(HomeClockButton btn) {
        if (((float)btn.width / (float)btn.height) > 2f) {
            btn.layoutType = LayoutType.LARGE;
        } else {
            btn.layoutType = LayoutType.NORMAL;
        }

    }

    private void setTextColor(int textColor){
        roomNumber.setText(DeviceInfo.roomName);
        roomNumber.setTextColor(ChatApplication.context.getColor(textColor));
        dayView.setTextColor(ChatApplication.context.getColor(textColor));
        cityView.setTextColor(ChatApplication.context.getColor(textColor));
        clock.setTextColor(ChatApplication.context.getColor(textColor));
    }

    private void setCityText(TextView cityView) {
        switch (ChatApplication.system_lang) {
            case japanese:
                if (!DeviceInfo.city_ja.isEmpty()) cityView.setText(String.format(",%s", DeviceInfo.city_ja));
                break;

            case zh_CN:
                if (!DeviceInfo.city_cn.isEmpty()) cityView.setText(String.format(",%s", DeviceInfo.city_cn));
                break;

            case zh_TW:
                if (!DeviceInfo.city_tw.isEmpty()) cityView.setText(String.format(",%s", DeviceInfo.city_tw));
                break;

            case en_US:
                if (!DeviceInfo.city_en.isEmpty()) cityView.setText(String.format(",%s", DeviceInfo.city_en));
                break;
        }
    }

    private void setWeightByLayoutType(LayoutType layoutType) {
        switch (layoutType) {
            case NORMAL:
                setViewWeight(5, 3);
                break;

            case LARGE:
                setViewWeight(2, 1);
                break;
        }
    }
}
