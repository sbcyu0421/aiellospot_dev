package ai.aiello.aiellospot.views.activity.fun;

import androidx.annotation.Nullable;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.library.Style;

import java.lang.ref.WeakReference;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.BaseActivity;
import ai.aiello.aiellospot.views.activity.dialog.AielloProgressDialog;
import ai.aiello.aiellospot.modules.surrounding.Items;
import ai.aiello.aiellospot.modules.surrounding.SubCategoryBean;
import ai.aiello.aiellospot.views.activity.ActivityLauncher;
import ai.aiello.aiellospot.views.componets.toast.MToaster;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.modules.surrounding.SurroundingModuleManager;

public class SurroundingActivity extends BaseActivity implements View.OnClickListener {

    private String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            type = SurroundingModuleManager.getInstance().getCurrentType();
            Log.d(TAG, "type = " + type);
            setContentView(R.layout.activity_surrounging);
            initView();
        } catch (Exception e) {
            MToaster.showButtonToast(this, getString(R.string.surrounding_format_error), Style.TYPE_STANDARD);
            ActivityLauncher.launchActivityByClass(this, ActivityLauncher.currentHomeName, R.anim.fade_in, R.anim.fade_out);
            SurroundingModuleManager.getInstance().create();
            e.printStackTrace();
        }
    }

    private void initView() {
        TextView surrounding_title = findViewById(R.id.surrounding_title);
        ImageView surrounding_bg = findViewById(R.id.surrounding_bg);
        switch (type) {
            case SurroundingModuleManager.CUISINE:
                surrounding_title.setText(R.string.ui_cuisine_service);
                surrounding_bg.setImageResource(R.drawable.bg_cuisine);
                break;

            case SurroundingModuleManager.SIGHTS:
                surrounding_title.setText(R.string.ui_sights_service);
                surrounding_bg.setImageResource(R.drawable.bg_sights_dim);
                break;

            case SurroundingModuleManager.TRANSPORT:
                surrounding_title.setText(R.string.ui_transport_service);
                surrounding_bg.setImageResource(R.drawable.bg_transport_dim);
                break;

            case SurroundingModuleManager.BUILDING:
                surrounding_title.setText(R.string.ui_building_service);
                surrounding_bg.setImageResource(R.drawable.bg_building_dim);
                break;

        }

        LinearLayout ly_back = findViewById(R.id.ly_back);

        ly_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exit();
            }
        });


        GridLayout gl_cuisine = findViewById(R.id.gl_cuisine);
        HashMap<String, SubCategoryBean.SubTypes> itemsMap = null;
        switch (type) {
            case SurroundingModuleManager.CUISINE:
                itemsMap = SurroundingModuleManager.getInstance().getCuisineBean().getTitleSet();
                break;

            case SurroundingModuleManager.SIGHTS:
                itemsMap = SurroundingModuleManager.getInstance().getSightsBean().getTitleSet();
                break;
            case SurroundingModuleManager.TRANSPORT:
                itemsMap = SurroundingModuleManager.getInstance().getTransportBean().getTitleSet();
                break;

            case SurroundingModuleManager.BUILDING:
                itemsMap = SurroundingModuleManager.getInstance().getLifeBean().getTitleSet();
                break;
        }

        int i = 0;
        for (Map.Entry<String, SubCategoryBean.SubTypes> entry : itemsMap.entrySet()) {
            if (i == 9) {
                break;
            }
            SurroundingBtnView surroundingBtnView = new SurroundingBtnView(
                    this,
                    entry.getValue().getCapacity(),
                    entry.getValue().getName(ChatApplication.locale),
                    entry.getValue().getQuery(ChatApplication.locale));

            int rowNum = i % 3;
            int colNum = i / 3;
            GridLayout.LayoutParams glp = new GridLayout.LayoutParams
                    (GridLayout.spec(rowNum, 1.0f), GridLayout.spec(colNum, 1.0f));
            glp.width = 0;
            glp.height = 0;
            surroundingBtnView.setLayoutParams(glp);

            gl_cuisine.addView(surroundingBtnView);
            surroundingBtnView.setOnClickListener(this);
            i++;
        }

    }

    private void exit() {
        Intent intent2 = new Intent(SurroundingActivity.this, ActivityLauncher.currentHomeName);
        startActivity(intent2);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }

    @Override
    public void onClick(View v) {
        new GetDataTask(SurroundingActivity.this, ((SurroundingBtnView) v).getQuery(), type).execute();
        SpotUserTraceLog3.getInstance().buildUIEventLog(
                SpotUserTraceLog3.EventSubject.SURROUNDING,
                SpotUserTraceLog3.EventAction.QUERY,
                 ((SurroundingBtnView) v).name
        );
    }

    private void showLoading() {
        if (aielloProgressDialog == null) {
            aielloProgressDialog = new AielloProgressDialog(new WeakReference<Activity>(this), R.style.CustomProgressDialog);
            aielloProgressDialog.setText(getResources().getString(R.string.please_wait));
        }
        aielloProgressDialog.show();
    }

    private void hideLoading() {
        if (aielloProgressDialog != null && aielloProgressDialog.isShowing()) {
            aielloProgressDialog.dismiss();
            aielloProgressDialog = null;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        hideLoading();
    }

    private AielloProgressDialog aielloProgressDialog = null;

    private static class GetDataTask extends AsyncTask<Void, Void, Integer> {
        WeakReference<SurroundingActivity> activityReference;
        String title;
        String type;
        Items items;

        GetDataTask(SurroundingActivity activity, String title, String type) {
            this.activityReference = new WeakReference<>(activity);
            this.title = title;
            this.type = type;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            activityReference.get().showLoading();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            try {
                items = SurroundingModuleManager.getInstance().getSurroundingItems(title, title, ChatApplication.locale.toString(), 0);
                return 2;
            } catch (Exception e) {
                Log.d("GetDataTask", e.toString());
                if (e instanceof SocketTimeoutException) {
                    return 0;
                } else {
                    return 1;
                }
            }
        }

        @Override
        protected void onPostExecute(Integer code) {
            super.onPostExecute(code);
            activityReference.get().hideLoading();
            try {
                switch (code) {
                    case 0:
                        MToaster.showButtonToast(activityReference.get(), activityReference.get().getString(R.string.surrounding_timeout_error), Style.TYPE_STANDARD);
                        break;
                    case 1:
                        MToaster.showButtonToast(activityReference.get(), activityReference.get().getString(R.string.loading_error), Style.TYPE_STANDARD);
                        break;

                    case 2:
                        if (items.getItems().size() > 1) {
                            Intent intent = new Intent(activityReference.get(), SurroundingListActivity.class);
                            SurroundingModuleManager.getInstance().setCurrentItems(items);
                            SurroundingModuleManager.getInstance().setCurrentBeanIndex(0);
                            activityReference.get().startActivity(intent);
                            activityReference.get().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            activityReference.get().finish();
                        } else {
                            Intent intent = new Intent(activityReference.get(), SurroundingDetailActivity.class);
                            SurroundingModuleManager.getInstance().setCurrentItems(items);
                            SurroundingModuleManager.getInstance().setCurrentBeanIndex(0);
                            activityReference.get().startActivity(intent);
                            activityReference.get().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            activityReference.get().finish();
                        }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
                MToaster.showButtonToast(activityReference.get(), activityReference.get().getString(R.string.loading_error), Style.TYPE_STANDARD);
            }
        }

    }


    @SuppressLint("AppCompatCustomView")
    static class SurroundingBtnView extends TextView {
        String title;
        String name;
        String query;
        int capacity;

        public SurroundingBtnView(Context context, int capacity, String name, String query) {
            super(context);
            setBackgroundResource(R.drawable.bg_cuision_item);
            this.title = String.format("%s", name);
            this.name = name;
            this.query = query;
            this.capacity = capacity;
            this.setGravity(Gravity.CENTER);
            this.setTextSize(32);
            this.setTextColor(Color.WHITE);
            this.setText(title);
        }

        public String getQuery() {
            return query;
        }

        public SurroundingBtnView(Context context) {
            super(context);
        }

        public SurroundingBtnView(Context context, @Nullable AttributeSet attrs) {
            super(context, attrs);
        }

        public SurroundingBtnView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
        }

        public SurroundingBtnView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
            super(context, attrs, defStyleAttr, defStyleRes);
        }

        public int getCapacity() {
            return capacity;
        }
    }


}