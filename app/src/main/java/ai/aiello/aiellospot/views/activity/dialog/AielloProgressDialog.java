package ai.aiello.aiellospot.views.activity.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import java.lang.ref.WeakReference;

import ai.aiello.aiellospot.R;

/**
 * Created by a1990 on 2019/4/9.
 */


public class AielloProgressDialog extends Dialog {

    private TextView tvMsg;

    public AielloProgressDialog(WeakReference<Activity> weakReference, int theme) {
        super(weakReference.get(), theme);
        setOption();

    }

    public void setOption() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.custom_dialog);
        tvMsg = this.findViewById(R.id.id_tv_custom_loading_msg);
        setCancelable(false);
    }


    public void setText(String msg) {
        if (tvMsg != null) {
            tvMsg.setText(msg);
        }
    }

    @Override
    public void show() {

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN;
        super.show();
        this.getWindow().getDecorView().setSystemUiVisibility(uiOptions);
        this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
    }


}

