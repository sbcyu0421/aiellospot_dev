package ai.aiello.aiellospot.views.activity.sdui;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;

import org.json.JSONObject;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.CardActivity;
import ai.aiello.aiellospot.intents.sdui.intents.MarketingIntent;
import ai.aiello.aiellospot.intents.sdui.layout.ClassicCardLayout;

public class ClassicCardActivity extends CardActivity {

    private LinearLayout btn_ok;
    private ScrollView scrollView_msg;

    private LinearLayout cls_bg;
    private ImageView cls_hero_img;
    private TextView cls_hero_title;
    private TextView cls_content;
    private JSONObject jsonObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classic_card);
        cls_hero_title = findViewById(R.id.cls_hero_title);
        cls_content = findViewById(R.id.cls_content);
        cls_hero_img = findViewById(R.id.cls_hero_img);
        scrollView_msg = findViewById(R.id.scrollView_msg);
        btn_ok = findViewById(R.id.btn_ok);


        String viewData = ((MarketingIntent)intentObject).getLayoutData();
        ClassicCardLayout classicViewData = new Gson().fromJson(viewData, ClassicCardLayout.class);

        //hero
        cls_hero_title.setText(classicViewData.getLayout().getHero().getText());
        Glide.with(this)
                .load(classicViewData.getLayout().getHero().getImage())
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(cls_hero_img);

        //content
        cls_content.setText(classicViewData.getLayout().getContent().getText());
        cls_content.setMovementMethod(new ScrollingMovementMethod());

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmAndGoBack();
            }
        });

    }


}
