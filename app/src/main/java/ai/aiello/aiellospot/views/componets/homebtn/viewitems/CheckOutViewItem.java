package ai.aiello.aiellospot.views.componets.homebtn.viewitems;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;

import java.lang.ref.WeakReference;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.dialog.AielloAlertDialog;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractHomeButton;
import ai.aiello.aiellospot.views.componets.homebtn.AbstractViewItem;
import ai.aiello.aiellospot.modules.checkio.CheckInOutModuleManager;
import ai.aiello.aiellospot.views.componets.homebtn.HomeBasicButton;

public class CheckOutViewItem extends AbstractViewItem {

    private static String TAG = CheckOutViewItem.class.getSimpleName();

    UIHandler uiHandler;
    Thread checkoutThread = null;

    AielloAlertDialog checkoutDialog = null;
    int checkOutTry = 0;

    private static CheckOutViewItem instance;

    public static CheckOutViewItem getInstance() {
        if (instance == null) {
            instance = new CheckOutViewItem();
        }
        return instance;
    }

    private CheckOutViewItem() {}

    @Override
    public void updateView() {

    }

    @Override
    public void handelBtnClickLogic() {
        wefBtn.get().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCheckOutClick();
            }
        });
    }

    private void handleOnCancel() {
        destroyCheckoutProcess();
        destroyCheckOutDialog();
    }

    private void handleOnConfirm() {
        checkOutTry = 0;
        checkoutDialog.setTextContent(wefActivity.get().getString(R.string.please_wait));
        checkoutDialog.setOnConfirmListener(null);
        checkoutThread = new Thread(() -> {
            try {
                boolean success = CheckInOutModuleManager.getInstance().checkOut();
                if (success) {
                    wefActivity.get().runOnUiThread(() -> checkoutDialog.setTextContent("退房成功，系統將自動重新啟動"));
                } else {
                    wefActivity.get().runOnUiThread(() -> {
                        checkoutDialog.setTextContent("服務發生暫時性問題。請再試一次。若持續收到此訊息，請稍後再試");
                        checkoutDialog.setOnConfirmListener(view -> handleOnConfirm());
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        checkoutThread.start();
    }

    private void onCheckOutClick() {
        if (checkoutDialog != null) return;
        checkoutDialog = new AielloAlertDialog(wefActivity.get(), R.style.Theme_AppCompat_Dialog_Alert);
        checkoutDialog.setTitle(wefActivity.get().getString(R.string.dialog_checkout_title), Color.RED)
                .setTextContent(wefActivity.get().getString(R.string.dialog_checkout_content))
                .setOnCancelListener((View.OnClickListener) v -> {
                   handleOnCancel();
                })
                .setOnConfirmListener(v -> {
                    handleOnConfirm();
                });
        checkoutDialog.show();
    }

    private void destroyCheckOutDialog(){
        try {
            if (checkoutDialog != null) {
                checkoutDialog.dismiss();
                checkoutDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Log.d(TAG, "checkoutDialog destroyed");
        }
    }

    private void destroyCheckoutProcess(){
        try {
            if (checkoutThread != null && !checkoutThread.isInterrupted()) {
                checkoutThread.interrupt();
                checkoutThread = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Log.d(TAG, "destroyCheckoutProcess destroyed");
        }
    }

    @Override
    public void unbindView(Activity activity) {
        super.unbindView(activity);
        if (uiHandler != null) {
            uiHandler.removeCallbacksAndMessages(null);
            uiHandler = null;
        }
        checkoutDialog = null;
        checkoutThread = null;
    }

    @Override
    public View buildButton(Context context) {
        HomeBasicButton homeBasicButton = new HomeBasicButton(context, this.width, this.height);
        homeBasicButton.updateImage(R.drawable.ic_checkout);
        homeBasicButton.updateText(R.string.ask_check_out);
        return homeBasicButton;
    }

    static class UIHandler extends Handler {

        Activity activity;

        UIHandler(WeakReference<Activity> reference) {
            activity = reference.get();
        }

        @Override
        public void handleMessage(Message msg) {
        }
    }
}
