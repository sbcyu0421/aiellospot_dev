package ai.aiello.aiellospot.views.activity.home;

import ai.aiello.aiellospot.views.activity.ActivityLauncher;

public class MainHomeActivity extends AbstractHomeActivity {

    public String TAG = MainHomeActivity.class.getSimpleName();

    @Override
    int getCurrentPage() {
        return 1;
    }

    @Override
    protected void onResume() {
        super.onResume();
        ActivityLauncher.currentHomeName = MainHomeActivity.class;
    }

}

