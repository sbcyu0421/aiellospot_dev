package ai.aiello.aiellospot.views.activity.clock;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.litesuits.android.log.Log;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.events.system.WakeUpEvent;
import ai.aiello.aiellospot.views.activity.CardActivity;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.modules.alarm.UserAlarmModuleManager;

public class AlarmRiseActivity extends CardActivity {


    private static final String TAG = AlarmRiseActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarmrise);
        TextView tv_alarm_msg = findViewById(R.id.tv_alarm_msg);
        LinearLayout btn_ok = findViewById(R.id.btn_ok);
        tv_alarm_msg.setText(UserAlarmModuleManager.alarm_msg);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SpotUserTraceLog3.getInstance().buildUIEventLog(
                        SpotUserTraceLog3.EventSubject.CLOCK,
                        SpotUserTraceLog3.EventAction.CANCEL,
                        ""
                );
                UserAlarmModuleManager.getInstance().stopRing("user stop alarm by ui");
                confirmAndGoBack();
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (UserAlarmModuleManager.getInstance().isAlarmRing()) {
            UserAlarmModuleManager.getInstance().showAlarmPopup(UserAlarmModuleManager.alarm_msg);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.d(TAG, "keyCode = " + keyCode);
        SpotUserTraceLog3.getInstance().buildHardwareEventLog(
                SpotUserTraceLog3.EventSubject.CLOCK,
                SpotUserTraceLog3.EventAction.CANCEL,
                ""
        );

        switch (keyCode) {
            case KEYCODE_AIELLO_VOLUME_UP:
                UserAlarmModuleManager.getInstance().stopRing("user stop alarm by ui");
                confirmAndGoBack();
                break;

            case KEYCODE_AIELLO_VOLUME_DOWN:
                UserAlarmModuleManager.getInstance().stopRing("user stop alarm by ui");
                confirmAndGoBack();
                break;

            case KEYCODE_AIELLO_BLUETOOTH:
                UserAlarmModuleManager.getInstance().stopRing("user stop alarm by ui");
                confirmAndGoBack();
                break;

            case KEYCODE_AIELLO_MUTE:
                UserAlarmModuleManager.getInstance().stopRing("user stop alarm by ui");
                confirmAndGoBack();
                break;
        }

        return true;
    }

    @Override
    public void onMessageEvent(WakeUpEvent event) {
        super.onMessageEvent(event);
        UserAlarmModuleManager.getInstance().showAlarmPopup(UserAlarmModuleManager.alarm_msg);
        confirmAndGoBack();
    }

}
