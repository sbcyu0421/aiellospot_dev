package ai.aiello.aiellospot.views.componets.homebtn;

import android.content.Context;
import android.view.View;

public interface IViewLogic {

    void handelBtnClickLogic();

    void updateView();

    View buildButton(Context context);
}
