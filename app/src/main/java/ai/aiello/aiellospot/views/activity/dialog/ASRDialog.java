package ai.aiello.aiellospot.views.activity.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Message;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.airbnb.lottie.LottieAnimationView;
import com.litesuits.android.log.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.events.system.TTSEvent;
import ai.aiello.aiellospot.events.module.AlarmStateChange;
import ai.aiello.aiellospot.intents.Classifier;
import ai.aiello.aiellospot.intents.IntentManager;
import ai.aiello.aiellospot.intents.IntentObject;
import ai.aiello.aiellospot.intents.items.UnHandleIntent;
import ai.aiello.aiellospot.intents.sdui.ActionManager;
import ai.aiello.aiellospot.core.asr.ASRManager;
import ai.aiello.aiellospot.core.media.AielloSoundPool;
import ai.aiello.aiellospot.core.MCountdownTimer;
import ai.aiello.aiellospot.core.media.MsttsManager;
import ai.aiello.aiellospot.core.chatbot.ChatBot;
import ai.aiello.aiellospot.core.config.SystemConfig;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.messager.MessageManager;
import ai.aiello.aiellospot.core.soundai.SoundAIManager;
import ai.aiello.aiellospot.modules.alarm.UserAlarmModuleManager;
import ai.aiello.chinese.ChineseUtils;

/**
 * Created by a1990 on 2019/4/9.
 */


public class ASRDialog extends Dialog implements ASRManager.SocketManagerListener {

    private LinearLayout ly_listen_card;
    private Animation amTranslate;
    private ASRManager asrManager;
    private LinearLayout ly_cancel;
    private TextView tv_asr;
    private LottieAnimationView complete_animation;
    private static ExecutorService cachedThreadExecutor;
    private static final String TAG = ASRDialog.class.getSimpleName();
    private static boolean isAdvanced;
    private ASRDialogListener listener;
    private boolean isCanceled;

    public interface ASRDialogListener {
        void onFinish(boolean hiddenControl);

        void onCancel(String msg);
    }


    public ASRDialog(Context context, int theme) {
        super(context, theme);
        if (context instanceof Activity) {
            setOwnerActivity((Activity) context);
        }
    }

    private void initView() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_chat);
        ly_listen_card = findViewById(R.id.ly_listen_card);
        tv_asr = findViewById(R.id.tv_asr);
        ly_cancel = findViewById(R.id.ly_cancel);
        complete_animation = findViewById(R.id.animation_view);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        ly_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isCanceled = true;
                listener.onCancel("user cancel");
            }
        });
    }

    private void runOnUIThread(Runnable runnable) {
        try {
            getOwnerActivity().runOnUiThread(runnable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startASREngine() {
        runOnUIThread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "startASREngine, advanced = " + isAdvanced);
                if (cachedThreadExecutor == null || cachedThreadExecutor.isShutdown()) {
                    cachedThreadExecutor = Executors.newFixedThreadPool(10);
                }
                IntentManager.clearTask();
                IntentManager.finishIntentList.clear();
                asrManager = ASRManager.getInstance();
                asrManager.startEmit(isAdvanced);
                asrManager.setOnEmitListener(ASRDialog.this); //to update ui

//                if (asrManager.getASRModel().equals("Xfyun")) {
//                    playListenAnimation(); // this model is oneshot
//                }
                if (isAdvanced) {
                    tv_asr.setText("");
                    releaseAnimation();
                    SoundAIManager.startBeam();
                }
            }
        });
    }

    private void setupLayout() {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN;
        super.show();
        this.getWindow().getDecorView().setSystemUiVisibility(uiOptions);
        this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        this.getWindow().setGravity(Gravity.CENTER);
        this.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT); //Controlling width and height.
    }

    public void show(ASRDialogListener listener, ChatApplication app) {
        setupLayout();
        this.listener = listener;
        initView();
        playCardInAnimate();
        show();
        handleEvent(EVENT_ASR_START, null);
    }


    @Override
    public void onAsrResponse(Message msg) {
        handleEvent(EVENT_ASR_RESPONSE, msg);
    }

    @Override
    public void onCancel() {
        listener.onCancel("asr cancel");
    }

    private static final int EVENT_ASR_START = 1;
    private static final int EVENT_ASR_RESPONSE = 2;
    private static final int EVENT_ASR_RESULT = 3;
    private static final int EVENT_CHATBOT_RESULT = 4;
    private static final int EVENT_PERFECT = 7;
    private static final int EVENT_IMPERFECT = 8;
    private static final int EVENT_COMPLETED = 9;


    private CountDownLatch latch;

    private void executeInitial() {
        MessageManager.getInstance().pause();
        ASRManager.getInstance().setOnASR(true);
        isCanceled = false;
        isAdvanced = false;
        advanceObject = null;
        SoundAIManager.spotOnASR(true);
        ActionManager.getInstance().stopForegroundAction();
        MsttsManager.getInstance().stopTTS();
        startASREngine();
        MCountdownTimer.stopTimer();
        IntentManager.clearTask();
    }

    private void executeAsrResult(Message msg) {
        ArrayList recog = (ArrayList) msg.obj;
        Log.d(TAG, recog.toString());
        String status = (String) recog.get(0);
        String asr_word = (String) recog.get(1);
        if (ChatApplication.system_lang.equals(ChatApplication.Language.zh_CN)) {
            asr_word = ChineseUtils.toSimplified(asr_word, false);
        }

        String finalAsr_word = asr_word;
        runOnUIThread(new Runnable() {
            @Override
            public void run() {
                tv_asr.setText(finalAsr_word);
            }
        });

        if (status.equals("final")) {
            AielloSoundPool.play(AielloSoundPool.vad_end);
            runOnUIThread(new Runnable() {
                @Override
                public void run() {
                    tv_asr.setText(replacePhrases(finalAsr_word));
                    playCompleteAnimation();
                }
            });
            //use ui thread
            String mis = (String) recog.get(2);
            mis = replacePhrases(mis);
            String[] mi = mis.split(",");
            Log.d(TAG, "mis=" + mis);
            stopASREngine("asr completed");

            Message eventMessage = Message.obtain();
            eventMessage.obj = mi;
            handleEvent(EVENT_ASR_RESULT, eventMessage);
        }
    }

    private IntentObject advanceObject = null;

    private void executeChatbot(Message msg) {

        String[] mi = (String[]) msg.obj;
        String session_id = "";
        int specialIntent = -1;

        if (isAdvanced) {
            session_id = advanceObject.getSessionId();
            specialIntent = advanceObject.getSpecialIntent();
        }

        IntentManager.chatbotResult.clear();
        latch = new CountDownLatch(mi.length);
        for (int s = 0; s < mi.length; s++) {
            mi[s] = mi[s].replaceAll("[.^:,\"\"]", "");
            mi[s] = mi[s].replace("[", "");
            mi[s] = mi[s].replace("]", "");
            Log.d(TAG, "mi[" + s + "]=" + mi[s]);


            cachedThreadExecutor.execute(new ChatBot.ChatBotRunnable(mi[s], session_id, s, specialIntent, latch));
        }

        //wait for chatbot result
        try {
            latch.await();
        } catch (InterruptedException e) {
            Log.e(TAG, e.toString());
        }
        handleEvent(EVENT_CHATBOT_RESULT, null);

    }

    private void executeBuildIntent() {
        UnHandleIntent.count = 0;
        Log.d(TAG, "IntentManager.chatbotResult.size() = " + IntentManager.chatbotResult.size());
        latch = new CountDownLatch(IntentManager.chatbotResult.size());
        for (JSONObject jObj : IntentManager.chatbotResult) {
            cachedThreadExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        IntentManager.IntentStandardFormat standardIO = null;
                        standardIO = ChatBot.parse(jObj);
                        IntentObject io = Classifier.classify(standardIO);
                        IntentManager.addTask(io);
                    } catch (Exception e) {
                        android.util.Log.e(TAG, "future stop : " + e.toString());
                    } finally {
                        latch.countDown();
                    }
                }
            });
        }

        //wait for perfect check
        try {
            latch.await();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        //drop duplicate intent`
        IntentManager.dropDuplicateInstance();

        //drop surroundingIntentInMulti
        IntentManager.dropForbiddenIntentInMulti();

        //checkExcludeIntent
        IntentManager.checkExcludeIntent();

        //check if contains advanced session
        IntentObject advancedIntent = IntentManager.getAdvancedIntent();

        //if get advanceIntent, then restart the session
        if (advancedIntent != null) {
            isAdvanced = true;
            IntentManager.removeTask(advancedIntent);
            MsttsManager.getInstance().speakWord(advancedIntent.getChat_response(), true, getContext());
            runOnUIThread(new Runnable() {
                @Override
                public void run() {
                    tv_asr.setText(advancedIntent.getChat_response());
                }
            });
            advanceObject = advancedIntent;
//            handleEvent(EVENT_IMPERFECT, null);
        } else {
            handleEvent(EVENT_PERFECT, null);
        }

    }


    private void executeIntentDetail() {
        latch = new CountDownLatch(IntentManager.taskQueue.size());
        for (IntentObject io : IntentManager.taskQueue) {
            ASRDialog.cachedThreadExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    io.execute(getContext(), latch);
                }
            });
        }
        try {
            latch.await();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
        handleEvent(EVENT_COMPLETED, null);
    }

    private void executeCompleted() {
        Log.d(TAG, "execute completed");

        //rule1: 全無意義intent且沒有出現過chat->unknown * 1
        //rule2: 全無意義intent但有出現過chat->chat * 1
        //rule3: 具有意義intent時，chat * 0 ; unknown * 0

        //移除不理解intent
        IntentManager.removeUnHandleIntent();
        //只留下一個roobo intent
        IntentObject roobo = IntentManager.removeRooboIntent();

        IntentManager.finishIntentList.clear();
        boolean hiddenControl = false;
        if (IntentManager.taskQueue.size() == 0) {
            if (roobo == null) {
                Log.i(TAG, "rule1");
                IntentManager.finishIntentList.add(new UnHandleIntent("Error", "", "", true, "", "").modify(getContext()));
            } else {
                Log.i(TAG, "rule2");
                IntentManager.finishIntentList.add(roobo);
            }
            //tts result
            speakTtsData(IntentManager.finishIntentList);
        } else {
            Log.i(TAG, "rule3");
            //add all
            IntentManager.finishIntentList.addAll(IntentManager.taskQueue);
            //tts result
            speakTtsData(IntentManager.finishIntentList);
            //hide control bar in multi
            hiddenControl = IntentManager.hideControlBarIntent();
        }
        listener.onFinish(hiddenControl);
    }

    private void speakTtsData(ArrayList<IntentObject> list) {
        StringBuilder sb = new StringBuilder();
        for (IntentObject io : list) {
            sb.append(io.get_intent_result());
            if (list.indexOf(io) != list.size() - 1) {
                sb.append(",");
            }
        }
        MsttsManager.getInstance().speakWord(sb.toString(), true, getContext());
    }


    //simple state machine
    private void handleEvent(int event, @Nullable Message msg) {
        Log.d(TAG, "handleEvent = " + event);
        if (isCanceled)
            return;
        switch (event) {
            case EVENT_ASR_START:
                executeInitial();
                break;
            case EVENT_ASR_RESPONSE:
                executeAsrResult(msg);
                break;
            case EVENT_ASR_RESULT:
                executeChatbot(msg);
                break;
            case EVENT_CHATBOT_RESULT:
                executeBuildIntent();
                break;
            case EVENT_IMPERFECT:
                startASREngine();
                break;
            case EVENT_PERFECT:
                executeIntentDetail();
                break;
            case EVENT_COMPLETED:
                executeCompleted();
                break;
        }
    }

    private void stopASREngine(String from) {
        Log.d(TAG, "stopASREngine = " + from);
        asrManager.setOnEmitListener(null);
        asrManager.stopEmit();
        SoundAIManager.stopBeam();
    }


    private void stopThreadPool() {
        try {
            cachedThreadExecutor.awaitTermination(50, TimeUnit.MILLISECONDS);
            cachedThreadExecutor = null;
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void playCardInAnimate() {
        amTranslate = new TranslateAnimation(1500.0f, 0.0f, 0.0f, 0.0f);
        amTranslate.setDuration(375);
        amTranslate.setRepeatCount(0);
        ly_listen_card.startAnimation(amTranslate);
    }

    private void playCardOutAnimation(Animation.AnimationListener listener) {
        amTranslate = new TranslateAnimation(0.0f, 1500.0f, 0.0f, 0.0f);
        amTranslate.setDuration(375);
        amTranslate.setRepeatCount(0);
        amTranslate.setFillAfter(true);
        amTranslate.setAnimationListener(listener);
        ly_listen_card.startAnimation(amTranslate);
    }


    private void playListenAnimation() {
        String m_animationName = "aiello_listen.json";
        try {
            if (complete_animation.isAnimating()) {
                releaseAnimation();
            }
            animationName = m_animationName;
            complete_animation.setAnimation(animationName);
            complete_animation.buildDrawingCache(true);
            complete_animation.setVisibility(View.VISIBLE);
            complete_animation.playAnimation();
        } catch (Exception e) {
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e(TAG, e.toString());
        }
    }


    private void playCompleteAnimation() {
        String m_animationName = "aiello_listen4.json";
        try {
            if (complete_animation.isAnimating()) {
                releaseAnimation();
            }
            animationName = m_animationName;
            complete_animation.setAnimation(animationName);
            complete_animation.buildDrawingCache(true);
            complete_animation.setVisibility(View.VISIBLE);
            complete_animation.playAnimation();
        } catch (Exception e) {
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e(TAG, e.toString());
        }
    }


    private String replacePhrases(String sentence) {
        HashMap<String, String> tmpMap = new HashMap<>();
        switch (ChatApplication.system_lang) {
            case zh_TW:
                tmpMap = SystemConfig.AsrBean.replace_phrases_tw;
                break;
            case zh_CN:
                tmpMap = SystemConfig.AsrBean.replace_phrases_cn;
                break;
            case en_US:
                tmpMap = SystemConfig.AsrBean.replace_phrases_en;
                break;
            case japanese:
                tmpMap = SystemConfig.AsrBean.replace_phrases_ja;
                break;
        }
        try {
            for (Map.Entry<String, String> entry : tmpMap.entrySet()) {
                Log.d(TAG, String.format("replace %s -> %s", entry.getKey(), entry.getValue()));
                sentence = sentence.replace(entry.getKey(), entry.getValue());
            }
        } catch (Exception e) {
            Log.e(TAG, "replacePhrases error = " + e.toString());
        }
        return sentence;
    }


    private String animationName = "";

    private void releaseAnimation() {
        try {
            complete_animation.setVisibility(View.INVISIBLE);
            complete_animation.cancelAnimation();
        } catch (Exception e) {
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e(TAG, e.toString());
        }
    }


    public void dismiss(boolean skipAnimation) {
        ChatBot.cleanForm();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        try {
            stopThreadPool();
            stopASREngine("asrDialog release");
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
        if (skipAnimation) {
            release();
        } else {
            playCardOutAnimation(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    Log.d(TAG, "onASRAnimationEnd");
                    release();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }
    }

    private void release() {
        releaseAnimation();
        super.dismiss();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(TTSEvent event) {
        handleEvent(EVENT_IMPERFECT, null);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(AlarmStateChange state) {
        switch (state.getState()) {
            case ALARM_RISE:
            case ALARM_TIMEOUT:
                UserAlarmModuleManager.getInstance().showAlarmPopup(UserAlarmModuleManager.alarm_msg);
                break;
        }
    }

}

