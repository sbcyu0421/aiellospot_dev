package ai.aiello.aiellospot.views.componets.homebtn;

import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ai.aiello.aiellospot.R;

public class HomeLeftStateButton extends HomeBaseButton {

    static String TAG = HomeLeftStateButton.class.getSimpleName();
    private TextView tv_left_status;

    public HomeLeftStateButton(Context context, int width, int height) {
        super(context, width, height);
    }

    @Override
    int getLayoutXml() {
        return R.layout.button_home_left_state;
    }

    @Override
    public void initViews(Context context, int width, int height) {
        super.initViews(context, width, height);
        this.tv_left_status = btn.findViewById(R.id.tv_left_status);
    }

    public TextView getTv_left_status() {
        return tv_left_status;
    }

}
