package ai.aiello.aiellospot.views.activity.repair;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.CardActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

public class RepairActivity extends CardActivity {

    private LinearLayout btn_ok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_repair);
        btn_ok = findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmAndGoBack();
            }

        });

    }

}
