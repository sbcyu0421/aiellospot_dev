package ai.aiello.aiellospot.views.activity.new_ota;

public interface OtaProgressListener {

    void onCheckResult(String apk, boolean isLatest);

    void onDownloadingProgress(String apk, int progress);

    void onDownloadingFail(String apk);

    void onInstalling(String apk);

    void onInstalled(String apk);

    void onOtaStart();

    void onOtaProgress(int progress);

    void onOtaCompleted();

}
