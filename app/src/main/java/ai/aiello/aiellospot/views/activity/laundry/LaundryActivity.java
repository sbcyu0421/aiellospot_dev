package ai.aiello.aiellospot.views.activity.laundry;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.CardActivity;

public class LaundryActivity extends CardActivity {


    private TextView tv_msg;
    private LinearLayout btn_ok,btn_price_table;
    private String laundary_msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activtiy_laundry);
        tv_msg = findViewById(R.id.tv_roobo_msg);
        btn_ok = findViewById(R.id.btn_ok);
        btn_price_table = findViewById(R.id.btn_price_table);

        laundary_msg = intentObject.getMsg1();

        int ml = laundary_msg.length();

        if (ml <= 17) {
            tv_msg.setHeight(180);
        } else if (ml <= 34) {
            tv_msg.setHeight(220);
        } else {
            tv_msg.setHeight(260);
        }

        tv_msg.setText(laundary_msg);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmAndGoBack();
            }
        });

        btn_price_table.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(LaundryActivity.this, LaundryPriceActivity.class);
                startActivity(intent2);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });

    }


}
