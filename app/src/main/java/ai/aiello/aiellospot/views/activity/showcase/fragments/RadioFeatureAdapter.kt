package ai.aiello.aiellospot.views.activity.showcase.fragments

import ai.aiello.aiellospot.ChatApplication
import ai.aiello.aiellospot.R
import ai.aiello.aiellospot.databinding.ItemHotelRadioBtnBinding
import ai.aiello.aiellospot.views.activity.showcase.beans.RadioTrackData
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import android.view.animation.*
import android.widget.LinearLayout
import android.graphics.Paint


class RadioFeatureAdapter(val onClickListener: OnClickListener, val context: Context) :
    ListAdapter<RadioTrackData, RecyclerView.ViewHolder>(CategoryDiffCallback) {


    class ViewHolder(var binding: ItemHotelRadioBtnBinding, val context: Context) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: RadioTrackData, position: Int) {

            binding.index = position + 1

            // define view width by text length
            val title = item.getTitleWithLocale(ChatApplication.system_lang)
            val performer = item.getSingerWithLocale(ChatApplication.system_lang)

            val paint = Paint()
            paint.textSize = 40F
            val titleLength = paint.measureText(title)

            paint.textSize = 20F
            val performerLength = paint.measureText(performer)

            var maxWidth = if (titleLength > performerLength) titleLength else performerLength
            if (maxWidth < 240F) { maxWidth = 240F}

            val layoutParams = LinearLayout.LayoutParams(
                (maxWidth).toInt(),
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            binding.tvTrackTitle.layoutParams = layoutParams
            binding.tvTrackSinger.layoutParams = layoutParams


            // setup animation
            val duration = (20 * maxWidth).toLong()

            val animation = TranslateAnimation(
                binding.llHotelTrack.width.toFloat(),
                -(maxWidth).toFloat(),
                0f,
                0f
            )

            animation.duration = duration
            val interpolator = LinearInterpolator()
            animation.interpolator = interpolator
            animation.repeatCount = -1

            if (item.onSelect) {
                binding.cvHotelFeatureTrack.setCardBackgroundColor(context.resources.getColor(R.color.radio_on_play))
                binding.tvTrackTitle.startAnimation(animation)
                binding.tvTrackSinger.startAnimation(animation)
            } else {
                binding.cvHotelFeatureTrack.setCardBackgroundColor(context.resources.getColor(R.color.hotel_radio_track))
                binding.tvTrackTitle.clearAnimation()
                binding.tvTrackSinger.clearAnimation()
            }

            binding.tvTrackTitle.text = title
            binding.tvTrackSinger.text = performer
            binding.tvTrackTime.text = item.duration

            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(ItemHotelRadioBtnBinding.inflate(layoutInflater, parent, false), context)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val track = getItem(position)

        if (holder is ViewHolder) {
            holder.bind(track, position)
            holder.itemView.setOnClickListener {
                onClickListener.onClick(track, position)
            }
        }
    }


    companion object CategoryDiffCallback : DiffUtil.ItemCallback<RadioTrackData>() {
        override fun areItemsTheSame(oldItem: RadioTrackData, newItem: RadioTrackData): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: RadioTrackData, newItem: RadioTrackData): Boolean {
            return oldItem.onSelect == newItem.onSelect
        }
    }

    class OnClickListener(val clickListener: (item: RadioTrackData, position: Int) -> Unit) {
        fun onClick(item: RadioTrackData, position: Int) = clickListener(item, position)
    }
}