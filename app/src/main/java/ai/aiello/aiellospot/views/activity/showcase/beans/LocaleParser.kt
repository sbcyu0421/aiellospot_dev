package ai.aiello.aiellospot.views.activity.showcase.beans

import ai.aiello.aiellospot.ChatApplication
import org.json.JSONObject

class LocaleParser {

    companion object {
        fun parser(locale: ChatApplication.Language, jsonObjectRawData: String): String {

            val jsonObject = JSONObject(jsonObjectRawData)

            var title = ""

            try {
                title = when (locale) {
                    ChatApplication.Language.en_US -> jsonObject.getString("name_en")
                    ChatApplication.Language.zh_TW -> jsonObject.getString("name_zh")
                    ChatApplication.Language.zh_CN -> jsonObject.getString("name_cn")
                    ChatApplication.Language.japanese -> jsonObject.getString("name_ja")
                    else -> jsonObject.getString("name_en")
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
            return title
        }
    }

}