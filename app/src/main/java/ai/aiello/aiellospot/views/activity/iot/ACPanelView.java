package ai.aiello.aiellospot.views.activity.iot;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONObject;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.modules.iot.IoTModuleManager;

public class ACPanelView extends Dialog {

    private static final String TAG = ACPanelView.class.getSimpleName();

    private TextView tv_room_degree;
    private ImageView ac_exit;
    protected long clickTimeStamp;


    public ACPanelView(Context context, int theme) {
        super(context, theme);
        clickTimeStamp = 0;
    }

    public ACPanelView setLayoutParams(int width, int height) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setCancelable(true);
        this.setContentView(R.layout.ac_panel_view);
        this.getWindow().setGravity(Gravity.CENTER);
        this.getWindow().setLayout(width, height);

        tv_room_degree = findViewById(R.id.tv_room_degree);
        ly_power = findViewById(R.id.ly_power);
        ly_level_up = findViewById(R.id.ly_level_up);
        ly_level_down = findViewById(R.id.ly_level_down);
        ly_level_speed = findViewById(R.id.ly_level_speed);
        ac_exit = findViewById(R.id.ac_exit);

        wind_mode_auto = findViewById(R.id.wind_mode_auto);
        wind_mode_high = findViewById(R.id.wind_mode_high);
        wind_mode_mid = findViewById(R.id.wind_mode_mid);
        wind_mode_low = findViewById(R.id.wind_mode_low);

        return this;
    }


    public void show() {
        this.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN;
        super.show();
        this.getWindow().getDecorView().setSystemUiVisibility(uiOptions);
        this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        return;
    }

    private UiDeviceAC deviceAC;
    private LinearLayout ly_power;
    private LinearLayout ly_level_up;
    private LinearLayout ly_level_down;
    private LinearLayout ly_level_speed;

    private ImageView wind_mode_auto;
    private ImageView wind_mode_high;
    private ImageView wind_mode_mid;
    private ImageView wind_mode_low;

    private Runnable runnable_visible = new Runnable() {
        @Override
        public void run() {
            Log.d(TAG, "shineText Runnable = " + shine);
            try {
                if (shine < shineMax) {
                    shine++;
                    shining = true;
                    tv_room_degree.setText(deviceAC.getsDegree() + "°C");
                    handler.postDelayed(runnable_invisible, shineFreq);
                } else {
                    shining = false;
                    tv_room_degree.setText(deviceAC.getrDegree() + "°C");
                }
            } catch (Exception e) {
                SpotDeviceLog.exception(TAG, "", e.getMessage());
                Log.e(TAG, e.toString());
            }
        }
    };

    private Runnable runnable_invisible = new Runnable() {
        @Override
        public void run() {
            try {
                tv_room_degree.setText("");
                handler.postDelayed(runnable_visible, shineFreq);
            } catch (Exception e) {
                SpotDeviceLog.exception(TAG, "", e.getMessage());
                Log.e(TAG, e.toString());
            }
        }
    };

    private Handler handler;
    private boolean shining = false;
    private int shine = 0;
    private final int shineMax = 5;
    private final int shineFreq = 500;
    private boolean sync = false;

    public boolean isShining() {
        return shining;
    }

    private void startShineText() {
        shine = 0;
        if (!shining) {
            handler.post(runnable_visible);
        }
    }

    public void setContent(UiDeviceAC ac) {
        this.deviceAC = ac;
        sync = true;
        handler = new Handler();
        handler.post(uiRunnable);
        handler.post(statusSyncRunnable);
        updateView();

        ly_power.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickTimeStamp = System.currentTimeMillis();

                if (cmdThread == null) {
                    deviceAC.copyStatusBeforeClick();
                }

                if (deviceAC.getPower() == UiDeviceAC.Power.On) {
                    deviceAC.setPower(UiDeviceAC.Power.Off);
                } else {
                    deviceAC.setPower(UiDeviceAC.Power.On);

                    SpotUserTraceLog3.getInstance().buildUIEventLog(
                            SpotUserTraceLog3.EventSubject.IOT,
                            SpotUserTraceLog3.EventAction.ON,
                            "Air conditioner"
                    );
                }

                updateView();
                doAction(3000);
            }
        });

        ly_level_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickTimeStamp = System.currentTimeMillis();
                startShineText();

                if (cmdThread == null) {
                    deviceAC.copyStatusBeforeClick();
                }

                if (deviceAC.getsDegree() < deviceAC.getTemperatureMax()) {
                    deviceAC.setsDegree(deviceAC.getsDegree() + 1);
                } else {
                    deviceAC.setsDegree(deviceAC.getTemperatureMax());
                }

                updateView();
                doAction(3000);
            }
        });

        ly_level_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickTimeStamp = System.currentTimeMillis();
                startShineText();

                if (cmdThread == null) {
                    deviceAC.copyStatusBeforeClick();
                }

                if (deviceAC.getsDegree() > deviceAC.getTemperatureMin()) {
                    deviceAC.setsDegree(deviceAC.getsDegree() - 1);
                } else {
                    deviceAC.setsDegree(deviceAC.getTemperatureMin());
                }
                updateView();
                doAction(3000);
            }
        });

        ly_level_speed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickTimeStamp = System.currentTimeMillis();

                if (cmdThread == null) {
                    deviceAC.copyStatusBeforeClick();
                }
                //upgrade by current level
                switch (deviceAC.getWindMode()) {
                    case Auto:
                        // next state
                        deviceAC.setWindMode(UiDeviceAC.WindMode.High);
                        break;

                    case High:
                        // next state
                        deviceAC.setWindMode(UiDeviceAC.WindMode.Medium);
                        break;

                    case Medium:
                        // next state
                        deviceAC.setWindMode(UiDeviceAC.WindMode.Low);
                        break;

                    case Low:
                        // next state
                        deviceAC.setWindMode(UiDeviceAC.WindMode.Auto);
                        break;

                }
                updateView();

                doAction(3000);
            }

        });

        ac_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }

    private CustomCmdThread cmdThread = null;

    private void doAction(int waitMillis) {
        try {
            cmdThread.interrupt();
        } catch (Exception e) {
        }
        cmdThread = new CustomCmdThread(waitMillis);
        cmdThread.start();
    }

    class CustomCmdThread extends Thread {
        int waitMillis;

        public CustomCmdThread(int waitMillis) {
            this.waitMillis = waitMillis;
            this.setName("AC_CustomCmdThread");
        }

        @Override
        public void run() {
            super.run();
            try {
                Log.d(TAG, "run: cmd after " + waitMillis + " ms");
                Thread.sleep(waitMillis);
                //Power
                if (deviceAC.getPower() != deviceAC.getBefore_power()) {
                    try {
                        for (UiDeviceAC.CmdUnit cmdUnit : deviceAC.getControlObj()) {
                            JSONObject action = new JSONObject();
                            action.put("uid", cmdUnit.getUuid());
                            if (deviceAC.getPower() == UiDeviceAC.Power.Off)
                                action.put("execute", "set_switch_off");
                            else {
                                action.put("execute", "set_switch_on");

                                SpotUserTraceLog3.getInstance().buildUIEventLog(
                                        SpotUserTraceLog3.EventSubject.IOT,
                                        SpotUserTraceLog3.EventAction.ON,
                                        "Air conditioner"
                                );
                            }
                            action.put("value", -1);
                            IoTModuleManager.getInstance().deliveryRCUAction(action.toString());
                            deviceAC.setBefore_power(deviceAC.getPower());
                        }
                        Thread.sleep(150);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (deviceAC.getPower() == UiDeviceAC.Power.Off)
                    return;

                //setTemp
                if (deviceAC.getsDegree() != deviceAC.getBefore_sDegree()) {
                    try {
                        for (UiDeviceAC.CmdUnit cmdUnit : deviceAC.getControlObj()) {
                            JSONObject action = new JSONObject();
                            action.put("uid", cmdUnit.getUuid());
                            action.put("execute", "set_temperature_");
                            action.put("value", deviceAC.getsDegree());
                            IoTModuleManager.getInstance().deliveryRCUAction(action.toString());
                            deviceAC.setBefore_sDegree(deviceAC.getsDegree());
                        }
                        Thread.sleep(150);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                //Fan
                if (deviceAC.getWindMode() != deviceAC.getBefore_windMode()) {
                    try {
                        for (UiDeviceAC.CmdUnit cmdUnit : deviceAC.getControlObj()) {
                            JSONObject action = new JSONObject();
                            action.put("uid", cmdUnit.getUuid());
                            switch (deviceAC.getWindMode()) {
                                case Auto:
                                    action.put("execute", "set_fan_speed_auto");
                                    break;
                                case High:
                                    action.put("execute", "set_fan_speed_max");
                                    break;
                                case Medium:
                                    action.put("execute", "set_fan_speed_middle");
                                    break;
                                case Low:
                                    action.put("execute", "set_fan_speed_min");
                                    break;
                            }
                            action.put("value", -1);
                            IoTModuleManager.getInstance().deliveryRCUAction(action.toString());
                            deviceAC.setBefore_windMode(deviceAC.getWindMode());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                cmdThread = null;
            } catch (InterruptedException e) {
                Log.d(TAG, "run: cmd interrupt");
            } finally {
                Log.d(TAG, "run: set cmdThread to null");
            }
        }
    }

    private void updateView() {
        wind_mode_auto.setImageResource(R.drawable.ic_ac_dot);
        wind_mode_low.setImageResource(R.drawable.ic_ac_dot);
        wind_mode_mid.setImageResource(R.drawable.ic_ac_dot);
        wind_mode_high.setImageResource(R.drawable.ic_ac_dot);

        switch (deviceAC.getWindMode()) {
            case Auto:
                wind_mode_auto.setImageResource(R.drawable.ic_ac_dot_fill);
                break;
            case Low:
                wind_mode_low.setImageResource(R.drawable.ic_ac_dot_fill);
                break;
            case Medium:
                wind_mode_mid.setImageResource(R.drawable.ic_ac_dot_fill);
                break;
            case High:
                wind_mode_high.setImageResource(R.drawable.ic_ac_dot_fill);
                break;
        }

        if (!shining) {
            tv_room_degree.setText(deviceAC.getrDegree() + "°C");
        } else {
            tv_room_degree.setText(deviceAC.getsDegree() + "°C");
        }

        if (deviceAC.getPower() == UiDeviceAC.Power.On) {
            ly_level_up.setBackgroundResource(R.drawable.btn_iot_ripple_enable);
            ly_level_down.setBackgroundResource(R.drawable.btn_iot_ripple_enable);
            ly_level_speed.setBackgroundResource(R.drawable.btn_iot_ripple_enable);
            ly_level_up.setEnabled(true);
            ly_level_down.setEnabled(true);
            ly_level_speed.setEnabled(true);
        } else {
            ly_level_up.setBackgroundResource(R.drawable.btn_iot_ripple_disable);
            ly_level_down.setBackgroundResource(R.drawable.btn_iot_ripple_disable);
            ly_level_speed.setBackgroundResource(R.drawable.btn_iot_ripple_disable);
            ly_level_up.setEnabled(false);
            ly_level_down.setEnabled(false);
            ly_level_speed.setEnabled(false);

            wind_mode_auto.setImageResource(R.drawable.ic_ac_dot);
            wind_mode_low.setImageResource(R.drawable.ic_ac_dot);
            wind_mode_mid.setImageResource(R.drawable.ic_ac_dot);
            wind_mode_high.setImageResource(R.drawable.ic_ac_dot);

        }


    }

    private Runnable statusSyncRunnable = new Runnable() {
        @Override
        public void run() {
            if (sync & !shining) {
                Log.d(TAG, "ask ac status");
                try {
                    JSONObject action = new JSONObject();
                    action.put("uid", deviceAC.getReadObj().getUuid());
                    action.put("execute", "sync");
                    action.put("value", -1);
                    IoTModuleManager.getInstance().deliveryRCUAction(action.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                handler.postDelayed(this, 20 * 1000);
            }
        }
    };

    private Runnable uiRunnable = new Runnable() {
        @Override
        public void run() {
            if (!shining) {
                updateView();
            }
            handler.postDelayed(this, 1000);
        }
    };

    @Override
    public void dismiss() {
        Log.d(TAG, "dimiss");
        clickTimeStamp = 0;
        doAction(0);
        super.dismiss();
        sync = false;
        handler.removeCallbacks(null);
    }
}
