package ai.aiello.aiellospot;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.provider.Settings;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.litesuits.android.log.Log;
import com.litesuits.common.io.FileUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ai.aiello.aiellospot.core.asr.ASRManager;
import ai.aiello.aiellospot.core.config.SystemConfig;
import ai.aiello.aiellospot.core.config.UIConfig;
import ai.aiello.aiellospot.core.device.DeviceControl;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.media.AielloMediaPlayer;
import ai.aiello.aiellospot.core.media.AielloSoundPool;
import ai.aiello.aiellospot.core.messager.MessageManager;
import ai.aiello.aiellospot.core.soundai.SoundAIManager;
import ai.aiello.aiellospot.core.system.AielloWifiManager;
import ai.aiello.aiellospot.core.system.BatteryListener;
import ai.aiello.aiellospot.core.system.InterruptService;
import ai.aiello.aiellospot.core.system.Pinger;
import ai.aiello.aiellospot.core.system.ServiceInstallReceiver;
import ai.aiello.aiellospot.core.system.WifiReceiver;
import ai.aiello.aiellospot.events.system.SystemEvent;
import ai.aiello.aiellospot.modules.iot.IoTModuleManager;
import ai.aiello.aiellospot.modules.music.MusicServiceModuleManager;
import ai.aiello.aiellospot.modules.showcase.radio.HotelMusicManager;
import ai.aiello.aiellospot.modules.voip.VoipModuleManager;
import ai.aiello.aiellospot.views.componets.homebtn.HomeLayoutManager;


public class ChatApplication extends Application {

    private static String TAG = ChatApplication.class.getSimpleName();
    public static String db_lang;
    public static String chatbot_lang;
    public static String dbLanguageCode;
    public static String tts_lang;
    public static Locale locale;
    public static Language system_lang = Language.None;
    public static ArrayBlockingQueue<byte[]> emitQueue;
    public static boolean tutor = false;
    public static Context context;

    public static int releaseType;
    public static boolean forceUpdate = true;  // production:true
    public static boolean printLog = true;    // production:false todo true

    public ASRManager asrManager;
    public IoTModuleManager ioTManager;
    public SoundAIManager soundAIManager;
    public BatteryListener batteryListener;
    public static ExecutorService worker = Executors.newFixedThreadPool(20);

    public static ArrayList<Language> supportLocaleList = new ArrayList<>();

    public static AppState currentAppState = AppState.BUSY;
    public static boolean hotReloadOnNextStateChange = false;

    public enum AppState {
        BUSY, IDLE
    }

    public enum Language {
        None,
        zh_TW,
        zh_CN,
        en_US,
        japanese
    }


    @Override
    public void onCreate() {
        super.onCreate();
        //get release type from project gradle.properties
        Log.e(TAG, "release_type = " + BuildConfig.conf_releaseType);
        releaseType = BuildConfig.conf_releaseType;
        context = this;
        Thread.currentThread().setName("Aiello Application");
        Log.isPrint = printLog;
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        intentFilter.addAction("android.net.wifi.STATE_CHANGE");
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        intentFilter.addAction("android.net.wifi.RSSI_CHANGED");
        intentFilter.addAction("android.net.wifi.SUPPLICANT_CONNECTION_CHANGE_ACTION");
        registerReceiver(new WifiReceiver(), intentFilter);

        IntentFilter intentFilter2 = new IntentFilter();
        intentFilter2.addAction("SERVICE_INSTALL_COMPLETE");
        registerReceiver(new ServiceInstallReceiver(), intentFilter2);

        DeviceControl.initDeviceControlManager(this, printLog);

        AielloSoundPool.initSoundPool(this);

        Settings.System.putFloat(getBaseContext().getContentResolver(),
                Settings.System.FONT_SCALE, (float) 1.0);

        AielloMediaPlayer.getInstance().init(this);

        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }


    public void clearBTopp() {
        Log.d(TAG, "clearBToop");
        getContentResolver().delete(Uri.parse("content://com.android.bluetooth.opp/btopp"), null, null);
    }

    public static void setUpWifiCountryCode() {
        String path = "/data/local/tmp/connectivity/regulatory_country";
        String existCode = "";
        try {
            existCode = FileUtils.readFileToString(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (!SystemConfig.wifi_country.equals(existCode) && !SystemConfig.wifi_country.isEmpty()) {
                File file = new File("/data/local/tmp/connectivity/regulatory_country");
                file.createNewFile();
                file.setReadable(true, false);
                FileUtils.writeStringToFile(file, SystemConfig.wifi_country);
                AielloWifiManager.reInitWifi();
                Log.d(TAG, " Setup country code : " + SystemConfig.wifi_country);
            } else {
                Log.e(TAG, " Setup country code : skip");
            }
        } catch (Exception e) {
            Log.e(TAG, " Setup country code : fail, error = " + e.toString());
        }
    }

    public void startPowerListener() {
        batteryListener = new BatteryListener(this);
        batteryListener.register();
    }

    //start MessageService
    public static void initMessageService(Context context) {
        MessageManager.getInstance().init(
                context,
                String.format(Locale.getDefault(), "%s_%s_%s", DeviceInfo.hotelName, DeviceInfo.roomName, DeviceInfo.MAC),
                DeviceInfo.MAC, "SPOT",
                DeviceInfo.hotelName,
                DeviceInfo.roomName,
                SystemConfig.Kafka.consumer_freq,
                SystemConfig.Kafka.consumer_data_length,
                SystemConfig.Kafka.keepalive_freq);
    }


    public void startSoundAIService(String saiModel) {
        if (SoundAIManager.saiServiceConn == null) {
            soundAIManager = new SoundAIManager(this, saiModel);
            SpotDeviceLog.info(TAG, "", "SoundAIService Manager Start");
            Log.e(TAG, "SoundAIService start");
        } else {
            try {
                SoundAIManager.changeWakeupWord(saiModel);
                Log.e(TAG, "SoundAIService resume, changeWakeupWord");
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }
            SpotDeviceLog.info(TAG, "", "MusicService resume, do nothing");
            Log.e(TAG, "SoundAIService resume, do nothing");
        }
    }


    public void startInterruptService() {
        Intent iis = new Intent(this, InterruptService.class);
        startService(iis);
    }


    public void initSPOTSystem() {
        setLang(system_lang);
        //initService
//        startInterruptService();
        startSoundAIService(DeviceInfo.saiModel);

        DeviceControl.set24Format(ChatApplication.this);
        DeviceControl.setTimeZone();
        DeviceControl.updateLanguage(locale);
        DeviceControl.updateTimezone(this);

        initPing(this);
        clearBTopp();

        EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.MODULE_START));
    }

    static Pinger pinger;

    public void initSocketManager() {
        emitQueue = new ArrayBlockingQueue<>(500, true);
        asrManager = ASRManager.getInstance();
        asrManager.initASRService(this);
    }

    public static void releaseResource() {

//        try {
//            VoipModuleManager.getInstance().stopPing();
//        } catch (Exception ex) {
//            SpotDeviceLog.exception(TAG, "", ex.getMessage());
//            Log.e(TAG, ex.toString());
//        }
//
//        try {
//            ASRManager.onEmit = false;
//        } catch (Exception ex) {
//            SpotDeviceLog.exception(TAG, "", ex.getMessage());
//            Log.e(TAG, ex.toString());
//        }
//
//        try {
//            MusicServiceModuleManager.getInstance().shutdownService();
//        } catch (Exception ex) {
//            SpotDeviceLog.exception(TAG, "", ex.getMessage());
//            Log.e(TAG, ex.toString());
//        }
//
//        try {
//            HotelMusicManager.INSTANCE.shutdownPlayer();
//        } catch (Exception ex) {
//            SpotDeviceLog.exception(TAG, "", ex.getMessage());
//            Log.e(TAG, ex.toString());
//        }

//        try {
//            BatteryListener.unregister();
//        } catch (Exception e) {
//            KafkaManager.getInstance().enqueueLog(ElasticDeviceLog.createExceptionlog(TAG, "", e.getMessage()));
//            Log.e(TAG, e.toString());
//        }

//        try {
//            pinger.interrupt();
//            pinger = null;
//        } catch (Exception e) {
//            SpotDeviceLog.exception(TAG, "", e.getMessage());
//            Log.e(TAG, e.toString());
//        }
//
//        SpotDeviceLog.info(TAG, "", "application releaseResource");
//        Log.e(TAG, "application releaseResource");
    }


    public static void setLang(Language i) {
        switch (i) {
            case en_US:
                ChatApplication.dbLanguageCode = "en";
                ChatApplication.tts_lang = "en";
                locale = Locale.ENGLISH;
                DeviceInfo.assistantName = UIConfig.wakupConfigBean.getWakeupWord().getEn();
                DeviceInfo.assistantName_voice = UIConfig.wakupConfigBean.getWakeupWordSelf().getEn();
                DeviceInfo.saiModel = UIConfig.wakupConfigBean.getSaiModel().getEn();
                DeviceInfo.city = DeviceInfo.city_en;
                break;

            case zh_TW:
                ChatApplication.dbLanguageCode = "zh-tw";
                ChatApplication.tts_lang = "tw";
                locale = Locale.TAIWAN;
                DeviceInfo.assistantName = UIConfig.wakupConfigBean.getWakeupWord().getTw();
                DeviceInfo.assistantName_voice = UIConfig.wakupConfigBean.getWakeupWordSelf().getTw();
                DeviceInfo.saiModel = UIConfig.wakupConfigBean.getSaiModel().getTw();
                DeviceInfo.city = DeviceInfo.city_tw;
                break;

            case zh_CN:
                ChatApplication.dbLanguageCode = "zh-cn";
                ChatApplication.tts_lang = "tw";
                locale = Locale.CHINA;
                DeviceInfo.assistantName = UIConfig.wakupConfigBean.getWakeupWord().getCn();
                DeviceInfo.assistantName_voice = UIConfig.wakupConfigBean.getWakeupWordSelf().getCn();
                DeviceInfo.saiModel = UIConfig.wakupConfigBean.getSaiModel().getCn();
                DeviceInfo.city = DeviceInfo.city_cn;
                break;

            case japanese:
                ChatApplication.dbLanguageCode = "ja";
                ChatApplication.tts_lang = "ja";
                locale = Locale.JAPANESE;
                DeviceInfo.assistantName = UIConfig.wakupConfigBean.getWakeupWord().getJa();
                DeviceInfo.assistantName_voice = UIConfig.wakupConfigBean.getWakeupWordSelf().getJa();
                DeviceInfo.saiModel = UIConfig.wakupConfigBean.getSaiModel().getJa();
                DeviceInfo.city = DeviceInfo.city_en;
                break;
        }
    }


    public static void showActivityDialog(final Activity activity) {

        Log.e("全局弹窗", "前台");
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);

        View view = LayoutInflater.from(activity).inflate(R.layout.popupwindow, null);
        alertDialog.setView(view);

        if (alertDialog.getWindow() != null) {
            Window window = alertDialog.getWindow();
            window.setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            WindowManager.LayoutParams params = window.getAttributes();
            params.width = WindowManager.LayoutParams.MATCH_PARENT;
            params.height = WindowManager.LayoutParams.MATCH_PARENT;
            params.gravity = Gravity.CENTER;
            window.setAttributes(params);
        }


        alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (alertDialog.isShowing()) {
                    alertDialog.dismiss();
                }
                return false;
            }
        });

        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (alertDialog.isShowing()) {
                    alertDialog.dismiss();
                }
            }
        });

        if (!alertDialog.isShowing()) {
            alertDialog.show();
        }

    }


    public static void setCurrentAppState(AppState state) {
        currentAppState = state;
        android.util.Log.d(TAG, "setCurrentIdleState: "+ state);
        EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.APP_STATE_CHANGED));
        if (hotReloadOnNextStateChange) {
            switch (currentAppState) {
                case BUSY:
                    EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.BUSY_STATE_HOT_RELOAD));
                    break;
                case IDLE:
                    EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.IDLE_STATE_HOT_RELOAD));
                    break;
            }
            hotReloadOnNextStateChange = false;
        };
    }


    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onMessageEvent(SystemEvent event) {
        switch (event.getType()){
            case BUSY_STATE_HOT_RELOAD:
                break;
            case IDLE_STATE_HOT_RELOAD:
                HomeLayoutManager.getInstance().initLayoutData();
                ChatApplication.setLang(ChatApplication.system_lang);
                ASRManager.getInstance().setASRModel();
                SoundAIManager.changeWakeupWord(DeviceInfo.saiModel);
                break;
        }
    }

    public static void initPing(Context context){
        if (pinger != null) {
            pinger.updateConfig();
        } else {
            pinger = new Pinger(context);
            pinger.start();
        }
    }

}

