package ai.aiello.aiellospot.intents.items;

import android.content.Context;

import com.litesuits.android.log.Log;

import org.json.JSONObject;

import java.util.concurrent.CountDownLatch;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.dialog.ControlBarDialog;
import ai.aiello.aiellospot.views.activity.roobo.RooboActivity;
import ai.aiello.aiellospot.views.activity.settings.SettingBlueToothActivity;
import ai.aiello.aiellospot.intents.ISingleCount;
import ai.aiello.aiellospot.intents.IntentObject;
import ai.aiello.aiellospot.core.device.DeviceControl;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;

public class DeviceControlIntent extends IntentObject implements ISingleCount {

    private static final String TAG = DeviceControlIntent.class.getSimpleName();
    private static int count;
    private int id;

    public DeviceControlIntent(String intent, String chat_response, String session, boolean end_of_dialog, String chatbot_data, String querytext) {
        super(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
        id = count++;
    }

    @Override
    public void execute(Context context, CountDownLatch latch) {
        switch (intent) {
            case "Dialogflow.HW.BlueTooth":
                controlBT(context);
                break;

            case "Dialogflow.HW.Monitor":
                controlMonitor(context);
                break;

            case "Dialogflow.HW.Speaker":
                controlSpeaker(context);
                break;
        }
        latch.countDown();
    }

    private void controlSpeaker(Context context) {
        setaClass(ControlBarDialog.class);
        setcName(context.getString(R.string.volume_intent));

        String key1 = "value";
        SpotUserTraceLog3.EventAction userLogAction = SpotUserTraceLog3.EventAction.UNKNOWN;

        try {
            JSONObject object = new JSONObject(chatbot_data);
            set_intent_result(getChat_response());
            set_user_log_response(get_intent_result());
            Object value = object.get(key1);
            try {
                //control by value
                int intValue = (int) value;
                if (intValue > 10) intValue = 10;
                if (intValue < 0) intValue = 0;
                DeviceControl.setVolume(intValue);
                userLogAction = SpotUserTraceLog3.EventAction.SET_VALUE;
            } catch (Exception e) {
                //control by action
                String action = (String) value;
                switch (action) {
                    case "up":
                        DeviceControl.upVolume();
                        userLogAction = SpotUserTraceLog3.EventAction.UP;
                        break;

                    case "down":
                        DeviceControl.downVolume();
                        userLogAction = SpotUserTraceLog3.EventAction.DOWN;
                        break;

                    case "max":
                        DeviceControl.maxVolume();
                        userLogAction = SpotUserTraceLog3.EventAction.MAX;
                        break;

                    case "min":
                        DeviceControl.minVolume();
                        userLogAction = SpotUserTraceLog3.EventAction.MIN;
                        break;

                    case "off":
                        DeviceControl.muteVol();
                        userLogAction = SpotUserTraceLog3.EventAction.OFF;
                        break;

                    case "on":
                        DeviceControl.defaultVol();
                        userLogAction = SpotUserTraceLog3.EventAction.ON;
                        break;

                    default:
                        changeToFormatErrorIntentObject(context.getString(R.string.speaker_error), new Exception("control value is incorrect"));
                        break;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            changeToFormatErrorIntentObject(context.getString(R.string.speaker_error), e);
        } finally {
            SpotUserTraceLog3.getInstance().buildVoiceEventLog(
                    SpotUserTraceLog3.EventSubject.VOLUME,
                    userLogAction,
                    this.querytext,
                    this.get_user_log_response(),
                    ""
            );
        }

    }

    private void controlMonitor(Context context) {
        setaClass(ControlBarDialog.class);
        setcName(context.getString(R.string.brightness_intent));

        String key1 = "value";

        SpotUserTraceLog3.EventAction userLogAction = SpotUserTraceLog3.EventAction.UNKNOWN;

        try {
            JSONObject object = new JSONObject(chatbot_data);
            set_intent_result(getChat_response());
            set_user_log_response(get_intent_result());
            Object value = object.get(key1);
            if (value instanceof Integer) {
                //control by value
                int intValue = (int) value;
                if (intValue == -1) {
                    throw new Exception("value is -1");
                }
                DeviceControl.setBrightness(intValue * 10);
                userLogAction = SpotUserTraceLog3.EventAction.SET_VALUE;
            } else {
                //control by action
                String action = (String) value;
                switch (action) {
                    case "up":
                        DeviceControl.upBrightness();
                        userLogAction = SpotUserTraceLog3.EventAction.UP;
                        break;

                    case "down":
                        DeviceControl.downBrightness();
                        userLogAction = SpotUserTraceLog3.EventAction.DOWN;
                        break;

                    case "max":
                        DeviceControl.maxBrightness();
                        userLogAction = SpotUserTraceLog3.EventAction.MAX;
                        break;

                    case "min":
                        DeviceControl.minBrightness();
                        userLogAction = SpotUserTraceLog3.EventAction.MIN;
                        break;

                    case "off":
                        DeviceControl.backLightOff();
                        setaClass(RooboActivity.class);
                        setMsg1(getcName());
                        setMsg2(getChat_response());
                        set_intent_result(getChat_response());
                        set_user_log_response(get_intent_result());
                        userLogAction = SpotUserTraceLog3.EventAction.OFF;
                        break;

                    case "on":
                        DeviceControl.backLightOn();
                        setaClass(RooboActivity.class);
                        setMsg1(getcName());
                        setMsg2(getChat_response());
                        set_intent_result(getChat_response());
                        set_user_log_response(get_intent_result());
                        userLogAction = SpotUserTraceLog3.EventAction.ON;
                        break;

                    default:
                        changeToFormatErrorIntentObject(context.getString(R.string.brightness_error), new Exception("control value is incorrect"));
                        break;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            changeToFormatErrorIntentObject(context.getString(R.string.brightness_error), new Exception("HW-Monitor-Control or set_vol format error"));
        }
        finally {
            SpotUserTraceLog3.getInstance().buildVoiceEventLog(
                    SpotUserTraceLog3.EventSubject.SCREEN,
                    userLogAction,
                    this.querytext,
                    this.get_user_log_response(),
                    ""
            );
        }
    }

    private void controlBT(Context context) {
        this.setaClass(SettingBlueToothActivity.class);
        this.setcName(context.getResources().getString(R.string.bluetooth_intent));
        String key1 = "value";
        SpotUserTraceLog3.EventAction userLogAction = SpotUserTraceLog3.EventAction.UNKNOWN;

        try {
            JSONObject object = new JSONObject(chatbot_data);
            set_intent_result(getChat_response());
            set_user_log_response(get_intent_result());
            String action = object.getString(key1);

            //control by action
            switch (action) {
                case "on":
                    userLogAction = SpotUserTraceLog3.EventAction.ON;
                    if (DeviceControl.blueToothState.equals(DeviceControl.BTState.OFF)) {
                        DeviceControl.enableBT();
                    } else {
                        set_intent_result(context.getString(R.string.status_bluetooth_on2));
                        set_user_log_response(get_intent_result());
                    }
                    break;
                case "off":
                    userLogAction = SpotUserTraceLog3.EventAction.OFF;
                    if (!DeviceControl.blueToothState.equals(DeviceControl.BTState.OFF)) {
                        DeviceControl.disableBT();
                    } else {
                        set_intent_result(context.getString(R.string.status_bluetooth_off));
                        set_user_log_response(get_intent_result());
                    }
                    break;
                default:
                    changeToFormatErrorIntentObject(context.getString(R.string.bt_error), new Exception("control value is incorrect"));
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            changeToFormatErrorIntentObject(context.getString(R.string.bt_error), e);
        }
        finally {
            SpotUserTraceLog3.getInstance().buildVoiceEventLog(
                    SpotUserTraceLog3.EventSubject.BLUETOOTH,
                    userLogAction,
                    this.querytext,
                    this.get_user_log_response(),
                    ""
            );
        }


    }


    @Override
    public int getCount() {
        return count;
    }

    @Override
    public void resetCount() {
        count = 0;
    }

    @Override
    public int getId() {
        return id;
    }


}
