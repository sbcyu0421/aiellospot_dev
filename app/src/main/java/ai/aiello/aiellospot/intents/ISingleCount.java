package ai.aiello.aiellospot.intents;

public interface ISingleCount {

    int max = 1;

    int getId();

    int getCount();

    void resetCount();

}
