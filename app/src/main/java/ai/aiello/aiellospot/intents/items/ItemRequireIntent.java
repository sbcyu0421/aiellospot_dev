package ai.aiello.aiellospot.intents.items;

import android.content.Context;

import com.litesuits.android.log.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.CountDownLatch;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.core.config.APIConfig;
import ai.aiello.aiellospot.core.config.Configer;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.intents.IntentManager;
import ai.aiello.aiellospot.intents.IntentObject;
import ai.aiello.aiellospot.utlis.Http;
import ai.aiello.aiellospot.views.activity.roomservice.ItemRequireActivity;

public class ItemRequireIntent extends IntentObject {

    private static final String TAG = ItemRequireIntent.class.getSimpleName();

    public ItemRequireIntent(String intent, String chat_response, String session, boolean end_of_dialog, String chatbot_data, String querytext) {
        super(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
        this.setaClass(ItemRequireActivity.class);
    }

    /**
     * 可送物品(接受), 需併單
     * 不可送物品(直接拒絕), 需併單
     * 不可送物品(有說明)
     */


    @Override
    public void execute(Context context, CountDownLatch latch) {
        this.setcName(context.getString(R.string.deliver_intent));

        try {

            JSONObject object = new JSONObject(chatbot_data);
            //item
            if (!object.toString().contains("numbers")) {
                throw new Exception("callback json no numbers key");
            }

            JSONArray able_item = object.getJSONArray("items");
            JSONArray unable_item = object.getJSONArray("und_items");
            JSONArray reask_resp = object.getJSONArray("reask_resp");

            boolean form_confirm = true;
            try {
                form_confirm = object.getBoolean("form_confirm");
            } catch (JSONException e) {
                e.printStackTrace();
                android.util.Log.e(TAG, "set form_confirm by default is true");
            }

            if (form_confirm) {

                if (able_item.length() == 0 && unable_item.length() == 0 && reask_resp.length() == 0) {
                    throw new Exception("ableItem && unalbeItem &&  reask_resp is empty");
                }

                IntentManager.removeTask(this);
                /*
                 * able deliver
                 */
                if (able_item.length() > 0) {
                    JSONObject ableCollectionToDB = new JSONObject();
                    JSONArray ableItemsToDB = new JSONArray();

                    ableCollectionToDB.put("lang", ChatApplication.dbLanguageCode);
                    ableCollectionToDB.put("type", 2);
                    ableCollectionToDB.put("items", ableItemsToDB);

                    StringBuilder able_sb_ui = new StringBuilder();
                    String res = "";

                    for (int s = 0; s < able_item.length(); s++) {
                        //for db use
                        JSONArray able_item_db = object.getJSONArray("items_uni");
                        String uni_item = able_item_db.getString(s);
                        JSONArray able_number = object.getJSONArray("numbers");
                        int uni_item_number = able_number.getInt(s);

                        JSONObject tmp = new JSONObject();
                        tmp.put("unikey", uni_item);
                        tmp.put("quantity", uni_item_number);
                        ableItemsToDB.put(tmp);

                        //for ui use
                        JSONArray able_item_ui = object.getJSONArray("items");
                        able_sb_ui.append(able_item_ui.getString(s));

                        if (s < able_item.length() - 1) {
                            able_sb_ui.append(", ");
                        }
                    }

                    sendTask(ableCollectionToDB);
                    res = String.format(context.getString(R.string.room_service_coming), able_sb_ui.toString());

                    //packet able result
                    IntentObject nio = ((ItemRequireIntent) this.clone());
                    nio.reNewUID();
                    nio.set_intent_result(res);
                    nio.setMsg1(res);
                    IntentManager.addTask(nio);
                }

                /*
                 * unable deliver
                 */

                if (unable_item.length() > 0) {

                    StringBuilder unable_sb_ui = new StringBuilder();
                    for (int s = 0; s < unable_item.length(); s++) {
                        unable_sb_ui.append(unable_item.get(s));
                        if (s != unable_item.length() - 1) {
                            unable_sb_ui.append(",");
                        }
                    }

                    //pack unable result
                    String unable_result = "";
                    String unable_comming_pre = context.getString(R.string.room_service_unable_coming_pre);
                    String unable_comming_suf = context.getString(R.string.room_service_unable_coming_suf);
                    unable_result = unable_comming_pre + " " + unable_sb_ui.toString() + " " + unable_comming_suf;

                    IntentObject nio = ((ItemRequireIntent) this.clone());
                    nio.reNewUID();
                    nio.set_intent_result(unable_result);
                    nio.setMsg1(unable_result);
                    IntentManager.addTask(nio);

                }

                /*
                 * unable deliver with excuse
                 */

                if (reask_resp.length() > 0) {
                    for (int r = 0; r < reask_resp.length(); r++) {
                        String res = reask_resp.getString(r);
                        IntentObject nio = ((ItemRequireIntent) this.clone());
                        nio.reNewUID();
                        nio.set_intent_result(res);
                        nio.setMsg1(res);
                        IntentManager.addTask(nio);
                    }
                }

            } else {
                set_intent_result(chat_response);
                setMsg1(chat_response);
            }

        } catch (Exception e) {
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e(TAG, e.toString());
            changeToFormatErrorIntentObject(context.getString(R.string.item_error), e);
        } finally {
            SpotUserTraceLog3.getInstance().buildVoiceEventLog(
                    SpotUserTraceLog3.EventSubject.DELIVERY,
                    SpotUserTraceLog3.EventAction.QUERY,
                    this.querytext,
                    this.get_intent_result(),
                    ""
            );
        }
        latch.countDown();
    }

    private void sendTask(JSONObject jsonObject){
        ChatApplication.worker.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    APIConfig.WEBAPI taskApi = APIConfig.WEBAPI.build(Configer.moduleConfigData.getJSONObject("delivery").getJSONObject("service_data").getJSONObject("create_task"), Configer.useProductAPI);
                    Log.i(TAG, "sendTask");
                    String url = String.format(taskApi.getUrl(), DeviceInfo.hotelName, DeviceInfo.roomName);
                    String s = Http.post(url, jsonObject, 5000, taskApi.getKey()).response;
                    JSONObject jObj = new JSONObject(s);
                    boolean isSucceeded = jObj.getBoolean("Result");
                    Log.e(TAG, "sendTask success = " + isSucceeded);
                } catch (Exception e) {
                    SpotDeviceLog.exception(TAG, "", "sendTask" + e.getMessage());
                    Log.e(TAG, "sendTask error: " + e.toString());
                }
            }
        });
    }



}
