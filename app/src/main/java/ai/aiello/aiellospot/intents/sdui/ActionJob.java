package ai.aiello.aiellospot.intents.sdui;

import org.json.JSONArray;
import org.json.JSONObject;

import java.time.Instant;

class ActionJob {
    private String id;
    private String intentName;
    private String intentMode;
    private int chatbotValue;
    private boolean foreground;
    private JSONArray beginActions;
    private JSONArray endActions;

    public ActionJob(String actionsData) throws Exception{
        this.id = Instant.now().getEpochSecond() + "";
        JSONObject action = new JSONObject(actionsData).getJSONObject("data");
        this.intentName = action.getString("intent_name");
        this.intentMode = action.getString("intent_mode");
        this.foreground = action.getBoolean("foreground");
        this.beginActions = action.getJSONArray("begin_action");
        this.endActions = action.getJSONArray("end_action");
        this.chatbotValue = action.getInt("chatbot_value");
    }

    public int getChatbotValue() {
        return chatbotValue;
    }

    public String getId() {
        return id;
    }

    public String getIntentName() {
        return intentName;
    }

    public String getIntentMode() {
        return intentMode;
    }

    public boolean isForeground() {
        return foreground;
    }

    public JSONArray getBeginActions() {
        return beginActions;
    }

    public JSONArray getEndActions() {
        return endActions;
    }
}
