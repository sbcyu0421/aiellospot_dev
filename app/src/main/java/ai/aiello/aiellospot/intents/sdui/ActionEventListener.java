package ai.aiello.aiellospot.intents.sdui;

import ai.aiello.aiellospot.intents.sdui.ActionExecutor;

public interface ActionEventListener {

    void onBeginActionInterrupted(ActionExecutor actionExecutor);

    void onBeginActionCompleted(ActionExecutor actionExecutor);

    void onEndActionCompleted(ActionExecutor actionExecutor);

}
