package ai.aiello.aiellospot.intents.items;

import android.content.Context;

import com.google.gson.Gson;
import com.litesuits.android.log.Log;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;
import java.util.concurrent.CountDownLatch;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.modules.showcase.radio.HotelMusicManager;
import ai.aiello.aiellospot.views.activity.CustomRespActivity;
import ai.aiello.aiellospot.views.activity.music.MusicPlayer3Activity;
import ai.aiello.aiellospot.core.log.MusicLogItem;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.core.media.MsttsManager;
import ai.aiello.aiellospot.events.system.SystemEvent;
import ai.aiello.aiellospot.intents.ISingleCount;
import ai.aiello.aiellospot.intents.IntentObject;
import ai.aiello.aiellospot.intents.sdui.ActionManager;
import ai.aiello.aiellospot.modules.music.MusicServiceModuleManager;
import ai.aiello.aiellospot.views.activity.showcase.ShowcaseActivity;

public class MusicIntent extends IntentObject implements ISingleCount {

    private static final String TAG = MusicIntent.class.getSimpleName();
    private static int count;
    private int id;
    private int handleEventCount = 0;

    public MusicIntent(String intent, String chat_response, String session, boolean end_of_dialog, String chatbot_data, String querytext) {
        super(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
        this.setaClass(MusicPlayer3Activity.class);
        id = count++;
        priority = 20;
    }

    @Override
    public void execute(Context context, CountDownLatch latch) {
        this.setcName(context.getResources().getString(R.string.music_intent));
        switch (intent) {
            case "Dialogflow.Music.Change":
                changeMusic(context);
                break;

            case "Roobo.Music":
            case "Roobo.NicheAudio":
                playMusic(context);
                break;

            case "Dialogflow.Music.Pause":
                pauseMusic(context);
                break;

            case "Dialogflow.Music.Resume":
                resumeMusic(context);
                break;
        }
        latch.countDown();
    }


    private void resumeMusic(Context context) {
        SpotUserTraceLog3.EventAction logAction = null;
        try {

            if (HotelMusicManager.INSTANCE.getPlayStatus() == HotelMusicManager.MusicState.HOLD ) {
                HotelMusicManager.INSTANCE.play();
                setaClass(ShowcaseActivity.class);
                logAction = SpotUserTraceLog3.EventAction.PLAY;
            } else if (MusicServiceModuleManager.playStatus == MusicServiceModuleManager.MusicState.HOLD) {
                MusicServiceModuleManager.getInstance().play();
                setaClass(MusicPlayer3Activity.class);
                logAction = SpotUserTraceLog3.EventAction.PLAY;
            } else {
                if (MusicServiceModuleManager.isPbrAuthorized) {
                    set_intent_result("");
                } else {
                    set_intent_result(context.getString(R.string.pbr_dialog_tts_hint));
                }
                logAction = SpotUserTraceLog3.EventAction.PLAY;
                if (HotelMusicManager.INSTANCE.getPlayStatus() == HotelMusicManager.MusicState.PLAY ) {
                    setaClass(ShowcaseActivity.class);
                } else if (MusicServiceModuleManager.playStatus == MusicServiceModuleManager.MusicState.PLAY) {
                    setaClass(MusicPlayer3Activity.class);
                }
                // auto play when each player is stop
                if (MusicServiceModuleManager.playStatus == MusicServiceModuleManager.MusicState.STOP &&
                        HotelMusicManager.INSTANCE.getPlayStatus() == HotelMusicManager.MusicState.STOP) {
                    MusicServiceModuleManager.getInstance().askKKAssistant("播放熱門的歌");
                }
            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
            changeToUnknownIntentObject(context, e);
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            logAction = SpotUserTraceLog3.EventAction.ERROR;

        } finally {
            MusicLogItem.buildMusicItem(
                    SpotUserTraceLog3.EventSubject.MUSIC,
                    logAction,
                    this.querytext,
                    get_intent_result(),
                    chatbot_data,
                    SpotUserTraceLog3.EventInputType.VOICE
            );
        }
    }

    private void pauseMusic(Context context) {
        EventBus.getDefault().post(new SystemEvent(SystemEvent.Type.ALL_CHANNEL_STOP));
        SpotUserTraceLog3.EventAction logAction = null;
        try {
            Thread.sleep(200);

            if (HotelMusicManager.INSTANCE.getPlayStatus() == HotelMusicManager.MusicState.PLAY ) {
                HotelMusicManager.INSTANCE.pause();
            } else if (MusicServiceModuleManager.playStatus == MusicServiceModuleManager.MusicState.PLAY) {
                MusicServiceModuleManager.getInstance().pause();
            }

            if (MusicServiceModuleManager.playerStatus == MusicServiceModuleManager.PlayerStatus.onPause) {
                logAction = SpotUserTraceLog3.EventAction.PAUSE;
            }

            if (MusicServiceModuleManager.playerStatus == MusicServiceModuleManager.PlayerStatus.Offline) {
                logAction = SpotUserTraceLog3.EventAction.OFFLINE;
            }

            ActionManager.getInstance().stopAllAction();
            MsttsManager.getInstance().stopTtsAndMedia();


            set_intent_result("");
            setMsg3("ShutUp");

        } catch (Exception e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            changeToUnknownIntentObject(context, e);
            logAction = SpotUserTraceLog3.EventAction.ERROR;
        } finally {
            MusicLogItem.buildMusicItem(
                    SpotUserTraceLog3.EventSubject.MUSIC,
                    logAction,
                    this.querytext,
                    get_intent_result(),
                    "",
                    SpotUserTraceLog3.EventInputType.VOICE
            );
        }
    }

    private void changeMusic(Context context) {

        SpotUserTraceLog3.EventAction logAction = null;

        try {

            String chatbot_data = getChatbot_data();
            JSONObject object = new JSONObject(chatbot_data);

            if (MusicServiceModuleManager.playerStatus != MusicServiceModuleManager.PlayerStatus.onPlay) {
                if (MusicServiceModuleManager.isPbrAuthorized) {
                    set_intent_result("");
                } else {
                    set_intent_result(context.getString(R.string.pbr_dialog_tts_hint));
                }
                MusicServiceModuleManager.getInstance().askKKAssistant("播放熱門的歌");
                logAction = SpotUserTraceLog3.EventAction.PLAY;
            } else {
                String action = object.getString("Music-Change");
                if (action.equals("prev")) {
                    MusicServiceModuleManager.getInstance().playPrev();
                    logAction = SpotUserTraceLog3.EventAction.PREV;
                } else {
                    MusicServiceModuleManager.getInstance().playNext();
                    logAction = SpotUserTraceLog3.EventAction.NEXT;
                }
                if (MusicServiceModuleManager.isPbrAuthorized) {
                    set_intent_result("");
                } else {
                    set_intent_result(context.getString(R.string.pbr_dialog_tts_hint));
                }
            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            changeToUnknownIntentObject(context, e);
            logAction = SpotUserTraceLog3.EventAction.ERROR;
        } finally {
            MusicLogItem.buildMusicItem(
                    SpotUserTraceLog3.EventSubject.MUSIC,
                    logAction,
                    this.querytext,
                    get_intent_result(),
                    "",
                    SpotUserTraceLog3.EventInputType.VOICE
            );
        }
    }

    private void playMusic(Context context) {
        SpotUserTraceLog3.EventAction logAction = null;

        try {
            // In kkbox, while not chinese, use entity from D.F, and then query its API
            if (MusicServiceModuleManager.vendor == MusicServiceModuleManager.Vendor.KKBOX) {
//                if (ChatApplication.locale.equals(Locale.CHINA) || ChatApplication.locale.equals(Locale.TAIWAN)) {
//                    MusicServiceModuleManager.getInstance().askKKAssistant(querytext);
//                } else {
//                    MusicServiceModuleManager.getInstance().askKKAssistant(
//                            new JSONObject(new Gson().toJson(getMusicEntity())).put("locale", ChatApplication.locale).put("workaround", true).toString());
//                }
                MusicServiceModuleManager.getInstance().askKKAssistant(
                        new JSONObject(new Gson().toJson(getMusicEntity()))
                                .put("locale", ChatApplication.locale)
                                .put("queryText", querytext)
                                .toString());
            } else {
                MusicServiceModuleManager.getInstance().askKKAssistant(querytext);
            }
            logAction = SpotUserTraceLog3.EventAction.PLAY;
            if (MusicServiceModuleManager.isPbrAuthorized) {
                set_intent_result("");
            } else {
                set_intent_result(context.getString(R.string.pbr_dialog_tts_hint));
            }
        } catch (Exception e) {
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e(TAG, e.toString());
            changeToUnknownIntentObject(context, e);
            logAction = SpotUserTraceLog3.EventAction.ERROR;
        } finally {
            MusicLogItem.buildMusicItem(
                    SpotUserTraceLog3.EventSubject.MUSIC,
                    logAction,
                    this.querytext,
                    get_intent_result(),
                    "",
                    SpotUserTraceLog3.EventInputType.VOICE
            );
        }
    }


    @Override
    public int getId() {
        return id;
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public void resetCount() {
        count = 0;
    }

    private MusicEntity getMusicEntity(){
        MusicEntity musicEntity = new MusicEntity();
        try {
            JSONObject object = new JSONObject(chatbot_data);
            try {
                musicEntity.setTrack(object.getString("music-track"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                musicEntity.setArtist(object.getString("music-artist"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                musicEntity.setGenre(object.getString("music-genre"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return musicEntity;
    }

    static class MusicEntity {
        String track;
        String genre;
        String artist;

        public void setTrack(String track) {
            this.track = track;
        }

        public void setGenre(String genre) {
            this.genre = genre;
        }

        public void setArtist(String artist) {
            this.artist = artist;
        }
    }

}
