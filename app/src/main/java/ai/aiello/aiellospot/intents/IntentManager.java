package ai.aiello.aiellospot.intents;

import android.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.PriorityQueue;

import ai.aiello.aiellospot.views.activity.dialog.ControlBarDialog;
import ai.aiello.aiellospot.views.activity.unknown.UnknownActivity;
import ai.aiello.aiellospot.intents.items.NewsIntent;
import ai.aiello.aiellospot.intents.items.RooboIntent;
import ai.aiello.aiellospot.intents.items.SurroundIntent;
import ai.aiello.aiellospot.intents.items.VoipIntent;
import ai.aiello.aiellospot.intents.sdui.intents.MarketingIntent;

/**
 * Created by a1990 on 2019/3/25.
 */

public class IntentManager {

    private static ArrayList<IntentObject> tmpListForMulti = new ArrayList<>();

    private static String TAG = IntentManager.class.getSimpleName();

    public static ArrayList<JSONObject> chatbotResult = new ArrayList<>();

    public static PriorityQueue<IntentObject> taskQueue = new PriorityQueue<>(100, new IntentComparator());

    public static ArrayList<IntentObject> finishIntentList = new ArrayList<>();

    public static String BUNDLE_TAG = "IntentBundle";

    public static void backupIntents() {
        tmpListForMulti = new ArrayList<>(finishIntentList);
    }

    public static void resumeBackupIntents(){
        finishIntentList = new ArrayList<>(tmpListForMulti);
    }

    public static synchronized void removeTask(IntentObject io) {
        try {
            taskQueue.remove(io);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public static synchronized void addTask(IntentObject io) {
        try {
            taskQueue.offer(io);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public static synchronized void clearTask() {
        try {
            IntentManager.taskQueue.clear();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }


    public static boolean hideControlBarIntent() {
        //hide vol control bar in multi
        IntentObject tmp_dialog = null;
        for (IntentObject io : finishIntentList) {
            if (io.getaClass().equals(ControlBarDialog.class)) {
                tmp_dialog = io;
            }
        }
        if (tmp_dialog != null) {
            finishIntentList.remove(tmp_dialog);
            return true;
        } else {
            return false;
        }
    }


    public static void removeUnHandleIntent() {
        ArrayList<IntentObject> tmp = new ArrayList<>();
        for (IntentObject io : IntentManager.taskQueue) {
            if (io.getaClass().equals(UnknownActivity.class)) {
                tmp.add((io));
            }
        }
        for (IntentObject rmi : tmp) {
            IntentManager.removeTask(rmi);
        }
    }

    public static IntentObject removeRooboIntent() {
        ArrayList<IntentObject> tmp = new ArrayList<>();
        IntentObject chatIntent = null;
        for (IntentObject io : taskQueue) {
            if (io instanceof RooboIntent) {
                tmp.add(io);
                chatIntent = io;
            }
        }
        for (IntentObject rmi : tmp) {
            IntentManager.removeTask(rmi);
        }
        return chatIntent;
    }


    public static void dropDuplicateInstance() {
        //drop the duplicate intent
        ArrayList<IntentObject> tmp = new ArrayList<>();
        for (IntentObject io : taskQueue) {
            if (io instanceof ISingleCount && ((ISingleCount) io).getId() >= ISingleCount.max) {
                tmp.add(io);
            }
        }

        for (IntentObject io : tmp) {
            removeTask(io);
            Log.d(TAG, String.format("drop duplicate intent = %s ", io.getClass().getSimpleName()));
        }

        // reset counter
        for (IntentObject io : taskQueue) {
            if (io instanceof ISingleCount) {
                ((ISingleCount) io).resetCount();
            }
        }

    }


    /*drop forbidden intent in multi-view, but we should keep the last one to guarantee at least one result */
    public static void dropForbiddenIntentInMulti() {
        Iterator<IntentObject> itr = taskQueue.iterator();
        while (itr.hasNext()) {
            if (taskQueue.size() == 1) break;
            IntentObject io = itr.next();
            if (io instanceof SurroundIntent || io instanceof NewsIntent || io instanceof MarketingIntent) {
                itr.remove();
                Log.i(TAG, "dropSurroundingIntentInMulti or NewsIntent or ServerDrivenIntent");
            }
        }
    }


    public static IntentObject getAdvancedIntent() {
        IntentObject advancedIntent = null;
        for (IntentObject io : IntentManager.taskQueue) {
            if (!io.getEnd_of_dialog()) {
                advancedIntent = io;
            }
        }
        return advancedIntent;
    }


    /**
     * VOIP is the only one
     */
    public static void checkExcludeIntent() {
        IntentObject checkExcludeIntent = null;
        for (IntentObject io : IntentManager.taskQueue) {
            if (io instanceof VoipIntent) {
                checkExcludeIntent = io;
                Log.d(TAG, String.format("get exclude intent = %s ", io.getClass().getSimpleName()));

            }
        }
        if (checkExcludeIntent != null) {
            clearTask();
            addTask(checkExcludeIntent);
        }

    }

    public static class IntentStandardFormat {
        String intent;
        String chat_response;
        String session;
        boolean end_of_dialog;
        String chatbot_data;
        String querytext;
        String org_querytext;
        String agentActions;

        public IntentStandardFormat(String intent, String chat_response, String session, boolean end_of_dialog, String chatbot_data, String querytext, String org_querytext, String agentActions) {
            this.intent = intent;
            this.chat_response = chat_response;
            this.session = session;
            this.end_of_dialog = end_of_dialog;
            this.chatbot_data = chatbot_data;
            this.querytext = querytext;
            this.org_querytext = org_querytext;
            this.agentActions = agentActions;
        }

        public String getIntent() {
            return intent;
        }

        public String getChat_response() {
            return chat_response;
        }

        public String getSession() {
            return session;
        }

        public boolean isEnd_of_dialog() {
            return end_of_dialog;
        }

        public String getChatbot_data() {
            return chatbot_data;
        }

        public String getQuerytext() {
            return querytext;
        }

        public String getOrg_querytext() {
            return org_querytext;
        }

        public String getAgentActions() {
            return agentActions;
        }

    }


}
