package ai.aiello.aiellospot.intents.items;

import android.annotation.SuppressLint;
import android.content.Context;

import com.litesuits.android.log.Log;

import java.net.SocketTimeoutException;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.fun.SurroundingDetailActivity;
import ai.aiello.aiellospot.views.activity.fun.SurroundingListActivity;
import ai.aiello.aiellospot.modules.surrounding.Items;
import ai.aiello.aiellospot.modules.surrounding.SurroundingModuleManager;
import ai.aiello.aiellospot.views.activity.qa.QAActivity;
import ai.aiello.aiellospot.intents.ISingleCount;
import ai.aiello.aiellospot.intents.IntentObject;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;

public class SurroundIntent extends IntentObject implements ISingleCount {

    private static final String TAG = SurroundIntent.class.getSimpleName();
    private static int count;
    private int id;

    public SurroundIntent(String intent, String chat_response, String session, boolean end_of_dialog, String chatbot_data, String querytext, String org_querytext) {
        super(intent, chat_response, session, end_of_dialog, chatbot_data, querytext, org_querytext);
        id = count++;
        priority = 20;
    }

    @SuppressLint("StringFormatInvalid")
    @Override
    public void execute(Context context, CountDownLatch latch) {

        try {
            this.setcName(context.getString(R.string.ui_cuisine_service));
            Items items = SurroundingModuleManager.getInstance().getSurroundingItems(org_querytext, querytext, ChatApplication.locale.toString(), 1);
            if (items != null) {
                SurroundingModuleManager.getInstance().setCurrentBeanIndex(0);
                if (items.getItems().size() > 1) {
                    setaClass(SurroundingListActivity.class);
                    StringBuilder sb = new StringBuilder();
                    if (ChatApplication.locale == Locale.TAIWAN || ChatApplication.locale == Locale.CHINA) {
                        String msg_tw = "以下是我在15公里內找到的有關「%s」的資訊，包括";
                        sb.append(String.format(msg_tw, querytext));
                        for (int i = 0; i < 3; i++) {
                            try {
                                String title = items.getItems().get(i).getDetail().getName();
                                sb.append(title);
                                if (i == 2) {
                                    break;
                                }
                                sb.append(",");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        sb.append("等" + items.getItems().size() + "則資訊");
                    } else if (ChatApplication.locale == Locale.ENGLISH) {
                        String msg_en = "There are %s pieces of information about %s within 15 kilometers, such as ";
                        sb.append(String.format(msg_en, items.getItems().size(), querytext));
                        for (int i = 0; i < 3; i++) {
                            try {
                                String title = items.getItems().get(i).getDetail().getName_en();
                                sb.append(title);
                                if (i == 2) {
                                    break;
                                }
                                sb.append(",");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        //japanese: {location1}, {location2}, {location3}他、計{6}件をおすすめいたします。
                        for (int i = 0; i < 3; i++) {
                            try {
                                String title = items.getItems().get(i).getDetail().getName_en();
                                sb.append(title);
                                if (i == 2) {
                                    break;
                                }
                                sb.append(",");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        sb.append(String.format("他、計 %s件をおすすめいたします。", items.getItems().size()));
                    }

                    set_intent_result(sb.toString());
                    set_user_log_response(get_intent_result());
                } else {
                    setaClass(SurroundingDetailActivity.class);
                    String address = items.getItems().get(0).getDetail().getAddress();
                    if (ChatApplication.locale == Locale.TAIWAN || ChatApplication.locale == Locale.CHINA) {
                        String msg_tw = "我在15公里內找到一個有關「%s」的資訊，位於%s";
                        set_intent_result(String.format(msg_tw, querytext, address));
                        set_user_log_response(get_intent_result());
                    } else if (ChatApplication.locale == Locale.ENGLISH) {
                        String msg_en = "I found a information about %s within 15 kilometers, which is located at %s";
                        set_intent_result(String.format(msg_en, querytext, address));
                        set_user_log_response(get_intent_result());
                    } else {
                        String title = items.getItems().get(0).getDetail().getName_en();
                        String msg_ja = " %sが１５キロメートル先の%sに見つかりました";
                        set_intent_result(String.format(msg_ja, title, address));
                        set_user_log_response(get_intent_result());
                    }
                }
                SurroundingModuleManager.getInstance().setCurrentItems(items);
                if (items.getType() == null) {
                    changeToNotFound(context);
                }
            } else {
                changeToNotFound(context);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            if (e instanceof SocketTimeoutException) {
                changeToFormatErrorIntentObject(context.getString(R.string.surrounding_timeout_error), e);
            } else {
                changeToFormatErrorIntentObject(context.getString(R.string.surrounding_format_error), e);
//                changeToUnknownIntentObject(context, e);
            }
        } finally {
            SpotUserTraceLog3.getInstance().buildVoiceEventLog(
                    SpotUserTraceLog3.EventSubject.SURROUNDING,
                    SpotUserTraceLog3.EventAction.QUERY,
                    this.querytext,
                    this.get_user_log_response(),
                    ""
            );
        }
        latch.countDown();
    }


    private void changeToNotFound(Context context) {
        Log.d(TAG, "intent change ---> changeToNotFound");
        String result = String.format(context.getString(R.string.vsp_surrounding_result_no), querytext);
        set_intent_result(result);
        set_user_log_response(get_intent_result());
        setMsg1(result);
        setaClass(QAActivity.class);
    }


    @Override
    public int getId() {
        return id;
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public void resetCount() {
        count = 0;
    }

}
