package ai.aiello.aiellospot.intents.items;

import android.content.Context;

import java.util.concurrent.CountDownLatch;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.unknown.TimeoutErrorActivity;
import ai.aiello.aiellospot.intents.ISingleCount;
import ai.aiello.aiellospot.intents.IntentObject;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;

public class TimeoutIntent extends IntentObject implements ISingleCount {

    private static final String TAG = TimeoutIntent.class.getSimpleName();
    private static int count;
    private int id;

    public TimeoutIntent(String intent, String chat_response, String session, boolean end_of_dialog, String chatbot_data, String querytext) {
        super(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
        this.setaClass(TimeoutErrorActivity.class);
        this.setcName("");
        id = count++;
    }

    @Override
    public void execute(Context context, CountDownLatch latch) {
        set_intent_result(context.getString(R.string.timeout_error_intent_msg));
        set_user_log_response(get_intent_result());
        SpotUserTraceLog3.getInstance().buildVoiceEventLog(
                SpotUserTraceLog3.EventSubject.TIME_OUT,
                SpotUserTraceLog3.EventAction.NONE,
                this.querytext,
                this.get_user_log_response(),
                ""
        );
        latch.countDown();
    }


    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public void resetCount() {
        count = 0;
    }


    @Override
    public int getId() {
        return id;
    }
}
