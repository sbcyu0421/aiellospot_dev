package ai.aiello.aiellospot.intents.items;

import android.content.Context;

import com.litesuits.android.log.Log;

import java.util.concurrent.CountDownLatch;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.intents.IntentObject;
import ai.aiello.aiellospot.views.activity.qa.QAActivity;

public class FAQIntent extends IntentObject {

    private static final String TAG = FAQIntent.class.getSimpleName();

    public FAQIntent(String intent, String chat_response, String session, boolean end_of_dialog, String chatbot_data, String querytext) {
        super(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
        this.setaClass(QAActivity.class);
    }

    @Override
    public void execute(Context context, CountDownLatch latch) {
        try {
            this.setcName(context.getString(R.string.info_intent));
            set_intent_result(getChat_response());
            set_user_log_response(get_intent_result());
            setMsg1(getChat_response());
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            changeToUnknownIntentObject(context, e);
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        } finally {
            SpotUserTraceLog3.getInstance().buildVoiceEventLog(
                    SpotUserTraceLog3.EventSubject.FAQ,
                    SpotUserTraceLog3.EventAction.QUERY,
                    this.querytext,
                    this.get_user_log_response(),
                    ""
            );
        }
        latch.countDown();
    }


}
