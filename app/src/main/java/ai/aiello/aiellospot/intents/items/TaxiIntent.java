package ai.aiello.aiellospot.intents.items;

import android.content.Context;

import com.litesuits.android.log.Log;

import java.util.concurrent.CountDownLatch;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.taxi.TaxiActivity;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.intents.ISingleCount;
import ai.aiello.aiellospot.intents.IntentObject;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;

public class TaxiIntent extends IntentObject implements ISingleCount {

    private static final String TAG = TaxiIntent.class.getSimpleName();
    private static int count;
    private int id;

    public TaxiIntent(String intent, String chat_response, String session, boolean end_of_dialog, String chatbot_data, String querytext) {
        super(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
        this.setaClass(TaxiActivity.class);
        id = count++;
    }

    @Override
    public void execute(Context context, CountDownLatch latch) {
        try {
            this.setcName(context.getResources().getString(R.string.taxi_intent));
            String result = getChat_response();
            set_intent_result(result);
            setMsg1(result);
            set_user_log_response(get_intent_result());
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            changeToUnknownIntentObject(context, e);
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        } finally {
            SpotUserTraceLog3.getInstance().buildVoiceEventLog(
                    SpotUserTraceLog3.EventSubject.TAXI,
                    SpotUserTraceLog3.EventAction.CALL,
                    this.querytext,
                    this.get_user_log_response(),
                    ""
            );
        }
        latch.countDown();
    }


    @Override
    public int getCount() {
        return count;
    }

    @Override
    public void resetCount() {
        count = 0;
    }


    @Override
    public int getId() {
        return id;
    }
}
