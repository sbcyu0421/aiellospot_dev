package ai.aiello.aiellospot.intents.items;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.CountDownLatch;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.core.config.APIConfig;
import ai.aiello.aiellospot.core.config.Configer;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.intents.IntentObject;
import ai.aiello.aiellospot.utlis.Http;
import ai.aiello.aiellospot.views.activity.roobo.RooboActivity;

public class RepairIntent extends IntentObject {

    private static final String TAG = RepairIntent.class.getSimpleName();

    public RepairIntent(String intent, String chat_response, String session, boolean end_of_dialog, String chatbot_data, String querytext) {
        super(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
        this.setaClass(RooboActivity.class);
    }

    @Override
    public void execute(Context context, CountDownLatch latch) {
        this.setcName(context.getResources().getString(R.string.repair_intent));
        if (isFormConfirm()) sendRepairRequest();
//        set_intent_result(context.getString(R.string.repair_msg));
        setMsg1(context.getResources().getString(R.string.repair_intent));
        setMsg2(getChat_response());
        set_intent_result(getChat_response());
        SpotUserTraceLog3.getInstance().buildVoiceEventLog(
                SpotUserTraceLog3.EventSubject.REPAIRMENT,
                SpotUserTraceLog3.EventAction.QUERY,
                this.querytext,
                this.get_intent_result(),
                ""
        );
        latch.countDown();
    }

    private boolean isFormConfirm() {
        try {
            JSONObject jObj = new JSONObject(chatbot_data);
            return jObj.getBoolean("form_confirm");
        } catch (JSONException e) {
            return false;
        }
    }

    private void sendRepairRequest() {
        ChatApplication.worker.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    APIConfig.WEBAPI repairAPI = APIConfig.WEBAPI.build(Configer.moduleConfigData.getJSONObject("repair")
                            .getJSONObject("service_data").getJSONObject("create_task"), Configer.useProductAPI);
                    int count = 0;
                    JSONObject jsonBody = new JSONObject();
                    jsonBody.put("room", DeviceInfo.roomName);
                    jsonBody.put("content", querytext);
                    // send repair request
                    while (count < 5) {
                        try {
                            Http.HttpRes res = Http.post(String.format(repairAPI.getUrl(), DeviceInfo.hotelName), jsonBody, 5000, repairAPI.getKey());
                            if (res.responseCode / 100 == 2) {
                                android.util.Log.d(TAG, "sendRepairRequest: Success");
                                break;
                            } else {
                                count ++;
                                android.util.Log.d(TAG, "sendRepairRequest: fail on " + count + " try, response code = " + res.responseCode);
                                Thread.sleep(2000);
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
