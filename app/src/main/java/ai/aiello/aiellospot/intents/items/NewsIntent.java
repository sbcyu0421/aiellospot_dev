package ai.aiello.aiellospot.intents.items;

import android.content.Context;

import com.litesuits.android.log.Log;

import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.concurrent.CountDownLatch;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.core.config.APIConfig;
import ai.aiello.aiellospot.core.config.Configer;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.core.media.MsttsManager;
import ai.aiello.aiellospot.intents.ISingleCount;
import ai.aiello.aiellospot.intents.IntentObject;
import ai.aiello.aiellospot.utlis.Http;
import ai.aiello.aiellospot.views.activity.news.NewsActivity;

public class NewsIntent extends IntentObject implements ISingleCount {

    private static final String TAG = NewsIntent.class.getSimpleName();
    private static int count;
    private int id;

    public NewsIntent(String intent, String chat_response, String session, boolean end_of_dialog, String chatbot_data, String querytext) {
        super(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
        setaClass(NewsActivity.class);
        id = count++;
    }

    @Override
    public void execute(Context context, CountDownLatch latch) {
        setcName(context.getResources().getString(R.string.news_intent));
        try {
            setMsg1(getcName());
            setMsg2(chat_response);
            String duration = playNews();
            setMsg3(duration);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            changeToFormatErrorIntentObject(context.getString(R.string.news_intent_no_found), e);
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        } finally {
            SpotUserTraceLog3.getInstance().buildVoiceEventLog(
                    SpotUserTraceLog3.EventSubject.NEWS,
                    SpotUserTraceLog3.EventAction.QUERY,
                    this.querytext,
                    this.get_user_log_response(),
                    ""
            );
        }
        latch.countDown();
    }


    @Override
    public int getCount() {
        return count;
    }

    @Override
    public void resetCount() {
        count = 0;
    }


    @Override
    public int getId() {
        return id;
    }

    public String playNews() throws Exception {
        APIConfig.WEBAPI newsAPI = APIConfig.WEBAPI.build(Configer.moduleConfigData.getJSONObject("news")
                .getJSONObject("service_data"), Configer.useProductAPI);
        android.util.Log.d(TAG, "request_newsapi");
        String result = Http.get(newsAPI.getUrl(),
                7000, newsAPI.getKey()).response;
        String content = StringEscapeUtils.unescapeJava(result);
        JSONObject jsonObject = new JSONObject(content);
        JSONArray data = jsonObject.getJSONArray("Data");
        //always get first one as looks like the news first one is the newest
        JSONObject _obj = data.getJSONObject(0);
        String news_url = _obj.getString("url");
        int duration = 0;
        //for compatibility
        try {
            String voice_head = _obj.getString("voice_head");
            duration = _obj.getInt("duration");
            if (!voice_head.equals("")) {
                MsttsManager.getInstance().speakWord(voice_head, true, ChatApplication.context);
            }
        } catch (Exception ex) {
            android.util.Log.w(TAG, "News old api");
        }
        MsttsManager.getInstance().reportNews(news_url, true, ChatApplication.context);
        if (duration != 0) {
            return String.valueOf(duration);
        } else {
            return String.valueOf(10 * 60);
        }
    }

}
