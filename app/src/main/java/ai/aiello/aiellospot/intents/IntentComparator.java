package ai.aiello.aiellospot.intents;


import java.util.Comparator;

public class IntentComparator implements Comparator<IntentObject> {

    @Override
    public int compare(IntentObject o1, IntentObject o2) {
        if (o1.getPriority() < o2.getPriority())
            return -1;
        else if (o1.getPriority() > o2.getPriority())
            return 1;
        return 0;
    }
}
