package ai.aiello.aiellospot.intents.items;

import android.content.Context;

import com.litesuits.android.log.Log;

import org.json.JSONObject;

import java.util.concurrent.CountDownLatch;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.roomservice.CleanActivity;
import ai.aiello.aiellospot.intents.IntentObject;

import ai.aiello.aiellospot.core.config.SystemConfig;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.modules.roomstatus.RoomStatusModuleManager;

public class RoomStatusIntent extends IntentObject {

    private static final String TAG = RoomStatusIntent.class.getSimpleName();

    public RoomStatusIntent(String intent, String chat_response, String session, boolean end_of_dialog, String chatbot_data, String querytext) {
        super(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
    }

    @Override
    public void execute(Context context, CountDownLatch latch) {
        setaClass(CleanActivity.class);
        setcName(context.getResources().getString(R.string.room_status_intent));
        SpotUserTraceLog3.EventAction userLogAction = SpotUserTraceLog3.EventAction.NONE;
        try {
            final String KEY_DND_ON = "dnd.on";
            final String KEY_DND_OFF = "dnd.off";
            final String KEY_CLEAN_ON = "clean.on";
            final String KEY_CLEAN_OFF = "clean.off";
            final String KEY_NOT_SUPPORT = "not.support";

            JSONObject object = new JSONObject(chatbot_data);
            String action = object.getString("action");
            String result = getChat_response();
            switch (action) {
                case KEY_DND_ON:
                    // push compatible
                    if (SystemConfig.OTA.support_action) {
                        RoomStatusModuleManager.getInstance().dontDisturb(true, "VOICE", false);
                    } else {
                        RoomStatusModuleManager.getInstance().dontDisturb(true, false, true, "VOICE");
                    }
                    setMsg1(result);
                    set_intent_result(result);
                    set_user_log_response(get_intent_result());
                    userLogAction = SpotUserTraceLog3.EventAction.DND_ON;
                    break;

                case KEY_DND_OFF:
                    // push compatible
                    if (SystemConfig.OTA.support_action) {
                        RoomStatusModuleManager.getInstance().dontDisturb(false, "VOICE", false);
                    } else {
                        RoomStatusModuleManager.getInstance().dontDisturb(false, false, true, "VOICE");
                    }
                    setMsg1(result);
                    set_intent_result(result);
                    set_user_log_response(get_intent_result());
                    userLogAction = SpotUserTraceLog3.EventAction.DND_OFF;
                    break;

                case KEY_CLEAN_ON:
                    // push compatible
                    if (SystemConfig.OTA.support_action) {
                        RoomStatusModuleManager.getInstance().requestClean(true, "VOICE", false);
                    } else {
                        RoomStatusModuleManager.getInstance().requestClean(true, false, true, "VOICE");
                    }
                    setMsg1(result);
                    set_intent_result(result);
                    set_user_log_response(get_intent_result());
                    userLogAction = SpotUserTraceLog3.EventAction.MUR_ON;
                    break;

                case KEY_CLEAN_OFF:
                    // push compatible
                    if (SystemConfig.OTA.support_action) {
                        RoomStatusModuleManager.getInstance().requestClean(false, "VOICE", false);
                    } else {
                        RoomStatusModuleManager.getInstance().requestClean(false, false, true, "VOICE");
                    }
                    setMsg1(result);
                    set_intent_result(result);
                    set_user_log_response(get_intent_result());
                    userLogAction = SpotUserTraceLog3.EventAction.MUR_OFF;
                    break;

                case KEY_NOT_SUPPORT:
                    setMsg1(result);
                    set_intent_result(result);
                    set_user_log_response(get_intent_result());
                    userLogAction = SpotUserTraceLog3.EventAction.NONE;
                    break;

                default:
                    changeToUnknownIntentObject(context, new Exception("unHandle action value is incorrect"));
                    userLogAction = SpotUserTraceLog3.EventAction.UNKNOWN;
                    break;
            }

        } catch (Exception e) {
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e(TAG, e.toString());
            changeToUnknownIntentObject(context, e);
            userLogAction = SpotUserTraceLog3.EventAction.UNKNOWN;
        }
        finally {
            SpotUserTraceLog3.getInstance().buildVoiceEventLog(
                    SpotUserTraceLog3.EventSubject.ROOM_STATUS,
                    userLogAction,
                    this.querytext,
                    this.get_user_log_response(),
                    ""
            );
        }
        latch.countDown();
    }


}
