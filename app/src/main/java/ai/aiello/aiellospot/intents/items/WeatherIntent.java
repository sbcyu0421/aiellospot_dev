package ai.aiello.aiellospot.intents.items;

import android.content.Context;

import com.litesuits.android.log.Log;

import java.util.concurrent.CountDownLatch;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.weather.WeatherActivity;
import ai.aiello.aiellospot.intents.IntentObject;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;

public class WeatherIntent extends IntentObject {

    private static final String TAG = WeatherIntent.class.getSimpleName();

    public WeatherIntent(String intent, String chat_response, String session, boolean end_of_dialog, String chatbot_data, String querytext) {
        super(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
        setaClass(WeatherActivity.class);
    }

    @Override
    public void execute(Context context, CountDownLatch latch) {
        try {
            setcName(context.getString(R.string.weather_intent));
//            JSONArray jsonArray = new JSONArray(chatbot_data);
//            JSONObject object = (JSONObject) jsonArray.get(0);
//            if (!object.getString("focus").equals("weather") && !object.getString("focus").equals("temp")) {
//                throw new Exception("callback json no focus key or focus value error");
//            } else if (!object.toString().contains("city") || object.getString("city").length() == 0) {
//                throw new Exception("callback json no city key or city value error");
//            } else if (!object.toString().contains("date") || object.getString("date").length() == 0) {
//                throw new Exception("callback json no date key or date value error");
//            } else {
//                setMsg1(getChat_response());
//                set_intent_result(getChat_response());
//            }
            setMsg1(getChat_response());
            set_intent_result(getChat_response());
            set_user_log_response(get_intent_result());
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            changeToFormatErrorIntentObject(context.getString(R.string.weather_error), e);
        } finally {
            SpotUserTraceLog3.getInstance().buildVoiceEventLog(
                    SpotUserTraceLog3.EventSubject.WEATHER,
                    SpotUserTraceLog3.EventAction.QUERY,
                    this.querytext,
                    this.get_user_log_response(),
                    ""
            );
        }
        latch.countDown();
    }


}
