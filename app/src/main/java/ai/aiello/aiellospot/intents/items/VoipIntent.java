package ai.aiello.aiellospot.intents.items;

import android.content.Context;

import com.litesuits.android.log.Log;

import org.json.JSONObject;

import java.util.concurrent.CountDownLatch;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.voip.VoipHomeActivity;
import ai.aiello.aiellospot.views.activity.voip.VoipOnCallActivity;
import ai.aiello.aiellospot.intents.ISingleCount;
import ai.aiello.aiellospot.intents.IntentObject;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.modules.voip.Phonebook;
import ai.aiello.aiellospot.modules.voip.VoipModuleManager;

public class VoipIntent extends IntentObject implements ISingleCount {

    private static final String TAG = VoipIntent.class.getSimpleName();
    private static int count;
    private int id;

    public VoipIntent(String intent, String chat_response, String session, boolean end_of_dialog, String chatbot_data, String querytext) {
        super(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
        this.setaClass(VoipOnCallActivity.class);
        id = count++;
    }


    @Override
    public void execute(Context context, CountDownLatch latch) {
        this.setcName(context.getString(R.string.voip_intent));
        String voip_control = "voip_control";
        String voip_subject = "voip_subject";
        String room_num = "room_num";

        SpotUserTraceLog3.EventAction userLogAction = SpotUserTraceLog3.EventAction.NONE;
        String data = "";

        try {
            if (getIntent().equals("Dialogflow.Voip")) {

                JSONObject object = new JSONObject(chatbot_data);
                String result = getChat_response();
                set_intent_result(result);
                String control = object.getString(voip_control);
                if (VoipModuleManager.getInstance().currentState.getName() == VoipModuleManager.StateName.Disable) {
                    changeToFormatErrorIntentObject(context.getString(R.string.voip_forbidden_action_on_disable_state), new Exception("forbidden action on disable state"));
                } else {
                    switch (control) {
                        case "make_call":

                            userLogAction = SpotUserTraceLog3.EventAction.DIAL;
//                        if (VoipManager.status == VoipManager.Status.ComingCallMode || VoipManager.status == VoipManager.Status.MakeCallMode) {
                            if (VoipModuleManager.getInstance().currentState.getName() == VoipModuleManager.StateName.OnMakeCallWaiting || VoipModuleManager.getInstance().currentState.getName() == VoipModuleManager.StateName.OnInComingCallWaiting) {
                                Log.e(TAG, "ComingCallMode,MakeCallMode  can't make call");
                                SpotDeviceLog.info(TAG, "", "e.getMessage()");
                                changeToFormatErrorIntentObject(context.getString(R.string.hint_only_voip), new Exception("ComingCallMode,MakeCallMode  can't make call"));
                            } else {
                                try {
                                    String subject = object.getString(voip_subject);
                                    switch (subject) {
                                        case "front_desk":
                                            VoipModuleManager.getInstance().makeCall(Phonebook.FRONT_DESK_NUMBER);
                                            set_user_log_response(context.getString(R.string.call_frontdesk));
                                            data = "Front desk";
                                            break;
                                        case "sos":
                                            VoipModuleManager.getInstance().makeCall(Phonebook.SOS_NUMBER);
                                            set_user_log_response(context.getString(R.string.call_sos));
                                            data = "Emergency";
                                            break;
                                        case "restaurant":
                                            VoipModuleManager.getInstance().makeCall(Phonebook.RESTAURANT_NUMBER);
                                            set_user_log_response(context.getString(R.string.call_restaurant));
                                            data = "Restaurant";
                                            break;
                                        case "room":
                                            String num_room = object.getString(room_num);
                                            if (!num_room.isEmpty()) {
                                                VoipModuleManager.getInstance().makeCall(num_room);
                                                data = num_room;
                                                set_user_log_response(context.getString(R.string.call_restaurant));
                                            } else {
                                                throw new Exception("num_room is empty");
                                            }
                                            break;
                                        default:
                                            throw new Exception("subject is empty");
                                    }

                                    //wait for socket emit completed
                                    do {
                                        Thread.sleep(1000);
                                    } while (VoipModuleManager.getInstance().makeCallRes.isEmpty());

                                    if (VoipModuleManager.getInstance().makeCallRes.equals(VoipModuleManager.ResponseStatus.OFFLINE.name())) {
                                        set_intent_result(context.getString(R.string.call_offline));
                                        set_user_log_response(get_intent_result());
                                        setaClass(VoipHomeActivity.class);
                                    } else if (VoipModuleManager.getInstance().makeCallRes.equals(VoipModuleManager.ResponseStatus.SELF.name())) {
                                        set_intent_result(context.getString(R.string.call_self_error));
                                        set_user_log_response(get_intent_result());
                                        setaClass(VoipHomeActivity.class);
                                    } else if (VoipModuleManager.getInstance().makeCallRes.equals(VoipModuleManager.ResponseStatus.BUSY.name())) {
                                        set_intent_result(context.getString(R.string.call_other_busy));
                                        set_user_log_response(get_intent_result());
                                        setaClass(VoipHomeActivity.class);
                                    } else if (VoipModuleManager.getInstance().makeCallRes.equals(VoipModuleManager.ResponseStatus.FAIL.name())) {
                                        set_intent_result(context.getString(R.string.makecall_fail));
                                        set_user_log_response(get_intent_result());
                                        setaClass(VoipHomeActivity.class);
                                    }

                                } catch (Exception ex) {
                                    Log.e(TAG, "callback json no " + voip_subject + " key or value");
                                    changeToFormatErrorIntentObject(context.getString(R.string.voip_error), ex);
                                }

                            }


                            break;

                        case "make_call_cancel":
                            userLogAction = SpotUserTraceLog3.EventAction.CANCEL;
                            switch (VoipModuleManager.getInstance().currentState.getName()) {
                                case OnInComingCallWaiting:
                                    //Log.e(TAG, "ComingCallMode can't cancel call");
                                    //changeToFormatErrorIntentObject2(io, getString(R.string.hint_only_accept_voip));
                                    set_intent_result(context.getString(R.string.hang_up_call));
                                    set_user_log_response(get_intent_result());
                                    setaClass(VoipHomeActivity.class);
                                    VoipModuleManager.getInstance().currentState.handleEvent(VoipModuleManager.Event.decline, VoipModuleManager.TX);
                                    break;
                                case OnMakeCallWaiting:
                                    setaClass(VoipHomeActivity.class);
                                    set_user_log_response(context.getString(R.string.cancel_call));
                                    VoipModuleManager.getInstance().currentState.handleEvent(VoipModuleManager.Event.decline, VoipModuleManager.TX);
                                    break;
                                default:
                                    set_intent_result(context.getString(R.string.no_coming_call));
                                    set_user_log_response(get_intent_result());
                                    break;
                            }
                            break;

                        case "accept_call":
                            userLogAction = SpotUserTraceLog3.EventAction.ACCEPT;
                            switch (VoipModuleManager.getInstance().currentState.getName()) {
                                case OnInComingCallWaiting:
                                    VoipModuleManager.getInstance().currentState.handleEvent(VoipModuleManager.Event.accept, "");
                                    set_user_log_response(context.getString(R.string.call_accepted));
                                    break;
                                case OnMakeCallWaiting:
                                    Log.e(TAG, "MakeCallMode can't accept_call");
                                    changeToFormatErrorIntentObject(context.getString(R.string.hint_only_cancel_voip), new Exception("MakeCallMode can't accept_call"));
                                    break;
                                default:
                                    set_intent_result(context.getString(R.string.no_coming_call));
                                    set_user_log_response(get_intent_result());
                                    break;
                            }

                            break;

                        case "decline_call":
                            userLogAction = SpotUserTraceLog3.EventAction.DECLINE;
                            switch (VoipModuleManager.getInstance().currentState.getName()) {
                                case OnInComingCallWaiting:
                                    setaClass(VoipHomeActivity.class);
                                    set_user_log_response(context.getString(R.string.call_declined));
                                    VoipModuleManager.getInstance().currentState.handleEvent(VoipModuleManager.Event.decline, VoipModuleManager.TX);
                                    break;
                                case OnMakeCallWaiting:
                                    set_intent_result(context.getString(R.string.hang_up_call));
                                    set_user_log_response(get_intent_result());
                                    setaClass(VoipHomeActivity.class);
                                    VoipModuleManager.getInstance().currentState.handleEvent(VoipModuleManager.Event.decline, VoipModuleManager.TX);
                                    break;
                                default:
                                    set_intent_result(context.getString(R.string.no_coming_call));
                                    set_user_log_response(get_intent_result());
                                    break;
                            }
                            break;

                        default:
                            userLogAction = SpotUserTraceLog3.EventAction.UNKNOWN;
                            if (VoipModuleManager.getInstance().currentState.getName().equals(VoipModuleManager.StateName.OnInComingCallWaiting) || VoipModuleManager.getInstance().currentState.getName().equals(VoipModuleManager.StateName.OnMakeCallWaiting)) {
                                Log.e(TAG, "default voip intent");
                                changeToFormatErrorIntentObject(context.getString(R.string.hint_only_voip), new Exception("default voip intent, do not work"));
                            } else {
                                changeToFormatErrorIntentObject(context.getString(R.string.voip_error), new Exception("default voip intent, do not work"));
                            }
                            break;
                    }
                }

            } else {
                throw new Exception("throw other intents");
            }

        } catch (Exception e) {
            userLogAction = SpotUserTraceLog3.EventAction.UNKNOWN;
            Log.d(TAG, "exception = " + e.toString());
            if (VoipModuleManager.getInstance().currentState.getName().equals(VoipModuleManager.StateName.OnInComingCallWaiting) || VoipModuleManager.getInstance().currentState.getName().equals(VoipModuleManager.StateName.OnMakeCallWaiting)) {
                Log.e(TAG, "default voip intent");
                changeToFormatErrorIntentObject(context.getString(R.string.hint_only_voip), new Exception("default voip intent, do not work"));
            } else {
                changeToFormatErrorIntentObject(context.getString(R.string.voip_error), new Exception("default voip intent, do not work"));
            }
        } finally {
            SpotUserTraceLog3.getInstance().buildVoiceEventLog(
                    SpotUserTraceLog3.EventSubject.VOIP,
                    userLogAction,
                    this.querytext,
                    this.get_user_log_response(),
                    data
            );
        }
        latch.countDown();

    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public void resetCount() {
        count = 0;
    }


    @Override
    public int getId() {
        return id;
    }

}
