package ai.aiello.aiellospot.intents.sdui.layout;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ClassicCardLayout implements Serializable {


    /**
     * layout : {"hero":{"image":"http://cloud.aiello.ai:8021/spot/assets/view/logo/icon_qa.png","text":"飯店活動標題"},"content":{"text":"飯店活動內容"}}
     * tag : 飯店活動
     * type : 1
     */

    @SerializedName("layout")
    private LayoutBean layout;
    @SerializedName("tag")
    private String tag;
    @SerializedName("type")
    private int type;

    public LayoutBean getLayout() {
        return layout;
    }

    public void setLayout(LayoutBean layout) {
        this.layout = layout;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public static class LayoutBean {
        /**
         * hero : {"image":"http://cloud.aiello.ai:8021/spot/assets/view/logo/icon_qa.png","text":"飯店活動標題"}
         * content : {"text":"飯店活動內容"}
         */

        @SerializedName("hero")
        private HeroBean hero;
        @SerializedName("content")
        private ContentBean content;

        public HeroBean getHero() {
            return hero;
        }

        public void setHero(HeroBean hero) {
            this.hero = hero;
        }

        public ContentBean getContent() {
            return content;
        }

        public void setContent(ContentBean content) {
            this.content = content;
        }

        public static class HeroBean {
            /**
             * image : http://cloud.aiello.ai:8021/spot/assets/view/logo/icon_qa.png
             * text : 飯店活動標題
             */

            @SerializedName("image")
            private String image;
            @SerializedName("text")
            private String text;

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getText() {
                return text;
            }

            public void setText(String text) {
                this.text = text;
            }
        }

        public static class ContentBean {
            /**
             * text : 飯店活動內容
             */

            @SerializedName("text")
            private String text;

            public String getText() {
                return text;
            }

            public void setText(String text) {
                this.text = text;
            }
        }
    }
}
