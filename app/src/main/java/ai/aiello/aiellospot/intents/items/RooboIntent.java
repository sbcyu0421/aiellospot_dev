package ai.aiello.aiellospot.intents.items;

import android.content.Context;

import com.litesuits.android.log.Log;

import java.util.concurrent.CountDownLatch;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.roobo.RooboActivity;
import ai.aiello.aiellospot.intents.IntentObject;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;

public class RooboIntent extends IntentObject {

    private static final String TAG = RooboIntent.class.getSimpleName();

    public RooboIntent(String intent, String chat_response, String session, boolean end_of_dialog, String chatbot_data, String querytext) {
        super(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
        this.setaClass(RooboActivity.class);
    }

    @Override
    public void execute(Context context, CountDownLatch latch) {
        SpotUserTraceLog3.EventSubject userLogSubject = SpotUserTraceLog3.EventSubject.NONE;
        SpotUserTraceLog3.EventAction userLogAction = SpotUserTraceLog3.EventAction.NONE;

        switch (intent) {
            case "Roobo.Calculator":
                this.setcName(context.getResources().getString(R.string.calculator_intent));
                userLogSubject = SpotUserTraceLog3.EventSubject.CALCULATOR;
                userLogAction = SpotUserTraceLog3.EventAction.QUERY;
                break;

            case "Roobo.Idiom":
                this.setcName(context.getResources().getString(R.string.idiom_intent));
                userLogSubject = SpotUserTraceLog3.EventSubject.IDIOM;
                userLogAction = SpotUserTraceLog3.EventAction.QUERY;
                break;

            case "Roobo.Chat":
            default:
                this.setcName(context.getResources().getString(R.string.roobo_intent));
                userLogSubject = SpotUserTraceLog3.EventSubject.CHAT;
                userLogAction = SpotUserTraceLog3.EventAction.QUERY;
                break;
        }

        try {
            setMsg1(getcName());
            setMsg2(getChat_response());
            set_intent_result(getChat_response());
            set_user_log_response(get_intent_result());
            Log.d(TAG, get_intent_result());
            if (getChat_response().length() == 0) {
                throw new Exception("callback json no response value error");
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            changeToUnknownIntentObject(context, e);
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        } finally {
            SpotUserTraceLog3.getInstance().buildVoiceEventLog(
                    userLogSubject,
                    userLogAction,
                    this.querytext,
                    this.get_user_log_response(),
                    ""
            );
        }
        latch.countDown();
    }


}
