package ai.aiello.aiellospot.intents;

import android.util.Log;

import ai.aiello.aiellospot.intents.items.AlarmIntent;
import ai.aiello.aiellospot.intents.items.ComplaintIntent;
import ai.aiello.aiellospot.intents.items.DeviceControlIntent;
import ai.aiello.aiellospot.intents.items.FAQIntent;
import ai.aiello.aiellospot.intents.items.LaundryIntent;
import ai.aiello.aiellospot.intents.sdui.intents.IoTIntent;
import ai.aiello.aiellospot.intents.items.ItemRequireIntent;
import ai.aiello.aiellospot.intents.items.MessageIntent;
import ai.aiello.aiellospot.intents.items.MusicIntent;
import ai.aiello.aiellospot.intents.items.NewsIntent;
import ai.aiello.aiellospot.intents.items.RepairIntent;
import ai.aiello.aiellospot.intents.items.RooboIntent;
import ai.aiello.aiellospot.intents.items.RoomStatusIntent;
import ai.aiello.aiellospot.intents.items.NotSupportIntent;
import ai.aiello.aiellospot.intents.items.SurroundIntent;
import ai.aiello.aiellospot.intents.items.TaxiIntent;
import ai.aiello.aiellospot.intents.items.TimeoutIntent;
import ai.aiello.aiellospot.intents.items.UnHandleIntent;
import ai.aiello.aiellospot.intents.items.ViewControlIntent;
import ai.aiello.aiellospot.intents.items.VoipIntent;
import ai.aiello.aiellospot.intents.items.WeatherIntent;
import ai.aiello.aiellospot.intents.sdui.intents.MarketingIntent;
import ai.aiello.aiellospot.modules.voip.VoipModuleManager;

public class Classifier {

    private static final String TAG = Classifier.class.getSimpleName();

    public static IntentObject classify(IntentManager.IntentStandardFormat standardFormat) {

        String intent = standardFormat.getIntent();
        String chat_response = standardFormat.getChat_response();
        String session = standardFormat.getSession();
        boolean end_of_dialog = standardFormat.isEnd_of_dialog();
        String chatbot_data = standardFormat.getChatbot_data();
        String querytext = standardFormat.getQuerytext();
        String org_querytext = standardFormat.getOrg_querytext();
        String agentActions = standardFormat.getAgentActions();

        IntentObject io = null;

        if (VoipModuleManager.getInstance().currentState.getName() == VoipModuleManager.StateName.OnMakeCallWaiting || VoipModuleManager.getInstance().currentState.getName() == VoipModuleManager.StateName.OnInComingCallWaiting) {
            Log.e(TAG, "you can't do any on waiting mode");
            io = new VoipIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
            return io;
        }

        if (intent.contains("Dialogflow.IoT")) {
            io = new IoTIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext, agentActions);
        } else if (intent.contains("Roobo")) {
            switch (intent) {
                case "Roobo.Music":
                case "Roobo.NicheAudio":
                    io = new MusicIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
                    break;

//                case "Roobo.Weather":
//                    io = new WeatherIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
//                    break;

                case "Roobo.News":
                    io = new NewsIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
                    break;

                case "Roobo.TimeDates":
                case "Roobo.Poetry":
                case "Roobo.Joke":
                case "Roobo.Star":
                case "Roobo.Baike":
                    io = new MessageIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
                    break;

                default:
                    io = new RooboIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
                    break;

            }

        } else {

            switch (intent) {
                case "Aiello.News":
                    io = new NewsIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
                    break;

                case "Aiello.Marketing":
                    io = new MarketingIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext, agentActions);
                    break;

                case "Dialogflow.Weather":
                    io = new WeatherIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
                    break;

                case "Dialogflow.Activity.Control":
                    io = new ViewControlIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
                    break;

                case "Dialogflow.Complaint":
                    io = new ComplaintIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
                    break;

                case "Dialogflow.Taxi":
                    io = new TaxiIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
                    break;

                case "Dialogflow.RoomService.Repair":
                    io = new RepairIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
                    break;

                case "Dialogflow.Voip":
                    io = new VoipIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
                    break;

                case "Dialogflow.Clock.Alarm":
                    io = new AlarmIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
                    break;

                case "Dialogflow.Music.Change":
                case "Dialogflow.Music.Pause":
                case "Dialogflow.Music.Resume":
                    io = new MusicIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
                    break;

                case "FAQ_AJ_HOTEL":
                    io = new FAQIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
                    break;

                case "Dialogflow.FAQ.Surrounding":
                    io = new SurroundIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext, org_querytext);
                    break;

                case "Dialogflow.RoomService.ItemRequire":
                    io = new ItemRequireIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
                    break;

                case "Dialogflow.RoomStatus":
                    io = new RoomStatusIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
                    break;

                case "Dialogflow.RoomService.Laundry":
                    io = new LaundryIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
                    break;

                case "Dialogflow.HW.BlueTooth":
                case "Dialogflow.HW.Monitor":
                case "Dialogflow.HW.Speaker":
                    io = new DeviceControlIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
                    break;

                case "Dialogflow.TimeoutError":
                    io = new TimeoutIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
                    break;

                case "Dialogflow.NotSupport":
                    io = new NotSupportIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
                    break;

                case "Module.NotSupport":
                    io = new MessageIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
                    break;

                default:
                    io = new UnHandleIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
                    break;

            }
        }
        Log.d(TAG, "classify intent = " + io.getClass().getSimpleName());
        return io;
    }


}
