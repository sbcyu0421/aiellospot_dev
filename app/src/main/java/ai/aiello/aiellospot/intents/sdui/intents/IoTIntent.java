package ai.aiello.aiellospot.intents.sdui.intents;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.concurrent.CountDownLatch;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.views.activity.iot.IOTCardActivity;
import ai.aiello.aiellospot.views.activity.roobo.RooboActivity;
import ai.aiello.aiellospot.core.config.SystemConfig;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.intents.IntentObject;
import ai.aiello.aiellospot.intents.sdui.ActionManager;
import ai.aiello.aiellospot.intents.sdui.ServerDrivenHelper;
import ai.aiello.aiellospot.modules.iot.IoTModuleManager;

public class IoTIntent extends IntentObject {

    private static final String TAG = IoTIntent.class.getSimpleName();

    public IoTIntent(String intent, String chat_response, String session, boolean end_of_dialog, String chatbot_data, String querytext, String agentActions) {
        super(intent, chat_response, session, end_of_dialog, chatbot_data, querytext, agentActions, true);
        this.setaClass(IOTCardActivity.class);
    }

    @Override
    public void execute(Context context, CountDownLatch latch) {
        this.setcName(context.getString(R.string.iot_intent));

        try {

            JSONObject object = new JSONObject(chatbot_data);
            boolean successInDB = object.getBoolean("success");
            String result = getChat_response();
            setMsg1(result);
            set_intent_result(result);

            if (successInDB) {
                // push compatible
                if (SystemConfig.OTA.support_action) {
                    setaClass(RooboActivity.class);
                    setMsg1(getcName());
                    setMsg2(getChat_response());
                    set_intent_result(getChat_response());
                    ServerDrivenHelper.ServerMessage serverMessage = ServerDrivenHelper.parse(agentActions);

                    // do view
                    int deviceStateCode = getDeviceStateCode(serverMessage);
                    Log.d(TAG, "deviceStateCode = " + deviceStateCode);
                    switch (deviceStateCode) {
                        case 1: // device not support
                            android.util.Log.i(TAG, "using chatbot resp");
                            break;
                        case 2: // None "iot" category device only, cases are action with only media. (Demo device always apply to this)
                            android.util.Log.i(TAG, "using chatbot resp");
                            break;
                        case 3: // device all online
                            android.util.Log.i(TAG, "using chatbot resp");
                            break;
                        case 4: // device all offline
                            setMsg2(context.getString(R.string.iot_not_connected));
                            set_intent_result(context.getString(R.string.iot_not_connected));
                            android.util.Log.i(TAG, "using custom resp");
                            break;
                        case 5: // device partial online
                            android.util.Log.i(TAG, "using chatbot resp");
                            break;
                    }


                    // do action
                    try {
                        ActionManager.getInstance().prepare(serverMessage.getResultAction(),true);
                    } catch (Exception e) {
                        android.util.Log.d(TAG, "execute: error : " + e.getMessage());
                    }
                } else {
                    IoTModuleManager.getInstance().push_iot_intent_to_remoteService(chatbot_data, getIntent(), null);
                    setaClass(RooboActivity.class);
                    setMsg1(getcName());
                    setMsg2(getChat_response());
                    set_intent_result(getChat_response());
                }
            }
        } catch (Exception e) {
            //sth error
            e.printStackTrace();
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            changeToFormatErrorIntentObject(context.getString(R.string.iot_error), e);
        } finally {
            String code = "";
            try {
                code = new JSONObject(chatbot_data).getJSONArray("loop_codes").getJSONObject(0).getString("code");
            } catch (Exception el) {
                el.printStackTrace();
            }
            SpotUserTraceLog3.getInstance().buildVoiceEventLog(
                    SpotUserTraceLog3.EventSubject.IOT,
                    SpotUserTraceLog3.EventAction.QUERY,
                    this.querytext,
                    get_intent_result(),
                    code
            );
        }
        latch.countDown();
    }

    private int getDeviceStateCode(ServerDrivenHelper.ServerMessage serverMessage) {

        /**
         * -- 5 conditions for checking device alive state --
         *
         * 1. iot device was not supported (only music) (getResultAction == null) -> return chatbot response
         * 2. for demo purpose || non of the device in agentAction has "iot" as their category-> return chatbot response
         * 3. all device is alive -> return chatbot response
         * 4. all device is NOT alive -> ** return "Device not connected" sentence as response
         * 5. partial device is alive -> return chatbot response
         */

        // condition 1: device not support.
        if (serverMessage.getResultAction().isEmpty()) return 1;

        HashMap<String, Boolean> deviceAliveMap = getDeviceAliveMap();

        if (deviceAliveMap.isEmpty()) {
            // condition 2: non of the device category is "iot"
            return 2;
        } else if (!deviceAliveMap.containsValue(false)) {
            // condition 3: all devices online.
            return 3;
        } else if (!deviceAliveMap.containsValue(true)) {
            // condition 4: all devices offline.
            return 4;
        } else {
            // condition 5: partial devices online.
            return 5;
        }
    }

    private HashMap<String, Boolean> getDeviceAliveMap() {

        /**
         * steps
         * 0. get ALL devices alive state from Push-service => Map1
         * 1. get devices/action info "uid" and "category" from agentAction
         * 2. if devices/action's category is "iot", then check it's alive state with Map1
         * 3. forming new Map2 by alive state, map = {{"Device_uid": "Alive of not(boolean)"}, {"Device_uid": "Alive of not(boolean)"}, ...}
         */

        // step 0.
        HashMap<String, IoTModuleManager.IotDeviceInfo> pushIotDeviceInfoMap = IoTModuleManager.iotDeviceInfo;
        HashMap<String, Boolean> iotDeviceInfoMap = new HashMap<>(); //the Map2

        // step 1.
        JSONArray beginActions = null;
        try {
            beginActions = new JSONObject(agentActions).getJSONObject("data").getJSONArray("begin_action");
        } catch (JSONException e) {
            Log.d(TAG, "getDeviceAliveMap: no data in begin_action");
            e.printStackTrace();
        }


        if (beginActions != null) {
            for (int i = 0; i < beginActions.length(); i ++) {
                try {
                    JSONObject jsonBeginAction = beginActions.getJSONObject(i);
                    String category = jsonBeginAction.getString("category");

                    //step 2.
                    if (category.equals("iot")) {
                        String uid = jsonBeginAction.getString("uid");

                        if (pushIotDeviceInfoMap.containsKey(uid)) {
                            boolean onlineState = pushIotDeviceInfoMap.get(uid).getOnline();

                            // step 3.
                            iotDeviceInfoMap.put(uid, onlineState);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }

        return iotDeviceInfoMap;
    }


}
