package ai.aiello.aiellospot.intents;

import android.content.Context;

import com.github.f4b6a3.uuid.UuidCreator;
import com.litesuits.android.log.Log;

import java.io.Serializable;
import java.util.concurrent.CountDownLatch;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.unknown.FormatErrorActivity;
import ai.aiello.aiellospot.views.activity.unknown.UnknownActivity;

import ai.aiello.aiellospot.core.chatbot.ChatbotValidator;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;

/**
 * Created by a1990 on 2019/3/20.
 */

public abstract class IntentObject implements Cloneable, Serializable {

    private static String TAG = IntentObject.class.getSimpleName();
    public String intent;
    public String chat_response;
    private String sessionId;
    public Boolean end_of_dialog;
    public String chatbot_data;
    private String intent_result = "";
    public String querytext = "";
    public String org_querytext = "";
    private String cName = "";
    private String uuid;
    private String msg1;
    private String msg2;
    private String msg3;
    private Object dataObject;
    public String agentActions;
    private String user_log_response;

    protected int specialIntent = -1;
    private Class aClass;

    private String exception;
    private boolean enable;

    protected int priority = 10;

    int getPriority() {
        return priority;
    }

    public abstract void execute(Context context, CountDownLatch latch);

    public IntentObject(String intent, String chat_response, String session, boolean end_of_dialog, String chatbot_data, String querytext) {
        this.intent = intent;
        this.chat_response = chat_response;
        this.sessionId = session;
        this.end_of_dialog = end_of_dialog;
        this.chatbot_data = chatbot_data;
        this.querytext = querytext;
        this.uuid = UuidCreator.getTimeBased().toString();
        Log.d(TAG, "IntentObject created : " + intent);
    }

    public IntentObject(String intent, String chat_response, String session, boolean end_of_dialog, String chatbot_data, String querytext, String agentActions, boolean dummy) {
        this.intent = intent;
        this.chat_response = chat_response;
        this.sessionId = session;
        this.end_of_dialog = end_of_dialog;
        this.chatbot_data = chatbot_data;
        this.querytext = querytext;
        this.uuid = UuidCreator.getTimeBased().toString();
        this.agentActions = agentActions;
        Log.d(TAG, "IntentObject created : " + intent);
    }

    public IntentObject(String intent, String chat_response, String session, boolean end_of_dialog, String chatbot_data, String querytext, String org_querytext) {
        this.intent = intent;
        this.chat_response = chat_response;
        this.sessionId = session;
        this.end_of_dialog = end_of_dialog;
        this.chatbot_data = chatbot_data;
        this.querytext = querytext;
        this.org_querytext = org_querytext;
        this.uuid = UuidCreator.getTimeBased().toString();
        Log.d(TAG, "IntentObject created : " + intent);
    }

    public void reNewUID() {
        this.uuid = UuidCreator.getTimeBased().toString();
    }

    public String getUuid() {
        return uuid;
    }

    public IntentObject(String cName, Class aClass, String intent_result) {
        this.cName = cName;
        this.aClass = aClass;
        this.intent_result = intent_result;
    }

    public IntentObject(String intent, String cName, Class aClass, String intent_result) {
        this.intent = intent;
        this.cName = cName;
        this.aClass = aClass;
        this.intent_result = intent_result;
    }

    public IntentObject(String intent, String cName, Class aClass, String chatbot_data, String chat_response) {
        this.intent = intent;
        this.cName = cName;
        this.aClass = aClass;
        this.chat_response = chat_response;
        this.chatbot_data = chatbot_data;
    }

    public String getQuerytext() {
        return querytext;
    }

    public String getIntent() {
        return intent;
    }

    public String getChat_response() {
        return chat_response;
    }

    public void setChat_response(String chat_response) {
        this.chat_response = chat_response;
    }

    public String getSessionId() {
        return sessionId;
    }

    public Boolean getEnd_of_dialog() {
        return end_of_dialog;
    }

    public String getChatbot_data() {
        return chatbot_data;
    }

    public void setaClass(Class aClass) {
        this.aClass = aClass;
    }

    public void set_intent_result(String result) {
        this.intent_result = result;
    }

    public Class getaClass() {
        return aClass;
    }

    public String get_intent_result() {
        return intent_result;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }


    //msg if need
    public String getMsg1() {
        return msg1;
    }

    public void setMsg1(String msg1) {
        this.msg1 = msg1;
    }

    public String getMsg2() {
        return msg2;
    }

    public void setMsg2(String msg2) {
        this.msg2 = msg2;
    }

    public String getMsg3() {
        return msg3;
    }

    public void setMsg3(String msg3) {
        this.msg3 = msg3;
    }


    public int getSpecialIntent() {
        return specialIntent;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }


    protected void changeToFormatErrorIntentObject(String msg, Exception e) {
        Log.e(TAG, "intent change ---> changeToFormatErrorIntentObject2, Exception = " + e.toString());
        SpotDeviceLog.error(TAG, msg, "intent change ---> changeToFormatErrorIntentObject2");
        ChatbotValidator.exception(this);
        setaClass(FormatErrorActivity.class);
        set_intent_result(msg);
        set_user_log_response(msg);
        setMsg1(getcName());
        setMsg2(msg);
        setException(e.toString());
    }


    protected void changeToUnknownIntentObject(Context context, Exception e) {
        Log.e(TAG, "intent change ---> changeToUnknownIntentObject2, Exception = " + e.toString());
        ChatbotValidator.exception(this);
        String i = context.getString(R.string.unknown_intent);
        String s = context.getString(R.string.unknown_intent_msg);
        setcName(i);
        setaClass(UnknownActivity.class);
        set_intent_result(s);
        set_user_log_response(s);
        setException(e.toString());
    }

    protected String getString(Context context, int stringId) {
        return context.getString(stringId);
    }

    public Object getDataObject() {
        return dataObject;
    }

    public void setDataObject(Object dataObject) {
        this.dataObject = dataObject;
    }

    public String get_user_log_response() {
        return user_log_response;
    }

    public void set_user_log_response(String user_log_response) {
        this.user_log_response = user_log_response;
    }

    public String getAgentActions() {
        return agentActions;
    }
}
