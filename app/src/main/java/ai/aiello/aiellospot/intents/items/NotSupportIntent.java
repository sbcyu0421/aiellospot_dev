package ai.aiello.aiellospot.intents.items;

import android.content.Context;

import com.litesuits.android.log.Log;

import org.json.JSONObject;

import java.util.concurrent.CountDownLatch;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.roobo.RooboActivity;
import ai.aiello.aiellospot.intents.IntentObject;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;

public class NotSupportIntent extends IntentObject {

    private static final String TAG = NotSupportIntent.class.getSimpleName();

    public NotSupportIntent(String intent, String chat_response, String session, boolean end_of_dialog, String chatbot_data, String querytext) {
        super(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
        this.setaClass(RooboActivity.class);
    }

    @Override
    public void execute(Context context, CountDownLatch latch) {
        try {
            JSONObject jsonObject = new JSONObject(getChatbot_data());
            String intent_orginal = jsonObject.getString("intent_orginal");
            this.setcName(context.getResources().getString(R.string.vsp_not_support_title));
            switch (intent_orginal) {
                //房控:
                case"Dialogflow.IoT":
                    this.setChat_response(context.getResources().getString(R.string.vsp_not_support_iot));
                //FAQ：
                case"Dialogflow.RoomService.ItemRequire":
                case"Dialogflow.RoomService.Laundry":
                case"FAQ_AJ_HOTEL":
                    this.setChat_response(context.getResources().getString(R.string.vsp_not_support_faq));
                    break;
                //叫車/客訴/修繕/VOIP/others
                case"Dialogflow.Taxi":
                case"Dialogflow.Complaint":
                case"Dialogflow.RoomService.Repair":
                case"Dialogflow.Voip":
                case"Dialogflow.Activity.Control":
                case"Dialogflow.RoomStatus":
                default:
                    this.setChat_response(context.getResources().getString(R.string.vsp_not_support_other));
                    break;
            }
            setMsg1(getcName());
            setMsg2(getChat_response());
            set_intent_result(getChat_response());
            set_user_log_response(get_intent_result());
            Log.d(TAG, get_intent_result());

        } catch (Exception e) {
            Log.e(TAG, e.toString());
            changeToUnknownIntentObject(context, e);
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        } finally {
            SpotUserTraceLog3.getInstance().buildVoiceEventLog(
                    SpotUserTraceLog3.EventSubject.UNSUPPORT_SERVICE,
                    SpotUserTraceLog3.EventAction.QUERY,
                    this.querytext,
                    this.get_user_log_response(),
                    ""
            );
            latch.countDown();
        }

    }


}
