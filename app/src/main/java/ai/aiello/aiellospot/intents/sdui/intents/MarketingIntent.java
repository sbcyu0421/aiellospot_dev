package ai.aiello.aiellospot.intents.sdui.intents;

import android.content.Context;

import com.google.gson.Gson;
import com.litesuits.android.log.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.Instant;
import java.util.concurrent.CountDownLatch;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.views.activity.iot.IOTCardActivity;
import ai.aiello.aiellospot.views.activity.roobo.RooboActivity;
import ai.aiello.aiellospot.views.activity.sdui.ClassicCardActivity;
import ai.aiello.aiellospot.core.config.APIConfig;
import ai.aiello.aiellospot.intents.sdui.ActionManager;
import ai.aiello.aiellospot.intents.sdui.ServerDrivenHelper;
import ai.aiello.aiellospot.intents.ISingleCount;
import ai.aiello.aiellospot.intents.IntentObject;
import ai.aiello.aiellospot.intents.sdui.layout.ClassicCardLayout;
import ai.aiello.aiellospot.intents.sdui.layout.LayoutType;
import ai.aiello.aiellospot.core.config.SystemConfig;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.media.MediaHolder;
import ai.aiello.aiellospot.core.media.PrimitiveAielloMediaPlayer;
import ai.aiello.aiellospot.modules.iot.IoTModuleManager;
import ai.aiello.aiellospot.utlis.Http;

public class MarketingIntent extends IntentObject implements ISingleCount {

    private static final String TAG = MarketingIntent.class.getSimpleName();
    private static int count;
    private int id;

    public MarketingIntent(String intent, String chat_response, String session, boolean end_of_dialog, String chatbot_data, String querytext, String agentActions) {
        super(intent, chat_response, session, end_of_dialog, chatbot_data, querytext, agentActions, true);
        this.setaClass(IOTCardActivity.class);
        id = count++;
    }

    @Override
    public void execute(Context context, CountDownLatch latch) {
        this.setcName(context.getResources().getString(R.string.market_intent));
        try {
            Log.d(TAG, "request action and view from server ");

            if (SystemConfig.OTA.support_action) {
                setaClass(RooboActivity.class);
                setMsg1(getcName());
                ServerDrivenHelper.ServerMessage serverMessage = ServerDrivenHelper.parse(agentActions);

                // do view
                String contentMessage = serverMessage.getResp().getContentMessage();
                if (!contentMessage.isEmpty()) {
                    setMsg2(contentMessage);
                    set_intent_result(contentMessage);
                    android.util.Log.i(TAG, "using custom resp");
                } else {
                    setMsg2(getChat_response());
                    set_intent_result(getChat_response());
                    android.util.Log.i(TAG, "using chatbot resp");
                }

                // do action
                try {
                    ActionManager.getInstance().prepare(serverMessage.getResultAction(), true);
                } catch (Exception e) {
                    android.util.Log.d(TAG, "execute: error : " + e.getMessage());
                }
            } else {
                JSONObject object = new JSONObject(chatbot_data);
                String event_type = object.getString("event_type");
                boolean success = false;
                int retry_max = 3;
                int retry_conut = 0;
                Http.HttpRes httpRes = null;
                while (!success && retry_conut < retry_max) {
                    if (retry_conut > 0) Thread.sleep(100);
                    String marketingUrl = String.format(
                            APIConfig.WEBAPI_MARKETING_ACTION_ELFIN.getUrl(),
                            DeviceInfo.hotelName, DeviceInfo.roomName, event_type);
                    // extend timeout to 10s for server cold start
                    httpRes = Http.get(marketingUrl, 10000, APIConfig.WEBAPI_MARKETING_ACTION_ELFIN.getKey());
                    success = (httpRes.responseCode == 200);
                    retry_conut++;
                }

                // data for view ( required )
                android.util.Log.d(TAG, "httpRes.response = " +httpRes.response);
                JSONObject viewObj = new JSONObject(httpRes.response).getJSONObject("view");
                int viewType = viewObj.getInt("type");
                switch (viewType) {
                    case LayoutType.ClassicCard:
                        ClassicCardLayout classicViewData = new Gson().fromJson(viewObj.toString(), ClassicCardLayout.class);
                        setLayout(viewObj.toString());
                        setcName(classicViewData.getTag());
                        setaClass(ClassicCardActivity.class);
                        break;
                    default:
                        throw new Exception("view type error");
                }

                // data for response ( required )
                String responseObj = new JSONObject(httpRes.response).getString("response");
                setChat_response(responseObj);

                // data for iot ( optional )
                try {
                    JSONArray iotObj = new JSONObject(httpRes.response).getJSONArray("iot");
                    JSONArray finalArray = new JSONArray();
                    for (int s = 0; s < iotObj.length(); s++) {
                        try {
                            JSONObject tmp = new JSONObject();
                            tmp.put("code", iotObj.getString(s));
                            finalArray.put(tmp);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    IoTModuleManager.getInstance().push_iot_intent_to_remoteService(finalArray, intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // data for action ( optional, try catch )
                try {
                    JSONObject mediaObj = new JSONObject(httpRes.response).getJSONObject("media");
                    String audio = mediaObj.getString("audio");

                    // play sound either from audio or tts resp
                    if (!audio.isEmpty()) {
                        MediaHolder mediaHolder = null;
                        // workaround, get data in the future
                        if (DeviceInfo.hotelID != 4299) {
                            // foreground music
                            mediaHolder = new MediaHolder(Instant.now().getEpochSecond() + "", MediaHolder.MARKETING, audio, true, "play", context);
                            PrimitiveAielloMediaPlayer.getInstance().enqueue(mediaHolder);
                        } else {
                            // background music
                            mediaHolder = new MediaHolder(Instant.now().getEpochSecond() + "", MediaHolder.MARKETING, audio, false, "play", context);
                            PrimitiveAielloMediaPlayer.getInstance().enqueue(mediaHolder);
                        }
                    } else {
                        set_intent_result(responseObj);  // set the result for tts playing
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            changeToUnknownIntentObject(context, e);
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        } finally {
            try {
                SpotUserTraceLog3.getInstance().buildVoiceEventLog(
                        SpotUserTraceLog3.EventSubject.MARKETING,
                        SpotUserTraceLog3.EventAction.QUERY,
                        this.querytext,
                        get_intent_result(),
                        ""
                );
            } catch (Exception e) {
                e.printStackTrace();
            }
            latch.countDown();
        }

    }


        @Override
    public int getCount() {
        return count;
    }

    @Override
    public void resetCount() {
        count = 0;
    }


    @Override
    public int getId() {
        return id;
    }

    private String layoutData;

    public String getLayoutData() {
        return layoutData;
    }

    public void setLayout(String layoutWrapper){
        this.layoutData = layoutWrapper;
    }


}
