package ai.aiello.aiellospot.intents.sdui;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import ai.aiello.aiellospot.core.device.DeviceControl;
import ai.aiello.aiellospot.core.media.AielloMediaPlayer;
import ai.aiello.aiellospot.core.media.MediaCompletedListener;
import ai.aiello.aiellospot.modules.iot.IoTModuleManager;

public class ActionExecutor {

    private static final String TAG = ActionExecutor.class.getSimpleName();
    private ActionJob actionJob;
    private ActionEventListener actionEventListener;
    private boolean working;
    private static ArrayList<String> activeUIDs = new ArrayList<>();

    ActionExecutor(String actionsData, ActionEventListener actionEventListener) throws Exception{
        this.actionJob = new ActionJob(actionsData);
        this.actionEventListener = actionEventListener;
    }

    public ActionJob getActionJob() {
        return actionJob;
    }

    void begin(){
        Log.d(TAG, actionJob.getId() + " begin session");
        working = true;
        activeUIDs.clear();
        try {
            JSONArray beginAction = actionJob.getBeginActions();
            int valueFromChatbot = actionJob.getChatbotValue();

            // keep active uid
            for (int i = 0; i < beginAction.length(); i++) {
                try {
                    JSONObject action = beginAction.getJSONObject(i);
                    String uid = action.getString("uid");
                    activeUIDs.add(uid);
                }catch (Exception e){

                }
            }


            for (int i = 0; i < beginAction.length(); i++) {
                try {
                    JSONObject action = beginAction.getJSONObject(i);
                    String execute = action.getString("execute");
                    String category = action.getString("category");
                    switch (category) {
                        case "media":
                            switch (execute) {
                                case "play":
                                    AielloMediaPlayer.getInstance().play(actionJob.getId(), i, action,
                                            // workaround, use player event as begin completed
                                            new MediaCompletedListener() {
                                                @Override
                                                public void onInterrupted() {
                                                    Log.d(TAG, "onInterrupted");
                                                    actionEventListener.onBeginActionInterrupted(ActionExecutor.this);
                                                }

                                                @Override
                                                public void onCompleted() {
                                                    Log.d(TAG, "onComplete");
                                                    actionEventListener.onBeginActionCompleted(ActionExecutor.this);
                                                }
                                            });
                                    break;
                                case "stop":
                                    AielloMediaPlayer.getInstance().stop(actionJob.getId());
                                    break;
                            }
                            break;

                        case "system":
                            DeviceControl.execute(execute);
                            break;

                        case "delay":
                            String delay_time = action.getString("value");
                            Thread.sleep(Integer.parseInt(delay_time));
                            break;

                        case "iot":
                            IoTModuleManager.getInstance().deliveryRCUAction(action.put("value", valueFromChatbot).toString());
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void end(boolean checkCurrent) {
        Log.d(TAG, actionJob.getId() + " end session, checkCurrent = " + checkCurrent);
        if (!working) return;
        working = false;
        try {
            AielloMediaPlayer.getInstance().stop(actionJob.getId());
            JSONArray endActions = actionJob.getEndActions();
            for (int i = 0; i < endActions.length(); i++) {
                try {
                    JSONObject action = endActions.getJSONObject(i);
                    String uid = action.getString("uid");
                    if (checkCurrent && activeUIDs.contains(uid)) continue;
                    String execute = action.getString("execute");
                    String category = action.getString("category");
                    switch (category) {
                        case "media":
                            switch (execute) {
                                case "play":
                                    AielloMediaPlayer.getInstance().play(actionJob.getId(), i, action, null);
                                    break;
                                case "stop":
                                    AielloMediaPlayer.getInstance().stop(actionJob.getId());
                                    break;
                            }
                            break;

                        case "system":
                            DeviceControl.execute(execute);
                            break;

                        case "delay":
                            String delay_time = action.getString("value");
                            Thread.sleep(Integer.parseInt(delay_time));
                            break;

                        case "iot":
                            IoTModuleManager.getInstance().deliveryRCUAction(action.put("value", -1).toString());
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } finally {
            actionEventListener.onEndActionCompleted(this);
        }
    }

    public boolean isForeground() {
        return actionJob.isForeground();
    }
}
