package ai.aiello.aiellospot.intents.items;

import android.content.Context;

import com.litesuits.android.log.Log;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.concurrent.CountDownLatch;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.clock.ClockActivity;
import ai.aiello.aiellospot.views.activity.home.MainHomeActivity;
import ai.aiello.aiellospot.views.activity.music.MusicPlayer3Activity;
import ai.aiello.aiellospot.intents.ISingleCount;
import ai.aiello.aiellospot.intents.IntentObject;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.modules.alarm.UserAlarmModuleManager;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.utlis.Formatter;

public class ViewControlIntent extends IntentObject implements ISingleCount {

    private static final String TAG = ViewControlIntent.class.getSimpleName();
    private static int count;
    private int id;

    public ViewControlIntent(String intent, String chat_response, String session, boolean end_of_dialog, String chatbot_data, String querytext) {
        super(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
        id = count++;
    }

    @Override
    public void execute(Context context, CountDownLatch latch) {
        String data = "";
        try {
            JSONObject jsonObject = new JSONObject(chatbot_data);
            switch (jsonObject.getString("type")) {
                case "music": //todo wait for chatbot implement
                    setaClass(MusicPlayer3Activity.class);
                    set_intent_result("");//不說話
                    set_user_log_response("Music");
                    data = "Music";
                    break;
                case "home": //todo wait for chatbot implement
                    setaClass(MainHomeActivity.class);
                    set_intent_result("");//不說話
                    set_user_log_response("Home");
                    data = "Home";
                    break;
                case "alarm":
                    //doc: https://cloud.google.com/dialogflow/docs/reference/system-entities
                    setaClass(ClockActivity.class);
                    String res = "";
                    try {
                        String startDateTime = "1970-08-15T23:59:59+08:00";
                        String endDateTime = "2500-08-15T23:59:59+08:00";
                        try {
                            JSONObject info = jsonObject.getJSONObject("info");
                            if (!info.getJSONObject("df_time_format").isNull("startDateTime") && !info.getJSONObject("df_time_format").isNull("endDateTime")) {
                                startDateTime = info.getJSONObject("df_time_format").getString("startDateTime");
                                endDateTime = info.getJSONObject("df_time_format").getString("endDateTime");
                            }
                        } catch (Exception e) {
                            Log.d(TAG, "no start and end time, default is all");
                        }
                        Date queryDateStart = Formatter.ISO8601DATEFORMAT.parse(startDateTime);
                        Date queryDateEnd = Formatter.ISO8601DATEFORMAT.parse(endDateTime);
                        queryDateStart.setSeconds(0);
                        queryDateEnd.setSeconds(0);

                        //沒有鬧鐘設定
                        if (UserAlarmModuleManager.getInstance().getAlarmMap().size() == 0) {
                            res = context.getString(R.string.alarm_query_empty_v3);
                        } else {
                            //有鬧鐘設定
                            //search and show result
                            ArrayList<Date> list = UserAlarmModuleManager.getInstance().isAlarmExistByPeriod(queryDateStart, queryDateEnd);
                            Collections.sort(list);
                            if (list.isEmpty()) {
                                if (queryDateStart.getTime() == queryDateEnd.getTime()) {
                                    //查詢某特定時間
                                    res = String.format(
                                            context.getString(R.string.alarm_query_no_found_v2),
                                            Formatter.getSpeechRelativeDay(queryDateStart),
                                            Formatter.getSpeechTimeDay(queryDateStart));
                                } else {
                                    //查詢一個區間
                                    res = context.getString(R.string.alarm_query_empty_v2);
                                }
                            } else {
                                if (list.size() > 1) {
                                    if (ChatApplication.system_lang == ChatApplication.Language.japanese) {
                                        for (Date d : list) {
                                            res += Formatter.getSpeechRelativeDay(d);
                                            res += Formatter.getSpeechTimeDay(d);
                                            if (list.indexOf(d) != list.size() - 1) {
                                                res += ",";
                                            }
                                        }
                                        res += String.format(context.getString(R.string.alarm_query_found_more_v2), list.size() + "");
                                    } else {
                                        res = String.format(context.getString(R.string.alarm_query_found_more_v2), list.size() + "");
                                        for (Date d : list) {
                                            res += Formatter.getSpeechRelativeDay(d);
                                            res += Formatter.getSpeechTimeDay(d);
                                            if (list.indexOf(d) != list.size() - 1) {
                                                res += ",";
                                            }
                                        }
                                    }
                                } else {
                                    res = String.format(
                                            context.getString(R.string.alarm_query_found_one_v2),
                                            Formatter.getSpeechRelativeDay(list.get(0)),
                                            Formatter.getSpeechTimeDay(list.get(0)));
                                }

                            }
                        }
                        set_intent_result(res);
                        set_user_log_response(get_intent_result());
                        data = "Alarm";
                    } catch (Exception e) {
                        e.getStackTrace();
                        changeToUnknownIntentObject(context, e);
                        data = "Alarm";
                    }
                    break;
                default:
                    throw new Exception("throw other intents");
            }
        } catch (Exception e) {
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            Log.e(TAG, e.toString());
            changeToUnknownIntentObject(context, e);
        } finally {
            SpotUserTraceLog3.getInstance().buildVoiceEventLog(
                    SpotUserTraceLog3.EventSubject.VIEW_CONTROL,
                    SpotUserTraceLog3.EventAction.QUERY,
                    this.querytext,
                    this.get_user_log_response(),
                    ""
            );
        }
        latch.countDown();
    }


    @Override
    public int getCount() {
        return count;
    }

    @Override
    public void resetCount() {
        count = 0;
    }


    @Override
    public int getId() {
        return id;
    }


}
