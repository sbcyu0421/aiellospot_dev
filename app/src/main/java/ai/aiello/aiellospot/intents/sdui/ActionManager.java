package ai.aiello.aiellospot.intents.sdui;

import android.util.Log;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * ActionExecutor need to check the state balance
 * Store cmd list and state by Intent
 * ( foreground, begin_action, end_action)
 */
public class ActionManager {

    private ArrayList<ActionExecutor> actionExecutors = new ArrayList<>();
    public static final String TAG = ActionManager.class.getSimpleName();
    private static ActionManager instance;
    private static ExecutorService executor = Executors.newSingleThreadExecutor();

    private ActionManager() {

    }

    public static ActionManager getInstance() {
        if (instance == null) {
            instance = new ActionManager();
        }
        return instance;
    }


    public void prepare(String actionsData, boolean autoStart) {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    ActionExecutor actionExecutor = new ActionExecutor(actionsData, new ActionEventListener() {
                        @Override
                        public void onBeginActionInterrupted(ActionExecutor actionExecutor) {
                            actionExecutor.end(true);
                        }

                        @Override
                        public void onBeginActionCompleted(ActionExecutor actionExecutor) {
                            actionExecutor.end(false);
                        }

                        @Override
                        public void onEndActionCompleted(ActionExecutor actionExecutor) {
                            actionExecutors.remove(actionExecutor);
                        }
                    });
                    if (autoStart) actionExecutor.begin();
                    actionExecutors.add(actionExecutor);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }


    public void stopAllAction() {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "stopAllAction");
                for (ActionExecutor actionWork : actionExecutors) {
                    actionWork.end(false);
                }
            }
        });

    }

    public void stopAction(ActionExecutor actionWork) {
        try {
            actionWork.end(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopForegroundAction() {
        executor.submit(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "stopForegroundAction");
                Iterator<ActionExecutor> iterator = actionExecutors.iterator();
                while (iterator.hasNext()) {
                    ActionExecutor actionExecutor = iterator.next();
                    if (actionExecutor.isForeground()) {
                        actionExecutor.end(false);
                    }
                }
            }
        });
    }

}
