package ai.aiello.aiellospot.intents.items;

import android.content.Context;

import java.util.concurrent.CountDownLatch;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.unknown.UnknownActivity;
import ai.aiello.aiellospot.intents.ISingleCount;
import ai.aiello.aiellospot.intents.IntentObject;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;

public class UnHandleIntent extends IntentObject implements ISingleCount {

    private static final String TAG = UnHandleIntent.class.getSimpleName();
    public static int count;
    private int id;

    public UnHandleIntent(String intent, String chat_response, String session, boolean end_of_dialog, String chatbot_data, String querytext) {
        super(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
        setaClass(UnknownActivity.class);
        id = count++;
    }

    @Override
    public void execute(Context context, CountDownLatch latch) {
        setcName(context.getResources().getString(R.string.unknown_intent));
        String s = context.getString(R.string.unknown_intent_msg);
        set_intent_result(s);

        SpotUserTraceLog3.getInstance().buildVoiceEventLog(
                SpotUserTraceLog3.EventSubject.UNHANDLE_TNTENT,
                SpotUserTraceLog3.EventAction.REPLY,
                this.querytext,
                this.get_user_log_response(),
                ""
        );
        latch.countDown();
    }

    public UnHandleIntent modify(Context context) {
        String s = context.getString(R.string.unknown_intent_msg);
        set_intent_result(s);
        return this;
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public void resetCount() {
        count = 0;
    }

    @Override
    public int getId() {
        return id;
    }


}
