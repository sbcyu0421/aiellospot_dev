package ai.aiello.aiellospot.intents.items;

import android.content.Context;

import com.litesuits.android.log.Log;

import java.util.concurrent.CountDownLatch;

import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.roobo.RooboActivity;
import ai.aiello.aiellospot.intents.IntentObject;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;

public class MessageIntent extends IntentObject {

    private static final String TAG = MessageIntent.class.getSimpleName();

    public MessageIntent(String intent, String chat_response, String session, boolean end_of_dialog, String chatbot_data, String querytext) {
        super(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
        this.setaClass(RooboActivity.class);
    }

    @Override
    public void execute(Context context, CountDownLatch latch) {
        SpotUserTraceLog3.EventSubject userLogSubject = SpotUserTraceLog3.EventSubject.NONE;
        SpotUserTraceLog3.EventAction userLogAction = SpotUserTraceLog3.EventAction.QUERY;
        switch (intent) {
            case "Roobo.TimeDates":
                this.setcName(context.getResources().getString(R.string.worldclock_intent));
                userLogSubject = SpotUserTraceLog3.EventSubject.WORLD_CLOCK;
                break;

            case "Roobo.Poetry":
                this.setcName(context.getResources().getString(R.string.poetry_intent));
                userLogSubject = SpotUserTraceLog3.EventSubject.CHAT;
                break;

            case "Roobo.Joke":
                this.setcName(context.getResources().getString(R.string.joke_intent));
                userLogSubject = SpotUserTraceLog3.EventSubject.JOKE;
                break;

            case "Roobo.Star":
            case "Roobo.Baike":
                this.setcName(context.getResources().getString(R.string.star_intent));
                userLogSubject = SpotUserTraceLog3.EventSubject.CONSTELLATION;
                break;
            case "Module.NotSupport":
                this.setcName(context.getResources().getString(R.string.not_support));
                userLogSubject = SpotUserTraceLog3.EventSubject.MODULE_NOT_SUPPORT;
                break;
        }

        try {
            setMsg1(getcName());
            setMsg2(getChat_response());
            set_intent_result(getChat_response());
            set_user_log_response(get_intent_result());
            Log.d(TAG, get_intent_result());
            if (getChat_response().length() == 0) {
                throw new Exception("callback json no response value error");
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            changeToUnknownIntentObject(context, e);
            SpotDeviceLog.exception(TAG, "", e.getMessage());
        } finally {
            SpotUserTraceLog3.getInstance().buildVoiceEventLog(
                    userLogSubject,
                    userLogAction,
                    this.querytext,
                    this.get_user_log_response(),
                    ""
            );
        }
        latch.countDown();
    }


}
