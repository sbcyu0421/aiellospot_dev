package ai.aiello.aiellospot.intents.sdui;

import org.json.JSONObject;

import java.util.Locale;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.core.config.APIConfig;
import ai.aiello.aiellospot.utlis.Http;
import ai.aiello.chinese.ChineseUtils;

public class ServerDrivenHelper {

    private static final String TAG = ServerDrivenHelper.class.getSimpleName();

    public synchronized static ServerMessage parse(String agentData) throws Exception {
        return new ServerMessage(agentData);
    }

    public synchronized static ServerMessage request(String hotel_name, String room_type, String unikey, int chatbot_value) throws Exception {
        boolean success = false;
        int retry_max = 3;
        int retry_conut = 0;

        Http.HttpRes httpRes = null;
        JSONObject jsonBody = new JSONObject();
        jsonBody.put("hotel_name", hotel_name);
        jsonBody.put("room_type", room_type);
        jsonBody.put("unikey", unikey);
        jsonBody.put("chatbot_value", chatbot_value);
        while (!success && retry_conut < retry_max) {
            if (retry_conut > 0) Thread.sleep(100);
            httpRes = Http.post(APIConfig.WEBAPI_INTENT_ACTION.getUrl(), jsonBody, 5000, APIConfig.WEBAPI_INTENT_ACTION.getKey());
            success = (httpRes.responseCode == 200);
            retry_conut++;
        }

        android.util.Log.d(TAG, "httpRes.response = " + httpRes.response);
        return new ServerMessage(httpRes.response);
    }

    public static class ServerMessage {
        String resultAction;
        Resp resp;

        ServerMessage(String resultAction) {
            this.resultAction = resultAction;
            this.resp = new Resp(resultAction);
        }

        public String getResultAction() {
            return resultAction;
        }

        public Resp getResp() {
            return resp;
        }

        public static class Resp {
            String viewRawData;

            Resp(String viewRawData) {
                this.viewRawData = viewRawData;
            }

            public String getContentMessage() {
                String response = "";
                try {
                    JSONObject view = new JSONObject(viewRawData).getJSONObject("data").getJSONObject("view");
                    if (ChatApplication.locale == Locale.CHINA) {
                        response = ChineseUtils.toTraditional(view.getString("zh"), true);
                    } else if (ChatApplication.locale == Locale.TAIWAN) {
                        response = view.getString("zh");
                    } else if (ChatApplication.locale == Locale.JAPANESE) {
                        response = view.getString("ja");
                    } else {
                        response = view.getString("en");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response;
            }
        }
    }

}
