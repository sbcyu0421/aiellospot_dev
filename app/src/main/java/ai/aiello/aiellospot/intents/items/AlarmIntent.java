package ai.aiello.aiellospot.intents.items;

import android.content.Context;

import com.litesuits.android.log.Log;

import org.json.JSONObject;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.views.activity.clock.ClockActivity;
import ai.aiello.aiellospot.intents.ISingleCount;
import ai.aiello.aiellospot.intents.IntentObject;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.modules.alarm.UserAlarmModuleManager;

import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.utlis.Formatter;

public class AlarmIntent extends IntentObject implements ISingleCount {

    private static final String TAG = AlarmIntent.class.getSimpleName();
    private static int count;
    private int id;

    public AlarmIntent(String intent, String chat_response, String session, boolean end_of_dialog, String chatbot_data, String querytext) {
        super(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
        this.setaClass(ClockActivity.class);
        id = count++;
    }


    @Override
    public void execute(Context context, CountDownLatch latch) {
        this.setcName(context.getString(R.string.alarm_intent));
        SpotUserTraceLog3.EventAction userLogAction = SpotUserTraceLog3.EventAction.NONE;
        try {
            JSONObject object = new JSONObject(chatbot_data);
            String alarm_control = object.getString("Alarm-Control");
            switch (alarm_control) {
                case "setup":
                    setup(context);
                    userLogAction = SpotUserTraceLog3.EventAction.SETUP;
                    break;
                case "cancel":
                    cancel(context);
                    userLogAction = SpotUserTraceLog3.EventAction.DELETE;
                    break;
                case "failure":
                    set_user_log_response("get failure form from chatbot in alarm intent");
                    throw new Exception("get failure form from chatbot in alarm intent");
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            changeToFormatErrorIntentObject(context.getString(R.string.alarm_failure_error), e);
            userLogAction = SpotUserTraceLog3.EventAction.UNKNOWN;
        } finally {
            SpotUserTraceLog3.getInstance().buildVoiceEventLog(
                    SpotUserTraceLog3.EventSubject.CLOCK,
                    userLogAction,
                    this.querytext,
                    this.get_user_log_response(),
                    ""
            );
        }
        latch.countDown();
    }


    @Override
    public int getId() {
        return id;
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public void resetCount() {
        count = 0;
    }

    private void setup(Context context) {
        try {
            JSONObject object = new JSONObject(chatbot_data);
            String startDateTime = object.getJSONObject("df_time_format").getString("startDateTime");
            String endDateTime = object.getJSONObject("df_time_format").getString("endDateTime");

            Date dateStart = Formatter.ISO8601DATEFORMAT.parse(startDateTime);
            Date dateEnd = Formatter.ISO8601DATEFORMAT.parse(endDateTime);
            dateStart.setSeconds(0);
            dateEnd.setSeconds(0);

            //不允許設定區間
            if (dateStart.getTime() != dateEnd.getTime()) {
                changeToFormatErrorIntentObject(context.getString(R.string.alarm_setup_not_support), new Exception("not a certain time"));
                return;
            }

            //不允許過去鬧鈴
            Calendar calendar_now = Calendar.getInstance();
            Date date_now = calendar_now.getTime();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateStart);
            if (calendar.compareTo(calendar_now) < 0) {
                set_intent_result(context.getString(R.string.set_alarm_fail_pass));
                set_user_log_response(get_intent_result());
                return;
            }

            //於每日凌晨0-~3點，設定明天鬧鈴時，視為今天的鬧鈴
            if (date_now.getHours() >= 0 && date_now.getHours() < 3 && date_now.getDate() + 1 == dateStart.getDate()) {
                dateStart.setDate(date_now.getDate());
                calendar.setTime(dateStart);
                Log.e(TAG, "dead zone, reset date to current date");
//                return;
            }

            boolean success = UserAlarmModuleManager.getInstance().addAlarm(dateStart);
            if (success) {
                String msg = null;
                if (ChatApplication.locale.equals(Locale.ENGLISH)) {
                    String[] shortMonths = new DateFormatSymbols().getMonths();
                    msg = "setup alarm clock to " + shortMonths[dateStart.getMonth()] + " " + (dateStart.getDate()) + " " + Formatter.binaryFormat(dateStart.getHours()) + ":" + Formatter.binaryFormat(dateStart.getMinutes());
                } else if (ChatApplication.locale.equals(Locale.JAPANESE)) {
                    msg = Formatter.binaryFormat(dateStart.getMonth() + 1) + "月" + Formatter.binaryFormat(dateStart.getDate()) + "日" + Formatter.binaryFormat(dateStart.getHours()) + "時" + Formatter.binaryFormat(dateStart.getMinutes()) + "分のアラームを設定いたしました";
                } else {
                    msg = "已為您設定" + Formatter.binaryFormat(dateStart.getMonth() + 1) + "月" + Formatter.binaryFormat(dateStart.getDate()) + "日" + Formatter.binaryFormat(dateStart.getHours()) + "點" + Formatter.binaryFormat(dateStart.getMinutes()) + "分的鬧鈴";
                }
                UserAlarmModuleManager.getInstance().saveToPreference(context);
                set_intent_result(msg);
                set_user_log_response(get_intent_result());
            } else {
                Log.d(TAG, context.getString(R.string.set_alarm_fail_full));
                set_intent_result(context.getString(R.string.set_alarm_fail_full));
                set_user_log_response(get_intent_result());
            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            changeToFormatErrorIntentObject(context.getString(R.string.alarm_setup_error), e);
        }
    }

    private void cancel(Context context) throws Exception {
        //關閉正在響的鬧鐘
        if (UserAlarmModuleManager.getInstance().isAlarmRing()) {
            UserAlarmModuleManager.getInstance().stopRing("user stop alarm by voice");
            UserAlarmModuleManager.getInstance().hideAlarmPopup(2);
            set_intent_result(context.getString(R.string.stop_alarm));
            set_user_log_response(get_intent_result());
            return;
        }
        try {
            //沒有任何鬧鐘設定
            if (UserAlarmModuleManager.getInstance().getFreeAlarm() == UserAlarmModuleManager.getInstance().alarmLimit) {
                set_intent_result(context.getString(R.string.no_alarm));
                set_user_log_response(get_intent_result());
                return;
            }

            JSONObject object = new JSONObject(chatbot_data);
            //刪除所有鬧鐘
            if (object.getString("Entity-Select-All").equals("all")) {
                set_intent_result(context.getString(R.string.cancel_alarm_success));
                set_user_log_response(get_intent_result());
                UserAlarmModuleManager.getInstance().removeAllAlarm();
                UserAlarmModuleManager.getInstance().saveToPreference(context);
                return;
            }
            //刪除區間鬧鐘
            String startDateTime = object.getJSONObject("df_time_format").getString("startDateTime");
            String endDateTime = object.getJSONObject("df_time_format").getString("endDateTime");
            Date dateStart = Formatter.ISO8601DATEFORMAT.parse(startDateTime);
            Date dateEnd = Formatter.ISO8601DATEFORMAT.parse(endDateTime);
            dateStart.setSeconds(0);
            dateEnd.setSeconds(0);
            boolean response = UserAlarmModuleManager.getInstance().removeAlarmByPeriod(dateStart, dateEnd, context);
            if (response) {
                set_intent_result(getChat_response());
                set_user_log_response(get_intent_result());
                UserAlarmModuleManager.getInstance().saveToPreference(context);
            } else {
                if (dateStart.getTime() == dateEnd.getTime()) {
                    String res = String.format(
                            context.getString(R.string.no_alarm_found_certain),
                            Formatter.getSpeechDayAndTimeFormat(dateStart));
                    set_intent_result(res);
                    set_user_log_response(get_intent_result());
                } else {
                    String res = String.format(
                            context.getString(R.string.no_alarm_found_period),
                            Formatter.getSpeechDayAndTimeFormat(dateStart),
                            Formatter.getSpeechDayAndTimeFormat(dateEnd));
                    set_intent_result(res);
                    set_user_log_response(get_intent_result());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            SpotDeviceLog.exception(TAG, "", e.getMessage());
            changeToFormatErrorIntentObject(context.getString(R.string.alarm_cancel_error), e);
        }
    }

}
