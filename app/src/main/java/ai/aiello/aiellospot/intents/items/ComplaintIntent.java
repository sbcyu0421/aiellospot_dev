package ai.aiello.aiellospot.intents.items;

import android.content.Context;

import com.litesuits.android.log.Log;

import org.json.JSONObject;

import java.util.concurrent.CountDownLatch;

import ai.aiello.aiellospot.ChatApplication;
import ai.aiello.aiellospot.R;
import ai.aiello.aiellospot.core.config.APIConfig;
import ai.aiello.aiellospot.core.config.Configer;
import ai.aiello.aiellospot.core.info.DeviceInfo;
import ai.aiello.aiellospot.core.info.UserInfo;
import ai.aiello.aiellospot.core.log.SpotDeviceLog;
import ai.aiello.aiellospot.core.log.SpotUserTraceLog3;
import ai.aiello.aiellospot.intents.ISingleCount;
import ai.aiello.aiellospot.intents.IntentObject;
import ai.aiello.aiellospot.utlis.Http;
import ai.aiello.aiellospot.views.activity.complain.ComplainActivity;

public class ComplaintIntent extends IntentObject implements ISingleCount {

    private static final String TAG = ComplaintIntent.class.getSimpleName();
    private static int count;
    private int id;

    public ComplaintIntent(String intent, String chat_response, String session, boolean end_of_dialog, String chatbot_data, String querytext) {
        super(intent, chat_response, session, end_of_dialog, chatbot_data, querytext);
        id = count++;
        specialIntent = 0;
    }

    @Override
    public void execute(Context context, CountDownLatch latch) {
        try {
            String type = new JSONObject(chatbot_data).getString("Complaint");
            String querytext = getQuerytext();
            switch (type){
                case "message":
                    this.setcName(context.getString(R.string.message_intent));
                    this.setaClass(ComplainActivity.class);
                    sendGuestMessage(querytext, ChatApplication.dbLanguageCode, type);
                    break;
                case "complaint":
                default:
                    this.setcName(context.getString(R.string.complain_intent));
                    this.setaClass(ComplainActivity.class);
                    sendComplaint(querytext, ChatApplication.dbLanguageCode, type);
                    break;
            }
            String response = context.getString(R.string.complain_response);
            Log.d(TAG, "post querytext = " + querytext);
            set_intent_result(response);
            set_user_log_response(get_intent_result());
            setMsg1(response);
        } catch (Exception e) {
            e.printStackTrace();
            changeToUnknownIntentObject(context, e);
        }
        finally {
            SpotUserTraceLog3.getInstance().buildVoiceEventLog(
                    SpotUserTraceLog3.EventSubject.COMPLAINT,
                    SpotUserTraceLog3.EventAction.QUERY,
                    this.querytext,
                    this.get_user_log_response(),
                    ""
            );
        }
        latch.countDown();
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public void resetCount() {
        count = 0;
    }

    @Override
    public int getId() {
        return id;
    }


    public void sendGuestMessage(String queryText, String locale, String type) {
        ChatApplication.worker.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    APIConfig.WEBAPI guestMessageApi = APIConfig.WEBAPI.build(Configer.moduleConfigData.getJSONObject("comments")
                            .getJSONObject("service_data"), Configer.useProductAPI);
                    int count = 0, maxTry = 5;
                    boolean success = false;
                    while (!success && count < maxTry) {
                        String apiUrl = String.format(guestMessageApi.getUrl(), DeviceInfo.hotelName);
                        JSONObject jsonBody = new JSONObject().put("room", DeviceInfo.roomName).put("message", queryText);
                        Http.HttpRes httpRes = Http.post(apiUrl, jsonBody, 5000, guestMessageApi.getKey());
                        if (httpRes.responseCode / 100 == 2) success = true;
                        else {
                            // retry
                            Thread.sleep(200);
                            count++;
                        }
                    }
                } catch (Exception e) {
                    SpotDeviceLog.exception(TAG, "", "sendComplaint" + e.getMessage());
                    Log.e(TAG, "sendComplaint error: " + e.toString());
                }
            }
        });
    }


    public void sendComplaint(String queryText, String locale, String type) {
        ChatApplication.worker.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    APIConfig.WEBAPI complaintApi = APIConfig.WEBAPI.build(Configer.moduleConfigData.getJSONObject("complaint").getJSONObject("service_data"), Configer.useProductAPI);
                    int count = 0, maxTry = 5;
                    boolean success = false;
                    while (!success && count < maxTry) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("Content", queryText);
                        jsonObject.put("Language", locale);
                        jsonObject.put("Device", DeviceInfo.MAC);
                        jsonObject.put("Guest", UserInfo.userName);
                        jsonObject.put("Gender", UserInfo.userGender);
                        jsonObject.put("Nation", UserInfo.userNationality);
                        jsonObject.put("Type", type);
                        String url = String.format(complaintApi.getUrl(), DeviceInfo.hotelName, DeviceInfo.roomName);
                        Http.HttpRes httpRes = Http.post(url, jsonObject, 5000, complaintApi.getKey());
                        if (httpRes.responseCode / 100 == 2) success = true;
                        else {
                            // retry
                            Thread.sleep(200);
                            count++;
                        }
                    }
                } catch (Exception e) {
                    SpotDeviceLog.exception(TAG, "", "sendComplaint" + e.getMessage());
                    Log.e(TAG, "sendComplaint error: " + e.toString());
                }
            }
        });
    }

}
