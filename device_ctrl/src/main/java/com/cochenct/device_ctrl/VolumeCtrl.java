package com.cochenct.device_ctrl;

import android.content.Context;
import android.media.AudioManager;
import com.litesuits.android.log.Log;

public class VolumeCtrl {
    private AudioManager audioManager;
    private int audioType_MUSIC = AudioManager.STREAM_MUSIC;
    private int audioType_RING = AudioManager.STREAM_RING;
    private int audioType_ALARM = AudioManager.STREAM_ALARM;
    private static String TAG = VolumeCtrl.class.getSimpleName();
    private int offset = 3;

    public VolumeCtrl(Context context) {
        audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        getMusicMaxVolume();
        getRingMaxVolume();

    }

    private int getMusicMaxVolume() {
        int max = audioManager.getStreamMaxVolume(audioType_MUSIC);
        Log.d(TAG, "MUSIC_maxVolume=" + max);
        return max;
    }

    private int getRingMaxVolume() {
        int max = audioManager.getStreamMaxVolume(audioType_RING);
        Log.d(TAG, "RING_maxVolume=" + max);
        return max;
    }

    public int getCurrentVolume() {
        int currentVolume = audioManager.getStreamVolume(audioType_MUSIC);
        Log.d(TAG, "SystemVol=" + currentVolume);
        if (currentVolume >= 1) {
            currentVolume = currentVolume + offset;
        }
        return currentVolume;
    }

    public void setMute(boolean m) {
        audioManager.adjustVolume(m ? AudioManager.ADJUST_MUTE : AudioManager.ADJUST_UNMUTE, AudioManager.FLAG_SHOW_UI);
    }

//    public int setVolume(int v) {
//        v = v < 0 ? 0 : v;
//        v = v > getMusicMaxVolume() ? getMusicMaxVolume() : v;
//        audioManager.setStreamVolume(audioType_MUSIC, v, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
//        //audioManager.setStreamVolume(audioType_MUSIC, v, 0);
//        return getCurrentVolume();
//    }


    public int setVolume(int v) {
        v = v < 0 ? 0 : v;
        if (v >= 1) {
            v = v + offset;
        }
        audioManager.setStreamVolume(audioType_MUSIC, v, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
        audioManager.setStreamVolume(audioType_RING, v/2, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
        return getCurrentVolume();
    }

}
