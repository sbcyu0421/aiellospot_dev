package com.cochenct.device_ctrl;


import android.content.Context;
import android.provider.Settings;

import com.litesuits.android.log.Log;

public class BrightCtrl {
    private Context contex;
    int backLightValue = -1;
    private String TAG = BrightCtrl.class.getSimpleName();
    private final static int offsetBrightness = 20;

    public BrightCtrl(Context c) {
        contex = c;
    }

    public int getBrightness() {

        try {
            backLightValue = android.provider.Settings.System.getInt(
                    contex.getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "BrightCtrl backLightValue=" + backLightValue);
        if (backLightValue == -1) {
            backLightValue = 50;
        } else {
            backLightValue += offsetBrightness;
        }

        return backLightValue;
    }

    public int setBrightness(int val) {
        // val: 0~100
        if (backLightValue == 0 && val != 0) {
            for (int i = 0; i < 2; i ++) {
                android.provider.Settings.System.putInt(contex.getContentResolver(), android.provider.Settings.System.SCREEN_BRIGHTNESS, i);
                try {
                    Thread.sleep(150);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        if (val != 0) {
            backLightValue = val < 0 ? 0 : val;
            backLightValue = val > 150 ? 150 : val;
            backLightValue += offsetBrightness;
        } else {
            backLightValue = 0;
        }

        android.provider.Settings.System.putInt(contex.getContentResolver(), android.provider.Settings.System.SCREEN_BRIGHTNESS, backLightValue);
        android.util.Log.d(TAG, "backLightValue: " + backLightValue );
        return backLightValue;
    }

}
