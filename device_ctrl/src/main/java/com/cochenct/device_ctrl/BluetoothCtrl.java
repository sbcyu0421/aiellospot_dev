package com.cochenct.device_ctrl;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import com.litesuits.android.log.Log;

import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Set;


public class BluetoothCtrl {

    private BluetoothAdapter mBluetoothAdapter;
    private static String TAG = BluetoothCtrl.class.getSimpleName();
    private Context wpc;


    public BluetoothCtrl(Context context) {
        wpc = context;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    public boolean isEnable() {
        return mBluetoothAdapter.isEnabled();
    }


    public String getBondedDevices() {
        Set<BluetoothDevice> devices = mBluetoothAdapter.getBondedDevices();
        if (devices != null && devices.size() > 0) {
            BluetoothDevice device = (BluetoothDevice) devices.iterator().next();
            return device.getName();
        } else {
            return null;
        }
    }

    private void removeBondDevice(){
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                try {
                    Method m = device.getClass()
                            .getMethod("removeBond", (Class[]) null);
                    m.invoke(device, (Object[]) null);
                } catch (Exception e) {
                    Log.e("Removing has been failed.", e.getMessage());
                }
            }
        }
    }


    public String getBluetoothName() {
        String n = "bt not init";
        mBluetoothAdapter.getName();
        if (mBluetoothAdapter.getName() != null) {
            n = mBluetoothAdapter.getName();
        }
        return n;
    }

    public void enableBT(String name) {
        if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        } else {
            Log.d(TAG,"already enable, just open discover mode");
            discover(name);
        }

    }


    public void disableBT() {
        mBluetoothAdapter.disable();
        removeBondDevice();
    }


    public void discover(String name) {
        // reset discover
        if (mBluetoothAdapter.isDiscovering()) {
            mBluetoothAdapter.cancelDiscovery();
        }
        // start discover
        // mBluetoothAdapter.startDiscovery();
        mBluetoothAdapter.setName(name);
        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0);
        wpc.startActivity(discoverableIntent);
        Log.d(TAG, "startDiscovery()");
    }


}
