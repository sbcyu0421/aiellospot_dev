## **A. Spot flow chart:**
![](https://i.imgur.com/lq2aXay.png)
  
1. **Configurate 2 main data** : 
    a. SpotConfig data which contains version of applications, hotel id, service info (API urls included), etc...,  
    b. DeviceInfo which contains device detail info and the room info where the device are at.  
2. **Version update by OTA** if version of application dose not match the latest one.
3. **Select locale** at Welcome page to setup system language and features that requires locale to activate, then enter homepage.
4. Homepage of spot await for inputs to start activate actions.
5. **2 type of inputs** are accepted by spot (Voice and UI), though handled differently but the intent are both send and understand by **Agent**.
6. **Agent** accepts inputs to understand and handles the Intent.  Connecting to **Chat-bot** to understand the intent, DBs for information the intent requires and come up with a result.  The result were parsed by Spot then send to **View and Module**.
7. **Modules** are of all kind of services, features in spot, which contains functions and data.  With a given standard to follow, they have identical lifecycle and functions.  By following the standard, new module will have no problem interacting with existing ones.
8. **View** are UI display on spot.



---




## **B. Eventbus and Modules:**


![](https://i.imgur.com/laqLNBE.png)
  
  
1. **2 types of Events in Eventbus** that effects on different scope.  
    a. System event: effects on the state of spot CORE, such as hardware state or system state (see graph for examples). Types of system events are fixed so should not be append or altered  
    
    b. Module event: effects on the state that only relative to themselves.  This event is mostly listen by Views for UI display.  Feel free to add a new module event.  
    
2. **Module**  
    SHOULD :  Send and listen to System Event that relative to it.  
    Can : Send a module event.  
    SHOULD NOT :  Listen to other module event.  
    
3. **View**  
    SHOULD : Listen to System Event that relative to it.  
    Can : Listen to a module event.  
    SHOULD NOT :  Send either system and module event.  
  

---

  
  
  
## **C. Starting a new module**
![](https://i.imgur.com/b4AOgO4.png)
  
  
1. **Example** of Creating ModuleManager:
    a. create your **ModuleManager** class

```
public class HappyTimeModuleManager extends AbstractModuleManager {

    public static HappyTimeModuleManager getInstance() {}   // make it a singleton
    private HappyTimeModuleManager () {}

    @Override
    public void create() {  // get all required data for this module besides those requires locale
        api = APIConfig.WEBAPI
                .build(Configer.moduleConfigData.getJSONObject("happytime").getJSONObject("service_data"), Configer.useProductAPI);
    }

    @Override
    public void onStart() {  // get remain data which requires locale
        fetchHappyTimeMenuWithLocale(locale, month);
    }

    @Override
    onHotReload() {  //  reset "ALL" data
        create();
        onStart();
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)  // listen to the EventBus
    public void onMessageEvent(SystemEvent event) {
        super.onMessageEvent(event);
        switch (event.getType()) {
            case 1:
            case 2:
        }
    }

    public void sendSomeMessage() {  // send to the EventBus
        EventBus.getDefault().post(new Event)
    }
}
```

  ** remember to add event types for your Module in aiellospot/events/module/newStateChange
  
  
b. Then go to "Main" **ModuleManager** (the manager of all moduleManagers), add your manager in **createModules()** method, which will start creating your module manager when Spot starts.

```
public static boolean createModules() {
    AdsModuleManager.getInstance().create();
}
```
  
  

---

  
  
## D. Customizing your Intent parser / IntentObject
![](https://i.imgur.com/LkN2DqL.png)
  
  
1. **Example** of creating a new Intent parser (intent object)  

    a. Create your Intent parser:

```
class HappyTimeIntent extends IntentObject {
    public HappyTimeIntent (String intent, String chat_response, String session, boolean   end_of_dialog, String chatbot_data, String querytext, String agentActions) {}
 
    @Override
        public void execute() {  // get remain data which requires locale
    // 1. Add your logic
    
    // 2. Setup SpotUserLog (a report mechanism in Spot)
        SpotUserTraceLog3.getInstance().buildVoiceEventLog(
            SpotUserTraceLog3.EventSubject.HappyTime,  // put in the intent type
            SpotUserTraceLog3.EventAction.UNKNOWN;  // describe the action
            this.querytext,  // the question/request sentence from user, should be in the intent
            this.get_user_log_response(),  // the response sentence from chatbot, also in intent
            data  // some extra data wasn't included above but is important
        )

    // 3. End the execution (tell CountDownLatch that this intent is done)
        latch.countDown();
    }
}
```
  


---


  
  
   b. Add your intent in "Classifier"

```
switch (intent) {
    case "YourIntentName":
    io = new YourIntent(intent, chat_response, session, end_of_dialog, chatbot_data, querytext) 
}
```
  
  
  

---

  
## **E. Future plan**  
  
**1. Structure**
![](https://i.imgur.com/nHjzEcO.png)

**2. Integrate local agent(for UI input) and cloud agent(for Voice input) to form united intent data**, then can be parse by same agent parser.