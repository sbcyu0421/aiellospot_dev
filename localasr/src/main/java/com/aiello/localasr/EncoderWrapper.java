package com.aiello.localasr;

import android.util.Log;

import com.google.common.collect.ImmutableList;

import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaFormat;
import java.io.IOException;

public class EncoderWrapper {
    static {
        System.loadLibrary("ogg_opus_encoder");
    }
    public enum CodecAndBitrate {
        // For FLAC, the bitrate isn't specified.

        // Note: Opus isn't actually limited to specific bitrates like AMRWB is.
        //
        // Note that because we run the OggOpusEncoder in low-latency mode, the
        // actual bitrate may be larger than this. See the ogg_opus_encoder lib for
        // details.
        //
        // The effect of this will be larger at low bitrates and low block
        // sizes. This is the bitrate used to configure the codec.
        //OGG_OPUS_BITRATE_12KBPS = 12000,
        //OGG_OPUS_BITRATE_16KBPS = 16000,
        //OGG_OPUS_BITRATE_24KBPS = 24000,
        //OGG_OPUS_BITRATE_32KBPS = 32000,
        //OGG_OPUS_BITRATE_64KBPS = 64000,
        //OGG_OPUS_BITRATE_96KBPS = 96000,
        //OGG_OPUS_BITRATE_128KBPS = 128000,
    }
    public static class EncoderException extends Exception {
        public EncoderException(String message) {
            super(message);
        }
    }

    public interface StreamingAudioInternalEncoder {
        void init(int sampleRateHz, int codecAndBitrate, boolean useVbr)
                throws EncoderException, IOException;

        byte[] processAudioBytes(byte[] input, int offset, int length);

        byte[] flushAndStop();
    }
    public static class OggOpusEncoder implements StreamingAudioInternalEncoder {
        // This is a pointer to the native object that we're working with. Zero when unallocated.
        static {
            System.loadLibrary("ogg_opus_encoder");
        }
        private long instance = 0;

        ImmutableList<Integer> validSampleRates = ImmutableList.of(8000, 12000, 16000, 24000, 48000);
        public OggOpusEncoder() {}

        @Override
        public void init(int sampleRateHz, int codecAndBitrate, boolean allowVbr)
                throws EncoderException {
            if (instance != 0) {
                flushAndStop();
            }

            if (!validSampleRates.contains(sampleRateHz)) {
                throw new EncoderException(
                        "Opus encoder requires a sample rate of 8kHz, 12kHz, 16kHz, 24kHz, or 48kHz.");
            }
            this.instance =
                    init(1 /* Mono audio. */, 24000, sampleRateHz, allowVbr);
        }

        private native long init(int channels, int bitrate, int sampleRateHz, boolean allowVbr);

        @Override
        public byte[] processAudioBytes(byte[] bytes, int offset, int length) {
            if (instance != 0) {
                return processAudioBytes(instance, bytes, offset, length);
            }
            return null;
        }

        private native byte[] processAudioBytes(long instance, byte[] samples, int offset, int length);

        /**
         * Complete the input stream, return any remaining bits of the output stream, and stop.
         * This should only be called once. Must be called after init().
         *
         * @return bytes of compressed audio
         */
        @Override
        public byte[] flushAndStop() {
            if (instance != 0) {
                byte[] flushedBytes = flush(instance);
                free(instance);
                instance = 0;
                return flushedBytes;
            } else {
                Log.e("OggEncoder","stop() called multiple times or without call to init()!");
                return new byte[0];
            }
        }

        @Override
        protected void finalize() throws Throwable {
            super.finalize();
            if (instance != 0) {
                Log.e("OggEncoder",
                        "Native OggOpusEncoder resources weren't cleaned up. You must call stop()!");
                free(instance);
            }
        }

        private native byte[] flush(long instance);
        private native void free(long instance);
    }
}
