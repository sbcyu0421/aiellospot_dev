package com.aiello.localasr;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;

import com.litesuits.android.log.Log;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ASRClientService implements ASRClientListener {

    private static String TAG = ASRClientService.class.getSimpleName();

    private AielloInterfaceListener listener;
    private GoogleStreamClient gclient;
    private XfyunClient xfyunClient;

    private SignalRASRClient signalrClient;
    private ASRModel currentModel = ASRModel.Google;
    private boolean advanced;
    private boolean isEmpty;
    private String mi_url;
    private String mlang;
    private int vadTimes = 1;
    private static Thread vadMonitor;
    private Handler handler;
    private JSONObject asrObject;
    public static String googleAccent = "";

    enum ASRModel {
        Google(1),
        Baidu(2),
        SignalR(3),
        Xfyun(4);

        private int index;

        ASRModel(int index) { this.index = index; }

        public static ASRModel getASRModel(int modelIndex) {
            for (ASRModel m : ASRModel.values()) {
                if (m.index == modelIndex) return m;
            }
            return Google; // circular end, then go to first one
        }

    }

    public ASRClientService(Context context, String mi_url,
                            JSONObject asrObject,
                            String config_google, String config_xfyun, AielloInterfaceListener Listener,String hotelname,String roomname, String accentMap) {
        this.listener = Listener;
        this.mi_url = mi_url;
        this.asrObject = asrObject;
        HandlerThread handlerThread = new HandlerThread("ASRClientService");
        handlerThread.start();
        this.handler = new Handler(handlerThread.getLooper());
        gclient = new GoogleStreamClient(16000, config_google, this);
        xfyunClient = new XfyunClient(context, config_xfyun, this);

        signalrClient = new SignalRASRClient(this,config_google,hotelname,roomname);
        setASRModel(asrObject);
        initAccentSupportMap(accentMap);
    }

    public void setASRModel(JSONObject asrObject) {
        try {
            currentModel = ASRModel.valueOf(asrObject.getString("model"));
            android.util.Log.d(TAG, "setASRModel: " + currentModel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * circular switch asr model
     * @return current ASR Model Ordinal
     */
    public int switchASRModel() {
        currentModel = ASRModel.getASRModel(currentModel.index + 1);
        android.util.Log.d(TAG, "switchASRModel to : " + currentModel.name());
        return currentModel.index;
    }

    /**
     * Google, Xfyun, SignalR
     * @return
     */
    public String getCurrentModel() {
        return currentModel.name();
    }

    private List<String> getEnhancePhrases(String lang) {
        // merge global and hotel custom phrase
        List<String> phrases = new ArrayList<>();
        android.util.Log.d(TAG, "getEnhancePhrases by lang = " + lang);
        if (lang.equals("zh_TW") || lang.equals("zh_CN")) {
            try {
                JSONArray enhanceWords = asrObject.getJSONObject("enhance_phrases_global").getJSONArray("tw");
                for (int i = 0; i < enhanceWords.length(); i++) {
                    phrases.add(enhanceWords.getString(i));
                }
                JSONArray enhanceWordsCustom = asrObject.getJSONObject("enhance_phrases_custom").getJSONArray("tw");
                for (int i = 0; i < enhanceWordsCustom.length(); i++) {
                    phrases.add(enhanceWordsCustom.getString(i));
                }
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }
        } else if (lang.equals("en_US")) {
            try {
                JSONArray enhanceWords = asrObject.getJSONObject("enhance_phrases_global").getJSONArray("en");
                for (int i = 0; i < enhanceWords.length(); i++) {
                    phrases.add(enhanceWords.getString(i));
                }
                JSONArray enhanceWordsCustom = asrObject.getJSONObject("enhance_phrases_custom").getJSONArray("en");
                for (int i = 0; i < enhanceWordsCustom.length(); i++) {
                    phrases.add(enhanceWordsCustom.getString(i));
                }
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }
        } else if (lang.equals("japanese")) {
            try {
                JSONArray enhanceWords = asrObject.getJSONObject("enhance_phrases_global").getJSONArray("japanese");
                for (int i = 0; i < enhanceWords.length(); i++) {
                    phrases.add(enhanceWords.getString(i));
                }
                JSONArray enhanceWordsCustom = asrObject.getJSONObject("enhance_phrases_custom").getJSONArray("japanese");
                for (int i = 0; i < enhanceWordsCustom.length(); i++) {
                    phrases.add(enhanceWordsCustom.getString(i));
                }
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }
        }
        return phrases;
    }

    private void startVADMonitor(int seconds) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                stopVAD();
                vadMonitor = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            int vadTime = seconds * 1000 * vadTimes;
                            Log.d(TAG, "startVADMonitor = " + vadTime + "ms");
                            Thread.sleep(vadTime);
                            if (isEmpty) {
                                ASRClientService.this.onError("empty message");
                            } else {
                                ASRClientService.this.onFinalResult(partRec, 3);
                                Log.e(TAG, "close VAD with no 3rd vad end");
                            }
                        } catch (Exception ignore) {
                            Log.d(TAG, "cancel VAD Monitor");
                        }
                    }
                });
                vadMonitor.start();
            }
        });
    }


    private void stopVAD() {
        if (vadMonitor != null) {
            vadMonitor.interrupt();
            vadMonitor = null;
        }
    }

    private static Map<String, String> guestAccentMap = new HashMap<>();
    static {
        // spot_locale / google_default_accent

    }

    private void initAccentSupportMap(String customerAccentSupportList) {
        // init default accent template
        guestAccentMap.put("zh_TW", "cmn-Hant-TW");
        guestAccentMap.put("zh_CN", "cmn-Hans-CN");
        guestAccentMap.put("en_US", "en-US");
        guestAccentMap.put("japanese", "ja-JP");

        // override with user input value
        try {
            JSONObject jsonObj = new JSONObject(customerAccentSupportList);
            for (Iterator<String> it = jsonObj.keys(); it.hasNext(); ) {
                String key = it.next();
                guestAccentMap.put(key, jsonObj.getString(key));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void start(String lang, boolean _advanced, double wifiSpeed) {
        android.util.Log.d(TAG, "start: "+ guestAccentMap.toString());

        if(!active) {
            active = true;
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, String.format("start, asr_option = %s, lang = %s", currentModel.name(), lang));
                    mlang = lang;
//                    googleAccent = guestAccentMap.getOrDefault(lang, "cmn-Hant-TW");
                    partRec = "";
                    advanced = _advanced;
                    vadTimes = (int) Math.round(wifiSpeed / 20.0);
                    if (vadTimes == 0) {
                        vadTimes = 1;
                    } else if (vadTimes > 2) {
                        vadTimes = 2;
                    }
                    isEmpty = true;
                    if (currentModel == ASRModel.Google) {
                        try {
                            gclient.start(mlang, 1, getEnhancePhrases(mlang), guestAccentMap);
                            startVADMonitor(advanced ? 15 : 5);
                        } catch (Exception ex) {
                            Log.e(TAG, "Error of Google Initalization: " + ex.getMessage());
                        }
                    } else if (currentModel == ASRModel.Xfyun) {
                        try {
                            xfyunClient.start(mlang, vadTimes, getEnhancePhrases(mlang), guestAccentMap);
                            startVADMonitor(advanced ? 15 : 10);
                        } catch (Exception ex) {
                            Log.e(TAG, "Error of Xfyun Initalization: " + ex.getMessage());
                        }
                    }else if (currentModel == ASRModel.SignalR ) {
                        try {
                            signalrClient.start(mlang, vadTimes, getEnhancePhrases(mlang), guestAccentMap);
                            //startVADMonitor(advanced ? 15 : 10);
                        } catch (Exception ex) {
                            Log.e(TAG, "Error of Azure Initalization: " + ex.getMessage());
                        }
                    }
                }
            });
        }
    }

    private ByteArrayBuffer mReadBuffer = new ByteArrayBuffer(204800);

    public void inputAudio(byte[] audio) {
        if (currentModel == ASRModel.Google) {
            gclient.inputAudio(audio);
        } else if (currentModel == ASRModel.Xfyun) {
            xfyunClient.inputAudio(audio);
        }if (currentModel == ASRModel.SignalR) {
            signalrClient.inputAudio(audio);
        }
    }


    @Override
    public void onPartialASRResult(String part_result) {
        if (part_result == null || part_result.isEmpty()) {
            return;
        }
        try {
            isEmpty = false;
            if (currentModel == ASRModel.Xfyun) {
                startVADMonitor(1);
            }else if (currentModel == ASRModel.SignalR) {

            }
            else {
                startVADMonitor(2);
            }
            partRec = part_result;
            JSONObject partial = new JSONObject();
            partial.put("interim", part_result);
            partial.put("final", "");
            listener.onPartialASRResult(partial.toString());
        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage());
        }
    }


    @Override
    public void onFinalResult(String finalResult, int state) {
        Log.d(TAG, "onFinalResult = " + finalResult + ", state = " + state);
        stop();
        askMultiService(finalResult);
    }


    @Override
    public void onError(String error_msg) {
        Log.e(TAG, "onError = " + error_msg);
        listener.onCancel();
        stop();
    }


    private String partRec = "";

    private synchronized void askMultiService(String finalresult) {
        Log.d(TAG, "askMultiService");
        handler.post(new Runnable() {
            @Override
            public void run() {
                Log.e(TAG, "multi_run");
                if (finalresult.isEmpty()) {
                    android.util.Log.e(TAG, "final result is empty");
                    listener.onCancel();
                    return;
                }

                if (advanced) {
                    try {
                        JSONObject final_json = new JSONObject();
                        final_json.put("interim", finalresult);
                        final_json.put("final", finalresult);
                        final_json.put("mi", "{\"mi\":[\"" + finalresult + "\"]}");
                        Log.d(TAG, final_json);
                        listener.onFinalResult(final_json.toString());
                    } catch (JSONException e) {
                        Log.e(TAG, e.toString());
                    }
                    return;
                }

                String res = "";
                if (mlang.equals("zh_TW") || mlang.equals("zh_CN")||mlang.equals("zh-TW")||mlang.equals("zh-CN")) {
                    HttpURLConnection connection = null;
                    InputStream inputStream = null;
                    try {
                        JSONObject final_json = new JSONObject();
                        final_json.put("interim", finalresult);
                        final_json.put("final", finalresult);

                        JSONObject data = new JSONObject();
                        data.put("context", finalresult);
                        String jsonstring = "{\"context\":" + finalresult + "}";
                        URL url = new URL(mi_url);
                        connection = (HttpURLConnection) url.openConnection();
                        connection.setRequestMethod("POST");
                        connection.setRequestProperty("Content-Type", "application/json");
                        connection.setDoInput(true);
                        connection.setDoOutput(true);
                        connection.setConnectTimeout(5000);
                        connection.setReadTimeout(5000);
                        setPostRequestContent(connection, data);
                        connection.connect();
                        int responsCode = connection.getResponseCode();

                        if (responsCode == HttpURLConnection.HTTP_OK) {
                            inputStream = connection.getInputStream();
                            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                            StringBuilder result = new StringBuilder();
                            String line;
                            while ((line = reader.readLine()) != null) {
                                result.append(line);
                            }
                            res = result.toString();
                            res = StringEscapeUtils.unescapeJava(res);
                            final_json.put("mi", res);
                            Log.d(TAG, final_json);
                            listener.onFinalResult(final_json.toString());
                        } else {
                            throw new Exception(connection.getResponseCode() + "");
                        }

                    } catch (Exception io) {
                        Log.e(TAG, "MultiIntent error : " + io.toString());
                        try {
                            JSONObject final_json = new JSONObject();
                            final_json.put("interim", finalresult);
                            final_json.put("final", finalresult);
                            JSONObject mi_workaround = new JSONObject();
                            mi_workaround.put("mi", finalresult);
                            final_json.put("mi", mi_workaround.toString());
                            listener.onFinalResult(final_json.toString());
                        } catch (Exception ex) {
                            Log.e(TAG, "MultiIntent error (new chinese json error): " + ex.toString());
                        }
                    } finally {
                        if (connection != null) {
                            try {
                                connection.getInputStream().close();
                            } catch (IOException e) {
                                Log.e(TAG, "3333" + e.toString());
                            }
                            connection.disconnect();
                            connection = null;
                        }
                    }
                } else {
                    try {
                        JSONObject final_json = new JSONObject();
                        final_json.put("interim", finalresult);
                        final_json.put("final", finalresult);
                        JSONObject mi_workaround = new JSONObject();
                        mi_workaround.put("mi", finalresult);
                        final_json.put("mi", mi_workaround.toString());
                        listener.onFinalResult(final_json.toString());
                    } catch (Exception ex) {
                        Log.e(TAG, "MultiIntent error (new eng json error): " + ex.toString());
                    }

                }
            }
        });
    }


    private void setPostRequestContent(HttpURLConnection conn,
                                       JSONObject jsonObject) throws IOException {
        OutputStream os = conn.getOutputStream();
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
        writer.write(jsonObject.toString());
        writer.flush();
        writer.close();
        os.close();
    }

    private boolean active;

    public void stop() {
        if (active) {
            active = false;
            handler.post(new Runnable() {
                @Override
                public void run() {
                    stopVAD();
                    if (currentModel == ASRModel.Google) {
                        gclient.stop();
                    } else if(currentModel == ASRModel.Xfyun){
                        xfyunClient.stop();
                    } else if(currentModel == ASRModel.SignalR){
                        signalrClient.stop();
                    }
                }
            });
        }
    }

}

