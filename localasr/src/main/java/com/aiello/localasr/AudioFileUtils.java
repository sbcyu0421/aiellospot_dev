package com.aiello.localasr;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class AudioFileUtils {

  private static String TAG = AudioFileUtils.class.getSimpleName();

  public static boolean writeFile(byte[] buffer, String filePath, boolean isAppend) {
    boolean fileCreate = false;
    File file = new File(filePath);
    FileOutputStream fos = null;
    try {
      if (!file.exists())
        file.createNewFile();

      fos = new FileOutputStream(file, isAppend);
      fos.write(buffer);
      fileCreate = true;
    } catch (IOException e) {
      com.litesuits.android.log.Log.e(TAG, e.toString());
    } finally {
      try {
        if (fos != null) {
          fos.flush();
          fos.close();
        }
      } catch (IOException e) {
        com.litesuits.android.log.Log.e(TAG, e.toString());
      }
    }
    return fileCreate;
  }

}
