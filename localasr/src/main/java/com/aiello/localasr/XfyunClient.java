package com.aiello.localasr;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.iflytek.cloud.*;
import com.iflytek.cloud.ui.RecognizerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ai.aiello.chinese.ChineseUtils;

import static com.aiello.localasr.AccentConvertMap.googleToXfyunConvertMap;

public class XfyunClient extends ASRClient {
    private static String TAG = XfyunClient.class.getSimpleName();
    private SpeechRecognizer mIat;
    private String mEngineType = SpeechConstant.TYPE_CLOUD;
    // 语音听写UI
    private RecognizerDialog mIatDialog;
    // 用HashMap存储听写结果
    private HashMap<String, String> mIatResults = new LinkedHashMap<String, String>();
    private String resultType = "json";
    private boolean cyclic = false;//音频流识别是否循环调用
    private StringBuffer buffer = new StringBuffer();
    private static boolean _asrstatus;
    private ASRClientListener mlisterner;
    private JSONObject configObject = null;
    private String lang;

    private final int END_OF_SPEED_DELAY_MS = 500;

    XfyunClient(Context mctx, String config, ASRClientListener listener) {
        this.mlisterner = listener;
        try {
            configObject = new JSONObject(config);
        } catch (Exception e) {
            Log.e(TAG, "config cast error = " + e.toString());
        }
        try {
            SpeechUtility.createUtility(mctx, SpeechConstant.APPID + "= " + configObject.getString("key"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mIat = SpeechRecognizer.createRecognizer(mctx, mInitListener);
    }

    private InitListener mInitListener = new InitListener() {

        @Override
        public void onInit(int code) {
            Log.d("", "SpeechRecognizer init() code = " + code);
            if (code != ErrorCode.SUCCESS) {
                Log.e(TAG, "初始化失败，错误码：" + code + ",请点击网址https://www.xfyun.cn/document/error-code查询解决方案");
            }
        }
    };

    public void setParam(String lang) {
        mIat.setParameter(SpeechConstant.PARAMS, null);

        // 设置听写引擎
        mIat.setParameter(SpeechConstant.ENGINE_TYPE, mEngineType);
        // 设置返回结果格式
        mIat.setParameter(SpeechConstant.RESULT_TYPE, resultType);
//        mIat.setParameter(SpeechConstant.ASR_DWA, "1");
        mIat.setParameter("dwa", "wpgs");
        mIat.setParameter(SpeechConstant.LANGUAGE, lang);
        mIat.setParameter(SpeechConstant.AUDIO_SOURCE, "-1");
        mIat.setParameter(SpeechConstant.FILTER_AUDIO_TIME, "500");
        Log.e(TAG, "last language:" + mIat.getParameter(SpeechConstant.LANGUAGE));

        //此处用于设置dialog中不显示错误码信息
        //mIat.setParameter("view_tips_plain","false");

        // 设置语音前端点:静音超时时间，即用户多长时间不说话则当做超时处理
        mIat.setParameter(SpeechConstant.VAD_BOS, "8000");

        // 设置语音后端点:后端点静音检测时间，即用户停止说话多长时间内即认为不再输入， 自动停止录音
        try {
            mIat.setParameter(SpeechConstant.VAD_EOS, configObject.getString("vad"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // 设置标点符号,设置为"0"返回结果无标点,设置为"1"返回结果有标点
        mIat.setParameter(SpeechConstant.ASR_PTT, "0");

        // 设置音频保存路径，保存音频格式支持pcm、wav，设置路径为sd卡请注意WRITE_EXTERNAL_STORAGE权限
        //   mIat.setParameter(SpeechConstant.AUDIO_FORMAT,"wav");
        //   mIat.setParameter(SpeechConstant.ASR_AUDIO_PATH, Environment.getExternalStorageDirectory()+"/msc/iat.wav");
        _asrstatus = true;
    }

    private RecognizerListener mRecognizerListener = new RecognizerListener() {

        @Override
        public void onBeginOfSpeech() {
            // 此回调表示：sdk内部录音机已经准备好了，用户可以开始语音输入
            Log.d(TAG, "開始說話");
        }

        @Override
        public void onError(SpeechError error) {
            // Tips：
            // 错误码：10118(您没有说话)，可能是录音机权限被禁，需要提示用户打开应用的录音权限。
            Log.e(TAG, "Error:" + error.toString());

        }

        @Override
        public void onEndOfSpeech() {
            // 此回调表示：检测到了语音的尾端点，已经进入识别过程，不再接受语音输入
            Log.e(TAG, "結束說話");
            new Handler().postDelayed(() -> {
                mlisterner.onFinalResult(words, 2);
                _asrstatus = false;
            }, END_OF_SPEED_DELAY_MS);
        }

        @Override
        public void onResult(RecognizerResult results, boolean isLast) {
            Log.d(TAG, results.getResultString());
            if (resultType.equals("json")) {
                printResult(results);
            } else if (resultType.equals("plain")) {
                buffer.append(results.getResultString());

            }

            if (isLast & cyclic) {

                mlisterner.onFinalResult(buffer.toString(), 2);
            }
        }

        @Override
        public void onVolumeChanged(int volume, byte[] data) {

            //  Log.d(TAG, "返回音频数据："+data.length);
        }

        @Override
        public void onEvent(int eventType, int arg1, int arg2, Bundle obj) {
            // 以下代码用于获取与云端的会话id，当业务出错时将会话id提供给技术支持人员，可用于查询会话日志，定位出错原因
            // 若使用本地能力，会话id为null
            //	if (SpeechEvent.EVENT_SESSION_ID == eventType) {
            //		String sid = obj.getString(SpeechEvent.KEY_EVENT_SESSION_ID);
            //		Log.d(TAG, "session id =" + sid);
            //	}
        }
    };

    static String words = "";

    private void printResult(RecognizerResult results) {
        String text = JsonParser.parseIatResult(results.getResultString());

        String sn = null;
        String pgs = null;
        String rg = null;
        // 读取json结果中的sn字段
        try {
            JSONObject resultJson = new JSONObject(results.getResultString());
            sn = resultJson.optString("sn");
            pgs = resultJson.optString("pgs");
            rg = resultJson.optString("rg");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //如果pgs是rpl就在已有的结果中删除掉要覆盖的sn部分
        if (pgs.equals("rpl")) {
            String[] strings = rg.replace("[", "").replace("]", "").split(",");
            int begin = Integer.parseInt(strings[0]);
            int end = Integer.parseInt(strings[1]);
            for (int i = begin; i <= end; i++) {
                mIatResults.remove(i + "");
            }
        }

        mIatResults.put(sn, text);
        StringBuffer resultBuffer = new StringBuffer();
        for (String key : mIatResults.keySet()) {
            resultBuffer.append(mIatResults.get(key));
        }
        String best_result;
        if (lang.equals("zh-cn")) {
            best_result = ChineseUtils.toTraditional(resultBuffer.toString(), false);
        } else {
            best_result = resultBuffer.toString();
        }
        words = best_result;
        mlisterner.onPartialASRResult(best_result);

    }

    @Override
    void inputAudio(byte[] buffer) {
        if (_asrstatus)
            mIat.writeAudio(buffer, 0, buffer.length);
    }

    @Override
    void start(String lang, int bufferTimes, List<String> enhancePhrases, Map<String, String> guestAccentMap) {
        android.util.Log.d(TAG, "start with xfyunClient, asr accent map = "+ guestAccentMap.toString() + " , spotLang = " + lang);
        mIatResults.clear();
        words = "";

        // 1. parse spot_lang -> google_lang -> xfyun_lang
        try {
            String userInputLang = guestAccentMap.get(lang); // get google lang
            this.lang = googleToXfyunConvertMap.get(userInputLang);
            if (this.lang == null) throw new Exception("Invalid Xyfun accent");
            android.util.Log.d(TAG, "Success to convert accent, xfyun accent = " + this.lang);

        } catch (Exception e) {
            if (lang.equals("zh_TW")) {
                this.lang = "zh-cn";
            } else if (lang.equals("zh_CN")) {
                this.lang = "zh-cn";
            } else if (lang.equals("en_US")) {
                this.lang = "en_us";
            } else if (lang.equals("japanese")) {
                this.lang = "ja_jp";
            } else {
                this.lang = "zh-cn";
            }
            android.util.Log.d(TAG, "Fail to convert accent, use default accent = " + this.lang);
            e.printStackTrace();
        }

        // 2. start SpeechRecognizer
        try {
            setParam(this.lang);
            mIat.startListening(mRecognizerListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    void stop() {
        Log.i(TAG, "onStop.");
        try {
            mIat.cancel();
        } catch (Exception e) {
            Log.e(TAG, "xfyun client stop, error = " + e.toString());
        }
    }

}
