package com.aiello.localasr;

import java.util.HashMap;
import java.util.Map;

public class AccentConvertMap {

    public static Map<String, String> googleToGoogleConvertMap = new HashMap<>();
    static {
        // google_locale / xfyun_accent
        googleToGoogleConvertMap.put("yue-Hant-HK", "yue-Hant-HK");
        googleToGoogleConvertMap.put("cmn-Hans-CN", "cmn-Hans-CN");
        googleToGoogleConvertMap.put("cmn-Hant-TW", "cmn-Hant-TW");
        googleToGoogleConvertMap.put("en-AU", "en-AU");
        googleToGoogleConvertMap.put("en-CA", "en-CA");
        googleToGoogleConvertMap.put("en-GH", "en-GH");
        googleToGoogleConvertMap.put("en-HK", "en-HK");
        googleToGoogleConvertMap.put("en-IN", "en-IN");
        googleToGoogleConvertMap.put("en-IE", "en-IE");
        googleToGoogleConvertMap.put("en-KE", "en-KE");
        googleToGoogleConvertMap.put("en-NZ", "en-NZ");
        googleToGoogleConvertMap.put("en-NG", "en-NG");
        googleToGoogleConvertMap.put("en-PK", "en-PK");
        googleToGoogleConvertMap.put("en-PH", "en-PH");
        googleToGoogleConvertMap.put("en-SG", "en-SG");
        googleToGoogleConvertMap.put("en-ZA", "en-ZA");
        googleToGoogleConvertMap.put("en-TZ", "en-TZ");
        googleToGoogleConvertMap.put("en-GB", "en-GB");
        googleToGoogleConvertMap.put("en-US", "en-US");
        googleToGoogleConvertMap.put("ms-MY", "ms-MY");
        googleToGoogleConvertMap.put("hi-IN", "hi-IN");
        googleToGoogleConvertMap.put("ru-RU", "ru-RU");
        googleToGoogleConvertMap.put("ja-JP", "ja-JP");
        googleToGoogleConvertMap.put("ko-KR", "ko-KR");
        googleToGoogleConvertMap.put("vi-VN", "vi-VN");
        googleToGoogleConvertMap.put("th-TH", "th-TH");
        googleToGoogleConvertMap.put("bg-BG", "bg-BG");
        googleToGoogleConvertMap.put("fr-BE", "fr-BE");
        googleToGoogleConvertMap.put("fr-CA", "fr-CA");
        googleToGoogleConvertMap.put("fr-FR", "fr-FR");
        googleToGoogleConvertMap.put("fr-CH", "fr-CH");
        googleToGoogleConvertMap.put("de-AT", "de-AT");
        googleToGoogleConvertMap.put("de-DE", "de-DE");
        googleToGoogleConvertMap.put("de-CH", "de-CH");
        googleToGoogleConvertMap.put("ar-DZ", "ar-DZ");
        googleToGoogleConvertMap.put("ar-BH", "ar-BH");
        googleToGoogleConvertMap.put("ar-EG", "ar-EG");
        googleToGoogleConvertMap.put("ar-IQ", "ar-IQ");
        googleToGoogleConvertMap.put("ar-IL", "ar-IL");
        googleToGoogleConvertMap.put("ar-JO", "ar-JO");
        googleToGoogleConvertMap.put("ar-KW", "ar-KW");
        googleToGoogleConvertMap.put("ar-LB", "ar-LB");
        googleToGoogleConvertMap.put("ar-MA", "ar-MA");
        googleToGoogleConvertMap.put("ar-OM", "ar-OM");
        googleToGoogleConvertMap.put("ar-QA", "ar-QA");
        googleToGoogleConvertMap.put("ar-SA", "ar-SA");
        googleToGoogleConvertMap.put("ar-PS", "ar-PS");
        googleToGoogleConvertMap.put("ar-TN", "ar-TN");
        googleToGoogleConvertMap.put("ar-AE", "ar-AE");
        googleToGoogleConvertMap.put("ar-YE", "ar-YE");
        googleToGoogleConvertMap.put("id-ID", "id-ID");
        googleToGoogleConvertMap.put("bn-BD", "bn-BD");
        googleToGoogleConvertMap.put("bn-IN", "bn-IN");
    }

    public static Map<String, String> googleToXfyunConvertMap = new HashMap<>();
    static {
        // google_locale / xfyun_accent
        googleToXfyunConvertMap.put("yue-Hant-HK", "zh_cn");
        googleToXfyunConvertMap.put("cmn-Hans-CN", "zh_cn");
        googleToXfyunConvertMap.put("cmn-Hant-TW", "zh_cn");
        googleToXfyunConvertMap.put("en-AU", "en_us");
        googleToXfyunConvertMap.put("en-CA", "en_us");
        googleToXfyunConvertMap.put("en-GH", "en_us");
        googleToXfyunConvertMap.put("en-HK", "en_us");
        googleToXfyunConvertMap.put("en-IN", "en_us");
        googleToXfyunConvertMap.put("en-IE", "en_us");
        googleToXfyunConvertMap.put("en-KE", "en_us");
        googleToXfyunConvertMap.put("en-NZ", "en_us");
        googleToXfyunConvertMap.put("en-NG", "en_us");
        googleToXfyunConvertMap.put("en-PK", "en_us");
        googleToXfyunConvertMap.put("en-PH", "en_us");
        googleToXfyunConvertMap.put("en-SG", "en_us");
        googleToXfyunConvertMap.put("en-ZA", "en_us");
        googleToXfyunConvertMap.put("en-TZ", "en_us");
        googleToXfyunConvertMap.put("en-GB", "en_us");
        googleToXfyunConvertMap.put("en-US", "en_us");
        googleToXfyunConvertMap.put("ms-MY", "ms_MY");
        googleToXfyunConvertMap.put("hi-IN", "hi_in");
        googleToXfyunConvertMap.put("ru-RU", "ru-ru");
        googleToXfyunConvertMap.put("ja-JP", "ja_jp");
        googleToXfyunConvertMap.put("ko-KR", "ko_kr");
        googleToXfyunConvertMap.put("vi-VN", "vi_VN");
        googleToXfyunConvertMap.put("th-TH", "th_TH");
        googleToXfyunConvertMap.put("bg-BG", "bg_bg");
        googleToXfyunConvertMap.put("fr-BE", "fr_fr");
        googleToXfyunConvertMap.put("fr-CA", "fr_fr");
        googleToXfyunConvertMap.put("fr-FR", "fr_fr");
        googleToXfyunConvertMap.put("fr-CH", "fr_fr");
        googleToXfyunConvertMap.put("de-AT", "de_DE");
        googleToXfyunConvertMap.put("de-DE", "de_DE");
        googleToXfyunConvertMap.put("de-CH", "de_DE");
        googleToXfyunConvertMap.put("ar-DZ", "ar_il");
        googleToXfyunConvertMap.put("ar-BH", "ar_il");
        googleToXfyunConvertMap.put("ar-EG", "ar_il");
        googleToXfyunConvertMap.put("ar-IQ", "ar_il");
        googleToXfyunConvertMap.put("ar-IL", "ar_il");
        googleToXfyunConvertMap.put("ar-JO", "ar_il");
        googleToXfyunConvertMap.put("ar-KW", "ar_il");
        googleToXfyunConvertMap.put("ar-LB", "ar_il");
        googleToXfyunConvertMap.put("ar-MA", "ar_il");
        googleToXfyunConvertMap.put("ar-OM", "ar_il");
        googleToXfyunConvertMap.put("ar-QA", "ar_il");
        googleToXfyunConvertMap.put("ar-SA", "ar_il");
        googleToXfyunConvertMap.put("ar-PS", "ar_il");
        googleToXfyunConvertMap.put("ar-TN", "ar_il");
        googleToXfyunConvertMap.put("ar-AE", "ar_il");
        googleToXfyunConvertMap.put("ar-YE", "ar_il");
        googleToXfyunConvertMap.put("id-ID", "id_ID");
        googleToXfyunConvertMap.put("bn-BD", "bn_BD");
        googleToXfyunConvertMap.put("bn-IN", "bn_BD");
    }

    public static Map<String, String> googleToSignalRConvertMap = new HashMap<>();
    static {
        // google_locale / SignalR_accent
        googleToSignalRConvertMap.put("ue-Hant-HK", "zh-CN");
        googleToSignalRConvertMap.put("cmn-Hans-CN", "zh-CN");
        googleToSignalRConvertMap.put("cmn-Hant-TW", "zh-TW");
        googleToSignalRConvertMap.put("en-AU", "en-US");
        googleToSignalRConvertMap.put("en-CA", "en-US");
        googleToSignalRConvertMap.put("en-GH", "en-US");
        googleToSignalRConvertMap.put("en-HK", "en-US");
        googleToSignalRConvertMap.put("en-IN", "en-US");
        googleToSignalRConvertMap.put("en-IE", "en-US");
        googleToSignalRConvertMap.put("en-KE", "en-US");
        googleToSignalRConvertMap.put("en-NZ", "en-US");
        googleToSignalRConvertMap.put("en-NG", "en-US");
        googleToSignalRConvertMap.put("en-PK", "en-US");
        googleToSignalRConvertMap.put("en-PH", "en-US");
        googleToSignalRConvertMap.put("en-SG", "en-US");
        googleToSignalRConvertMap.put("en-ZA", "en-US");
        googleToSignalRConvertMap.put("en-TZ", "en-US");
        googleToSignalRConvertMap.put("en-GB", "en-US");
        googleToSignalRConvertMap.put("en-US", "en-US");
        googleToSignalRConvertMap.put("ja-JP", "ja-JP");
    }
}
