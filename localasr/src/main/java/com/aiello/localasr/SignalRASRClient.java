package com.aiello.localasr;

import android.util.Log;

import com.microsoft.signalr.HubConnection;
import com.microsoft.signalr.HubConnectionBuilder;
import com.microsoft.signalr.HubConnectionState;
import com.microsoft.signalr.OnClosedCallback;
import com.microsoft.signalr.messagepack.MessagePackHubProtocol;

import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;
import java.util.Map;

import static com.aiello.localasr.AccentConvertMap.googleToSignalRConvertMap;
import static com.aiello.localasr.AccentConvertMap.googleToXfyunConvertMap;

public class SignalRASRClient extends ASRClient {
    private static final String TAG = SignalRASRClient.class.getSimpleName();
    ASRClientListener mListener;
    HubConnection hubConnection;
    public static int buffer_count = 0;
    private EncoderWrapper.OggOpusEncoder wrapper;
    private static int bufferSize = 4096;
    private boolean canBeSend = false;
    private ByteArrayBuffer mReadBuffer = new ByteArrayBuffer(204800);
    private String hotelname;
    private String roomname;
    public SignalRASRClient( ASRClientListener listener, String credentials_google,String hotelname,String roomname){
        wrapper = new EncoderWrapper.OggOpusEncoder();
        mListener = listener;
        this.hotelname = hotelname;
        this.roomname = roomname;
    }
    @Override
    void inputAudio(byte[] buffer) {
        if (buffer_count > 20) { //Miss first 20 buffer :32x20 = 640 ms by purpose to filter Ding
            mReadBuffer.append(buffer, 0, buffer.length);
            if (!canBeSend) {
                return;
            }
            if (mReadBuffer.toByteArray().length < bufferSize) {
                return;
            } else {

                byte[] encodeddata = wrapper.processAudioBytes(mReadBuffer.toByteArray(), 0, mReadBuffer.length());
                Log.d("Azure", "encoded buffer is sent : " + encodeddata.length);
                if (encodeddata != null) {

                    hubConnection.send("ReceiveAudio",encodeddata);
                    mReadBuffer.clear();
                    // stream.write(encodeddata);
                    Log.d("AzureAudio", "encoded buffer is sent : " + encodeddata.length);
                }
            }
        }else{
            buffer_count++;
        }
    }
    public static int[] toIntArray(byte buf[])
    {
        final ByteBuffer buffer = ByteBuffer.wrap(buf)
                .order(ByteOrder.LITTLE_ENDIAN);
        final int[] ret = new int[buf.length / 4];
        buffer.asIntBuffer().put(ret);
        return ret;
    }
    @Override
    void start(String lang, int bufferTimes, List<String> enhancePhrases, Map<String, String> guestAccentMap) {

//        switch (lang) {
//            case "zh_TW":
//                lang = "zh-TW";
//                break;
//            case "zh_CN":
//                lang = "zh-CN";
//                break;
//            case "en_US":
//                lang = "en-US";
//                break;
//            case "japanese":
//                lang = "ja-JP";
//                break;
//        }

        String signalRLang = "";
        // 1. parse spot_lang -> google_lang -> signalR_lang
        try {
            String userInputLang = guestAccentMap.get(lang); // get google lang
            signalRLang = googleToXfyunConvertMap.get(userInputLang);
            if (signalRLang == null) throw new Exception("Invalid Azure accent");
            Log.d(TAG, "Azure client: Success to convert accent from google -> Azure, accent = " + signalRLang);

//            signalRLang = AccentConvertMap.googleToSignalRConvertMap.get(googleLang);
//            Log.d(TAG, "Azure client: Success to convert accent from google -> Azure, accent = " + signalRLang);

        } catch (Exception e) {
            if (lang.equals("zh_TW")) {
                signalRLang = "zh-TW";
            } else if (lang.equals("zh_CN")) {
                signalRLang = "zh-CN";
            } else if (lang.equals("en_US")) {
                signalRLang = "en-US";
            } else if (lang.equals("japanese")) {
                signalRLang = "ja-JP";
            } else {
                signalRLang = "zh-TW";
            }
            Log.d(TAG, "Azure client: Fail to convert accent from google -> Azure, use default accent = " + signalRLang);
            e.printStackTrace();
        }

        Log.e(TAG, "Azure Client start, lang =:"+signalRLang);

        // 2. start SpeechRecognizer
        try {
            wrapper.init(16000, 12000, true);
        }catch(Exception ex){
            Log.e("Azure","Initalization Failure for Ogg Encoder");
        }
        hubConnection = HubConnectionBuilder.create("http://65.52.165.205:3045/voicehub")
        //        .withHubProtocol(new MessagePackHubProtocol())
                .build();

     //   hubConnection.setServerTimeout(3000);
        hubConnection.on("PartialSpeech",(message) -> {
           mListener.onPartialASRResult(message);
        }, String.class);
        hubConnection.on("FinalSpeech",(message) -> { canBeSend = false;
           message =  message.replaceAll("\\p{P}","");
           mListener.onFinalResult(message,2);
        }, String.class);
        hubConnection.on("Error",(message) -> {
            mListener.onError(message);
        }, String.class);
        hubConnection.on("SpeechEndDetected",(message) -> {
            mListener.onError("SignalR Disconnect");
        }, String.class);

        hubConnection.onClosed(new OnClosedCallback() {
            @Override
            public void invoke(Exception exception) {
                if(exception!=null)
                mListener.onError("Exception:"+exception.getMessage());
               // stop();
            }
        });
        Log.d("Azure","Connecting SignalR");
        try {
            hubConnection.start().blockingAwait();
            canBeSend = true;
            buffer_count = 0;
            Log.d("Azure SignalR", "Start hub audio");
            hubConnection.send("Configuration",hotelname,roomname,signalRLang);

        }catch(Exception ex){
            mListener.onError("No SignalR Connection");
        }

     //   Log.d("Azure","Start Audio");
    }

    @Override
    void stop() {
        Log.d("Azure","Call Client Stop");
        try {
            if(hubConnection!=null && hubConnection.getConnectionState() == HubConnectionState.CONNECTED ) {
                canBeSend = false;
                Log.d("Azure","Call SignalR disconnect");
                hubConnection.send("Stop", "Stop by Spot");
                Thread.sleep(100);
                hubConnection.stop().blockingAwait();
            }
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        hubConnection = null;



    }
}
