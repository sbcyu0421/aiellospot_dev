package com.aiello.localasr;

public interface ASRClientListener {

    void onPartialASRResult(String part_result);

    void onFinalResult(String final_result, int state);

    void onError(String error_msg);

}
