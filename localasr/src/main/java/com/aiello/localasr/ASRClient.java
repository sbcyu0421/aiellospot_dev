package com.aiello.localasr;

import java.util.List;
import java.util.Map;

public abstract class ASRClient {

    abstract void inputAudio(byte[] buffer);

    abstract void start(String lang, int bufferTimes, List<String> enhancePhrases, Map<String, String> guestAccentMap);

    abstract void stop();

}
