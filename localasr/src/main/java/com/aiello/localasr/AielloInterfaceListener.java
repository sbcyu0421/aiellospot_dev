package com.aiello.localasr;

public interface AielloInterfaceListener {
    void onPartialASRResult(String part_result);
    void onFinalResult(String final_result);
    void onError(String error_msg);
    void onCancel();
}
