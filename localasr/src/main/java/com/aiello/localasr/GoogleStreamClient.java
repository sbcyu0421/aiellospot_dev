package com.aiello.localasr;

import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.api.gax.rpc.ClientStream;
import com.google.api.gax.rpc.ResponseObserver;
import com.google.api.gax.rpc.StreamController;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.speech.v1p1beta1.*;
import com.google.protobuf.ByteString;
import com.litesuits.android.log.Log;

import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.aiello.localasr.AccentConvertMap.googleToGoogleConvertMap;

/**
 * Client that sends streaming audio to Speech.Recognize and returns streaming transcript.
 */
public class GoogleStreamClient extends ASRClient {

    private static String TAG = GoogleStreamClient.class.getSimpleName();
    private int mSamplingRate;
    private SpeechClient mSpeechClient;
    private ASRClientListener mListener;
    private ResponseObserver<StreamingRecognizeResponse> responseObserver = null;
    private ClientStream<StreamingRecognizeRequest> clientStream;
    private String partial_result;
    private String lang;
    private JSONObject configObject = null;
    private boolean canBeSend;
    private EncoderWrapper.OggOpusEncoder wrapper;

    ByteArrayBuffer file_storage=new ByteArrayBuffer(1280000);
    /**
     * Construct client connecting to Cloud Speech server at {@code host:port}.
     */
    GoogleStreamClient(int samplingRate, String credentials_google, ASRClientListener listener) {
        try {
            this.mSamplingRate = samplingRate;
            this.mListener = listener;
            configObject = new JSONObject(credentials_google);
            Log.e(customTAG, "Start time:" + System.currentTimeMillis());
            String credentialsFiles = configObject.getString("credentials_files");
            GoogleCredentials credentials = GoogleCredentials.fromStream(new ByteArrayInputStream(credentialsFiles.getBytes(StandardCharsets.UTF_8)));
            FixedCredentialsProvider credentialsProvider = FixedCredentialsProvider.create(credentials);

            SpeechSettings speechSettings = SpeechSettings.newBuilder()
                    .setCredentialsProvider(credentialsProvider)
                    .build();
            mSpeechClient = SpeechClient.create(speechSettings);
            Log.e(customTAG, "End time:" + System.currentTimeMillis());

            responseObserver = new ResponseObserver<StreamingRecognizeResponse>() {

                public void onStart(StreamController controller) {
                    Log.e(observerTAG, "onStart");
                    partial_result = "";
                    mReadBuffer.clear();
                }

                public void onResponse(StreamingRecognizeResponse response) {
                    if (response.getSpeechEventType() != StreamingRecognizeResponse.SpeechEventType.END_OF_SINGLE_UTTERANCE) {
                        StreamingRecognitionResult result = response.getResultsList().get(0);
                        SpeechRecognitionAlternative alternative = result.getAlternativesList().get(0);
                        partial_result = alternative.getTranscript();
                        if (!result.getIsFinal()) {
                            this.onASRPartial();
                        } else {
                            this.onASRFinal();
                        }
                    }
                }

                @Override
                public void onComplete() {
                    Log.e(observerTAG, "onComplete");
                }

                public void onError(Throwable t) {
                    Log.e(observerTAG, "onError : " + t.getCause());
                    mListener.onError(t.getCause().toString());
                }


                //custom event
                void onASRPartial() {
                    Log.e(observerTAG, "onASRPartial");
                    if (canBeSend) {
                        mListener.onPartialASRResult(partial_result);
                    }
                }

                void onASRFinal() {
                    Log.e(observerTAG, "onASRFinal");
                    if (canBeSend) {
                        mListener.onFinalResult(partial_result, 1);
                    }
                }
            };
        } catch (Exception ex) {
            Log.e(TAG, "Error initial mSpeechClient:" + ex.getMessage());
        }
    }


    private ByteArrayBuffer mReadBuffer = new ByteArrayBuffer(204800);
    private static int bufferSize = 2048;

    @Override
    void inputAudio(byte[] buffer) {
        try {
            Log.d(customTAG, "SoundAI buffer is sent : " + buffer.length);
            mReadBuffer.append(buffer, 0, buffer.length);

            if (clientStream == null || !clientStream.isSendReady()) {
//                Log.d(customTAG, "send is not ready, buffer appended");
                return;
            }
            if (mReadBuffer.toByteArray().length < bufferSize) {
//                Log.d(customTAG, "buffer length less than " + (2048 * bufferTimes) + ", buffer appended");
                return;
            }
            if (!mSpeechClient.isTerminated() && canBeSend) {
                synchronized(this) {
                    byte[] encodeddata = wrapper.processAudioBytes(mReadBuffer.toByteArray(), 0, mReadBuffer.length());
                    Log.d(customTAG, "encoded buffer is sent : " + encodeddata.length);
                    if (encodeddata != null) {
                        StreamingRecognizeRequest request =
                                StreamingRecognizeRequest.newBuilder()
                                        .setAudioContent(ByteString.copyFrom(encodeddata, 0, encodeddata.length))
                                        .build();
                        file_storage.append(encodeddata, 0, encodeddata.length);

                        clientStream.send(request);
                    }
                }
                mReadBuffer.clear();



            }
        } catch (Exception e) {
            Log.d(TAG, "not ready, exception = " + e.toString());
        }
    }


    private String customTAG = "Google_ASR_DEBUG";
    private String observerTAG = "OBSERVER";

    public static int[] getInt(byte[] arr) {
        int[] ibuff= new int[arr.length/2];
        int counter=0;
        for(int i=0;i<arr.length-2;i+=2) {
            ibuff[counter] = arr[i]<<8 &0xFF00 | arr[i+1]&0xFF;
            counter++;
        }
        return ibuff;
    } // end of getInt
    private void initializeRecognition(List<String> enhanceWord) throws InterruptedException, IOException {
        Log.d(customTAG, "initializeRecognition");
        mReadBuffer.clear();
        clientStream = mSpeechClient.streamingRecognizeCallable().splitCall(responseObserver);

        if (mSpeechClient != null) {

            Log.d(TAG, "enhance phrases = " + enhanceWord);
            wrapper = new EncoderWrapper.OggOpusEncoder();
            try {
                wrapper.init(16000, 12000, true);
            }catch(Exception ex){
                Log.e("Encoder","Initalization Failure for Ogg Encoder");
            }
            SpeechContext speechContextsElement =
                    SpeechContext.newBuilder().addAllPhrases(enhanceWord).build();
            List<SpeechContext> speechContexts = Arrays.asList(speechContextsElement);

            RecognitionConfig recognitionConfig =
                    RecognitionConfig.newBuilder()
                            .setEncoding(RecognitionConfig.AudioEncoding.OGG_OPUS)
                            .setLanguageCode(lang)
                            .addAllSpeechContexts(speechContexts)
                            .setUseEnhanced(true)
                            .setDiarizationConfig(SpeakerDiarizationConfig.newBuilder()
                                    .setEnableSpeakerDiarization(false).build())
                            .setSampleRateHertz(mSamplingRate)
                            .build();

            StreamingRecognitionConfig streamingRecognitionConfig =
                    StreamingRecognitionConfig.newBuilder()
                            .setConfig(recognitionConfig)
                            .setInterimResults(true)
                            .setSingleUtterance(true)
                            .build();

            StreamingRecognizeRequest request =
                    StreamingRecognizeRequest.newBuilder()
                            .setStreamingConfig(streamingRecognitionConfig)
                            .build(); // The first request in a streaming call has to be a config
            clientStream.send(request);
            canBeSend = true;
        } else {
            mListener.onError("mSpeechClient is Null");
        }
    }

    @Override
    void start(String lang, int bufferTimes, List<String> enhancePhrases, Map<String, String> guestAccentMap) {
        bufferSize = 2048 * bufferTimes;
        android.util.Log.d(TAG, "start with googleClient, asr accent map = "+ guestAccentMap.toString() + " , spotLang = " + lang);
        // 1. parse spot_lang -> google_lang
        try {
            String userInputLang = guestAccentMap.get(lang);
            this.lang = googleToGoogleConvertMap.get(userInputLang);
            if (this.lang == null) throw new Exception("Invalid Google accent");
            android.util.Log.d(TAG, "Success to convert accent, google accent = " + this.lang);
        } catch (Exception e) {
            if (lang.equals("zh_TW")) {
                this.lang = "cmn-Hant-TW";
            } else if (lang.equals("zh_CN")) {
                this.lang = "cmn-Hans-CN";
            } else if (lang.equals("en_US")) {
                this.lang = "en-US";
            } else if (lang.equals("japanese")) {
                this.lang = "ja-JP";
            } else {
                this.lang = "cmn-Hant-TW";
            }
            android.util.Log.d(TAG, "Fail to convert accent, use default accent = " + this.lang);
            e.printStackTrace();
        }

        // 2. start SpeechRecognizer
        try {
            initializeRecognition(enhancePhrases);
        } catch (Exception e) {
            mListener.onError(e.toString());
            e.printStackTrace();
        }
    }

    @Override
    void stop() {
      
        try {
            Log.e(customTAG, "stop");
            synchronized (this) {
                byte[] finaldata = wrapper.flushAndStop();

                //file_storage.append(finaldata,0,finaldata.length);
                //AudioFileUtils.writeFile(file_storage.toByteArray(),Environment.getExternalStorageDirectory()+"/ogg_test.ogg",false);
                file_storage.clear();
                if (!canBeSend) {
                    return;
                }  
		canBeSend = false;

                //Dont really see any benefit doing so, so disable for now -- need to re-enable when needed
                StreamingRecognizeRequest request =
                        StreamingRecognizeRequest.newBuilder()
                                .setAudioContent(ByteString.copyFrom(finaldata, 0, finaldata.length))
                                .build();
                clientStream.send(request);
                Log.e(customTAG, "closeSend");
                clientStream.closeSend();
            }
            mReadBuffer.clear();
        } catch (Exception e) {
            Log.e(customTAG, "call stop, error = " + e.toString());
        }
    }


}

